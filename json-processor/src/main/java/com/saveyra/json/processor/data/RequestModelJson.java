package com.saveyra.json.processor.data;

import org.json.simple.JSONArray;



public class RequestModelJson {
	private String categoryType;
	private JSONArray jsonData;

	/**
	 * Default constructor
	 * 
	 */
	public RequestModelJson() {
	}

	/**
	 * Parameterized constructor
	 * 
	 * @param jsonData
	 */
	public RequestModelJson(String categoryType, JSONArray jsonData) {
		super();
		this.categoryType = categoryType;
		this.jsonData = jsonData;
	}

	/**
	 * @return the categoryType
	 */
	public String getCategoryType() {
		return categoryType;
	}

	/**
	 * @param categoryType the categoryType to set
	 */
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	/**
	 * @return the jsonData
	 */
	public JSONArray getJsonData() {
		return jsonData;
	}

	/**
	 * @param jsonData the jsonData to set
	 */
	public void setJsonData(JSONArray jsonData) {
		this.jsonData = jsonData;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RequestModelJson [categoryType=" + categoryType + ", jsonData=" + jsonData + "]";
	}
}
