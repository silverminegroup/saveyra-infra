package com.saveyra.json.processor.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.saveyra.json.processor.data.HomeRequestData;
import com.saveyra.json.processor.data.RestResponse;
import com.saveyra.json.processor.model.SmgSaveyraCardsEntity;
import com.saveyra.json.processor.service.SmgSaveyraCardsService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class HomeScreenController {
	private static final String UPDATE = "update";
	private static final String INSERT = "insert";
	private static final Logger LOGGER = LoggerFactory.getLogger(HomeScreenController.class);

	@Autowired
	private SmgSaveyraCardsService smgSaveyraCardsService;

	@SuppressWarnings("unchecked")
	@CrossOrigin
	@PostMapping("/home/screen/cards")
	public RestResponse saveHomeScreenCardsData(@Valid @RequestBody HomeRequestData requestData) {
		LOGGER.info("saveHomeScreenCardsData()==>POST /home/screen/cards call is initiated.");
		RestResponse response = new RestResponse();
		Map<String, Integer> operationAndCount = new HashMap<String, Integer>();
		operationAndCount.put(INSERT, 0);
		operationAndCount.put(UPDATE, 0);
		try {
			JSONParser parser = new JSONParser();
			JSONArray jsonArray = requestData.getCards();
			if (jsonArray != null) {
				Object object = parser.parse(String.valueOf(jsonArray));
				JSONArray cards = (JSONArray) object;
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				ArrayList<SmgSaveyraCardsEntity> insertCards = new ArrayList<>();
				ArrayList<SmgSaveyraCardsEntity> updateCards = new ArrayList<>();
				
				for (Object o : cards) {
					try {
						JSONObject card = (JSONObject) o;
						SmgSaveyraCardsEntity smgSaveyraCardsEntity = this.smgSaveyraCardsService
								.findByCardsInstanceId(card.get("instanceId").toString());
						if (smgSaveyraCardsEntity == null) {
							// Make sure all the fields should be there, when we insert otherwise ignore
							if (card.get("status") == null) {
								LOGGER.warn("ignored as input does not have all the data, which are required for inserts");
								continue;
							}
							smgSaveyraCardsEntity = new SmgSaveyraCardsEntity();
							SmgSaveyraCardsEntity newEntity = constructCardsData(smgSaveyraCardsEntity, card, sdf);
							if (newEntity != null) {
								operationAndCount.put(INSERT, operationAndCount.get(INSERT) + 1);
								insertCards.add(newEntity);
								//smgSaveyraCardsService.persist(newEntity);
							}
						} else {
							SmgSaveyraCardsEntity updateEntity = constructCardsData(smgSaveyraCardsEntity, card, sdf);
							if (updateEntity != null) {
								operationAndCount.put(UPDATE, operationAndCount.get(UPDATE) + 1);
								//smgSaveyraCardsService.update(updateEntity);
								updateCards.add(updateEntity);
							}
						}
					} catch (Exception e) {
						LOGGER.error("Inner try/catch block==>Exception: {}", e.getMessage(), e);
					}
				}
				
				LOGGER.info("insertCards.size() {}", insertCards.size());
				if (insertCards.size() > 0) {
					smgSaveyraCardsService.insertCards(insertCards);
				}
				LOGGER.info("updateCards.size() {}", insertCards.size());
				if (updateCards.size() > 0) {
					smgSaveyraCardsService.updateCards(updateCards);
				}
			}
			LOGGER.info("saveHomeScreenCardsData()==>POST /home/screen/cards call is end.");
		} catch (Exception e) {
			LOGGER.error("Outer try/catch block==>Exception: {}", e.getMessage(), e);
			return getFailureResponse(response, e.getMessage());
		}
		return getSuccessResponse(response, "" + operationAndCount.get(INSERT) + " card(s) are inserted/"
				+ operationAndCount.get(UPDATE) + " card(s) are updated successfully.");
	}

	private RestResponse getFailureResponse(RestResponse response, String statusMessage) {
		LOGGER.info("getFailureResponse()==>Entered");
		response.setStatus("FAILURE");
		response.setStatusMessage(statusMessage);
		LOGGER.info("getFailureResponse()==>Exit");
		return response;
	}

	private RestResponse getSuccessResponse(RestResponse response, String statusMessage) {
		LOGGER.info("getSuccessResponse()==>Entered");
		response.setStatus("SUCCESS");
		response.setStatusMessage(statusMessage);
		LOGGER.info("getSuccessResponse()==>Exit");
		return response;
	}

	private SmgSaveyraCardsEntity constructCardsData(SmgSaveyraCardsEntity smgSaveyraCardsEntity, Map<String, Object> o,
			SimpleDateFormat sdf) {
		try {
			LOGGER.info("constructCardsData() start}");
			if (o.get("createdOn") != null)
				smgSaveyraCardsEntity.setCreatedOn(sdf.parse(o.get("createdOn").toString()));
			if (o.get("expiry") != null)
				smgSaveyraCardsEntity.setExpiry(sdf.parse(o.get("expiry").toString()));
			if (o.get("imagePath") != null)
				smgSaveyraCardsEntity.setImagepath(o.get("imagePath").toString());
			if (o.get("lastmodifiedOn") != null)
				smgSaveyraCardsEntity.setLastmodifiedOn(sdf.parse(o.get("lastmodifiedOn").toString()));
			if (o.get("status") != null)
				smgSaveyraCardsEntity.setStatus(o.get("status").toString());
			if (o.get("textColor") != null)
				smgSaveyraCardsEntity.setTextColor(o.get("textColor").toString());
			if (o.get("textFontSize") != null)
				smgSaveyraCardsEntity.setTextFontSize(o.get("textFontSize").toString());
			if (o.get("backgroundColor") != null)
				smgSaveyraCardsEntity.setBackgroundColor(o.get("backgroundColor").toString());
			if (o.get("cardType") != null)
				smgSaveyraCardsEntity.setCardType(o.get("cardType").toString());
			if (o.get("navToScreen") != null)
				smgSaveyraCardsEntity.setNavToScreen(o.get("navToScreen").toString());
			if (o.get("title") != null)
				smgSaveyraCardsEntity.setTitle(o.get("title").toString());
			if (o.get("question") != null)
				smgSaveyraCardsEntity.setQuestion(o.get("question").toString());
			if (o.get("choices") != null)
				smgSaveyraCardsEntity.setChoices(o.get("choices").toString());
			if (o.get("percentage") != null)
				smgSaveyraCardsEntity.setPercentage(o.get("percentage").toString());
			if (o.get("description") != null)
				smgSaveyraCardsEntity.setDescription(o.get("description").toString());
			if (o.get("answer") != null)
				smgSaveyraCardsEntity.setAnswer(o.get("answer").toString());
			if (o.get("order") != null)
				smgSaveyraCardsEntity.setOrder(o.get("order").toString());
			if (o.get("keyword") != null)
				smgSaveyraCardsEntity.setKeyword(o.get("keyword").toString());
			if (o.get("imageExtension") != null)
				smgSaveyraCardsEntity.setImageExtension(o.get("imageExtension").toString());
			// for Merchandise stickers
			if (o.get("stickerPack") != null)
				smgSaveyraCardsEntity.setStickerPack(o.get("stickerPack").toString());
			if (o.get("displayText") != null)
				smgSaveyraCardsEntity.setDisplayText(o.get("displayText").toString());
			
			smgSaveyraCardsEntity.setInstanceId(o.get("instanceId").toString());
			smgSaveyraCardsEntity.setVersion("v2");
			LOGGER.info("constructCardsData() end");
			return smgSaveyraCardsEntity;
		} catch (ParseException e) {
			LOGGER.error("Error in parsing {}", e.getMessage(), e);
		}
		LOGGER.info("constructCardsData() end");
		return null;
	}
}
