package com.saveyra.json.processor.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "smg_saveyra_api_home_screen_promotions")
public class SmgSaveyraPromotionsEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "title")
	private String title;
	@Column(name = "imageId")
	private String imageId;
	@Column(name = "imageWidthRatio")
	private String imageWidthRatio;
	@Column(name = "imageHeightRatio")
	private String imageHeightRatio;
	@Column(name = "headline")
	private String headline;
	@Column(name = "headLineColor")
	private String headLineColor;
	@Column(name = "headLineFontSize")
	private String headLineFontSize;
	@Column(name = "text")
	private String text;
	@Column(name = "textColor")
	private String textColor;
	@Column(name = "textFontSize")
	private String textFontSize;
	@Column(name = "imagepath")
	private String imagepath;
	@Column(name = "status")
	private String status;
	@Column(name = "expiry")
	private Date expiry;
	@Column(name = "createdOn")
	private Date createdOn;
	@Column(name = "lastmodifiedOn")
	private Date lastmodifiedOn;
	@Column(name = "instanceId")
	private String instanceId;
	@Column(name = "keywords")
	private String keywords;
	@Column(name = "category")
	private String category;
	@Column(name = "subcategory")
	private String subcategory;
	@Column(name = "imageSizeFormat")
	private String imageSizeFormat;

	/**
	 * @return the keywords
	 */
	public String getKeywords() {
		return keywords;
	}

	/**
	 * @param keywords the keywords to set
	 */
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the subcategory
	 */
	public String getSubcategory() {
		return subcategory;
	}

	/**
	 * @param subcategory the subcategory to set
	 */
	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the imageWidthRatio
	 */
	public String getImageWidthRatio() {
		return imageWidthRatio;
	}

	/**
	 * @param imageWidthRatio the imageWidthRatio to set
	 */
	public void setImageWidthRatio(String imageWidthRatio) {
		this.imageWidthRatio = imageWidthRatio;
	}

	/**
	 * @return the imageHeightRatio
	 */
	public String getImageHeightRatio() {
		return imageHeightRatio;
	}

	/**
	 * @param imageHeightRatio the imageHeightRatio to set
	 */
	public void setImageHeightRatio(String imageHeightRatio) {
		this.imageHeightRatio = imageHeightRatio;
	}

	/**
	 * @return the headline
	 */
	public String getHeadline() {
		return headline;
	}

	/**
	 * @param headline the headline to set
	 */
	public void setHeadline(String headline) {
		this.headline = headline;
	}

	/**
	 * @return the headLineColor
	 */
	public String getHeadLineColor() {
		return headLineColor;
	}

	/**
	 * @param headLineColor the headLineColor to set
	 */
	public void setHeadLineColor(String headLineColor) {
		this.headLineColor = headLineColor;
	}

	/**
	 * @return the headLineFontSize
	 */
	public String getHeadLineFontSize() {
		return headLineFontSize;
	}

	/**
	 * @param headLineFontSize the headLineFontSize to set
	 */
	public void setHeadLineFontSize(String headLineFontSize) {
		this.headLineFontSize = headLineFontSize;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the textColor
	 */
	public String getTextColor() {
		return textColor;
	}

	/**
	 * @param textColor the textColor to set
	 */
	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}

	/**
	 * @return the textFontSize
	 */
	public String getTextFontSize() {
		return textFontSize;
	}

	/**
	 * @param textFontSize the textFontSize to set
	 */
	public void setTextFontSize(String textFontSize) {
		this.textFontSize = textFontSize;
	}

	/**
	 * @return the imagepath
	 */
	public String getImagepath() {
		return imagepath;
	}

	/**
	 * @param imagepath the imagepath to set
	 */
	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the expiry
	 */
	public Date getExpiry() {
		return expiry;
	}

	/**
	 * @param expiry the expiry to set
	 */
	public void setExpiry(Date expiry) {
		this.expiry = expiry;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the lastmodifiedOn
	 */
	public Date getLastmodifiedOn() {
		return lastmodifiedOn;
	}

	/**
	 * @param lastmodifiedOn the lastmodifiedOn to set
	 */
	public void setLastmodifiedOn(Date lastmodifiedOn) {
		this.lastmodifiedOn = lastmodifiedOn;
	}

	/**
	 * @return the instanceId
	 */
	public String getInstanceId() {
		return instanceId;
	}

	/**
	 * @param instanceId the instanceId to set
	 */
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	/**
	 * @return the imageId
	 */
	public String getImageId() {
		return imageId;
	}

	/**
	 * @param imageId the imageId to set
	 */
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	/**
	 * @return the imageSizeFormat
	 */
	public String getImageSizeFormat() {
		return imageSizeFormat;
	}

	/**
	 * @param imageSizeFormat the imageSizeFormat to set
	 */
	public void setImageSizeFormat(String imageSizeFormat) {
		this.imageSizeFormat = imageSizeFormat;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SmgSaveyraPromotionsEntity [id=" + id + ", title=" + title + ", imageId=" + imageId
				+ ", imageWidthRatio=" + imageWidthRatio + ", imageHeightRatio=" + imageHeightRatio + ", headline="
				+ headline + ", headLineColor=" + headLineColor + ", headLineFontSize=" + headLineFontSize + ", text="
				+ text + ", textColor=" + textColor + ", textFontSize=" + textFontSize + ", imagepath=" + imagepath
				+ ", status=" + status + ", expiry=" + expiry + ", createdOn=" + createdOn + ", lastmodifiedOn="
				+ lastmodifiedOn + ", instanceId=" + instanceId + ", keywords=" + keywords + ", category=" + category
				+ ", subcategory=" + subcategory + ", imageSizeFormat=" + imageSizeFormat + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((expiry == null) ? 0 : expiry.hashCode());
		result = prime * result + ((headLineColor == null) ? 0 : headLineColor.hashCode());
		result = prime * result + ((headLineFontSize == null) ? 0 : headLineFontSize.hashCode());
		result = prime * result + ((headline == null) ? 0 : headline.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((imageHeightRatio == null) ? 0 : imageHeightRatio.hashCode());
		result = prime * result + ((imageId == null) ? 0 : imageId.hashCode());
		result = prime * result + ((imageSizeFormat == null) ? 0 : imageSizeFormat.hashCode());
		result = prime * result + ((imageWidthRatio == null) ? 0 : imageWidthRatio.hashCode());
		result = prime * result + ((imagepath == null) ? 0 : imagepath.hashCode());
		result = prime * result + ((instanceId == null) ? 0 : instanceId.hashCode());
		result = prime * result + ((keywords == null) ? 0 : keywords.hashCode());
		result = prime * result + ((lastmodifiedOn == null) ? 0 : lastmodifiedOn.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((subcategory == null) ? 0 : subcategory.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		result = prime * result + ((textColor == null) ? 0 : textColor.hashCode());
		result = prime * result + ((textFontSize == null) ? 0 : textFontSize.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SmgSaveyraPromotionsEntity other = (SmgSaveyraPromotionsEntity) obj;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (expiry == null) {
			if (other.expiry != null)
				return false;
		} else if (!expiry.equals(other.expiry))
			return false;
		if (headLineColor == null) {
			if (other.headLineColor != null)
				return false;
		} else if (!headLineColor.equals(other.headLineColor))
			return false;
		if (headLineFontSize == null) {
			if (other.headLineFontSize != null)
				return false;
		} else if (!headLineFontSize.equals(other.headLineFontSize))
			return false;
		if (headline == null) {
			if (other.headline != null)
				return false;
		} else if (!headline.equals(other.headline))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (imageHeightRatio == null) {
			if (other.imageHeightRatio != null)
				return false;
		} else if (!imageHeightRatio.equals(other.imageHeightRatio))
			return false;
		if (imageId == null) {
			if (other.imageId != null)
				return false;
		} else if (!imageId.equals(other.imageId))
			return false;
		if (imageSizeFormat == null) {
			if (other.imageSizeFormat != null)
				return false;
		} else if (!imageSizeFormat.equals(other.imageSizeFormat))
			return false;
		if (imageWidthRatio == null) {
			if (other.imageWidthRatio != null)
				return false;
		} else if (!imageWidthRatio.equals(other.imageWidthRatio))
			return false;
		if (imagepath == null) {
			if (other.imagepath != null)
				return false;
		} else if (!imagepath.equals(other.imagepath))
			return false;
		if (instanceId == null) {
			if (other.instanceId != null)
				return false;
		} else if (!instanceId.equals(other.instanceId))
			return false;
		if (keywords == null) {
			if (other.keywords != null)
				return false;
		} else if (!keywords.equals(other.keywords))
			return false;
		if (lastmodifiedOn == null) {
			if (other.lastmodifiedOn != null)
				return false;
		} else if (!lastmodifiedOn.equals(other.lastmodifiedOn))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (subcategory == null) {
			if (other.subcategory != null)
				return false;
		} else if (!subcategory.equals(other.subcategory))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		if (textColor == null) {
			if (other.textColor != null)
				return false;
		} else if (!textColor.equals(other.textColor))
			return false;
		if (textFontSize == null) {
			if (other.textFontSize != null)
				return false;
		} else if (!textFontSize.equals(other.textFontSize))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
}
