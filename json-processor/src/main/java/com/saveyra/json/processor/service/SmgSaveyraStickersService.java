package com.saveyra.json.processor.service;

import com.saveyra.json.processor.model.SaveyraImagesCollection;
import com.saveyra.json.processor.model.SmgSaveyraStickersEntity;

public interface SmgSaveyraStickersService {
	SmgSaveyraStickersEntity findByStrickersImageId(String imageId);
	void persist(SmgSaveyraStickersEntity smgSaveyraStickersEntity);
	void update(SmgSaveyraStickersEntity smgSaveyraStickersEntity);
	//mongo db queries
	SaveyraImagesCollection findByImageId(String imageId);
	void save(SaveyraImagesCollection saveyraImagesCollection);
	void update(SaveyraImagesCollection saveyraImagesCollection);
}
