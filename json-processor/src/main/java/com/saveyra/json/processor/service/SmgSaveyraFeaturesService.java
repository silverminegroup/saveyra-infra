package com.saveyra.json.processor.service;

import java.util.List;

import com.saveyra.json.processor.model.SmgSaveyraFeaturesEntity;

public interface SmgSaveyraFeaturesService {
	SmgSaveyraFeaturesEntity findByFeaturesInstanceId(String instanceId);
	void persist(SmgSaveyraFeaturesEntity smgSaveyraFeaturesEntity);
	void update(SmgSaveyraFeaturesEntity smgSaveyraFeaturesEntity);
	List<SmgSaveyraFeaturesEntity> findByAll();
	int updateStatus(String status);
}
