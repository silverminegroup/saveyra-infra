package com.saveyra.json.processor.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "smg_saveyra_stickers_metadata")
public class SmgSaveyraStickersEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "imageId")
	private String imageId;
	@Column(name = "tokens")
	private String tokens;
	@Column(name = "imagePath")
	private String imagePath;
	@Column(name = "imageSizeFormat")
	private String imageSizeFormat;
	@Column(name = "imageExtension")
	private String imageExtension;
	@Column(name = "createdon")
	private Date createdon;
	@Column(name = "status")
	private String status;
	@Column(name = "productId")
	private String productId;
	@Column(name = "expiry")
	private Date expiry;
	@Column(name = "imageType")
	private String imageType;
	@Column(name = "paidStickers")
	private Integer paidStickers;
	@Column(name = "lastModified")
	private Date lastModified;
	@Column(name = "caption")
	private String caption;
	@Column(name = "occasionActiveStartDate")
	private Date occasionActiveStartDate;
	@Column(name = "occasionDate")
	private Date occasionDate;
	@Column(name = "price")
	private String price;
	@Column(name = "category")
	private String category;
	@Column(name = "keywords")
	private String keywords;
	@Column(name = "subcategory")
	private String subcategory;
	@Column(name = "imageName")
	private String imageName;
	@Column(name = "imageDescription")
	private String imageDescription;
	@Column(name = "imageFor")
	private String imageFor;
	@Column(name = "priorityFor")
	private String priorityFor;
	@Column(name = "orderBy")
	private String order;
	@Column(name="age_group")
	private String age;
	@Column(name="sticker_gender")
	private String gender;
	@Column(name="purchaseURL")
	private String purchaseURL;
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the imageId
	 */
	public String getImageId() {
		return imageId;
	}
	/**
	 * @param imageId the imageId to set
	 */
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	/**
	 * @return the tokens
	 */
	public String getTokens() {
		return tokens;
	}
	/**
	 * @param tokens the tokens to set
	 */
	public void setTokens(String tokens) {
		this.tokens = tokens;
	}
	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}
	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	/**
	 * @return the imageSizeFormat
	 */
	public String getImageSizeFormat() {
		return imageSizeFormat;
	}
	/**
	 * @param imageSizeFormat the imageSizeFormat to set
	 */
	public void setImageSizeFormat(String imageSizeFormat) {
		this.imageSizeFormat = imageSizeFormat;
	}
	/**
	 * @return the imageExtension
	 */
	public String getImageExtension() {
		return imageExtension;
	}
	/**
	 * @param imageExtension the imageExtension to set
	 */
	public void setImageExtension(String imageExtension) {
		this.imageExtension = imageExtension;
	}
	/**
	 * @return the createdon
	 */
	public Date getCreatedon() {
		return createdon;
	}
	/**
	 * @param createdon the createdon to set
	 */
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	/**
	 * @return the expiry
	 */
	public Date getExpiry() {
		return expiry;
	}
	/**
	 * @param expiry the expiry to set
	 */
	public void setExpiry(Date expiry) {
		this.expiry = expiry;
	}
	/**
	 * @return the imageType
	 */
	public String getImageType() {
		return imageType;
	}
	/**
	 * @param imageType the imageType to set
	 */
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}
	/**
	 * @return the paidStickers
	 */
	public Integer getPaidStickers() {
		return paidStickers;
	}
	/**
	 * @param paidStickers the paidStickers to set
	 */
	public void setPaidStickers(Integer paidStickers) {
		this.paidStickers = paidStickers;
	}
	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}
	/**
	 * @param lastModified the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}
	/**
	 * @return the caption
	 */
	public String getCaption() {
		return caption;
	}
	/**
	 * @param caption the caption to set
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}
	/**
	 * @return the occasionActiveStartDate
	 */
	public Date getOccasionActiveStartDate() {
		return occasionActiveStartDate;
	}
	/**
	 * @param occasionActiveStartDate the occasionActiveStartDate to set
	 */
	public void setOccasionActiveStartDate(Date occasionActiveStartDate) {
		this.occasionActiveStartDate = occasionActiveStartDate;
	}
	/**
	 * @return the occasionDate
	 */
	public Date getOccasionDate() {
		return occasionDate;
	}
	/**
	 * @param occasionDate the occasionDate to set
	 */
	public void setOccasionDate(Date occasionDate) {
		this.occasionDate = occasionDate;
	}
	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}
	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * @return the keywords
	 */
	public String getKeywords() {
		return keywords;
	}
	/**
	 * @param keywords the keywords to set
	 */
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	/**
	 * @return the subcategory
	 */
	public String getSubcategory() {
		return subcategory;
	}
	/**
	 * @param subcategory the subcategory to set
	 */
	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}
	/**
	 * @return the imageName
	 */
	public String getImageName() {
		return imageName;
	}
	/**
	 * @param imageName the imageName to set
	 */
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	/**
	 * @return the imageDescription
	 */
	public String getImageDescription() {
		return imageDescription;
	}
	/**
	 * @param imageDescription the imageDescription to set
	 */
	public void setImageDescription(String imageDescription) {
		this.imageDescription = imageDescription;
	}
	/**
	 * @return the imageFor
	 */
	public String getImageFor() {
		return imageFor;
	}
	/**
	 * @param imageFor the imageFor to set
	 */
	public void setImageFor(String imageFor) {
		this.imageFor = imageFor;
	}
	/**
	 * @return the priorityFor
	 */
	public String getPriorityFor() {
		return priorityFor;
	}
	/**
	 * @param priorityFor the priorityFor to set
	 */
	public void setPriorityFor(String priorityFor) {
		this.priorityFor = priorityFor;
	}
	/**
	 * @return the order
	 */
	public String getOrder() {
		return order;
	}
	/**
	 * @param order the order to set
	 */
	public void setOrder(String order) {
		this.order = order;
	}
	/**
	 * @return the age
	 */
	public String getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(String age) {
		this.age = age;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the purchaseURL
	 */
	public String getPurchaseURL() {
		return purchaseURL;
	}
	/**
	 * @param purchaseURL the purchaseURL to set
	 */
	public void setPurchaseURL(String purchaseURL) {
		this.purchaseURL = purchaseURL;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SmgSaveyraStickersEntity [id=" + id + ", imageId=" + imageId + ", tokens=" + tokens + ", imagePath="
				+ imagePath + ", imageSizeFormat=" + imageSizeFormat + ", imageExtension=" + imageExtension
				+ ", createdon=" + createdon + ", status=" + status + ", productId=" + productId + ", expiry=" + expiry
				+ ", imageType=" + imageType + ", paidStickers=" + paidStickers + ", lastModified=" + lastModified
				+ ", caption=" + caption + ", occasionActiveStartDate=" + occasionActiveStartDate + ", occasionDate="
				+ occasionDate + ", price=" + price + ", category=" + category + ", keywords=" + keywords
				+ ", subcategory=" + subcategory + ", imageName=" + imageName + ", imageDescription=" + imageDescription
				+ ", imageFor=" + imageFor + ", priorityFor=" + priorityFor + ", order=" + order + ", age=" + age
				+ ", gender=" + gender + ", purchaseURL=" + purchaseURL + "]";
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((age == null) ? 0 : age.hashCode());
		result = prime * result + ((caption == null) ? 0 : caption.hashCode());
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((createdon == null) ? 0 : createdon.hashCode());
		result = prime * result + ((expiry == null) ? 0 : expiry.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((imageDescription == null) ? 0 : imageDescription.hashCode());
		result = prime * result + ((imageExtension == null) ? 0 : imageExtension.hashCode());
		result = prime * result + ((imageFor == null) ? 0 : imageFor.hashCode());
		result = prime * result + ((imageId == null) ? 0 : imageId.hashCode());
		result = prime * result + ((imageName == null) ? 0 : imageName.hashCode());
		result = prime * result + ((imagePath == null) ? 0 : imagePath.hashCode());
		result = prime * result + ((imageSizeFormat == null) ? 0 : imageSizeFormat.hashCode());
		result = prime * result + ((imageType == null) ? 0 : imageType.hashCode());
		result = prime * result + ((keywords == null) ? 0 : keywords.hashCode());
		result = prime * result + ((lastModified == null) ? 0 : lastModified.hashCode());
		result = prime * result + ((occasionActiveStartDate == null) ? 0 : occasionActiveStartDate.hashCode());
		result = prime * result + ((occasionDate == null) ? 0 : occasionDate.hashCode());
		result = prime * result + ((order == null) ? 0 : order.hashCode());
		result = prime * result + ((paidStickers == null) ? 0 : paidStickers.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((priorityFor == null) ? 0 : priorityFor.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		result = prime * result + ((purchaseURL == null) ? 0 : purchaseURL.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((subcategory == null) ? 0 : subcategory.hashCode());
		result = prime * result + ((tokens == null) ? 0 : tokens.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SmgSaveyraStickersEntity other = (SmgSaveyraStickersEntity) obj;
		if (age == null) {
			if (other.age != null)
				return false;
		} else if (!age.equals(other.age))
			return false;
		if (caption == null) {
			if (other.caption != null)
				return false;
		} else if (!caption.equals(other.caption))
			return false;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (createdon == null) {
			if (other.createdon != null)
				return false;
		} else if (!createdon.equals(other.createdon))
			return false;
		if (expiry == null) {
			if (other.expiry != null)
				return false;
		} else if (!expiry.equals(other.expiry))
			return false;
		if (gender == null) {
			if (other.gender != null)
				return false;
		} else if (!gender.equals(other.gender))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (imageDescription == null) {
			if (other.imageDescription != null)
				return false;
		} else if (!imageDescription.equals(other.imageDescription))
			return false;
		if (imageExtension == null) {
			if (other.imageExtension != null)
				return false;
		} else if (!imageExtension.equals(other.imageExtension))
			return false;
		if (imageFor == null) {
			if (other.imageFor != null)
				return false;
		} else if (!imageFor.equals(other.imageFor))
			return false;
		if (imageId == null) {
			if (other.imageId != null)
				return false;
		} else if (!imageId.equals(other.imageId))
			return false;
		if (imageName == null) {
			if (other.imageName != null)
				return false;
		} else if (!imageName.equals(other.imageName))
			return false;
		if (imagePath == null) {
			if (other.imagePath != null)
				return false;
		} else if (!imagePath.equals(other.imagePath))
			return false;
		if (imageSizeFormat == null) {
			if (other.imageSizeFormat != null)
				return false;
		} else if (!imageSizeFormat.equals(other.imageSizeFormat))
			return false;
		if (imageType == null) {
			if (other.imageType != null)
				return false;
		} else if (!imageType.equals(other.imageType))
			return false;
		if (keywords == null) {
			if (other.keywords != null)
				return false;
		} else if (!keywords.equals(other.keywords))
			return false;
		if (lastModified == null) {
			if (other.lastModified != null)
				return false;
		} else if (!lastModified.equals(other.lastModified))
			return false;
		if (occasionActiveStartDate == null) {
			if (other.occasionActiveStartDate != null)
				return false;
		} else if (!occasionActiveStartDate.equals(other.occasionActiveStartDate))
			return false;
		if (occasionDate == null) {
			if (other.occasionDate != null)
				return false;
		} else if (!occasionDate.equals(other.occasionDate))
			return false;
		if (order == null) {
			if (other.order != null)
				return false;
		} else if (!order.equals(other.order))
			return false;
		if (paidStickers == null) {
			if (other.paidStickers != null)
				return false;
		} else if (!paidStickers.equals(other.paidStickers))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (priorityFor == null) {
			if (other.priorityFor != null)
				return false;
		} else if (!priorityFor.equals(other.priorityFor))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		if (purchaseURL == null) {
			if (other.purchaseURL != null)
				return false;
		} else if (!purchaseURL.equals(other.purchaseURL))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (subcategory == null) {
			if (other.subcategory != null)
				return false;
		} else if (!subcategory.equals(other.subcategory))
			return false;
		if (tokens == null) {
			if (other.tokens != null)
				return false;
		} else if (!tokens.equals(other.tokens))
			return false;
		return true;
	}
}
