package com.saveyra.json.processor.repository;

import org.json.simple.JSONObject;

public interface MongoDao {
	void updateMongo(String imageID, JSONObject jf);
	public String resolveKeywords(JSONObject jf);
	public String resolveSubCategory(JSONObject jf);
	public String resolveCategory(JSONObject jf);
	void insertMongo(String imageID, JSONObject jf);
	int findMongo(String imageID);
}
