package com.saveyra.json.processor.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saveyra.json.processor.model.SmgSaveyraFeaturesEntity;
import com.saveyra.json.processor.repository.SmgSaveyraFeaturesRepository;
import com.saveyra.json.processor.service.SmgSaveyraFeaturesService;

@Service
public class SmgSaveyraFeaturesServiceImpl implements SmgSaveyraFeaturesService {
	@Autowired
	private SmgSaveyraFeaturesRepository smgSaveyraFeaturesRepository;

	@Transactional
	@Override
	public SmgSaveyraFeaturesEntity findByFeaturesInstanceId(String instanceId) {
		return smgSaveyraFeaturesRepository.findByInstanceId(instanceId);
	}

	@Transactional(readOnly = false)
	@Override
	public void persist(SmgSaveyraFeaturesEntity smgSaveyraPromotionsEntity) {
		smgSaveyraFeaturesRepository.save(smgSaveyraPromotionsEntity);
	}

	@Transactional(readOnly = false)
	@Override
	public void update(SmgSaveyraFeaturesEntity smgSaveyraPromotionsEntity) {
		smgSaveyraFeaturesRepository.save(smgSaveyraPromotionsEntity);
	}

	@Override
	public List<SmgSaveyraFeaturesEntity> findByAll() {
		List<SmgSaveyraFeaturesEntity> list = new ArrayList<>();
		Iterable<SmgSaveyraFeaturesEntity> records = smgSaveyraFeaturesRepository.findAll();
		if(records != null )
			records.iterator().forEachRemaining(list::add);
		return list;
	}

	@Transactional(readOnly = false)
	@Override
	public int updateStatus(String status) {
		return smgSaveyraFeaturesRepository.updateStatus(status);
	}

}
