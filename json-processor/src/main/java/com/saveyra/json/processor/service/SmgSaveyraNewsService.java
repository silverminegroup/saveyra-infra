package com.saveyra.json.processor.service;

import java.util.List;

import com.saveyra.json.processor.model.SmgSaveyraNewsEntity;

public interface SmgSaveyraNewsService {
	SmgSaveyraNewsEntity findByNewsInstanceId(String instanceId);
	void persist(SmgSaveyraNewsEntity smgSaveyraNewsEntity);
	void update(SmgSaveyraNewsEntity smgSaveyraNewsEntity);
	List<SmgSaveyraNewsEntity> findByAll();
	int updateStatus(String status);
}
