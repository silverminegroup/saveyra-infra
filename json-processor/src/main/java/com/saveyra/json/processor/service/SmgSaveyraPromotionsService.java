package com.saveyra.json.processor.service;

import java.util.List;

import com.saveyra.json.processor.model.SmgSaveyraPromotionsEntity;

public interface SmgSaveyraPromotionsService {
	SmgSaveyraPromotionsEntity findByPromotionsInstanceId(String instanceId);
	void persist(SmgSaveyraPromotionsEntity smgSaveyraPromotionsEntity);
	void update(SmgSaveyraPromotionsEntity smgSaveyraPromotionsEntity);
	List<SmgSaveyraPromotionsEntity> findByAll();
	int updateStatus(String status);
}
