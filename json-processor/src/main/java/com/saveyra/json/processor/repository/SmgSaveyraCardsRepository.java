package com.saveyra.json.processor.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.saveyra.json.processor.model.SmgSaveyraCardsEntity;

@Repository
public interface SmgSaveyraCardsRepository extends CrudRepository<SmgSaveyraCardsEntity, Integer> {
	SmgSaveyraCardsEntity findByInstanceId(String instanceId);
	@Modifying(clearAutomatically = true)
	@Query(value="UPDATE smg_saveyra_api_home_screen_cards cards SET cards.status = :status WHERE cards.status = 'enabled'", nativeQuery = true)
	int updateStatus(@Param("status") String status);
}
