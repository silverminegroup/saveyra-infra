package com.saveyra.json.processor.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimezoneConverter {
	private static final Logger LOGGER = LoggerFactory.getLogger(TimezoneConverter.class);

	public static Date convertLocalToUTC(Date date) {
		TimeZone utc = TimeZone.getTimeZone("UTC");
		SimpleDateFormat sourceDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sourceDF.setTimeZone(utc);
		String input = sourceDF.format(date);
		LOGGER.info("Converted date object to string with utc {}", input);

		SimpleDateFormat destDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeZone(utc);
		try {
			cal.setTime(destDF.parse(input));
			LOGGER.info("Converted string to date object with utc");
		} catch (ParseException e) {
			e.printStackTrace();
			LOGGER.error("Error in date parsing {}", e);
		}
		return cal.getTime();
	}
}
