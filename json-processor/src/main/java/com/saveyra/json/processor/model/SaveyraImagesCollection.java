package com.saveyra.json.processor.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "saveyraImages")
public class SaveyraImagesCollection {
	@Id
	private String _id;
	private String artistName;
	private String avatarModule;
	private String category;
	private String createdDateTimeStamp;
	private String description;
	private String imageType;
	private String imageSizeFormat;
	private String imageID;
	private String imageName;
	private String imagePath;
	private String keywords = "Both";
	private String platform;
	private String subCategory;
	
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	public String getAvatarModule() {
		return avatarModule;
	}
	public void setAvatarModule(String avatarModule) {
		this.avatarModule = avatarModule;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCreatedDateTimeStamp() {
		return createdDateTimeStamp;
	}
	public void setCreatedDateTimeStamp(String createdDateTimeStamp) {
		this.createdDateTimeStamp = createdDateTimeStamp;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImageType() {
		return imageType;
	}
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}
	public String getImageSizeFormat() {
		return imageSizeFormat;
	}
	public void setImageSizeFormat(String imageSizeFormat) {
		this.imageSizeFormat = imageSizeFormat;
	}
	public String getImageID() {
		return imageID;
	}
	public void setImageID(String imageID) {
		this.imageID = imageID;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getSubCategory() {
		return subCategory;
	}
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
}
