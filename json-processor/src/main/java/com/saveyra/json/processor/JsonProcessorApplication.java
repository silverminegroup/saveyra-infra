package com.saveyra.json.processor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;



@SpringBootApplication  
@EnableAutoConfiguration
@ComponentScan("com.saveyra.json.processor")
@PropertySource(value="classpath:jsonprocessor.properties")
public class JsonProcessorApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(JsonProcessorApplication.class, args);
	}
	
	@Override  
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(JsonProcessorApplication.class);     
	}
}  
          