package com.saveyra.json.processor.repository;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.saveyra.json.processor.model.SaveyraImagesCollection;

@Repository("MongoDao")
public class MongoDaoImpl implements MongoDao {

	private static final Logger logger = LoggerFactory.getLogger(MongoDaoImpl.class);
	private final MongoTemplate mongoTemplate;

	@Autowired
	public MongoDaoImpl(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}
	
	@Override
	public int findMongo(String imageID) {
		SaveyraImagesCollection mr = mongoTemplate.findOne(
		        Query.query(Criteria.where("imageID").is(imageID)),
		        SaveyraImagesCollection.class,
		        "saveyraImages"
		    );		
		if(mr==null) {
			return 0;
		}
		
		return 1;
	}
	
	
	@Override
	public void insertMongo(String imageID, JSONObject jf) {		
		logger.info("[ENTER] saveMongo");
		if(imageID == null || jf == null) {
			logger.error("[ERROR] Error in Json");
			return;
		}		
		SaveyraImagesCollection mr = new SaveyraImagesCollection();
	
		mr.setArtistName(jf.get("artistName").toString().trim());
		mr.setAvatarModule(null);
		mr.setCategory(resolveCategory(jf).trim());
		mr.setCreatedDateTimeStamp(jf.get("createdon").toString().trim());
		mr.setDescription(jf.get("description").toString().trim());
		mr.setImageID(jf.get("imageId").toString().trim());
		mr.setImageName(jf.get("imageName").toString().trim());
		mr.setImagePath(jf.get("imagePath").toString().trim());
		mr.setImageSizeFormat(jf.get("imageSizeFormat").toString().trim());
		mr.setImageType(jf.get("imageType").toString().trim());
		mr.setKeywords(resolveKeywords(jf).trim());
		mr.setPlatform("Both");
		mr.setSubCategory(resolveSubCategory(jf).trim());
		mongoTemplate.save(mr);
		logger.info("[LEAVE] saveMongo");
	}

	@Override
	public void updateMongo(String imageID, JSONObject jf) {		
		logger.info("[ENTER] updateMongo");
		if(imageID == null || jf == null) {
			logger.error("[ERROR] Error in Json");
			return;
		}		
		Query query = new Query();
		query.addCriteria(Criteria.where("imageID").is(imageID.trim()));
		SaveyraImagesCollection mr = mongoTemplate.findOne(query, SaveyraImagesCollection.class);
		mr.setArtistName(jf.get("artistName").toString().trim());
		mr.setAvatarModule(null);
		mr.setCategory(resolveCategory(jf).trim());
		mr.setCreatedDateTimeStamp(jf.get("createdon").toString().trim());
		mr.setDescription(jf.get("description").toString().trim());
		mr.setImageID(jf.get("imageId").toString().trim());
		mr.setImageName(jf.get("imageName").toString().trim());
		mr.setImagePath(jf.get("imagePath").toString().trim());
		mr.setImageSizeFormat(jf.get("imageSizeFormat").toString().trim());
		mr.setImageType(jf.get("imageType").toString().trim());
		mr.setKeywords(resolveKeywords(jf).trim());
		mr.setPlatform("Both");
		mr.setSubCategory(resolveSubCategory(jf).trim());
		mongoTemplate.save(mr);
		logger.info("[LEAVE] updateMongo");
	}

	public String resolveCategory(JSONObject jf) {
		if (jf == null || jf.get("category") == null) {
			return "";
		}
		return jf.get("category").toString();
	}

	public String resolveKeywords(JSONObject jf) {
		if (jf == null || jf.get("keywords") == null) {
			return "";
		}
		return jf.get("keywords").toString();
	}

	public String resolveSubCategory(JSONObject jf) {
		if (jf == null || jf.get("subCategory") == null) {
			return "";
		}
		return jf.get("subCategory").toString();
	}
}
