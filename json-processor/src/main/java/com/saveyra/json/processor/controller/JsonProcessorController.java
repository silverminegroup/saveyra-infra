package com.saveyra.json.processor.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.saveyra.json.processor.data.RequestModelJson;
import com.saveyra.json.processor.data.RestResponse;
import com.saveyra.json.processor.model.SaveyraImagesCollection;
import com.saveyra.json.processor.model.SmgSaveyraCardsEntity;
import com.saveyra.json.processor.model.SmgSaveyraFeaturesEntity;
import com.saveyra.json.processor.model.SmgSaveyraNewsEntity;
import com.saveyra.json.processor.model.SmgSaveyraPromotionsEntity;
import com.saveyra.json.processor.model.SmgSaveyraStickersEntity;
import com.saveyra.json.processor.service.SmgSaveyraCardsService;
import com.saveyra.json.processor.service.SmgSaveyraFeaturesService;
import com.saveyra.json.processor.service.SmgSaveyraNewsService;
import com.saveyra.json.processor.service.SmgSaveyraPromotionsService;
import com.saveyra.json.processor.service.SmgSaveyraStickersService;
import com.saveyra.json.processor.util.TimezoneConverter;

@RestController
@RequestMapping("/")
@CrossOrigin
public class JsonProcessorController {
	private static final Logger LOGGER = LoggerFactory.getLogger(JsonProcessorController.class);
	@Autowired
	private SmgSaveyraNewsService smgSaveyraNewsService;
	
	@Autowired
	private SmgSaveyraCardsService smgSaveyraCardsService;
	
	@Autowired
	private SmgSaveyraPromotionsService smgSaveyraPromotionsService;
	
	@Autowired
	private SmgSaveyraStickersService smgSaveyraStickersService;
	
	@Autowired
	private SmgSaveyraFeaturesService smgSaveyraFeaturesService;
	
	@Value("${re.url}")
	private String reUrl;

	
	//Valid Categories
	public enum Categories {
		NEWS("News"), CARDS("Cards"), PROMOTIONS("Promotions"), STICKERS("Stickers"), IMAGES("Images"), FEATURES("Features");
		//NEWS("News"), CARDS("Cards"), PROMOTIONS("Promotions"), FEATURES("Features");
		private String name;

		public String getName() {
			return this.name;
		}

		Categories(String name) {
			this.name = name;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@CrossOrigin
	@PostMapping("/createCategoryJsonData")
	public RestResponse saveCotegroyJson(@Valid @RequestBody List<RequestModelJson> requestData) {
		LOGGER.info("saveCotegroyJson()==>POST /createCategoryJsonData call is initiated.");
		RestResponse response = new RestResponse();
		Map<String, Integer> trackInsertCount = new HashMap<>();
		trackInsertCount.put(Categories.NEWS.getName(), 0);
		trackInsertCount.put(Categories.CARDS.getName(), 0);
		trackInsertCount.put(Categories.PROMOTIONS.getName(), 0);
		trackInsertCount.put(Categories.STICKERS.getName(), 0);
		trackInsertCount.put(Categories.IMAGES.getName(), 0);
		trackInsertCount.put(Categories.FEATURES.getName(), 0);
		
		Map<String, Integer> trackUpdateCount = new HashMap<>();
		trackUpdateCount.put(Categories.NEWS.getName(), 0);
		trackUpdateCount.put(Categories.CARDS.getName(), 0);
		trackUpdateCount.put(Categories.PROMOTIONS.getName(), 0);
		trackUpdateCount.put(Categories.STICKERS.getName(), 0);
		trackUpdateCount.put(Categories.IMAGES.getName(), 0);
		trackUpdateCount.put(Categories.FEATURES.getName(), 0);
		
		Set<String> categories = new HashSet<String>();
		for(Categories ctgry: Categories.values()) {
			categories.add(ctgry.getName().toLowerCase());
		}
		
		Set<String> invalidCategories = new HashSet<String>();
		Set<String> validCategories = new HashSet<String>();
		List<String> errors = new ArrayList<String>();
		try {
			JSONParser parser = new JSONParser();
			String latestLastModifiedDate = null;
			for(RequestModelJson request: requestData) {
				Map<String, String> errorMsgs = new HashMap<String, String>();
				String category = request.getCategoryType();
				//validate category
				if(!categories.contains(category)) {
					LOGGER.error("Invalid Category {}. It should be either 'News' or 'Cards' or 'Promotions' or 'Stickers'  or 'Features'.", category);
					//LOGGER.error("Invalid Category {}. It should be either 'News' or 'Cards' or 'Promotions' or 'Features'.", category);
					if(category == null || category.isEmpty()) {
						invalidCategories.add("empty(\"\")");
					} else {
						invalidCategories.add(category);
					}
					errorMsgs.put("invalid.category", "Invalid Category '"+invalidCategories.toString()+"'. It should be either 'News' or 'Cards' or 'Promotions' or 'Stickers' or 'Features'");
					//errorMsgs.put("invalid.category", "Invalid Category '"+invalidCategories.toString()+"'. It should be either 'News' or 'Cards' or 'Promotions' or 'Features'");
				}
				
				if(request.getJsonData() == null || request.getJsonData().size() == 0) {
					validCategories.add(category);
					LOGGER.error("No data in the request for the category {} ", category);
					errorMsgs.put("invalid.json.data.empty","No data in the request for categories "+validCategories.toString());
				}
				errors.addAll(errorMsgs.values());
				if(errorMsgs.size() > 0) {
					continue;
				}
				//Parse json
				Object parsedObj  = parser.parse(String.valueOf(request.getJsonData()));
				JSONArray jsonArray = (JSONArray) parsedObj;
				LOGGER.info("Category: {}, JSON data size: {}", category, jsonArray.size());
				SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
				for(Object obj: jsonArray.toArray()) {
					JSONObject o = (JSONObject)obj;
					if(Categories.NEWS.getName().equalsIgnoreCase(category)) {
						SmgSaveyraNewsEntity smgSaveyraNewsEntity = this.smgSaveyraNewsService.findByNewsInstanceId( o.get("instanceId").toString());
						if (smgSaveyraNewsEntity != null) {
							LOGGER.info("Updating news record..");
							this.smgSaveyraNewsService.update(constructNewsData(smgSaveyraNewsEntity, o, sdf));
							trackUpdateCount.put(Categories.NEWS.getName(), trackUpdateCount.get(Categories.NEWS.getName())+1);
							LOGGER.info("Updated {} news record successfully", trackUpdateCount.get(Categories.NEWS.getName()));
						} else {
							LOGGER.info("Inserting news record..");
							//Disable all previous news
							int disabled = this.smgSaveyraNewsService.updateStatus("disabled");
							LOGGER.info("Disabled previous news records {}", disabled);
							smgSaveyraNewsEntity = new SmgSaveyraNewsEntity();
							this.smgSaveyraNewsService.persist(constructNewsData(smgSaveyraNewsEntity, o, sdf));
							trackInsertCount.put(Categories.NEWS.getName(), trackInsertCount.get(Categories.NEWS.getName())+1);
							LOGGER.info("Inserted {} news record successfully.", trackInsertCount.get(Categories.NEWS.getName()));
						}
					} 
					
					if(Categories.CARDS.getName().equalsIgnoreCase(category)) {
						SmgSaveyraCardsEntity smgSaveyraCardsEntity = this.smgSaveyraCardsService.findByCardsInstanceId( o.get("instanceId").toString());
						if (smgSaveyraCardsEntity != null) {
							LOGGER.info("Updating cards record..");
							this.smgSaveyraCardsService.update(constructCardsData(smgSaveyraCardsEntity, o, sdf));
							trackUpdateCount.put(Categories.CARDS.getName(), trackUpdateCount.get(Categories.CARDS.getName())+1);
							LOGGER.info("Updated {} cards record successfully.", trackUpdateCount.get(Categories.CARDS.getName()));
						} else {
							LOGGER.info("Inserting cards record..");
							//Commented based on - SAV-1271
							//Disable all previous cards
							//int disabled = this.smgSaveyraCardsService.updateStatus("disabled");
							//LOGGER.info("Disabled previous cards records {}", disabled);
							smgSaveyraCardsEntity = new SmgSaveyraCardsEntity();
							this.smgSaveyraCardsService.persist(constructCardsData(smgSaveyraCardsEntity, o, sdf));
							trackInsertCount.put(Categories.CARDS.getName(), trackInsertCount.get(Categories.CARDS.getName())+1);
							LOGGER.info("Inserted {} cards record successfully.", trackInsertCount.get(Categories.CARDS.getName()));
						}
					} 
					
					if(Categories.PROMOTIONS.getName().equalsIgnoreCase(category)) {
						SmgSaveyraPromotionsEntity smgSaveyraPromotionsEntity = this.smgSaveyraPromotionsService.findByPromotionsInstanceId( o.get("instanceId").toString());
						if (smgSaveyraPromotionsEntity != null) {
							LOGGER.info("Updating promotions record..");
							this.smgSaveyraPromotionsService.update(constructPromotionsData(smgSaveyraPromotionsEntity, o, sdf));
							trackUpdateCount.put(Categories.PROMOTIONS.getName(), trackUpdateCount.get(Categories.PROMOTIONS.getName())+1);
							LOGGER.info("Updated {} promotions record successfully.", trackUpdateCount.get(Categories.PROMOTIONS.getName()));
						} else {
							LOGGER.info("Inserting promotions record..");
							//Disable all previous promotions
							int disabled = this.smgSaveyraPromotionsService.updateStatus("disabled");
							LOGGER.info("Disabled previous promotions records {}", disabled);
							smgSaveyraPromotionsEntity = new SmgSaveyraPromotionsEntity();
							this.smgSaveyraPromotionsService.persist(constructPromotionsData(smgSaveyraPromotionsEntity, o, sdf));
							trackInsertCount.put(Categories.PROMOTIONS.getName(), trackInsertCount.get(Categories.PROMOTIONS.getName())+1);
							LOGGER.info("Inserted {} promotions record successfully.", trackInsertCount.get(Categories.PROMOTIONS.getName()));
						}
					} 
					if(Categories.STICKERS.getName().equalsIgnoreCase(category)) {
						Date lastModified = new Date();
						//Mysql db operations
						SmgSaveyraStickersEntity smgSaveyraStickersEntity = this.smgSaveyraStickersService.findByStrickersImageId( o.get("imageId").toString());
						if (smgSaveyraStickersEntity != null) {
							LOGGER.info("Updating stickers record..");
							smgSaveyraStickersEntity.setLastModified(TimezoneConverter.convertLocalToUTC(lastModified));
							this.smgSaveyraStickersService.update(constructStickersData(smgSaveyraStickersEntity, o, sdf));
							trackUpdateCount.put(Categories.STICKERS.getName(), trackUpdateCount.get(Categories.STICKERS.getName())+1);
							LOGGER.info("Updated {} stickers record successfully", trackUpdateCount.get(Categories.STICKERS.getName()));
						} else {
							LOGGER.info("Inserting stickers record..");
							smgSaveyraStickersEntity = new SmgSaveyraStickersEntity();
							smgSaveyraStickersEntity.setLastModified(TimezoneConverter.convertLocalToUTC(lastModified));
							this.smgSaveyraStickersService.persist(constructStickersData(smgSaveyraStickersEntity, o, sdf));
							trackInsertCount.put(Categories.STICKERS.getName(), trackInsertCount.get(Categories.STICKERS.getName())+1);
							LOGGER.info("Inserted {} stickers record successfully.", trackInsertCount.get(Categories.STICKERS.getName()));
						}
						
						//Mongo db operations
						SaveyraImagesCollection imagesCollection = this.smgSaveyraStickersService.findByImageId( o.get("imageId").toString());
						if (imagesCollection != null) {
							LOGGER.info("MongoDB==>Updating images record..");
							this.smgSaveyraStickersService.update(constructImagesCollectionData(imagesCollection, o, sdf));
							trackUpdateCount.put(Categories.IMAGES.getName(), trackUpdateCount.get(Categories.IMAGES.getName())+1);
							LOGGER.info("MongoDB==>Updated {} images record successfully", trackUpdateCount.get(Categories.IMAGES.getName()));
						} else {
							LOGGER.info("MongoDB==>Inserting images record..");
							imagesCollection = new SaveyraImagesCollection();
							this.smgSaveyraStickersService.save(constructImagesCollectionData(imagesCollection, o, sdf));
							trackInsertCount.put(Categories.IMAGES.getName(), trackInsertCount.get(Categories.IMAGES.getName())+1);
							LOGGER.info("MongoDB==>Inserted {} images record successfully.", trackInsertCount.get(Categories.IMAGES.getName()));
						}
						latestLastModifiedDate = sdf.format(smgSaveyraStickersEntity.getLastModified());
					} 
					
					if(Categories.FEATURES.getName().equalsIgnoreCase(category)) {
						SmgSaveyraFeaturesEntity smgSaveyraFeaturesEntity = this.smgSaveyraFeaturesService.findByFeaturesInstanceId( o.get("instanceId").toString());
						if (smgSaveyraFeaturesEntity != null) {
							LOGGER.info("Updating features record..");
							this.smgSaveyraFeaturesService.update(constructFeaturesData(smgSaveyraFeaturesEntity, o, sdf));
							trackUpdateCount.put(Categories.FEATURES.getName(), trackUpdateCount.get(Categories.FEATURES.getName())+1);
							LOGGER.info("Updated {} features record successfully.", trackUpdateCount.get(Categories.FEATURES.getName()));
						} else {
							LOGGER.info("Inserting promotions record..");
							//Disable all previous features
							int disabled = this.smgSaveyraFeaturesService.updateStatus("disabled");
							LOGGER.info("Disabled previous features records {}", disabled);
							smgSaveyraFeaturesEntity = new SmgSaveyraFeaturesEntity();
							this.smgSaveyraFeaturesService.persist(constructFeaturesData(smgSaveyraFeaturesEntity, o, sdf));
							trackInsertCount.put(Categories.FEATURES.getName(), trackInsertCount.get(Categories.FEATURES.getName())+1);
							LOGGER.info("Inserted {} features record successfully.", trackInsertCount.get(Categories.FEATURES.getName()));
						}
					} 
				}
			}
			//Makes call to RE to let RE know about changes in sticker metadata table
            if(latestLastModifiedDate != null && latestLastModifiedDate.length() > 0) {
                 httpRequestToRE(latestLastModifiedDate);
            }
			LOGGER.info("saveCotegroyJson()==>POST /createCategoryJsonData call is end.");
		} catch (Exception e) {
			LOGGER.error("Exception: {}", e.getMessage(), e);
			return getFailureResponse(response, e.getMessage());
		}
		if(errors.size() > 0) {
			return getWarnResponse(response, errors.toString()+". Inserted/Updated data for other categories successfully.");
		}
		return getSuccessResponse(response, "Inserted "+trackInsertCount.get(Categories.PROMOTIONS.getName())+"-'Promotions', "+trackInsertCount.get(Categories.NEWS.getName())+"-'News', "+trackInsertCount.get(Categories.CARDS.getName())+"-'Cards', "
				+trackInsertCount.get(Categories.STICKERS.getName())+"-'Stickers', "+trackInsertCount.get(Categories.IMAGES.getName())+"-'(MongoDB)Images', "+ +trackInsertCount.get(Categories.FEATURES.getName())+"-'Features' /Updated "+trackUpdateCount.get(Categories.PROMOTIONS.getName())+"-'Promotions', "+trackUpdateCount.get(Categories.NEWS.getName())+"-'News', "+trackUpdateCount.get(Categories.CARDS.getName())+"-'Cards', "
				+trackUpdateCount.get(Categories.STICKERS.getName())+"-'Stickers', "+trackUpdateCount.get(Categories.IMAGES.getName())+"-'(MongoDB)Images', "+trackUpdateCount.get(Categories.FEATURES.getName())+"-'Features' records successfully.");
		//return getSuccessResponse(response, "Inserted "+trackInsertCount.get(Categories.PROMOTIONS.getName())+"-'Promotions', "+trackInsertCount.get(Categories.NEWS.getName())+"-'News', "+trackInsertCount.get(Categories.CARDS.getName())+"-'Cards', "
			//	+trackInsertCount.get(Categories.FEATURES.getName())+"-'Features' /Updated "+trackUpdateCount.get(Categories.PROMOTIONS.getName())+"-'Promotions', "+trackUpdateCount.get(Categories.NEWS.getName())+"-'News', "+trackUpdateCount.get(Categories.CARDS.getName())+"-'Cards', "
			//	+trackUpdateCount.get(Categories.FEATURES.getName())+"-'Features' records successfully.");
	}

	private RestResponse getFailureResponse(RestResponse response, String statusMessage) {
		LOGGER.info("getFailureResponse()==>Entered");
		response.setStatus("FAILURE");
		response.setStatusMessage(statusMessage);
		LOGGER.info("getFailureResponse()==>Exit");
		return response;
	}
	
	private RestResponse getWarnResponse(RestResponse response, String statusMessage) {
		LOGGER.info("getWarnResponse()==>Entered");
		response.setStatus("WARNING!");
		response.setStatusMessage(statusMessage);
		LOGGER.info("getWarnResponse()==>Exit");
		return response;
	}
	
	private RestResponse getSuccessResponse(RestResponse response, String statusMessage) {
		LOGGER.info("getSuccessResponse()==>Entered");
		response.setStatus("SUCCESS");
		response.setStatusMessage(statusMessage);
		LOGGER.info("getSuccessResponse()==>Exit");
		return response;
	}
	
	private SmgSaveyraNewsEntity constructNewsData(SmgSaveyraNewsEntity smgSaveyraNewsEntity, Map<String, Object> o, SimpleDateFormat sdf) throws ParseException {
		LOGGER.info("constructNewsData()==>Entered");
		smgSaveyraNewsEntity.setArticleURL( o.get("articleURL").toString());
		smgSaveyraNewsEntity.setCreatedOn(sdf.parse( o.get("createdOn").toString()));
		smgSaveyraNewsEntity.setExpiry(sdf.parse( o.get("expiry").toString()));
		smgSaveyraNewsEntity.setHeadline( o.get("headline").toString());
		smgSaveyraNewsEntity.setHeadLineColor( o.get("headLineColor").toString());
		smgSaveyraNewsEntity.setHeadLineFontSize( o.get("headLineFontSize").toString());
		smgSaveyraNewsEntity.setImageHeightRatio( o.get("imageHeightRatio").toString());
		smgSaveyraNewsEntity.setImagepath( o.get("imagepath").toString());
		smgSaveyraNewsEntity.setImageWidthRatio( o.get("imageWidthRatio").toString());
		smgSaveyraNewsEntity.setLastmodifiedOn(sdf.parse( o.get("lastmodifiedOn").toString()));
		smgSaveyraNewsEntity.setStatus( o.get("status").toString());
		smgSaveyraNewsEntity.setText( o.get("text").toString());
		smgSaveyraNewsEntity.setTextColor( o.get("textColor").toString());
		smgSaveyraNewsEntity.setTextFontSize( o.get("textFontSize").toString());
		smgSaveyraNewsEntity.setTitle( o.get("title").toString());
		smgSaveyraNewsEntity.setInstanceId(o.get("instanceId").toString());
		smgSaveyraNewsEntity.setImageSizeFormat(o.get("imageSizeFormat").toString());
		LOGGER.info("constructNewsData()==>Exit");
		return smgSaveyraNewsEntity;
	}
	
	
	private SmgSaveyraCardsEntity constructCardsData(SmgSaveyraCardsEntity smgSaveyraCardsEntity, Map<String, Object> o, SimpleDateFormat sdf) throws ParseException {
		LOGGER.info("constructCardsData()==>Entered");
		smgSaveyraCardsEntity.setCreatedOn(sdf.parse( o.get("createdOn").toString()));
		smgSaveyraCardsEntity.setExpiry(sdf.parse( o.get("expiry").toString()));
		smgSaveyraCardsEntity.setImagepath( o.get("imagePath").toString());
		smgSaveyraCardsEntity.setLastmodifiedOn(sdf.parse( o.get("lastmodifiedOn").toString()));
		smgSaveyraCardsEntity.setStatus( o.get("status").toString());
		smgSaveyraCardsEntity.setDescription(o.get("description").toString());
		smgSaveyraCardsEntity.setTextColor( o.get("textColor").toString());
		smgSaveyraCardsEntity.setTextFontSize( o.get("textFontSize").toString());
		smgSaveyraCardsEntity.setBackgroundColor( o.get("backgroundColor").toString());
		smgSaveyraCardsEntity.setCardType( o.get("cardType").toString());
		smgSaveyraCardsEntity.setNavToScreen( o.get("navToScreen").toString());
		smgSaveyraCardsEntity.setInstanceId(o.get("instanceId").toString());
		LOGGER.info("constructCardsData()==>Exit");
		return smgSaveyraCardsEntity;
	}
	
	private SmgSaveyraPromotionsEntity constructPromotionsData(SmgSaveyraPromotionsEntity smgSaveyraPromotionsEntity, Map<String, Object> o, SimpleDateFormat sdf) throws ParseException {
		LOGGER.info("constructPromotionsData()==>Entered");
		smgSaveyraPromotionsEntity.setCreatedOn(sdf.parse( o.get("createdOn").toString()));
		smgSaveyraPromotionsEntity.setExpiry(sdf.parse( o.get("expiry").toString()));
		smgSaveyraPromotionsEntity.setHeadline( o.get("headline").toString());
		smgSaveyraPromotionsEntity.setHeadLineColor( o.get("headLineColor").toString());
		smgSaveyraPromotionsEntity.setHeadLineFontSize( o.get("headLineFontSize").toString());
		smgSaveyraPromotionsEntity.setImageHeightRatio( o.get("imageHeightRatio").toString());
		smgSaveyraPromotionsEntity.setImagepath( o.get("imagepath").toString());
		smgSaveyraPromotionsEntity.setImageWidthRatio( o.get("imageWidthRatio").toString());
		smgSaveyraPromotionsEntity.setLastmodifiedOn(sdf.parse( o.get("lastmodifiedOn").toString()));
		smgSaveyraPromotionsEntity.setStatus( o.get("status").toString());
		smgSaveyraPromotionsEntity.setText( o.get("text").toString());
		smgSaveyraPromotionsEntity.setTextColor( o.get("textColor").toString());
		smgSaveyraPromotionsEntity.setTextFontSize( o.get("textFontSize").toString());
		smgSaveyraPromotionsEntity.setTitle( o.get("title").toString());
		smgSaveyraPromotionsEntity.setImageId( o.get("imageId").toString());
		smgSaveyraPromotionsEntity.setKeywords( o.get("keywords").toString());
		smgSaveyraPromotionsEntity.setCategory( o.get("category").toString());
		smgSaveyraPromotionsEntity.setSubcategory( o.get("subcategory").toString());
		smgSaveyraPromotionsEntity.setInstanceId(o.get("instanceId").toString());
		smgSaveyraPromotionsEntity.setImageSizeFormat(o.get("imageSizeFormat").toString());
		LOGGER.info("constructPromotionsData()==>Exit");
		return smgSaveyraPromotionsEntity;
	}
	
	private SmgSaveyraStickersEntity constructStickersData(SmgSaveyraStickersEntity smgSaveyraStickersEntity, Map<String, Object> o, SimpleDateFormat sdf) throws ParseException {
		LOGGER.info("constructStickersData()==>Entered");
		smgSaveyraStickersEntity.setCreatedon(sdf.parse( o.get("createdon").toString()));
		smgSaveyraStickersEntity.setExpiry(sdf.parse( o.get("expiry").toString()));
		smgSaveyraStickersEntity.setCaption(o.get("caption").toString());
		smgSaveyraStickersEntity.setImageExtension(o.get("imageExtension").toString());
		smgSaveyraStickersEntity.setImageSizeFormat(o.get("imageSizeFormat").toString());
		smgSaveyraStickersEntity.setImageType(o.get("imageType").toString());
		smgSaveyraStickersEntity.setImagePath( o.get("imagePath").toString());
		if(o.get("occasionActiveStartDate") != null && !o.get("occasionActiveStartDate").toString().isEmpty())
			smgSaveyraStickersEntity.setOccasionActiveStartDate(sdf.parse(o.get("occasionActiveStartDate").toString()));
		smgSaveyraStickersEntity.setStatus( o.get("status").toString());
		if(o.get("occasionDate") != null && !o.get("occasionDate").toString().isEmpty())
			smgSaveyraStickersEntity.setOccasionDate(sdf.parse( o.get("occasionDate").toString()));
		smgSaveyraStickersEntity.setPaidStickers(Integer.parseInt(o.get("paidStickers").toString()));
		smgSaveyraStickersEntity.setTokens(o.get("tokens").toString());
		smgSaveyraStickersEntity.setProductId(o.get("productID").toString());
		smgSaveyraStickersEntity.setImageId( o.get("imageId").toString());
		smgSaveyraStickersEntity.setKeywords( o.get("keywords").toString());
		smgSaveyraStickersEntity.setCategory( o.get("category").toString());
		smgSaveyraStickersEntity.setSubcategory( o.get("subCategory").toString());
		smgSaveyraStickersEntity.setPrice(o.get("price").toString());
		smgSaveyraStickersEntity.setImageName(o.get("imageName").toString());
		smgSaveyraStickersEntity.setImageDescription(o.get("imageDescription").toString());
		smgSaveyraStickersEntity.setImageFor(o.get("imageFor").toString());
		smgSaveyraStickersEntity.setPriorityFor(o.get("priorityFor").toString());
		smgSaveyraStickersEntity.setOrder(o.get("order").toString());
		if (o.get("age_group") != null) {
			smgSaveyraStickersEntity.setAge(o.get("age_group").toString());
		}
		if (o.get("sticker_gender") != null) {
			smgSaveyraStickersEntity.setGender(o.get("sticker_gender").toString());
		}
		//For  Merchandise stickers
		if (o.get("purchaseURL") != null) {
			smgSaveyraStickersEntity.setPurchaseURL(o.get("purchaseURL").toString());
		}
		LOGGER.info("constructStickersData()==>Exit");
		return smgSaveyraStickersEntity;
	}
	
	private SaveyraImagesCollection constructImagesCollectionData(SaveyraImagesCollection imagesCollection, Map<String, Object> o, SimpleDateFormat sdf) throws ParseException {
		LOGGER.info("constructImagesCollectionData()==>Entered");
		imagesCollection.setArtistName( o.get("artist_name").toString());
		imagesCollection.setAvatarModule(null);
		imagesCollection.setImageID(o.get("imageId").toString());
		imagesCollection.setDescription(o.get("description").toString());
		imagesCollection.setImageSizeFormat(o.get("imageSizeFormat").toString());
		imagesCollection.setImageType(o.get("imageType").toString());
		imagesCollection.setImagePath( o.get("imagePath").toString());
		imagesCollection.setImageName(o.get("image_name").toString());
		imagesCollection.setCreatedDateTimeStamp(o.get("createdon").toString());
		imagesCollection.setPlatform("Both");
		convertJsonToStringByCommaSeparator(o, imagesCollection);
		LOGGER.info("constructImagesCollectionData()==>Exit");
		return imagesCollection;
	}
	
	/**
	 * Converts JSON data to string with comma separated for values as it is easy to search 
	 * 
	 * @param o
	 * @param colection
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private void convertJsonToStringByCommaSeparator(Map<String, Object> o, SaveyraImagesCollection colection) {
		LOGGER.info("convertJsonToStringByCommaSeparator()==>Entered");
		if (o == null || o.get("category") == null) {
			colection.setCategory("");
		} else {
			JSONObject subJo1 = (JSONObject)o.get("category");
			JSONObject subJo = (JSONObject)subJo1.get("category");
			StringBuilder categoryBuilder = new StringBuilder();
			subJo.keySet().forEach(key->{
				categoryBuilder.append(subJo.get(key));
				categoryBuilder.append(",");
			});
			colection.setCategory(categoryBuilder.toString().trim().replaceAll(",$", ""));
		}
		
		if (o == null || o.get("subCategory") == null) {
			colection.setSubCategory("");
		} else {
			JSONObject subJo1 = (JSONObject)o.get("subCategory");
			JSONObject subJo = (JSONObject)subJo1.get("subCategory");
			StringBuilder subCategoryBuilder = new StringBuilder();
			subJo.keySet().forEach(key->{
				subCategoryBuilder.append(subJo.get(key));
				subCategoryBuilder.append(",");
			});
			colection.setSubCategory(subCategoryBuilder.toString().trim().replaceAll(",$", ""));
		}
		
		
		if (o == null || o.get("keywords") == null) {
			colection.setKeywords("");
		} else {
			JSONObject subJo1 = (JSONObject)o.get("keywords");
			JSONObject subJo = (JSONObject)subJo1.get("keywords");
			StringBuilder keywordsBuilder = new StringBuilder();
			subJo.keySet().forEach(key->{
				keywordsBuilder.append(subJo.get(key));
				keywordsBuilder.append(",");
			});
			colection.setKeywords(keywordsBuilder.toString().trim().replaceAll(",$", ""));
		}
		LOGGER.info("convertJsonToStringByCommaSeparator()==>Exit");
	}

	
	private SmgSaveyraFeaturesEntity constructFeaturesData(SmgSaveyraFeaturesEntity smgSaveyraFeaturesEntity, Map<String, Object> o, SimpleDateFormat sdf) throws ParseException {
		LOGGER.info("constructFeaturesData()==>Entered");
		smgSaveyraFeaturesEntity.setCreatedOn(sdf.parse( o.get("createdOn").toString()));
		smgSaveyraFeaturesEntity.setExpiry(sdf.parse( o.get("expiry").toString()));
		smgSaveyraFeaturesEntity.setHeadline( o.get("headline").toString());
		smgSaveyraFeaturesEntity.setHeadLineColor( o.get("headLineColor").toString());
		smgSaveyraFeaturesEntity.setHeadLineFontSize( o.get("headLineFontSize").toString());
		smgSaveyraFeaturesEntity.setImageHeightRatio( o.get("imageHeightRatio").toString());
		smgSaveyraFeaturesEntity.setImagepath( o.get("imagepath").toString());
		smgSaveyraFeaturesEntity.setImageWidthRatio( o.get("imageWidthRatio").toString());
		smgSaveyraFeaturesEntity.setLastmodifiedOn(sdf.parse( o.get("lastmodifiedOn").toString()));
		smgSaveyraFeaturesEntity.setStatus( o.get("status").toString());
		smgSaveyraFeaturesEntity.setText( o.get("text").toString());
		smgSaveyraFeaturesEntity.setTextColor( o.get("textColor").toString());
		smgSaveyraFeaturesEntity.setTextFontSize( o.get("textFontSize").toString());
		smgSaveyraFeaturesEntity.setTitle( o.get("title").toString());
		smgSaveyraFeaturesEntity.setImageId( o.get("imageId").toString());
		smgSaveyraFeaturesEntity.setKeywords( o.get("keywords").toString());
		smgSaveyraFeaturesEntity.setCategory( o.get("category").toString());
		smgSaveyraFeaturesEntity.setSubcategory( o.get("subcategory").toString());
		smgSaveyraFeaturesEntity.setInstanceId(o.get("instanceId").toString());
		smgSaveyraFeaturesEntity.setImageSizeFormat(o.get("imageSizeFormat").toString());
		LOGGER.info("constructFeaturesData()==>Exit");
		return smgSaveyraFeaturesEntity;
	}
	
	private void httpRequestToRE(String latestLastModifiedDate) {
		CloseableHttpClient httpClient = null;
		try {
			LOGGER.info("Making call to RE for the sticker updates and lastModified: {}", latestLastModifiedDate);
			if (latestLastModifiedDate != null) {
				httpClient = HttpClientBuilder.create().build();
				HttpGet getRequest = new HttpGet(
						reUrl + "?lastModified=" + URLEncoder.encode(latestLastModifiedDate, "UTF-8"));
				HttpResponse httpResponse = httpClient.execute(getRequest);
				LOGGER.info("RE response status code {}", httpResponse.getStatusLine().getStatusCode());
			} else {
				LOGGER.warn("Request is not for stickers category");
			}
		} catch (Exception e) {
			LOGGER.error("Error in posting request to RE {}.", e.getMessage(), e);
			e.printStackTrace();
		} finally {
			try {
				// close resource
				if (httpClient != null) {
					httpClient.close();
				}
			} catch (IOException e) {
				LOGGER.error("IOException {}", e.getMessage(), e);
				e.printStackTrace();
			}
		}
}
}
