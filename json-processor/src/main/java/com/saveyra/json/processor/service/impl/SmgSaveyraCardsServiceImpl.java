package com.saveyra.json.processor.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saveyra.json.processor.DBConnection;
import com.saveyra.json.processor.model.SmgSaveyraCardsEntity;
import com.saveyra.json.processor.repository.SmgSaveyraCardsRepository;
import com.saveyra.json.processor.service.SmgSaveyraCardsService;

@Service
public class SmgSaveyraCardsServiceImpl implements SmgSaveyraCardsService {
	private static final Logger LOGGER = LoggerFactory.getLogger(SmgSaveyraCardsServiceImpl.class);
	@Autowired
	private SmgSaveyraCardsRepository smgSaveyraCardsRepository;
	@Value("${jdbc.batch_size}")
	private int batchSize;
	
	@Value("${spring.datasource.url}")
	private String mysqlUrl;
	
	@Value("${spring.datasource.username}")
	private String userName;
	
	@Value("${spring.datasource.password}")
	private String password;
	
	@Value("${spring.datasource.driver-class-name}")
	private String driverName;

	@Transactional
	@Override
	public SmgSaveyraCardsEntity findByCardsInstanceId(String instanceId) {
		return smgSaveyraCardsRepository.findByInstanceId(instanceId);
	}

	@Transactional(readOnly = false)
	@Override
	public void persist(SmgSaveyraCardsEntity smgSaveyraCardsEntity) {
		smgSaveyraCardsRepository.save(smgSaveyraCardsEntity);
	}

	@Transactional(readOnly = false)
	@Override
	public void update(SmgSaveyraCardsEntity smgSaveyraCardsEntity) {
		smgSaveyraCardsRepository.save(smgSaveyraCardsEntity);
	}

	@Override
	public List<SmgSaveyraCardsEntity> findByAll() {
		List<SmgSaveyraCardsEntity> list = new ArrayList<>();
		Iterable<SmgSaveyraCardsEntity> records = smgSaveyraCardsRepository.findAll();
		if(records != null )
			records.iterator().forEachRemaining(list::add);
		return list;
	}

	@Transactional(readOnly = false)
	@Override
	public int updateStatus(String status) {
		return smgSaveyraCardsRepository.updateStatus(status);
	}
	
	@Override
	public void insertCards(List<SmgSaveyraCardsEntity> cards) {
		LOGGER.info("insertCards()==>entered.");
		Connection con = null;
		PreparedStatement ps = null;
		String query = "INSERT INTO smg_saveyra_api_home_screen_cards (imagepath, textColor, textFontSize, backgroundColor,"
				+ "navToScreen, status, expiry, cardType, createdOn, lastmodifiedOn, instanceId, answer, choices, description, cardOrder, "
				+ "question, title, version, percentage, keyword, imageExtension, stickerPack, displayText) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			LOGGER.info("insertCards()==>batch size to insert records {}", batchSize);
			DBConnection db = DBConnection.INSTANCE;
			con = db.getConnection(driverName, mysqlUrl, userName, password);
			if (con != null) {
				con.setAutoCommit(false);
				ps = con.prepareStatement(query);
				
				long start = System.currentTimeMillis();
				int numberOfRecordsForBatch = 0;
				for(int i =0; i<cards.size();i++){
					ps.setString(1, cards.get(i).getImagepath());
					ps.setString(2, cards.get(i).getTextColor());
					ps.setString(3, cards.get(i).getTextFontSize());
					ps.setString(4, cards.get(i).getBackgroundColor());
					ps.setString(5, cards.get(i).getNavToScreen());
					//Not null constraints
					if (cards.get(i).getStatus() != null) {
						ps.setString(6, cards.get(i).getStatus());
					} else {
						ps.setString(6, "enabled");
					}
					if (cards.get(i).getExpiry() != null) {
						ps.setTimestamp(7, new Timestamp(cards.get(i).getExpiry().getTime()));
					} else {
						ps.setTimestamp(7, new Timestamp(new Date().getTime()));
					}
					//Not null constraints
					ps.setString(8, cards.get(i).getCardType());
					if (cards.get(i).getCreatedOn() != null) {
						ps.setTimestamp(9, new Timestamp(cards.get(i).getCreatedOn().getTime()));
					} else {
						ps.setTimestamp(9, new Timestamp(new Date().getTime()));
					}
					//Not null constraints
					if (cards.get(i).getLastmodifiedOn() != null) {
						ps.setTimestamp(10, new Timestamp(cards.get(i).getLastmodifiedOn().getTime()));
					} else {
						ps.setTimestamp(10, new Timestamp(new Date().getTime()));
					}
					ps.setString(11, cards.get(i).getInstanceId());
					ps.setString(12, cards.get(i).getAnswer());
					ps.setString(13, cards.get(i).getChoices());
					ps.setString(14, cards.get(i).getDescription());
					ps.setString(15, cards.get(i).getOrder());
					ps.setString(16, cards.get(i).getQuestion());
					ps.setString(17, cards.get(i).getTitle());
					ps.setString(18, cards.get(i).getVersion());
					ps.setString(19, cards.get(i).getPercentage());
					ps.setString(20, cards.get(i).getKeyword());
					ps.setString(21, cards.get(i).getImageExtension());
					ps.setString(22, cards.get(i).getStickerPack());
					ps.setString(23, cards.get(i).getDisplayText());
					ps.addBatch();
					
					numberOfRecordsForBatch = numberOfRecordsForBatch + 1;
					if ((numberOfRecordsForBatch % batchSize) == 0) {
						int result[] = ps.executeBatch();
						if (result != null) {
							LOGGER.info("insertCards()==>InsideLoop==>Number of inserted records {}", result.length);
						}
						numberOfRecordsForBatch = 0;
					}
				}
				int result [] = ps.executeBatch();
				if (result != null) {
					LOGGER.info("insertCards()==>OutsideLoop==>Number of inserted records {}", result.length);
				}
				con.commit();
				LOGGER.info("insertCards()==>Time Taken="+(System.currentTimeMillis()-start)+" ms");
			}
			
		} catch (SQLException e) {
			LOGGER.error("insertCards()==>SQLException {}", e.getMessage(), e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (con != null)
					con.close();
				LOGGER.info("insertCards()==>finally block==>Resourece is released.");
			} catch (SQLException e) {
				LOGGER.error("insertCards()==>==>finally block==>Error in while releasing resource {}.", e.getMessage(), e);
			}
		}
		LOGGER.info("insertCards()==>exit.");
	}

	@Override
	public void updateCards(List<SmgSaveyraCardsEntity> cards) {
		LOGGER.info("updateCards()==>entered.");
		Connection con = null;
		PreparedStatement ps = null;
		String query = "UPDATE smg_saveyra_api_home_screen_cards SET imagepath=?, textColor=?, textFontSize=?, backgroundColor=?,"
				+ "navToScreen=?, status=?, expiry=?, cardType=?, createdOn=?, lastmodifiedOn=?, instanceId=?, answer=?, choices=?, description=?, cardOrder=?, "
				+ "question=?, title=?, version=?, percentage=? , keyword=?, imageExtension=?, stickerPack=?, displayText=? WHERE instanceId=?";
		try {
			LOGGER.info("updateCards()==>batch size to update records {}", batchSize);
			DBConnection db = DBConnection.INSTANCE;
			con = db.getConnection(driverName, mysqlUrl, userName, password);
			if (con != null) {
				con.setAutoCommit(false);
				ps = con.prepareStatement(query);
				
				long start = System.currentTimeMillis();
				int numberOfRecordsForBatch = 0;
				for (int i = 0; i < cards.size(); i++) {
					ps.setString(1, cards.get(i).getImagepath());
					ps.setString(2, cards.get(i).getTextColor());
					ps.setString(3, cards.get(i).getTextFontSize());
					ps.setString(4, cards.get(i).getBackgroundColor());
					ps.setString(5, cards.get(i).getNavToScreen());
					//Not null constraints
					if (cards.get(i).getStatus() != null) {
						ps.setString(6, cards.get(i).getStatus());
					} else {
						ps.setString(6, "enabled");
					}
					if (cards.get(i).getExpiry() != null) {
						ps.setTimestamp(7, new Timestamp(cards.get(i).getExpiry().getTime()));
					} else {
						ps.setTimestamp(7, new Timestamp(new Date().getTime()));
					}
					ps.setString(8, cards.get(i).getCardType());
					//Not null constraints
					if (cards.get(i).getCreatedOn() != null) {
						ps.setTimestamp(9, new Timestamp(cards.get(i).getCreatedOn().getTime()));
					} else {
						ps.setTimestamp(9, new Timestamp(new Date().getTime()));
					}
					//Not null constraints
					if (cards.get(i).getLastmodifiedOn() != null) {
						ps.setTimestamp(10, new Timestamp(cards.get(i).getLastmodifiedOn().getTime()));
					} else {
						ps.setTimestamp(10, new Timestamp(new Date().getTime()));
					}
					ps.setString(11, cards.get(i).getInstanceId());
					ps.setString(12, cards.get(i).getAnswer());
					ps.setString(13, cards.get(i).getChoices());
					ps.setString(14, cards.get(i).getDescription());
					ps.setString(15, cards.get(i).getOrder());
					ps.setString(16, cards.get(i).getQuestion());
					ps.setString(17, cards.get(i).getTitle());
					ps.setString(18, cards.get(i).getVersion());
					ps.setString(19, cards.get(i).getPercentage());
					ps.setString(20, cards.get(i).getKeyword());
					ps.setString(21, cards.get(i).getImageExtension());
					ps.setString(22, cards.get(i).getStickerPack());
					ps.setString(23, cards.get(i).getDisplayText());
					ps.setString(24, cards.get(i).getInstanceId());
					ps.addBatch();
					numberOfRecordsForBatch = numberOfRecordsForBatch + 1;
					if ((numberOfRecordsForBatch % batchSize) == 0) {
						int result[] = ps.executeBatch();
						if (result != null) {
							LOGGER.info("updateCards()==>InsideLoop==>Number of updated records {}", result.length);
						}
						numberOfRecordsForBatch = 0;
					}
				}
				int result [] = ps.executeBatch();
				if (result != null) {
					LOGGER.info("updateCards()==>OutsideLoop==>Number of updated records {}", result.length);
				}
				con.commit();
				LOGGER.info("updateCards()==>Time Taken="+(System.currentTimeMillis()-start)+" ms");
			}
			
		} catch (SQLException e) {
			LOGGER.error("updateCards==>SQLException {}", e.getMessage(), e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (con != null)
					con.close();
				LOGGER.info("updateCards()==>finally block==>Resource is released.");
			} catch (SQLException e) {
				LOGGER.error("updateCards()==>finally block==>Error in while releasing resource {}.", e.getMessage(), e);
			}
		}
		LOGGER.info("updateCards()==>exit.");
	}
}
