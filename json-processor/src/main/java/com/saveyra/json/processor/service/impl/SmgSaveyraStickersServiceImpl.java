package com.saveyra.json.processor.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saveyra.json.processor.model.SaveyraImagesCollection;
import com.saveyra.json.processor.model.SmgSaveyraStickersEntity;
import com.saveyra.json.processor.repository.SaveyraImagesCollectionRepository;
import com.saveyra.json.processor.repository.SmgSaveyraStickersRepository;
import com.saveyra.json.processor.service.SmgSaveyraStickersService;

@Service
public class SmgSaveyraStickersServiceImpl implements SmgSaveyraStickersService {
	@Autowired
	private SmgSaveyraStickersRepository smgSaveyraStickersRepository;
	
	@Autowired
	private SaveyraImagesCollectionRepository saveyraImagesCollectionRepository;

	@Transactional
	@Override
	public SmgSaveyraStickersEntity findByStrickersImageId(String imageId) {
		return smgSaveyraStickersRepository.findByImageId(imageId);
	}

	@Transactional(readOnly = false)
	@Override
	public void persist(SmgSaveyraStickersEntity smgSaveyraStickersEntity) {
		smgSaveyraStickersRepository.save(smgSaveyraStickersEntity);
	}

	@Transactional(readOnly = false)
	@Override
	public void update(SmgSaveyraStickersEntity smgSaveyraStickersEntity) {
		smgSaveyraStickersRepository.save(smgSaveyraStickersEntity);
	}

	@Override
	public SaveyraImagesCollection findByImageId(String imageId) {
		return saveyraImagesCollectionRepository.findByImageID(imageId);
	}

	@Transactional(readOnly = false)
	@Override
	public void save(SaveyraImagesCollection saveyraImagesCollection) {
		saveyraImagesCollectionRepository.save(saveyraImagesCollection);
	}

	@Transactional(readOnly = false)
	@Override
	public void update(SaveyraImagesCollection saveyraImagesCollection) {
		saveyraImagesCollectionRepository.save(saveyraImagesCollection);	
	}

}
