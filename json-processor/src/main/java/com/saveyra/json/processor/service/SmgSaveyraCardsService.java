package com.saveyra.json.processor.service;

import java.util.List;

import com.saveyra.json.processor.model.SmgSaveyraCardsEntity;

public interface SmgSaveyraCardsService {
	SmgSaveyraCardsEntity findByCardsInstanceId(String instanceId);
	void persist(SmgSaveyraCardsEntity smgSaveyraCardsEntity);
	void update(SmgSaveyraCardsEntity smgSaveyraCardsEntity);
	List<SmgSaveyraCardsEntity> findByAll();
	int updateStatus(String status);
	void insertCards(List<SmgSaveyraCardsEntity> cards);
	void updateCards(List<SmgSaveyraCardsEntity> cards);
}
