package com.saveyra.json.processor.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.saveyra.json.processor.model.SmgSaveyraStickersEntity;

@Repository
public interface SmgSaveyraStickersRepository extends CrudRepository<SmgSaveyraStickersEntity, Integer>  {
	SmgSaveyraStickersEntity findByImageId(String imageId);
}
