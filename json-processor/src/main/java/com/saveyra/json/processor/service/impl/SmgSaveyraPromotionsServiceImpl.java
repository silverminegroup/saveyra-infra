package com.saveyra.json.processor.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saveyra.json.processor.model.SmgSaveyraPromotionsEntity;
import com.saveyra.json.processor.repository.SmgSaveyraPromotionsRepository;
import com.saveyra.json.processor.service.SmgSaveyraPromotionsService;

@Service
public class SmgSaveyraPromotionsServiceImpl implements SmgSaveyraPromotionsService {
	@Autowired
	private SmgSaveyraPromotionsRepository smgSaveyraPromotionsRepository;

	@Transactional
	@Override
	public SmgSaveyraPromotionsEntity findByPromotionsInstanceId(String instanceId) {
		return smgSaveyraPromotionsRepository.findByInstanceId(instanceId);
	}

	@Transactional(readOnly = false)
	@Override
	public void persist(SmgSaveyraPromotionsEntity smgSaveyraPromotionsEntity) {
		smgSaveyraPromotionsRepository.save(smgSaveyraPromotionsEntity);
	}

	@Transactional(readOnly = false)
	@Override
	public void update(SmgSaveyraPromotionsEntity smgSaveyraPromotionsEntity) {
		smgSaveyraPromotionsRepository.save(smgSaveyraPromotionsEntity);
	}

	@Override
	public List<SmgSaveyraPromotionsEntity> findByAll() {
		List<SmgSaveyraPromotionsEntity> list = new ArrayList<>();
		Iterable<SmgSaveyraPromotionsEntity> records = smgSaveyraPromotionsRepository.findAll();
		if(records != null )
			records.iterator().forEachRemaining(list::add);
		return list;
	}

	@Transactional(readOnly = false)
	@Override
	public int updateStatus(String status) {
		return smgSaveyraPromotionsRepository.updateStatus(status);
	}

}
