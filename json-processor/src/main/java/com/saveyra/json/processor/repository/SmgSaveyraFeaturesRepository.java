package com.saveyra.json.processor.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.saveyra.json.processor.model.SmgSaveyraFeaturesEntity;

@Repository
public interface SmgSaveyraFeaturesRepository extends CrudRepository<SmgSaveyraFeaturesEntity, Integer> {
	SmgSaveyraFeaturesEntity findByInstanceId(String instanceId);
	@Modifying(clearAutomatically = true)
	@Query(value="UPDATE smg_saveyra_api_home_screen_features features SET features.status = :status WHERE features.status = 'enabled'", nativeQuery = true)
	int updateStatus(@Param("status") String status);
}
