package com.saveyra.json.processor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum DBConnection {
	INSTANCE;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DBConnection.class);
	
	public Connection getConnection(String driverName, String url, String userName, String password) {
		LOGGER.info("Database connection is establishing... {}, {}, {}, {} ", driverName, url, userName, password);
		Connection con = null;
		try {
			//inputStream = this.getClass().getClassLoader().getResourceAsStream("jsonprocessor.properties");
			//props.load(inputStream);

			// load the Driver Class
			Class.forName(driverName);

			// create the connection now
			con = DriverManager.getConnection(url+"&rewriteBatchedStatements=true", userName, password);
		} catch (ClassNotFoundException e) {
			LOGGER.error("ClassNotFoundException {}",e.getMessage(), e);
		} catch (SQLException e) {
			LOGGER.error("SQLException {}",e.getMessage(), e);
		}
		LOGGER.info("Database connection is established.");
		return con;
	}
}
