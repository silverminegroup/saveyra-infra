package com.saveyra.json.processor.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saveyra.json.processor.model.SmgSaveyraNewsEntity;
import com.saveyra.json.processor.repository.SmgSaveyraNewsRepository;
import com.saveyra.json.processor.service.SmgSaveyraNewsService;

@Service
public class SmgSaveyraNewsServiceImpl implements SmgSaveyraNewsService {
	@Autowired
	private SmgSaveyraNewsRepository smgSaveyraNewsRepository;

	@Transactional
	@Override
	public SmgSaveyraNewsEntity findByNewsInstanceId(String instanceId) {
		return smgSaveyraNewsRepository.findByInstanceId(instanceId);
	}

	@Transactional(readOnly = false)
	@Override
	public void persist(SmgSaveyraNewsEntity smgSaveyraNewsEntity) {
		smgSaveyraNewsRepository.save(smgSaveyraNewsEntity);
	}

	@Transactional(readOnly = false)
	@Override
	public void update(SmgSaveyraNewsEntity smgSaveyraNewsEntity) {
		smgSaveyraNewsRepository.save(smgSaveyraNewsEntity);
	}

	@Override
	public List<SmgSaveyraNewsEntity> findByAll() {
		List<SmgSaveyraNewsEntity> list = new ArrayList<>();
		Iterable<SmgSaveyraNewsEntity> records = smgSaveyraNewsRepository.findAll();
		if(records != null )
			records.iterator().forEachRemaining(list::add);
		return list;
	}

	@Transactional(readOnly = false)
	@Override
	public int updateStatus(String status) {
		return smgSaveyraNewsRepository.updateStatus(status);
		
	}

}
