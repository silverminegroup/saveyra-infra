package com.saveyra.json.processor.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.saveyra.json.processor.model.SmgSaveyraPromotionsEntity;

@Repository
public interface SmgSaveyraPromotionsRepository extends CrudRepository<SmgSaveyraPromotionsEntity, Integer> {
	SmgSaveyraPromotionsEntity findByInstanceId(String instanceId);
	@Modifying(clearAutomatically = true)
	@Query(value="UPDATE smg_saveyra_api_home_screen_promotions promotions SET promotions.status = :status WHERE promotions.status = 'enabled'", nativeQuery = true)
	int updateStatus(@Param("status") String status);
}
