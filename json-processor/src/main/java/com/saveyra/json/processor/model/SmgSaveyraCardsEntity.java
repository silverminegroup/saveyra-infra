package com.saveyra.json.processor.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "smg_saveyra_api_home_screen_cards")
public class SmgSaveyraCardsEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "imagepath")
	private String imagepath;
	@Column(name = "textColor")
	private String textColor;
	@Column(name = "textFontSize")
	private String textFontSize;
	@Column(name = "backgroundColor")
	private String backgroundColor;
	@Column(name = "navToScreen")
	private String navToScreen;
	@Column(name = "status")
	private String status;
	@Column(name = "expiry")
	private Date expiry;
	@Column(name = "cardType")
	private String cardType;
	@Column(name = "createdOn")
	private Date createdOn;
	@Column(name = "lastmodifiedOn")
	private Date lastmodifiedOn;
	@Column(name = "instanceId")
	private String instanceId;
	@Column(name = "answer")
	private String answer;
	@Column(name = "choices")
	private String choices;
	@Column(name = "description")
	private String description;
	@Column(name = "cardOrder")
	private String order;
	@Column(name = "question")
	private String question;
	@Column(name = "title")
	private String title;
	@Column(name = "version")
	private String version;
	@Column(name = "percentage")
	private String percentage;
	@Column(name = "keyword")
	private String keyword;
	@Column(name="imageExtension")
	private String imageExtension;
	@Column(name="stickerPack")
	private String stickerPack;
	@Column(name="displayText")
	private String displayText;
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the imagepath
	 */
	public String getImagepath() {
		return imagepath;
	}
	/**
	 * @param imagepath the imagepath to set
	 */
	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}
	/**
	 * @return the textColor
	 */
	public String getTextColor() {
		return textColor;
	}
	/**
	 * @param textColor the textColor to set
	 */
	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}
	/**
	 * @return the textFontSize
	 */
	public String getTextFontSize() {
		return textFontSize;
	}
	/**
	 * @param textFontSize the textFontSize to set
	 */
	public void setTextFontSize(String textFontSize) {
		this.textFontSize = textFontSize;
	}
	/**
	 * @return the backgroundColor
	 */
	public String getBackgroundColor() {
		return backgroundColor;
	}
	/**
	 * @param backgroundColor the backgroundColor to set
	 */
	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}
	/**
	 * @return the navToScreen
	 */
	public String getNavToScreen() {
		return navToScreen;
	}
	/**
	 * @param navToScreen the navToScreen to set
	 */
	public void setNavToScreen(String navToScreen) {
		this.navToScreen = navToScreen;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the expiry
	 */
	public Date getExpiry() {
		return expiry;
	}
	/**
	 * @param expiry the expiry to set
	 */
	public void setExpiry(Date expiry) {
		this.expiry = expiry;
	}
	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}
	/**
	 * @param cardType the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the lastmodifiedOn
	 */
	public Date getLastmodifiedOn() {
		return lastmodifiedOn;
	}
	/**
	 * @param lastmodifiedOn the lastmodifiedOn to set
	 */
	public void setLastmodifiedOn(Date lastmodifiedOn) {
		this.lastmodifiedOn = lastmodifiedOn;
	}
	/**
	 * @return the instanceId
	 */
	public String getInstanceId() {
		return instanceId;
	}
	/**
	 * @param instanceId the instanceId to set
	 */
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	/**
	 * @return the answer
	 */
	public String getAnswer() {
		return answer;
	}
	/**
	 * @param answer the answer to set
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	/**
	 * @return the choices
	 */
	public String getChoices() {
		return choices;
	}
	/**
	 * @param choices the choices to set
	 */
	public void setChoices(String choices) {
		this.choices = choices;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the order
	 */
	public String getOrder() {
		return order;
	}
	/**
	 * @param order the order to set
	 */
	public void setOrder(String order) {
		this.order = order;
	}
	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}
	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}
	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	/**
	 * @return the percentage
	 */
	public String getPercentage() {
		return percentage;
	}
	/**
	 * @param percentage the percentage to set
	 */
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	/**
	 * @return the keyword
	 */
	public String getKeyword() {
		return keyword;
	}
	/**
	 * @param keyword the keyword to set
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	/**
	 * @return the imageExtension
	 */
	public String getImageExtension() {
		return imageExtension;
	}
	/**
	 * @param imageExtension the imageExtension to set
	 */
	public void setImageExtension(String imageExtension) {
		this.imageExtension = imageExtension;
	}
	/**
	 * @return the stickerPack
	 */
	public String getStickerPack() {
		return stickerPack;
	}
	/**
	 * @param stickerPack the stickerPack to set
	 */
	public void setStickerPack(String stickerPack) {
		this.stickerPack = stickerPack;
	}
	/**
	 * @return the displayText
	 */
	public String getDisplayText() {
		return displayText;
	}
	/**
	 * @param displayText the displayText to set
	 */
	public void setDisplayText(String displayText) {
		this.displayText = displayText;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SmgSaveyraCardsEntity [id=" + id + ", imagepath=" + imagepath + ", textColor=" + textColor
				+ ", textFontSize=" + textFontSize + ", backgroundColor=" + backgroundColor + ", navToScreen="
				+ navToScreen + ", status=" + status + ", expiry=" + expiry + ", cardType=" + cardType + ", createdOn="
				+ createdOn + ", lastmodifiedOn=" + lastmodifiedOn + ", instanceId=" + instanceId + ", answer=" + answer
				+ ", choices=" + choices + ", description=" + description + ", order=" + order + ", question="
				+ question + ", title=" + title + ", version=" + version + ", percentage=" + percentage + ", keyword="
				+ keyword + ", imageExtension=" + imageExtension + ", stickerPack=" + stickerPack + ", displayText="
				+ displayText + "]";
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((answer == null) ? 0 : answer.hashCode());
		result = prime * result + ((backgroundColor == null) ? 0 : backgroundColor.hashCode());
		result = prime * result + ((cardType == null) ? 0 : cardType.hashCode());
		result = prime * result + ((choices == null) ? 0 : choices.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((displayText == null) ? 0 : displayText.hashCode());
		result = prime * result + ((expiry == null) ? 0 : expiry.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((imageExtension == null) ? 0 : imageExtension.hashCode());
		result = prime * result + ((imagepath == null) ? 0 : imagepath.hashCode());
		result = prime * result + ((instanceId == null) ? 0 : instanceId.hashCode());
		result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
		result = prime * result + ((lastmodifiedOn == null) ? 0 : lastmodifiedOn.hashCode());
		result = prime * result + ((navToScreen == null) ? 0 : navToScreen.hashCode());
		result = prime * result + ((order == null) ? 0 : order.hashCode());
		result = prime * result + ((percentage == null) ? 0 : percentage.hashCode());
		result = prime * result + ((question == null) ? 0 : question.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((stickerPack == null) ? 0 : stickerPack.hashCode());
		result = prime * result + ((textColor == null) ? 0 : textColor.hashCode());
		result = prime * result + ((textFontSize == null) ? 0 : textFontSize.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SmgSaveyraCardsEntity other = (SmgSaveyraCardsEntity) obj;
		if (answer == null) {
			if (other.answer != null)
				return false;
		} else if (!answer.equals(other.answer))
			return false;
		if (backgroundColor == null) {
			if (other.backgroundColor != null)
				return false;
		} else if (!backgroundColor.equals(other.backgroundColor))
			return false;
		if (cardType == null) {
			if (other.cardType != null)
				return false;
		} else if (!cardType.equals(other.cardType))
			return false;
		if (choices == null) {
			if (other.choices != null)
				return false;
		} else if (!choices.equals(other.choices))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (displayText == null) {
			if (other.displayText != null)
				return false;
		} else if (!displayText.equals(other.displayText))
			return false;
		if (expiry == null) {
			if (other.expiry != null)
				return false;
		} else if (!expiry.equals(other.expiry))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (imageExtension == null) {
			if (other.imageExtension != null)
				return false;
		} else if (!imageExtension.equals(other.imageExtension))
			return false;
		if (imagepath == null) {
			if (other.imagepath != null)
				return false;
		} else if (!imagepath.equals(other.imagepath))
			return false;
		if (instanceId == null) {
			if (other.instanceId != null)
				return false;
		} else if (!instanceId.equals(other.instanceId))
			return false;
		if (keyword == null) {
			if (other.keyword != null)
				return false;
		} else if (!keyword.equals(other.keyword))
			return false;
		if (lastmodifiedOn == null) {
			if (other.lastmodifiedOn != null)
				return false;
		} else if (!lastmodifiedOn.equals(other.lastmodifiedOn))
			return false;
		if (navToScreen == null) {
			if (other.navToScreen != null)
				return false;
		} else if (!navToScreen.equals(other.navToScreen))
			return false;
		if (order == null) {
			if (other.order != null)
				return false;
		} else if (!order.equals(other.order))
			return false;
		if (percentage == null) {
			if (other.percentage != null)
				return false;
		} else if (!percentage.equals(other.percentage))
			return false;
		if (question == null) {
			if (other.question != null)
				return false;
		} else if (!question.equals(other.question))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (stickerPack == null) {
			if (other.stickerPack != null)
				return false;
		} else if (!stickerPack.equals(other.stickerPack))
			return false;
		if (textColor == null) {
			if (other.textColor != null)
				return false;
		} else if (!textColor.equals(other.textColor))
			return false;
		if (textFontSize == null) {
			if (other.textFontSize != null)
				return false;
		} else if (!textFontSize.equals(other.textFontSize))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}
	
	
}
