package com.saveyra.json.processor.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.saveyra.json.processor.model.SaveyraImagesCollection;

public interface SaveyraImagesCollectionRepository extends MongoRepository<SaveyraImagesCollection, Integer>{
	SaveyraImagesCollection findByImageID(String imageId);
}
