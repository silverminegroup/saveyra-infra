package com.saveyra.json.processor.data;

import org.json.simple.JSONArray;

public class HomeRequestData {
	private JSONArray cards;

	/**
	 * @return the cards
	 */
	public JSONArray getCards() {
		return cards;
	}

	/**
	 * @param cards the cards to set
	 */
	public void setCards(JSONArray cards) {
		this.cards = cards;
	}
}
