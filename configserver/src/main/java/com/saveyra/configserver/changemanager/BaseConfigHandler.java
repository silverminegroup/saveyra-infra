package com.saveyra.configserver.changemanager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.TypeAdapter;
import com.saveyra.configserver.utility.Declaration;
import com.saveyra.configserver.utility.ResolveJson;
import com.saveyra.configserver.utility.ResolveJsonV3;
import com.saveyra.configserver.utility.configurationV3;

public class BaseConfigHandler {

	private static final Logger logger = LoggerFactory.getLogger(BaseConfigHandler.class);

	public void readAllBaseConfig() {
		logger.info("[ENTER] readAllBaseConfig");
		File folder = new File(Declaration.getJAR_PARENT() + Declaration.getBaseConfigPath());
		if (!folder.exists()) {
			logger.error("[ERROR] JAVA CONFIGURATIONS FOLDER NOT FOUND in " + Declaration.getJAR_PARENT()
					+ Declaration.getBaseConfigPath());
			return;
		}
		logger.info(
				"[INFO] JAVA CONFIGURATIONS FOLDER " + Declaration.getJAR_PARENT() + Declaration.getBaseConfigPath());
		File[] listOfFiles = folder.listFiles();

		if (listOfFiles == null) {
			logger.error("[ERROR] No Files in Folder");
			return;
		}

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				logger.info("[INFO] Processing File : " + listOfFiles[i]);
				File file;
				String JSON = null;
				String version = null;
				try {
					version = (listOfFiles[i].getName().split("-")[1]).split("\\.")[0];
				} catch (Exception e) {
					logger.error("[ERROR] Error in file");
					continue;
				}
				try {
					file = new File(listOfFiles[i].toString());
					JSON = new String(Files.readAllBytes(Paths.get(file.toURI())));
				} catch (FileNotFoundException e1) {
					logger.error("[ERROR] Error in file");
					e1.printStackTrace();
					return;
				} catch (IOException e) {
					logger.error("[ERROR] Error in file");
					e.printStackTrace();
					return;
				}
				Gson gson = new Gson();
				//System.out.println("version >> " + version);
				
				int versionInt = 1;
				
				if(version.contains("v")) {
					versionInt = Integer.parseInt(version.split("v")[1]);
				}else if(version.contains("V")) {
					versionInt = Integer.parseInt(version.split("V")[1]);
				}

				if (versionInt>2) {
					TypeAdapter<ResolveJsonV3> adapterV3 = gson.getAdapter(ResolveJsonV3.class);
					try {
						//JsonParser parser = new JsonParser();
						/*Object obj = parser.parse(JSON);
						JsonObject lev1 = (JsonObject) obj;
						JsonObject parent = (JsonObject) lev1.get("configuration");*/
						//configurationV3 v3 = gson.fromJson(JSON, configurationV3.class);
						//ResolveJsonV3 rjv3 = new ResolveJsonV3(v3);
						//System.out.println(">>>>> " + rjv3.getConfigurationV3().getANALYTICS_URL());
						
						
						Declaration.getResolvedJsonMap().put(version, adapterV3.fromJson(JSON));
						Declaration.getResolvedJsonMapBackUp().put(version, adapterV3.fromJson(JSON));
					} catch (Exception eIn) {
						eIn.printStackTrace();
						logger.info("[INFO] Error in file Resolution");
						return;
					}
				} else {
					TypeAdapter<ResolveJson> adapter = gson.getAdapter(ResolveJson.class);
					try {
						Declaration.getResolvedJsonMap().put(version, adapter.fromJson(JSON));
						Declaration.getResolvedJsonMapBackUp().put(version, adapter.fromJson(JSON));
					} catch (IOException e) {
						e.printStackTrace();
						logger.info("[INFO] Error in file Resolution");
					} catch (Exception e) {
						e.printStackTrace();
						logger.info("[INFO] Error in file Resolution");
					}
				}
			}
		}
		logger.info("[EXIT] readAllBaseConfig");
	}

	public void readBaseConfig(String fileName) {
		logger.info("[ENTER]  readBaseConfig");
		File file;
		String JSON = null;
		String version = null;
		try {
			version = (fileName.split("-")[1]).split("\\.")[0];
		} catch (Exception e) {
			logger.info("[INFO] Error in file");
			return;
		}
		try {
			String path = Declaration.getJAR_PARENT() + Declaration.getBaseConfigPath() + fileName;
			file = new File(path);
			if (!file.exists()) {
				logger.error("[ERROR] File Not Found in " + path);
			}

			try {
				logger.info("[INFO] Thread Waiting :" + Thread.currentThread().getName());
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.info("[INFO] Thread Wait Exception");
				e.printStackTrace();
			}

			JSON = new String(Files.readAllBytes(Paths.get(file.toURI())));
		} catch (FileNotFoundException e1) {

			e1.printStackTrace();
			return;
		} catch (IOException e) {

			e.printStackTrace();
			return;
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		Gson gson = new Gson();
		
		int versionInt = 1;
		
		if(version.contains("v")) {
			versionInt = Integer.parseInt(version.split("v")[1]);
		}else if(version.contains("V")) {
			versionInt = Integer.parseInt(version.split("V")[1]);
		}
		

		if (versionInt>2) {
			TypeAdapter<ResolveJsonV3> adapterV3 = gson.getAdapter(ResolveJsonV3.class);
			try {
				//JsonParser parser = new JsonParser();
				/*Object obj = parser.parse(JSON);
				JsonObject lev1 = (JsonObject) obj;
				JsonObject parent = (JsonObject) lev1.get("configuration");*/
				//configurationV3 v3 = gson.fromJson(JSON, configurationV3.class);
				//ResolveJsonV3 rjv3 = new ResolveJsonV3(v3);
				//System.out.println(">>>>> " + rjv3.getConfigurationV3().getANALYTICS_URL());
				
				Declaration.getResolvedJsonMap().put(version, adapterV3.fromJson(JSON));
				Declaration.getResolvedJsonMapBackUp().put(version, adapterV3.fromJson(JSON));
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			TypeAdapter<ResolveJson> adapter = gson.getAdapter(ResolveJson.class);
			try {
				Declaration.getResolvedJsonMap().put(version, adapter.fromJson(JSON));
				Declaration.getResolvedJsonMapBackUp().put(version, adapter.fromJson(JSON));
			} catch (IOException e) {
				e.printStackTrace();
				logger.error("[ERROR] File Read error");
				return;
			}
		}
		logger.info("[EXIT] readBaseConfig");
	}
}
