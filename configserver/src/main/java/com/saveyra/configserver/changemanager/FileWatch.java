package com.saveyra.configserver.changemanager;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.saveyra.configserver.utility.Declaration;

public class FileWatch implements Runnable {

	String Path;
	private static final Logger logger = LoggerFactory.getLogger(FileWatch.class);

	public FileWatch(String sPath) {
		this.Path = sPath;
	}

	@Override
	public void run() {

		logger.info("[INFO] File Watch Service Start :"+Thread.currentThread().getName());

		try {
			WatchService watchService = FileSystems.getDefault().newWatchService();			
			Path path = Paths.get(Path);
			path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE,
					StandardWatchEventKinds.ENTRY_MODIFY);
			WatchKey key;
			while ((key = watchService.take()) != null) {
				for (WatchEvent<?> event : key.pollEvents()) {
					logger.info("[INFO] Event kind:" + event.kind().toString() + ". File affected: "
							+ event.context().toString() + ".");

					if (event.kind().toString().equals("ENTRY_CREATE")||event.kind().toString().equals("ENTRY_MODIFY")) {												
						String sFilePathPlatform = Declaration.getJAR_PARENT() + Declaration.getPlatformConfigPath()
								+ event.context().toString();
						File file = null;
						try {
							file = new File(sFilePathPlatform);
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							logger.error("[ERROR] Thread Sleep Error");
							e.printStackTrace();
						}
						
						if (file.exists()) {
							ParameterConfigHandler pcHandler = new ParameterConfigHandler();
							pcHandler.readParameterConfig(event.context().toString());
						} else {
							String sFilePathBase = Declaration.getJAR_PARENT() + Declaration.getBaseConfigPath()
									+ event.context().toString();
							file = new File(sFilePathBase);
							if (file.exists()) {
								BaseConfigHandler bcHandler = new BaseConfigHandler();
								bcHandler.readBaseConfig(event.context().toString());
							}
						}
					} else if (event.kind().toString().equals("ENTRY_DELETE")) {
						//To be handled
					}
				}
				key.reset();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}