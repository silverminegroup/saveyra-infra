package com.saveyra.configserver.changemanager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.saveyra.configserver.utility.Declaration;

public class ParameterConfigHandler {

	private static final Logger logger = LoggerFactory.getLogger(ParameterConfigHandler.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void readAllConfgurations() {

		logger.info("[ENTER] readAllConfgurations");
		File folder = new File(Declaration.getJAR_PARENT() + Declaration.getPlatformConfigPath());
		if (!folder.exists()) {
			logger.error("[ERROR] JAVA CONFIGURATIONS FOLDER NOT FOUND !");
			return;
		}
		logger.info(
				"[INFO] JAVA CONFIGURATIONS FOLDER " + Declaration.getJAR_PARENT() + Declaration.getPlatformConfigPath()
						+ "\n" + Declaration.getJAR_PARENT() + Declaration.getPlatformConfigPath());
		File[] listOfFiles = folder.listFiles();

		if (listOfFiles == null) {
			logger.error("[ERROR] No Files in Folder");
			return;
		}
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				logger.info("[INFO] Processing File : " + listOfFiles[i]);
				File file;
				String JSON = null;
				String key = null;
				try {
					key = listOfFiles[i].getName().split("\\.")[0];
				} catch (Exception e) {
					logger.error("[ERROR] Error in filename");
					continue;
				}
				try {
					file = new File(listOfFiles[i].toString());
					JSON = new String(Files.readAllBytes(Paths.get(file.toURI())));
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
					logger.error("[ERROR] Error in file read");
					return;
				} catch (IOException e) {
					logger.error("[ERROR] Error in file read");
					e.printStackTrace();
					return;
				}
				Gson gson = new Gson();
				TypeAdapter<Map> adapter = gson.getAdapter(Map.class);
				try {
					Declaration.getConfigJsonMap().put(key, adapter.fromJson(JSON));
				} catch (IOException e) {
					logger.error("[ERROR] Error in reading file");
					e.printStackTrace();
					return;
				}
			}
		}
		logger.info("[EXIT] readAllConfgurations");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void readParameterConfig(String fileName) {
		logger.info("[ENTER] readParameterConfig");
		File file;
		String JSON = null;
		try {
			String path = Declaration.getJAR_PARENT() + Declaration.getPlatformConfigPath() + fileName;
			file = new File(path);
			if (!file.exists()) {
				logger.error("[ERROR] FILE NOT FOUND in " + path);
				return;
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.info("[INFO] Thread Sleep Error");
				e.printStackTrace();
			}
			JSON = new String(Files.readAllBytes(Paths.get(file.toURI())));
		} catch (FileNotFoundException e1) {
			logger.info("[INFO] Error in file");
			e1.printStackTrace();
			return;
		} catch (IOException e) {
			logger.info("[INFO] Error in file");
			e.printStackTrace();
			return;
		}
		String key = null;
		try {
			key = fileName.split("\\.")[0];
		} catch (Exception e) {
			logger.info("[INFO] Error in file");
			return;
		}
		Gson gson = new Gson();
		TypeAdapter<Map> adapter = gson.getAdapter(Map.class);
		try {
			Declaration.getConfigJsonMap().put(key, adapter.fromJson(JSON));
		} catch (IOException e) {
			logger.info("[INFO] Error in Map put");
			e.printStackTrace();
			return;
		}
		logger.info("[EXIT] readParameterConfig");
	}
}
