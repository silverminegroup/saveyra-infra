package com.saveyra.configserver;

import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.EnableAsync;
import com.saveyra.configserver.changemanager.BaseConfigHandler;
import com.saveyra.configserver.changemanager.FileWatch;
import com.saveyra.configserver.changemanager.ParameterConfigHandler;
import com.saveyra.configserver.utility.Declaration;

@SpringBootApplication
@EnableAsync
@EnableAutoConfiguration(exclude = { ErrorMvcAutoConfiguration.class })
@ComponentScan("com.saveyra.configserver")
@PropertySource(value="classpath:application.properties")
public class ConfigserverApplication extends SpringBootServletInitializer implements CommandLineRunner {

	private static final Logger logger = LoggerFactory.getLogger(ConfigserverApplication.class);

	public static void main(String[] args) {
		logger.info("[START] Starting Application");
		SpringApplication.run(ConfigserverApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void doSomethingAfterStartup() {
		logger.info("[START] Starting File Watcher Service Configuration");
		File jarPath = new File(
				ConfigserverApplication.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		String propertiesPath = jarPath.getParentFile().getAbsolutePath();
		Declaration.setJAR_PATH(propertiesPath);
		String jarParent = jarPath.getParentFile().getParentFile().getParentFile().getAbsolutePath();
		Declaration.setJAR_PARENT(jarParent);		
		// Update Base Configuration from Folder
		BaseConfigHandler bcHandler = new BaseConfigHandler();
		bcHandler.readAllBaseConfig();
		// Update Platform configuration from folder
		ParameterConfigHandler pcHandler = new ParameterConfigHandler();
		pcHandler.readAllConfgurations();	
		logger.info("[END] File Watcher Service Configuration Complete");
	}

	@Override
	public void run(String... args) throws Exception {
		File jarPath = new File(
				ConfigserverApplication.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		String propertiesPath = jarPath.getParentFile().getParentFile().getParentFile().getAbsolutePath();
		String baseConfigPath = propertiesPath + Declaration.getBaseConfigPath();
		String modelConfigPath = propertiesPath + Declaration.getPlatformConfigPath();
		// Start FileWatcher Service
		if(new File(baseConfigPath).exists()) {
			new Thread(new FileWatch(baseConfigPath), "watcher-service-basic").start();
		}else {
			logger.error("[ERROR] FILE WATCHER SERVICE CANNOT START for "+baseConfigPath+" . Please ensure JavaConfigurations folder is available");
		}	
		if(new File(modelConfigPath).exists()) {
			new Thread(new FileWatch(modelConfigPath), "watcher-service-platform")
			.start();
		}else {
			logger.error("[ERROR] FILE WATCHER SERVICE CANNOT START for "+modelConfigPath+". Please ensure JavaConfigurations folder is available");
		}
				
	}
}
