package com.saveyra.configserver.controller;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.saveyra.configserver.changemanager.BaseConfigHandler;
import com.saveyra.configserver.utility.Declaration;
import com.saveyra.configserver.utility.FileAdd;
import com.saveyra.configserver.utility.FileUpdate;
import com.saveyra.configserver.utility.ResolveJson;
import com.saveyra.configserver.utility.ResolveJsonV3;
import com.saveyra.configserver.utility.configuration;
import com.saveyra.configserver.utility.configurationV3;

@RestController("/")
@EnableAutoConfiguration
@PropertySource(value = { "classpath:application.properties" })
public class ConfigServerController {

	private static final Logger logger = LoggerFactory.getLogger(ConfigServerController.class);

	@CrossOrigin
	@RequestMapping(value = "getConfig", method = RequestMethod.GET)
	public ResponseEntity<String> getConfiguration(@Valid @RequestParam(value = "manufacturer") String manufacturer,
			@RequestParam(value = "model") String model, @RequestParam(value = "platform") String platform,
			@RequestParam(value = "baseversion") String baseversion,
			@RequestHeader(name = "CLIENTINFO", required = false) String clientInfo) {
		String fileKey = manufacturer + "_" + model + "_" + platform + "_config";

		logger.info("[Enter] getConfig with params : " + fileKey);

		HashMap<String, Object> hMap = new HashMap<String, Object>();
		hMap = (HashMap<String, Object>) Declaration.getResolvedJsonMap();
		if (hMap.get(baseversion) == null) {
			logger.info("[INFO] Error in version number -- null response");
			HttpHeaders respHeader = new HttpHeaders();
			respHeader.set("Content-Type", "application/json");
			ResponseEntity<String> resp = new ResponseEntity<String>("{\"message\":\"Base Config Not Found\"}",
					respHeader, HttpStatus.OK);
			return resp;
		}
		int versionId = 1;
		try {

			versionId = Integer.parseInt(baseversion.toUpperCase().split("V")[1]);
			logger.info(" version number {}", versionId);
		} catch (NumberFormatException nfe) {
			// taken default value

		}
		if (versionId > 2) {

			// if (baseversion.equalsIgnoreCase("v3")) {
			Object obj = hMap.get(baseversion);
			ResolveJsonV3 tempRes = new ResolveJsonV3(((ResolveJsonV3) obj).getConfiguration());
			if (tempRes.getConfiguration() == null) {
				logger.info("[INFO] Error in version number -- null config");
				HttpHeaders respHeader = new HttpHeaders();
				respHeader.set("Content-Type", "application/json");
				ResponseEntity<String> resp = new ResponseEntity<String>("{\"message\":\"Base Config Not Found\"}",
						respHeader, HttpStatus.OK);
				return resp;
			}

			Gson gson = new Gson();
			if (Declaration.getConfigJsonMap().get(fileKey) != null) {
				logger.info("Entered into first");
				configurationV3 cnf = new configurationV3();
				cnf = (configurationV3) tempRes.getConfiguration();
				logger.info("cnf1 info {}", cnf.toString());
				for (String s : Declaration.getConfigJsonMap().get(fileKey).keySet()) {
					java.lang.reflect.Method method = null;
					try {
						method = cnf.getClass().getMethod("set" + s, String.class);
					} catch (SecurityException e) {
						e.printStackTrace();
					} catch (NoSuchMethodException e) {
						e.printStackTrace();
					}

					try {
						method.invoke(tempRes.getConfiguration(), Declaration.getConfigJsonMap().get(fileKey).get(s));
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}

					tempRes.setConfiguration(cnf);

				}
				HttpHeaders respHeader = new HttpHeaders();
				respHeader.set("Content-Type", "application/json");
				ResponseEntity<String> resp = new ResponseEntity<String>(gson.toJson(tempRes), respHeader,
						HttpStatus.OK);
				logger.info("[Leave] getConfig with updated JSON");
				return resp;
			} else {

				HttpHeaders respHeader = new HttpHeaders();
				respHeader.set("Content-Type", "application/json");

				// Check Client Version and Remove

				float versionNumber = 0.0f;
				logger.info("ClientVers " + clientInfo);
				if (clientInfo != null) {
					String[] client = clientInfo.split("\\;");
					String clientVersion = "";
					String version = "";

					if (client != null && client.length > 2) {
						logger.info("ClientVers " + client);
						if (platform.equalsIgnoreCase("IOS")) {
							clientVersion = client[1];

							String versionSplit[] = clientVersion.split("\\.");
							version = versionSplit[0] + "." + versionSplit[1];
							versionNumber = Float.parseFloat(version);
							logger.info("versionNumber1 " + versionNumber);
						} else {
							if (client[1].toLowerCase().contains("staging"))
								platform = "ANDROIDSTAGING";
							clientVersion = client[2];
							versionNumber = Float.parseFloat(clientVersion);
						}
					}
				}
				BaseConfigHandler bcHandler = new BaseConfigHandler();
				bcHandler.readAllBaseConfig();
				Object obj1 = Declaration.getResolvedJsonMapBackUp().get(baseversion);
				ResolveJsonV3 res = new ResolveJsonV3(((ResolveJsonV3) obj1).getConfiguration());
				configurationV3 configuration = res.getConfiguration();
				logger.info("VERSion " + versionNumber);
				logger.info("PLAtform " + platform);
				if ((versionNumber < 2.5 && platform.equalsIgnoreCase("IOS"))
						|| (versionNumber < 47.0 && platform.equalsIgnoreCase("ANDROID"))
						|| (versionNumber < 269.0 && platform.equalsIgnoreCase("ANDROIDSTAGING"))) {

					List<Map<String, String>> languages = configuration.getLanguages();
					Integer Langsizediff = languages.size() - 9;
					for (int i = 0; i < Langsizediff; i++) {
						languages.remove(languages.size() - 1);
					}

					res.setConfiguration(configuration);
					ResponseEntity<String> resp = new ResponseEntity<String>(gson.toJson(res), respHeader,
							HttpStatus.OK);
					logger.info("Entered into second");
					return resp;
				} else if (((versionNumber == 2.5) && platform.equalsIgnoreCase("IOS"))
						|| ((versionNumber >= 47.0 && versionNumber <= 53.0) && platform.equalsIgnoreCase("ANDROID"))
						|| ((versionNumber >= 269 && versionNumber <= 282 )&& platform.equalsIgnoreCase("ANDROIDSTAGING"))) {
					logger.info("Entered into second1");
					List<Map<String, String>> languages = configuration.getLanguages();

					Integer Langsizediff = languages.size() - 10;
					for (int i = 0; i < Langsizediff; i++) {
						languages.remove(languages.size() - 1);
					}

					res.setConfiguration(configuration);
					ResponseEntity<String> resp = new ResponseEntity<String>(gson.toJson(res), respHeader,
							HttpStatus.OK);
					logger.info("Entered into third");
					return resp;

				}

				else if (((versionNumber == 2.6f)  && platform.equalsIgnoreCase("IOS") )
						|| (( versionNumber >= 55.0 && versionNumber <= 55.0) && platform.equalsIgnoreCase("ANDROID" ))
						||(( versionNumber >= 282 && versionNumber <= 287) && platform.equalsIgnoreCase("ANDROIDSTAGING" ) )){
					logger.info("Entered into forth");
					List<Map<String, String>> languages = configuration.getLanguages();

					Integer Langsizediff = languages.size() - 12;
					for (int i = 0; i < Langsizediff; i++) {
						languages.remove(languages.size() - 1);
					}

					res.setConfiguration(configuration);
					ResponseEntity<String> resp = new ResponseEntity<String>(gson.toJson(res), respHeader,
							HttpStatus.OK);
					return resp;

				}
				//Remove of Malayalam & Marathi
				else if (((versionNumber >= 2.7f && versionNumber <= 2.8)  && platform.equalsIgnoreCase("IOS") )
						|| (( versionNumber >= 56.0 && versionNumber <= 59.0) && platform.equalsIgnoreCase("ANDROID" ))
						||(( versionNumber >= 288 && versionNumber <= 289) && platform.equalsIgnoreCase("ANDROIDSTAGING" ) )){
					logger.info("Entered into forth");
					List<Map<String, String>> languages = configuration.getLanguages();

					Integer Langsizediff = languages.size() - 13;
					for (int i = 0; i < Langsizediff; i++) {
						languages.remove(languages.size() - 1);
					}

					res.setConfiguration(configuration);
					ResponseEntity<String> resp = new ResponseEntity<String>(gson.toJson(res), respHeader,
							HttpStatus.OK);
					return resp;

				}
				//Remove of assam and nepali
				else if (((versionNumber == 3.0f )  && platform.equalsIgnoreCase("IOS") )
						|| (( versionNumber >= 60.0 && versionNumber <= 61.0) && platform.equalsIgnoreCase("ANDROID" ))
						||(( versionNumber >= 290 && versionNumber <= 294) && platform.equalsIgnoreCase("ANDROIDSTAGING" ) )){
					logger.info("Entered into forth");
					List<Map<String, String>> languages = configuration.getLanguages();

					Integer Langsizediff = languages.size() - 15;
					for (int i = 0; i < Langsizediff; i++) {
						languages.remove(languages.size() - 1);
					}

					res.setConfiguration(configuration);
					ResponseEntity<String> resp = new ResponseEntity<String>(gson.toJson(res), respHeader,
							HttpStatus.OK);
					return resp;

				}	
				//Remove of Dogiri and Konkani and santhali
				else if (((versionNumber == 3.1f )  && platform.equalsIgnoreCase("IOS") )
										|| (( versionNumber >= 62.0 && versionNumber <= 62.0) && platform.equalsIgnoreCase("ANDROID" ))
										||(( versionNumber >= 295 && versionNumber <= 297) && platform.equalsIgnoreCase("ANDROIDSTAGING" ) )){
									logger.info("Entered into fifth");
									List<Map<String, String>> languages = configuration.getLanguages();
									logger.info("Entered into fifth"+languages.size());
									Integer Langsizediff = languages.size() - 17;
									for (int i = 0; i < Langsizediff; i++) {
										languages.remove(languages.size() - 1);
									}

									res.setConfiguration(configuration);
									ResponseEntity<String> resp = new ResponseEntity<String>(gson.toJson(res), respHeader,
											HttpStatus.OK);
									return resp;

								}			
				
				// BaseConfigHandler bcHandler = new BaseConfigHandler();
				bcHandler.readAllBaseConfig();
				ResponseEntity<String> resp = new ResponseEntity<String>(
						gson.toJson(Declaration.getResolvedJsonMapBackUp().get(baseversion)), respHeader,
						HttpStatus.OK);
				return resp;

			}
			
			
		} else {
			ResolveJson tempRes = new ResolveJson(((ResolveJson) hMap.get(baseversion)).getConfiguration());

			if (tempRes.getConfiguration() == null) {
				logger.info("[INFO] Error in version number");
				HttpHeaders respHeader = new HttpHeaders();
				respHeader.set("Content-Type", "application/json");
				ResponseEntity<String> resp = new ResponseEntity<String>("{\"message\":\"Base Config Not Found\"}",
						respHeader, HttpStatus.OK);
				return resp;
			}

			Gson gson = new Gson();
			if (Declaration.getConfigJsonMap().get(fileKey) != null) {
				configuration cnf = new configuration();
				cnf = (configuration) tempRes.getConfiguration();
				for (String s : Declaration.getConfigJsonMap().get(fileKey).keySet()) {
					java.lang.reflect.Method method = null;
					try {
						method = cnf.getClass().getMethod("set" + s, String.class);
					} catch (SecurityException e) {
						e.printStackTrace();
					} catch (NoSuchMethodException e) {
						e.printStackTrace();
					}
					try {
						method.invoke(tempRes.getConfiguration(), Declaration.getConfigJsonMap().get(fileKey).get(s));
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
					tempRes.setConfiguration(cnf);
				}
				HttpHeaders respHeader = new HttpHeaders();
				respHeader.set("Content-Type", "application/json");
				ResponseEntity<String> resp = new ResponseEntity<String>(gson.toJson(tempRes), respHeader,
						HttpStatus.OK);
				logger.info("[Leave] getConfig with updated JSON");
				return resp;
			} else {
				HttpHeaders respHeader = new HttpHeaders();
				respHeader.set("Content-Type", "application/json");
				ResponseEntity<String> resp = new ResponseEntity<String>(
						gson.toJson(Declaration.getResolvedJsonMapBackUp().get(baseversion)), respHeader,
						HttpStatus.OK);
				logger.info("[Leave] getConfig with base JSON");
				return resp;
			}
		}

	}

	@CrossOrigin
	@PostMapping("/updateConfig")
	public String updateConfiguration(@Valid @RequestBody String json) throws IOException {
		logger.info("\n[INFO] Enter updateConfiguration , Request : \n" + json);
		ObjectMapper mapper = new ObjectMapper();
		Map nMap = mapper.readValue(json, Map.class);
		// String type = nMap.get("type").toString(); // BaseConfig - ModelConfig
		String version = nMap.get("version").toString(); // v1 - v2 - ..
		String key = nMap.get("key").toString(); // String key
		String value = nMap.get("value").toString(); // String value
		Gson gson = new Gson();
		String returnString = "";
		/*
		 * System.out.println("Key : "+version);
		 * 
		 * for(String s:Declaration.getResolvedJsonMap().keySet()) {
		 * System.out.println(s); }
		 */

		if (Declaration.getResolvedJsonMap().containsKey(version)) {
			if (version.equalsIgnoreCase("v3") || version.equalsIgnoreCase("v4")) {
				// ResolveJsonV3 rj = (ResolveJsonV3)
				// Declaration.getResolvedJsonMap().get(version);

				HashMap<String, Object> hMap = new HashMap<String, Object>();
				hMap = (HashMap<String, Object>) Declaration.getResolvedJsonMap();

				Object objRJ = hMap.get(version);
				ResolveJsonV3 tempRes = new ResolveJsonV3(((ResolveJsonV3) objRJ).getConfiguration());

				if (tempRes.getConfiguration() == null) {
					returnString = "Version :" + version + ", Not a valid configuration version";
					return gson.toJson(returnString);
				}
				Field[] fields = configuration.class.getDeclaredFields();
				// System.out.println("Fields Length : "+fields.length);
				for (int i = 0; i < fields.length; i++) {
					// System.out.println("Field :"+fields[i].getName());
					if (fields[i].getName().equals(key)) {
						// System.out.println("Field Inside :"+fields[i].getName());
						try {
							configurationV3 cf = tempRes.getConfiguration();
							Object obj = cf.getClass().getDeclaredField(key);
							cf.getClass().getDeclaredField(key).set(cf, value);
							tempRes.setConfiguration(cf);
							Declaration.getResolvedJsonMap().put(version, tempRes);
							Declaration.getResolvedJsonMapBackUp().put(version, tempRes);
							FileUpdate fu = new FileUpdate();
							fu.handleFileUpdate(version, key, value);

							returnString = "successfully updated Key :" + key + " with Value : " + value;
							logger.info("[LEAVE] updateConfiguration");
							return gson.toJson(returnString);
						} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException
								| SecurityException e) {
							logger.error("[ERROR] IllegalArgumentException");
							e.printStackTrace();
						}

					}
				}
				returnString = "Key :" + key + " Not a valid configuration parameter";
				logger.info("[LEAVE] updateConfiguration");
				return gson.toJson(returnString);
			}

			else {

				ResolveJson rj = (ResolveJson) Declaration.getResolvedJsonMap().get(version);

				if (rj == null) {
					returnString = "Version :" + version + ", Not a valid configuration version";
					return gson.toJson(returnString);
				}
				Field[] fields = configuration.class.getDeclaredFields();
				// System.out.println("Fields Length : "+fields.length);
				for (int i = 0; i < fields.length; i++) {
					// System.out.println("Field :"+fields[i].getName());
					if (fields[i].getName().equals(key)) {
						// System.out.println("Field Inside :"+fields[i].getName());
						try {
							configuration cf = rj.getConfiguration();
							Object obj = cf.getClass().getDeclaredField(key);
							cf.getClass().getDeclaredField(key).set(cf, value);
							rj.setConfiguration(cf);
							Declaration.getResolvedJsonMap().put(version, rj);
							Declaration.getResolvedJsonMapBackUp().put(version, rj);
							FileUpdate fu = new FileUpdate();
							fu.handleFileUpdate(version, key, value);

							returnString = "successfully updated Key :" + key + " with Value : " + value;
							logger.info("[LEAVE] updateConfiguration");
							return gson.toJson(returnString);
						} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException
								| SecurityException e) {
							logger.error("[ERROR] IllegalArgumentException");
							e.printStackTrace();
						}

					}
				}
				returnString = "Key :" + key + " Not a valid configuration parameter";
				logger.info("[LEAVE] updateConfiguration");
				return gson.toJson(returnString);
			}

		} else {
			returnString = "Version :" + version + ", Not a valid configuration version";
			logger.info("[LEAVE] updateConfiguration");
			return gson.toJson(returnString);

		}
	}

	@CrossOrigin
	@PostMapping("/addConfig")
	public String addConfiguration(@Valid @RequestBody String json) throws IOException {
		logger.info("\n[INFO] Enter addConfig , Request : \n" + json);
		ObjectMapper mapper = new ObjectMapper();
		Map nMap = mapper.readValue(json, Map.class);
		// String type = nMap.get("type").toString(); // BaseConfig - ModelConfig
		String version = nMap.get("version").toString(); // v1 - v2 - ..
		String key = nMap.get("key").toString(); // String key
		String value = nMap.get("value").toString(); // String value
		Gson gson = new Gson();
		String returnString = "";
		// System.out.println("Key : "+version);

		/*
		 * for(String s:Declaration.getResolvedJsonMap().keySet()) {
		 * System.out.println(s); }
		 */

		if (Declaration.getResolvedJsonMap().containsKey(version)) {
			FileAdd fa = new FileAdd();
			if (fa.handleFileAdd(version, key, value).equals("Failure")) {
				returnString = "Failed to Added Key :" + key;
				return gson.toJson(returnString);
			}
			;
			BaseConfigHandler bcHandler = new BaseConfigHandler();
			bcHandler.readAllBaseConfig();
			returnString = "Successfully Added Key :" + key;
			logger.info("[LEAVE] addConfig");
			return gson.toJson(returnString);
		} else {
			returnString = "Version :" + version + ", Not a valid configuration version";
			logger.info("[LEAVE] addConfig");
			return gson.toJson(returnString);

		}
	}
}
