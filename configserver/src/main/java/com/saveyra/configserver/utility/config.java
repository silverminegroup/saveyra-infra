package com.saveyra.configserver.utility;

import java.util.Map;

public class config {	
	
	Map<String,String> config;
	/*
	String platform_version;
	String S3_BUCKET_URL;
	String S3_BUCKET_TOKEN;
	String QA_SERVER_URL;
	String PROD_SERVER_URL;
	
	
	public config(String platform_version, String s3_BUCKET_URL, String s3_BUCKET_TOKEN, String qA_SERVER_URL,
			String pROD_SERVER_URL) {
		super();
		this.platform_version = platform_version;
		S3_BUCKET_URL = s3_BUCKET_URL;
		S3_BUCKET_TOKEN = s3_BUCKET_TOKEN;
		QA_SERVER_URL = qA_SERVER_URL;
		PROD_SERVER_URL = pROD_SERVER_URL;
	}
	public String getPlatform_version() {
		return platform_version;
	}
	public void setPlatform_version(String platform_version) {
		this.platform_version = platform_version;
	}
	public String getS3_BUCKET_URL() {
		return S3_BUCKET_URL;
	}
	public void setS3_BUCKET_URL(String s3_BUCKET_URL) {
		S3_BUCKET_URL = s3_BUCKET_URL;
	}
	public String getS3_BUCKET_TOKEN() {
		return S3_BUCKET_TOKEN;
	}
	public void setS3_BUCKET_TOKEN(String s3_BUCKET_TOKEN) {
		S3_BUCKET_TOKEN = s3_BUCKET_TOKEN;
	}
	public String getQA_SERVER_URL() {
		return QA_SERVER_URL;
	}
	public void setQA_SERVER_URL(String qA_SERVER_URL) {
		QA_SERVER_URL = qA_SERVER_URL;
	}
	public String getPROD_SERVER_URL() {
		return PROD_SERVER_URL;
	}
	public void setPROD_SERVER_URL(String pROD_SERVER_URL) {
		PROD_SERVER_URL = pROD_SERVER_URL;
	}
	*/

	public config(Map<String, String> config) {
		super();
		this.config = config;
	}

	public Map<String, String> getConfig() {
		return config;
	}

	public void setConfig(Map<String, String> config) {
		this.config = config;
	}
	
	
}
