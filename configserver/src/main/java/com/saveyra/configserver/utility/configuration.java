package com.saveyra.configserver.utility;

import java.util.List;
import java.util.Map;

public class configuration {
	private boolean SNAPCHAT_LOGIN;
	public String S3_BUCKET_URL;
	public String S3_BUCKET_TOKEN;
	public String QA_SERVER_URL;
	public String PROD_SERVER_URL;
	public String DEV_SERVER_URL;
	public String ANALYTICS_URL;
	public String CRASHLYTICS_URL;
	public String MAX_STICKERS_DEVICE;
	public String MAX_AVATARS_DEVICE;
	public String SERVER_REFRESH_INTERVAL;
	public String FAQ_URL;
	public String TERMS_URL;
	public String PRIVACY_URL;
	public String AVATAR_SUPPORT_ENABLED;
	public String NOTIFICATION_SUPPORT_ENABLED;
	public String STICKER_SHARING_APPS;
	public String NUMBER_OF_STICKERS;
	public String URL_FOR_STICKER_SHARE;
	public String SAVE_STICKER_TO_GALLERY;
	public String NUMBER_OF_RECENT_STICKERS;
	public String KB_NUMBER_OF_RECENT_STICKERS;
	public String MANDATORY_UPGRADE;
	public String OPTIONAL_UPGRADE;
	public String CLIENT_VERSION;
	public List<String> languages;
	public String applestore;
	public String instagram;
	public String facebook;
	public String twitter;
	public String playstore;
	public com.saveyra.configserver.utility.extended.splashscreen splashscreen;
	public Map<String,List<Map<String , List<String>>>> orderlist;
	public String dateTime;	
	public String iosMinVersion;
	public String iosMaxVersion;
	public String androidMinVersion;
	public String androidMaxVersion;
	public String gBoardRefreshInterval;
	public String gboardIconUrl;
	public String imageResolution;
	
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	
	public String getImageResolution() {
		return imageResolution;
	}
	public void setImageResolution(String imageResolution) {
		this.imageResolution = imageResolution;
	}
	public String getS3_BUCKET_URL() {
		return S3_BUCKET_URL;
	}
	public void setS3_BUCKET_URL(String s3_BUCKET_URL) {
		S3_BUCKET_URL = s3_BUCKET_URL;
	}
	public String getS3_BUCKET_TOKEN() {
		return S3_BUCKET_TOKEN;
	}
	public void setS3_BUCKET_TOKEN(String s3_BUCKET_TOKEN) {
		S3_BUCKET_TOKEN = s3_BUCKET_TOKEN;
	}
	public String getQA_SERVER_URL() {
		return QA_SERVER_URL;
	}
	public void setQA_SERVER_URL(String qA_SERVER_URL) {
		QA_SERVER_URL = qA_SERVER_URL;
	}
	public String getPROD_SERVER_URL() {
		return PROD_SERVER_URL;
	}
	public void setPROD_SERVER_URL(String pROD_SERVER_URL) {
		PROD_SERVER_URL = pROD_SERVER_URL;
	}
	public String getDEV_SERVER_URL() {
		return DEV_SERVER_URL;
	}
	public void setDEV_SERVER_URL(String dEV_SERVER_URL) {
		DEV_SERVER_URL = dEV_SERVER_URL;
	}
	public String getANALYTICS_URL() {
		return ANALYTICS_URL;
	}
	public void setANALYTICS_URL(String aNALYTICS_URL) {
		ANALYTICS_URL = aNALYTICS_URL;
	}
	public String getCRASHLYTICS_URL() {
		return CRASHLYTICS_URL;
	}
	public void setCRASHLYTICS_URL(String cRASHLYTICS_URL) {
		CRASHLYTICS_URL = cRASHLYTICS_URL;
	}
	public String getMAX_STICKERS_DEVICE() {
		return MAX_STICKERS_DEVICE;
	}
	public void setMAX_STICKERS_DEVICE(String mAX_STICKERS_DEVICE) {
		MAX_STICKERS_DEVICE = mAX_STICKERS_DEVICE;
	}
	public String getMAX_AVATARS_DEVICE() {
		return MAX_AVATARS_DEVICE;
	}
	public void setMAX_AVATARS_DEVICE(String mAX_AVATARS_DEVICE) {
		MAX_AVATARS_DEVICE = mAX_AVATARS_DEVICE;
	}
	public String getSERVER_REFRESH_INTERVAL() {
		return SERVER_REFRESH_INTERVAL;
	}
	public void setSERVER_REFRESH_INTERVAL(String sERVER_REFRESH_INTERVAL) {
		SERVER_REFRESH_INTERVAL = sERVER_REFRESH_INTERVAL;
	}
	public String getFAQ_URL() {
		return FAQ_URL;
	}
	public void setFAQ_URL(String fAQ_URL) {
		FAQ_URL = fAQ_URL;
	}
	public String getTERMS_URL() {
		return TERMS_URL;
	}
	public void setTERMS_URL(String tERMS_URL) {
		TERMS_URL = tERMS_URL;
	}
	public String getAVATAR_SUPPORT_ENABLED() {
		return AVATAR_SUPPORT_ENABLED;
	}
	public void setAVATAR_SUPPORT_ENABLED(String aVATAR_SUPPORT_ENABLED) {
		AVATAR_SUPPORT_ENABLED = aVATAR_SUPPORT_ENABLED;
	}
	public String getNOTIFICATION_SUPPORT_ENABLED() {
		return NOTIFICATION_SUPPORT_ENABLED;
	}
	public void setNOTIFICATION_SUPPORT_ENABLED(String nOTIFICATION_SUPPORT_ENABLED) {
		NOTIFICATION_SUPPORT_ENABLED = nOTIFICATION_SUPPORT_ENABLED;
	}
	public String getSTICKER_SHARING_APPS() {
		return STICKER_SHARING_APPS;
	}
	public void setSTICKER_SHARING_APPS(String sTICKER_SHARING_APPS) {
		STICKER_SHARING_APPS = sTICKER_SHARING_APPS;
	}
	public String getNUMBER_OF_STICKERS() {
		return NUMBER_OF_STICKERS;
	}
	public void setNUMBER_OF_STICKERS(String nUMBER_OF_STICKERS) {
		NUMBER_OF_STICKERS = nUMBER_OF_STICKERS;
	}
	public String getURL_FOR_STICKER_SHARE() {
		return URL_FOR_STICKER_SHARE;
	}
	public void setURL_FOR_STICKER_SHARE(String uRL_FOR_STICKER_SHARE) {
		URL_FOR_STICKER_SHARE = uRL_FOR_STICKER_SHARE;
	}
	public String getSAVE_STICKER_TO_GALLERY() {
		return SAVE_STICKER_TO_GALLERY;
	}
	public void setSAVE_STICKER_TO_GALLERY(String sAVE_STICKER_TO_GALLERY) {
		SAVE_STICKER_TO_GALLERY = sAVE_STICKER_TO_GALLERY;
	}
	public String getNUMBER_OF_RECENT_STICKERS() {
		return NUMBER_OF_RECENT_STICKERS;
	}
	public void setNUMBER_OF_RECENT_STICKERS(String nUMBER_OF_RECENT_STICKERS) {
		NUMBER_OF_RECENT_STICKERS = nUMBER_OF_RECENT_STICKERS;
	}
	public String getKB_NUMBER_OF_RECENT_STICKERS() {
		return KB_NUMBER_OF_RECENT_STICKERS;
	}
	public void setKB_NUMBER_OF_RECENT_STICKERS(String kB_NUMBER_OF_RECENT_STICKERS) {
		KB_NUMBER_OF_RECENT_STICKERS = kB_NUMBER_OF_RECENT_STICKERS;
	}
	public String getMANDATORY_UPGRADE() {
		return MANDATORY_UPGRADE;
	}
	public void setMANDATORY_UPGRADE(String mANDATORY_UPGRADE) {
		MANDATORY_UPGRADE = mANDATORY_UPGRADE;
	}
	public String getOPTIONAL_UPGRADE() {
		return OPTIONAL_UPGRADE;
	}
	public void setOPTIONAL_UPGRADE(String oPTIONAL_UPGRADE) {
		OPTIONAL_UPGRADE = oPTIONAL_UPGRADE;
	}
	public String getCLIENT_VERSION() {
		return CLIENT_VERSION;
	}
	public void setCLIENT_VERSION(String cLIENT_VERSION) {
		CLIENT_VERSION = cLIENT_VERSION;
	}
	public List<String> getLanguages() {
		return languages;
	}
	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}
	public String getApplestore() {
		return applestore;
	}
	public void setApplestore(String applestore) {
		this.applestore = applestore;
	}
	public String getInstagram() {
		return instagram;
	}
	public void setInstagram(String instagram) {
		this.instagram = instagram;
	}
	public String getFacebook() {
		return facebook;
	}
	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}
	public String getTwitter() {
		return twitter;
	}
	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}
	public String getPlaystore() {
		return playstore;
	}
	public void setPlaystore(String playstore) {
		this.playstore = playstore;
	}

	public String getIosMinVersion() {
		return iosMinVersion;
	}
	public void setIosMinVersion(String iosMinVersion) {
		this.iosMinVersion = iosMinVersion;
	}
	public String getIosMaxVersion() {
		return iosMaxVersion;
	}
	public void setIosMaxVersion(String iosMaxVersion) {
		this.iosMaxVersion = iosMaxVersion;
	}
	public String getAndroidMinVersion() {
		return androidMinVersion;
	}
	public void setAndroidMinVersion(String androidMinVersion) {
		this.androidMinVersion = androidMinVersion;
	}
	public String getAndroidMaxVersion() {
		return androidMaxVersion;
	}
	public void setAndroidMaxVersion(String androidMaxVersion) {
		this.androidMaxVersion = androidMaxVersion;
	}

	public Map<String, List<Map<String, List<String>>>> getOrderlist() {
		return orderlist;
	}
	public void setOrderlist(Map<String, List<Map<String, List<String>>>> orderlist) {
		this.orderlist = orderlist;
	}
	
	public configuration() {
		super();
	}
	public String getPRIVACY_URL() {
		return PRIVACY_URL;
	}
	public void setPRIVACY_URL(String pRIVACY_URL) {
		PRIVACY_URL = pRIVACY_URL;
	}
	public String getgBoardRefreshInterval() {
		return gBoardRefreshInterval;
	}
	public void setgBoardRefreshInterval(String gBoardRefreshInterval) {
		this.gBoardRefreshInterval = gBoardRefreshInterval;
	}
	public String getGboardIconUrl() {
		return gboardIconUrl;
	}
	public void setGboardIconUrl(String gboardIconUrl) {
		this.gboardIconUrl = gboardIconUrl;
	}
	public boolean isSNAPCHAT_LOGIN() {
		return SNAPCHAT_LOGIN;
	}
	public void setSNAPCHAT_LOGIN(boolean sNAPCHAT_LOGIN) {
		SNAPCHAT_LOGIN = sNAPCHAT_LOGIN;
	}
	public com.saveyra.configserver.utility.extended.splashscreen getSplashscreen() {
		return splashscreen;
	}
	public void setSplashscreen(com.saveyra.configserver.utility.extended.splashscreen splashscreen) {
		this.splashscreen = splashscreen;
	}
}
