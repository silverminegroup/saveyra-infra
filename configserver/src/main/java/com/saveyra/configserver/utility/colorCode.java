package com.saveyra.configserver.utility;

import java.util.List;

public class colorCode {
	private List<rgb> colorCode;
	
	public colorCode(List<rgb> colorCode) {
		super();
		this.colorCode = colorCode;
	}

	public List<rgb> getColorCode() {
		return colorCode;
	}

	public void setColorCode(List<rgb> colorCode) {
		this.colorCode = colorCode;
	}	
}
