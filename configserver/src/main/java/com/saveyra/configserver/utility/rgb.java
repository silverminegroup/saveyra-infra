package com.saveyra.configserver.utility;

import java.util.List;

public class rgb {
	private List<String> rgb;	

	public rgb(List<String> colorList) {
		super();
		this.rgb = colorList;
	}

	public List<String> getColorList() {
		return rgb;
	}

	public void setColorList(List<String> colorList) {
		this.rgb = colorList;
	}
} 
