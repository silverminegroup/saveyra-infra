package com.saveyra.configserver.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.saveyra.configserver.ConfigserverApplication;


public class FileUpdate {
	
	private static final Logger logger = LoggerFactory.getLogger(FileUpdate.class);
	
	public String handleFileUpdate(String version, String key, String value) {
		logger.info("[INFO] Enter handleFileUpdate , Request : " + version+" Key : "+key+" value : "+value);
		File jarPath = new File(
				ConfigserverApplication.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		String propertiesPath = jarPath.getParentFile().getParentFile().getParentFile().getAbsolutePath();
		String baseConfigPath = propertiesPath + Declaration.getBaseConfigPath();
		String filePath = baseConfigPath + "/configuration-" + version + ".json";
		File originalFile = new File(filePath);
		BufferedReader br = null;
		PrintWriter pw = null;
		String line = null;
		try {
			br = new BufferedReader(new FileReader(originalFile));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block			
			e1.printStackTrace();
			logger.error("[Error] File Not Found! Exiting handleFileUpdate");
			return "Failure";
		}
		File tempFile = new File(baseConfigPath + "/temp-" + version + ".json");
		
		try {
			pw = new PrintWriter(new FileWriter(tempFile));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			logger.error("[Error] Create File Failed ! Exiting handleFileUpdate");
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return "Failure";
		}		
		try {
			while ((line = br.readLine()) != null) {
				if (line.contains(key)) {
					if(line.contains("\",")) {
						line = line.substring(0, line.indexOf(":")) +":\""+ value + "\",";
					}else {
						line = line.substring(0, line.indexOf(":")) +":\""+ value + "\"";
					}					
				}
				pw.println(line);
				pw.flush();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error("[Error] Flush Failed ! Exiting handleFileUpdate");
			return "Failure";
		} finally {
			pw.close();
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}			
		}
		
		
		if(DeleteFile(originalFile).equals("Success")) {
			RenameFile(tempFile,originalFile);
		}	
		logger.info("[Leave] handleFileUpdate");
		return "Success";
	}

	public String DeleteFile(File originalFile) {
		if(!originalFile.delete()) {
			return "Failure";
		}
		return "Success";
	}

	public String RenameFile(File tempFile,File orignalFile) {
		if(!tempFile.renameTo(orignalFile)) {
			return "Failure";
		}else {
			return "success";
		}
	}
	
	

}
