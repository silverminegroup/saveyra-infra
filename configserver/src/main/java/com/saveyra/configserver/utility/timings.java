package com.saveyra.configserver.utility;

import java.util.List;

public class timings {
	private List<String> timeList;	

	public timings(List<String> timeList) {
		super();
		this.timeList = timeList;
	}

	public List<String> getTimeList() {
		return timeList;
	}

	public void setTimeList(List<String> timeList) {
		this.timeList = timeList;
	}
}
