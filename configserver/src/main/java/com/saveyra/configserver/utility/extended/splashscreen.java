package com.saveyra.configserver.utility.extended;

import java.util.List;

public class splashscreen {
	private List<com.saveyra.configserver.utility.splashScreen> splashScreen;
	private String screenCount;
	
	public List<com.saveyra.configserver.utility.splashScreen> getSplashScreen() {
		return splashScreen;
	}
	public void setSplashScreen(List<com.saveyra.configserver.utility.splashScreen> splashScreen) {
		this.splashScreen = splashScreen;
	}
	public String getScreenCount() {
		return screenCount;
	}
	public void setScreenCount(String screenCount) {
		this.screenCount = screenCount;
	}
	public splashscreen(List<com.saveyra.configserver.utility.splashScreen> splashScreen, String screenCount) {
		super();
		this.splashScreen = splashScreen;
		this.screenCount = screenCount;
	}		
}
