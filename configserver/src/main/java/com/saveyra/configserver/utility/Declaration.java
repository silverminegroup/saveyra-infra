package com.saveyra.configserver.utility;

import java.util.HashMap;
import java.util.Map;

public class Declaration {

	private static ResolveJson resolvedJson;
	
	private static String JAR_PATH = "";
	private static String JAR_PARENT = "";
	
	private static Map<String,Object> resolvedJsonMap = new HashMap<String,Object>();
	
	private static Map<String,Object> resolvedJsonMapBackUp = new HashMap<String,Object>();
	
	private static Map<String,Map<String,String>> configJsonMap = new HashMap<String,Map<String,String>>();
	
	private static final String PLATFORM_CONFIG_PATH = "/JavaConfigurations/ModelConfig/";
	
	private static final String BASE_CONFIG_PATH = "/JavaConfigurations/BaseConfig/";

	public static String getPlatformConfigPath() {
		return PLATFORM_CONFIG_PATH;
	}

	public ResolveJson getResolvedJson() {
		return resolvedJson;
	}

	public static void setResolvedJson(ResolveJson resolvedJson) {
		Declaration.resolvedJson = resolvedJson;
	}

	public static String getBaseConfigPath() {
		return BASE_CONFIG_PATH;
	}

	public static String getJAR_PATH() {
		return JAR_PATH;
	}

	public static void setJAR_PATH(String jAR_PATH) {
		JAR_PATH = jAR_PATH;
	}

	public static Map<String,Map<String,String>> getConfigJsonMap() {
		return configJsonMap;
	}

	public static void setConfigJsonMap(Map<String,Map<String,String>> configJsonMap) {
		Declaration.configJsonMap = configJsonMap;
	}

	public static String getJAR_PARENT() {
		return JAR_PARENT;
	}

	public static void setJAR_PARENT(String jAR_PARENT) {
		JAR_PARENT = jAR_PARENT;
	}

	public static Map<String,Object> getResolvedJsonMap() {
		return resolvedJsonMap;
	}

	public static void setResolvedJsonMap(Map<String,Object> resolvedJsonMap) {
		Declaration.resolvedJsonMap = resolvedJsonMap;
	}

	public static Map<String,Object> getResolvedJsonMapBackUp() {
		return resolvedJsonMapBackUp;
	}

	public static void setResolvedJsonMapBackUp(Map<String,Object> resolvedJsonMapBackUp) {
		Declaration.resolvedJsonMapBackUp = resolvedJsonMapBackUp;
	}
	
}
