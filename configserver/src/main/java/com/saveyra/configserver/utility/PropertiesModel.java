package com.saveyra.configserver.utility;

import java.util.List;

public class PropertiesModel {
	
	private List<config> config;

	public List<config> getConfig() {
		return config;
	}

	public void setConfig(List<config> config) {
		this.config = config;
	}

	public PropertiesModel(List<com.saveyra.configserver.utility.config> config) {
		super();
		this.config = config;
	}

}
