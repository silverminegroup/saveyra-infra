package com.saveyra.configserver.utility;

public class ResolveJson {
	private configuration configuration;

	public ResolveJson(com.saveyra.configserver.utility.configuration configuration) {
		super();
		this.configuration = configuration;
	}

	public configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(configuration configuration) {
		this.configuration = configuration;
	}	
}
