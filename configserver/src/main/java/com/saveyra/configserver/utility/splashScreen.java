package com.saveyra.configserver.utility;

import java.util.List;

public class splashScreen {
	
	private String status = null;
	private List<String> timings = null;
	private List<rgb> colorCode = null;
	private String id = null;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<String> getTimings() {
		return timings;
	}
	public void setTimings(List<String> timings) {
		this.timings = timings;
	}
	public List<rgb> getColorCode() {
		return colorCode;
	}
	public void setColorCode(List<rgb> colorCode) {
		this.colorCode = colorCode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public splashScreen(String status, List<String> timings, List<rgb> colorCode, String id) {
		super();
		this.status = status;
		this.timings = timings;
		this.colorCode = colorCode;
		this.id = id;
	}
	
	public splashScreen(List<String> timings, List<rgb> colorCode, String id) {
		super();
		this.status = "";
		this.timings = timings;
		this.colorCode = colorCode;
		this.id = id;
	}
		
	

}
