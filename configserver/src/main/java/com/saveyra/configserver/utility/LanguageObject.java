package com.saveyra.configserver.utility;

public class LanguageObject {
	private String name;
	private String displayName;
	private String code;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public LanguageObject(String name, String displayName, String code) {
		super();
		this.name = name;
		this.displayName = displayName;
		this.code = code;
	}	
}
