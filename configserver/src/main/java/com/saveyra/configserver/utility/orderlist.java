package com.saveyra.configserver.utility;

import java.util.List;
import java.util.Map;

public class orderlist {	
	private List<Map<String , List<String>>> orderlist ;



	public orderlist(List<Map<String, List<String>>> orderlist) {
		super();
		this.orderlist = orderlist;
	}

	public List<Map<String , List<String>>> getOrderlist() {
		return orderlist;
	}

	public void setOrderlist(List<Map<String , List<String>>> orderlist) {
		this.orderlist = orderlist;
	}	
}
