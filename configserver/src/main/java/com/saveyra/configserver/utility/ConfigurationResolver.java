package com.saveyra.configserver.utility;

import java.io.File;
import java.util.Properties;

public class ConfigurationResolver {
	private Properties prop;

	public ConfigurationResolver(String configFileName, boolean basicProperty) {
		super();
		String folderPath;
		if (basicProperty) {
			folderPath = "JavaConfigurations\\BaseConfig\\";
		} else {
			folderPath = "JavaConfigurations\\ModelConfig\\";
		}

		// Properties props = new Properties();
		File jarPath = new File(
				ConfigurationResolver.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		String propertiesPath = jarPath.getParent() + "\\" + folderPath + configFileName;
		System.out.println("PropertyFileLocation : " + propertiesPath);

	}

	public Properties getProp() {
		return prop;
	}
}
