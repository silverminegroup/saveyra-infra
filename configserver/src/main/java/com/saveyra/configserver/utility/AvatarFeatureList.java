package com.saveyra.configserver.utility;

public class AvatarFeatureList {
	private String avatarFaceShape;
	private String avatarSkintone;
	private String avatarHairstyle;
	private String avatarHaircolour;
	private String avatarFacialHair;
	private String avatarFacialHaircolour;
	private String avatarEyeShape;
	private String avatarEyeColour;
	private String avatarEyebrow;
	private String avatarNose;
	private String avatarMouth;
	private String avatarLipstick;
	private String avatarBodytype;
	private String avatarOutfit;
	public String getAvatarFaceShape() {
		return avatarFaceShape;
	}
	public void setAvatarFaceShape(String avatarFaceShape) {
		this.avatarFaceShape = avatarFaceShape;
	}
	public String getAvatarSkintone() {
		return avatarSkintone;
	}
	public void setAvatarSkintone(String avatarSkintone) {
		this.avatarSkintone = avatarSkintone;
	}
	public String getAvatarHairstyle() {
		return avatarHairstyle;
	}
	public void setAvatarHairstyle(String avatarHairstyle) {
		this.avatarHairstyle = avatarHairstyle;
	}
	public String getAvatarHaircolour() {
		return avatarHaircolour;
	}
	public void setAvatarHaircolour(String avatarHaircolour) {
		this.avatarHaircolour = avatarHaircolour;
	}
	public String getAvatarFacialHair() {
		return avatarFacialHair;
	}
	public void setAvatarFacialHair(String avatarFacialHair) {
		this.avatarFacialHair = avatarFacialHair;
	}
	public String getAvatarFacialHaircolour() {
		return avatarFacialHaircolour;
	}
	public void setAvatarFacialHaircolour(String avatarFacialHaircolour) {
		this.avatarFacialHaircolour = avatarFacialHaircolour;
	}
	public String getAvatarEyeShape() {
		return avatarEyeShape;
	}
	public void setAvatarEyeShape(String avatarEyeShape) {
		this.avatarEyeShape = avatarEyeShape;
	}
	public String getAvatarEyeColour() {
		return avatarEyeColour;
	}
	public void setAvatarEyeColour(String avatarEyeColour) {
		this.avatarEyeColour = avatarEyeColour;
	}
	public String getAvatarEyebrow() {
		return avatarEyebrow;
	}
	public void setAvatarEyebrow(String avatarEyebrow) {
		this.avatarEyebrow = avatarEyebrow;
	}
	public String getAvatarNose() {
		return avatarNose;
	}
	public void setAvatarNose(String avatarNose) {
		this.avatarNose = avatarNose;
	}
	public String getAvatarMouth() {
		return avatarMouth;
	}
	public void setAvatarMouth(String avatarMouth) {
		this.avatarMouth = avatarMouth;
	}
	public String getAvatarLipstick() {
		return avatarLipstick;
	}
	public void setAvatarLipstick(String avatarLipstick) {
		this.avatarLipstick = avatarLipstick;
	}
	public String getAvatarBodytype() {
		return avatarBodytype;
	}
	public void setAvatarBodytype(String avatarBodytype) {
		this.avatarBodytype = avatarBodytype;
	}
	public String getAvatarOutfit() {
		return avatarOutfit;
	}
	public void setAvatarOutfit(String avatarOutfit) {
		this.avatarOutfit = avatarOutfit;
	}
	public AvatarFeatureList(String avatarFaceShape, String avatarSkintone, String avatarHairstyle,
			String avatarHaircolour, String avatarFacialHair, String avatarFacialHaircolour, String avatarEyeShape,
			String avatarEyeColour, String avatarEyebrow, String avatarNose, String avatarMouth, String avatarLipstick,
			String avatarBodytype, String avatarOutfit) {
		super();
		this.avatarFaceShape = avatarFaceShape;
		this.avatarSkintone = avatarSkintone;
		this.avatarHairstyle = avatarHairstyle;
		this.avatarHaircolour = avatarHaircolour;
		this.avatarFacialHair = avatarFacialHair;
		this.avatarFacialHaircolour = avatarFacialHaircolour;
		this.avatarEyeShape = avatarEyeShape;
		this.avatarEyeColour = avatarEyeColour;
		this.avatarEyebrow = avatarEyebrow;
		this.avatarNose = avatarNose;
		this.avatarMouth = avatarMouth;
		this.avatarLipstick = avatarLipstick;
		this.avatarBodytype = avatarBodytype;
		this.avatarOutfit = avatarOutfit;
	}	
}
