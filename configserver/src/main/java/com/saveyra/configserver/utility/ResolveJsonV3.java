package com.saveyra.configserver.utility;

public class ResolveJsonV3 {	
	private configurationV3 configuration;

	public configurationV3 getConfiguration() {
		return configuration;
	}

	public void setConfiguration(configurationV3 configuration) {
		this.configuration = configuration;
	}

	public ResolveJsonV3(configurationV3 configuration) {
		super();
		this.configuration = configuration;
	}	
}
