package com.saveyra.configserver.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.saveyra.configserver.ConfigserverApplication;

public class FileAdd {
	public String handleFileAdd(String version, String key, String value) {
		File jarPath = new File(
				ConfigserverApplication.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		String propertiesPath = jarPath.getParentFile().getParentFile().getParentFile().getAbsolutePath();
		String baseConfigPath = propertiesPath + Declaration.getBaseConfigPath();
		String filePath = baseConfigPath + "/configuration-" + version + ".json";
		File originalFile = new File(filePath);
		BufferedReader br = null;
		PrintWriter pw = null;
		String line = null;
		try {
			br = new BufferedReader(new FileReader(originalFile));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block			
			e1.printStackTrace();
			return "Failure";
		}
		File tempFile = new File(baseConfigPath + "/temp-" + version + ".json");
		
		try {
			pw = new PrintWriter(new FileWriter(tempFile));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return "Failure";
		}		
		try {
			int eol = 0;
			while ((line = br.readLine()) != null) {
								
				if (!line.contains("},")&&line.contains("}") && eol<2) {			
					eol++;					
					if(eol==2) {
						pw.println(",\""+key+"\":\""+value+"\"");
						pw.flush();
						pw.println("}");
						pw.flush();
						pw.println("}");
						pw.flush();
						eol =0;
						break;
					}else {
						continue;
						/*pw.println("},");
						pw.flush();
						pw.println(line);
						pw.flush();
						eol = 0;*/
					}
				}else if(eol>0) {
					eol=0;
					pw.println("}");
					pw.flush();
					pw.println(line);
					pw.flush();
					continue;					
				}
				
				if (line.contains(key)) {
					System.out.println("Key Exists");
					pw.close();
					if(br != null) {
						try {
							br.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					DeleteFile(tempFile);
					return "Failure";
				}				
				pw.println(line);
				pw.flush();
			}			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Failure";
		} finally {
			if(pw!=null) {
				pw.close();
			}			
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}			
		}
		
		
		if(DeleteFile(originalFile).equals("Success")) {
			RenameFile(tempFile,originalFile);
		}		
		return "Success";
	}

	public String DeleteFile(File originalFile) {
		if(!originalFile.delete()) {
			return "Failure";
		}
		return "Success";
	}

	public String RenameFile(File tempFile,File orignalFile) {
		if(!tempFile.renameTo(orignalFile)) {
			return "Failure";
		}else {
			return "success";
		}
	}
	
	

}
