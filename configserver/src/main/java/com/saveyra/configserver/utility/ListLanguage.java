package com.saveyra.configserver.utility;

import java.util.ArrayList;

public class ListLanguage {
	private ArrayList<LanguageObject> languages;

	public ArrayList<LanguageObject> getLanguages() {
		return languages;
	}

	public void setLanguages(ArrayList<LanguageObject> languages) {
		this.languages = languages;
	}

	public ListLanguage(ArrayList<LanguageObject> languages) {
		super();
		this.languages = languages;
	}
	
	
}
