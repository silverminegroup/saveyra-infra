#Description
How to build user-session-report project:
1. Build UserSessionReport project - mvn clean install
2. Generates user-session-report-0.0.1-SNAPSHOT.jar under the target folder
3. Follow steps from README.md in src/main/resources folder of this project to run script.