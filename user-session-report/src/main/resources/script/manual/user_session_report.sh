#!/usr/bin/bash
yesterday=$(date -d"-1day" +"%m_%d_%Y")
source_file=messageBroker.xlsx
target_file=user_session_$yesterday.csv
java -Xmx4096m -jar ReadAndWriteDataIntoExcel.jar $source_file $target_file > report.log
scp $target_file ubuntu@18.205.113.199:/home/ubuntu/
rm -rf $source_file $target_file

