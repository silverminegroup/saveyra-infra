Description:
This script runs every morning 11 AM of system timezone. It captures previous day events and calculate user session count, then writes into csv file.
It has separate database shiva_sessions_report, where we store last session count for an applicationId as marketing team needs it.

Set up user session report script:
1. Create a directory in a server where you want to run the script - mkdir user_session_report
2. Copy user_session_report.sh and ReadAndWriteDataIntoExcel.jar files from src/main/resources/script/manual folder of the project into a user_session_report directory.
3. cd user_session_report
4. chmod +x user_session_report.sh
5. cd /user/bin
6. crontab -e 
7. add - * 11 * * * bash <path>/user_session_report/user_session_report.sh
8. SHIFT+ESC
9. SHIFT+:+wq
