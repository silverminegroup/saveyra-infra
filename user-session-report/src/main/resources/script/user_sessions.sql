CREATE DATABASE IF NOT EXISTS user_sessions_report;

use user_sessions_report;

create table user_sessions(id int auto_increment not null, application_id varchar(255), session_id int not null, start_time datetime, end_time datetime, event_name varchar(1024), event_count int, primary key(id));

create index application_id_idx on user_sessions(application_id);
