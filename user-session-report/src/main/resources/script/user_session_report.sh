#!/usr/bin/sh
docker cp user-session-report-0.0.1-SNAPSHOT.jar tomcat-server:/home/user-session-report-0.0.1-SNAPSHOT.jar
docker exec -i tomcat-server bash -c "java -Xmx4096m -jar /home/user-session-report-0.0.1-SNAPSHOT.jar"
yesterday=$(date -d"-1day" +"%m_%d_%Y")
target_file=user_session_report_$yesterday.csv
docker cp tomcat-server:/tmp/user_session_report.csv $target_file
docker exec -i tomcat-server bash -c "rm -rf /tmp/user_session_report.csv"
scp $target_file ubuntu@18.205.113.199:/home/ubuntu/
rm -rf $target_file