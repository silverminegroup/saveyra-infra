package com.saveyra.usersession.report.s3;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.codec.binary.Base64;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.BucketLifecycleConfiguration;
import com.amazonaws.services.s3.model.BucketVersioningConfiguration;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ListVersionsRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3VersionSummary;
import com.amazonaws.services.s3.model.SetBucketVersioningConfigurationRequest;
import com.amazonaws.services.s3.model.VersionListing;
import com.amazonaws.services.s3.model.lifecycle.LifecycleAndOperator;
import com.amazonaws.services.s3.model.lifecycle.LifecycleFilter;
import com.amazonaws.services.s3.model.lifecycle.LifecyclePrefixPredicate;

public class ObjectVersion {

	public static AmazonS3 awsCredentials() {
		AWSCredentials credentials = new BasicAWSCredentials(
				  "AKIA2XNGOFU6FUOTDGF4", 
				  "tLoUwqt7/PgmxUJWl+wdaqgDUQ/zlbq1B0B2QkgH"
				);
		AmazonS3 s3client = AmazonS3ClientBuilder
				  .standard()
				  .withCredentials(new AWSStaticCredentialsProvider(credentials))
				  .withRegion(Regions.AP_SOUTH_1)
				  .build();
		return s3client;
	}
	
	private static void objectTag() {
		String bucketName = "user-session-report";
		String keyName = "reports/";
        
		//createBucketVersioning(bucketName);
		
		createRule(bucketName, keyName);
		 
		fileUpload(bucketName, keyName);
		 
		latestVersionObject(bucketName, keyName);
       
	}
	private static void latestVersionObject(String bucketName, String keyName) {
		AmazonS3 s3Client = awsCredentials();
		ListVersionsRequest request = new ListVersionsRequest()
                .withBucketName(bucketName)
                .withPrefix(keyName+ "application3.properties").withMaxResults(1);
        VersionListing versionListing = s3Client.listVersions(request);
        List<S3VersionSummary> vs = versionListing.getVersionSummaries();
        
        vs.forEach(s -> System.out.println("bucketname "+s.getBucketName()+", versionId "+s.getVersionId()+", isLatest "+s.isLatest()+" lastModified "+s.getLastModified()+ ", keyName "+s.getKey()));
	}
	
	private static void createBucketVersioning(String bucketName) {
		AmazonS3 s3Client = awsCredentials();
		BucketVersioningConfiguration configuration = 
    			new BucketVersioningConfiguration().withStatus("Enabled");
        
		SetBucketVersioningConfigurationRequest setBucketVersioningConfigurationRequest = 
				new SetBucketVersioningConfigurationRequest(bucketName,configuration);
		
		s3Client.setBucketVersioningConfiguration(setBucketVersioningConfigurationRequest);
		
		// 2. Get bucket versioning configuration information.
		BucketVersioningConfiguration conf = s3Client.getBucketVersioningConfiguration(bucketName);
		 System.out.println("bucket versioning configuration status:    " + conf.getStatus());
	}
	
	public static void fileUpload(String bucketName, String keyName) {
		AmazonS3 s3client = awsCredentials();
		try {
			String csvFile = "D:\\projects\\git\\saveyra_infra_new\\saveyra-infra\\UserSessionReport\\src\\main\\java\\com\\saveyra\\usersession\\report\\s3\\application3.properties";

			//byte [] bytes = readFileToByteArray(new File(csvFile));
			//bytes = Base64.decodeBase64(Base64.encodeBase64String(bytes));
			//InputStream is = new ByteArrayInputStream(bytes);
			//ObjectMetadata metadata = new ObjectMetadata();
			//metadata.setContentLength(bytes.length);
			PutObjectRequest  putObjectRequest = new PutObjectRequest(bucketName, keyName+"application3.properties",new File(csvFile));
			putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead);
		    PutObjectResult por = s3client.putObject(putObjectRequest);
		    //por.setExpirationTimeRuleId(csvFile);
			if (por.getMetadata() != null)
				System.out.println("PutObjectResult "+por.getMetadata());
			else
				System.out.println("File is not uploaded to s3");
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Error in uploading file to aws S3");
		} finally {
			s3client.shutdown();
		}
	}
	
	private static byte[] readFileToByteArray(File file){
        FileInputStream fis = null;
        // Creating a byte array using the length of the file
        // file.length returns long which is cast to int
        byte[] bArray = new byte[(int) file.length()];
        try{
            fis = new FileInputStream(file);
            fis.read(bArray);
            fis.close();        
            
        }catch(IOException ioExp){
            ioExp.printStackTrace();
        }
        return bArray;
    }
	
	
	public static void createRule(String bucketName, String prefix) {
		// Create a rule to archive objects with the "glacierobjects/" prefix to Glacier immediately.
        try {
        	AmazonS3 s3Client = awsCredentials();
        	BucketLifecycleConfiguration configuration = s3Client.getBucketLifecycleConfiguration(bucketName);
    		if (configuration == null) {
    			configuration = new BucketLifecycleConfiguration();
    		}
    		
        	List<BucketLifecycleConfiguration.Rule> rules = new ArrayList<BucketLifecycleConfiguration.Rule>();
        	if  (configuration.getRules() == null) {
        		BucketLifecycleConfiguration.Rule rule = new BucketLifecycleConfiguration.Rule();
                rule.withFilter(new LifecycleFilter(new LifecycleAndOperator(
                        Arrays.asList(new LifecyclePrefixPredicate(prefix)))))
                .withNoncurrentVersionExpirationInDays(1)
                .withStatus(BucketLifecycleConfiguration.ENABLED);
        		rules.add(rule);
        	}

            // Add a new rule with both a prefix predicate and a tag predicate.
            configuration.setRules(rules);

            // Save the configuration.
            s3Client.setBucketLifecycleConfiguration(bucketName, configuration);

            // Verify that the configuration now has three rules.
            configuration = s3Client.getBucketLifecycleConfiguration(bucketName);
            System.out.println("found: " + configuration.getRules().size());

        } catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process 
            // it, so it returned an error response.
            e.printStackTrace();
        } catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            e.printStackTrace();
        }
	}
	
	public static void main(String[] args) throws IOException {
		objectTag();
    }
}