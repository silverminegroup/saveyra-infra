package com.saveyra.usersession.report.db.service.impl;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.cribbstechnologies.clients.mandrill.exception.RequestFailedException;
import com.cribbstechnologies.clients.mandrill.model.MandrillRecipient;
import com.saveyra.usersession.report .db.EventsData;
import com.saveyra.usersession.report.db.MessageBrokerCollection;
import com.saveyra.usersession.report.db.service.SaveyraMsgBrokerService;
import com.saveyra.usersession.report.mail.SendEmail;
import com.saveyra.usersession.report.s3.FileUpload;
import com.saveyra.usersession.report.util.TimezoneConverter;

@Service
public class SaveyraMsgBrokerServiceImpl implements SaveyraMsgBrokerService {
	private static Logger logger = LoggerFactory.getLogger(SaveyraMsgBrokerServiceImpl.class);

	@Autowired
	private MongoTemplate mongoTemplate;

	@Value("${emails}")
	private String emails;
	

	@Value("${spring.datasource.url}")
	private static String mysqlUrl;
	
	@Value("${spring.datasource.username}")
	private static String userName;
	
	@Value("${spring.datasource.password}")
	private static String password;
	
	@Value("${spring.datasource.driver-class-name}")
	private static String driverName;
	
	@Override
	public List<MessageBrokerCollection> findAll() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR, -1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		Date start = sdf.parse(sdf.format(TimezoneConverter.convert("Asia/Kolkata", "UTC", cal.getTime())));
		
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		Date end = sdf.parse(sdf.format(TimezoneConverter.convert("Asia/Kolkata", "UTC", cal.getTime())));
		
		System.out.println("start "+start+" end "+end);
		Query q = new Query();
		List<String> list = new ArrayList<>();
		list.add(null);
		list.add("");
		q.addCriteria(Criteria.where("applicationID").nin(list).andOperator(Criteria.where("dateTime").gte(start), Criteria.where("dateTime").lte(end)));
		q.with(new Sort(Sort.Direction.ASC, "applicationID").and(new Sort(Sort.Direction.ASC, "dateTime")));
		return mongoTemplate.find(q, MessageBrokerCollection.class);
	}

	@Override
	public void readFromMongoDB() throws ParseException, java.text.ParseException, FileNotFoundException, IOException {
		 List<MessageBrokerCollection> collections = findAll();
		System.out.println("readFromMongoDB()==>Entered ");
		Hashtable<String, List<EventsData>> applicationDetails = new Hashtable<String, List<EventsData>>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		int eventCount =0;
		int sessionId = 0;
		long totalmilli = 0l;
		String[] arr = {"803c5027-c78a-46b5-903b-d6b75de50c32", "37ba4723-b9d5-41c7-9744-61307b84ada3", "87b6a8cc-0625-4b22-8634-a4daee5afdaa", "fbb9539d-53fe-4eee-90f1-49cdae72f7a7",
				"8630b3bf-5727-4e58-8051-23fb1179e653", "257808be-39f6-4832-890d-37db925923c5", "16958f4e-10a5-4756-b582-bb4f17052925", "6ab96913-ba3c-4d6a-a95b-3e816a9e3c74", 
				"04e5435b-e0d4-45c9-bd68-b35671005dae", "47d7a7a0-be82-47dc-a84c-3e51227096b0", "5cb9adee-2d37-41d6-ba8e-f38ae308b0b6", "b629d7cb-aad8-4ffd-b409-bfce38ecc6b1", 
				"bb1673b9-36dc-40a2-b10c-6c4694cd0b9f"};
		List<EventsData> eventsDataList = null;
		for (MessageBrokerCollection collection: collections) {
				String applicationId = collection.getApplicationID();
				String eventName = collection.getEventName();
				String dateTime = sdf.format(collection.getDateTime());
				System.out.println("applicationId: "+applicationId+", dateCell :"+dateTime+", eventName: "+eventName);
					if (applicationId != null) {
						//ignore testing devices.
						if(Arrays.asList(arr).contains(applicationId)) {
							System.out.println("Ignored testing applicationId: "+applicationId); 
							continue;
						}
						if (applicationDetails.get(applicationId) == null) {
							EventsData result = getLastEventsDataByAppID(applicationId);
							eventsDataList = new ArrayList<EventsData>();
							sessionId =  1;
							if(result.getSessionId() > 0) {
								sessionId = result.getSessionId();
							}
							eventCount =  1;
							if(result.getEventCount() > 0) {
								eventCount = result.getEventCount();
							}
							System.out.println("applicationId: "+applicationId+", dateTime :"+dateTime+", eventCount: "+eventCount);
							Date startDate = sdf.parse(dateTime);
							Date endDate = sdf.parse(dateTime);
							if(result.getStartDate() != null) {
								long millis = startDate.getTime() - result.getStartDate().getTime();
								totalmilli = totalmilli + millis;
								System.out.println("applicationId: "+applicationId+" total time :"+totalmilli+" ms, eventCount: "+eventCount);
								eventCount = eventCount + 1;
								long MAX_DURATION = 30 * 60 * 1000;  
								if (totalmilli > MAX_DURATION) {
									sessionId = sessionId + 1;
									eventCount = 1;
									totalmilli =0;
								}
							}
							totalmilli =0;
							EventsData newEventsData = new EventsData();
							newEventsData.setSessionId(sessionId);
							newEventsData.setEventCount(eventCount);
							if (eventName != null) {
								newEventsData.setEventName(eventName);
							}
							if (dateTime != null) {
								newEventsData.setStartDate(startDate);
								newEventsData.setEndDate(endDate);
							}
							eventsDataList.add(newEventsData);
							applicationDetails.put(applicationId, eventsDataList);
						} else {
							eventsDataList = applicationDetails.get(applicationId);
							EventsData oldEventsData2 = eventsDataList.get(eventsDataList.size() - 1);
							if (dateTime != null) {
								oldEventsData2.setEndDate(sdf.parse(dateTime));
							}
							EventsData newEventsData = new EventsData();
							if (dateTime != null) {
								newEventsData.setStartDate(sdf.parse(dateTime));
								newEventsData.setEndDate(sdf.parse(dateTime));
							}
							long millis = newEventsData.getStartDate().getTime() - oldEventsData2.getStartDate().getTime();
							totalmilli = totalmilli + millis;
							System.out.print("applicationId: "+applicationId+" total time :"+totalmilli+" ms, eventCount: "+eventCount);
							eventCount = eventCount + 1;
							long MAX_DURATION = 30 * 60 * 1000;  
							if (totalmilli > MAX_DURATION) {
								sessionId = oldEventsData2.getSessionId() + 1;
								eventCount = 1;
								totalmilli =0;
							}
							System.out.println(", sessionId: "+sessionId);
							newEventsData.setSessionId(sessionId);
							newEventsData.setEventCount(eventCount);
							if (eventName != null) {
								newEventsData.setEventName(eventName);
							}
							eventsDataList.add(newEventsData);
							applicationDetails.put(applicationId, eventsDataList);
						}
						

			}
		}
		
		BufferedWriter bw = null;
		FileWriter fw = null;
		String outputFile = "user_session_report.csv";
		fw = new FileWriter("/tmp/"+outputFile);
		bw = new BufferedWriter(fw);
		try {
			SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
			Map<String, Map<String,  List<Integer>>> appIdAndSessions = new HashMap<String, Map<String,  List<Integer>> >();
			long totalmilli1 = 0;
			long MAX_DURATION = 30 * 60 * 1000;  
			int count = 0;
			//myExcelSheet = myExcelBook.createSheet("Report1");
			for (String appId : applicationDetails.keySet()) {
				Date previousDate = null;
				Map<String, List<Integer>> dayWiseSessions = new HashMap<String, List<Integer>>();
				List<Integer> sessions = null;
				int size = applicationDetails.get(appId).size();
				EventsData lastRecord = applicationDetails.get(appId).get(size -1 );
				int isInsert = insertRecords(appId, lastRecord.getSessionId(), lastRecord.getStartDate(), lastRecord.getEndDate(), lastRecord.getEventName(), lastRecord.getEventCount());
				if(isInsert > 1) {
					System.out.println("Inserted successfully.");
				}
				for (EventsData data : applicationDetails.get(appId)) {
					/*System.out.println("appId: " + appId + ", sessionId: " + data.getSessionId() + ", startDate: "
							+ data.getStartDate() + " endDate: " + data.getEndDate() + " eventNames: "
							+ data.getEventName() + " eventCount: " + data.getEventCount());*/
					//writeModelFirstOpenCountIntoExcel(myExcelSheet, rowInd, appId, data);
					Date currentDate = data.getStartDate();
					if(dayWiseSessions.get(s.format(data.getStartDate())) == null) {
						sessions = new ArrayList<Integer>();
						count = 1;
						sessions.add(count);
						dayWiseSessions.put(s.format(data.getStartDate()), sessions);
						appIdAndSessions.put(appId, dayWiseSessions);
						totalmilli1 = 0;
					} else {
						sessions = dayWiseSessions.get(s.format(data.getStartDate()));
						count = dayWiseSessions.get(s.format(data.getStartDate())).get(sessions.size() - 1);
						long millis = currentDate.getTime() - ((previousDate ==null)?currentDate.getTime():previousDate.getTime());
						totalmilli1 = totalmilli1 + millis;
						if (totalmilli1 > MAX_DURATION) {
							count = count + 1;
							totalmilli1 =0;
						}
						sessions.add(count);
						dayWiseSessions.put(s.format(currentDate),  sessions);
					}
					previousDate = currentDate;
					
					Date istStartDate = TimezoneConverter.convert("UTC", "Asia/Kolkata", data.getStartDate());
					Date istEndDate = TimezoneConverter.convert("UTC", "Asia/Kolkata", data.getStartDate());
					
					String content = appId + "," + data.getSessionId() + " ,"
							+ istStartDate + " ," + istEndDate + " ,"
							+ data.getEventName() + " ," + data.getEventCount()+", "+count+"\n ";
					bw.write(content);
					//rowInd++;
				}
				appIdAndSessions.put(appId, dayWiseSessions);
			}
			
			//dayWiseSessions(applicationDetails, bw);
		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}

		}
		//upload csv file to s3
		/*FileUpload.fileUpload(outputFile);
		//send an email to recipients
		String [] emailIds= emails.split(",");
		List<MandrillRecipient> list = new ArrayList<>();
		for(String recipient: emailIds) {
			list.add(new MandrillRecipient("", recipient));
		}
		MandrillRecipient[] recipients =  list.toArray(new MandrillRecipient[list.size()]);
		try {
			new SendEmail().sendMessage("User session report", recipients, "Saveyra", "support@saveyra.com", "https://user-session-report.s3.ap-south-1.amazonaws.com/reports/"+outputFile);
		} catch (RequestFailedException e) {
			e.printStackTrace();
		}*/
		//SendEmail.send("shivkumar@silverminegroup.com", "Ch@ngeMyGr0wth", email, "User session report", "https://user-session-report.s3.amazonaws.com/reports/"+outputFile);
		System.out.println("readFromMongoDB()==>Exit");
	}
	
	private static EventsData getLastEventsDataByAppID(String applicationId) {
		Connection con = null;
		EventsData events = new EventsData();
		try {
			con = dbConnection();
			// here sonoo is database name, root is username and password
			PreparedStatement stmt = con.prepareStatement("select session_id, event_count, start_time, end_time from user_sessions where application_id=? order by session_id desc limit 1");
			stmt.setString(1, applicationId);
			ResultSet rs = stmt.executeQuery();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			while (rs.next()) {
				events.setSessionId(rs.getInt(1));
				events.setEventCount(rs.getInt(2));
				events.setStartDate(sdf.parse(sdf.format(rs.getTimestamp(3))));
				events.setEndDate(sdf.parse(sdf.format(rs.getTimestamp(4))));
			}

		} catch (Exception e) {
			System.out.println(e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return events;
	}
	
	private static int insertRecords(String applicationId, int sessionId, Date start_time, Date end_time, String event_name, int event_count) {
		Connection con = null;
		int result = 0;
		try {
			con = dbConnection();
			
			PreparedStatement deletestmt = con.prepareStatement("delete from user_sessions where application_id=?");
			deletestmt.setString(1, applicationId);
			int delete = deletestmt.executeUpdate();
			if(delete > 1) {
				System.out.println("Previous record of "+applicationId+" deleted successfully.");
			}
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			// here sonoo is database name, root is username and password
			PreparedStatement stmt = con.prepareStatement("insert into user_sessions (application_id, session_id, start_time, end_time, event_name, event_count) values (?, ?, ?, ?, ?, ?)");
			stmt.setString(1, applicationId);
			stmt.setInt(2, sessionId);
			stmt.setTimestamp(3, new Timestamp(sdf.parse(sdf.format(start_time)).getTime()));
			stmt.setTimestamp(4, new Timestamp(sdf.parse(sdf.format(end_time)).getTime()));
			stmt.setString(5, event_name);
			stmt.setInt(6, event_count);
			result = stmt.executeUpdate();
			if(result > 1) {
				System.out.println("Last record of "+applicationId+" inserted successfully.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}
	
	private static Connection dbConnection() {
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://mysql-db:3306/user_sessions_report?useSSL=false&useUnicode=true&characterEncoding=UTF-8", "root", "root");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return con;
	}

}
