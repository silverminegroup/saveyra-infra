package com.saveyra.usersession.report.db;

import java.util.Date;

public class EventsData {
	private int sessionId;
	private Date startDate;
	private Date endDate;
	private String eventName;
	private int eventCount;
	private int daySessionId;

	/**
	 * @return the sessionId
	 */
	public int getSessionId() {
		return sessionId;
	}

	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the eventName
	 */
	public String getEventName() {
		return eventName;
	}

	/**
	 * @param eventName the eventName to set
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	/**
	 * @return the eventCount
	 */
	public int getEventCount() {
		return eventCount;
	}

	/**
	 * @param eventCount the eventCount to set
	 */
	public void setEventCount(int eventCount) {
		this.eventCount = eventCount;
	}

	/**
	 * @return the daySessionId
	 */
	public int getDaySessionId() {
		return daySessionId;
	}

	/**
	 * @param daySessionId the daySessionId to set
	 */
	public void setDaySessionId(int daySessionId) {
		this.daySessionId = daySessionId;
	}

}
