package com.saveyra.usersession.report.s3;

import java.io.File;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GroupGrantee;
import com.amazonaws.services.s3.model.Permission;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;

public class FileUpload {

	public static AmazonS3 awsCredentials() {
		AWSCredentials credentials = new BasicAWSCredentials(
				  "AKIA2XNGOFU6FUOTDGF4", 
				  "tLoUwqt7/PgmxUJWl+wdaqgDUQ/zlbq1B0B2QkgH"
				);
		AmazonS3 s3client = AmazonS3ClientBuilder
				  .standard()
				  .withCredentials(new AWSStaticCredentialsProvider(credentials))
				  .withRegion(Regions.AP_SOUTH_1)
				  .build();
		return s3client;
	}
	
	
	public static void fileUpload(String csvFile) {
		AmazonS3 s3client = awsCredentials();
		try {
			PutObjectRequest  putObjectRequest = new PutObjectRequest("user-session-report", "reports/"+csvFile, new File("/tmp/"+csvFile));
			putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead);
		    PutObjectResult por = s3client.putObject(putObjectRequest);
			if (por.getMetadata() != null)
				System.out.println("PutObjectResult "+por.getMetadata().getUserMetadata());
			else
				System.out.println("File is not uploaded to s3");
		} catch(Exception e) {
			System.out.println("Error in uploading file to aws S3");
		} finally {
			s3client.shutdown();
		}
	}
	
}