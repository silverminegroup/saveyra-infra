package com.saveyra.usersession.report.mail;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import com.cribbstechnologies.clients.mandrill.exception.RequestFailedException;
import com.cribbstechnologies.clients.mandrill.model.MandrillHtmlMessage;
import com.cribbstechnologies.clients.mandrill.model.MandrillMessageRequest;
import com.cribbstechnologies.clients.mandrill.model.MandrillRecipient;
import com.cribbstechnologies.clients.mandrill.model.response.message.SendMessageResponse;
import com.cribbstechnologies.clients.mandrill.request.MandrillMessagesRequest;
import com.cribbstechnologies.clients.mandrill.request.MandrillRESTRequest;
import com.cribbstechnologies.clients.mandrill.util.MandrillConfiguration;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SendEmail {
	
	private static final String API_VERSION = "1.0";
    private static final String BASE_URL = "https://mandrillapp.com/api";
    private static final String MANDRILL_API_KEY = "PMMpnH3NxCsKlJpE1z32LQ";

    private static MandrillRESTRequest request = new MandrillRESTRequest();
    private static MandrillConfiguration config = new MandrillConfiguration();
    private static MandrillMessagesRequest messagesRequest = new MandrillMessagesRequest();
    @SuppressWarnings("deprecation")
	private static HttpClient client = new DefaultHttpClient();
    private static ObjectMapper mapper = new ObjectMapper();

    public SendEmail() {
        config.setApiKey(MANDRILL_API_KEY);
        config.setApiVersion(API_VERSION);
        config.setBaseURL(BASE_URL);
        request.setConfig(config);
        request.setObjectMapper(mapper);
        request.setHttpClient(client);
        messagesRequest.setRequest(request);
    }
	public static void send(String from,String password,String to,String sub,String msg){  
        //Get properties object    
        Properties prop = new Properties();    
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); //TLS
        //get Session   
        Session session = Session.getDefaultInstance(prop,    
         new javax.mail.Authenticator() {    
         protected PasswordAuthentication getPasswordAuthentication() {    
         return new PasswordAuthentication(from,password);  
         }    
        });    
        //compose message    
        try {    
         MimeMessage message = new MimeMessage(session);    
         message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));    
         message.setSubject(sub);    
         message.setText(msg);    
         //send message  
         Transport.send(message);    
         System.out.println("message sent successfully");    
        } catch (MessagingException e) {throw new RuntimeException(e);}    
           
  }  
	public SendMessageResponse sendMessage(
            String subject,
            MandrillRecipient[] recipients,
            String senderName,
            String senderEmail,
            String content
    ) throws RequestFailedException {
        MandrillMessageRequest mmr = new MandrillMessageRequest();
        MandrillHtmlMessage message = new MandrillHtmlMessage();

        Map<String, String> headers = new HashMap<String, String>();
        message.setFrom_email(senderEmail);
        message.setFrom_name(senderName);
        message.setHeaders(headers);

        message.setText(content);
        message.setSubject(subject);
        message.setTo(recipients);
        message.setTrack_clicks(true);
        message.setTrack_opens(true);
        mmr.setMessage(message);

        return messagesRequest.sendMessage(mmr);
    }
}
