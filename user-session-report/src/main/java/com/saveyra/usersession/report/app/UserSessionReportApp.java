package com.saveyra.usersession.report.app;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

import com.saveyra.usersession.report.db.service.SaveyraMsgBrokerService;


@SpringBootApplication
@ComponentScan(basePackages= {"com.saveyra"})
@PropertySource(value = "classpath:application.properties")
public class UserSessionReportApp implements CommandLineRunner{

	@Autowired
	private SaveyraMsgBrokerService service;
	
	public static void main(String args[]) throws ParseException {
		SpringApplication.run(UserSessionReportApp.class, args);
	}


	@Override
	public void run(String... args) throws Exception {
		//if (args != null && args.length == 1) {
			//String filePath = args[0];
			//String outputFile = args[0];
			//System.out.println("Output file: " + outputFile);
			service.readFromMongoDB();
		//} else {
			//System.out.println("No input file found. Please specify input file java -jar  '</inputfilepath/fileName.xlsx>' '</outputfile path/fileName.csv>'");
		//}
		System.exit(0);
	}
}
