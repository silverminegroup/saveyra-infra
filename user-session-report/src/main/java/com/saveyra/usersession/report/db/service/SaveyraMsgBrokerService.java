package com.saveyra.usersession.report.db.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import com.saveyra.usersession.report.db.MessageBrokerCollection;

public interface SaveyraMsgBrokerService {
	List<MessageBrokerCollection> findAll() throws ParseException;
	void readFromMongoDB() throws ParseException, ParseException, FileNotFoundException, IOException;
}
