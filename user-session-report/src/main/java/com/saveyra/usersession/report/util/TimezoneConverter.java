package com.saveyra.usersession.report.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TimezoneConverter {
	private static SimpleDateFormat timezoneSDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static Date convert(String sourceTimezone, String targetTimezone, Date date) {
		Date parseDate = null;
		try {
			timezoneSDf.setTimeZone(TimeZone.getTimeZone(sourceTimezone));
			String utcDate = timezoneSDf.format(date);

			timezoneSDf.setTimeZone(TimeZone.getTimeZone(targetTimezone));
			parseDate = timezoneSDf.parse(timezoneSDf.format(timezoneSDf.parse(utcDate)));
		} catch (ParseException pe) {
			return date;
		}
		return parseDate;

	}
}
