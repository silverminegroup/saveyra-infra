package com.saveyra.aws;

import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.map.ObjectMapper;

import com.saveyra.notification.model.AlertObject;

public class SnsMessageGenerator {

	private static final ObjectMapper objectMapper = new ObjectMapper();

	public static enum Platform {
		// Apple Push Notification Service
		APNS,
		// Sandbox version of Apple Push Notification Service
		APNS_SANDBOX,
		// Google Cloud Messaging
		GCM

	}

	public static String jsonify(Object message) {
		try {
			return objectMapper.writeValueAsString(message);
		} catch (Exception e) {
			e.printStackTrace();
			throw (RuntimeException) e;
		}
	}

	public static String getAppleMessage(String n_id,String Message, String notificationCategory, String notificationSubcategory,
			String imageId, String navTo, String thumbnail, String stickerCategory,String notificationType,String action,String title) {
		Map<String, Object> appleMessageMap = new HashMap<String, Object>();
		Map<String, Object> appMessageMap = new HashMap<String, Object>();
		AlertObject ao = new AlertObject(title,"",Message);		
		appMessageMap.put("alert", ao);// Apple Standard
		appMessageMap.put("n_id", n_id);
		appMessageMap.put("notificationCategory", notificationCategory);
		appMessageMap.put("badge", 1);// Apple Standard
		appMessageMap.put("sound", "default");
		appMessageMap.put("notificationSubcategory", notificationSubcategory);
		appMessageMap.put("imageId", imageId);		
		appMessageMap.put("navTo", navTo);
		appMessageMap.put("notificationType", notificationType);
		appMessageMap.put("action", action);
		appMessageMap.put("thumbnail", thumbnail);
		appMessageMap.put("stickerCategory", stickerCategory);
		appleMessageMap.put("aps", appMessageMap);// Apple Standard
		if(thumbnail!=null && !(thumbnail.equalsIgnoreCase(""))) {
			appMessageMap.put("category", "CustomSamplePush");
			appMessageMap.put("mutable-content", "1");
		}
		
		
		return jsonify(appleMessageMap);
	}

	public static String getAndroidMessage(String n_id,String Message, String notificationCategory, String notificationSubcategory,
			String imageId, String navTo, String thumbnail, String stickerCategory,String notificationType,String action,String title) {
		Map<String, Object> androidMessageMap = new HashMap<String, Object>();
		androidMessageMap.put("collapse_key", "Welcome");
		androidMessageMap.put("data", getData(n_id,Message, notificationCategory, notificationSubcategory, imageId, navTo,
				thumbnail, stickerCategory,notificationType,action,title));
		androidMessageMap.put("delay_while_idle", true);
		//androidMessageMap.put("time_to_live", 100);
		androidMessageMap.put("dry_run", false);
		return jsonify(androidMessageMap);
	}

	private static Map<String, String> getData(String n_id,String Message, String notificationCategory,
			String notificationSubcategory, String imageId, String navTo, String thumbnail, String stickerCategory,String notificationType,String action,String title) {
		Map<String, String> payload = new HashMap<String, String>();
		payload.put("n_id", n_id);
		payload.put("message", Message);
		payload.put("notificationCategory", notificationCategory);
		payload.put("notificationSubcategory", notificationSubcategory);
		payload.put("notificationType", notificationType);
		payload.put("imageId", imageId);
		payload.put("navTo", navTo);
		payload.put("thumbnail", thumbnail);
		payload.put("action", action);
		payload.put("stickerCategory", stickerCategory);
		payload.put("title", title);
		return payload;
	}
}