package com.saveyra.aws;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.saveyra.aws.SnsMessageGenerator.Platform;
import com.saveyra.notification.utility.Declaration;

public class AmazonSNSClientWrapper {

	private final AmazonSNS snsClient;

	//private static final Logger logger = (Logger) LoggerFactory.getLogger(AmazonSNSClientWrapper.class);

	public AmazonSNSClientWrapper(AmazonSNS client) {
		this.snsClient = client;
	}

	/* platformToken is Device ID */
	private CreatePlatformEndpointResult createPlatformEndpoint(Platform platform, String customData,
			String platformToken, String applicationArn) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("Enabled", "True");
		CreatePlatformEndpointRequest platformEndpointRequest = new CreatePlatformEndpointRequest()
				.withToken(platformToken).withPlatformApplicationArn(applicationArn).withCustomUserData(customData)
				.withAttributes(map);
		return snsClient.createPlatformEndpoint(platformEndpointRequest);
	}

	private PublishResult publish(String endpointArn, Platform platform,
			Map<Platform, Map<String, MessageAttributeValue>> attributesMap, String n_id, String Message,
			String notificationCategory, String notificationSubcategory, String imageId, String navTo, String thumbnail,
			String stickerCategory, String notificationType, String action , String title) {
		PublishRequest publishRequest = new PublishRequest();
		String notificationAttributes = Message;

		if (notificationAttributes != null && !notificationAttributes.isEmpty()) {
			publishRequest.setMessage(notificationAttributes);
		} else {
			// If the message attributes are not set in the requisite method,
			// notification is sent with default attributes
			//logger.warn("[WARNING] Notification Attributes NULL");
		}
		publishRequest.setMessageStructure("json");
		Map<String, String> messageMap = new HashMap<String, String>();

		String snsPlatformMsg = getSnsPlatformMessage(platform, n_id, Message, notificationCategory,
				notificationSubcategory, imageId, navTo, thumbnail, stickerCategory, notificationType, action, title);
		messageMap.put(platform.name(), snsPlatformMsg);
		snsPlatformMsg = SnsMessageGenerator.jsonify(messageMap);
		String jsonFormattedString = snsPlatformMsg.replaceAll("\\\\", "");// Removing the escape character in Regular
																			// expressions
		publishRequest.setTargetArn(endpointArn);
		StringBuilder builder = new StringBuilder();
		builder.append("{Message Attributes: " + jsonFormattedString);
		builder.deleteCharAt(builder.length() - 1);
		builder.append("}");
		//logger.info("[INFO] Publishing Message : \n" + builder.toString() + "\n");
		publishRequest.setMessage(snsPlatformMsg);
		return snsClient.publish(publishRequest);
	}

	/**
	 * 
	 * @param platform
	 * @param platformToken
	 * @param applicationName
	 * @param attrsMap
	 * @param Message
	 * @param notificationCategory
	 * @param notificationSubcategory
	 * @param imageId
	 * @param navTo
	 * @param thumbnail
	 * @param stickerCategory
	 */
	public String AppleNotification(Platform platform, String platformToken, String applicationName,
			Map<Platform, Map<String, MessageAttributeValue>> attrsMap, String n_id, String Message,
			String notificationCategory, String notificationSubcategory, String imageId, String navTo, String thumbnail,
			String stickerCategory, String notificationType, String action, String title ) {
		String platformApplicationArn = null;

		if (Declaration.getEnvironment().equalsIgnoreCase("prod")) {
			platformApplicationArn = Declaration.getAppleArnApns();
		} else {
			platformApplicationArn = Declaration.getAppleArnApnsSandbox();
		}

		//logger.info("[INFO] Application Arn : " + platformApplicationArn + "  App Name : " + applicationName);

		CreatePlatformEndpointResult platformEndpointResult = createPlatformEndpoint(platform, "Custom data",
				platformToken, platformApplicationArn);

		Map<String, String> messageMap = new HashMap<String, String>();
		String snsMessage = getSnsPlatformMessage(platform, n_id, Message, notificationCategory,
				notificationSubcategory, imageId, navTo, thumbnail, stickerCategory, notificationType, action,title);
		messageMap.put(platform.name(), snsMessage);
		snsMessage = SnsMessageGenerator.jsonify(messageMap);
		//String jsonFormattedString = snsMessage.replaceAll("\\\\", "");
		PublishResult publishResult = publish(platformEndpointResult.getEndpointArn(), platform, attrsMap, n_id,
				Message, notificationCategory, notificationSubcategory, imageId, navTo, thumbnail, stickerCategory,
				notificationType, action,title);
		//logger.info(
		//		"[INFO] PUBLISHED !  [MessageId] " + publishResult.getMessageId() + "\n[DATA] " + jsonFormattedString);
		return publishResult.getMessageId();
	}

	public String AndroidNotification(Platform platform, String platformToken, String applicationName,
			Map<Platform, Map<String, MessageAttributeValue>> attrsMap, String n_id, String Message,
			String notificationCategory, String notificationSubcategory, String imageId, String navTo, String thumbnail,
			String stickerCategory,String notificationType,String action,String title) {
		String platformApplicationArn = null;

		if (Declaration.getEnvironment().equalsIgnoreCase("prod")) {
			platformApplicationArn = Declaration.getAndroidArnGcm();
		} else {
			platformApplicationArn = Declaration.getAndroidArnGsmStaging();
		}

		//logger.info("[INFO] Application Arn : " + platformApplicationArn + "  App Name : " + applicationName);

		CreatePlatformEndpointResult platformEndpointResult = createPlatformEndpoint(platform,
				"CustomData - Useful to store endpoint specific data", platformToken, platformApplicationArn);
		PublishResult publishResult = publish(platformEndpointResult.getEndpointArn(), platform, attrsMap, n_id,
				Message, notificationCategory, notificationSubcategory, imageId, navTo, thumbnail, stickerCategory,
				notificationType, action,title);
		//logger.info("[INFO] PUBLISHED !  [MessageId] " + publishResult.getMessageId() + "\n[DATA] " + Message);
		return publishResult.getMessageId();
	}

	private String getSnsPlatformMessage(Platform platform, String n_id, String Message, String notificationCategory,
			String notificationSubcategory, String imageId, String navTo, String thumbnail, String stickerCategory,
			String notificationType, String action,String title) {
		switch (platform) {
		case APNS:
			//logger.info("[INFO] NOTIFICATION IOS");
			return SnsMessageGenerator.getAppleMessage(n_id, Message, notificationCategory, notificationSubcategory,
					imageId, navTo, thumbnail, stickerCategory, notificationType, action, title);
		case APNS_SANDBOX:
			//logger.info("[INFO] NOTIFICATION IOS SANDBOX");
			return SnsMessageGenerator.getAppleMessage(n_id, Message, notificationCategory, notificationSubcategory,
					imageId, navTo, thumbnail, stickerCategory, notificationType, action ,title);
		case GCM:
			//logger.info("[INFO] NOTIFICATION IOS ANDROID");
			return SnsMessageGenerator.getAndroidMessage(n_id, Message, notificationCategory, notificationSubcategory,
					imageId, navTo, thumbnail, stickerCategory, notificationType, action ,title);
		default:
			//logger.error("[ERROR] NOTIFICATION PLATFORM NOT SUPPORTED : " + platform.name());
			throw new IllegalArgumentException("Platform not supported : " + platform.name());
		}

	}

	public static Map<String, MessageAttributeValue> getValidNotificationAttributes(
			Map<String, MessageAttributeValue> notificationAttributes) {
		Map<String, MessageAttributeValue> validAttributes = new HashMap<String, MessageAttributeValue>();
		for (Map.Entry<String, MessageAttributeValue> entry : notificationAttributes.entrySet()) {
			if (!StringUtils.isBlank(entry.getValue().getStringValue())) {
				validAttributes.put(entry.getKey(), entry.getValue());
			}
		}
		return validAttributes;
	}
}