package com.saveyra.notification;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.Executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.event.EventListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.saveyra.notification.utility.Declaration;

import net.javacrumbs.shedlock.spring.annotation.EnableSchedulerLock;

@SpringBootApplication
@EnableScheduling
@EnableSchedulerLock(defaultLockAtMostFor = "PT30S")
@EnableJpaAuditing // audits Entity class
@EnableAutoConfiguration
@ComponentScan({ "com.saveyra.notification", "com.saveyara.aws" })
@PropertySource(value="classpath:application.properties")
public class SaveyranotificationApplication extends SpringBootServletInitializer implements CommandLineRunner {
	
	private static final Logger logger = LoggerFactory.getLogger(SaveyranotificationApplication.class);

	public static void main(String[] args) {
		//AbstractApplicationContext  context = new AnnotationConfigApplicationContext(SchedulerConfig.class);
		SpringApplication.run(SaveyranotificationApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void doSomethingAfterStartup() {
		File warPath = new File(
				SaveyranotificationApplication.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		String warParent = warPath.getParentFile().getParentFile().getParentFile().getAbsolutePath();		
		
		Declaration.setWAR_PARENT_PATH(warParent);
		
		File folder = new File(Declaration.getWAR_PARENT_PATH() + Declaration.getConfigpath());
		logger.info("CONFIG PATH : "+Declaration.getWAR_PARENT_PATH() + Declaration.getConfigpath());
		File[] listOfFiles = folder.listFiles();		
		if(listOfFiles == null) {
			logger.error("Config Not found. Continueing as DEV/QA BUILD");
			Declaration.setEnvironment("dev/qa");
			return;
		}

		for (File file : listOfFiles) {
			if (file.isFile()) {
				logger.info("Config File : "+ file.getName());
				Scanner scanner;
				try {
					scanner = new Scanner(file);
					while (scanner.hasNextLine()) {
						final String lineFromFile = scanner.nextLine();
						if (lineFromFile.contains("JAVA_BUILD")) {
							String env = lineFromFile.split("JAVA_BUILD=")[1];
							logger.info("ENVIRONMENT : " + env);
							Declaration.setEnvironment(env);
							break;
						}
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void run(String... args) throws Exception {	

	}
	
	@Bean
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(10);
        executor.setMaxPoolSize(10);
        executor.setQueueCapacity(5000);
        executor.setThreadNamePrefix("BulkPush-");
        executor.initialize();
        return executor;
    }

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(SaveyranotificationApplication.class);
	}
}
