package com.saveyra.notification.scheduler.model;

import java.util.List;

public class NotificationJson {
	private String gender;
	private String frequency;
	private String age_grp;
	private String url;
	private String scheduled_time;
	private List<MessageObject> data;
	
	public NotificationJson(String gender, String frequency, String age_grp, String url, String scheduled_time,
			List<MessageObject> data) {
		super();
		this.gender = gender;
		this.frequency = frequency;
		this.age_grp = age_grp;
		this.url = url;
		this.scheduled_time = scheduled_time;
		this.data = data;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getAge_grp() {
		return age_grp;
	}

	public void setAge_grp(String age_grp) {
		this.age_grp = age_grp;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getScheduled_time() {
		return scheduled_time;
	}

	public void setScheduled_time(String scheduled_time) {
		this.scheduled_time = scheduled_time;
	}

	public List<MessageObject> getData() {
		return data;
	}

	public void setData(List<MessageObject> data) {
		this.data = data;
	}	
}