package com.saveyra.notification.scheduler.model;

public class MessageObject {
	private MessageBody message_body;
	private String language;
	
	public MessageObject(MessageBody message_body, String language) {
		super();
		this.message_body = message_body;
		this.language = language;
	}
	
	public MessageBody getMessage_body() {
		return message_body;
	}
	public void setMessage_body(MessageBody message_body) {
		this.message_body = message_body;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}	
}
