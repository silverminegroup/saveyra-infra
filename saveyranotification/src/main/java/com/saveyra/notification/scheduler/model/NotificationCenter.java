package com.saveyra.notification.scheduler.model;

public class NotificationCenter {
	private int id;
	private String notificationtitle;
	private String message;
	private String navigation_to;
	private String thumbnail;
	private String createdDate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNotificationtitle() {
		return notificationtitle;
	}
	public void setNotificationtitle(String notificationtitle) {
		this.notificationtitle = notificationtitle;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getNavigation_to() {
		return navigation_to;
	}
	public void setNavigation_to(String navigation_to) {
		this.navigation_to = navigation_to;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public NotificationCenter(int id, String notificationtitle, String message, String navigation_to, String thumbnail,
			String createdDate) {
		super();
		this.id = id;
		this.notificationtitle = notificationtitle;
		this.message = message;
		this.navigation_to = navigation_to;
		this.thumbnail = thumbnail;
		this.createdDate = createdDate;
	}	
}
