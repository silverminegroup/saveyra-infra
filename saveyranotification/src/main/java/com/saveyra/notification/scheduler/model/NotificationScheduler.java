package com.saveyra.notification.scheduler.model;

import java.sql.Timestamp;

public class NotificationScheduler {	
	private Integer id;
	private String n_id;
	private String message_body;
	private String url;
	private String status;
	private Timestamp created_timestamp;
	private Timestamp scheduled_time;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getN_id() {
		return n_id;
	}
	public void setN_id(String n_id) {
		this.n_id = n_id;
	}
	public String getMessage_body() {
		return message_body;
	}
	public void setMessage_body(String message_body) {
		this.message_body = message_body;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Timestamp getCreated_timestamp() {
		return created_timestamp;
	}
	public void setCreated_timestamp(Timestamp created_timestamp) {
		this.created_timestamp = created_timestamp;
	}
	public Timestamp getScheduled_time() {
		return scheduled_time;
	}
	public void setScheduled_time(Timestamp scheduled_time) {
		this.scheduled_time = scheduled_time;
	}
	
	public NotificationScheduler(Integer id, String n_id, String message_body, String url, String status,
			Timestamp created_timestamp, Timestamp scheduled_time) {
		super();
		this.id = id;
		this.n_id = n_id;
		this.message_body = message_body;
		this.url = url;
		this.status = status;
		this.created_timestamp = created_timestamp;
		this.scheduled_time = scheduled_time;
	}	
}
