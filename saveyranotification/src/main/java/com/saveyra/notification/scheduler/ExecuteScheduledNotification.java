package com.saveyra.notification.scheduler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.saveyara.aws.repo.NotificationsSent;
import com.saveyra.notification.controller.NotificationController;
import com.saveyra.notification.repository.MysqlRepo;
import com.saveyra.notification.utility.Declaration;

@PropertySource(value="classpath:application.properties")
public class ExecuteScheduledNotification implements Runnable{

	@Autowired
	MysqlRepo repo;

	@Autowired
	private Environment env;
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(ScheduledTask.class);
	
	public ExecuteScheduledNotification(String json) {
		logger.info("\n[INFO] Enter ScheduledNotificationExecute , Request : \n" + json);

		ObjectMapper mapper = new ObjectMapper();
		@SuppressWarnings("rawtypes")
		Map notificationMap = null;
		try {
			notificationMap = mapper.readValue(json, Map.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		@SuppressWarnings("unchecked")
		List<String> notificationIdList = (List<String>) notificationMap.get("notificationId");
		List<String> disableNotificationsList = new ArrayList<String>();
		long successCount = 0;
		long FailureCount = 0;
		/*AmazonSNSClient sns = new AmazonSNSClient(
				new BasicAWSCredentials("AKIAIIGSDYMQGEZV47ZA", "BR96gGn+HOVieXRydNwTxur7HsMSImaDvrMiK2cy"));
*/
		
		AmazonSNSClient sns = new AmazonSNSClient(
				new BasicAWSCredentials(env.getProperty("saveyra.accessKey","AKIA2XNGOFU6FUOTDGF4"), env.getProperty("saveyra.secretKey","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")));

		
		String notificationType = notificationMap.get("notificationType") == null
				? notificationMap.get("notificationtype").toString()
				: notificationMap.get("notificationType").toString();
		String action = notificationMap.get("action") == null ? "" : notificationMap.get("action").toString();
		// Update Common Notification Params
		NotificationsSent notification = new NotificationsSent();
		notification.setN_id(notificationMap.get("n_id").toString());
		notification.setMessage(notificationMap.get("message").toString());
		notification.setPlatform(notificationMap.get("platform").toString());
		notification
				.setImage_id(notificationMap.get("imageId") == null ? "" : notificationMap.get("imageId").toString());
		notification.setNotificationcategory(notificationMap.get("notificationcategory").toString());
		notification.setNotificationType(notificationType);
		notification.setNavigation_to(notificationMap.get("navTo").toString());
		notification.setSticker_category(notificationMap.get("stickerCategory").toString());
		notification.setThumbnail(
				notificationMap.get("thumbnail") == null ? "" : notificationMap.get("thumbnail").toString());
		notification.setAction(action);
		notification.setTitle(notificationMap.get("title") == null ? "" : notificationMap.get("title").toString());

		//System.out.println("SIZE : "+notificationIdList.size());

		for (int i = 0; i < notificationIdList.size(); i++) {

			notification.setNotification_id(notificationIdList.get(i));
			notification.setPlatform(notificationMap.get("platform").toString());
			String platform = notification.getPlatform();
			String iOS = "iOS";//
			String Android = "Android";//
			String All = "All";
			
			// ############################# IOS ########################################
			if (platform.equals(iOS)) {
				if (Declaration.getEnvironment().equalsIgnoreCase("prod")) {
					sns.setEndpoint("https://sns.ap-south-1.amazonaws.com");
				} else {
					sns.setEndpoint("https://sns.us-east-1.amazonaws.com");
				}
				if (notification.getNotification_id().trim().length() != 64) {
					FailureCount++;
					if (notification.getNotification_id().trim().length() < 100) {
						disableNotificationsList.add(notification.getNotification_id());
					}
					continue;
				}

				try {
					// logger.info("\n[INFO] iOS Notification");
					NotificationController Apple = new NotificationController(sns);
					if (Declaration.getEnvironment().equalsIgnoreCase("prod")) {
						Apple.AppleAppNotification(notification);
					} else {
						Apple.AppleSandboxAppNotification(notification);
					}
					successCount++;
					notification.setStatus("Success");
				} catch (AmazonServiceException ase) {
					logger.error("\n[ERROR] AmazonServiceException : " + ase.getMessage() + " [Device Token] "
							+ notification.getNotification_id() + " [STATUS CODE] " + ase.getStatusCode()
							+ " [ERROR CODE] " + ase.getErrorCode() + " [ERROR TYPE] " + ase.getErrorType()
							+ " [Request ID] " + ase.getRequestId());
					String errorString = "[STATUS CODE] " + ase.getStatusCode() + " [ERROR CODE] " + ase.getErrorCode()
							+ " [ERROR TYPE] " + ase.getErrorType() + " [Request ID] " + ase.getRequestId();
					notification.setStatus(errorString.length() > 255 ? errorString.substring(0, 254) : errorString);
					FailureCount++;
					disableNotificationsList.add(notification.getNotification_id());
				} catch (AmazonClientException ace) {
					logger.error("\n[ERROR] AmazonClientException : " + ace.getMessage() + " [Device Token] "
							+ notification.getNotification_id());
					String errorString = "[ERROR] " + ace.getMessage();
					notification.setStatus(errorString.length() > 255 ? errorString.substring(0, 254) : errorString);
					FailureCount++;
					disableNotificationsList.add(notification.getNotification_id());
				} finally {
					/*
					 * String msg = notification.getMessage(); ByteBuffer byteBuffer =
					 * StandardCharsets.UTF_8.encode(msg); notification.setMessage(new
					 * String(byteBuffer.array(), "ASCII"));
					 */
					repo.save(notification);

				}
				// logger.info("\n[INFO] iOS Notification End");
			} else if (platform.equals(Android)) {
				sns.setEndpoint("https://sns.ap-south-1.amazonaws.com");

				if (notification.getNotification_id().trim().length() < 100) {
					if (notification.getNotification_id().trim().length() != 64) {
						disableNotificationsList.add(notification.getNotification_id());
					}
					FailureCount++;
					continue;
				}

				// logger.info("[INFO] Android Notification Start");
				try {
					NotificationController Android1 = new NotificationController(sns);
					Android1.AndroidAppNotification(notification);
					notification.setStatus("Success");
					successCount++;
				} catch (AmazonServiceException ase) {
					logger.error("\n[ERROR] AmazonServiceException : " + ase.getMessage() + " [Device Token] "
							+ notification.getNotification_id() + " [STATUS CODE] " + ase.getStatusCode()
							+ " [ERROR CODE] " + ase.getErrorCode() + " [ERROR TYPE] " + ase.getErrorType()
							+ " [Request ID] " + ase.getRequestId());
					String errorString = "[STATUS CODE] " + ase.getStatusCode() + " [ERROR CODE] " + ase.getErrorCode()
							+ " [ERROR TYPE] " + ase.getErrorType() + " [Request ID] " + ase.getRequestId();
					notification.setStatus(errorString.length() > 255 ? errorString.substring(0, 254) : errorString);
					FailureCount++;
					disableNotificationsList.add(notification.getNotification_id());
				} catch (AmazonClientException ace) {
					logger.error("\n[ERROR] AmazonClientException : " + ace.getMessage() + " [Device Token] "
							+ notification.getNotification_id());
					String errorString = "[ERROR] " + ace.getMessage();
					notification.setStatus(errorString.length() > 255 ? errorString.substring(0, 254) : errorString);
					FailureCount++;
					disableNotificationsList.add(notification.getNotification_id());
				} finally {
					/*
					 * String msg = notification.getMessage(); ByteBuffer byteBuffer =
					 * StandardCharsets.UTF_8.encode(msg); notification.setMessage(new
					 * String(byteBuffer.array(), "ASCII"));
					 */
					repo.save(notification);
				}
				// logger.info("\n[INFO] Android Notification End");
			} else if (platform.equals(All)) {
				// Apple notifications
				if (notification.getNotification_id().trim().length() == 64) {
					// logger.info("[INFO] iOS Notification Start");
					if (Declaration.getEnvironment().equalsIgnoreCase("prod")) {
						sns.setEndpoint("https://sns.ap-south-1.amazonaws.com");
					} else {
						sns.setEndpoint("https://sns.us-east-1.amazonaws.com");
					}

					try {
						NotificationController Apple = new NotificationController(sns);
						if (Declaration.getEnvironment().equalsIgnoreCase("prod")) {
							Apple.AppleAppNotification(notification);
						} else {
							Apple.AppleSandboxAppNotification(notification);
						}
						notification.setPlatform("iOS");
						successCount++;
						notification.setStatus("Success");
					} catch (AmazonServiceException ase) {
						logger.error("[ERROR] AmazonServiceException : " + ase.getMessage() + " [Device Token] "
								+ notification.getNotification_id() + " [STATUS CODE] " + ase.getStatusCode()
								+ " [ERROR CODE] " + ase.getErrorCode() + " [ERROR TYPE] " + ase.getErrorType()
								+ " [Request ID] " + ase.getRequestId());
						String errorString = "[STATUS CODE] " + ase.getStatusCode() + " [ERROR CODE] "
								+ ase.getErrorCode() + " [ERROR TYPE] " + ase.getErrorType() + " [Request ID] "
								+ ase.getRequestId();
						notification
								.setStatus(errorString.length() > 254 ? errorString.substring(0, 254) : errorString);
						FailureCount++;
						disableNotificationsList.add(notification.getNotification_id());
					} catch (AmazonClientException ace) {
						logger.error("[ERROR] AmazonClientException : " + ace.getMessage() + " [Device Token] "
								+ notification.getNotification_id());
						String errorString = "[ERROR] " + ace.getMessage();
						notification
								.setStatus(errorString.length() > 254 ? errorString.substring(0, 254) : errorString);
						FailureCount++;
						disableNotificationsList.add(notification.getNotification_id());
					} finally {
						repo.save(notification);
					}
				} else if (notification.getNotification_id().trim().length() != 64
						&& notification.getNotification_id().trim().length() < 100) {
					disableNotificationsList.add(notification.getNotification_id());
					logger.info("[INFO] Invalid Token : " + notification.getNotification_id());
				} else if (notification.getNotification_id().trim().length() > 100) {
					// Android notifications
					logger.info("[INFO] Android Notification Start");
					sns.setEndpoint("https://sns.ap-south-1.amazonaws.com");
					try {
						NotificationController androidNotification = new NotificationController(sns);
						androidNotification.AndroidAppNotification(notification);
						notification.setPlatform("Android");
						notification.setStatus("Success");
						successCount++;
					} catch (AmazonServiceException ase) {
						logger.error("[ERROR] AmazonServiceException : " + ase.getMessage() + " [Device Token] "
								+ notification.getNotification_id() + " [STATUS CODE] " + ase.getStatusCode()
								+ " [ERROR CODE] " + ase.getErrorCode() + " [ERROR TYPE] " + ase.getErrorType()
								+ " [Request ID] " + ase.getRequestId());
						String errorString = "[STATUS CODE] " + ase.getStatusCode() + " [ERROR CODE] "
								+ ase.getErrorCode() + " [ERROR TYPE] " + ase.getErrorType() + " [Request ID] "
								+ ase.getRequestId();
						notification
								.setStatus(errorString.length() > 254 ? errorString.substring(0, 254) : errorString);
						FailureCount++;
						disableNotificationsList.add(notification.getNotification_id());
					} catch (AmazonClientException ace) {
						logger.error("[ERROR] AmazonClientException : " + ace.getMessage() + " [Device Token] "
								+ notification.getNotification_id());
						String errorString = "[ERROR] " + ace.getMessage();
						notification
								.setStatus(errorString.length() > 254 ? errorString.substring(0, 254) : errorString);
						disableNotificationsList.add(notification.getNotification_id());
					} finally {
						repo.save(notification);
					}
				}
			}
		}

		logger.info("[INFO] Notification Sent Successfully  : " + successCount
				+ "\n       Notification Sent Failure  : " + FailureCount);
		StringBuffer sb = new StringBuffer();

		if (disableNotificationsList != null) {
			for (int i = 0; i < disableNotificationsList.size(); i++) {
				sb.append("'");
				sb.append(disableNotificationsList.get(i));
				if (i < (disableNotificationsList.size() - 1)) {
					sb.append("',");
				} else {
					sb.append("'");
				}
			}
		}
		// Update status of all errors
		repo.disableDeviceToken(sb.toString());

		logger.info("\n[INFO] Leave ScheduledNotificationExecute");
	   }
	
	@Override
	public void run() {
		// TODO Auto-generated method stub		
	}

}
