package com.saveyra.notification.scheduler.model;

import java.util.List;
import java.util.Set;

public class MessageBody {
	private String n_id;
	private Set<String> notificationId;
	private String message;
	private String platform;
	private String imageId;
	private String notificationcategory;
	private String notificationtype;
	private String navTo;
	private String stickerCategory;
	private String thumbnail;
	private String title;
	public MessageBody(String n_id, Set<String> notificationId, String message, String platform, String imageId,
			String notificationcategory, String notificationtype, String navTo, String stickerCategory,
			String thumbnail, String title) {
		super();
		this.n_id = n_id;
		this.notificationId = notificationId;
		this.message = message;
		this.platform = platform;
		this.imageId = imageId;
		this.notificationcategory = notificationcategory;
		this.notificationtype = notificationtype;
		this.navTo = navTo;
		this.stickerCategory = stickerCategory;
		this.thumbnail = thumbnail;
		this.title = title;
	}
	public String getN_id() {
		return n_id;
	}
	public void setN_id(String n_id) {
		this.n_id = n_id;
	}
	public Set<String> getNotificationId() {
		return notificationId;
	}
	public void setNotificationId(Set<String> dTList) {
		this.notificationId = dTList;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getImageId() {
		return imageId;
	}
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	public String getNotificationcategory() {
		return notificationcategory;
	}
	public void setNotificationcategory(String notificationcategory) {
		this.notificationcategory = notificationcategory;
	}
	public String getNotificationtype() {
		return notificationtype;
	}
	public void setNotificationtype(String notificationtype) {
		this.notificationtype = notificationtype;
	}
	public String getNavTo() {
		return navTo;
	}
	public void setNavTo(String navTo) {
		this.navTo = navTo;
	}
	public String getStickerCategory() {
		return stickerCategory;
	}
	public void setStickerCategory(String stickerCategory) {
		this.stickerCategory = stickerCategory;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
}