package com.saveyra.notification.scheduler.model;

import java.sql.Timestamp;

public class NotificationSchedulerV2 {

	private Integer id;
	private String n_id;
	private String message_body;
	private String language;
	private String timeZone;
	private String status;
	private Timestamp created_timestamp;
	private Timestamp scheduled_time;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getN_id() {
		return n_id;
	}
	public void setN_id(String n_id) {
		this.n_id = n_id;
	}
	public String getMessage_body() {
		return message_body;
	}
	public void setMessage_body(String message_body) {
		this.message_body = message_body;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Timestamp getCreated_timestamp() {
		return created_timestamp;
	}
	public void setCreated_timestamp(Timestamp created_timestamp) {
		this.created_timestamp = created_timestamp;
	}
	public Timestamp getScheduled_time() {
		return scheduled_time;
	}
	public void setScheduled_time(Timestamp scheduled_time) {
		this.scheduled_time = scheduled_time;
	}
	
	public NotificationSchedulerV2(Integer id, String n_id, String message_body, String language,
			String timeZone, String status, Timestamp created_timestamp, Timestamp scheduled_time) {
		super();
		this.id = id;
		this.n_id = n_id;
		this.message_body = message_body;
		this.language = language;
		this.timeZone = timeZone;
		this.status = status;
		this.created_timestamp = created_timestamp;
		this.scheduled_time = scheduled_time;
	}
}
