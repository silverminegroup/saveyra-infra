package com.saveyra.notification.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.saveyara.aws.repo.NotificationsSent;
import com.saveyra.notification.model.DeviceTokenObject;
import com.saveyra.notification.scheduler.model.NotificationCenter;
import com.saveyra.notification.scheduler.model.NotificationScheduler;
import com.saveyra.notification.scheduler.model.NotificationSchedulerV2;

import net.javacrumbs.shedlock.core.LockProvider;
import net.javacrumbs.shedlock.provider.jdbctemplate.JdbcTemplateLockProvider;

@Repository
public class MysqlRepo {

	private static final Logger logger = LoggerFactory.getLogger(MysqlRepo.class);

	@SuppressWarnings("unused")
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	DataSource dataSource;
	
	@Bean
	public LockProvider lockProvider(DataSource dataSource) {
	    return new JdbcTemplateLockProvider(dataSource);
	}
	
	
	@Transactional(readOnly = true)
	public List<NotificationCenter> getNotificationCenter() {
		//logger.info("[ENTER] getNotificationCenter");
		List<NotificationCenter> notList = new ArrayList<NotificationCenter>();
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		try {
			conn = dataSource.getConnection();
			query = "SELECT id,notificationtitle,message,navigation_to,thumbnail,concat(scheduledate,' ',scheduletime) as createdDate FROM smg_saveyra_notification where date(expirydate) >=  CURDATE() and notificationdelivery like '%NC%'";
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				notList.add(new NotificationCenter(rs.getInt(1), rs.getString(2), rs.getString(3),
						rs.getString(4), rs.getString(5), rs.getString(6)));
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				if(pstmt!=null) {
					pstmt.close();
				}								
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if(rs!=null) {
					rs.close();
				}				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}				
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//logger.info("[LEAVE] getNotificationCenter");
		return notList;
	}

	// #//Scheduler
	
	@Transactional
	public void updateSchedulerV2(Integer id) {
		//logger.info("[ENTER] updateScheduler for ID :" + id);
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dataSource.getConnection();
			String query = "update smg_saveyra_notification_scheduler set status = 'Executed',updated_timestamp = now(), execution_time = now() where id=?";
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				if(pstmt!=null) {
					pstmt.close();
				}								
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//logger.info("[LEAVE] updateScheduler");
	}

	@Transactional
	public void updateScheduler(Integer id) {
		//logger.info("[ENTER] updateScheduler for ID :" + id);
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dataSource.getConnection();
			String query = "update smg_saveyra_notification_scheduler set status = 'Executed',updated_timestamp = now(), execution_time = now() where id=?";
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				if(pstmt!=null) {
					pstmt.close();
				}								
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//logger.info("[LEAVE] updateScheduler");
	}
	
	@Transactional
	public ArrayList<DeviceTokenObject> getFilteredDeviceTokensV2(String gender, String platform, String frequency,
			String age_grp,String language, String tZone) {
		logger.info("[Enter ] getFilteredDeviceTokens " +gender+"  "+platform+"  "+frequency+"  "+age_grp);
		ResultSet rs = null;
		if(gender.equalsIgnoreCase("All")&&platform.equalsIgnoreCase("All")&&frequency.equalsIgnoreCase("All")&&age_grp.equalsIgnoreCase("All")) {
			
			ArrayList<DeviceTokenObject> deviceTokenList = new ArrayList<DeviceTokenObject>();
			
			Connection conn = null;
			PreparedStatement pstmt = null;
			try {
				conn = dataSource.getConnection();
				String query = null;
				if(language.equalsIgnoreCase("English")) {					
					if(tZone.equalsIgnoreCase("Asia/Kolkata")) {
						query = "select dp.applicationId,dp.deviceToken,tbl2.language,tbl2.gender,tbl2.timeZone ,tbl2.lastModified FROM smg_saveyra_api_user_device_premises dp LEFT JOIN smg_saveyra_api_user_settings tbl2 ON dp.applicationId=tbl2.applicationId LEFT JOIN  smg_saveyra_api_notification_preferences np ON dp.applicationId=np.applicationId where dp.status='enabled' and tbl2.language in ('English','',null) and tbl2.timeZone in('Asia/Kolkata','Asia/Calcutta','','NA') and (dp.deviceToken != '' or dp.deviceToken != null) and np.status = 1 order by tbl2.lastModified desc, tbl2.applicationId";
						pstmt = conn.prepareStatement(query);
					}else {
						query = "select dp.applicationId,dp.deviceToken,tbl2.language,tbl2.gender,tbl2.timeZone  ,tbl2.lastModified FROM smg_saveyra_api_user_device_premises dp LEFT JOIN smg_saveyra_api_user_settings tbl2 ON dp.applicationId=tbl2.applicationId LEFT JOIN  smg_saveyra_api_notification_preferences np ON dp.applicationId=np.applicationId where dp.status='enabled' and (tbl2.language = 'English' or tbl2.language = '' or tbl2.language = null) and tbl2.timeZone=? and (dp.deviceToken != '' or dp.deviceToken != null) and np.status = 1 order by tbl2.lastModified desc, tbl2.applicationId";
						pstmt = conn.prepareStatement(query);
						pstmt.setString(1, tZone);
					}
				}else {								
					if(tZone.equalsIgnoreCase("Asia/Kolkata")) {						
						query = "select dp.applicationId,dp.deviceToken,tbl2.language,tbl2.gender,tbl2.timeZone,tbl2.lastModified FROM smg_saveyra_api_user_device_premises dp LEFT JOIN smg_saveyra_api_user_settings tbl2 ON dp.applicationId=tbl2.applicationId LEFT JOIN  smg_saveyra_api_notification_preferences np ON dp.applicationId=np.applicationId where dp.status='enabled' and tbl2.language = ? and tbl2.timeZone in ('Asia/Kolkata','Asia/Calcutta','','NA') and (dp.deviceToken != '' or dp.deviceToken != null) and np.status = 1 order by tbl2.lastModified desc, tbl2.applicationId";
						pstmt = conn.prepareStatement(query);	
						pstmt.setString(1, language);
					}else {
						query = "select dp.applicationId,dp.deviceToken,tbl2.language,tbl2.gender,tbl2.timeZone  ,tbl2.lastModified FROM smg_saveyra_api_user_device_premises dp LEFT JOIN smg_saveyra_api_user_settings tbl2 ON dp.applicationId=tbl2.applicationId LEFT JOIN  smg_saveyra_api_notification_preferences np ON dp.applicationId=np.applicationId where dp.status='enabled' and tbl2.language = ? and tbl2.timeZone=? and(dp.deviceToken != '' or dp.deviceToken != null) and np.status = 1 order by tbl2.lastModified desc, tbl2.applicationId";
						pstmt = conn.prepareStatement(query);	
						pstmt.setString(1, language);
						pstmt.setString(2, tZone);
					}					
				}				
				
				rs = pstmt.executeQuery();
				
				while (rs.next()) {
					
					String lang = rs.getString(3);
					if(lang==null||lang.equals("")){
						lang = "English";
					}
					String tz = rs.getString(5);					
					if(tz==null||tz.equalsIgnoreCase("NA")||tz.equalsIgnoreCase("")||tz.equalsIgnoreCase("Asia/Calcutta")) {
						tz="Asia/Kolkata";
					}
					deviceTokenList.add(new DeviceTokenObject(rs.getString(1), rs.getString(2),"","","", lang, rs.getString(4),tz,null,""));	
				}				
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("[ERROR] SQL Fetch Fail");
			} finally {
				try {
					pstmt.close();
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}	
			//Create Device Token List 
			
			ArrayList<String> dtoListUnique = new ArrayList<String>();
			ArrayList<DeviceTokenObject> returnList = new ArrayList<DeviceTokenObject>();
			
			logger.error("[INFO] Fetched Tokens : "+deviceTokenList.size() );
			
			
			for(DeviceTokenObject dto :deviceTokenList) {
				
				if(dtoListUnique.contains(dto.getApplicationId())) {
					continue;
				}else {
					dtoListUnique.add(dto.getApplicationId());
					returnList.add(dto);
				}				
			}	
			
			logger.error("[INFO] Filtered Tokens : "+returnList.size() +"  Zone : "+tZone);				
			return returnList;
		}
		return null;
	}

	@Transactional
	public ArrayList<DeviceTokenObject> getFilteredDeviceTokens(String gender, String platform, String frequency,
			String age_grp) {
		logger.info("[Enter ] getFilteredDeviceTokens " +gender+"  "+platform+"  "+frequency+"  "+age_grp);
		ResultSet rs = null;
		if(gender.equalsIgnoreCase("All")&&platform.equalsIgnoreCase("All")&&frequency.equalsIgnoreCase("All")&&age_grp.equalsIgnoreCase("All")) {
			
			ArrayList<DeviceTokenObject> deviceTokenList = new ArrayList<DeviceTokenObject>();
			
			Connection conn = null;
			PreparedStatement pstmt = null;
			try {
				conn = dataSource.getConnection();
				String query = "select dp.applicationId,dp.deviceToken,tbl2.language,tbl2.gender,tbl2.timeZone  ,tbl2.lastModified FROM smg_saveyra_api_user_device_premises dp " + 
						"LEFT JOIN smg_saveyra_api_user_settings tbl2 ON dp.applicationId=tbl2.applicationId LEFT JOIN  smg_saveyra_api_notification_preferences np ON dp.applicationId=np.applicationId " + 
						"where dp.status='enabled' and (dp.deviceToken != '' or dp.deviceToken != null) and np.status = 1 order by tbl2.lastModified desc, tbl2.applicationId"; 
						
				pstmt = conn.prepareStatement(query);				
				rs = pstmt.executeQuery();
				
				while (rs.next()) {
					deviceTokenList.add(new DeviceTokenObject(rs.getString(1), rs.getString(2),"","","", rs.getString(3), rs.getString(4),rs.getString(5),null,""));	
				}				
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("[ERROR] SQL Fetch Fail");
			} finally {
				try {
					rs.close();
					pstmt.close();
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}	
			//Create Device Token List 
			
			ArrayList<String> dtoListUnique = new ArrayList<String>();
			ArrayList<DeviceTokenObject> returnList = new ArrayList<DeviceTokenObject>();
			
			logger.error("[INFO] Fetched Tokens : "+deviceTokenList.size());
			
			
			for(DeviceTokenObject dto :deviceTokenList) {
				
				if(dtoListUnique.contains(dto.getApplicationId())) {
					continue;
				}else {
					dtoListUnique.add(dto.getApplicationId());
					returnList.add(dto);
				}				
			}	
			
			logger.error("[INFO] Filtered Tokens : "+returnList.size());		
			
			
			return returnList;
			
		}else {/*
		List<DeviceTokenObject> deviceTokenList = new ArrayList<DeviceTokenObject>();			
		Connection dbConnection = null;
		CallableStatement callableStatement = null;

		String getValidDeviceTokens = "{call getDeviceTokens()}";

		try {
			dbConnection = dataSource.getConnection();
			callableStatement = dbConnection.prepareCall(getValidDeviceTokens);
			rs = callableStatement.executeQuery();
			while (rs.next()) {				
				deviceTokenList.add(new DeviceTokenObject(rs.getString(1), rs.getString(2),rs.getString(3),rs.getString(4), rs.getString(5),rs.getString(6),
						rs.getString(7),rs.getString(8),null,null));				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (callableStatement != null) {
				try {
					callableStatement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		
		//logger.warn("APP ID COUNT FROM Devicepremises/session : "+deviceTokenList.size());
		
		// Reduce deviceTokens		
		StringBuffer sb = new StringBuffer();
		for(DeviceTokenObject dto : deviceTokenList) {
			sb.append(dto.getApplicationId());
			if(deviceTokenList.indexOf(dto)<deviceTokenList.size()-1) {
				sb.append("','");
			}
		}

		PreparedStatement pstmt = null;
		ResultSet rs2 = null;
		String query = null;
		
		List<String> exclusionAppIdList = new ArrayList<String>();
		
		try {
			dbConnection = dataSource.getConnection();
			query = "select t.applicationID from smg_saveyra_api_notification_preferences t inner join ( select applicationID, max(lastModified) as MaxDate from smg_saveyra_api_notification_preferences where applicationId in ('"+sb.toString()+"') group by applicationId) tm on t.applicationId = tm.applicationId and t.lastModified = tm.MaxDate and t.status=0";
			pstmt = dbConnection.prepareStatement(query);
			rs2 = pstmt.executeQuery();
			while (rs2.next()) {
				exclusionAppIdList.add(rs2.getString(1));				
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				rs2.close();
				pstmt.close();
				dbConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		//logger.warn("Reduced : APP ID COUNT FROM Devicepremises/session : "+exclusionAppIdList.size());
		
		
		ArrayList<DeviceTokenObject> reducedDTListByUserPreference = new ArrayList<DeviceTokenObject>();
		StringBuffer sb2 = new StringBuffer();
		
		for (DeviceTokenObject dto : deviceTokenList) {
			if(exclusionAppIdList.contains(dto.getApplicationId())) {
				continue;
			}
			
			reducedDTListByUserPreference.add(dto);
			sb2.append(dto.getApplicationId());
			if(deviceTokenList.indexOf(dto)<deviceTokenList.size()-1) {
				sb2.append("','");
			}
		}		
		
		//logger.info("Final DeviceToken Count  : "+reducedDTListByUserPreference.size());
		
		List<String> dobList = new ArrayList<String>();
		if(!frequency.equalsIgnoreCase("All")) {
			//Get dob for reduced list
			
			try {
				dbConnection = dataSource.getConnection();
				//query = "select t.applicationID from smg_saveyra_api_notification_preferences t inner join ( select applicationID, max(lastModified) as MaxDate from smg_saveyra_api_notification_preferences where applicationId in ('"+sb.toString()+"') group by applicationId) tm on t.applicationId = tm.applicationId and t.lastModified = tm.MaxDate and t.status=0";
				query = "SELECT dob FROM smg_saveyra_api_facebook_user_profile where applicationId in ('"+sb2.toString()+"') union all SELECT dob FROM smg_saveyra_api_mobile_user_profile where applicationId in ('"+sb2.toString()+"')";
				
				pstmt = dbConnection.prepareStatement(query);
				rs2 = pstmt.executeQuery();
				while (rs2.next()) {
					dobList.add(rs2.getDate(1)==null?"2004-01-01":new SimpleDateFormat("yyyy-MM-dd").format(rs2.getDate(1)));				
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("[ERROR] SQL Fetch Fail");
			} finally {
				try {
					rs2.close();
					pstmt.close();
					dbConnection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			//UpdateDOB
			for (int i=0;i<deviceTokenList.size();i++) {
				try {
					deviceTokenList.get(i).setDob((Date) new SimpleDateFormat("yyyy-MM-dd").parse(dobList.get(i)));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					try {
						deviceTokenList.get(i).setDob((Date) new SimpleDateFormat("yyyy-MM-dd").parse("2014-01-01"));
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}	
		}
	
		Set<DeviceTokenObject> returnList = new LinkedHashSet<DeviceTokenObject>();
		
		// //logger.info("deviceTokenList "+deviceTokenList.size());
		for (DeviceTokenObject dto : reducedDTListByUserPreference) {
			// Filter with platform
			if (!platform.equalsIgnoreCase("All")) {
				if (dto.getPlatform().equalsIgnoreCase(platform)) {
					// Success
				} else {
					// Failure
					//logger.warn("FilteredAPPID By Platform");
					continue;
				}
			}

			// Filter with gender
			if (!gender.equalsIgnoreCase("All")) {
				if (dto.getGender().equalsIgnoreCase(gender)) {
					// Success
				} else {
					// Failure
					//logger.warn("FilteredAPPID By Gender");
					continue;
				}
			}

			// Filter with age_grp
			if (!age_grp.equalsIgnoreCase("All")) {
				// date Format Check
				LocalDate now = LocalDate.now();
				
				Period period = Period.between(dto.getDob().toLocalDate(), now);

				if (period.getYears() > 21 && period.getYears() < 31) {
					if (!(age_grp.equalsIgnoreCase("22-30"))) {
						//logger.warn("FilteredAPPID By age_grp");
						continue;
					}
				} else if (period.getYears() > 30 && period.getYears() < 100) {
					if (!(age_grp.equalsIgnoreCase("31-99"))) {
						//logger.warn("FilteredAPPID By age_grp");
						continue;
					}
				}
			}

			// Filter with frequency
			if (!frequency.equalsIgnoreCase("All")) {
				if (dto.getFrequency().equalsIgnoreCase(frequency)) {
					// Success
				} else {
					// Failure
					//logger.warn("FilteredAPPID By frequency");
					continue;
				}
			}
			returnList.add(dto);
		}
		//logger.info("[LEAVE] getFilteredDeviceTokens " + returnList.size());
		
		
		return returnList;
	*/}
		return null;
	}
	
	@Transactional(readOnly = true)
	public String getFrequency(String appId) {
		////logger.info("[ENTER] getFrequency");
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		try {
			conn = dataSource.getConnection();
			query = "SELECT frequency FROM smg_saveyra_api_notification_preferences where applicationId = ?";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, appId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				return rs.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	//	//logger.info("[LEAVE] getFrequency");
		return null;
	}
	
	@Transactional
	public void addScheduleV2(NotificationSchedulerV2 notSch) {
		logger.info("[ENTER] addScheduleV2");
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dataSource.getConnection();
			String query = "INSERT INTO smg_saveyra_notification_scheduler "
					+ "(n_id,message_body,language,timeZone,created_timestamp,scheduled_time)VALUES(?,?,?,?,?,?);";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, notSch.getN_id());
			pstmt.setString(2, notSch.getMessage_body());
			pstmt.setString(3, notSch.getLanguage());
			pstmt.setString(4, notSch.getTimeZone());
			pstmt.setTimestamp(5, notSch.getCreated_timestamp());
			pstmt.setTimestamp(6, notSch.getScheduled_time());
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Insert Fail [Notification Token :]" + notSch.getN_id());
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] addScheduleV2");
	}

	@Transactional
	public void addSchedule(NotificationScheduler notSch) {
		//logger.info("[ENTER] addSchedule");
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dataSource.getConnection();
			String query = "INSERT INTO smg_saveyra_notification_scheduler (n_id,message_body,url,status,created_timestamp,scheduled_time)VALUES(?,?,?,?,?,?)";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, notSch.getN_id());
			pstmt.setString(2, notSch.getMessage_body());
			pstmt.setString(3, notSch.getUrl());
			pstmt.setString(4, notSch.getStatus());
			pstmt.setTimestamp(5, notSch.getCreated_timestamp());
			pstmt.setTimestamp(6, notSch.getScheduled_time());
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Insert Fail [Notification Token :]" + notSch.getN_id());
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//logger.info("[LEAVE] addSchedule");
	}
	
	@Transactional(readOnly = true)
	public List<NotificationSchedulerV2> getScduledMessagesV2() {
		//logger.info("[ENTER] getScduledMessages");
		List<NotificationSchedulerV2> schList = new ArrayList<NotificationSchedulerV2>();
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		try {
			conn = dataSource.getConnection();
			query = "SELECT * FROM smg_saveyra_notification_scheduler where status='Scheduled' and timestamp(scheduled_time) BETWEEN timestamp((now() - INTERVAL 5 MINUTE)) AND timestamp((now() + INTERVAL 5 MINUTE)) limit 1";
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				schList.add(new NotificationSchedulerV2(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
						rs.getString(6), rs.getTimestamp(7), rs.getTimestamp(9)));
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//logger.info("[LEAVE] getScduledMessages "+schList.size());
		return schList;
	}

	@Transactional(readOnly = true)
	public List<NotificationScheduler> getScduledMessages() {
		logger.info("[ENTER] getScduledMessages");
		List<NotificationScheduler> schList = new ArrayList<NotificationScheduler>();
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		try {
			conn = dataSource.getConnection();
			conn.setAutoCommit(false);
			query = "SELECT * FROM smg_saveyra_notification_scheduler where status='Scheduled' and timestamp(scheduled_time) BETWEEN timestamp((now() - INTERVAL 5 MINUTE)) AND timestamp((now() + INTERVAL 5 MINUTE)) limit 1";
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			conn.commit();
			while (rs.next()) {
				schList.add(new NotificationScheduler(rs.getInt(1),rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getTimestamp(6), rs.getTimestamp(8)));
			}
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] getScduledMessages "+schList.size());
		return schList;
	}

	// #//Scheduler

	@Transactional
	public void save(NotificationsSent notification) {
		//logger.info("[ENTER] save");
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dataSource.getConnection();
			String query = "INSERT INTO smg_saveyra_notification_sent (created_at,message,notification_id,platform,title,image_id,navigation_to,sticker_category,thumbnail,notificationcategory,notificationtype,type,userid,status,nav_to,n_id,message_id,application_id)VALUES(now(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,(SELECT applicationId FROM smg_saveyra_api_user_device_premises WHERE deviceToken =?));";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, notification.getMessage() == null ? "" : notification.getMessage());
			pstmt.setString(2, notification.getNotification_id() == null ? "" : notification.getNotification_id());
			pstmt.setString(3, notification.getPlatform() == null ? "" : notification.getPlatform());
			pstmt.setString(4, notification.getTitle() == null ? "" : notification.getTitle());
			pstmt.setString(5, notification.getImage_id() == null ? "" : notification.getImage_id());
			pstmt.setString(6, notification.getNavigation_to() == null ? "" : notification.getNavigation_to());
			pstmt.setString(7, notification.getSticker_category() == null ? "" : notification.getSticker_category());
			pstmt.setString(8, notification.getThumbnail() == null ? "" : notification.getThumbnail());
			pstmt.setString(9,
					notification.getNotificationcategory() == null ? "" : notification.getNotificationcategory());
			pstmt.setString(10, notification.getNotificationType() == null ? "" : notification.getNotificationType());
			pstmt.setString(11, notification.getType() == null ? "" : notification.getType());
			pstmt.setString(12, notification.getUserid() == null ? "" : notification.getUserid());
			pstmt.setString(13, notification.getStatus() == null ? "" : notification.getStatus());
			pstmt.setString(14, notification.getNav_to() == null ? "" : notification.getNav_to());
			pstmt.setString(15, notification.getN_id() == null ? "" : notification.getN_id());
			pstmt.setString(16, notification.getMessage_id() == null ? "" : notification.getMessage_id());
			pstmt.setString(17, notification.getNotification_id() == null ? "" : notification.getNotification_id());
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Insert Fail [Notification Token :]" + notification.getNotification_id());
		} finally {
			try {
				if (!pstmt.isClosed()) {
					pstmt.close();
				}
				if (!conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//logger.info("[LEAVE] save");
	}

	@Transactional
	public void disableDeviceToken(String deviceTokenList) {
		//logger.info("[ENTER] disableDeviceToken :" + deviceTokenList);
		if (deviceTokenList == null || deviceTokenList.equals("")) {
			//logger.info("[LEAVE] disableDeviceToken - No Device to disable");
			return;
		}
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dataSource.getConnection();
			String query = "UPDATE smg_saveyra_api_user_device_premises SET status = 'disabled' WHERE deviceToken IN ("
					+ deviceTokenList + ")";

			// //logger.info("QUERY \n\n "+query+"\n\n");
			pstmt = conn.prepareStatement(query);
			// pstmt.setString(1, deviceTokenList);
			//logger.info("[SQL Execute] disableDeviceToken" + pstmt.toString());
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//logger.info("[LEAVE] disableDeviceToken");
	}

	@Transactional(readOnly = true)
	public Map<String, String> findCreatedTime(String instanceIDList, String type) {
		//logger.info("[ENTER] findCretaedTime");
		Map<String, String> resultMap = new HashMap<String, String>();
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		try {
			conn = dataSource.getConnection();
			switch (type) {
			case "promotion":
				query = "SELECT createdOn,instanceId FROM smg_saveyra_api_home_screen_promotions where instanceId in ("
						+ instanceIDList + ")";
				break;
			case "news":
				query = "SELECT createdOn,instanceId FROM smg_saveyra_api_home_screen_news where instanceId in ("
						+ instanceIDList + ")";
				break;
			case "card":
				query = "SELECT createdOn,instanceId FROM smg_saveyra_api_home_screen_cards where instanceId in ("
						+ instanceIDList + ")";
				break;
			case "feature":
				query = "SELECT createdOn,instanceId FROM smg_saveyra_api_home_screen_features where instanceId in ("
						+ instanceIDList + ")";
				break;
			default:
				// //logger.info("Error ......");
				return null;
			}

			pstmt = conn.prepareStatement(query);
			// pstmt.setString(1, instanceIDList);
			// //logger.info("ID LIST : " + instanceIDList + "\nQUERY " +
			// pstmt.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				// //logger.info(rs.getString(2)+" ---- "+ rs.getString(1));
				resultMap.put(rs.getString(2), rs.getString(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {			
			try {
				pstmt.close();
				rs.close();				
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
		//logger.info("[LEAVE] findCretaedTime");
		return resultMap;
	}

	@Transactional
	public List<String> getDeviceTokens(String gender, String platform, String frequency, String age_grp,
			String language) {
		//logger.info("[ENTER] getDeviceTokens");
		Set<String> uniqueList = new HashSet<String>();
		List<String> deviceTokenList = new ArrayList<String>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int count = 0;
		try {
			conn = dataSource.getConnection();
			String query = "select distinct(d.deviceToken),d.platform,'' as fuser,'' as mkey, case when m_user.dob='' or m_user.dob is null then '2004-01-01' else m_user.dob end as m_dob,case when f_user.dob='' or f_user.dob is null then '2004-01-01' else f_user.dob end as f_dob,case when notify.frequency='' or notify.frequency is null then 'Once a day' else notify.frequency end as frequency, ifnull(notify.status,1) as status,ifnull(m_user.gender,'') as m_gender ,ifnull(f_user.gender,'') as f_gender ,s.language from smg_saveyra_api_user_device_premises d left join smg_saveyra_api_notification_preferences notify on d.applicationId=notify.applicationId left join smg_saveyra_api_mobile_user_profile m_user on d.applicationId=m_user.applicationId left join smg_saveyra_api_facebook_user_profile f_user on d.applicationId=f_user.applicationId left join smg_saveyra_api_user_session ss on d.applicationId=ss.applicationId and m_user.m_key = ss.m_key and f_user.f_key=ss.f_key left join smg_saveyra_api_user_settings s on d.applicationId=s.applicationId  where d.status='enabled' and (notify.status=1 or notify.status is null) and d.deviceToken is not null and d.deviceToken!='' group by d.deviceToken,d.platform, f_user.f_key,m_user.m_key,f_dob,m_dob,frequency,notify.status,f_gender,m_gender,s.language";
			pstmt = conn.prepareStatement(query);
			// //logger.info("[SQL Execute] disableDeviceToken" + pstmt.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				/*
				 * //logger.info("[INFO ]"+ rs.getString(1) + "  " + rs.getString(2) + " - " +
				 * rs.getString(5) + " - " + rs.getString(6) + " - " + rs.getString(5) + " - " +
				 * rs.getString(6) + " - " + rs.getString(7) + " - " + rs.getString(8) + " - " +
				 * rs.getString(9)+ " - " + rs.getString(10)+ " -- " + rs.getString(11));
				 */
				// Platform
				if (!platform.equalsIgnoreCase("All")) {
					if (!rs.getString(2).equalsIgnoreCase(platform)) {
						// //logger.info("Skipping for platform "+platform+" received :
						// "+rs.getString(2));
						continue;
					}
				}

				// Frequency
				if (!frequency.equalsIgnoreCase("All")) {
					if (rs.getString(5) != null && !rs.getString(5).equalsIgnoreCase("")
							&& !rs.getString(5).equalsIgnoreCase(frequency)) {
						// //logger.info("Skipping for platform "+platform+" received :
						// "+rs.getString(2));
						continue;
					}
				}

				// Age
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

				if ((rs.getString(5) == null || rs.getString(5).equals(""))
						&& (rs.getString(6) == null || rs.getString(6).equals(""))) {
					// m_date and f_date are NULL
					if (!(age_grp.equalsIgnoreCase("13-21") || age_grp.equalsIgnoreCase("All"))) {
						continue;
					}
				} else if (rs.getString(5) != null && !(rs.getString(5).equals(""))) {
					LocalDate localDate = null;
					String[] splitDate = rs.getString(5).split("-");
					if (splitDate[1].length() < 2) {
						splitDate[1] = 0 + splitDate[1];
					}
					if (splitDate[2].length() < 2) {
						splitDate[2] = 0 + splitDate[2];
					}
					String joinedDate = splitDate[0] + "-" + splitDate[1] + "-" + splitDate[2];
					localDate = LocalDate.parse(joinedDate, formatter);

					LocalDate now = LocalDate.now();
					Period period = Period.between(localDate, now);

					// //logger.info("Calculated AGE : "+period.getYears());

					if (period.getYears() > 21 && period.getYears() < 31) {
						if (!(age_grp.equalsIgnoreCase("22-30") || age_grp.equalsIgnoreCase("All"))) {
							// //logger.info("Skipping for Age >< 22-30 ");
							continue;
						}
					} else if (period.getYears() > 30 && period.getYears() < 100) {
						if (!(age_grp.equalsIgnoreCase("31-99") || age_grp.equalsIgnoreCase("All"))) {
							// //logger.info("Skipping for Age >< 31-99");
							continue;
						}
					}
				} else if (rs.getString(6) != null && !(rs.getString(6).equals(""))) {
					LocalDate localDate = null;
					String[] splitDate = rs.getString(5).split("-");
					if (splitDate[1].length() < 2) {
						splitDate[1] = 0 + splitDate[1];
					}
					if (splitDate[2].length() < 2) {
						splitDate[2] = 0 + splitDate[2];
					}
					String joinedDate = splitDate[0] + "-" + splitDate[1] + "-" + splitDate[2];
					localDate = LocalDate.parse(joinedDate, formatter);

					LocalDate now = LocalDate.now();
					Period period = Period.between(localDate, now);

					if (period.getYears() > 21 && period.getYears() < 31) {
						if (!(age_grp.equalsIgnoreCase("22-30") || age_grp.equalsIgnoreCase("All"))) {
							continue;
						}
					} else if (period.getYears() > 30 && period.getYears() < 100) {
						if (!(age_grp.equalsIgnoreCase("31-99") || age_grp.equalsIgnoreCase("All"))) {
							continue;
						}
					}
				}

				// Language
				if (!language.equalsIgnoreCase("Default")) {
					if (language.equalsIgnoreCase("English") && (rs.getString(11) == null
							|| rs.getString(11).equalsIgnoreCase("English") || rs.getString(11).equalsIgnoreCase("null")
							|| rs.getString(11).equalsIgnoreCase(""))) {
						// Add English
					} else if (language.equalsIgnoreCase(rs.getString(11))) {
						// Add Local Language
					} else {
						// Skip
						continue;
					}
				}
				uniqueList.add(rs.getString(1));
			}
			for (String s : uniqueList) {
				deviceTokenList.add(s);
			}
			count = uniqueList.size();

			logger.error("[INFO] COUNT : " + count);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//logger.info("[LEAVE] getDeviceTokens");
		return deviceTokenList;
	}

	@Transactional
	public Map<String, List<String>> getValidDeviceTokens() {
		//logger.info("[ENTER] getValidDeviceTokens");
		Map<String, List<String>> deviceTokenMap = new HashMap<String, List<String>>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = dataSource.getConnection();
			String query = "select distinct(d.deviceToken),d.platform,'' as fuser,'' as mkey, case when m_user.dob='' or m_user.dob is null then '2004-01-01' else m_user.dob end as m_dob,case when f_user.dob='' or f_user.dob is null then '2004-01-01' else f_user.dob end as f_dob,case when notify.frequency='' or notify.frequency is null then 'Once a day' else notify.frequency end as frequency, ifnull(notify.status,1) as status,ifnull(m_user.gender,'') as m_gender ,ifnull(f_user.gender,'') as f_gender ,s.language from smg_saveyra_api_user_device_premises d left join smg_saveyra_api_notification_preferences notify on d.applicationId=notify.applicationId left join smg_saveyra_api_mobile_user_profile m_user on d.applicationId=m_user.applicationId left join smg_saveyra_api_facebook_user_profile f_user on d.applicationId=f_user.applicationId left join smg_saveyra_api_user_session ss on d.applicationId=ss.applicationId and m_user.m_key = ss.m_key and f_user.f_key=ss.f_key left join smg_saveyra_api_user_settings s on d.applicationId=s.applicationId  where d.status='enabled' and (notify.status=1 or notify.status is null) and d.deviceToken is not null and d.deviceToken!='' group by d.deviceToken,d.platform, f_user.f_key,m_user.m_key,f_dob,m_dob,frequency,notify.status,f_gender,m_gender,s.language";
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				List<String> uniqueList = new ArrayList<String>();
				for (int i = 1; i < 12; i++) {
					uniqueList.add(rs.getString(i));
				}
				deviceTokenMap.put(rs.getString(1), uniqueList);
			}
			//logger.info("[INFO ] All Count  " + deviceTokenMap.size());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//logger.info("[LEAVE] getValidDeviceTokens");
		return deviceTokenMap;
	}

	@Transactional
	public List<String> getExclusionDeviceTokens(String gender, String platform, String frequency, String age_grp,
			String[] language) {
		//logger.info("[ENTER] getExclusionDeviceTokens");
		Set<String> uniqueList = new HashSet<String>();
		List<String> deviceTokenList = new ArrayList<String>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = dataSource.getConnection();
			String query = "select distinct(d.deviceToken),d.platform,'' as fuser,'' as mkey, case when m_user.dob='' or m_user.dob is null then '2004-01-01' else m_user.dob end as m_dob,case when f_user.dob='' or f_user.dob is null then '2004-01-01' else f_user.dob end as f_dob,case when notify.frequency='' or notify.frequency is null then 'Once a day' else notify.frequency end as frequency, ifnull(notify.status,1) as status,ifnull(m_user.gender,'') as m_gender ,ifnull(f_user.gender,'') as f_gender ,s.language from smg_saveyra_api_user_device_premises d left join smg_saveyra_api_notification_preferences notify on d.applicationId=notify.applicationId left join smg_saveyra_api_mobile_user_profile m_user on d.applicationId=m_user.applicationId left join smg_saveyra_api_facebook_user_profile f_user on d.applicationId=f_user.applicationId left join smg_saveyra_api_user_session ss on d.applicationId=ss.applicationId and m_user.m_key = ss.m_key and f_user.f_key=ss.f_key left join smg_saveyra_api_user_settings s on d.applicationId=s.applicationId  where d.status='enabled' and (notify.status=1 or notify.status is null) and d.deviceToken is not null and d.deviceToken!='' group by d.deviceToken,d.platform, f_user.f_key,m_user.m_key,f_dob,m_dob,frequency,notify.status,f_gender,m_gender,s.language";
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			int count = 0;

			while (rs.next()) {
				// gender
				if (!gender.equalsIgnoreCase("All")) {
					if (!gender.equalsIgnoreCase(rs.getString(10))) {
						continue;
					}
				}

				// Platform
				if (!platform.equalsIgnoreCase("All")) {
					if (!rs.getString(2).equalsIgnoreCase(platform)) {
						continue;
					}
				}

				// Frequency
				if (!frequency.equalsIgnoreCase("All")) {
					if (rs.getString(7) != null && !rs.getString(7).equalsIgnoreCase("")
							&& !rs.getString(7).equalsIgnoreCase(frequency)) {
						continue;
					}
				}

				// Age
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

				// //logger.info("\t[INFO ] DATE 1 "+rs.getString(5)+" -DATE 2 "+rs.getString(6));

				if ((rs.getString(5) == null || rs.getString(5).equals("2004-01-01"))
						&& (rs.getString(6) == null || rs.getString(6).equals("2004-01-01"))) {
					// m_date and f_date are NULL

					// //logger.info("\t[INFO ] Skipped for AGE for NULL "+rs.getString(5)+" --
					// "+rs.getString(6));

					if (!(age_grp.equalsIgnoreCase("13-21") || age_grp.equalsIgnoreCase("All"))) {
						// //logger.info("Skipping for Ager Group");
						// //logger.info("\t[INFO ] Skipped for AGE for NULL");
						continue;
					}
				} else if (rs.getString(5) != null && !(rs.getString(5).equals(""))) {
					LocalDate localDate = null;

					String[] splitDate = rs.getString(5).split("-");
					if (splitDate[1].length() < 2) {
						splitDate[1] = 0 + splitDate[1];
					}
					if (splitDate[2].length() < 2) {
						splitDate[2] = 0 + splitDate[2];
					}
					String joinedDate = splitDate[0] + "-" + splitDate[1] + "-" + splitDate[2];
					localDate = LocalDate.parse(joinedDate, formatter);

					LocalDate now = LocalDate.now();
					Period period = Period.between(localDate, now);

					// //logger.info("\t[INFO ] Calculated Mobile AGE : "+period.getYears());

					if (period.getYears() > 21 && period.getYears() < 31) {
						if (!(age_grp.equalsIgnoreCase("22-30") || age_grp.equalsIgnoreCase("All"))) {
							// //logger.info("Skipping for Age >< 22-30 ");
							// //logger.info("\t[INFO ] Skipped for AGE for COUNT 1");
							continue;
						}
					} else if (period.getYears() > 30 && period.getYears() < 100) {
						if (!(age_grp.equalsIgnoreCase("31-99") || age_grp.equalsIgnoreCase("All"))) {
							// //logger.info("Skipping for Age >< 31-99");
							// //logger.info("\t[INFO ] Skipped for AGE for Count 2");
							continue;
						}
					}
				} else if (rs.getString(6) != null && !(rs.getString(6).equals(""))) {
					// f_date is NULL
					// //logger.info("FB Date is not NULL");
					LocalDate localDate = null;
					String[] splitDate = rs.getString(5).split("-");
					if (splitDate[1].length() < 2) {
						splitDate[1] = 0 + splitDate[1];
					}
					if (splitDate[2].length() < 2) {
						splitDate[2] = 0 + splitDate[2];
					}
					String joinedDate = splitDate[0] + "-" + splitDate[1] + "-" + splitDate[2];
					localDate = LocalDate.parse(joinedDate, formatter);

					LocalDate now = LocalDate.now();
					Period period = Period.between(localDate, now);
					// //logger.info("\t[INFO ] Calculated FB AGE : "+period.getYears());

					if (period.getYears() > 21 && period.getYears() < 31) {
						if (!(age_grp.equalsIgnoreCase("22-30") || age_grp.equalsIgnoreCase("All"))) {
							// //logger.info("Skipping for Age >< 22-30 ");
							// //logger.info("\t[INFO ] Skipped for AGE FB COUNT 1");
							continue;
						}
					} else if (period.getYears() > 30 && period.getYears() < 100) {
						if (!(age_grp.equalsIgnoreCase("31-99") || age_grp.equalsIgnoreCase("All"))) {
							// //logger.info("Skipping for Age >< 31-99");
							// //logger.info("\t[INFO ] Skipped for AGE FB COUNT 2");
							continue;
						}
					}
				}

				// Language Exclusion
				boolean addFlag = true;

				for (String lang : language) {
					if (lang.trim().equalsIgnoreCase("English")) {
						// ENGLISH
						if (rs.getString(11) == null || rs.getString(11).equalsIgnoreCase("English")
								|| rs.getString(11).equalsIgnoreCase("null") || rs.getString(11).equalsIgnoreCase("")) {
							addFlag = false;
							//logger.info("\t[INFO ] Skipping ENG Lang " + lang + "   " + rs.getString(11));
							break;
						}
					}
					if (rs.getString(11) != null && lang.trim().equalsIgnoreCase(rs.getString(11).trim())) {
						// Other Language
						addFlag = false;
						//logger.info("\t[INFO ] Skipping OTHR Lang " + lang + "    " + rs.getString(11));
						break;
					} else {
						//logger.info("\t[INFO ] ADDING OTHR Lang " + lang + "    " + rs.getString(11));
						addFlag = true;
					}
				}
				if (addFlag) {
					uniqueList.add(rs.getString(1));
				}
			}

			for (String s : uniqueList) {
				deviceTokenList.add(s);
			}
			count = uniqueList.size();

			//logger.info("\n\n[INFO ] COUNT " + count);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//logger.info("[LEAVE] getExclusionDeviceTokens");
		return deviceTokenList;
	}
	
	@Transactional(readOnly = true)
	public List<String> getUniquTimeZoneByLanguage(String language) {
		logger.info("[ENTER] getUniquTimeZoneByLanguage");
		List<String> tzList = new ArrayList<String>();
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		try {
			conn = dataSource.getConnection();
			query = "SELECT distinct(timeZone) FROM smg_saveyra_api_user_settings where language =?";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1,language);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				tzList.add(rs.getString(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] getUniquTimeZoneByLanguage "+tzList.size());
		return tzList;
	}
	
}
