package com.saveyra.notification.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.saveyra.notification.model.AuditTrail;

@Repository
public interface AuditTrailRepository extends CrudRepository<AuditTrail, Long> {

}
