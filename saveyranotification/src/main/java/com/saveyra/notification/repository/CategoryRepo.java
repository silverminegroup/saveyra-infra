package com.saveyra.notification.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.saveyra.notification.model.CategoryModel;

@Repository
public class CategoryRepo {

	private static final Logger logger = LoggerFactory.getLogger(MysqlRepo.class);

	@SuppressWarnings("unused")
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	DataSource dataSource;
	
	@Transactional
	public void addCategory(CategoryModel cm) {
		logger.info("[ENTER] addCategory");
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dataSource.getConnection();
			String query = "INSERT INTO smg_saveyra_api_avatar_expression_category (categoryId,category,subCategory,metaTags,layers,createdon,lastModified,status,expression) VALUES ((select uuid()),?,?,?,?,(select now()),(select now()),?,?)";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, cm.getCategory());
			pstmt.setString(2, cm.getSubCategory());
			pstmt.setString(3, cm.getMetaTags());
			pstmt.setString(4, cm.getLayers());
			pstmt.setString(5, cm.getStatus());
			pstmt.setString(6, cm.getExpression());
			
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Insert Fail");
		} finally {
			try {
				if(pstmt!=null) {
					pstmt.close();
				}					
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] addCategory");
	}


}
