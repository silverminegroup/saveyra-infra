package com.saveyra.notification.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.saveyra.notification.model.SaveyraNotification;

@Repository
public interface SaveyraNotificationRepository extends JpaRepository<SaveyraNotification, Long> {
	/* server Queries */
	
	  @Query(value =
	  "SELECT * FROM smg_saveyra_notification ", nativeQuery =
	  true) public List<SaveyraNotification> findALL();
	  
	  @Query(value =
			  "SELECT id,notificationtitle,message,navigation_to,thumbnail,image_id,concat(scheduledate,' ',scheduletime) as createdDate FROM smg_saveyra_notification where date(expirydate) >=  CURDATE() and notificationdelivery like '%NC%'", nativeQuery =
			  true) public List<SaveyraNotification> findNotifications();
	  
	  @Query(value =
	  "SELECT * FROM smg_saveyra_notification t WHERE t.flag = '0'"
	  , nativeQuery = true) public List<SaveyraNotification> findByStopped();
	  
	  @Query(value =
	  "SELECT * FROM smg_saveyra_notification t WHERE t.flag = '1'"
	  , nativeQuery = true) public List<SaveyraNotification> findByRunning();
	  
	  @Query(value =
	  "SELECT * FROM smg_saveyra_notification t WHERE t.flag = '2'"
	  , nativeQuery = true) public List<SaveyraNotification> findByCompleted();   
	  
	  // public List<SaveyraNotification> findByall1(Date sqlDate1,Date sqlDate);
	  
	  @Query(value = "select * from smg_saveyra_notification b where b.scheduledate between ?1 and ?2 order by scheduledate"
	  , nativeQuery = true)
	  List<SaveyraNotification> findByDatesBetween(String formatted, String toformated);
	 
	  @Query(value
	  ="select * from smg_saveyra_notification where expirydate > sysdate() and notificationdelivery  like '%NC%'"
	  ,nativeQuery = true) public List<SaveyraNotification> findbynotification();
	  
	  @Query(
	  value="select * from smg_saveyra_notification b where b.scheduledate between ?1 and ?2  and platform=?3"
	  ,nativeQuery = true)//and notificationtype=?3 public
	  List<SaveyraNotification> findbytype(String formatted, String
	  toformated,String platfrom);//String type,
	  
	  @Query(
	  value="select * from smg_saveyra_notification b where b.scheduledate between ?1 and ?2  and notificationtype=?3 and platform=?4"
	  ,nativeQuery = true)//and notificationtype=?3 public
	  List<SaveyraNotification> findbytype1(String formatted, String
	  toformated,String type,String platfrom);
	  
	  
	  @Query(
	  value="select * from smg_saveyra_notification b  where b.scheduledate between ?1 and ?2 and platform='Android' or platform='iOS' or platform='All'"
	  ,nativeQuery = true) public List<SaveyraNotification> findbytype2(String
	  formatted, String toformated,String type);
	  
	  @Query(
	  value="select * from smg_saveyra_notification b where b.scheduledate between ?1 and ?2  and b.notificationtype=?3 and (b.platform='Android' or b.platform='iOS' or b.platform='All')"
	  ,nativeQuery = true)//and notificationtype=?3 public
	  List<SaveyraNotification> findbytype3(String formatted, String
	  toformated,String type,String platfrom);
	  
	
	 
	// Local Database Queries
	/*@Query(value = "SELECT * FROM smg_saveyra_notification ", nativeQuery = true)
	public List<SaveyraNotification> findALL();

	@Query(value = "SELECT * FROM smg_saveyra_notification t WHERE t.flag = '0'", nativeQuery = true)
	public List<SaveyraNotification> findByStopped();

	@Query(value = "SELECT * FROM smg_saveyra_notification t WHERE t.flag = '1'", nativeQuery = true)
	public List<SaveyraNotification> findByRunning();

	@Query(value = "SELECT * FROM smg_saveyra_notification t WHERE t.flag = '2'", nativeQuery = true)
	public List<SaveyraNotification> findByCompleted();

	// public List<SaveyraNotification> findByall1(Date sqlDate1,Date sqlDate);

	@Query(value = "select * from smg_saveyra_notification b where b.scheduledate between ?1 and ?2 order by scheduledate", nativeQuery = true)

	List<SaveyraNotification> findByDatesBetween(String formatted, String toformated);

	@Query(value = "select * from smg_saveyra_notification where expirydate > sysdate() and notificationdelivery  like '%NC%'", nativeQuery = true)
	public List<SaveyraNotification> findbynotification();

	@Query(value = "select * from smg_saveyra_notification b where b.scheduledate between ?1 and ?2  and platform=?3", nativeQuery = true) // and
																																			// notificationtype=?3
	public List<SaveyraNotification> findbytype(String formatted, String toformated, String platfrom);// String type,

	@Query(value = "select * from smg_saveyra_notification b where b.scheduledate between ?1 and ?2  and notificationtype=?3 and platform=?4", nativeQuery = true) // and
																																									// notificationtype=?3
	public List<SaveyraNotification> findbytype1(String formatted, String toformated, String type, String platfrom);

	@Query(value = "select * from smg_saveyra_notification b  where b.scheduledate between ?1 and ?2 and platform='Android' or platform='iOS' or platform='All'", nativeQuery = true)
	public List<SaveyraNotification> findbytype2(String formatted, String toformated, String type);

	@Query(value = "select * from smg_saveyra_notification b where b.scheduledate between ?1 and ?2  and b.notificationtype=?3 and (b.platform='Android' or b.platform='iOS' or b.platform='All')", nativeQuery = true) // and
																																																						// notificationtype=?3
	public List<SaveyraNotification> findbytype3(String formatted, String toformated, String type, String platfrom);
*/
}