package com.saveyra.notification.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.saveyra.notification.model.DeviceTokenObject;
import com.saveyra.notification.model.ScheduleNotificationJson;
import com.saveyra.notification.repository.MysqlRepo;
import com.saveyra.notification.scheduler.model.MessageBody;
import com.saveyra.notification.scheduler.model.MessageObject;
import com.saveyra.notification.scheduler.model.NotificationJson;
import com.saveyra.notification.scheduler.model.NotificationScheduler;
import com.saveyra.notification.scheduler.model.NotificationSchedulerV2;

@RestController
@RequestMapping("/")
public class ScheduleController {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(ScheduleController.class);
	
	@Autowired
	MysqlRepo repo;
	
	
	@CrossOrigin
	@PostMapping("/scheduleNotificationV2")
	public ResponseEntity<String> scheduleNotificationV2(@Valid @RequestBody String json) throws IOException {
		logger.info("\n[Enter] scheduleNotificationV2 , Request : \n" + json);
		HttpHeaders respHeader = new HttpHeaders();
		respHeader.set("Content-Type", "application/json");		
		NotificationJson nj = new Gson().fromJson(json, NotificationJson.class);		
	   
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		try {
			if(new Date().compareTo(sdf.parse(nj.getScheduled_time())) > 0) {
				ResponseEntity<String> resp = new ResponseEntity<String>(
						"{\"message\":\"Request Complete\",\"Status\":\"Time Expired\"}",
						respHeader, HttpStatus.BAD_REQUEST);
				return resp;
			}		
		}catch(Exception e) {
			ResponseEntity<String> resp = new ResponseEntity<String>(
					"{\"message\":\"Request Complete\",\"Status\":\"Time Expired\"}",
					respHeader, HttpStatus.BAD_REQUEST);
			return resp;
		}		
		
		for(MessageObject mo : nj.getData()) {
			//Prepare and add to scheduler		
			Timestamp scheduledtime = null;
			try {
				scheduledtime = new Timestamp(sdf.parse(nj.getScheduled_time()).getTime());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(scheduledtime == null) {
				ResponseEntity<String> resp = new ResponseEntity<String>(
						"{\"message\":\"Request Complete\",\"Status\":\"Unparsable Scheduled Time\"}",
						respHeader, HttpStatus.BAD_REQUEST);
				return resp;
			}
			
			//Get Unique timeZones
			ArrayList<String> uqTzList = (ArrayList<String>) repo.getUniquTimeZoneByLanguage(mo.getLanguage());
			
			
			Set<String> tzList= new HashSet<String>();
			
			for(String tz:uqTzList ){				
				if(tz==null||tz.equalsIgnoreCase("")||tz.equalsIgnoreCase("NA")||tz.equalsIgnoreCase("Asia/Calcutta")) {
					tz="Asia/Kolkata";
				}			
				tzList.add(tz);				
			}
			
			
			for(String tz:tzList) {
				
				logger.info("Adding Zone "+tz);
				if(getZoneBasedDateTime(nj.getScheduled_time(),tz)==null) {
					logger.info("\n[DEBUG] Time Expired / Error in Zone");
					continue;
				}else {					
					Timestamp scheduledtimebyZone =new Timestamp(getZoneBasedDateTime(nj.getScheduled_time(),tz).getTime());
					Timestamp createdTime = new Timestamp(System.currentTimeMillis());
					
				NotificationSchedulerV2 nSched = new NotificationSchedulerV2(0, mo.getMessage_body().getN_id(), new Gson().toJson(mo.getMessage_body()),mo.getLanguage(),tz,"Scheduled",createdTime,scheduledtimebyZone);
				repo.addScheduleV2(nSched);
				}
			}			
		}			
		logger.info("\n[Leave] scheduleNotificationV2");
		ResponseEntity<String> resp = new ResponseEntity<String>(
				"{\"message\":\"Request Complete\",\"Status\":\"Success\"}",
				respHeader, HttpStatus.OK);
		return resp;
	}
	
	@CrossOrigin
	@PostMapping("/scheduleNotification")
	public ResponseEntity<String> scheduleNotification(@Valid @RequestBody String json) throws IOException {
		logger.info("\n[Enter] ScheduleNotification , Request : \n" + json);
		HttpHeaders respHeader = new HttpHeaders();
		respHeader.set("Content-Type", "application/json");		
		NotificationJson nj = new Gson().fromJson(json, NotificationJson.class);		
	   
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		try {
			if(new Date().compareTo(sdf.parse(nj.getScheduled_time())) > 0) {
				ResponseEntity<String> resp = new ResponseEntity<String>(
						"{\"message\":\"Request Complete\",\"Status\":\"Time Expired\"}",
						respHeader, HttpStatus.BAD_REQUEST);
				return resp;
			}		
		}catch(Exception e) {
			ResponseEntity<String> resp = new ResponseEntity<String>(
					"{\"message\":\"Request Complete\",\"Status\":\"Time Expired\"}",
					respHeader, HttpStatus.BAD_REQUEST);
			return resp;
		}
		
		
		 //get Filtered Device Tokens
		ArrayList<DeviceTokenObject> filteredDevicetokenList = repo.getFilteredDeviceTokens(nj.getGender(),nj.getData().get(0).getMessage_body().getPlatform(),nj.getFrequency(),nj.getAge_grp());
		
		//Create Map for sameZone tokens
		Map<String,List<String>> devicetokenMapByZone = new HashMap<String,List<String>>();		
		
		//Create Map for language and Device Token
		Map<String,String> devicetokenMapByLanguage = new HashMap<String,String>();
		
		
		
				
		for(DeviceTokenObject dto:filteredDevicetokenList) {			
			
			if(dto.getTimeZone()==null || dto.getTimeZone().equals("")||dto.getTimeZone().equals("NA")) {
				dto.setTimeZone("Asia/Kolkata");
			}			
			
			//check if key exists
			if(!devicetokenMapByZone.keySet().contains(dto.getTimeZone())) {
				List<String> dtListByZone = new ArrayList<String>();				
				dtListByZone.add(dto.getDeviceToken());								
				devicetokenMapByZone.put(dto.getTimeZone(), dtListByZone);
			}else {
				List<String> dtListByZone = new ArrayList<String>();
				dtListByZone.addAll(devicetokenMapByZone.get(dto.getTimeZone()));
				if(!dtListByZone.contains(dto.getDeviceToken())) {
					dtListByZone.add(dto.getDeviceToken());
				}
				devicetokenMapByZone.put(dto.getTimeZone(), dtListByZone);
			}				
			devicetokenMapByLanguage.put(dto.getDeviceToken(),dto.getLanguage());
		}
			
		//logger.info("\n [DEBUG] devicetokenMapByZone : "+devicetokenMapByZone.size());

		
		//logger.info("\n [DEBUG] devicetokenMapByZone : "+devicetokenMapByZone.size());
		
		//Process for each zone
		for(String zone : devicetokenMapByZone.keySet()) {
			logger.info("\n [Debug] Processing Zone "+zone);
			//Process All Requested languages Sequentially
			//Container for language based same zone device tokens			
			for(MessageObject mObj :nj.getData()) {
				//logger.info("\n \t[Debug] Processing Language "+mObj.getLanguage()+" For Zone : "+zone);
				List<String> dtListZoneLanguage = new ArrayList<String>();
				//Get device tokens for current zone
				List<String> dtListByZone = new ArrayList<String>();
				dtListByZone.addAll(devicetokenMapByZone.get(zone));
				if(dtListByZone != null) {
					for(String dt:dtListByZone) {
						//Check for language
						if(!mObj.getLanguage().trim().equalsIgnoreCase("All")) {
							if(mObj.getLanguage().trim().equalsIgnoreCase("English")) {
								if(devicetokenMapByLanguage.get(dt)== null || devicetokenMapByLanguage.get(dt).trim().equalsIgnoreCase("English")) {
									dtListZoneLanguage.add(dt);
								}
							}else {
								if(devicetokenMapByLanguage.get(dt)!= null && devicetokenMapByLanguage.get(dt).trim().equalsIgnoreCase(mObj.getLanguage().trim())) {
									dtListZoneLanguage.add(dt);
								}
							}
						}else {
							dtListZoneLanguage.add(dt);
						}						
					}
				}
				
				//logger.info("\n [Debug]-- "+zone+"  dtListZoneLanguage "+dtListZoneLanguage.size()+"  for language "+mObj.getLanguage());
				
				
				if(dtListZoneLanguage.size()>0) {
					//Prepare message body
					MessageBody mBody = mObj.getMessage_body();
					Set<String> setZoneLang = new HashSet<String>();					
					setZoneLang.addAll(dtListZoneLanguage);
					mBody.setNotificationId(setZoneLang);
					// Call schedule Notification 		
					String sUrl = "";
					switch(nj.getUrl()) {
					case "prod":
						sUrl = "https://saveyra.com/saveyranotification-v1.1/bulkPushNotification";
						break;
					case "qa":
						sUrl = "https://qa.saveyra.com/saveyranotification-v1.1/bulkPushNotification";
						break;
					default:
						sUrl = "https://qa.saveyra.com/saveyranotification-v1.1/bulkPushNotification";
					}
					
					String strNid = mBody.getN_id();
					String strBody = new Gson().toJson(mBody);	
					if(getZoneBasedDateTime(nj.getScheduled_time(),zone)==null) {
						logger.info("\n[DEBUG] Time Expired / Error in Zone");
						continue;
					}else {
					
						Timestamp scheduledtime =new Timestamp(getZoneBasedDateTime(nj.getScheduled_time(),zone).getTime());
						Timestamp createdTime = new Timestamp(System.currentTimeMillis());
						logger.info("\n [Debug] COUNT : "+mBody.getNotificationId().size()+"  "+strNid+"  "+strBody+"  "+sUrl+" Scheduled "+createdTime+"  "+scheduledtime);
					NotificationScheduler nSched = new NotificationScheduler(0,strNid,strBody,sUrl,"Scheduled",createdTime,scheduledtime);
					repo.addSchedule(nSched);
					}
				}	else {
					logger.info("\n[DEBUG] No Tokens for language "+mObj.getLanguage());
				}			
			}			
		}		
		logger.info("\n[Leave] ScheduleNotification");
		ResponseEntity<String> resp = new ResponseEntity<String>(
				"{\"message\":\"Request Complete\",\"Status\":\"Success\"}",
				respHeader, HttpStatus.OK);
		return resp;
	}
	
	
	@CrossOrigin
	@PostMapping("/scheduleNotificationByToken")
	public ResponseEntity<String> scheduleNotificationByToken(@Valid @RequestBody String json) throws IOException {
		logger.info("\n[Enter] ScheduleNotification , Request : \n" + json);
		ResponseEntity<String> resp = null;		
		HttpHeaders respHeader = new HttpHeaders();
		respHeader.set("Content-Type", "application/json");		
		ScheduleNotificationJson nj = new Gson().fromJson(json, ScheduleNotificationJson.class);		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");		
		NotificationScheduler nSched;		
		try {
			nSched = new NotificationScheduler(0,nj.getMessageBody().getN_id(),new Gson().toJson(nj.getMessageBody()),nj.getUrl(),"Scheduled",new Timestamp(new Date().getTime()),new Timestamp(sdf.parse(nj.getScheduledTime()).getTime()));
			repo.addSchedule(nSched);		
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp = new ResponseEntity<String>(
					"{\"message\":\"Request Complete\",\"Status\":\"failure\"}",
					respHeader, HttpStatus.BAD_REQUEST);
		}		
		resp = new ResponseEntity<String>(
				"{\"message\":\"Request Complete\",\"Status\":\"success\"}",
				respHeader, HttpStatus.OK);

		return resp;		
	}
	
	
	
	public static Date getZoneBasedDateTime(String UtcDateTime,String timezone ) {
		//Evaluate scheduled Time\
		System.out.println("getZoneBasedDateTime "+UtcDateTime+"  "+timezone);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date schDate = null;
	    try {
			schDate = sdf.parse(UtcDateTime);
		} catch (ParseException e1) {
			//Auto-generated catch block
			e1.printStackTrace();
		} 
	    
	    //Date zoneDate = sdf.parse(new Date());
	    Date zoneDate = new Date();
	    
	    if(!timezone.contains("+")||!timezone.contains("-")) {
	    	if(timezone.equalsIgnoreCase("NA")||timezone.contains("Z")) {
	    		timezone="+05:30";
	    	}else {
	    		ZoneOffset zoneOffset = ZoneId.of(timezone).getRules().getOffset(Instant.now());
		    	timezone = zoneOffset.toString();
	    	}
	    	/*ZoneOffset zoneOffset = ZoneId.of(timezone).getRules().getOffset(Instant.now());
	    	timezone = zoneOffset.toString();*/
	    }
	    
	    //Split Timezone	    
	    char zoneOperator = timezone.charAt(0);
	    
	    int hour;
	    int minutes;
	    try {
	    
	      hour = Integer.parseInt(timezone.substring(1, 3));
	      minutes = Integer.parseInt(timezone.substring(4));
	    
	    }catch(Exception e) {	    	
	    	 hour = 5;
	 	     minutes = 30;	    	
	    }
	    
	    if(zoneOperator=='+') {
	    	Calendar cal = Calendar.getInstance(); 
		    cal.setTime(schDate); 
		    cal.add(Calendar.HOUR_OF_DAY, ((5-hour)<0?(5-hour):-(5-hour))); 
		    cal.add(Calendar.MINUTE, ((30-minutes)<0?(30-minutes):-(30-minutes)));
		    zoneDate = cal.getTime();  
	    }else if(zoneOperator=='-') {
	    	Calendar cal = Calendar.getInstance(); 
		    cal.setTime(schDate); 
		    cal.add(Calendar.HOUR_OF_DAY, (5+hour)); 
		    cal.add(Calendar.MINUTE, (30+minutes));
		    zoneDate = cal.getTime();
	    }
	    
	    //Date tempdate = new Date();	    
	    if(zoneDate.compareTo(new Date())>=0) {
	    	//Return time
	    	System.out.println(" >>> gotZoneBasedDateTime :"+zoneDate.toString());
	    	return zoneDate;
	    }else {
	    	System.out.println(" >>> gotZoneBasedDateTime :"+zoneDate.toString());	
	    }
	    
	    return null;
	}
}
