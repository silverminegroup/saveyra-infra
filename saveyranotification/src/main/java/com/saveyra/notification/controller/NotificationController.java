package com.saveyra.notification.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.google.gson.Gson;
import com.saveyara.aws.repo.NotificationsSent;
import com.saveyra.aws.AmazonSNSClientWrapper;
import com.saveyra.aws.SnsMessageGenerator.Platform;
import com.saveyra.notification.exception.ResourceNotFoundException;
import com.saveyra.notification.model.AuditTrail;
import com.saveyra.notification.model.RequestModelJson;
import com.saveyra.notification.model.SaveyraNotification;
//import com.saveyra.notification.repository.AuditTrailRepository;
import com.saveyra.notification.repository.MysqlRepo;
import com.saveyra.notification.repository.SaveyraNotificationRepository;
import com.saveyra.notification.utility.Declaration;

@RestController
@RequestMapping("/")
@EnableAutoConfiguration
@Qualifier("com.amazonaws.services.sns.AmazonSNS")
@CrossOrigin
@PropertySource(value="classpath:application.properties")
public class NotificationController {

	@Autowired
	MysqlRepo repo;
	
	@Autowired
	private Environment env;

	@Autowired
	SaveyraNotificationRepository saveyraNotificationRepository;

	/*
	 * @Autowired private AuditTrailRepository auditTrailRepository;
	 */

	private AmazonSNSClientWrapper snsClientWrapper;
	private static final Logger logger = (Logger) LoggerFactory.getLogger(NotificationController.class);

	public NotificationController(AmazonSNS snsClient) {
		this.snsClientWrapper = new AmazonSNSClientWrapper(snsClient);
	}

	public static final Map<Platform, Map<String, MessageAttributeValue>> attributesMap = new HashMap<Platform, Map<String, MessageAttributeValue>>();
	static {
		attributesMap.put(Platform.GCM, null);
		attributesMap.put(Platform.APNS, null);
		attributesMap.put(Platform.APNS_SANDBOX, null);
	}

	public NotificationsSent AppleSandboxAppNotification(@Valid @RequestBody NotificationsSent note) {
		String applicationName = "Emoji_IOS";
		String deviceToken = note.getNotification_id();
		String subcat = note.getNotificationcategory();
		logger.info("[INFO] Apple Notification : " + deviceToken + "Category : " + subcat);

		String messageID = snsClientWrapper.AppleNotification(Platform.APNS_SANDBOX, deviceToken, applicationName,
				attributesMap, note.getN_id(), note.getMessage(), note.getNotificationcategory(),
				note.getNotificationType(), note.getImage_id(), note.getNavigation_to(), note.getThumbnail(),
				note.getSticker_category(), note.getNotificationType(), note.getAction(), note.getTitle());
		note.setMessage_id(messageID);

		return note;
	}

	public NotificationsSent AppleAppNotification(@Valid @RequestBody NotificationsSent note) {
		String applicationName = "Saveyra_IOS";
		String notificationId = note.getNotification_id();
		logger.info("[INFO] Apple Notification : " + notificationId);

		String messageID = snsClientWrapper.AppleNotification(Platform.APNS, notificationId, applicationName,
				attributesMap, note.getN_id(), note.getMessage(), note.getNotificationcategory(),
				note.getNotificationType(), note.getImage_id(), note.getNavigation_to(), note.getThumbnail(),
				note.getSticker_category(), note.getNotificationType(), note.getAction(), note.getTitle());
		note.setMessage_id(messageID);
		return note;
	}

	public NotificationsSent AndroidAppNotification(@Valid @RequestBody NotificationsSent notification) {
		String applicationName = null;
		if (Declaration.getEnvironment().equalsIgnoreCase("prod")) {
			applicationName = "SaveyraApp_Android";
		} else {
			applicationName = "SaveyraApp_Staging_Android";
		}
		String registrationId = notification.getNotification_id();
		String messageID = snsClientWrapper.AndroidNotification(Platform.GCM, registrationId, applicationName,
				attributesMap, notification.getN_id(), notification.getMessage(),
				notification.getNotificationcategory(), notification.getNotificationType(), notification.getImage_id(),
				notification.getNavigation_to(), notification.getThumbnail(), notification.getSticker_category(),
				notification.getNotificationType(), notification.getAction(), notification.getTitle());
		notification.setMessage_id(messageID);
		return notification;
	}

	@CrossOrigin
	@PostMapping("/bulkPushNotification")
	public ResponseEntity<String> createBulkNote(@Valid @RequestBody String json) throws IOException {
		logger.info("\n[INFO] Enter bulkPushNotification , Request : \n" + json);
		ObjectMapper mapper = new ObjectMapper();
		@SuppressWarnings("rawtypes")
		Map notificationMap = mapper.readValue(json, Map.class);

		@SuppressWarnings("unchecked")
		List<String> notificationIdList = (List<String>) notificationMap.get("notificationId");
		
		if(notificationIdList==null || notificationIdList.size()==0 ||notificationMap.get("n_id") == null) {
			ResponseEntity<String> resp = new ResponseEntity<String>(
					"{\"message\":\"Request Complete\",\"status\":\"Failed to fetch DeviceTokens / n_id\"", HttpStatus.OK);
			return resp;
		}
		List<String> disableNotificationsList = new ArrayList<String>();
		long successCount = 0;
		long FailureCount = 0;
		/*AmazonSNSClient sns = new AmazonSNSClient(
				new BasicAWSCredentials("AKIAIIGSDYMQGEZV47ZA", "BR96gGn+HOVieXRydNwTxur7HsMSImaDvrMiK2cy"));*/
		
		AmazonSNSClient sns = new AmazonSNSClient(
				new BasicAWSCredentials(env.getProperty("saveyra.accessKey","AKIA2XNGOFU6FUOTDGF4"), env.getProperty("saveyra.secretKey","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")));

		String notificationType = notificationMap.get("notificationType") == null
				? notificationMap.get("notificationtype").toString()
				: notificationMap.get("notificationType").toString();
		String action = notificationMap.get("action") == null ? "" : notificationMap.get("action").toString();
		// Update Common Notification Params
		NotificationsSent notification = new NotificationsSent();
		notification.setN_id(notificationMap.get("n_id").toString());
		notification.setMessage(notificationMap.get("message").toString()== null ?"":notificationMap.get("message").toString());
		notification.setPlatform(notificationMap.get("platform").toString()== null ?"":notificationMap.get("platform").toString());
		notification.setImage_id(notificationMap.get("imageId").toString()== null ?"":notificationMap.get("imageId").toString());
		notification.setNotificationcategory(notificationMap.get("notificationcategory").toString()== null ?"":notificationMap.get("notificationcategory").toString());
		notification.setNotificationType(notificationType);
		notification.setNavigation_to(notificationMap.get("navTo").toString()==null?"":notificationMap.get("navTo").toString());
		notification.setSticker_category(notificationMap.get("stickerCategory").toString()==null?"":notificationMap.get("stickerCategory").toString());
		notification.setThumbnail(
				notificationMap.get("thumbnail") == null ? "" : notificationMap.get("thumbnail").toString());
		notification.setAction(action);
		notification.setTitle(notificationMap.get("title") == null ? "" : notificationMap.get("title").toString());

		// //System.out.println("SIZE : "+notificationIdList.size());

		for (int i = 0; i < notificationIdList.size(); i++) {

			notification.setNotification_id(notificationIdList.get(i));
			notification.setPlatform(notificationMap.get("platform").toString());
			// //System.out.println("\n\nSTART COUNT : "+i+ " Not ID
			// "+notificationIdList.get(i)+" len
			// "+notification.getNotification_id().trim().length()+"
			// "+notificationMap.get("platform").toString());
			String platform = notification.getPlatform();
			String iOS = "iOS";// =note1.getPlatform();
			String Android = "Android";// =note1.getPlatform();
			String All = "All";
			// ############################# IOS ########################################
			if (platform.equals(iOS)) {
				if (Declaration.getEnvironment().equalsIgnoreCase("prod")) {
					sns.setEndpoint("https://sns.ap-south-1.amazonaws.com");
				} else {
					sns.setEndpoint("https://sns.us-east-1.amazonaws.com");
				}
				if (notification.getNotification_id().trim().length() != 64) {
					FailureCount++;
					if (notification.getNotification_id().trim().length() < 100) {
						disableNotificationsList.add(notification.getNotification_id());
					}
					// //System.out.println("Length Not 64");
					continue;
				}

				try {
					logger.info("\n[INFO] iOS Notification Start");
					NotificationController Apple = new NotificationController(sns);
					if (Declaration.getEnvironment().equalsIgnoreCase("prod")) {
						Apple.AppleAppNotification(notification);
					} else {
						Apple.AppleSandboxAppNotification(notification);
					}
					successCount++;
					notification.setStatus("Success");
				} catch (AmazonServiceException ase) {
					logger.error("\n[ERROR] AmazonServiceException : " + ase.getMessage() + " [STATUS CODE] "
							+ ase.getStatusCode() + " [ERROR CODE] " + ase.getErrorCode() + " [ERROR TYPE] "
							+ ase.getErrorType() + " [Request ID] " + ase.getRequestId());
					String errorString = "[STATUS CODE] " + ase.getStatusCode() + " [ERROR CODE] " + ase.getErrorCode()
							+ " [ERROR TYPE] " + ase.getErrorType() + " [Request ID] " + ase.getRequestId();
					notification.setStatus(errorString.length() > 255 ? errorString.substring(0, 254) : errorString);
					FailureCount++;
					disableNotificationsList.add(notification.getNotification_id());
				} catch (AmazonClientException ace) {
					logger.error("\n[ERROR] AmazonClientException : " + ace.getMessage());
					String errorString = "[ERROR] " + ace.getMessage();
					notification.setStatus(errorString.length() > 255 ? errorString.substring(0, 254) : errorString);
					FailureCount++;
					disableNotificationsList.add(notification.getNotification_id());
				} finally {
					/*
					 * String msg = notification.getMessage(); ByteBuffer byteBuffer =
					 * StandardCharsets.UTF_8.encode(msg); notification.setMessage(new
					 * String(byteBuffer.array(), "ASCII"));
					 */
					repo.save(notification);

				}
				logger.info("\n[INFO] iOS Notification End");
			} else if (platform.equals(Android)) {
				sns.setEndpoint("https://sns.ap-south-1.amazonaws.com");

				if (notification.getNotification_id().trim().length() < 100) {
					if (notification.getNotification_id().trim().length() != 64) {
						disableNotificationsList.add(notification.getNotification_id());
					}
					FailureCount++;
					continue;
				}

				logger.info("[INFO] Android Notification Start");
				try {
					NotificationController Android1 = new NotificationController(sns);
					Android1.AndroidAppNotification(notification);
					notification.setStatus("Success");
					successCount++;
				} catch (AmazonServiceException ase) {
					logger.error("\n[ERROR] AmazonServiceException : " + ase.getMessage() + " [STATUS CODE] "
							+ ase.getStatusCode() + " [ERROR CODE] " + ase.getErrorCode() + " [ERROR TYPE] "
							+ ase.getErrorType() + " [Request ID] " + ase.getRequestId());
					String errorString = "[STATUS CODE] " + ase.getStatusCode() + " [ERROR CODE] " + ase.getErrorCode()
							+ " [ERROR TYPE] " + ase.getErrorType() + " [Request ID] " + ase.getRequestId();
					notification.setStatus(errorString.length() > 255 ? errorString.substring(0, 254) : errorString);
					FailureCount++;
					disableNotificationsList.add(notification.getNotification_id());
				} catch (AmazonClientException ace) {
					logger.error("\n[ERROR] AmazonClientException : " + ace.getMessage());
					String errorString = "[ERROR] " + ace.getMessage();
					notification.setStatus(errorString.length() > 255 ? errorString.substring(0, 254) : errorString);
					FailureCount++;
					disableNotificationsList.add(notification.getNotification_id());
				} finally {
					/*
					 * String msg = notification.getMessage(); ByteBuffer byteBuffer =
					 * StandardCharsets.UTF_8.encode(msg); notification.setMessage(new
					 * String(byteBuffer.array(), "ASCII"));
					 */
					repo.save(notification);
				}
				logger.info("\n[INFO] Android Notification End");
			} else if (platform.equals(All)) {
				// Apple notifications

				// //System.out.println("All >>
				// "+notification.getNotification_id().trim().length());
				if (notification.getNotification_id().trim().length() == 64) {
					logger.info("[INFO] iOS Notification Start");
					if (Declaration.getEnvironment().equalsIgnoreCase("prod")) {
						sns.setEndpoint("https://sns.ap-south-1.amazonaws.com");
					} else {
						sns.setEndpoint("https://sns.us-east-1.amazonaws.com");
					}

					try {
						NotificationController Apple = new NotificationController(sns);
						if (Declaration.getEnvironment().equalsIgnoreCase("prod")) {
							Apple.AppleAppNotification(notification);
						} else {
							Apple.AppleSandboxAppNotification(notification);
						}
						notification.setPlatform("iOS");
						successCount++;
						notification.setStatus("Success");
					} catch (AmazonServiceException ase) {
						logger.error("[ERROR] AmazonServiceException : " + ase.getMessage() + " [STATUS CODE] "
								+ ase.getStatusCode() + " [ERROR CODE] " + ase.getErrorCode() + " [ERROR TYPE] "
								+ ase.getErrorType() + " [Request ID] " + ase.getRequestId());
						String errorString = "[STATUS CODE] " + ase.getStatusCode() + " [ERROR CODE] "
								+ ase.getErrorCode() + " [ERROR TYPE] " + ase.getErrorType() + " [Request ID] "
								+ ase.getRequestId();
						notification
								.setStatus(errorString.length() > 254 ? errorString.substring(0, 254) : errorString);
						FailureCount++;
						disableNotificationsList.add(notification.getNotification_id());
					} catch (AmazonClientException ace) {
						logger.error("[ERROR] AmazonClientException : " + ace.getMessage());
						String errorString = "[ERROR] " + ace.getMessage();
						notification
								.setStatus(errorString.length() > 254 ? errorString.substring(0, 254) : errorString);
						FailureCount++;
						disableNotificationsList.add(notification.getNotification_id());
					} finally {
						/*
						 * String msg = notification.getMessage(); ByteBuffer byteBuffer =
						 * StandardCharsets.UTF_8.encode(msg); notification.setMessage(new
						 * String(byteBuffer.array(), "ASCII"));
						 */
						repo.save(notification);
					}
				} else if (notification.getNotification_id().trim().length() < 100) {

					// //System.out.println("LENGTH NOT 64 :
					// "+notification.getNotification_id().trim().length());
					disableNotificationsList.add(notification.getNotification_id());
					logger.info("[INFO] Invalid Token : " + notification.getNotification_id());
				} else {
					// //System.out.println("\n\nError in Token");
				}

				// Android notifications
				if (notification.getNotification_id().trim().length() > 100) {
					logger.info("[INFO] Android Notification Start");
					sns.setEndpoint("https://sns.ap-south-1.amazonaws.com");
					try {
						NotificationController androidNotification = new NotificationController(sns);
						androidNotification.AndroidAppNotification(notification);
						notification.setPlatform("Android");
						notification.setStatus("Success");
						successCount++;
					} catch (AmazonServiceException ase) {
						logger.error("[ERROR] AmazonServiceException : " + ase.getMessage() + " [STATUS CODE] "
								+ ase.getStatusCode() + " [ERROR CODE] " + ase.getErrorCode() + " [ERROR TYPE] "
								+ ase.getErrorType() + " [Request ID] " + ase.getRequestId());
						String errorString = "[STATUS CODE] " + ase.getStatusCode() + " [ERROR CODE] "
								+ ase.getErrorCode() + " [ERROR TYPE] " + ase.getErrorType() + " [Request ID] "
								+ ase.getRequestId();
						notification
								.setStatus(errorString.length() > 254 ? errorString.substring(0, 254) : errorString);
						FailureCount++;
						disableNotificationsList.add(notification.getNotification_id());
					} catch (AmazonClientException ace) {
						logger.error("[ERROR] AmazonClientException : " + ace.getMessage());
						String errorString = "[ERROR] " + ace.getMessage();
						notification
								.setStatus(errorString.length() > 254 ? errorString.substring(0, 254) : errorString);
						disableNotificationsList.add(notification.getNotification_id());
					} finally {
						// if (Declaration.Environment.equalsIgnoreCase("prod")) {
						/*
						 * String msg = notification.getMessage(); ByteBuffer byteBuffer =
						 * StandardCharsets.UTF_8.encode(msg);
						 * 
						 * for(Byte b:byteBuffer.array()) { //System.out.println(b); }
						 * 
						 * 
						 * notification.setMessage(new String(byteBuffer.array(), "UTF-8"));
						 */

						repo.save(notification);
						// }
					}
				} else if (notification.getNotification_id().trim().length() > 64) {
					disableNotificationsList.add(notification.getNotification_id());
					logger.info("[INFO] Invalid Token :" + notification.getNotification_id());
				}
			} else {
				// //System.out.println("Platform not found");
			}
		}

		logger.info("[INFO] Notification Sent Successfully  : " + successCount
				+ "\n       Notification Sent Failure  : " + FailureCount);
		StringBuffer sb = new StringBuffer();

		if (disableNotificationsList != null) {
			for (int i = 0; i < disableNotificationsList.size(); i++) {
				sb.append("'");
				sb.append(disableNotificationsList.get(i));
				if (i < (disableNotificationsList.size() - 1)) {
					sb.append("',");
				} else {
					sb.append("'");
				}
			}
		}
		// Update status of all errors
		repo.disableDeviceToken(sb.toString());

		HttpHeaders respHeader = new HttpHeaders();
		respHeader.set("Content-Type", "application/json");
		ResponseEntity<String> resp = new ResponseEntity<String>(
				"{\"message\":\"Request Complete\",\"Success Count\":\"" + successCount + "\",\"FailureCount\":\""
						+ FailureCount + "\"}",
				respHeader, HttpStatus.OK);

		return resp;
	}

	/*@CrossOrigin
	@PostMapping("/multiLingualBulkNotification")
	public ResponseEntity<String> createMultiLingualBulkNote(@Valid @RequestBody String json) throws IOException {
		logger.info("\n[INFO] Enter multiLingualBulkNotification , Request : \n" + json);
		
		HttpHeaders respHeader = new HttpHeaders();
		respHeader.set("Content-Type", "application/json");
		ResponseEntity<String> resp =null;
		
		
		AsyncClass async = new AsyncClass();
		
		async.createMultiLingualBulkNote(json);
		
		
		// Send response
		
		resp = new ResponseEntity<String>(
				"{\"message\":\"Request Complete\"}",
				respHeader, HttpStatus.OK);
		return resp;
	}

	private Set<String> getFilteredAppIdsDefault(Map<String, List<String>> deviceTokenMap, String gender, String platform,
			String frequency, String age_grp, List<String> languageArray) {
		logger.info("[ENTER] getFilteredAppIdsDefault");

		Set<String> filteredAppIdSet = new HashSet<String>();

		for (String deviceToken : deviceTokenMap.keySet()) {
			List<String> tokenRow = deviceTokenMap.get(deviceToken);
			// gender
			if (!gender.equalsIgnoreCase("All")) {
				if (!gender.equalsIgnoreCase(tokenRow.get(8))) {
					continue;
				}
			}

			// Platform
			if (!platform.equalsIgnoreCase("All")) {
				if (!tokenRow.get(1).equalsIgnoreCase(platform)) {
					continue;
				}
			}

			// Frequency
			if (!frequency.equalsIgnoreCase("All")) {
				if (tokenRow.get(6) != null && !tokenRow.get(6).equalsIgnoreCase("")
						&& !tokenRow.get(6).equalsIgnoreCase(frequency)) {
					continue;
				}
			}

			// Age
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			if ((tokenRow.get(4) == null || tokenRow.get(4).equals("2004-01-01"))
					&& (tokenRow.get(5) == null || tokenRow.get(5).equals("2004-01-01"))) {

				if (!(age_grp.equalsIgnoreCase("13-21") || age_grp.equalsIgnoreCase("All"))) {
					continue;
				}
			} else if (tokenRow.get(4) != null && !(tokenRow.get(4).equals(""))) {
				LocalDate localDate = null;

				String[] splitDate = tokenRow.get(4).split("-");
				if (splitDate[1].length() < 2) {
					splitDate[1] = 0 + splitDate[1];
				}
				if (splitDate[2].length() < 2) {
					splitDate[2] = 0 + splitDate[2];
				}
				String joinedDate = splitDate[0] + "-" + splitDate[1] + "-" + splitDate[2];
				localDate = LocalDate.parse(joinedDate, formatter);

				LocalDate now = LocalDate.now();
				Period period = Period.between(localDate, now);
				if (period.getYears() > 21 && period.getYears() < 31) {
					if (!(age_grp.equalsIgnoreCase("22-30") || age_grp.equalsIgnoreCase("All"))) {
						continue;
					}
				} else if (period.getYears() > 30 && period.getYears() < 100) {
					if (!(age_grp.equalsIgnoreCase("31-99") || age_grp.equalsIgnoreCase("All"))) {
						continue;
					}
				}
			} else if (tokenRow.get(5) != null && !(tokenRow.get(5).equals(""))) {
				LocalDate localDate = null;
				String[] splitDate = tokenRow.get(5).split("-");
				if (splitDate[1].length() < 2) {
					splitDate[1] = 0 + splitDate[1];
				}
				if (splitDate[2].length() < 2) {
					splitDate[2] = 0 + splitDate[2];
				}
				String joinedDate = splitDate[0] + "-" + splitDate[1] + "-" + splitDate[2];
				localDate = LocalDate.parse(joinedDate, formatter);

				LocalDate now = LocalDate.now();
				Period period = Period.between(localDate, now);
				if (period.getYears() > 21 && period.getYears() < 31) {
					if (!(age_grp.equalsIgnoreCase("22-30") || age_grp.equalsIgnoreCase("All"))) {
						continue;
					}
				} else if (period.getYears() > 30 && period.getYears() < 100) {
					if (!(age_grp.equalsIgnoreCase("31-99") || age_grp.equalsIgnoreCase("All"))) {
						continue;
					}
				}
			}
			if (languageArray.contains("Default")) {
				if (languageArray.contains("English") && (tokenRow.get(10) == null
						|| tokenRow.get(10).equalsIgnoreCase("English") || tokenRow.get(10).equalsIgnoreCase("null")
						|| tokenRow.get(10).equalsIgnoreCase(""))) {
					// Skip
					continue;
				} else if (languageArray.contains(tokenRow.get(10))) {
					// Skip
					continue;
				} else {
					// Add 
				}
			}
			filteredAppIdSet.add(tokenRow.get(0));
		}
		logger.info("[LEAVE] getFilteredAppIdsDefault : "+filteredAppIdSet.size());		
		return filteredAppIdSet;
	}*/

	/*@SuppressWarnings("unused")
	private Set<String> getFilteredAppIds(Map<String, List<String>> deviceTokenMap, String gender, String platform,
			String frequency, String age_grp, String language) {
		logger.info("[ENTER] getFilteredAppIds "+language );		
		Set<String> filteredAppIdSet = new HashSet<String>();

		for (String deviceToken : deviceTokenMap.keySet()) {
			List<String> tokenRow = deviceTokenMap.get(deviceToken);
			
			//System.out.println("LANGUAGE "+tokenRow.get(10));
			
			// gender
			if (!gender.equalsIgnoreCase("All")) {
				if (!gender.equalsIgnoreCase(tokenRow.get(8))) {
					continue;
				}
			}

			// Platform
			if (!platform.equalsIgnoreCase("All")) {
				if (!tokenRow.get(1).equalsIgnoreCase(platform)) {
					continue;
				}
			}

			// Frequency
			if (!frequency.equalsIgnoreCase("All")) {
				if (tokenRow.get(6) != null && !tokenRow.get(6).equalsIgnoreCase("")
						&& !tokenRow.get(6).equalsIgnoreCase(frequency)) {
					continue;
				}
			}

			// Age
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			if ((tokenRow.get(4) == null || tokenRow.get(4).equals("2004-01-01"))
					&& (tokenRow.get(5) == null || tokenRow.get(5).equals("2004-01-01"))) {

				if (!(age_grp.equalsIgnoreCase("13-21") || age_grp.equalsIgnoreCase("All"))) {
					continue;
				}
			} else if (tokenRow.get(4) != null && !(tokenRow.get(4).equals(""))) {
				LocalDate localDate = null;

				String[] splitDate = tokenRow.get(4).split("-");
				if (splitDate[1].length() < 2) {
					splitDate[1] = 0 + splitDate[1];
				}
				if (splitDate[2].length() < 2) {
					splitDate[2] = 0 + splitDate[2];
				}
				String joinedDate = splitDate[0] + "-" + splitDate[1] + "-" + splitDate[2];
				localDate = LocalDate.parse(joinedDate, formatter);

				LocalDate now = LocalDate.now();
				Period period = Period.between(localDate, now);
				if (period.getYears() > 21 && period.getYears() < 31) {
					if (!(age_grp.equalsIgnoreCase("22-30") || age_grp.equalsIgnoreCase("All"))) {
						continue;
					}
				} else if (period.getYears() > 30 && period.getYears() < 100) {
					if (!(age_grp.equalsIgnoreCase("31-99") || age_grp.equalsIgnoreCase("All"))) {
						continue;
					}
				}
			} else if (tokenRow.get(5) != null && !(tokenRow.get(5).equals(""))) {
				LocalDate localDate = null;
				String[] splitDate = tokenRow.get(5).split("-");
				if (splitDate[1].length() < 2) {
					splitDate[1] = 0 + splitDate[1];
				}
				if (splitDate[2].length() < 2) {
					splitDate[2] = 0 + splitDate[2];
				}
				String joinedDate = splitDate[0] + "-" + splitDate[1] + "-" + splitDate[2];
				localDate = LocalDate.parse(joinedDate, formatter);

				LocalDate now = LocalDate.now();
				Period period = Period.between(localDate, now);
				if (period.getYears() > 21 && period.getYears() < 31) {
					if (!(age_grp.equalsIgnoreCase("22-30") || age_grp.equalsIgnoreCase("All"))) {
						continue;
					}
				} else if (period.getYears() > 30 && period.getYears() < 100) {
					if (!(age_grp.equalsIgnoreCase("31-99") || age_grp.equalsIgnoreCase("All"))) {
						continue;
					}
				}
			}
			if (!language.equalsIgnoreCase("Default")) {
				if (language.equalsIgnoreCase("English") && (tokenRow.get(10) == null
						|| tokenRow.get(10).equalsIgnoreCase("English") || tokenRow.get(10).equalsIgnoreCase("null")
						|| tokenRow.get(10).equalsIgnoreCase(""))) {
					// Add English
				} else if (language.equalsIgnoreCase(tokenRow.get(10))) {
					// Add Local Language
				} else {
					// Skip
					continue;
				}
			}
			filteredAppIdSet.add(tokenRow.get(0));
		}
		logger.info("[LEAVE] getFilteredAppIds : "+filteredAppIdSet.size());
		return filteredAppIdSet;
	}
*/
	

	@SuppressWarnings("unused")
	private List<NotificationsSent> getNotificationObjects(List<String> notificationIDList, RequestModelJson rmj) {
		List<NotificationsSent> notList = new ArrayList<NotificationsSent>();
		for (String s : notificationIDList) {
			NotificationsSent nf = new NotificationsSent();
			nf.setN_id(rmj.getN_id());
			nf.setNotification_id(s);
			nf.setMessage(rmj.getMessage());
			nf.setPlatform(rmj.getPlatform());
			nf.setImage_id(rmj.getImageId());
			nf.setNotificationcategory(rmj.getNotificationcategory());
			nf.setNotificationType(rmj.getNotificationtype());
			nf.setNavigation_to(rmj.getNavTo());
			nf.setSticker_category(rmj.getStickerCategory());
			nf.setThumbnail(rmj.getThumbnail());
			notList.add(nf);
		}
		return notList;
	}

	/**
	 * Construct AuditTrail using notification details
	 * 
	 * @param notification
	 * @param status
	 * @param statusMessage
	 * @return
	 */
	@SuppressWarnings("unused")
	private AuditTrail constructAuditTrail(NotificationsSent notification, String status, String statusMessage) {
		AuditTrail auditTrail = new AuditTrail();
		auditTrail.setApplicationId(notification.getUserid());
		auditTrail.setDeviceToken(notification.getNotification_id());
		auditTrail.setNotificationType(notification.getNotificationType());
		auditTrail.setPlatform(notification.getPlatform());
		auditTrail.setStatus(status);
		auditTrail.setStatusMessage(statusMessage);
		auditTrail.setCreatedOn(new java.util.Date());
		return auditTrail;
	}

	@CrossOrigin
	@GetMapping("/Getnotification")
	public List<SaveyraNotification> getAllnotes() {

		List<SaveyraNotification> notificationDetails = saveyraNotificationRepository.findALL();
		for (int i = 0; i < notificationDetails.size(); i++) {
			HttpStatus status = HttpStatus.OK;
			notificationDetails.get(i).setStatus(status);
			if (notificationDetails.get(i).getFlag() == null) {
				continue;
			}

			if (notificationDetails.get(i).getFlag().equals("0")) {

				notificationDetails.get(i).setFlag("Stopped");

			} else if (notificationDetails.get(i).getFlag().equals("1")) {

				notificationDetails.get(i).setFlag("Scheduled");
			} else if (notificationDetails.get(i).getFlag().equals("2")) {

				notificationDetails.get(i).setFlag("Completed");
			} else {

				notificationDetails.get(i).setFlag("Invalid Process");
			}
		}
		return notificationDetails;

	}

	/* Save(Insert) notification to the database */
	@CrossOrigin
	@PostMapping("/notification")
	public SaveyraNotification createNote(@Valid @RequestBody SaveyraNotification note) {
		String msg = note.getMessage();
		ByteBuffer byteBuffer = StandardCharsets.UTF_8.encode(msg);
		try {
			note.setMessage(new String(byteBuffer.array(), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return saveyraNotificationRepository.save(note);
	}

	/* Get Notification By Id */

	@CrossOrigin
	@GetMapping("Getnotification/{id}")
	public SaveyraNotification getNoteById(@PathVariable(value = "id") Long noteId) {
		return saveyraNotificationRepository.findById(noteId)
				.orElseThrow(() -> new ResourceNotFoundException("SaveyraNotification", "id", noteId));
	}

	/* Update of Notification */

	@CrossOrigin
	@PutMapping("/updatenotification/{id}") // @Valid

	public SaveyraNotification updateNote(@PathVariable(value = "id") Long noteId,
			@Valid @RequestBody SaveyraNotification noteDetails) {// note

		// HttpStatus Status = noteDetails.getStatus().OK;// = HttpStatus.OK;

		SaveyraNotification notification = saveyraNotificationRepository.findById(noteId)
				.orElseThrow(() -> new ResourceNotFoundException("SaveyraNotification", "id", noteId));
		notification.setNotificationtype(noteDetails.getNotificationtype());
		notification.setNotificationtitle(noteDetails.getNotificationtitle());
		notification.setNotificationcategory(noteDetails.getNotificationcategory());
		notification.setMessage(noteDetails.getMessage());
		notification.setGender(noteDetails.getGender());
		notification.setFrequency(noteDetails.getFrequency());
		notification.setAgegroup(noteDetails.getAgegroup());
		notification.setPlatform(noteDetails.getPlatform());
		notification.setRegion(noteDetails.getRegion());
		notification.setSchedule(noteDetails.getSchedule());
		notification.setScheduledate(noteDetails.getScheduledate());
		notification.setScheduletime(noteDetails.getScheduletime());
		notification.setUpdatedAt(noteDetails.getUpdatedAt());
		notification.setType(noteDetails.getType());
		notification.setExpirydate(noteDetails.getExpirydate());
		notification.setNotificationdelivery(noteDetails.getNotificationdelivery());
		notification.setImageId(noteDetails.getImageId());
		notification.setThumbnail(noteDetails.getThumbnail());
		notification.setStickerCategory(noteDetails.getStickerCategory());

		SaveyraNotification updatedNote = saveyraNotificationRepository.save(notification);

		return updatedNote;
	}

	/* Stop Notification */
	@CrossOrigin
	@PutMapping("/stopnotification")

	public SaveyraNotification stopnotification(@Valid @RequestBody SaveyraNotification saveyraNotification) {

		Long noteId = saveyraNotification.getId();

		SaveyraNotification notification = saveyraNotificationRepository.findById(noteId)
				.orElseThrow(() -> new ResourceNotFoundException("SaveyraNotification", "id", noteId));
		notification.setId(notification.getId());
		notification.setFlag("0");
		SaveyraNotification updatedNote = saveyraNotificationRepository.save(notification);

		return updatedNote;

	}

	/* // Delete notification using delete method passing the id value */
	@CrossOrigin
	@DeleteMapping("/Deletenotification/{id}")
	public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long noteId) {

		SaveyraNotification note = saveyraNotificationRepository.findById(noteId)
				.orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));

		saveyraNotificationRepository.delete(note);
		// //System.out.println("Delete Opration Done");
		return ResponseEntity.ok().build();
	}

	@CrossOrigin
	@PostMapping("/status")
	public List<SaveyraNotification> getNoteByStoped(@Valid @RequestBody SaveyraNotification saveyraNotification) {

		String flag = saveyraNotification.getFlag();
		String flag3 = "0";
		String flag1 = "1";
		String flag2 = "2";

		if (flag.equals(flag3)) {
			List<SaveyraNotification> notificationDetails = saveyraNotificationRepository.findByStopped();

			for (int i = 0; i < notificationDetails.size(); i++) {

				notificationDetails.get(i).setFlag("Stopped");

			}

			return notificationDetails;
		}
		if (flag.equals(flag1)) {
			List<SaveyraNotification> notificationDetails1 = saveyraNotificationRepository.findByRunning();
			for (int i = 0; i < notificationDetails1.size(); i++) {

				notificationDetails1.get(i).setFlag("Scheduled");

			}

			return notificationDetails1;
		}
		if (flag.equals(flag2)) {
			List<SaveyraNotification> notificationDetails2 = saveyraNotificationRepository.findByCompleted();
			for (int i = 0; i < notificationDetails2.size(); i++) {

				notificationDetails2.get(i).setFlag("Completed");

			}

			return notificationDetails2;

		}
		return null;

	}

	@CrossOrigin
	@PostMapping("/datewise")
	public List<SaveyraNotification> getbyDate(@Valid @RequestBody SaveyraNotification fromdate) {

		// java.util.Date from = fromDate.getFromDate();
		// java.util.Date to = fromDate.getToDate();

		java.util.Date from = new java.util.Date();

		from = fromdate.getFromDate();

		//java.sql.Date sqlDate = new java.sql.Date(from.getTime());

		java.util.Date to = new java.util.Date();
		to = fromdate.getToDate();

		//java.sql.Date sqlDate1 = new java.sql.Date(to.getTime());

		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");

		String formatted = sdf.format(from);
		String toformated = sdf.format(to);

		List<SaveyraNotification> notificationDetails = saveyraNotificationRepository.findByDatesBetween(formatted,
				toformated);
		for (int i = 0; i < notificationDetails.size(); i++) {
			if (notificationDetails.get(i).getFlag().equals("0")) {

				notificationDetails.get(i).setFlag("Stopped");

			} else if (notificationDetails.get(i).getFlag().equals("1")) {

				notificationDetails.get(i).setFlag("Scheduled");
			} else if (notificationDetails.get(i).getFlag().equals("2")) {

				notificationDetails.get(i).setFlag("Completed");
			} else {
				HttpStatus Status = HttpStatus.BAD_REQUEST;
				notificationDetails.get(i).setFlag("Invalid ");
				notificationDetails.get(i).setStatus(Status);
			}
		}

		return notificationDetails;

	}

	@CrossOrigin
	@GetMapping("/notificationcenter")

	public List<SaveyraNotification> getbynotificationcenter() {

		List<SaveyraNotification> notificationDetails = saveyraNotificationRepository.findbynotification();
		for (int i = 0; i < notificationDetails.size(); i++) {
			HttpStatus status = HttpStatus.OK;
			notificationDetails.get(i).setStatus(status);

		}
		return notificationDetails;

	}

	@CrossOrigin
	@PostMapping("/notificationcenter1")
	public List<SaveyraNotification> getbynotification(@Valid @RequestBody String userid) {

		return saveyraNotificationRepository.findbynotification();
	}

	@CrossOrigin
	@PostMapping("/notificationreport")
	public List<SaveyraNotification> getbynotificationreport(@Valid @RequestBody SaveyraNotification fromdate) {// ,SaveyraNotification
		// saveyraNotification

		java.util.Date from = new java.util.Date();

		from = fromdate.getFromDate();
		// java.util.Date utilDate1 = new java.util.Date();
		//java.sql.Date sqlDate = new java.sql.Date(from.getTime());

		java.util.Date to = new java.util.Date();
		to = fromdate.getToDate();

		//java.sql.Date sqlDate1 = new java.sql.Date(to.getTime());

		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");

		String formatted = sdf.format(from);
		String toformated = sdf.format(to);

		String type = fromdate.getNotificationtype();
		String platfrom = fromdate.getPlatform();

		HttpStatus Status = fromdate.getStatus();

		if (type.equals("All") && platfrom.equals("All")) {
			List<SaveyraNotification> notificationDetails2 = saveyraNotificationRepository.findByDatesBetween(formatted,
					toformated);
			for (int i = 0; i < notificationDetails2.size(); i++) {
				notificationDetails2.get(i).setStatus(Status);

			}
			return notificationDetails2;
		} else if (type.equals("All") && platfrom.equals("iOS")) {

			List<SaveyraNotification> notificationDetails = saveyraNotificationRepository.findbytype(formatted,
					toformated, platfrom);
			for (int i = 0; i < notificationDetails.size(); i++) {
				notificationDetails.get(i).setStatus(Status);

			}

			return notificationDetails;
		} else if (type.equals("All") && platfrom.equals("Android")) {

			List<SaveyraNotification> notificationDetails = saveyraNotificationRepository.findbytype(formatted,
					toformated, platfrom);
			for (int i = 0; i < notificationDetails.size(); i++) {
				notificationDetails.get(i).setStatus(Status);

			}

			return notificationDetails;
		} else if (platfrom.equals("All")) {

			List<SaveyraNotification> notificationDetails = saveyraNotificationRepository.findbytype3(formatted,
					toformated, type, platfrom);
			for (int i = 0; i < notificationDetails.size(); i++) {
				notificationDetails.get(i).setStatus(Status);
			}
			return notificationDetails;
		} else {

			List<SaveyraNotification> notificationDetails1 = saveyraNotificationRepository.findbytype1(formatted,
					toformated, type, platfrom);
			for (int i = 0; i < notificationDetails1.size(); i++) {
				notificationDetails1.get(i).setStatus(Status);

			}
			return notificationDetails1;
		}		
	}

	@CrossOrigin
	@PostMapping("/getCreatedDateTime")
	public String getCreatedDateTime(@Valid @RequestBody String json) throws IOException {
		logger.info("\n[INFO] Enter getCreatedDateTime , Request : \n" + json);
		ObjectMapper mapper = new ObjectMapper();
		@SuppressWarnings("rawtypes")
		Map requestMap = mapper.readValue(json, Map.class);

		@SuppressWarnings("unchecked")
		List<String> instanceIdList = (List<String>) requestMap.get("instanceId");
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < instanceIdList.size(); i++) {
			sb.append("'" + instanceIdList.get(i) + "'");
			if (i < instanceIdList.size() - 1) {
				sb.append(",");
			}
		}
		// //System.out.println("FETCH LIST : "+sb.toString()+ " REQ "+(String)
		// requestMap.get("type"));
		Gson gson = new Gson();
		Map<String, String> resultMap = repo.findCreatedTime(sb.toString(), (String) requestMap.get("type"));
		return gson.toJson(resultMap);
	}
}
