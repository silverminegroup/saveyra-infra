package com.saveyra.notification.controller;

import java.io.IOException;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.saveyra.notification.model.CategoryModel;
import com.saveyra.notification.repository.CategoryRepo;

@RestController
@RequestMapping("/")
public class CategoryFeederController {
private static final Logger logger = (Logger) LoggerFactory.getLogger(ScheduleController.class);
	
	@Autowired
	CategoryRepo repo;
	
	@CrossOrigin
	@PostMapping("/addCategory")
	public ResponseEntity<String> insertCategory(@Valid @RequestBody String json) throws IOException {
		logger.info("\n[Enter] insertCategory , Request : \n" + json);
		CategoryModel cm;
		try {
			cm = new Gson().fromJson(json, CategoryModel.class);
		}catch(Exception e) {
			ResponseEntity<String> resp = new ResponseEntity<String>(
					"{\"message\":\"Error in Request\",\"status\":\"Failed to insert Category\"", HttpStatus.BAD_REQUEST);
			return resp;
		}		
		
		repo.addCategory(cm);

		ResponseEntity<String> resp = new ResponseEntity<String>(
				"{\"message\":\"Request Complete\",\"status\":\"Added Category "+cm.getCategory()+"\"", HttpStatus.OK);
		return resp;
		
	}
}
