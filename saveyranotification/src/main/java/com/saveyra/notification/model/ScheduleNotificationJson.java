package com.saveyra.notification.model;

public class ScheduleNotificationJson {
	private String categoryType;
	private ScheduleNotificationObject messageBody;
	private String scheduledTime;
	private String url;
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	public ScheduleNotificationObject getMessageBody() {
		return messageBody;
	}
	public void setMessageBody(ScheduleNotificationObject messageBody) {
		this.messageBody = messageBody;
	}
	public String getScheduledTime() {
		return scheduledTime;
	}
	public void setScheduledTime(String scheduledTime) {
		this.scheduledTime = scheduledTime;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
