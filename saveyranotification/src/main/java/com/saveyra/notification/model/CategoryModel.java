package com.saveyra.notification.model;

public class CategoryModel {
	
	private String category;
	private String subCategory;
	private String metaTags;
	private String layers;
	private String status;
	private String expression;
	
	public CategoryModel(String category, String subCategory, String metaTags, String layers, String status,
			String expression) {
		super();
		this.category = category;
		this.subCategory = subCategory;
		this.metaTags = metaTags;
		this.layers = layers;
		this.status = status;
		this.expression = expression;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getMetaTags() {
		return metaTags;
	}

	public void setMetaTags(String metaTags) {
		this.metaTags = metaTags;
	}

	public String getLayers() {
		return layers;
	}

	public void setLayers(String layers) {
		this.layers = layers;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}
}
