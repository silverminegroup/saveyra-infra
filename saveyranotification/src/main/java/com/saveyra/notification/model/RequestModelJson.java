package com.saveyra.notification.model;

import java.util.List;

public class RequestModelJson {
	private String n_id;
	private List<String> notificationId;
	private String message;
	private String platform;
	private String imageId;
	private String notificationcategory;
	private String notificationtype;
	private String navTo;
	private String stickerCategory;
	private String thumbnail;
	public List<String> getNotificationId() {
		return notificationId;
	}
	public void setNotificationId(List<String> notificationId) {
		this.notificationId = notificationId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getImageId() {
		return imageId;
	}
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	public String getNotificationcategory() {
		return notificationcategory;
	}
	public void setNotificationcategory(String notificationcategory) {
		this.notificationcategory = notificationcategory;
	}
	public String getNotificationtype() {
		return notificationtype;
	}
	public void setNotificationtype(String notificationtype) {
		this.notificationtype = notificationtype;
	}
	
	public String getStickerCategory() {
		return stickerCategory;
	}
	public void setStickerCategory(String stickerCategory) {
		this.stickerCategory = stickerCategory;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public RequestModelJson(List<String> notificationId, String message, String platform, String imageId,
			String notificationcategory, String notificationtype, String navTo, String stickerCategory,
			String thumbnail) {
		super();
		this.notificationId = notificationId;
		this.message = message;
		this.platform = platform;
		this.imageId = imageId;
		this.notificationcategory = notificationcategory;
		this.notificationtype = notificationtype;
		this.navTo = navTo;
		this.stickerCategory = stickerCategory;
		this.thumbnail = thumbnail;
	}
	public String getNavTo() {
		return navTo;
	}
	public void setNavTo(String navTo) {
		this.navTo = navTo;
	}
	public String getN_id() {
		return n_id;
	}
	public void setN_id(String n_id) {
		this.n_id = n_id;
	}

}
