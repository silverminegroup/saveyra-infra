package com.saveyra.notification.model;

import java.sql.Date;

public class DeviceTokenObject {
	private String applicationId;
	private String deviceToken;
	private String platform;
	private String f_key;
	private String m_key;
	private String language;
	private String gender;
	private String timeZone;
	private Date dob;
	private String frequency;
	
		
	public DeviceTokenObject(String applicationId, String deviceToken, String platform, String f_key, String m_key,
			String language, String gender, String timeZone, Date dob, String frequency) {
		super();
		this.applicationId = applicationId;
		this.deviceToken = deviceToken;
		this.platform = platform;
		this.f_key = f_key;
		this.m_key = m_key;
		this.language = language;
		this.gender = gender;
		this.timeZone = timeZone;
		this.dob = dob;
		this.frequency = frequency;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getDeviceToken() {
		return deviceToken;
	}
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getF_key() {
		return f_key;
	}
	public void setF_key(String f_key) {
		this.f_key = f_key;
	}
	public String getM_key() {
		return m_key;
	}
	public void setM_key(String m_key) {
		this.m_key = m_key;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	
		
}
