package com.saveyra.notification.model;

import java.util.List;
import java.util.Map;

public class RequestJsonModel {
	Map<String,String> basic;
	List<String> n_id;
	List<String> language;
	List<String> title;
	List<String> message;
		
	public List<String> getN_id() {
		return n_id;
	}
	public void setN_id(List<String> n_id) {
		this.n_id = n_id;
	}
	public Map<String, String> getBasic() {
		return basic;
	}
	public void setBasic(Map<String, String> basic) {
		this.basic = basic;
	}
	public List<String> getLanguage() {
		return language;
	}
	public void setLanguage(List<String> language) {
		this.language = language;
	}
	public List<String> getTitle() {
		return title;
	}
	public void setTitle(List<String> title) {
		this.title = title;
	}
	public List<String> getMessage() {
		return message;
	}
	public void setMessage(List<String> message) {
		this.message = message;
	}
	public RequestJsonModel(Map<String, String> basic, List<String> language, List<String> title,
			List<String> message) {
		super();
		this.basic = basic;
		this.language = language;
		this.title = title;
		this.message = message;
	}
}