package com.saveyra.notification.model;

public class DeviceTokenFormat {
	private String devicetoken;
	
	public DeviceTokenFormat(String devicetoken) {
		super();
		this.devicetoken = devicetoken;
	}

	public String getDevicetoken() {
		return devicetoken;
	}

	public void setDevicetoken(String devicetoken) {
		this.devicetoken = devicetoken;
	}
}
