package com.saveyra.notification.model;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "SMG_SAVEYRA_NOTIFICATION")
@EntityListeners(AuditingEntityListener.class)
/* @JsonIgnoreProperties(value = { "createdAt" }, allowGetters = true) */

public class SaveyraNotification {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String flag;

	private String notificationtitle;

	private String notificationtype;

	private String message;

	private String gender;

	private String platform;

	private String agegroup;

	private String region;

	private String frequency;
	// @NotBlank

	private String schedule;
	// @NotBlank

	private String imageId;	

	private String notificationcategory;	
	
	private String navigationTo;
	
	private String thumbnail;
	
	private String stickerCategory;

	private String language;
	
	private String type;

	
	@Transient
	private HttpStatus status;

	@Transient
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
	public Date fromDate;
	
	@Transient
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
	public Date toDate;

	/*private String notificationtype;
	private String platform;*/
	
	
	
	private String notificationdelivery;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
	/*
	 * // "Asia/Kolkata")
	 * 
	 * @Temporal(TemporalType.DATE)
	 */
	private Date scheduledate;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
	private Date expirydate;

	@JsonFormat(pattern = "HH:mm")
	@JsonDeserialize(using = SqlTimeDeserializer.class)
	@Column(name = "scheduletime")
	private Time scheduletime;

	@Column(nullable = false, updatable = false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", locale = "en_IN", timezone = "Asia/Kolkata")
	@Temporal(TemporalType.DATE)
	@CreatedDate
	private Date createdAt;

	@Column(updatable = true)
	@Temporal(TemporalType.DATE)
	@LastModifiedDate
	private Date updatedAt;

	private String createdby;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNotificationtitle() {
		return notificationtitle;
	}

	public void setNotificationtitle(String notificationtitle) {
		this.notificationtitle = notificationtitle;
	}



	public String getNotificationtype() {
		return notificationtype;
	}

	public void setNotificationtype(String notificationtype) {
		this.notificationtype = notificationtype;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getAgegroup() {
		return agegroup;
	}

	public void setAgegroup(String agegroup) {
		this.agegroup = agegroup;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public Date getScheduledate() {
		return scheduledate;
	}

	public void setScheduledate(Date scheduledate) {
		this.scheduledate = scheduledate;
	}

	public Time getScheduletime() {
		return scheduletime;
	}

	public void setScheduletime(Time scheduletime) {
		this.scheduletime = scheduletime;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public String getNotificationdelivery() {
		return notificationdelivery;
	}

	public void setNotificationdelivery(String notificationdelivery) {
		this.notificationdelivery = notificationdelivery;
	}

	public Date getExpirydate() {
		return expirydate;
	}

	public void setExpirydate(Date expirydate) {
		this.expirydate = expirydate;
	}

	
	public HttpStatus getStatus() {
		return status;
	}

	
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	
	public Date getFromDate() {
		return fromDate;
	}
	
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	
	public Date getToDate() {
		return toDate;
	}
	
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}



	
	public String getNotificationcategory() {
		return notificationcategory;
	}

	public void setNotificationcategory(String notificationcategory) {
		this.notificationcategory = notificationcategory;
	}


	public String getNavigationTo() {
		return navigationTo;
	}

	public void setNavigationTo(String navigationTo) {
		this.navigationTo = navigationTo;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getStickerCategory() {
		return stickerCategory;
	}

	public void setStickerCategory(String stickerCategory) {
		this.stickerCategory = stickerCategory;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}

