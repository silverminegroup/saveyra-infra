package com.saveyra.notification.utility;

public class Declaration {
	private  static String Environment = "dev"; // dev, qa , prod
	private  static final String APPLE_ARN_APNS = "arn:aws:sns:ap-south-1:737472818492:app/APNS/Saveyra_IOS";
	private  static final String APPLE_ARN_APNS_SANDBOX = "arn:aws:sns:us-east-1:737472818492:app/APNS_SANDBOX/Emoji_IOS";
	private  static final String ANDROID_ARN_GCM = "arn:aws:sns:ap-south-1:737472818492:app/GCM/SaveyraApp_Android";
	private  static final String ANDROID_ARN_GSM_STAGING = "arn:aws:sns:ap-south-1:737472818492:app/GCM/SaveyraApp_Staging_Android";
	private  static final String STRING_IOS = "iOS";
	private  static final String STRING_ANDROID = "Android";
	private  static final String STRING_ALL = "All";	
	private  static final String APPLE_APNS_ENDPOINT = "https://sns.ap-south-1.amazonaws.com";
	private  static final String APPLE_APNS_SANDBOX_ENDPOINT = "https://sns.ap-south-1.amazonaws.com";
	private  static final String ANDROID_GCM_ENDPOINT = "https://sns.ap-south-1.amazonaws.com";
	private  static final String ANDROID_GCM_STAGING_ENDPOINT ="https://sns.ap-south-1.amazonaws.com";
	private  static String WAR_PARENT_PATH = "";
	private  static final String ConfigPath = "/config";
	
	public static String getEnvironment() {
		return Environment;
	}
	public static void setEnvironment(String environment) {
		Environment = environment;
	}
	public static String getWAR_PARENT_PATH() {
		return WAR_PARENT_PATH;
	}
	public static void setWAR_PARENT_PATH(String wAR_PARENT_PATH) {
		WAR_PARENT_PATH = wAR_PARENT_PATH;
	}
	public static String getAppleArnApns() {
		return APPLE_ARN_APNS;
	}
	public static String getAppleArnApnsSandbox() {
		return APPLE_ARN_APNS_SANDBOX;
	}
	public static String getAndroidArnGcm() {
		return ANDROID_ARN_GCM;
	}
	public static String getAndroidArnGsmStaging() {
		return ANDROID_ARN_GSM_STAGING;
	}
	public static String getStringIos() {
		return STRING_IOS;
	}
	public static String getStringAndroid() {
		return STRING_ANDROID;
	}
	public static String getStringAll() {
		return STRING_ALL;
	}
	public static String getAppleApnsEndpoint() {
		return APPLE_APNS_ENDPOINT;
	}
	public static String getAppleApnsSandboxEndpoint() {
		return APPLE_APNS_SANDBOX_ENDPOINT;
	}
	public static String getAndroidGcmEndpoint() {
		return ANDROID_GCM_ENDPOINT;
	}
	public static String getAndroidGcmStagingEndpoint() {
		return ANDROID_GCM_STAGING_ENDPOINT;
	}
	public static String getConfigpath() {
		return ConfigPath;
	}		
}
