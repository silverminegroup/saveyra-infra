package com.saveyra.notification.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@EnableAsync
@EnableAutoConfiguration
@ComponentScan({ "com.saveyra.notification", "com.saveyara.aws" })
public class SaveyraNotificationConfig {

}
