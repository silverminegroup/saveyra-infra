package com.saveyara.aws.repo;

import java.util.List;

//import org.apache.tomcat.jni.SSLContext.SNICallBack;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonWebServiceRequest;
import com.amazonaws.ResponseMetadata;
import com.amazonaws.regions.Region;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.AddPermissionRequest;
import com.amazonaws.services.sns.model.AddPermissionResult;
import com.amazonaws.services.sns.model.CheckIfPhoneNumberIsOptedOutRequest;
import com.amazonaws.services.sns.model.CheckIfPhoneNumberIsOptedOutResult;
import com.amazonaws.services.sns.model.ConfirmSubscriptionRequest;
import com.amazonaws.services.sns.model.ConfirmSubscriptionResult;
import com.amazonaws.services.sns.model.CreatePlatformApplicationRequest;
import com.amazonaws.services.sns.model.CreatePlatformApplicationResult;
import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.CreateTopicResult;
import com.amazonaws.services.sns.model.DeleteEndpointRequest;
import com.amazonaws.services.sns.model.DeleteEndpointResult;
import com.amazonaws.services.sns.model.DeletePlatformApplicationRequest;
import com.amazonaws.services.sns.model.DeletePlatformApplicationResult;
import com.amazonaws.services.sns.model.DeleteTopicRequest;
import com.amazonaws.services.sns.model.DeleteTopicResult;
import com.amazonaws.services.sns.model.GetEndpointAttributesRequest;
import com.amazonaws.services.sns.model.GetEndpointAttributesResult;
import com.amazonaws.services.sns.model.GetPlatformApplicationAttributesRequest;
import com.amazonaws.services.sns.model.GetPlatformApplicationAttributesResult;
import com.amazonaws.services.sns.model.GetSMSAttributesRequest;
import com.amazonaws.services.sns.model.GetSMSAttributesResult;
import com.amazonaws.services.sns.model.GetSubscriptionAttributesRequest;
import com.amazonaws.services.sns.model.GetSubscriptionAttributesResult;
import com.amazonaws.services.sns.model.GetTopicAttributesRequest;
import com.amazonaws.services.sns.model.GetTopicAttributesResult;
import com.amazonaws.services.sns.model.ListEndpointsByPlatformApplicationRequest;
import com.amazonaws.services.sns.model.ListEndpointsByPlatformApplicationResult;
import com.amazonaws.services.sns.model.ListPhoneNumbersOptedOutRequest;
import com.amazonaws.services.sns.model.ListPhoneNumbersOptedOutResult;
import com.amazonaws.services.sns.model.ListPlatformApplicationsRequest;
import com.amazonaws.services.sns.model.ListPlatformApplicationsResult;
import com.amazonaws.services.sns.model.ListSubscriptionsByTopicRequest;
import com.amazonaws.services.sns.model.ListSubscriptionsByTopicResult;
import com.amazonaws.services.sns.model.ListSubscriptionsRequest;
import com.amazonaws.services.sns.model.ListSubscriptionsResult;
import com.amazonaws.services.sns.model.ListTagsForResourceRequest;
import com.amazonaws.services.sns.model.ListTagsForResourceResult;
import com.amazonaws.services.sns.model.ListTopicsRequest;
import com.amazonaws.services.sns.model.ListTopicsResult;
import com.amazonaws.services.sns.model.OptInPhoneNumberRequest;
import com.amazonaws.services.sns.model.OptInPhoneNumberResult;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.amazonaws.services.sns.model.RemovePermissionRequest;
import com.amazonaws.services.sns.model.RemovePermissionResult;
import com.amazonaws.services.sns.model.SetEndpointAttributesRequest;
import com.amazonaws.services.sns.model.SetEndpointAttributesResult;
import com.amazonaws.services.sns.model.SetPlatformApplicationAttributesRequest;
import com.amazonaws.services.sns.model.SetPlatformApplicationAttributesResult;
import com.amazonaws.services.sns.model.SetSMSAttributesRequest;
import com.amazonaws.services.sns.model.SetSMSAttributesResult;
import com.amazonaws.services.sns.model.SetSubscriptionAttributesRequest;
import com.amazonaws.services.sns.model.SetSubscriptionAttributesResult;
import com.amazonaws.services.sns.model.SetTopicAttributesRequest;
import com.amazonaws.services.sns.model.SetTopicAttributesResult;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.amazonaws.services.sns.model.SubscribeResult;
import com.amazonaws.services.sns.model.TagResourceRequest;
import com.amazonaws.services.sns.model.TagResourceResult;
import com.amazonaws.services.sns.model.UnsubscribeRequest;
import com.amazonaws.services.sns.model.UnsubscribeResult;
import com.amazonaws.services.sns.model.UntagResourceRequest;
import com.amazonaws.services.sns.model.UntagResourceResult;

@Service  
public class Repository  implements AmazonSNS
{
	@Override
	public void setEndpoint(String endpoint) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setRegion(Region region) {
		// TODO Auto-generated method stub
		
	}

	@Override  
	public AddPermissionResult addPermission(AddPermissionRequest addPermissionRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AddPermissionResult addPermission(String topicArn, String label, List<String> aWSAccountIds,
			List<String> actionNames) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CheckIfPhoneNumberIsOptedOutResult checkIfPhoneNumberIsOptedOut(
			CheckIfPhoneNumberIsOptedOutRequest checkIfPhoneNumberIsOptedOutRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ConfirmSubscriptionResult confirmSubscription(ConfirmSubscriptionRequest confirmSubscriptionRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ConfirmSubscriptionResult confirmSubscription(String topicArn, String token,
			String authenticateOnUnsubscribe) {
		// TODO Auto-generated method stub
		return null;
	}   

	@Override
	public ConfirmSubscriptionResult confirmSubscription(String topicArn, String token) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CreatePlatformApplicationResult createPlatformApplication(
			CreatePlatformApplicationRequest createPlatformApplicationRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CreatePlatformEndpointResult createPlatformEndpoint(
			CreatePlatformEndpointRequest createPlatformEndpointRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CreateTopicResult createTopic(CreateTopicRequest createTopicRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CreateTopicResult createTopic(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeleteEndpointResult deleteEndpoint(DeleteEndpointRequest deleteEndpointRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeletePlatformApplicationResult deletePlatformApplication(
			DeletePlatformApplicationRequest deletePlatformApplicationRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeleteTopicResult deleteTopic(DeleteTopicRequest deleteTopicRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeleteTopicResult deleteTopic(String topicArn) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GetEndpointAttributesResult getEndpointAttributes(
			GetEndpointAttributesRequest getEndpointAttributesRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GetPlatformApplicationAttributesResult getPlatformApplicationAttributes(
			GetPlatformApplicationAttributesRequest getPlatformApplicationAttributesRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GetSMSAttributesResult getSMSAttributes(GetSMSAttributesRequest getSMSAttributesRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GetSubscriptionAttributesResult getSubscriptionAttributes(
			GetSubscriptionAttributesRequest getSubscriptionAttributesRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GetSubscriptionAttributesResult getSubscriptionAttributes(String subscriptionArn) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GetTopicAttributesResult getTopicAttributes(GetTopicAttributesRequest getTopicAttributesRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GetTopicAttributesResult getTopicAttributes(String topicArn) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListEndpointsByPlatformApplicationResult listEndpointsByPlatformApplication(
			ListEndpointsByPlatformApplicationRequest listEndpointsByPlatformApplicationRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListPhoneNumbersOptedOutResult listPhoneNumbersOptedOut(
			ListPhoneNumbersOptedOutRequest listPhoneNumbersOptedOutRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListPlatformApplicationsResult listPlatformApplications(
			ListPlatformApplicationsRequest listPlatformApplicationsRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListPlatformApplicationsResult listPlatformApplications() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListSubscriptionsResult listSubscriptions(ListSubscriptionsRequest listSubscriptionsRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListSubscriptionsResult listSubscriptions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListSubscriptionsResult listSubscriptions(String nextToken) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListSubscriptionsByTopicResult listSubscriptionsByTopic(
			ListSubscriptionsByTopicRequest listSubscriptionsByTopicRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListSubscriptionsByTopicResult listSubscriptionsByTopic(String topicArn) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListSubscriptionsByTopicResult listSubscriptionsByTopic(String topicArn, String nextToken) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListTopicsResult listTopics(ListTopicsRequest listTopicsRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListTopicsResult listTopics() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListTopicsResult listTopics(String nextToken) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OptInPhoneNumberResult optInPhoneNumber(OptInPhoneNumberRequest optInPhoneNumberRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PublishResult publish(PublishRequest publishRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PublishResult publish(String topicArn, String message) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PublishResult publish(String topicArn, String message, String subject) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RemovePermissionResult removePermission(RemovePermissionRequest removePermissionRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RemovePermissionResult removePermission(String topicArn, String label) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SetEndpointAttributesResult setEndpointAttributes(
			SetEndpointAttributesRequest setEndpointAttributesRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SetPlatformApplicationAttributesResult setPlatformApplicationAttributes(
			SetPlatformApplicationAttributesRequest setPlatformApplicationAttributesRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SetSMSAttributesResult setSMSAttributes(SetSMSAttributesRequest setSMSAttributesRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SetSubscriptionAttributesResult setSubscriptionAttributes(
			SetSubscriptionAttributesRequest setSubscriptionAttributesRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SetSubscriptionAttributesResult setSubscriptionAttributes(String subscriptionArn, String attributeName,
			String attributeValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SetTopicAttributesResult setTopicAttributes(SetTopicAttributesRequest setTopicAttributesRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SetTopicAttributesResult setTopicAttributes(String topicArn, String attributeName, String attributeValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SubscribeResult subscribe(SubscribeRequest subscribeRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SubscribeResult subscribe(String topicArn, String protocol, String endpoint) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UnsubscribeResult unsubscribe(UnsubscribeRequest unsubscribeRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UnsubscribeResult unsubscribe(String subscriptionArn) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ResponseMetadata getCachedResponseMetadata(AmazonWebServiceRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListTagsForResourceResult listTagsForResource(ListTagsForResourceRequest listTagsForResourceRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TagResourceResult tagResource(TagResourceRequest tagResourceRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UntagResourceResult untagResource(UntagResourceRequest untagResourceRequest) {
		// TODO Auto-generated method stub
		return null;
	}
}
