package com.saveyara.aws.repo;

import java.util.Date;

public class NotificationsSent {	
	private Date created_at;
	private String message;
	private String notification_id;
	private String platform;
	private String title;
	private String image_id;
	private String navigation_to;
	private String sticker_category;
	private String thumbnail;
	private String notificationcategory;
	private String notificationType;
	private String type;
	private String userid;
	private String status;
	private String nav_to;
	private String n_id;
	private String message_id;
	private String application_id;
	private String action;
	
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getNotification_id() {
		return notification_id;
	}
	public void setNotification_id(String notification_id) {
		this.notification_id = notification_id;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getImage_id() {
		return image_id;
	}
	public void setImage_id(String image_id) {
		this.image_id = image_id;
	}
	public String getNavigation_to() {
		return navigation_to;
	}
	public void setNavigation_to(String navigation_to) {
		this.navigation_to = navigation_to;
	}
	public String getSticker_category() {
		return sticker_category;
	}
	public void setSticker_category(String sticker_category) {
		this.sticker_category = sticker_category;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getNotificationcategory() {
		return notificationcategory;
	}
	public void setNotificationcategory(String notificationcategory) {
		this.notificationcategory = notificationcategory;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNav_to() {
		return nav_to;
	}
	public void setNav_to(String nav_to) {
		this.nav_to = nav_to;
	}
	public String getN_id() {
		return n_id;
	}
	public void setN_id(String n_id) {
		this.n_id = n_id;
	}
	public String getMessage_id() {
		return message_id;
	}
	public void setMessage_id(String message_id) {
		this.message_id = message_id;
	}
	public String getApplication_id() {
		return application_id;
	}
	public void setApplication_id(String application_id) {
		this.application_id = application_id;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	
	
}
