package com.saveyara.aws.repo;

import java.util.List;

public class NotificationList {
	private List<NotificationsSent> notificationList;
	
	public NotificationList(List<NotificationsSent> notList) {
		super();
		this.notificationList = notList;
	}

	public List<NotificationsSent> getNotList() {
		return notificationList;
	}

	public void setNotList(List<NotificationsSent> notList) {
		this.notificationList = notList;
	}
	
}
