CREATE TABLE audit_trail (
    id INT AUTO_INCREMENT NOT NULL,
    application_id VARCHAR(255),
    device_token VARCHAR(255) NOT NULL,
    platform VARCHAR(50) NOT NULL,
    notification_type VARCHAR(255) NOT NULL,
    status VARCHAR(255) NOT NULL,
    status_message VARCHAR(1024),
    created_on DATETIME NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB;