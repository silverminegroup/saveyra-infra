package com.saveyra.AvatarFeeder.repo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class MysqlRepo {

	private static final Logger logger = LoggerFactory.getLogger(MysqlRepo.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	DataSource dataSource;

	@Transactional(readOnly = true)
	public String getcategoryId(String expression) {
		logger.info("[ENTER] expression "+expression);
		if (expression == null) {
			logger.error("[ERROR] Error in Json");
			return null;
		}
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String catID = "";
		try {
			conn = dataSource.getConnection();
			String query = "select categoryId from smg_saveyra_api_avatar_expression_category where expression = ?";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, expression);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				catID = rs.getString(1);
				logger.info("[INFO] catID= " + catID);
			} else {
				logger.error("[ERROR] SQL Select Fail - Cat ID");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] getcategoryId");
		return catID;
	}
	
	@Transactional(readOnly = true)
	public String getImageKeyFromMapper(String mapKey) {
		logger.info("[ENTER] getImageKeyFromMapper "+mapKey);
		if (mapKey == null) {
			logger.error("[ERROR] Error in Json");
			return null;
		}
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String imageKey = "";
		try {
			conn = dataSource.getConnection();
			String query = "SELECT imageKey FROM smg_saveyra_api_avatar_mapping where mapKey = ?";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, mapKey);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				imageKey = rs.getString(1);
				logger.info("[INFO] ImageKey = " + imageKey);
			} else {
				logger.error("[ERROR] SQL Select Fail - Map Key");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail - Map Key");
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] getImageKeyFromMapper");
		return imageKey;
	}
	
	@Transactional
	public void insertRecordtoMapper(String imageKey, String mapKey) {
		logger.info("[ENTER] insertRecordToMapper");
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dataSource.getConnection();
			String query = "INSERT INTO smg_saveyra_api_avatar_mapping (imageKey,mapKey) VALUES (?,?)";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, imageKey);
			pstmt.setString(2, mapKey);
			
			// execute insert SQL statement
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Insert Fail -Insert Map");
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] insertRecordToMapper");
	}
	
	@Transactional
	public void insertNeutralRecord(String tableName,String imageKey,String  gender, String imagePath, String tokens, String imageSizeFormat, String imageExtension, java.sql.Timestamp expiry, String status, Integer isPaid, java.sql.Timestamp createdOn) {
		logger.info("[ENTER] insertNeutralRecord");
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dataSource.getConnection();
			String query = "INSERT INTO "+tableName+" (imageKey,gender,imagePath,tokens,imageSizeFormat,imageExtension,expiry,status,isPaid,createdOn)VALUES(?,?,?,?,?,?,?,?,?,?)";
			System.out.println("QUERY : "+ query);
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, imageKey);
			pstmt.setString(2, gender);
			pstmt.setString(3, imagePath);
			pstmt.setString(4, tokens);
			pstmt.setString(5, imageSizeFormat);
			pstmt.setString(6,imageExtension);
			pstmt.setTimestamp(7, expiry);
			pstmt.setString(8, status);
			pstmt.setInt(9, isPaid);
			pstmt.setTimestamp(10, createdOn);			
			// execute insert SQL stetement
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Insert Fail");
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] insertNeutralRecord");
	}
	
	@Transactional
	public void insertExtensionRecord(String gender,String tableName,String specialColumn ,String imageKey,String fkcategory,String special,String imagePath,String tokens,String imageSizeFormat,String imageExtension,java.sql.Timestamp expiry,String status,java.sql.Timestamp createdOn) {
		logger.info("[ENTER] insertExtensionRecord");
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dataSource.getConnection();
			String query;
			if(specialColumn!=null && !specialColumn.equals("")) {
				query = "INSERT INTO smg_saveyra_api_avatar_"+tableName+" (imageKey, fkcategory, "+specialColumn+", imagePath, tokens, imageSizeFormat, imageExtension, expiry, status, createdOn)VALUES(?,?,?,?,?,?,?,?,?,?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, imageKey);
				pstmt.setString(2, fkcategory);
				pstmt.setString(3, special);
				pstmt.setString(4, imagePath);
				pstmt.setString(5, tokens);
				pstmt.setString(6, imageSizeFormat);
				pstmt.setString(7,imageExtension);
				pstmt.setTimestamp(8, expiry);
				pstmt.setString(9, status);			
				pstmt.setTimestamp(10, createdOn);	
			}else {
				query = "INSERT INTO smg_saveyra_api_avatar_"+tableName+" (imageKey, fkcategory,gender, imagePath, tokens, imageSizeFormat, imageExtension, expiry, status,isPaid, createdOn)VALUES(?,?,?,?,?,?,?,?,?,?,?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, imageKey);
				pstmt.setString(2, fkcategory);
				pstmt.setString(3, gender);
				pstmt.setString(4, imagePath);
				pstmt.setString(5, tokens);
				pstmt.setString(6, imageSizeFormat);
				pstmt.setString(7,imageExtension);
				pstmt.setTimestamp(8, expiry);
				pstmt.setString(9, status);		
				pstmt.setInt(10, 0);
				pstmt.setTimestamp(11, createdOn);
			}
			
			logger.info("[QUERY] "+query);			
			//execute insert SQL stetement
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Insert Fail");
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] insertExtensionRecord");
	}
	
	@Transactional
	public void updateRecord(String imageId, String tokens, String imagePath, String imageSizeFormat,
			String imageExtension, Date createdon, String status, Date expiry, String imageType, Integer paidStickers,
			Date lastModified, String caption, Date occassionActiveStartDate, Date occassionDate, String price,
			String category, String keywords, String subcategory) {
		logger.info("[ENTER] updateRecord");
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dataSource.getConnection();
			String query = "UPDATE smg_saveyra_stickers_metadata SET tokens = ?,imagePath = ?,imageSizeFormat = ?,imageExtension = ?,createdon = ?,status = ?,expiry = ?,imageType = ?,paidStickers = ?,lastModified = ?,caption = ?,occasionActiveStartDate = ?,occasionDate = ?,price = ?,category = ?,keywords = ?,subcategory = ? WHERE imageId = ?";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, tokens);
			pstmt.setString(2, imagePath);
			pstmt.setString(3, imageSizeFormat);
			pstmt.setString(4, imageExtension);
			if (createdon == null) {
				pstmt.setDate(5, null);
			} else {
				pstmt.setDate(5, new java.sql.Date(createdon.getTime()));
			}
			pstmt.setString(6, status);
			if (expiry == null) {
				pstmt.setDate(7, null);
			} else {
				pstmt.setDate(7, new java.sql.Date(expiry.getTime()));
			}
			pstmt.setString(8, imageType);
			pstmt.setInt(9, paidStickers);
			if (lastModified == null) {
				pstmt.setDate(10, null);
			} else {
				pstmt.setDate(10, new java.sql.Date(lastModified.getTime()));
			}
			pstmt.setString(11, caption);
			if (occassionActiveStartDate == null) {
				pstmt.setDate(12, null);
			} else {
				pstmt.setDate(12, new java.sql.Date(occassionActiveStartDate.getTime()));
			}

			if (occassionActiveStartDate == null) {
				pstmt.setDate(13, null);
			} else {
				pstmt.setDate(13, new java.sql.Date(occassionDate.getTime()));
			}
			pstmt.setString(14, price);
			pstmt.setString(15, category);
			pstmt.setString(16, keywords);
			pstmt.setString(17, subcategory);
			pstmt.setString(18, imageId);
			// execute insert SQL stetement
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] updateRecord");
	}
	
	public boolean checkTable(String tablename) {
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}		
		
		try {
			if(tableExist(conn,tablename)) {
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return false;	 
	}
	
	public static boolean tableExist(Connection conn, String tableName) throws SQLException {
	    boolean tExists = false;
	    try (ResultSet rs = conn.getMetaData().getTables(null, null, tableName, null)) {
	        while (rs.next()) { 
	            String tName = rs.getString("TABLE_NAME");
	            if (tName != null && tName.equals(tableName)) {
	                tExists = true;
	                break;
	            }
	        }
	    }
	    return tExists;
	}

	@Transactional
	public void deleteRecord(String imageId) {
		logger.info("[ENTER] updateRecord");
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dataSource.getConnection();
			String query = "DELETE FROM smg_saveyra_stickers_metadata WHERE imageId = ?";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, imageId);
			// execute insert SQL stetement
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] updateRecord");
	}
	
	
	
	
	
	
	
	@Transactional
	public void updateExtraObject(String fkCategory, String gender, String imageURL) {
		logger.info("[ENTER] updateExtraObject "+imageURL);
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dataSource.getConnection();
			String query = "UPDATE smg_saveyra_api_avatar_expression_background SET imagePath =? WHERE fkcategory=? and gender=?;";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, imageURL);
			pstmt.setString(2, fkCategory);
			pstmt.setString(3, gender);			
			// execute insert SQL statement
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] updateExtraObject");
	}
	
	@Transactional(readOnly = true)
	public String getImageBackgroundURL(String fkcategory,String gender) {
		logger.info("[ENTER] getImageBackgroundURL "+fkcategory+"  "+gender);
		if (fkcategory == null || gender ==null) {
			logger.error("[ERROR] Error in Json");
			return null;
		}
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String imagePath = "";
		try {
			conn = dataSource.getConnection();
			String query = "SELECT imagePath FROM smg_saveyra_api_avatar_expression_background where fkcategory = ? and gender = ?";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, fkcategory);
			pstmt.setString(2, gender);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				imagePath = rs.getString(1);
				logger.info("[INFO] ImagePath = " + imagePath);
			} else {
				logger.error("[ERROR] SQL Select Fail - Map Key");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail - Map Key");
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] getImageKeyFromMapper");
		return imagePath;
	}
}
