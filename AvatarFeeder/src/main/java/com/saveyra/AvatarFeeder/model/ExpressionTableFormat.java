package com.saveyra.AvatarFeeder.model;

public class ExpressionTableFormat {	
	  private String imageKey;
	  private String fkcategory; 
	  private String special;
	  private String imagePath;
	  private String tokens;
	  private String imageSizeFormat;
	  private String imageExtension;
	  private java.sql.Timestamp expiry;
	  private String status;
	  private java.sql.Timestamp createdOn;
	  private java.sql.Timestamp lastUpdated;
	  private String tableName;
	  private String mapKey;
	  
	public String getImageKey() {
		return imageKey;
	}
	public void setImageKey(String imageKey) {
		this.imageKey = imageKey;
	}
	
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getTokens() {
		return tokens;
	}
	public void setTokens(String tokens) {
		this.tokens = tokens;
	}
	public String getImageSizeFormat() {
		return imageSizeFormat;
	}
	public void setImageSizeFormat(String imageSizeFormat) {
		this.imageSizeFormat = imageSizeFormat;
	}
	public String getImageExtension() {
		return imageExtension;
	}
	public void setImageExtension(String imageExtension) {
		this.imageExtension = imageExtension;
	}
	public java.sql.Timestamp getExpiry() {
		return expiry;
	}
	public void setExpiry(java.sql.Timestamp expiry) {
		this.expiry = expiry;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public java.sql.Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(java.sql.Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public java.sql.Timestamp getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(java.sql.Timestamp lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public String getFkcategory() {
		return fkcategory;
	}
	public void setFkcategory(String fkcategory) {
		this.fkcategory = fkcategory;
	}
	public String getSpecial() {
		return special;
	}
	public void setSpecial(String special) {
		this.special = special;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getMapKey() {
		return mapKey;
	}
	public void setMapKey(String mapKey) {
		this.mapKey = mapKey;
	}	  
}
