package com.saveyra.AvatarFeeder.model;

import java.util.Date;

public class GenericTableFormat {
	  private String imageName;
	  private String imageFolder;
	  private String imageKey;
	  private String gender;
	  private String imagePath;
	  private String tokens;
	  private String imageSizeFormat;
	  private String imageExtension;
	  private java.sql.Timestamp expiry;
	  private String status;
	  private Integer isPaid;
	  private java.sql.Timestamp createdOn;
	  private java.sql.Timestamp lastUpdated;
	public String getImageKey() {
		return imageKey;
	}
	public void setImageKey(String imageKey) {
		this.imageKey = imageKey;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getTokens() {
		return tokens;
	}
	public void setTokens(String tokens) {
		this.tokens = tokens;
	}
	public String getImageSizeFormat() {
		return imageSizeFormat;
	}
	public void setImageSizeFormat(String imageSizeFormat) {
		this.imageSizeFormat = imageSizeFormat;
	}
	public String getImageExtension() {
		return imageExtension;
	}
	public void setImageExtension(String imageExtension) {
		this.imageExtension = imageExtension;
	}
	public java.sql.Timestamp getExpiry() {
		return expiry;
	}
	public void setExpiry(java.sql.Timestamp expiry) {
		this.expiry = expiry;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getIsPaid() {
		return isPaid;
	}
	public void setIsPaid(Integer isPaid) {
		this.isPaid = isPaid;
	}
	public java.sql.Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(java.sql.Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public java.sql.Timestamp getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(java.sql.Timestamp lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public String getImageFolder() {
		return imageFolder;
	}
	public void setImageFolder(String imageFolder) {
		this.imageFolder = imageFolder;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}	  
}
