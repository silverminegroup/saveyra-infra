package com.saveyra.AvatarFeeder;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

import com.saveyra.AvatarFeeder.repo.MysqlRepo;
import com.saveyra.AvatarFeeder.utility.Declaration;
import com.saveyra.AvatarFeeder.utility.FolderMapper;
import com.saveyra.AvatarFeeder.utility.S3Helper;

@SpringBootApplication
@ComponentScan("com.saveyra.AvatarFeeder")
@PropertySource(value="classpath:application.properties")
public class AvatarFeederApplication implements CommandLineRunner {

	@Autowired
	MysqlRepo repository;

	@Autowired
	FolderMapper folderMapper;
	
	private static final Logger logger = LoggerFactory.getLogger(AvatarFeederApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(AvatarFeederApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		logger.info("[ENTER] Avatar Feeder");
		if (args.length < 2) {
			logger.error("[ERROR] Argument Missing");
			return;
		}
		
		String type = args[0];
		String s3bucket = args[1];
				
		logger.info("[INFO]   Type : " + type + "  BUCKET  : "+s3bucket);

		if (type.toUpperCase().equals("NEUTRAL")) {
			
			if(s3bucket.equalsIgnoreCase("DEV")) {
				//Nothing
				Declaration.setImagePath(Declaration.imagePathNeutralDev);			
			}else if(s3bucket.equalsIgnoreCase("QA")) {
				Declaration.setImagePath(Declaration.imagePathNeutralQa);
			}else if(s3bucket.equalsIgnoreCase("PROD")){
				Declaration.setImagePath(Declaration.imagePathNeutralProd);
			}			
			
			// Update Valid Names
			Declaration.getValidFolderNames().add("background_hair");
			Declaration.getValidFolderNames().add("beard");
			Declaration.getValidFolderNames().add("body_shape");
			Declaration.getValidFolderNames().add("ear");
			Declaration.getValidFolderNames().add("earing");
			Declaration.getValidFolderNames().add("eyebrow");
			Declaration.getValidFolderNames().add("eye_outline");
			Declaration.getValidFolderNames().add("hair_style");
			Declaration.getValidFolderNames().add("iris");
			Declaration.getValidFolderNames().add("jawline");
			Declaration.getValidFolderNames().add("lips");
			Declaration.getValidFolderNames().add("nose");
			Declaration.getValidFolderNames().add("outfit");
			
			//If S3 path Exists	
			if (!S3Helper.getS3ClientInstance().doesObjectExist(Declaration.getBUCKET(), Declaration.getUPDATE_PATH())) {
				logger.error("[ERROR] Folder Path doest not exist : cms-content/Avatar_Update/");
			} else {
				//List Major Directories
				List<String> genderDirectories = S3Helper.getSubFolderOrFiles(Declaration.getBUCKET(), Declaration.getUPDATE_PATH()+"Neutral/");

				for (String genderFolder : genderDirectories) {								
					if (genderFolder.equals("Male") || genderFolder.equals("Female")) {							
						//Get Neutral Gender Sub-folders						
						List<String> neutralGenderSubDirectories = S3Helper.getSubFolderOrFiles(Declaration.getBUCKET(), Declaration.getUPDATE_PATH()+"Neutral/"+genderFolder+"/");
						for (String subDir : neutralGenderSubDirectories) {													
							if (!Declaration.getValidFolderNames().contains(subDir)) {
								// Check if table available in DB
								if (!repository.checkTable("smg_saveyra_api_avatar_" + subDir)) {
									logger.error("[ERROR] Table Not Found : smg_saveyra_api_avatar_" + subDir);
									return;
								}
							}
						}
					} else {
						logger.error("[ERROR] Folder Structure Error, Refer Guidelines");
					}
				}
			}

		} else if (type.toUpperCase().equals("EXPRESSION")) {
			
			Declaration.setImagePath(Declaration.getImagePathProd());	
			
			// Update Valid Names
			Declaration.getValidFolderNames().add("background_hair");
			Declaration.getValidFolderNames().add("background_layer");
			Declaration.getValidFolderNames().add("beard");
			Declaration.getValidFolderNames().add("body_shape");
			Declaration.getValidFolderNames().add("ear");
			Declaration.getValidFolderNames().add("earing");
			Declaration.getValidFolderNames().add("eyebrow");
			Declaration.getValidFolderNames().add("eye_outline");
			Declaration.getValidFolderNames().add("hair_style");
			Declaration.getValidFolderNames().add("iris");
			Declaration.getValidFolderNames().add("jawline");
			Declaration.getValidFolderNames().add("lips");
			Declaration.getValidFolderNames().add("nose");
			Declaration.getValidFolderNames().add("outfit");
			Declaration.getValidFolderNames().add("extra_objects");
			
			if (!S3Helper.getS3ClientInstance().doesObjectExist(Declaration.getBUCKET(), Declaration.getUPDATE_PATH())) {
				logger.error("[ERROR] Folder Path doest not exist :" +  Declaration.getUPDATE_PATH());
			} else {
				//Expression Directories
				List<String> expressionDirectories = S3Helper.getSubFolderOrFiles(Declaration.getBUCKET(), Declaration.getUPDATE_PATH()+"Expression/");
								
				for (String expressionFolder : expressionDirectories) {							
					if (expressionFolder.equals("Male") || expressionFolder.equals("Female")) {
						logger.error("[ERROR] Folder Structure Error, Refer Guidelines");
					} else {
						// Expression Gender Directories						
						List<String> expressionGenderDirectories = S3Helper.getSubFolderOrFiles(Declaration.getBUCKET(), Declaration.getUPDATE_PATH()+"Expression/"+expressionFolder);
												
						for (String genderFolder : expressionGenderDirectories) {
							if (genderFolder.equals("Male") || genderFolder.equals("Female")) {
								// Expression Gender Inner Directories								
								List<String> expressionGenderInnerDirectories = S3Helper.getSubFolderOrFiles(Declaration.getBUCKET(), Declaration.getUPDATE_PATH()+"Expression/"+expressionFolder+"/"+genderFolder+"/");

								for (String innerDir : expressionGenderInnerDirectories) {
									if (!Declaration.getValidFolderNames().contains(innerDir)) {
										// Check if table available in DB
										if (!repository.checkTable("smg_saveyra_api_avatar_" + innerDir)) {
											logger.error(
													"[ERROR] Table Not Found : smg_saveyra_api_avatar_" + innerDir);
											return;
										}
									}
								}
							} else {
								logger.error("[ERROR] Folder Structure Error, Refer Guidelines");
							}
						}
					}
				}
			}
		} else {
			logger.error("[ERROR] Unknown Type : " + type);
			return;
		}
		folderMapper.mapGenderFolder(args[0]);
		logger.info("[LEAVE] Avatar Feeder");
	}	
}
