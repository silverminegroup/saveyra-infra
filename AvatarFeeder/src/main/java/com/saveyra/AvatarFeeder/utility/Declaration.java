package com.saveyra.AvatarFeeder.utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Declaration {
	public static String imagePathNeutralDev = "https://s3.ap-south-1.amazonaws.com/saveyraassets/cms-content/prod/Avatar/";
	public static String imagePathNeutralQa = "https://s3.ap-south-1.amazonaws.com/saveyraassets/cms-content/prod/Avatar/";
	public static String imagePathNeutralProd = "https://s3.ap-south-1.amazonaws.com/saveyraassets/cms-content/prod/Avatar/";
	
	private static String imagePath = "/";
	private static String imagePathProd = "/";
	private static String imagePathQA = "/";
	
	private static final String status = "enabled";
	private static final Integer isPaid = 0;
	private static final Date expiry = getDate();
	private static final String token = "1234";
	private static String type ="";
	private static String subCategory = "";
	private static ArrayList<String> validFolderNames = new ArrayList<String>();
	private static String UPDATE_PATH = "cms-content/Avatar_Update/";
	private static String BUCKET = "saveyraassets";
	


	public static String getStatus() {
		return status;
	}


	public static Integer getIspaid() {
		return isPaid;
	}


	public static Date getExpiry() {
		return expiry;
	}


	public static Date getDate() {
		DateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");	
		Date expiry = null;
		try {
		expiry = sdformat.parse("2034-01-01");
		}catch(Exception e) {
			e.printStackTrace();
		}
		return expiry;
	}


	public static String getToken() {
		return token;
	}


	public static String getType() {
		return type;
	}


	public static void setType(String type) {
		Declaration.type = type;
	}


	public static String getSubCategory() {
		return subCategory;
	}


	public static void setSubCategory(String subCategory) {
		Declaration.subCategory = subCategory;
	}


	public static ArrayList<String> getValidFolderNames() {
		return validFolderNames;
	}


	public static void setValidFolderNames(ArrayList<String> validFolderNames) {
		Declaration.validFolderNames = validFolderNames;
	}


	public static String getImagePathProd() {
		return imagePathProd;
	}


	public static void setImagePathProd(String imagePathProd) {
		Declaration.imagePathProd = imagePathProd;
	}


	public static String getImagePathQA() {
		return imagePathQA;
	}


	public static void setImagePathQA(String imagePathQA) {
		Declaration.imagePathQA = imagePathQA;
	}


	public static String getImagePath() {
		return imagePath;
	}


	public static void setImagePath(String imagePath) {
		Declaration.imagePath = imagePath;
	}


	public static String getBUCKET() {
		return BUCKET;
	}


	public static void setBUCKET(String bUCKET) {
		BUCKET = bUCKET;
	}


	public static String getUPDATE_PATH() {
		return UPDATE_PATH;
	}


	public static void setUPDATE_PATH(String uPDATE_PATH) {
		UPDATE_PATH = uPDATE_PATH;
	}

}


