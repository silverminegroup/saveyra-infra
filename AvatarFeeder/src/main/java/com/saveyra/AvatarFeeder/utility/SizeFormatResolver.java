package com.saveyra.AvatarFeeder.utility;

import java.util.List;

public class SizeFormatResolver {
	
	public String resolveSizeString(List<String> fileList) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < fileList.size(); i++) {
			if (fileList.get(i).contains("x")) {
				if (sb.length() > 0) {
					sb.append(",");
				}
				String[] spFile = fileList.get(i).split("_");
				sb.append(spFile[spFile.length - 1]);
			}
		}
		return sb.toString();
	}
}
