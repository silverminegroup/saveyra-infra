package com.saveyra.AvatarFeeder.utility;

public class ImageExtensionResolver {
	
	public String getImageExtension(String filename) {
		if(filename.contains(".")) {
			String[] fileSplit = filename.split("\\.");
			return fileSplit[fileSplit.length-1];
		}
		return null;
	}	
}
