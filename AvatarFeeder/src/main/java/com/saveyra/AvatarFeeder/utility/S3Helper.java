package com.saveyra.AvatarFeeder.utility;

import java.util.ArrayList;
import java.util.List;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class S3Helper 
{ 
    // static variable single_instance of type Singleton 
    private static S3Helper s3_instance = null; 
  
    public static AWSCredentials credentials = null;

	public static AmazonS3 s3client = null;
	
    private S3Helper() 
    { 
    	credentials = new BasicAWSCredentials("AKIAI4U5GJKRNFGDNICA",
    				"MBTMRaEkibDqckoyllI8AqzvPMRRQmcroRLXDa5n");

    	s3client = AmazonS3ClientBuilder.standard()
    				.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.AP_SOUTH_1).build();
    } 
  
    // static method to create instance of Singleton class 
    public static S3Helper getInstance() 
    { 
        if (s3_instance == null) {
        	s3_instance = new S3Helper();
        }        	   
        return s3_instance; 
    } 
    
    public static AmazonS3 getS3ClientInstance() {

        if (s3_instance == null) {
        	s3_instance = new S3Helper();
        }        	   
        return s3client;
    }
    
    public static List<String> getSubFolderOrFiles(String bucketName, String prefix) {
    	System.out.println("bucketName "+bucketName+"    prefix  "+prefix );
		String delimiter = "/";
		if (!prefix.endsWith(delimiter)) {
			prefix += delimiter;
		}
		ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName).withPrefix(prefix)
				.withDelimiter(delimiter);
		ObjectListing objects = s3client.listObjects(listObjectsRequest);
		List<String> subFolders = new ArrayList<String>();	
		
		if(objects.getCommonPrefixes().isEmpty()) {
			for (S3ObjectSummary summary: objects.getObjectSummaries()) {
				System.out.println(">>>>> SPLITTING "+summary.getKey() + "   \n  >>>> WITH  "+prefix);
				
				String splitOne = summary.getKey().split(prefix)[1];
				if (splitOne.contains("/")) {
					subFolders.add(splitOne.split("/")[0]);
				}else {
					subFolders.add(splitOne);
				}
			}
		}else {
			for (String s : objects.getCommonPrefixes()) {
				String splitOne = s.split(prefix)[1];
				if (splitOne.contains("/")) {
					subFolders.add(splitOne.split("/")[0]);
				}else {
					subFolders.add(splitOne);
				}
			}
		}		
		return subFolders;
	}
    
} 



