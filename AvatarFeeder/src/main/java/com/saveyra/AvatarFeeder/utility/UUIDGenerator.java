package com.saveyra.AvatarFeeder.utility;

import java.util.UUID;

public class UUIDGenerator {
	public String getUUID() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
}
