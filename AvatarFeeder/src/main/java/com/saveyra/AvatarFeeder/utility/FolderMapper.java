package com.saveyra.AvatarFeeder.utility;

import java.io.File;
import java.io.FilenameFilter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.saveyra.AvatarFeeder.model.ExpressionTableFormat;
import com.saveyra.AvatarFeeder.model.GenericTableFormat;
import com.saveyra.AvatarFeeder.repo.MysqlRepo;

@Service
public class FolderMapper {

	@Autowired
	MysqlRepo repository;

	private static final Logger logger = LoggerFactory.getLogger(FolderMapper.class);

	public Map<String, List<String>> createDirerctoryMap(String sPath) {
		logger.info("[ENTER] createDirerctoryMap : " + sPath);
		Map<String, List<String>> directoryMap = new HashMap<String, List<String>>();
		//File directory = new File(sPath);
		// Get List of Folders
		List<String> subDirectories = S3Helper.getSubFolderOrFiles(Declaration.getBUCKET(), sPath);
		if (subDirectories == null) {
			logger.info("[LEAVE] createDirerctoryMap NULL SUB DIRECTORY");
			return null;
		}
		for (String dir : subDirectories) {
			String sKey = "";
			StringBuilder sb = new StringBuilder();
			sb.append(dir);
			sKey = sb.toString();
			// Create List of files in folder	
			logger.info("[DEBUG] DIR "+dir);
			List<String> innerDirectories = S3Helper.getSubFolderOrFiles(Declaration.getBUCKET(), sPath+dir+"/");	
			logger.info("[DEBUG] innerDirectories "+innerDirectories.size());
			directoryMap.put(sKey, innerDirectories);
		}
		logger.info("[LEAVE] createDirerctoryMap SIZE" + directoryMap.size());
		return directoryMap;
	}

	public Set<ExpressionTableFormat> processExpression(String special, String fkCategory, String gender, String folder,
			List<String> list) {
		logger.info("[Enter] processExpression (" + special + " , " + fkCategory + " , " + gender + " , " + folder
				+ " , " + list.size());
		Set<ExpressionTableFormat> expressionTableFormatList = new HashSet<ExpressionTableFormat>();

		for (int i = 0; i < list.size(); i++) {
			// Get Split file Name
			StringBuilder fileName = new StringBuilder();
			if (list.get(i).split("_").length > 2) {

				String[] fileNameArray = list.get(i).split("_");
				for (int j = 2; j < fileNameArray.length; j++) {
					fileName.append(fileNameArray[j]);
					if (j < fileNameArray.length - 1) {
						fileName.append("_");
					}
				}
			} else {
				fileName.append(list.get(i));
			}

			String imagePath = new ImagePathResolver(gender, folder, fileName.toString().split("\\.")[0])
					.getResolvedURL();

			String imageExtension = new ImageExtensionResolver().getImageExtension(fileName.toString());

			String sizeFormat = "";

			String imageKey = "";

			if (folder.equals("background_layer")) {
				imageKey = new UUIDGenerator().getUUID();
			} else {
				imageKey = repository.getImageKeyFromMapper(gender + "_" + fileName.toString().split("\\.")[0]);
			}
			ExpressionTableFormat expressionTableFormat = new ExpressionTableFormat();
			expressionTableFormat.setSpecial(special);
			expressionTableFormat.setImageKey(new UUIDGenerator().getUUID());
			expressionTableFormat.setFkcategory(fkCategory);
			expressionTableFormat.setImagePath(imagePath);
			expressionTableFormat.setImageExtension(imageExtension);
			expressionTableFormat.setImageSizeFormat(sizeFormat);
			expressionTableFormat.setCreatedOn(new Timestamp(Calendar.getInstance().getTime().getTime()));
			expressionTableFormat.setExpiry(new Timestamp(Declaration.getExpiry().getTime()));
			expressionTableFormat.setTokens(Declaration.getToken());
			expressionTableFormat.setTableName(new ResolveTable().getTableName(folder));
			expressionTableFormat.setMapKey(imageKey);
			expressionTableFormatList.add(expressionTableFormat);
		}
		logger.info("[Leave] processExpression  Size :" + expressionTableFormatList.size());
		return expressionTableFormatList;
	}

	public List<GenericTableFormat> processObject(String gender, String folder, List<String> files) {
		logger.info("[ENTER] processObject  (" + gender + " , " + folder + "  File_Size : " + files.size());
		List<GenericTableFormat> genericTableFormatList = new ArrayList<GenericTableFormat>();
		ArrayList<String> groupedFileList = null;
		String lastMajorFile = "";
		String majorFile = "";
		for (int i = 0; i < files.size(); i++) {
			if (!files.get(i).contains("x")) {
				lastMajorFile = majorFile;
				majorFile = files.get(i);
				if (groupedFileList != null && groupedFileList.size() > 0) {
					// Create File Object
					if (lastMajorFile.equals("")) {
						lastMajorFile = majorFile;
					}
					String imagePath = new ImagePathResolver(gender, folder, lastMajorFile.split("\\.")[0])
							.getResolvedURL();
					String imageExtension = new ImageExtensionResolver().getImageExtension(lastMajorFile);
					String sizeFormat = new SizeFormatResolver().resolveSizeString(groupedFileList);
					GenericTableFormat genericTableFormat = new GenericTableFormat();
					genericTableFormat.setGender(gender);
					genericTableFormat.setImageFolder(folder);
					genericTableFormat.setImageName(lastMajorFile.split("\\.")[0]);
					genericTableFormat.setImageKey(new UUIDGenerator().getUUID());
					genericTableFormat.setImagePath(imagePath);
					genericTableFormat.setStatus("enabled");
					genericTableFormat.setImageExtension(imageExtension);
					genericTableFormat.setImageSizeFormat(sizeFormat);
					genericTableFormat.setCreatedOn(new Timestamp(Calendar.getInstance().getTime().getTime()));
					genericTableFormat.setExpiry(new Timestamp(Declaration.getExpiry().getTime()));
					genericTableFormat.setIsPaid(Declaration.getIspaid());
					genericTableFormat.setTokens(Declaration.getToken());
					genericTableFormatList.add(genericTableFormat);
				}
				groupedFileList = new ArrayList<String>();
			} else {
				groupedFileList.add(files.get(i).split("\\.")[0]);
				if (i == files.size() - 1) {
					if (groupedFileList != null && groupedFileList.size() > 0) {
						// Create File Object
						lastMajorFile = majorFile;
						String imagePath = new ImagePathResolver(gender, folder, lastMajorFile.split("\\.")[0])
								.getResolvedURL();
						String imageExtension = new ImageExtensionResolver().getImageExtension(lastMajorFile);
						String sizeFormat = new SizeFormatResolver().resolveSizeString(groupedFileList);
						GenericTableFormat genericTableFormat = new GenericTableFormat();
						genericTableFormat.setGender(gender);
						genericTableFormat.setImageFolder(folder);
						genericTableFormat.setImageName(lastMajorFile.split("\\.")[0]);
						genericTableFormat.setImageKey(new UUIDGenerator().getUUID());
						genericTableFormat.setImagePath(imagePath);
						genericTableFormat.setStatus("enabled");
						genericTableFormat.setImageExtension(imageExtension);
						genericTableFormat.setImageSizeFormat(sizeFormat);
						genericTableFormat.setCreatedOn(new Timestamp(Calendar.getInstance().getTime().getTime()));
						genericTableFormat.setExpiry(new Timestamp(Declaration.getExpiry().getTime()));
						genericTableFormat.setIsPaid(Declaration.getIspaid());
						genericTableFormat.setTokens(Declaration.getToken());
						genericTableFormatList.add(genericTableFormat);
					}
				}
			}
		}
		logger.info("[LEAVE] processObject  Size :" + genericTableFormatList);
		return genericTableFormatList;
	}

	public void mapSubFolderAndFiles(String gender, String sPath) {
		logger.info("[ENTER] mapSubFolderAndFiles  (" + gender + " , " + sPath + " )");
		String sFolder = "";
		List<GenericTableFormat> gtfList = new ArrayList<GenericTableFormat>();
		// logger.info(" [TEMP]Received " + gender + " " + sPath);
		File directory = new File(sPath);
		String[] directories = directory.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) {
				return new File(current, name).isDirectory();
			}
		});
		if (directories == null) {
			return;
		}
		for (int i = 0; i < directories.length; i++) {
			String sformat = "";
			String sExtension = "";
			String imagePath = "";
			ImagePathResolver ipr = null;

			String[] folderSplit = directories[i].split("_");
			StringBuilder sb = new StringBuilder();
			for (int j = 0; j < folderSplit.length - 1; j++) {
				sb.append(folderSplit[j]);
				if (j != folderSplit.length - 2) {
					sb.append("_");
				}
			}
			sFolder = sb.toString();
			File folder = new File(sPath + "/" + directories[i]);
			String[] fList = folder.list(new FilenameFilter() {
				@Override
				public boolean accept(File current, String name) {
					return !(new File(current, name).isHidden());
				}
			});
			ArrayList<String> groupedFileList = null;
			String sMajorFile = "";
			String sLastMajorFile = "";
			int majorIndex = 0;
			String usedImagePath = "";
			for (int k = 0; k < fList.length; k++) {
				String splittedFile = fList[k].split("\\.")[0];
				if (!fList[k].contains("x")) {
					sLastMajorFile = sMajorFile;
					majorIndex = k;
					sMajorFile = fList[k].split("\\.")[0];
					String lastImagePath = imagePath;
					String lastExtension = sExtension;
					GenericTableFormat gtf = new GenericTableFormat();
					ipr = new ImagePathResolver(gender, sFolder, sMajorFile);
					imagePath = ipr.getResolvedURL();
					sExtension = new ImageExtensionResolver().getImageExtension(fList[k]);
					if (groupedFileList != null && groupedFileList.size() > 0) {

						if (sLastMajorFile.equals("") || sLastMajorFile.equals(usedImagePath)) {
							sLastMajorFile = sMajorFile;
						}
						if (lastExtension.equals("")) {
							lastExtension = sExtension;
						}
						if (lastImagePath.equals("")) {
							lastImagePath = imagePath;
						}
						usedImagePath = lastImagePath;
						SizeFormatResolver sfr = new SizeFormatResolver();
						sformat = sfr.resolveSizeString(groupedFileList);
						ipr = new ImagePathResolver(gender, sFolder, sMajorFile);
						imagePath = ipr.getResolvedURL();
						// Create Object
						gtf.setGender(gender);
						gtf.setImageExtension(lastExtension);
						gtf.setImageKey(new UUIDGenerator().getUUID());
						gtf.setImagePath(lastImagePath);
						gtf.setImageSizeFormat(sfr.resolveSizeString(groupedFileList));
						gtfList.add(gtf);
					}
					groupedFileList = new ArrayList<String>();
				} else {
					groupedFileList.add(splittedFile);
				}
				if (k == fList.length - 1) {
					if (groupedFileList != null && groupedFileList.size() > 0) {
						GenericTableFormat gtf = new GenericTableFormat();
						SizeFormatResolver sfr = new SizeFormatResolver();
						// Create Object
						gtf.setGender(gender);
						gtf.setImageExtension(new ImageExtensionResolver().getImageExtension(fList[majorIndex]));
						gtf.setImageKey(new UUIDGenerator().getUUID());
						gtf.setImagePath(imagePath);
						gtf.setImageSizeFormat(sfr.resolveSizeString(groupedFileList));
						gtfList.add(gtf);
					}
				}
			}
		}
		logger.info("[Leave] mapSubFolderAndFiles");
	}

	public void mapGenderFolder(String type) {
		logger.info("[Enter] mapGenderFolder ( "+ type +" )");
		if (type.toUpperCase().equals("NEUTRAL")) {
			Declaration.setType("Neutral");	
			List<GenericTableFormat> genericTableFormatList = new ArrayList<GenericTableFormat>();
			List<String> genderDirectories = S3Helper.getSubFolderOrFiles(Declaration.getBUCKET(), Declaration.getUPDATE_PATH()+"Neutral/");

			if (genderDirectories == null || genderDirectories.size() == 0) {
				return;
			}

			for (String s : genderDirectories) {
				Map<String, List<String>> directoryMap = createDirerctoryMap(Declaration.getUPDATE_PATH()+"Neutral/" + s+"/");
				
				for (String key : directoryMap.keySet()) {
					logger.info("KEY "+key+"  MAP SIZE :  "+directoryMap.get(key).size());
					genericTableFormatList.addAll(processObject(s, key, directoryMap.get(key)));
				}
			}

			for (GenericTableFormat genericTableFormat : genericTableFormatList) {
				repository.insertNeutralRecord("smg_saveyra_api_avatar_" + genericTableFormat.getImageFolder(),
						genericTableFormat.getImageKey(), genericTableFormat.getGender(),
						genericTableFormat.getImagePath(), genericTableFormat.getTokens(),
						genericTableFormat.getImageSizeFormat(), genericTableFormat.getImageExtension(),
						genericTableFormat.getExpiry(), genericTableFormat.getStatus(), genericTableFormat.getIsPaid(),
						genericTableFormat.getCreatedOn());
				try {
					repository.insertRecordtoMapper(genericTableFormat.getImageKey(),
							genericTableFormat.getGender() + "_" + genericTableFormat.getImageName());
				} catch (Exception e) {
					logger.error("[ERROR] Exception in insertRecordtoMapper : " + e.getMessage());
				}
			}
			logger.info("[INFO] JSON :" + new Gson().toJson(genericTableFormatList));

		} else if (type.toUpperCase().equals("EXPRESSION")) {
			Declaration.setType("Expression");
			
			
			/*File directory = new File(sPath);

			String[] subCatDirs = directory.list(new FilenameFilter() {
				@Override
				public boolean accept(File current, String name) {
					return new File(current, name).isDirectory();
				}
			});*/
			
			//Expression Directories
			List<String> expressionDirectories = S3Helper.getSubFolderOrFiles(Declaration.getBUCKET(), Declaration.getUPDATE_PATH()+"Expression/");
			
			if (expressionDirectories == null || expressionDirectories.size() == 0) {
				return;
			}
			// SUB_CAT
			for (String expDir : expressionDirectories) {

				Declaration.setSubCategory(expDir);
				// logger.info(" [TEMP]Current SUB CAT :" + subCatDir);

				/*File catFolder = new File(Declaration.getUPDATE_PATH()+"Expression/"+expDir+"/");

				String[] genderDirectories = catFolder.list(new FilenameFilter() {
					@Override
					public boolean accept(File current, String name) {
						return new File(current, name).isDirectory();
					}
				});*/
				
				// Expression Gender Directories						
				List<String> expressionGenderDirectories = S3Helper.getSubFolderOrFiles(Declaration.getBUCKET(), Declaration.getUPDATE_PATH()+"Expression/"+expDir);

				if (expressionGenderDirectories == null || expressionGenderDirectories.size() == 0) {
					continue;
				}
				// GENDER
				for (String genderDir : expressionGenderDirectories) {
					// Temp handlers for extra object
					boolean processExtraObject = false;
					List<String> fileList_extra = null;
					String key_extra = null;
					String fkCategory_extra = null;
					String gender_extra = null;

					Set<ExpressionTableFormat> expressionTableFormatList = new HashSet<ExpressionTableFormat>();

					/*//logger.info(" [TEMP]\t\t [DEBUG] Current GENDER :" + gender);
					File genderFolder = new File(sPath + "/" + subCatDir + "/" + gender);

					String[] innerDirectories = genderFolder.list(new FilenameFilter() {
						@Override
						public boolean accept(File current, String name) {
							return new File(current, name).isDirectory();
						}
					});*/
					
					// Expression Gender Inner Drectories								
					List<String> expressionGenderInnerDirectories = S3Helper.getSubFolderOrFiles(Declaration.getBUCKET(), Declaration.getUPDATE_PATH()+"Expression/"+expDir+"/"+genderDir+"/");


					if (expressionGenderInnerDirectories == null || expressionGenderInnerDirectories.size() == 0) {
						continue;
					}

					Map<String, List<String>> directoryMap = createDirerctoryMap(Declaration.getUPDATE_PATH()+"Expression/"+expDir+"/"+genderDir+"/");

					/*for (String s : directoryMap.keySet()) {
						//logger.info(" [TEMP]\t\t\t\t[DEBUG]  " + s + "  " + directoryMap.get(s).toString());
					}*/

					// IMAGE FOLDER
					for (String key : directoryMap.keySet()) {
						if (Declaration.getSubCategory() == null) {
							continue;
						}
						//logger.info(" [TEMP]\t\t\t[DEBUG] Searching for sub cat :" + Declaration.getSubCategory());
						String fkCategory = repository.getcategoryId(Declaration.getSubCategory());
						if (fkCategory == null || fkCategory.equals("")) {
							continue;
						}
						// logger.info(" [TEMP]OLD Length : " + expressionTableFormatList.size());
						//logger.info(" [TEMP]\n\n<<<<<<<<<<FOLDER >>>>>>>>>>>>  " + key + "  \n\n");
						if (key.equalsIgnoreCase("extra_objects") || key.equalsIgnoreCase("extra_object")) {

							processExtraObject = true;
							fileList_extra = directoryMap.get(key);
							key_extra = key;
							fkCategory_extra = fkCategory;
							gender_extra = genderDir;

						} else {
							expressionTableFormatList.addAll(processExpression(new ResolveSpecial().getSpecial(key),
									fkCategory, genderDir, key, directoryMap.get(key)));
						}
						// logger.info(" [TEMP]New Length : " + expressionTableFormatList.size());
					}

					//logger.info("JSON : " + new Gson().toJson(expressionTableFormatList) + "\nTotal Inserted : "
						//	+ expressionTableFormatList.size());

					for (ExpressionTableFormat expressionTableFormat : expressionTableFormatList) {
						repository.insertExtensionRecord(genderDir, expressionTableFormat.getTableName(),
								expressionTableFormat.getSpecial(), expressionTableFormat.getImageKey(),
								expressionTableFormat.getFkcategory(), expressionTableFormat.getMapKey(),
								expressionTableFormat.getImagePath(), expressionTableFormat.getTokens(),
								expressionTableFormat.getImageSizeFormat(), expressionTableFormat.getImageExtension(),
								expressionTableFormat.getExpiry(), expressionTableFormat.getStatus(),
								expressionTableFormat.getCreatedOn());
					}

					if (processExtraObject) {
						handleExtraObject(fileList_extra, key_extra, fkCategory_extra, gender_extra);
						processExtraObject = false;
					}
					
					logger.info("[INFO] JSON :" + new Gson().toJson(expressionTableFormatList) + "\nTotal Inserted : "
							+ expressionTableFormatList.size());
				}
			}
		}
	}

	public void handleExtraObject(List<String> files, String key, String fkCategory, String gender) {
		String existingImageUrl = repository.getImageBackgroundURL(fkCategory, gender);
		StringBuffer sBuffer = new StringBuffer();
		for (int i = 0; i < files.size(); i++) {
			// Get Split file Name
			StringBuilder fileName = new StringBuilder();
			if (files.get(i).split("_").length > 2) {

				String[] fileNameArray = files.get(i).split("_");
				for (int j = 2; j < fileNameArray.length; j++) {
					fileName.append(fileNameArray[j]);
					if (j < fileNameArray.length - 1) {
						fileName.append("_");
					}
				}
			} else {
				fileName.append(files.get(i));
			}
			sBuffer.append(new ImagePathResolver(gender, key, fileName.toString().split("\\.")[0]).getResolvedURL());
			if (i < (files.size() - 2)) {
				sBuffer.append(",");
			} else {
				sBuffer.append("," + existingImageUrl);
			}

		}
		logger.info(" [TEMP]Resolved Image Path : " + sBuffer.toString());
		// Update In DB
		repository.updateExtraObject(fkCategory, gender, sBuffer.toString());
	}
}
