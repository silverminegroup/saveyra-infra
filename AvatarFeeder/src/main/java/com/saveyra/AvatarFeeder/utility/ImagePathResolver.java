package com.saveyra.AvatarFeeder.utility;

public class ImagePathResolver {	
	private String gender;
	private String folder;
	private String file;
	public ImagePathResolver(String gender,String folder, String file) {
		//System.out.println("gender "+gender+" folder "+ folder+" file "+file);
		this.gender=gender;
		this.folder=folder;
		this.file=file;
	}
	
	public String getResolvedURL() {
		if(Declaration.getType().equalsIgnoreCase("neutral")) {
			return Declaration.getImagePath()+Declaration.getType()+"/"+this.gender+"/"+this.folder+"/"+this.file;
		}else {
			return Declaration.getImagePath()+Declaration.getType()+"/"+Declaration.getSubCategory()+"/"+this.gender+"/"+this.folder+"/"+this.gender.toLowerCase()+"_"+Declaration.getSubCategory().toLowerCase()+"_"+this.file;
		}		
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getFolder() {
		return folder;
	}
	public void setFolder(String folder) {
		this.folder = folder;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
}
