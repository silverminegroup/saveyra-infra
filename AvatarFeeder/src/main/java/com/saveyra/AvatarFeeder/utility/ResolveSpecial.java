package com.saveyra.AvatarFeeder.utility;

public class ResolveSpecial {

	public String getSpecial(String sFolder) {
		String retString = "";
		switch (sFolder) {
		case "background_hair":
			retString = "backgroundHair";
			break;
		case "body_shape":
			retString = "bodyShape";
			break;
		case "background_layer":
			retString = "";
			break;
		case "eye_outline":
			retString = "eyeoutline";
			break;
		case "hair_style":
			retString = "hairStyle";	
			break;
		default:
			retString = sFolder;
		}
		return retString;
	}
}
