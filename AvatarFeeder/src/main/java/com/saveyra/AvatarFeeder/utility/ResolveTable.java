package com.saveyra.AvatarFeeder.utility;

public class ResolveTable {
	
	public String getTableName(String sFolder) {
		String actualTable = "";
		switch(sFolder) {
		case "background_layer":
			actualTable = "expression_background";
			break;
		default:
			actualTable = sFolder+"_expression";
		}
		return actualTable;		
	}
}
