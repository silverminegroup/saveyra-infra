package com.saveyra.analytics;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

import com.saveyra.analytics.DAO.UsageDataDAO;


@SpringBootApplication
@ComponentScan("com.saveyra.analytics")
@PropertySource(value="classpath:application.properties")
public class AnalyticsApplication extends SpringBootServletInitializer implements CommandLineRunner {

	@Autowired
	UsageDataDAO usageDataDAO;

	public static void main(String[] args) {
		SpringApplication.run(AnalyticsApplication.class, args);
	}

	@PostConstruct
	public void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC")); 
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(AnalyticsApplication.class);
	}

	@Override
	public void run(String... args) throws Exception {
		//Load Exclusions into List
		usageDataDAO.readExclusionAtStartup();
	}
	
/*	public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
	    PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	    properties.setLocation(new FileSystemResource("analytics.application.properties"));
	    properties.setIgnoreResourceNotFound(false);
	    return properties;
	}*/
}
