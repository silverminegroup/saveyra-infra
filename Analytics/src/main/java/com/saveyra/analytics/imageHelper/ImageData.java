package com.saveyra.analytics.imageHelper;

public class ImageData {
	private String ImageID;
	private Integer Shared;
	private Integer Viewed;
	public String getImageID() {
		return ImageID;
	}
	public void setImageID(String imageID) {
		ImageID = imageID;
	}
	public Integer getShared() {
		return Shared;
	}
	public void setShared(Integer shared) {
		Shared = shared;
	}
	public Integer getViewed() {
		return Viewed;
	}
	public void setViewed(Integer viewed) {
		Viewed = viewed;
	}
	public ImageData(String imageID, Integer shared, Integer viewed) {
		super();
		ImageID = imageID;
		Shared = shared;
		Viewed = viewed;
	}
	
}
