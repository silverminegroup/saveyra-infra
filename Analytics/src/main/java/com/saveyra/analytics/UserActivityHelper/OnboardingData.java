package com.saveyra.analytics.UserActivityHelper;

public class OnboardingData {
	private String Date;
	private String Platform;
	private String Completed;
	private String Skipped;
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public String getCompleted() {
		return Completed;
	}
	public void setCompleted(String completed) {
		Completed = completed;
	}
	public String getSkipped() {
		return Skipped;
	}
	public void setSkipped(String skipped) {
		Skipped = skipped;
	}
	public OnboardingData(String date, String platform, String completed, String skipped) {
		super();
		Date = date;
		Platform = platform;
		Completed = completed;
		Skipped = skipped;
	}
}
