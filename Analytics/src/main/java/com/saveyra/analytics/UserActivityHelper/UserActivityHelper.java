package com.saveyra.analytics.UserActivityHelper;

import java.util.ArrayList;

public class UserActivityHelper {

	private String ApplikcationID;
	private String keyboardSearched_1;
	private String keyboardSessionStart_2;
	private String keyboardTextpad_3;
	private String keyboardSearchresultsStickerselected_4;
	private String keyboardDefaultStickerselected_5;
	private String standaloneStickerStickerviewed_6;
	private String standaloneStickerStickershared_7;
	private String createUser_8;
	private String firstOpen_9;
	private String appUpdate_10;
	private String appEachLaunch_11;
	private String appSessionStart_12;
	private String appSessionEnd_13;
	private String onboardingCompleted_14;
	private String connectKeyboardInitiated_15;
	private String enabledKeyboard_16;
	private String connectKeyboardCompleted_17;
	private String keyboardTakeMeThereClicked_18;
	private String signupClicked_19;
	private String signupFacebookInitiated_20;
	private String signupFacebookCompleted_21;
	private String signupMobileInitiated_22;
	private String signupMobileCompleted_23;
	private String signupMobileResetOtp_24;
	private String signupMobileExceededOtpAttempt_25;
	private String signupProfile_26;
	private String loginClicked_27;
	private String loginFacebookInitiated_28;
	private String loginFacebookCompleted_29;
	private String loginMobileInitiated_30;
	private String loginMobileCompleted_31;
	private String loginMobileResetOtp_32;
	private String loginMobileExceededOtpAttempt_33;
	private String legalClicked_34;
	private String newsClicked_35;
	private String promotionsClicked_36;
	private String recommendedStickersViewed_37;
	private String recommendedStickersShared_38;
	private String reminderClicked_39;
	private String announcementClicked_40;
	private String avatarReset_41;
	private String createAvatarInitiated_42;
	private String createAvatarCompleted_43;
	private String avatarGenderSelected_44;
	private String avatarFaceShapeSelected_45;
	private String avatarFaceShapeViewed_46;
	private String avatarSkintoneSelected_47;
	private String avatarSkintoneViewed_48;
	private String avatarHairstyleSelected_49;
	private String avatarHairstyleViewed_50;
	private String avatarHaircolourSelected_51;
	private String avatarHaircolourViewed_52;
	private String avatarFacialHairSelected_53;
	private String avatarFacialHairViewed_54;
	private String avatarFacialHaircolourSelected_55;
	private String avatarFacialHaircolourViewed_56;
	private String avatarEyeShapeSelected_57;
	private String avatarEyeShapeViewed_58;
	private String avatarEyeColourSelected_59;
	private String avatarEyeColourViewed_60;
	private String avatarEyebrowSelected_61;
	private String avatarEyebrowViewed_62;
	private String avatarNoseSelected_63;
	private String avatarNoseViewed_64;
	private String avatarMouthSelected_65;
	private String avatarMouthViewed_66;
	private String avatarLipstickSelected_67;
	private String avatarLipstickViewed_68;
	private String avatarBodytypeSelected_69;
	private String avatarBodytypeViewed_70;
	private String avatarOutfitSelected_71;
	private String avatarOutfitViewed_72;
	private String searchSuggestedKeywordSelected_73;
	private String searchTopCategoriesSelected_74;
	private String standaloneSearched_75;
	private String standaloneSearchresultsStickerShared_76;
	private String standaloneSearchresultsStickerViewed_77;
	private String settingLocationAccess_78;
	private String settingMediaAccess_79;
	private String settingsNotifications_80;
	private String settingsNotificationFrequency_81;
	private String settingsLanguagePreferenceSelected_82;
	private String settingsInviteFriends_83;
	private String settingsRateInPlaystoreInitiated_84;
	private String pushnotificationOpened_85;
	private String keyboardSessionEnd_86;
	private String settingsLogout_87;
	private String keyboardDisable_88;
	private String keyboardStickerShared_89;
	private String avatarEarViewed_90;
	private String avatarEarSelected_91;
	private String standaloneStickerLiked_92;
	private String standaloneStickerUnliked_93;
	private String standaloneStickerDownloaded_94;
	private String recommendedStickerDownloaded_95;
	private String standaloneSearchedresultsStickerDownloaded_96;
	private String standaloneOnboardingScreen1Viewed_97;
	private String standaloneOnboardingScreen2Viewed_98;
	private String standaloneOnboardingScreen3Viewed_99;
	private String standaloneOnboardingScreen4Viewed_100;
	private String promotionsStickerViewed_101;
	private String promotionsStickerShared_102;
	private String standaloneAcceptPermissionInitiated_103;
	private String promotionsStickerDownloaded_104;
	private String promotionsStickerLiked_105;
	private String promotionsStickerShared_106;
	private String notificationStickerShared_107;
	private String notificationStickerViewed_108;
	private String notificationStickerDownloaded_109;
	private String notificationStickerLiked_110;
	private String recommendationStickerLiked_111;
	private String recommendationStickerUnliked_112;
	private String promotionStickerUnLiked_113;
	private String notificationStickerUnLiked_114;
	private String recommendationStickerSelected_115;
	private String standaloneSearchedresultsStickerLiked_116;
	private String standaloneSearchedresultsStickerUnLiked_117;
	private String homescreenNewFeatureItemClicked_118;
	private String homeScreenViewed_119;
	private String avatarScreenViewed_120;
	private String stickerScreenViewed_121;
	private String settingScreenViewed_122;
	private String searchScreenViewed_123;

	public String getApplikcationID() {
		return ApplikcationID;
	}

	public void setApplikcationID(String applikcationID) {
		ApplikcationID = applikcationID;
	}

	public String getKeyboardSearched_1() {
		return keyboardSearched_1;
	}

	public void setKeyboardSearched_1(String keyboardSearched_1) {
		this.keyboardSearched_1 = keyboardSearched_1;
	}

	public String getKeyboardSessionStart_2() {
		return keyboardSessionStart_2;
	}

	public void setKeyboardSessionStart_2(String keyboardSessionStart_2) {
		this.keyboardSessionStart_2 = keyboardSessionStart_2;
	}

	public String getKeyboardTextpad_3() {
		return keyboardTextpad_3;
	}

	public void setKeyboardTextpad_3(String keyboardTextpad_3) {
		this.keyboardTextpad_3 = keyboardTextpad_3;
	}

	public String getKeyboardSearchresultsStickerselected_4() {
		return keyboardSearchresultsStickerselected_4;
	}

	public void setKeyboardSearchresultsStickerselected_4(String keyboardSearchresultsStickerselected_4) {
		this.keyboardSearchresultsStickerselected_4 = keyboardSearchresultsStickerselected_4;
	}

	public String getKeyboardDefaultStickerselected_5() {
		return keyboardDefaultStickerselected_5;
	}

	public void setKeyboardDefaultStickerselected_5(String keyboardDefaultStickerselected_5) {
		this.keyboardDefaultStickerselected_5 = keyboardDefaultStickerselected_5;
	}

	public String getStandaloneStickerStickerviewed_6() {
		return standaloneStickerStickerviewed_6;
	}

	public void setStandaloneStickerStickerviewed_6(String standaloneStickerStickerviewed_6) {
		this.standaloneStickerStickerviewed_6 = standaloneStickerStickerviewed_6;
	}

	public String getStandaloneStickerStickershared_7() {
		return standaloneStickerStickershared_7;
	}

	public void setStandaloneStickerStickershared_7(String standaloneStickerStickershared_7) {
		this.standaloneStickerStickershared_7 = standaloneStickerStickershared_7;
	}

	public String getCreateUser_8() {
		return createUser_8;
	}

	public void setCreateUser_8(String createUser_8) {
		this.createUser_8 = createUser_8;
	}

	public String getFirstOpen_9() {
		return firstOpen_9;
	}

	public void setFirstOpen_9(String firstOpen_9) {
		this.firstOpen_9 = firstOpen_9;
	}

	public String getAppUpdate_10() {
		return appUpdate_10;
	}

	public void setAppUpdate_10(String appUpdate_10) {
		this.appUpdate_10 = appUpdate_10;
	}

	public String getAppEachLaunch_11() {
		return appEachLaunch_11;
	}

	public void setAppEachLaunch_11(String appEachLaunch_11) {
		this.appEachLaunch_11 = appEachLaunch_11;
	}

	public String getAppSessionStart_12() {
		return appSessionStart_12;
	}

	public void setAppSessionStart_12(String appSessionStart_12) {
		this.appSessionStart_12 = appSessionStart_12;
	}

	public String getAppSessionEnd_13() {
		return appSessionEnd_13;
	}

	public void setAppSessionEnd_13(String appSessionEnd_13) {
		this.appSessionEnd_13 = appSessionEnd_13;
	}

	public String getOnboardingCompleted_14() {
		return onboardingCompleted_14;
	}

	public void setOnboardingCompleted_14(String onboardingCompleted_14) {
		this.onboardingCompleted_14 = onboardingCompleted_14;
	}

	public String getConnectKeyboardInitiated_15() {
		return connectKeyboardInitiated_15;
	}

	public void setConnectKeyboardInitiated_15(String connectKeyboardInitiated_15) {
		this.connectKeyboardInitiated_15 = connectKeyboardInitiated_15;
	}

	public String getEnabledKeyboard_16() {
		return enabledKeyboard_16;
	}

	public void setEnabledKeyboard_16(String enabledKeyboard_16) {
		this.enabledKeyboard_16 = enabledKeyboard_16;
	}

	public String getConnectKeyboardCompleted_17() {
		return connectKeyboardCompleted_17;
	}

	public void setConnectKeyboardCompleted_17(String connectKeyboardCompleted_17) {
		this.connectKeyboardCompleted_17 = connectKeyboardCompleted_17;
	}

	public String getKeyboardTakeMeThereClicked_18() {
		return keyboardTakeMeThereClicked_18;
	}

	public void setKeyboardTakeMeThereClicked_18(String keyboardTakeMeThereClicked_18) {
		this.keyboardTakeMeThereClicked_18 = keyboardTakeMeThereClicked_18;
	}

	public String getSignupClicked_19() {
		return signupClicked_19;
	}

	public void setSignupClicked_19(String signupClicked_19) {
		this.signupClicked_19 = signupClicked_19;
	}

	public String getSignupFacebookInitiated_20() {
		return signupFacebookInitiated_20;
	}

	public void setSignupFacebookInitiated_20(String signupFacebookInitiated_20) {
		this.signupFacebookInitiated_20 = signupFacebookInitiated_20;
	}

	public String getSignupFacebookCompleted_21() {
		return signupFacebookCompleted_21;
	}

	public void setSignupFacebookCompleted_21(String signupFacebookCompleted_21) {
		this.signupFacebookCompleted_21 = signupFacebookCompleted_21;
	}

	public String getSignupMobileInitiated_22() {
		return signupMobileInitiated_22;
	}

	public void setSignupMobileInitiated_22(String signupMobileInitiated_22) {
		this.signupMobileInitiated_22 = signupMobileInitiated_22;
	}

	public String getSignupMobileCompleted_23() {
		return signupMobileCompleted_23;
	}

	public void setSignupMobileCompleted_23(String signupMobileCompleted_23) {
		this.signupMobileCompleted_23 = signupMobileCompleted_23;
	}

	public String getSignupMobileResetOtp_24() {
		return signupMobileResetOtp_24;
	}

	public void setSignupMobileResetOtp_24(String signupMobileResetOtp_24) {
		this.signupMobileResetOtp_24 = signupMobileResetOtp_24;
	}

	public String getSignupMobileExceededOtpAttempt_25() {
		return signupMobileExceededOtpAttempt_25;
	}

	public void setSignupMobileExceededOtpAttempt_25(String signupMobileExceededOtpAttempt_25) {
		this.signupMobileExceededOtpAttempt_25 = signupMobileExceededOtpAttempt_25;
	}

	public String getSignupProfile_26() {
		return signupProfile_26;
	}

	public void setSignupProfile_26(String signupProfile_26) {
		this.signupProfile_26 = signupProfile_26;
	}

	public String getLoginClicked_27() {
		return loginClicked_27;
	}

	public void setLoginClicked_27(String loginClicked_27) {
		this.loginClicked_27 = loginClicked_27;
	}

	public String getLoginFacebookInitiated_28() {
		return loginFacebookInitiated_28;
	}

	public void setLoginFacebookInitiated_28(String loginFacebookInitiated_28) {
		this.loginFacebookInitiated_28 = loginFacebookInitiated_28;
	}

	public String getLoginFacebookCompleted_29() {
		return loginFacebookCompleted_29;
	}

	public void setLoginFacebookCompleted_29(String loginFacebookCompleted_29) {
		this.loginFacebookCompleted_29 = loginFacebookCompleted_29;
	}

	public String getLoginMobileInitiated_30() {
		return loginMobileInitiated_30;
	}

	public void setLoginMobileInitiated_30(String loginMobileInitiated_30) {
		this.loginMobileInitiated_30 = loginMobileInitiated_30;
	}

	public String getLoginMobileCompleted_31() {
		return loginMobileCompleted_31;
	}

	public void setLoginMobileCompleted_31(String loginMobileCompleted_31) {
		this.loginMobileCompleted_31 = loginMobileCompleted_31;
	}

	public String getLoginMobileResetOtp_32() {
		return loginMobileResetOtp_32;
	}

	public void setLoginMobileResetOtp_32(String loginMobileResetOtp_32) {
		this.loginMobileResetOtp_32 = loginMobileResetOtp_32;
	}

	public String getLoginMobileExceededOtpAttempt_33() {
		return loginMobileExceededOtpAttempt_33;
	}

	public void setLoginMobileExceededOtpAttempt_33(String loginMobileExceededOtpAttempt_33) {
		this.loginMobileExceededOtpAttempt_33 = loginMobileExceededOtpAttempt_33;
	}

	public String getLegalClicked_34() {
		return legalClicked_34;
	}

	public void setLegalClicked_34(String legalClicked_34) {
		this.legalClicked_34 = legalClicked_34;
	}

	public String getNewsClicked_35() {
		return newsClicked_35;
	}

	public void setNewsClicked_35(String newsClicked_35) {
		this.newsClicked_35 = newsClicked_35;
	}

	public String getPromotionsClicked_36() {
		return promotionsClicked_36;
	}

	public void setPromotionsClicked_36(String promotionsClicked_36) {
		this.promotionsClicked_36 = promotionsClicked_36;
	}

	public String getRecommendedStickersViewed_37() {
		return recommendedStickersViewed_37;
	}

	public void setRecommendedStickersViewed_37(String recommendedStickersViewed_37) {
		this.recommendedStickersViewed_37 = recommendedStickersViewed_37;
	}

	public String getRecommendedStickersShared_38() {
		return recommendedStickersShared_38;
	}

	public void setRecommendedStickersShared_38(String recommendedStickersShared_38) {
		this.recommendedStickersShared_38 = recommendedStickersShared_38;
	}

	public String getReminderClicked_39() {
		return reminderClicked_39;
	}

	public void setReminderClicked_39(String reminderClicked_39) {
		this.reminderClicked_39 = reminderClicked_39;
	}

	public String getAnnouncementClicked_40() {
		return announcementClicked_40;
	}

	public void setAnnouncementClicked_40(String announcementClicked_40) {
		this.announcementClicked_40 = announcementClicked_40;
	}

	public String getAvatarReset_41() {
		return avatarReset_41;
	}

	public void setAvatarReset_41(String avatarReset_41) {
		this.avatarReset_41 = avatarReset_41;
	}

	public String getCreateAvatarInitiated_42() {
		return createAvatarInitiated_42;
	}

	public void setCreateAvatarInitiated_42(String createAvatarInitiated_42) {
		this.createAvatarInitiated_42 = createAvatarInitiated_42;
	}

	public String getCreateAvatarCompleted_43() {
		return createAvatarCompleted_43;
	}

	public void setCreateAvatarCompleted_43(String createAvatarCompleted_43) {
		this.createAvatarCompleted_43 = createAvatarCompleted_43;
	}

	public String getAvatarGenderSelected_44() {
		return avatarGenderSelected_44;
	}

	public void setAvatarGenderSelected_44(String avatarGenderSelected_44) {
		this.avatarGenderSelected_44 = avatarGenderSelected_44;
	}

	public String getAvatarFaceShapeSelected_45() {
		return avatarFaceShapeSelected_45;
	}

	public void setAvatarFaceShapeSelected_45(String avatarFaceShapeSelected_45) {
		this.avatarFaceShapeSelected_45 = avatarFaceShapeSelected_45;
	}

	public String getAvatarFaceShapeViewed_46() {
		return avatarFaceShapeViewed_46;
	}

	public void setAvatarFaceShapeViewed_46(String avatarFaceShapeViewed_46) {
		this.avatarFaceShapeViewed_46 = avatarFaceShapeViewed_46;
	}

	public String getAvatarSkintoneSelected_47() {
		return avatarSkintoneSelected_47;
	}

	public void setAvatarSkintoneSelected_47(String avatarSkintoneSelected_47) {
		this.avatarSkintoneSelected_47 = avatarSkintoneSelected_47;
	}

	public String getAvatarSkintoneViewed_48() {
		return avatarSkintoneViewed_48;
	}

	public void setAvatarSkintoneViewed_48(String avatarSkintoneViewed_48) {
		this.avatarSkintoneViewed_48 = avatarSkintoneViewed_48;
	}

	public String getAvatarHairstyleSelected_49() {
		return avatarHairstyleSelected_49;
	}

	public void setAvatarHairstyleSelected_49(String avatarHairstyleSelected_49) {
		this.avatarHairstyleSelected_49 = avatarHairstyleSelected_49;
	}

	public String getAvatarHairstyleViewed_50() {
		return avatarHairstyleViewed_50;
	}

	public void setAvatarHairstyleViewed_50(String avatarHairstyleViewed_50) {
		this.avatarHairstyleViewed_50 = avatarHairstyleViewed_50;
	}

	public String getAvatarHaircolourSelected_51() {
		return avatarHaircolourSelected_51;
	}

	public void setAvatarHaircolourSelected_51(String avatarHaircolourSelected_51) {
		this.avatarHaircolourSelected_51 = avatarHaircolourSelected_51;
	}

	public String getAvatarHaircolourViewed_52() {
		return avatarHaircolourViewed_52;
	}

	public void setAvatarHaircolourViewed_52(String avatarHaircolourViewed_52) {
		this.avatarHaircolourViewed_52 = avatarHaircolourViewed_52;
	}

	public String getAvatarFacialHairSelected_53() {
		return avatarFacialHairSelected_53;
	}

	public void setAvatarFacialHairSelected_53(String avatarFacialHairSelected_53) {
		this.avatarFacialHairSelected_53 = avatarFacialHairSelected_53;
	}

	public String getAvatarFacialHairViewed_54() {
		return avatarFacialHairViewed_54;
	}

	public void setAvatarFacialHairViewed_54(String avatarFacialHairViewed_54) {
		this.avatarFacialHairViewed_54 = avatarFacialHairViewed_54;
	}

	public String getAvatarFacialHaircolourSelected_55() {
		return avatarFacialHaircolourSelected_55;
	}

	public void setAvatarFacialHaircolourSelected_55(String avatarFacialHaircolourSelected_55) {
		this.avatarFacialHaircolourSelected_55 = avatarFacialHaircolourSelected_55;
	}

	public String getAvatarFacialHaircolourViewed_56() {
		return avatarFacialHaircolourViewed_56;
	}

	public void setAvatarFacialHaircolourViewed_56(String avatarFacialHaircolourViewed_56) {
		this.avatarFacialHaircolourViewed_56 = avatarFacialHaircolourViewed_56;
	}

	public String getAvatarEyeShapeSelected_57() {
		return avatarEyeShapeSelected_57;
	}

	public void setAvatarEyeShapeSelected_57(String avatarEyeShapeSelected_57) {
		this.avatarEyeShapeSelected_57 = avatarEyeShapeSelected_57;
	}

	public String getAvatarEyeShapeViewed_58() {
		return avatarEyeShapeViewed_58;
	}

	public void setAvatarEyeShapeViewed_58(String avatarEyeShapeViewed_58) {
		this.avatarEyeShapeViewed_58 = avatarEyeShapeViewed_58;
	}

	public String getAvatarEyeColourSelected_59() {
		return avatarEyeColourSelected_59;
	}

	public void setAvatarEyeColourSelected_59(String avatarEyeColourSelected_59) {
		this.avatarEyeColourSelected_59 = avatarEyeColourSelected_59;
	}

	public String getAvatarEyeColourViewed_60() {
		return avatarEyeColourViewed_60;
	}

	public void setAvatarEyeColourViewed_60(String avatarEyeColourViewed_60) {
		this.avatarEyeColourViewed_60 = avatarEyeColourViewed_60;
	}

	public String getAvatarEyebrowSelected_61() {
		return avatarEyebrowSelected_61;
	}

	public void setAvatarEyebrowSelected_61(String avatarEyebrowSelected_61) {
		this.avatarEyebrowSelected_61 = avatarEyebrowSelected_61;
	}

	public String getAvatarEyebrowViewed_62() {
		return avatarEyebrowViewed_62;
	}

	public void setAvatarEyebrowViewed_62(String avatarEyebrowViewed_62) {
		this.avatarEyebrowViewed_62 = avatarEyebrowViewed_62;
	}

	public String getAvatarNoseSelected_63() {
		return avatarNoseSelected_63;
	}

	public void setAvatarNoseSelected_63(String avatarNoseSelected_63) {
		this.avatarNoseSelected_63 = avatarNoseSelected_63;
	}

	public String getAvatarNoseViewed_64() {
		return avatarNoseViewed_64;
	}

	public void setAvatarNoseViewed_64(String avatarNoseViewed_64) {
		this.avatarNoseViewed_64 = avatarNoseViewed_64;
	}

	public String getAvatarMouthSelected_65() {
		return avatarMouthSelected_65;
	}

	public void setAvatarMouthSelected_65(String avatarMouthSelected_65) {
		this.avatarMouthSelected_65 = avatarMouthSelected_65;
	}

	public String getAvatarMouthViewed_66() {
		return avatarMouthViewed_66;
	}

	public void setAvatarMouthViewed_66(String avatarMouthViewed_66) {
		this.avatarMouthViewed_66 = avatarMouthViewed_66;
	}

	public String getAvatarLipstickSelected_67() {
		return avatarLipstickSelected_67;
	}

	public void setAvatarLipstickSelected_67(String avatarLipstickSelected_67) {
		this.avatarLipstickSelected_67 = avatarLipstickSelected_67;
	}

	public String getAvatarLipstickViewed_68() {
		return avatarLipstickViewed_68;
	}

	public void setAvatarLipstickViewed_68(String avatarLipstickViewed_68) {
		this.avatarLipstickViewed_68 = avatarLipstickViewed_68;
	}

	public String getAvatarBodytypeSelected_69() {
		return avatarBodytypeSelected_69;
	}

	public void setAvatarBodytypeSelected_69(String avatarBodytypeSelected_69) {
		this.avatarBodytypeSelected_69 = avatarBodytypeSelected_69;
	}

	public String getAvatarBodytypeViewed_70() {
		return avatarBodytypeViewed_70;
	}

	public void setAvatarBodytypeViewed_70(String avatarBodytypeViewed_70) {
		this.avatarBodytypeViewed_70 = avatarBodytypeViewed_70;
	}

	public String getAvatarOutfitSelected_71() {
		return avatarOutfitSelected_71;
	}

	public void setAvatarOutfitSelected_71(String avatarOutfitSelected_71) {
		this.avatarOutfitSelected_71 = avatarOutfitSelected_71;
	}

	public String getAvatarOutfitViewed_72() {
		return avatarOutfitViewed_72;
	}

	public void setAvatarOutfitViewed_72(String avatarOutfitViewed_72) {
		this.avatarOutfitViewed_72 = avatarOutfitViewed_72;
	}

	public String getSearchSuggestedKeywordSelected_73() {
		return searchSuggestedKeywordSelected_73;
	}

	public void setSearchSuggestedKeywordSelected_73(String searchSuggestedKeywordSelected_73) {
		this.searchSuggestedKeywordSelected_73 = searchSuggestedKeywordSelected_73;
	}

	public String getSearchTopCategoriesSelected_74() {
		return searchTopCategoriesSelected_74;
	}

	public void setSearchTopCategoriesSelected_74(String searchTopCategoriesSelected_74) {
		this.searchTopCategoriesSelected_74 = searchTopCategoriesSelected_74;
	}

	public String getStandaloneSearched_75() {
		return standaloneSearched_75;
	}

	public void setStandaloneSearched_75(String standaloneSearched_75) {
		this.standaloneSearched_75 = standaloneSearched_75;
	}

	public String getStandaloneSearchresultsStickerShared_76() {
		return standaloneSearchresultsStickerShared_76;
	}

	public void setStandaloneSearchresultsStickerShared_76(String standaloneSearchresultsStickerShared_76) {
		this.standaloneSearchresultsStickerShared_76 = standaloneSearchresultsStickerShared_76;
	}

	public String getStandaloneSearchresultsStickerViewed_77() {
		return standaloneSearchresultsStickerViewed_77;
	}

	public void setStandaloneSearchresultsStickerViewed_77(String standaloneSearchresultsStickerViewed_77) {
		this.standaloneSearchresultsStickerViewed_77 = standaloneSearchresultsStickerViewed_77;
	}

	public String getSettingLocationAccess_78() {
		return settingLocationAccess_78;
	}

	public void setSettingLocationAccess_78(String settingLocationAccess_78) {
		this.settingLocationAccess_78 = settingLocationAccess_78;
	}

	public String getSettingMediaAccess_79() {
		return settingMediaAccess_79;
	}

	public void setSettingMediaAccess_79(String settingMediaAccess_79) {
		this.settingMediaAccess_79 = settingMediaAccess_79;
	}

	public String getSettingsNotifications_80() {
		return settingsNotifications_80;
	}

	public void setSettingsNotifications_80(String settingsNotifications_80) {
		this.settingsNotifications_80 = settingsNotifications_80;
	}

	public String getSettingsNotificationFrequency_81() {
		return settingsNotificationFrequency_81;
	}

	public void setSettingsNotificationFrequency_81(String settingsNotificationFrequency_81) {
		this.settingsNotificationFrequency_81 = settingsNotificationFrequency_81;
	}

	public String getSettingsLanguagePreferenceSelected_82() {
		return settingsLanguagePreferenceSelected_82;
	}

	public void setSettingsLanguagePreferenceSelected_82(String settingsLanguagePreferenceSelected_82) {
		this.settingsLanguagePreferenceSelected_82 = settingsLanguagePreferenceSelected_82;
	}

	public String getSettingsInviteFriends_83() {
		return settingsInviteFriends_83;
	}

	public void setSettingsInviteFriends_83(String settingsInviteFriends_83) {
		this.settingsInviteFriends_83 = settingsInviteFriends_83;
	}

	public String getSettingsRateInPlaystoreInitiated_84() {
		return settingsRateInPlaystoreInitiated_84;
	}

	public void setSettingsRateInPlaystoreInitiated_84(String settingsRateInPlaystoreInitiated_84) {
		this.settingsRateInPlaystoreInitiated_84 = settingsRateInPlaystoreInitiated_84;
	}

	public String getPushnotificationOpened_85() {
		return pushnotificationOpened_85;
	}

	public void setPushnotificationOpened_85(String pushnotificationOpened_85) {
		this.pushnotificationOpened_85 = pushnotificationOpened_85;
	}

	public String getKeyboardSessionEnd_86() {
		return keyboardSessionEnd_86;
	}

	public void setKeyboardSessionEnd_86(String keyboardSessionEnd_86) {
		this.keyboardSessionEnd_86 = keyboardSessionEnd_86;
	}

	public String getSettingsLogout_87() {
		return settingsLogout_87;
	}

	public void setSettingsLogout_87(String settingsLogout_87) {
		this.settingsLogout_87 = settingsLogout_87;
	}

	public String getKeyboardDisable_88() {
		return keyboardDisable_88;
	}

	public void setKeyboardDisable_88(String keyboardDisable_88) {
		this.keyboardDisable_88 = keyboardDisable_88;
	}

	public String getKeyboardStickerShared_89() {
		return keyboardStickerShared_89;
	}

	public void setKeyboardStickerShared_89(String keyboardStickerShared_89) {
		this.keyboardStickerShared_89 = keyboardStickerShared_89;
	}

	public String getAvatarEarViewed_90() {
		return avatarEarViewed_90;
	}

	public void setAvatarEarViewed_90(String avatarEarViewed_90) {
		this.avatarEarViewed_90 = avatarEarViewed_90;
	}

	public String getAvatarEarSelected_91() {
		return avatarEarSelected_91;
	}

	public void setAvatarEarSelected_91(String avatarEarSelected_91) {
		this.avatarEarSelected_91 = avatarEarSelected_91;
	}

	public String getStandaloneStickerLiked_92() {
		return standaloneStickerLiked_92;
	}

	public void setStandaloneStickerLiked_92(String standaloneStickerLiked_92) {
		this.standaloneStickerLiked_92 = standaloneStickerLiked_92;
	}

	public String getStandaloneStickerUnliked_93() {
		return standaloneStickerUnliked_93;
	}

	public void setStandaloneStickerUnliked_93(String standaloneStickerUnliked_93) {
		this.standaloneStickerUnliked_93 = standaloneStickerUnliked_93;
	}

	public String getStandaloneStickerDownloaded_94() {
		return standaloneStickerDownloaded_94;
	}

	public void setStandaloneStickerDownloaded_94(String standaloneStickerDownloaded_94) {
		this.standaloneStickerDownloaded_94 = standaloneStickerDownloaded_94;
	}

	public String getRecommendedStickerDownloaded_95() {
		return recommendedStickerDownloaded_95;
	}

	public void setRecommendedStickerDownloaded_95(String recommendedStickerDownloaded_95) {
		this.recommendedStickerDownloaded_95 = recommendedStickerDownloaded_95;
	}

	public String getStandaloneSearchedresultsStickerDownloaded_96() {
		return standaloneSearchedresultsStickerDownloaded_96;
	}

	public void setStandaloneSearchedresultsStickerDownloaded_96(String standaloneSearchedresultsStickerDownloaded_96) {
		this.standaloneSearchedresultsStickerDownloaded_96 = standaloneSearchedresultsStickerDownloaded_96;
	}

	public String getStandaloneOnboardingScreen1Viewed_97() {
		return standaloneOnboardingScreen1Viewed_97;
	}

	public void setStandaloneOnboardingScreen1Viewed_97(String standaloneOnboardingScreen1Viewed_97) {
		this.standaloneOnboardingScreen1Viewed_97 = standaloneOnboardingScreen1Viewed_97;
	}

	public String getStandaloneOnboardingScreen2Viewed_98() {
		return standaloneOnboardingScreen2Viewed_98;
	}

	public void setStandaloneOnboardingScreen2Viewed_98(String standaloneOnboardingScreen2Viewed_98) {
		this.standaloneOnboardingScreen2Viewed_98 = standaloneOnboardingScreen2Viewed_98;
	}

	public String getStandaloneOnboardingScreen3Viewed_99() {
		return standaloneOnboardingScreen3Viewed_99;
	}

	public void setStandaloneOnboardingScreen3Viewed_99(String standaloneOnboardingScreen3Viewed_99) {
		this.standaloneOnboardingScreen3Viewed_99 = standaloneOnboardingScreen3Viewed_99;
	}

	public String getStandaloneOnboardingScreen4Viewed_100() {
		return standaloneOnboardingScreen4Viewed_100;
	}

	public void setStandaloneOnboardingScreen4Viewed_100(String standaloneOnboardingScreen4Viewed_100) {
		this.standaloneOnboardingScreen4Viewed_100 = standaloneOnboardingScreen4Viewed_100;
	}

	public String getPromotionsStickerViewed_101() {
		return promotionsStickerViewed_101;
	}

	public void setPromotionsStickerViewed_101(String promotionsStickerViewed_101) {
		this.promotionsStickerViewed_101 = promotionsStickerViewed_101;
	}

	public String getPromotionsStickerShared_102() {
		return promotionsStickerShared_102;
	}

	public void setPromotionsStickerShared_102(String promotionsStickerShared_102) {
		this.promotionsStickerShared_102 = promotionsStickerShared_102;
	}

	public String getStandaloneAcceptPermissionInitiated_103() {
		return standaloneAcceptPermissionInitiated_103;
	}

	public void setStandaloneAcceptPermissionInitiated_103(String standaloneAcceptPermissionInitiated_103) {
		this.standaloneAcceptPermissionInitiated_103 = standaloneAcceptPermissionInitiated_103;
	}

	public String getPromotionsStickerDownloaded_104() {
		return promotionsStickerDownloaded_104;
	}

	public void setPromotionsStickerDownloaded_104(String promotionsStickerDownloaded_104) {
		this.promotionsStickerDownloaded_104 = promotionsStickerDownloaded_104;
	}

	public String getPromotionsStickerLiked_105() {
		return promotionsStickerLiked_105;
	}

	public void setPromotionsStickerLiked_105(String promotionsStickerLiked_105) {
		this.promotionsStickerLiked_105 = promotionsStickerLiked_105;
	}

	public String getPromotionsStickerShared_106() {
		return promotionsStickerShared_106;
	}

	public void setPromotionsStickerShared_106(String promotionsStickerShared_106) {
		this.promotionsStickerShared_106 = promotionsStickerShared_106;
	}

	public String getNotificationStickerShared_107() {
		return notificationStickerShared_107;
	}

	public void setNotificationStickerShared_107(String notificationStickerShared_107) {
		this.notificationStickerShared_107 = notificationStickerShared_107;
	}

	public String getNotificationStickerViewed_108() {
		return notificationStickerViewed_108;
	}

	public void setNotificationStickerViewed_108(String notificationStickerViewed_108) {
		this.notificationStickerViewed_108 = notificationStickerViewed_108;
	}

	public String getNotificationStickerDownloaded_109() {
		return notificationStickerDownloaded_109;
	}

	public void setNotificationStickerDownloaded_109(String notificationStickerDownloaded_109) {
		this.notificationStickerDownloaded_109 = notificationStickerDownloaded_109;
	}

	public String getNotificationStickerLiked_110() {
		return notificationStickerLiked_110;
	}

	public void setNotificationStickerLiked_110(String notificationStickerLiked_110) {
		this.notificationStickerLiked_110 = notificationStickerLiked_110;
	}

	public String getRecommendationStickerLiked_111() {
		return recommendationStickerLiked_111;
	}

	public void setRecommendationStickerLiked_111(String recommendationStickerLiked_111) {
		this.recommendationStickerLiked_111 = recommendationStickerLiked_111;
	}

	public String getRecommendationStickerUnliked_112() {
		return recommendationStickerUnliked_112;
	}

	public void setRecommendationStickerUnliked_112(String recommendationStickerUnliked_112) {
		this.recommendationStickerUnliked_112 = recommendationStickerUnliked_112;
	}

	public String getPromotionStickerUnLiked_113() {
		return promotionStickerUnLiked_113;
	}

	public void setPromotionStickerUnLiked_113(String promotionStickerUnLiked_113) {
		this.promotionStickerUnLiked_113 = promotionStickerUnLiked_113;
	}

	public String getNotificationStickerUnLiked_114() {
		return notificationStickerUnLiked_114;
	}

	public void setNotificationStickerUnLiked_114(String notificationStickerUnLiked_114) {
		this.notificationStickerUnLiked_114 = notificationStickerUnLiked_114;
	}

	public String getRecommendationStickerSelected_115() {
		return recommendationStickerSelected_115;
	}

	public void setRecommendationStickerSelected_115(String recommendationStickerSelected_115) {
		this.recommendationStickerSelected_115 = recommendationStickerSelected_115;
	}

	public String getStandaloneSearchedresultsStickerLiked_116() {
		return standaloneSearchedresultsStickerLiked_116;
	}

	public void setStandaloneSearchedresultsStickerLiked_116(String standaloneSearchedresultsStickerLiked_116) {
		this.standaloneSearchedresultsStickerLiked_116 = standaloneSearchedresultsStickerLiked_116;
	}

	public String getStandaloneSearchedresultsStickerUnLiked_117() {
		return standaloneSearchedresultsStickerUnLiked_117;
	}

	public void setStandaloneSearchedresultsStickerUnLiked_117(String standaloneSearchedresultsStickerUnLiked_117) {
		this.standaloneSearchedresultsStickerUnLiked_117 = standaloneSearchedresultsStickerUnLiked_117;
	}

	public String getHomescreenNewFeatureItemClicked_118() {
		return homescreenNewFeatureItemClicked_118;
	}

	public void setHomescreenNewFeatureItemClicked_118(String homescreenNewFeatureItemClicked_118) {
		this.homescreenNewFeatureItemClicked_118 = homescreenNewFeatureItemClicked_118;
	}

	public String getHomeScreenViewed_119() {
		return homeScreenViewed_119;
	}

	public void setHomeScreenViewed_119(String homeScreenViewed_119) {
		this.homeScreenViewed_119 = homeScreenViewed_119;
	}

	public String getAvatarScreenViewed_120() {
		return avatarScreenViewed_120;
	}

	public void setAvatarScreenViewed_120(String avatarScreenViewed_120) {
		this.avatarScreenViewed_120 = avatarScreenViewed_120;
	}

	public String getStickerScreenViewed_121() {
		return stickerScreenViewed_121;
	}

	public void setStickerScreenViewed_121(String stickerScreenViewed_121) {
		this.stickerScreenViewed_121 = stickerScreenViewed_121;
	}

	public String getSettingScreenViewed_122() {
		return settingScreenViewed_122;
	}

	public void setSettingScreenViewed_122(String settingScreenViewed_122) {
		this.settingScreenViewed_122 = settingScreenViewed_122;
	}

	public String getSearchScreenViewed_123() {
		return searchScreenViewed_123;
	}

	public void setSearchScreenViewed_123(String searchScreenViewed_123) {
		this.searchScreenViewed_123 = searchScreenViewed_123;
	}

	public UserActivityHelper(String applikcationID, ArrayList<String> list) {
		super();
		ApplikcationID = applikcationID;
		if (list.size() < 123) {
			System.out.println("Cannot Create Object");
		}
		this.keyboardSearched_1 = list.get(0);
		this.keyboardSessionStart_2 = list.get(1);
		this.keyboardTextpad_3 = list.get(2);
		this.keyboardSearchresultsStickerselected_4 = list.get(3);
		this.keyboardDefaultStickerselected_5 = list.get(4);
		this.standaloneStickerStickerviewed_6 = list.get(5);
		this.standaloneStickerStickershared_7 = list.get(6);
		this.createUser_8 = list.get(7);
		this.firstOpen_9 = list.get(8);
		this.appUpdate_10 = list.get(9);
		this.appEachLaunch_11 = list.get(10);
		this.appSessionStart_12 = list.get(11);
		this.appSessionEnd_13 = list.get(12);
		this.onboardingCompleted_14 = list.get(13);
		this.connectKeyboardInitiated_15 = list.get(14);
		this.enabledKeyboard_16 = list.get(15);
		this.connectKeyboardCompleted_17 = list.get(16);
		this.keyboardTakeMeThereClicked_18 = list.get(17);
		this.signupClicked_19 = list.get(18);
		this.signupFacebookInitiated_20 = list.get(19);
		this.signupFacebookCompleted_21 = list.get(20);
		this.signupMobileInitiated_22 = list.get(21);
		this.signupMobileCompleted_23 = list.get(22);
		this.signupMobileResetOtp_24 = list.get(23);
		this.signupMobileExceededOtpAttempt_25 = list.get(24);
		this.signupProfile_26 = list.get(25);
		this.loginClicked_27 = list.get(26);
		this.loginFacebookInitiated_28 = list.get(27);
		this.loginFacebookCompleted_29 = list.get(28);
		this.loginMobileInitiated_30 = list.get(29);
		this.loginMobileCompleted_31 = list.get(30);
		this.loginMobileResetOtp_32 = list.get(31);
		this.loginMobileExceededOtpAttempt_33 = list.get(32);
		this.legalClicked_34 = list.get(33);
		this.newsClicked_35 = list.get(34);
		this.promotionsClicked_36 = list.get(35);
		this.recommendedStickersViewed_37 = list.get(36);
		this.recommendedStickersShared_38 = list.get(37);
		this.reminderClicked_39 = list.get(38);
		this.announcementClicked_40 = list.get(39);
		this.avatarReset_41 = list.get(40);
		this.createAvatarInitiated_42 = list.get(41);
		this.createAvatarCompleted_43 = list.get(42);
		this.avatarGenderSelected_44 = list.get(43);
		this.avatarFaceShapeSelected_45 = list.get(44);
		this.avatarFaceShapeViewed_46 = list.get(45);
		this.avatarSkintoneSelected_47 = list.get(46);
		this.avatarSkintoneViewed_48 = list.get(47);
		this.avatarHairstyleSelected_49 = list.get(48);
		this.avatarHairstyleViewed_50 = list.get(49);
		this.avatarHaircolourSelected_51 = list.get(50);
		this.avatarHaircolourViewed_52 = list.get(51);
		this.avatarFacialHairSelected_53 = list.get(52);
		this.avatarFacialHairViewed_54 = list.get(53);
		this.avatarFacialHaircolourSelected_55 = list.get(54);
		this.avatarFacialHaircolourViewed_56 = list.get(55);
		this.avatarEyeShapeSelected_57 = list.get(56);
		this.avatarEyeShapeViewed_58 = list.get(57);
		this.avatarEyeColourSelected_59 = list.get(58);
		this.avatarEyeColourViewed_60 = list.get(59);
		this.avatarEyebrowSelected_61 = list.get(60);
		this.avatarEyebrowViewed_62 = list.get(61);
		this.avatarNoseSelected_63 = list.get(62);
		this.avatarNoseViewed_64 = list.get(63);
		this.avatarMouthSelected_65 = list.get(64);
		this.avatarMouthViewed_66 = list.get(65);
		this.avatarLipstickSelected_67 = list.get(66);
		this.avatarLipstickViewed_68 = list.get(67);
		this.avatarBodytypeSelected_69 = list.get(68);
		this.avatarBodytypeViewed_70 = list.get(69);
		this.avatarOutfitSelected_71 = list.get(70);
		this.avatarOutfitViewed_72 = list.get(71);
		this.searchSuggestedKeywordSelected_73 = list.get(72);
		this.searchTopCategoriesSelected_74 = list.get(73);
		this.standaloneSearched_75 = list.get(74);
		this.standaloneSearchresultsStickerShared_76 = list.get(75);
		this.standaloneSearchresultsStickerViewed_77 = list.get(76);
		this.settingLocationAccess_78 = list.get(77);
		this.settingMediaAccess_79 = list.get(78);
		this.settingsNotifications_80 = list.get(79);
		this.settingsNotificationFrequency_81 = list.get(80);
		this.settingsLanguagePreferenceSelected_82 = list.get(81);
		this.settingsInviteFriends_83 = list.get(82);
		this.settingsRateInPlaystoreInitiated_84 = list.get(83);
		this.pushnotificationOpened_85 = list.get(84);
		this.keyboardSessionEnd_86 = list.get(85);
		this.settingsLogout_87 = list.get(86);
		this.keyboardDisable_88 = list.get(87);
		this.keyboardStickerShared_89 = list.get(88);
		this.avatarEarViewed_90 = list.get(89);
		this.avatarEarSelected_91 = list.get(90);
		this.standaloneStickerLiked_92 = list.get(91);
		this.standaloneStickerUnliked_93 = list.get(92);
		this.standaloneStickerDownloaded_94 = list.get(93);
		this.recommendedStickerDownloaded_95 = list.get(94);
		this.standaloneSearchedresultsStickerDownloaded_96 = list.get(95);
		this.standaloneOnboardingScreen1Viewed_97 = list.get(96);
		this.standaloneOnboardingScreen2Viewed_98 = list.get(97);
		this.standaloneOnboardingScreen3Viewed_99 = list.get(98);
		this.standaloneOnboardingScreen4Viewed_100 = list.get(99);
		this.promotionsStickerViewed_101 = list.get(100);
		this.promotionsStickerShared_102 = list.get(101);
		this.standaloneAcceptPermissionInitiated_103 = list.get(102);
		this.promotionsStickerDownloaded_104 = list.get(103);
		this.promotionsStickerLiked_105 = list.get(104);
		this.promotionsStickerShared_106 = list.get(105);
		this.notificationStickerShared_107 = list.get(106);
		this.notificationStickerViewed_108 = list.get(107);
		this.notificationStickerDownloaded_109 = list.get(108);
		this.notificationStickerLiked_110 = list.get(109);
		this.recommendationStickerLiked_111 = list.get(110);
		this.recommendationStickerUnliked_112 = list.get(111);
		this.promotionStickerUnLiked_113 = list.get(112);
		this.notificationStickerUnLiked_114 = list.get(113);
		this.recommendationStickerSelected_115 = list.get(114);
		this.standaloneSearchedresultsStickerLiked_116 = list.get(115);
		this.standaloneSearchedresultsStickerUnLiked_117 = list.get(116);
		this.homescreenNewFeatureItemClicked_118 = list.get(117);
		this.homeScreenViewed_119 = list.get(118);
		this.avatarScreenViewed_120 = list.get(119);
		this.stickerScreenViewed_121 = list.get(120);
		this.settingScreenViewed_122 = list.get(121);
		this.searchScreenViewed_123 = list.get(122);
	}

}
