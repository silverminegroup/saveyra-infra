package com.saveyra.analytics.UserActivityHelper;

import java.util.HashMap;

public class UserActivityHourBasis {
	
	private String Date;
	private String Hour;
	private String Platform;
	private String UserType;
	private long UserCount;
	private long Male;
	private long Female;
	private HashMap<String, Integer> targetMap;
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getHour() {
		return Hour;
	}
	public void setHour(String hour) {
		Hour = hour;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public String getUserType() {
		return UserType;
	}
	public void setUserType(String userType) {
		UserType = userType;
	}
	public long getUserCount() {
		return UserCount;
	}
	public void setUserCount(long userCount) {
		UserCount = userCount;
	}
	public long getMale() {
		return Male;
	}
	public void setMale(long male) {
		Male = male;
	}
	public long getFemale() {
		return Female;
	}
	public void setFemale(long female) {
		Female = female;
	}
	public HashMap<String, Integer> getTargetMap() {
		return targetMap;
	}
	public void setTargetMap(HashMap<String, Integer> targetMap) {
		this.targetMap = targetMap;
	}
	
	public UserActivityHourBasis(String date, String hour, String platform, String userType, long userCount, long male,
			long female, HashMap<String, Integer> targetMap) {
		super();
		Date = date;
		Hour = hour;
		Platform = platform;
		UserType = userType;
		UserCount = userCount;
		Male = male;
		Female = female;
		this.targetMap = targetMap;
	}
}
