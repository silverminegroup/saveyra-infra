package com.saveyra.analytics.UserActivityHelper;

public class ActiveUserData {
	private String Date;
	private String Platform;
	private long UserCount;
	private long NewUser;
	private long ReturningUser;
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public long getUserCount() {
		return UserCount;
	}
	public void setUserCount(long userCount) {
		UserCount = userCount;
	}
	public long getNewUser() {
		return NewUser;
	}
	public void setNewUser(long newUser) {
		NewUser = newUser;
	}
	public long getReturningUser() {
		return ReturningUser;
	}
	public void setReturningUser(long returningUser) {
		ReturningUser = returningUser;
	}
	public ActiveUserData(String date, String platform, long userCount, long newUser, long returningUser) {
		super();
		Date = date;
		Platform = platform;
		UserCount = userCount;
		NewUser = newUser;
		ReturningUser = returningUser;
	}
	
}
