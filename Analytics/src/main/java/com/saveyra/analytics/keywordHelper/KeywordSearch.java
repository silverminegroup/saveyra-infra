package com.saveyra.analytics.keywordHelper;

public class KeywordSearch {
	private String Date;
	private String Keyword;
	private long Count;
	
	public String getKeyword() {
		return Keyword;
	}
	public void setKeyword(String keyword) {
		Keyword = keyword;
	}
	public long getCount() {
		return Count;
	}
	public void setCount(long count) {
		Count = count;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public KeywordSearch(String date, String keyword, long count) {
		super();
		Date = date;
		Keyword = keyword;
		Count = count;
	}	
}
