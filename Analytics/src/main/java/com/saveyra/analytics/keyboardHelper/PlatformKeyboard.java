package com.saveyra.analytics.keyboardHelper;

public class PlatformKeyboard {
	private String Date;
	private String Platform;
	private long Enabled;
	
	public PlatformKeyboard(String sDate,String sPlatform,long sEnabled) {
		this.Date = sDate;
		this.Platform = sPlatform;
		this.Enabled = sEnabled;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public String getPlatform() {
		return Platform;
	}

	public void setPlatform(String platform) {
		this.Platform = platform;
	}

	public long getEnabled() {
		return Enabled;
	}

	public void setEnabled(long enabled) {
		Enabled = enabled;
	}
}
