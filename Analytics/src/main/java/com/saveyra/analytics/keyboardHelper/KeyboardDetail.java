package com.saveyra.analytics.keyboardHelper;

public class KeyboardDetail {
	private String Date;
	private String Platform;
	private long Initialized;
	private long Enabled;
	private long Completed;
	private long Searched;
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public long getInitialized() {
		return Initialized;
	}
	public void setInitialized(long initialized) {
		Initialized = initialized;
	}
	public long getEnabled() {
		return Enabled;
	}
	public void setEnabled(long enabled) {
		Enabled = enabled;
	}
	public long getCompleted() {
		return Completed;
	}
	public void setCompleted(long completed) {
		Completed = completed;
	}
	public long getSearched() {
		return Searched;
	}
	public void setSearched(long searched) {
		Searched = searched;
	}
	public KeyboardDetail(String date, String platform, long initialized, long enabled, long completed, long searched) {
		super();
		Date = date;
		Platform = platform;
		Initialized = initialized;
		Enabled = enabled;
		Completed = completed;
		Searched = searched;
	}	
}
