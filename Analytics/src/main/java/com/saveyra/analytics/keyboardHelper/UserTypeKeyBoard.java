package com.saveyra.analytics.keyboardHelper;

public class UserTypeKeyBoard {
	private String Date;
	private String Platform;
	private String ApplicationId;
	private String UserType;
	
	public UserTypeKeyBoard(String date, String platform, String applicationId, String userType) {
		super();
		Date = date;
		Platform = platform;
		ApplicationId = applicationId;
		UserType = userType;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public String getPlatform() {
		return Platform;
	}

	public void setPlatform(String platform) {
		Platform = platform;
	}

	public String getApplicationId() {
		return ApplicationId;
	}

	public void setApplicationId(String applicationId) {
		ApplicationId = applicationId;
	}

	public String getUserType() {
		return UserType;
	}

	public void setUserType(String userType) {
		UserType = userType;
	}
}

