package com.saveyra.analytics.specialRequest;

import java.util.Map;

public class AppIdReport {
	
	private Map<String,String> EventDetails;
	

	public Map<String, String> getEventDetails() {
		return EventDetails;
	}
	public void setEventDetails(Map<String, String> eventDetails) {
		EventDetails = eventDetails;
	}
	public AppIdReport(Map<String, String> eventDetails) {
		super();
		
		EventDetails = eventDetails;
	}
}
