package com.saveyra.analytics.DAO;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.saveyra.analytics.model.HomeScreenCard;
import com.saveyra.analytics.model.HomeScreenPermissionReport;
import com.saveyra.analytics.model.UsageData;
import com.saveyra.analytics.stickerShareHelper.StickerShareData;

public interface UsageDataDAO {
	
	void readExclusionAtStartup();
	
	// #######################  REPORT #########################

	long getOnboardingGenderViewedCountByDateUserLevel(String platform, Date startDate);
	long getOnboardingGenderCompletedCountByDateUserLevel(String platform, Date startDate);	
	long getOnboardingLanguageViewedCountByDateUserLevel(String platform, Date startDate);
	long getOnboardingLanguageCompletedCountByDateUserLevel(String platform, Date startDate);
	List<String> getUserTypeKeyboardCompleteCountByDateUserLevel(Date Date, String platform);
	List<String> findUserTypeAvatarCreatedByDate(String platform, Date Date);
	
	
	long getNewUserCountByDate(Date sDate ,String platform);
	
	long getTotalUserCountByDate(Date sDate ,String platform);
	
	long getSubCategoryCountByDate(Date sDate, String subCat,String platform);
	
	HashSet<String> getSubCategoryListBetweenDates(String platform,Date sDate,Date eDate);
	
	long getKeyboardInitCountByDateUserLevel(Date Date, String platform);
	
	long getKeyboardEnableCountByDateUserLevel(Date Date, String platform);
	
	long getKeyboardCompleteCountByDateUserLevel(Date Date, String platform);
	
	long getKeyboardSearchedCountByDateUserLevel(Date Date, String platform);
	
	long findAvatarCreatedByDateUnique(String platform, Date Date);
	
	long findAvatarCreatedByDate(String platform, Date Date);
	
	long findAvatarResetByDateUnique(String platform, Date Date);
	
	long findAvatarResetByDate(String platform, Date Date);
	
	long findAvatarInitByDateUserLevel(String platform,Date Date);
	
	long findAvatarInitByDate(String platform,Date Date);
	
	List<String> getAllImageIDsBydate(Date startDate);
	
	StickerShareData getImageIDCountViewed(String imageId,Date startDate,String platform,StickerShareData ssd);
	
	StickerShareData getImageIDCountShared(String imageId,Date startDate,String platform,StickerShareData ssd);
	
	long getImageIDCountDownloaded(String imageId,Date startDate,String platform);
	
	long getImageIDCountLiked(String imageId,Date startDate,String platform);
	
	long getImageIDCountUnLiked(String imageId,Date startDate,String platform);
	
	long getExtendedTargetAppShareCount(String tagetApp, String imageId,Date startDate, String platform);
	
	Map<String,Integer> getAllKeywordsByDate(Date dateStart,String platform);
	
	long getLanguageCount(String language,Date startDate,String platform);
	
	long getTargetAppShareCount(String tagetApp, Date startDate,String platform);
	
	long getMobileUserInitReportByDate(String platform, Date startDate);
	
	long getMobileUserResetOTPReportByDate(String platform, Date startDate);
	
	long getFBUserInitReportByDate(String platform, Date startDate);
	
	long getNotificationViewCountByDate(Date Date, String platform);
	
	Map<String, Integer> getSharedCountByImageID(Date startDate, Date endDate);

	Map<String, Integer> getViewedCountByImageId(Date startDate, Date endDate);
	
	long getTotalActiveUsersByDate(String platform, Date Date);
	
	long getTotalNewUsersByDate(String platform, Date startDate);
	
	long getWeeklyActiveUserCountByDate(Date startDate);
	
	long getMonthlyActiveUserCountByDate(Date startDate);
	
	
	//TEST
	
	Map<String,Long> getAvatarFeatureReportByDate(String platform, Date Date, String eventCode,String fetureId);
	
	List<String> getAllActiveUsersListBetweenDate(String platform, Date startDate,Date endDate);
	
	
	LinkedHashMap<String,List<String>> getAllImageIDsBydateAndAppId(Date startDate, String ApplicationId);
	
	
	long getUserWiseTargetAppShareCount(String AppId,String tagetApp, String imageId, Date startDate);
	
	
	long getUniqueSignupCountByDate(Date Date,String platform, String signupType);
	
	long findAvatarCreatedCountByDate(Date Date, String platform);
	
	
	/*Notification Start*/

	// APP FIRST BY APPID
	boolean isAppFirstOpen(String AppID , Date date);

	// GUEST USER
	boolean isGuestUser(String AppID);

	// KEYBOARD CONNECTED
	boolean isKeyboardConnected(String AppID);

	// AVATAR CREATED
	boolean isAvatarCreated(String AppID);
	
	/*Notification End*/

	/*Analytics Report START*/
	
	
	long findAvatarResetByDateUserLevel(String platform, Date Date);
	
	long findAvatarCreatedByDateUserLevel(String platform, Date Date);
	
	
	
	long getSignupCountByDate(Date Date,String platform, String signupType);
	
	long getKeyboardEnabledCountByDate(Date Date,String platform);
	
	long getKeyboardDisabledCountByDate(Date Date,String platform);
	
	long getKeyboardInitCountByDate(Date Date, String platform);
	
	long getKeyboardEnableCountByDate(Date Date, String platform);
	
	long getKeyboardCompleteCountByDate(Date Date, String platform);
	
	long getKeyboardSearchedCountByDate(Date Date, String platform);
	

	
	
	long getKeyboardEnabledCountTillDate(Date sDate, String platform);
	 
	long getKeyboardDisabledCountTillDate(Date sDate, String platform);
	
	
	
	

	
	
	
	long getUniqueSearchCountByDate(Date Date, String platform);
	
	long getUniqueSearchSharedCountByDate(Date Date, String platform);
	
	long getUniqueSearchViewCountByDate(Date Date, String platform);
	
	List<String> getAllTargetAppBydate(Date startDate, Date endDate);
	
	
	
	
	
	
	
	//List<String> getSubCategoryList(String Platform);
	
	HashSet<String> getSubCategoryList();
	
	
	
	String getUserActivityByEventAndDate(String AppID,String eventCode,Date date);
	
	Map<String,String> findAllEventDataForAppIdByDate(String appId,Date Date);
	
	
	//HashMap<String,Integer> getAllImageIDsBydateAndPlatform(Date startDate, String platform);
	
	
	
	List<String> getTargetAppListByDate(Date startDate, String platform);
	
	long getTargetShareCountByDate(Date startDate,String platform,String targetApp);
	
	List<String> findUniqueAppIdFirstOpenByDate(String platform,Date Date);
	
	List<String> findAllEventsForAppIdByDate(String appId,Date Date);
	
	List<String> findUniqueAppIdEachLaunchByDate(Date Date);
	
	//List<String> getAllKeywords();
	
	//Map<String,Integer> getAllKeywordsByDate(Date dateStart);
	
	//long getKeywordSearchCount(String keyword,String platform);
	
	long getKeywordSearchCountByDate(String keyword,String platform,Date startDate);
	
	
	
	
	
	long getFBUserCompleteReportByDate(String platform, Date startDate);
	
	
	long getMobileUserCompleteReportByDate(String platform, Date startDate);

	
	
	
/*Analytics Report End*/
	
	/*Graph Report Start*/
	
	long getTotalUniqueUsersGraphByDate(String platform, Date Date);
	
	long getTotalKeyboardEnableGraphByDate(String platform, Date startDate);
	
	long getTotalKeyboardDisableGraphByDate(String platform, Date startDate);
	
	long getTotalSharesByDate(String platform, Date startDate);
	
	
	
	
	long getTotalRegisteredUsersByDate(String platform,Date date);
	
	//List<String> getSubCategoryList(String platform,Date sDate);
	HashSet<String> getSubCategoryList(String platform,Date sDate);
	
	
	/*Graph Report End*/
	
	
	
	
	
	long getSubCategoryCountforAndroidByDate(Date sDate, String subCat);

	long getSubCategoryCountforiOSByDate(Date sDate, String subCat);

	long getSubCategoryCountforAllByDate(Date sDate, String subCat);
	
	
	
	
	
	List<String> getSubCategoryListAndroid();

	List<String> getSubCategoryListiOS();

	
	
	long getOnboardingCountByDate(String platform, Date Date);
	
	long getSkippedCountByDate(String platform, Date Date);
	

	// UNIQUE USERS
	long getTotalUniqueUsers(String platform); // Android,iOS,All

	

	long getTotalUniqueUsersBeforeDate(String platform, String Date);

	long getTotalUniqueUsersBetweenDates(String platform, String sDate, String eDate);

	// UNREGISTERED USERS
	long getTotalUnregisteredUsers(String platform); // Android,iOS,All

	long getTotalUnregisteredUsersByDate(String platform, String sDate); // Android,iOS,All

	long getTotalUnregisteredBeforeDate(String platform, String Date);

	long getTotalUnregisteredBetweenDates(String platform, String sDate, String eDate);

	// REGISTERED USERS
	long getTotalRegisteredUsers(String platform); // Android,iOS,All

	//long getTotalRegisteredUsersByDate(String platform, String sDate); // Android,iOS,All

	long getTotalRegisteredUsersBeforeDate(String platform, String Date);

	long getTotalRegisteredUsersBetweenDates(String platform, String sDate, String eDate);

	// KEYBOARD ENABLE
	long getTotalKeyboardEnable(String platform);// Android,iOS,All

	long getTotalKeyboardEnableByDate(String platform, String Date);

	long getTotalKeyboardEnableBeforeDate(String platform, String Date);

	long getTotalKeyboardEnableBetweenDates(String platform, String sDate, String eDate);

	// AVATAR CREATED
	long getTotalAvatarCreated(String platform);// Android,iOS,All

	

	long getTotalAvatarCreatedBeforeDate(String platform, String Date);

	long getTotalAvatarCreatedBetweenDates(String platform, String sDate, String eDate);

	// TOTAL SHARES
	long getTotalShares(String platform);// Android,iOS,All

	//long getTotalSharesByDate(String platform, String Date);

	long getTotalSharesBeforeDate(String platform, String Date);

	long getTotalSharesBetweenDates(String platform, String sDate, String eDate);

	// ACTIVE USERS
	long getTotalActiveUsers(String platform);// Android,iOS,All

	

	long getTotalActiveUsersBeforeDate(String platform, String Date);

	long getTotalActiveUsersBetweenDates(String platform, String sDate, String eDate);

	// REGISTERED USERS
	long getTotalRegisteredUsers(String platform, String medium); // Android,iOS,All

	//long getTotalRegisteredUsersByDate(String platform, String medium, String Date);

	long getTotalRegisteredUsersBeforeDate(String platform, String medium, String Date);

	long getTotalRegisteredUsersBetweenDates(String platform, String medium, String sDate, String eDate);

	// SIGNUP COUNT
	long getTotalSignupCount(String platform, String medium); // Android,iOS,All

	long getTotalSignupCountByDate(String platform, String medium, String Date);

	long getTotalSignupCountBeforeDate(String platform, String medium, String Date);

	long getTotalSignupCountBetweenDates(String platform, String medium, String sDate, String eDate);

	long getAndroidSignupCountForFB(String sDate, String eDate);

	long getiOSSignupCountForFB(String sDate, String eDate);

	long getAndroidSignupCountForMobile(String sDate, String eDate);

	long getiOSSignupCountForMobile(String sDate, String eDate);

	long getTotalAndroidSignupCountForFB();

	long getTotaliOSSignupCountForFB();

	long getTotalAndroidSignupCountForMobile();

	long getTotaliOSSignupCountForMobile();

	long getAndroidSignupCountForFBAfterDate(String sDate);

	long getiOSSignupCountForFBAfterDate(String sDate);

	long getAndroidSignupCountForMobileAfterDate(String sDate);

	long getiOSSignupCountForMobileAfterDate(String sDate);

	long getAndroidSignupCountForFBBeforeDate(String eDate);

	long getiOSSignupCountForFBBeforeDate(String eDate);

	long getAndroidSignupCountForMobileBeforeDate(String eDate);

	long getiOSSignupCountForMobileBeforeDate(String eDate);

	//long getAndroidSignupCountForFBByDate(String sDate);

	long getiOSSignupCountForFBByDate(String sDate);

	long getAndroidSignupCountForMobileByDate(String sDate);

	long getiOSSignupCountForMobileByDate(String sDate);

	long getiOSAndAndroidCountForFBByDate(String Date);

	long getiOSAndAndroidCountForMobByDate(String Date);

	long getAndroidKeyboardEnabledCountByDate(String sDate);

	long getAndroidKeyboardDisabledCountByDate(String sDate);

	long getiOSKeyboardEnabledCountByDate(String sDate);

	long getiOSKeyboardDisabledCountByDate(String sDate);

	long getiOSAndAndroidKeyboardEnabledCountByDate(String Date);

	long getiOSAndAndroidKeyboardDisabledCountByDate(String sDate);

	long getAllUniqueUsers();

	long getAllUniqueUsersAndroid();

	long getAllUniqueUsersiOS();

	long getAllUniqueUsersTilldate(String sDate);

	long getAllUniqueUsersAndroidTillDate(String sDate);

	long getAllUniqueUsersiOSTillDate(String sDate);

	String saveUsageData(UsageData usageData);

	String getAllData(String platform);

	String getAllData(String sPlatform, Integer iEvent);

	String getAllData();

	long getTotaliOSAndAndroidKeyboardEnabledCount();

	long getTotaliOSAndAndroidKeyboardDisabledCount();

	long getTotaliOSAndAndroidKeyboardEnabledCountTillDate(String Date);

	long getTotaliOSAndAndroidKeyboardDisabledCountTillDate(String Date);

	long getTotaliOSKeyboardDisabledCount();

	long getTotalAndroidKeyboardDisabledCount();

	long getTotaliOSKeyboardEnabledCount();

	long getTotalAndroidKeyboardEnabledCount();

	long getTotaliOSKeyboardDisabledCountTillDate(String sDAte);

	long getTotalAndroidKeyboardDisabledCountTillDate(String sDAte);

	long getTotaliOSKeyboardEnabledCountTillDate(String sDate);

	long getTotalAndroidKeyboardEnabledCountTillDate(String sDAte);
	

	
	
	

	

	long getNewUserCountforAndroidByDate(Date sDate);

	long getNewUserCountforiOSByDate(Date sDate);

	long getNewUserCountforAllByDate(Date sDate);

	long getReturningUserCountforAndroidByDate(Date sDate);

	long getReturningUserCountforiOSByDate(Date sDate);

	long getReturningUserCountforAllByDate(Date sDate);

	long getSubCategoryCountforAndroid(String subCat);

	long getSubCategoryCountforiOS(String subCat);

	long getSubCategoryCountforAll(String subCat);
	
	
	
	List<UsageData> getBetweenDate1(Date startDate, Date endDate);
	long getBetweenDate1Count(Date startDate, Date endDate);
	
	List<UsageData> getByDate2(Date startDate);
	long getByDate2Count(Date startDate);
	
	List<UsageData> getByDateAndEvent1(Date startDate, Date endDate);
	long getByDateAndEvent1Count(Date startDate, Date endDate);
	
	List<UsageData> getByDateAndEvent2(Date startDate);
	long getByDateAndEvent2Count(Date startDate);



	//test
	String findAllAppIDWithMinCount();









	
	
	
	//ScreenView Count :
	long getHomeScreenViewedCountByDate(String platform, Date startDate);//119
	long getAvatarScreenViewedCountByDate(String platform, Date startDate);//120
	long getStickerScreenViewedCountByDate(String platform, Date startDate);//121
	long getSettingsScreenViewedCountByDate(String platform, Date startDate);//122
	long getSearchScreenViewedCountByDate(String platform, Date startDate);//123
	long getOnboardingScreenViewedCountByDate(String platform, Date startDate);//124
	long getNotificationSettingScreenViewedCountByDate(String platform, Date startDate);//128
	long getLanguagePreferenceScreenViewedCountByDate(String platform, Date startDate);//131
	long getSignUpScreenViewedCountByDate(String platform, Date startDate);//132
	long getMobileSignUpScreenViewedCountByDate(String platform, Date startDate);//133
	long getLoginScreenViewedCountByDate(String platform, Date startDate);//134
	long getLoginMobileScreenViewedCountByDate(String platform, Date startDate);//135
	long getHelpCentreScreenViewedCountByDate(String platform, Date startDate);//136
	long getReportAProblemScreenViewedCountByDate(String platform, Date startDate);//137
	long getContactUsScreenViewedCountByDate(String platform, Date startDate);//139
	long getLegalInfoScreenViewedCountByDate(String platform, Date startDate);//141	
	
	long getHomeScreenViewedCountByDateUserLevel(String platform, Date startDate);//119
	long getAvatarScreenViewedCountByDateUserLevel(String platform, Date startDate);//120
	long getStickerScreenViewedCountByDateUserLevel(String platform, Date startDate);//121
	long getSettingsScreenViewedCountByDateUserLevel(String platform, Date startDate);//122
	long getSearchScreenViewedCountByDateUserLevel(String platform, Date startDate);//123
	long getOnboardingScreenViewedCountByDateUserLevel(String platform, Date startDate);//124
	long getNotificationSettingScreenViewedCountByDateUserLevel(String platform, Date startDate);//128
	long getLanguagePreferenceScreenViewedCountByDateUserLevel(String platform, Date startDate);//131
	long getSignUpScreenViewedCountByDateUserLevel(String platform, Date startDate);//132
	long getMobileSignUpScreenViewedCountByDateUserLevel(String platform, Date startDate);//133
	long getLoginScreenViewedCountByDateUserLevel(String platform, Date startDate);//134
	long getLoginMobileScreenViewedCountByDateUserLevel(String platform, Date startDate);//135
	long getHelpCentreScreenViewedCountByDateUserLevel(String platform, Date startDate);//136
	long getReportAProblemScreenViewedCountByDateUserLevel(String platform, Date startDate);//137
	long getContactUsScreenViewedCountByDateUserLevel(String platform, Date startDate);//139
	long getLegalInfoScreenViewedCountByDateUserLevel(String platform, Date startDate);//141

	Map<String, Integer> getNotificationReadCountByNid(Date startDate, String platform);

	Map<String, Integer> getNotificationReadCountByNidNoExclusion(Date startDate, String platform);

	long getImageIDCountSharedByDate(String imageId, Date startDate);

	Map<String, Integer> getHomeCardViewCountUnique(Date sDate, String platform);

	List<HomeScreenCard> getHomeCardShareDetails(String cardId, String platform);
	
	List<HomeScreenPermissionReport> getPermissionReport(Date sDate, String platform);

	long getPermissionByDate(Date sDate, String platform, String screen, String status, String permissionEvent);
	

	long getReminderByDate(Date sDate, String platform, String screen, String permissionEvent);

	boolean isNotificationViewed(String applicationId, String n_id);

	Map<String, Integer> getHomeCardTypeViewCountUnique(Date sDate, String platform);

	long getHomeCardTypeShareCountUnique(Date sDate, String platform ,String cardId);

	ArrayList<String> getTotalActiveAppIDByDate(String platform, Date startDate);

	long getOnboardingCompletedCountByDateUserLevel(String platform, Date startDate);

	long getOnboardingViewedCountByDateUserLevel(String platform, Date startDate);

	ArrayList<String> getTotalActiveAppIDBetweenDate(String platform, Date startDate, Date endDate);

	long getReadNotificationCountByNid(String n_id);	
}
