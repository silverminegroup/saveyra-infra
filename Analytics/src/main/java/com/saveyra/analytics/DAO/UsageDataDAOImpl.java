package com.saveyra.analytics.DAO;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.saveyra.analytics.model.ExclusionModel;
import com.saveyra.analytics.model.HomeScreenCard;
import com.saveyra.analytics.model.HomeScreenPermissionReport;
import com.saveyra.analytics.model.UsageData;
import com.saveyra.analytics.stickerShareHelper.StickerShareData;
import com.saveyra.analytics.utility.Declaration;

@Repository("UsageDataDAO")
public class UsageDataDAOImpl implements UsageDataDAO {

	private static final Logger logger = LoggerFactory.getLogger(UsageDataDAOImpl.class);
	private final MongoTemplate mongoTemplate;

	@Autowired
	public UsageDataDAOImpl(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}
	
	@Override
	public boolean isNotificationViewed(String appId,String n_id) {		
		//System.out.println("Request : "+appId+"   "+n_id);
		Query query = new Query();		
		query.addCriteria(Criteria.where("eventCode").is("85"));
		query.addCriteria(Criteria.where("data.notificationid").is(n_id));
		query.addCriteria(Criteria.where("applicationID").is(appId));		
		long count =  mongoTemplate.count(query, UsageData.class);		
		
		if(count >0) {
			return true;
		}
		
		return false;
	}
	
	
	

	@Override
	public void readExclusionAtStartup() {
		try {
			Query query = new Query();
			List<ExclusionModel> exclusionList = mongoTemplate.find(query, ExclusionModel.class, "exclusion");
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < exclusionList.size(); i++) {
				Declaration.getExclusionList().add(exclusionList.get(i).getApplicationID());

				sb.append("'" + exclusionList.get(i).getApplicationID() + "'");
				if (i < exclusionList.size() - 1) {
					sb.append(",");
				}
			}
			Declaration.setExclusion_list(sb.toString());
		} catch (IllegalStateException exception) {
			logger.error("[ERROR] Read Exception");
			exception.printStackTrace();
		} catch (Throwable throwable) {
			logger.error("[ERROR] Read Exception");
		}
	}

	

	// ############################### REPORT START #############################
	/*
	 * 12
	 * 
	 * List<Bson> filters = new ArrayList<>(); filters.add(Filters.eq("eventCode",
	 * "93")); filters.add(Filters.eq("eventCode", "112"));
	 * filters.add(Filters.eq("eventCode", "113"));
	 * filters.add(Filters.eq("eventCode", "114"));
	 * filters.add(Filters.eq("eventCode", "117")); if (platform.equals("Android"))
	 * { cursor = mongoTemplate.getCollection("messageBroker")
	 * .aggregate(Arrays.asList( Aggregates.match(Filters.eq("platform",
	 * "Android")), Aggregates.match(Filters.or(filters)),
	 */
	
	@Override
	public long getReminderByDate(Date sDate, String platform,String screen,String reminderEvent) {
		//System.out.println("REQUEST : "+sDate+" - "+  platform+" - "+ screen+" - "+ status+" - "+ permissionEvent);
		Query query = new Query();
		//long Count=0;
		if (platform.equals("Android")) {
			query.addCriteria(Criteria.where("eventCode").is(reminderEvent));
			query.addCriteria(Criteria.where("platform").is(platform));
			query.addCriteria(Criteria.where("data.screen").is(screen));
			query.addCriteria(Criteria.where("dateTime").gte(sDate).lt(getNextDateObject(sDate)));
			query.addCriteria(Criteria.where("applicationID").nin(Declaration.getExclusionList()));
		} else if (platform.equals("iOS")) {
			query.addCriteria(Criteria.where("eventCode").is(reminderEvent));
			query.addCriteria(Criteria.where("platform").is(platform));
			query.addCriteria(Criteria.where("data.screen").is(screen));
			query.addCriteria(Criteria.where("dateTime").gte(sDate).lt(getNextDateObject(sDate)));
			query.addCriteria(Criteria.where("applicationID").nin(Declaration.getExclusionList()));

		} else if (platform.equals("All")) {
				query.addCriteria(Criteria.where("eventCode").is(reminderEvent));
				query.addCriteria(Criteria.where("data.screen").is(screen));
				query.addCriteria(Criteria.where("dateTime").gte(sDate).lt(getNextDateObject(sDate)));
				query.addCriteria(Criteria.where("applicationID").nin(Declaration.getExclusionList()));
						
		}		
		
		System.out.println("query "+query.toString());
		return mongoTemplate.count(query, UsageData.class);		
	}

	@Override
	public long getPermissionByDate(Date sDate, String platform,String screen,String status,String permissionEvent) {
		System.out.println("REQUEST : "+sDate+" - "+  platform+" - "+ screen+" - "+ status+" - "+ permissionEvent);
		Query query = new Query();
		//long Count=0;
		if (platform.equals("Android")) {
			query.addCriteria(Criteria.where("eventCode").is(permissionEvent));
			query.addCriteria(Criteria.where("platform").is(platform));
			query.addCriteria(Criteria.where("data.screen").is(screen));
			query.addCriteria(Criteria.where("data.status").is(status));
			query.addCriteria(Criteria.where("dateTime").gte(sDate).lt(getNextDateObject(sDate)));
			query.addCriteria(Criteria.where("applicationID").nin(Declaration.getExclusionList()));
		} else if (platform.equals("iOS")) {
			query.addCriteria(Criteria.where("eventCode").is(permissionEvent));
			query.addCriteria(Criteria.where("platform").is(platform));
			query.addCriteria(Criteria.where("data.screen").is(screen));
			query.addCriteria(Criteria.where("data.status").is(status));
			query.addCriteria(Criteria.where("dateTime").gte(sDate).lt(getNextDateObject(sDate)));
			query.addCriteria(Criteria.where("applicationID").nin(Declaration.getExclusionList()));

		} else if (platform.equals("All")) {
				query.addCriteria(Criteria.where("eventCode").is(permissionEvent));
				query.addCriteria(Criteria.where("data.screen").is(screen));
				query.addCriteria(Criteria.where("data.status").is(status));
				query.addCriteria(Criteria.where("dateTime").gte(sDate).lt(getNextDateObject(sDate)));
				query.addCriteria(Criteria.where("applicationID").nin(Declaration.getExclusionList()));						
		}		
		System.out.println("query "+query.toString());
		
		return mongoTemplate.count(query, UsageData.class);		
	}
	
	@Override
	public List<HomeScreenPermissionReport> getPermissionReport(Date sDate, String platform) {
		MongoCursor<Document> cursor = null;
		List<HomeScreenPermissionReport> hspr = new ArrayList<HomeScreenPermissionReport>();
		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("eventCode", "80"));
		filters.add(Filters.eq("eventCode", "79"));
		filters.add(Filters.eq("eventCode", "78"));
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", sDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(sDate))),
							Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", sDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(sDate))),
							Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();

		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", sDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(sDate))),
							Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}

		/*
		 * Integer NotificationEnableCount = 0; Integer MediaEnableCount = 0; Integer
		 * LocationEnableCount = 0; Integer NotificationDisableCount = 0; Integer
		 * MediaDisableCount = 0; Integer LocationDisableCount = 0;
		 */

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				String dataString = d.get("data").toString();
				if (dataString != null && !dataString.equals("")) {
					String[] contentArray = dataString.split("\\{\\{")[1].split("\\}\\}");
					String[] keyValpair = contentArray[0].split(",");
					Map<String, String> keyValMap = new HashMap<String, String>();
					for (String s : keyValpair) {
						String[] tempAr = s.split("=");
						if (tempAr.length == 2) {
							keyValMap.put(tempAr[0].trim(), tempAr[1].trim());
						} else {
							keyValMap.put(tempAr[0].trim(), "");
						}
					}

					String type = "";
					if (Integer.parseInt(d.get("eventCode").toString()) == 80) {
						type = "Notification";
						if (keyValMap.get("status").equalsIgnoreCase("false")) {
							hspr.add(new HomeScreenPermissionReport("", platform, type, "", keyValMap.get("status"),
									keyValMap.get("screen")));
						} else {
							hspr.add(new HomeScreenPermissionReport("", platform, type, keyValMap.get("status"), "",
									keyValMap.get("screen")));
						}
					} else if (Integer.parseInt(d.get("eventCode").toString()) == 79) {
						type = "Media";
						if (keyValMap.get("status").equalsIgnoreCase("false")) {
							hspr.add(new HomeScreenPermissionReport("", platform, type, "", keyValMap.get("status"),
									keyValMap.get("screen")));
						} else {
							hspr.add(new HomeScreenPermissionReport("", platform, type, keyValMap.get("status"), "",
									keyValMap.get("screen")));
						}
					} else if (Integer.parseInt(d.get("eventCode").toString()) == 78) {
						type = "Location";
						if (keyValMap.get("status").equalsIgnoreCase("false")) {
							hspr.add(new HomeScreenPermissionReport("", platform, type, "", keyValMap.get("status"),
									keyValMap.get("screen")));
						} else {
							hspr.add(new HomeScreenPermissionReport("", platform, type, keyValMap.get("status"), "",
									keyValMap.get("screen")));
						}
					}
				}
			}
		}
		// System.out.println(NotificationEnableCount+" "+MediaEnableCount+"
		// "+LocationEnableCount+" "+NotificationDisableCount+" "+MediaDisableCount+"
		// "+LocationDisableCount);

		return hspr;
	}
	
	@Override
	public Map<String, Integer> getHomeCardTypeViewCountUnique(Date sDate, String platform) {
		// long Count = 0;
		Map<String, Integer> retMap = new HashMap<String, Integer>();
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", sDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(sDate))),
							Aggregates.match(Filters.eq("eventCode", "151")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$data.cardType", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", sDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(sDate))),
							Aggregates.match(Filters.eq("eventCode", "151")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$data.cardType", Accumulators.sum("count", 1))))
					.iterator();

		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", sDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(sDate))),
							Aggregates.match(Filters.eq("eventCode", "151")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$data.cardType", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				if(d.get("_id")!=null&&d.get("count")!=null) {
					retMap.put(d.get("_id").toString(), Integer.parseInt(d.get("count").toString()));
				}
				System.out.println(d.get("_id").toString()+" "+d.get("count").toString());
				
				// Count++;
			}
		}
		return retMap;
	}
	
	
	@Override
	public long getHomeCardTypeShareCountUnique(Date sDate, String platform, String cardId) {
		System.out.println(">>>>>>   "+cardId);
		long Count = 0;		
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", sDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(sDate))),
							Aggregates.match(Filters.eq("eventCode", "152")),
							Aggregates.match(Filters.eq("data.cardType", cardId)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", sDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(sDate))),
							Aggregates.match(Filters.eq("eventCode", "152")),
							Aggregates.match(Filters.eq("data.cardType", cardId)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();

		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", sDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(sDate))),
							Aggregates.match(Filters.eq("eventCode", "152")),
							Aggregates.match(Filters.eq("data.cardType", cardId)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}
	


	@Override
	public Map<String, Integer> getHomeCardViewCountUnique(Date sDate, String platform) {
		// long Count = 0;
		Map<String, Integer> retMap = new HashMap<String, Integer>();
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", sDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(sDate))),
							Aggregates.match(Filters.eq("eventCode", "151")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$data.cardId", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", sDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(sDate))),
							Aggregates.match(Filters.eq("eventCode", "151")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$data.cardId", Accumulators.sum("count", 1))))
					.iterator();

		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", sDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(sDate))),
							Aggregates.match(Filters.eq("eventCode", "151")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$data.cardId", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				if(d.get("_id")!=null&&d.get("count")!=null) {
					retMap.put(d.get("_id").toString(), Integer.parseInt(d.get("count").toString()));
				}
				System.out.println(d.get("_id").toString()+" "+d.get("count").toString());
				
				// Count++;
			}
		}
		return retMap;
	}

	@Override
	public List<HomeScreenCard> getHomeCardShareDetails(String cardId, String platform) {
		// long Count = 0;
		MongoCursor<Document> cursor = null;

		List<HomeScreenCard> homeList = new ArrayList<HomeScreenCard>();
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.eq("eventCode", "152")),
							Aggregates.match(Filters.eq("data.cardId", cardId)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.eq("eventCode", "152")),
							Aggregates.match(Filters.eq("data.cardId", cardId)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();

		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "152")),
							Aggregates.match(Filters.eq("data.cardId", cardId)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))

					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				String dataString = d.get("data").toString();
				System.out.println(dataString);
				// Document{{cardId=12312312, cardType=Permission, cardName=Notififcation,
				// status=deny, orderNumber=3, targetApplication=whatsapp,
				// selectedOptionValue=jabba, selectedOptionIndex=1}}
				if (dataString != null && !dataString.equals("")) {
					String[] contentArray = dataString.split("\\{\\{")[1].split("\\}\\}");
					String[] keyValpair = contentArray[0].split(",");
					Map<String, String> keyValMap = new HashMap<String, String>();
					for (String s : keyValpair) {
						String[] tempAr = s.split("=");
						if (tempAr.length == 2) {
							keyValMap.put(tempAr[0].trim(), tempAr[1].trim());
						} else {
							keyValMap.put(tempAr[0].trim(), "");
						}

					}
					// System.out.println( platform+" "+ keyValMap.get("cardType")+" "+
					// keyValMap.get("orderNumber")+" "+ keyValMap.get("targetApplication"));
					// "cardId":"999111","cardType":"mcq","cardName":"mcq","status":"",
					// "orderNumber":"2", "targetApplication":""
					// ,"selectedOptionValue":"csd","selectedOptionIndex":"3","result":"wrong","screen":"home"
					System.out.println("FOUND : " + keyValMap.get("cardType") + "  " + keyValMap.get("orderNumber")
							+ "  " + keyValMap.get("targetApplication") + " -- " + keyValMap.get("selectedOptionIndex")
							+ " -- " + keyValMap.get("result"));
					homeList.add(new HomeScreenCard("", platform, keyValMap.get("cardType"), "", "",
							keyValMap.get("orderNumber"), keyValMap.get("targetApplication"),
							keyValMap.get("selectedOptionIndex"), keyValMap.get("result")));
				}
				// Count++;
			}
		}
		return homeList;
	}

	@Override
	public long getNewUserCountByDate(Date sDate, String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.eq("eventCode", "9")),
							Aggregates.match(Filters.gte("dateTime", sDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(sDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();

		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.eq("eventCode", "9")),
							Aggregates.match(Filters.gte("dateTime", sDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(sDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();

		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "9")),
							Aggregates.match(Filters.gte("dateTime", sDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(sDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();

		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getTotalUserCountByDate(Date sDate, String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", sDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(sDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", sDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(sDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();

		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", sDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(sDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getSubCategoryCountByDate(Date sDate, String subCat, String platform) {
		Query query = new Query();
		if (platform.equals("Android")) {
			query.addCriteria(Criteria.where("eventCode").is("7"));
			query.addCriteria(Criteria.where("platform").is("Android"));
			query.addCriteria(Criteria.where("data.subcategory").is(subCat));
			query.addCriteria(Criteria.where("dateTime").gte(sDate).lt(getNextDateObject(sDate)));
			query.addCriteria(Criteria.where("applicationID").nin(Declaration.getExclusionList()));
		} else if (platform.equals("iOS")) {
			query.addCriteria(Criteria.where("eventCode").is("7"));
			query.addCriteria(Criteria.where("platform").is("iOS"));
			query.addCriteria(Criteria.where("data.subcategory").is(subCat));
			query.addCriteria(Criteria.where("dateTime").gte(sDate).lt(getNextDateObject(sDate)));
			query.addCriteria(Criteria.where("applicationID").nin(Declaration.getExclusionList()));
		} else if (platform.equals("All")) {
			query.addCriteria(Criteria.where("eventCode").is("7"));
			query.addCriteria(Criteria.where("data.subcategory").is(subCat));
			query.addCriteria(Criteria.where("dateTime").gte(sDate).lt(getNextDateObject(sDate)));
			query.addCriteria(Criteria.where("applicationID").nin(Declaration.getExclusionList()));
		}
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public HashSet<String> getSubCategoryListBetweenDates(String platform, Date sDate, Date eDate) {
		HashSet<String> catList = new HashSet<String>();
		Query query = new Query();
		if (platform.equals("Android")) {
			query.addCriteria(Criteria.where("eventCode").is("7"));
			query.addCriteria(Criteria.where("platform").is("Android"));
			query.addCriteria(Criteria.where("dateTime").gte(sDate).lt(getNextDateObject(eDate)));
			query.addCriteria(Criteria.where("data.subcategory").exists(true));
			query.addCriteria(Criteria.where("applicationID").nin(Declaration.getExclusionList()));
		} else if (platform.equals("iOS")) {
			query.addCriteria(Criteria.where("eventCode").is("7"));
			query.addCriteria(Criteria.where("platform").is("iOS"));
			query.addCriteria(Criteria.where("dateTime").gte(sDate).lt(getNextDateObject(eDate)));
			query.addCriteria(Criteria.where("data.subcategory").exists(true));
			query.addCriteria(Criteria.where("applicationID").nin(Declaration.getExclusionList()));
		} else if (platform.equals("All")) {
			query.addCriteria(Criteria.where("eventCode").is("7"));
			query.addCriteria(Criteria.where("dateTime").gte(sDate).lt(getNextDateObject(eDate)));
			query.addCriteria(Criteria.where("data.subcategory").exists(true));
			query.addCriteria(Criteria.where("applicationID").nin(Declaration.getExclusionList()));
		}
		List<UsageData> tempList = mongoTemplate.find(query, UsageData.class);
		for (UsageData ud : tempList) {
			Map<String, String> mTemp = ud.getData();
			catList.add(mTemp.get("subcategory"));
		}
		return catList;
	}

	@Override
	public long getKeyboardInitCountByDateUserLevel(Date Date, String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "15")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "15")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "15")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getKeyboardCompleteCountByDateUserLevel(Date Date, String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "17")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "17")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "17")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getKeyboardEnableCountByDateUserLevel(Date Date, String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "16")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "16")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "16")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getKeyboardSearchedCountByDateUserLevel(Date Date, String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "17")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "17")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "17")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long findAvatarCreatedByDate(String platform, Date Date) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "43")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "43")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "43")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null || d.get("_id") == null || d.get("_id").equals("")) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long findAvatarCreatedByDateUnique(String platform, Date Date) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "43")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "43")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "43")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null || d.get("_id") == null || d.get("_id").equals("")) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long findAvatarResetByDate(String platform, Date Date) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "41")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "41")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "41")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))// ,
					)).iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null || d.get("_id") == null || d.get("_id").equals("")) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long findAvatarResetByDateUnique(String platform, Date Date) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "41")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "41")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "41")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null || d.get("_id") == null || d.get("_id").equals("")) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long findAvatarInitByDate(String platform, Date Date) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "42")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "42")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "42")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null || d.get("_id") == null || d.get("_id").equals("")) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long findAvatarInitByDateUserLevel(String platform, Date Date) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "42")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "42")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "42")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null || d.get("_id") == null || d.get("_id").equals("")) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getImageIDCountDownloaded(String imageId, Date startDate, String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("eventCode", "94"));
		filters.add(Filters.eq("eventCode", "95"));
		filters.add(Filters.eq("eventCode", "96"));
		filters.add(Filters.eq("eventCode", "104"));
		filters.add(Filters.eq("eventCode", "109"));
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.eq("data.imageId", imageId)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.eq("data.imageId", imageId)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.eq("data.imageId", imageId)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getImageIDCountLiked(String imageId, Date startDate, String platform) {

		long Count = 0;
		MongoCursor<Document> cursor = null;
		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("eventCode", "92"));
		filters.add(Filters.eq("eventCode", "105"));
		filters.add(Filters.eq("eventCode", "111"));
		filters.add(Filters.eq("eventCode", "110"));
		filters.add(Filters.eq("eventCode", "116"));
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.eq("data.imageId", imageId)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.eq("data.imageId", imageId)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.eq("data.imageId", imageId)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;

	}

	@Override
	public long getImageIDCountUnLiked(String imageId, Date startDate, String platform) {

		long Count = 0;
		MongoCursor<Document> cursor = null;
		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("eventCode", "93"));
		filters.add(Filters.eq("eventCode", "112"));
		filters.add(Filters.eq("eventCode", "113"));
		filters.add(Filters.eq("eventCode", "114"));
		filters.add(Filters.eq("eventCode", "117"));
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.eq("data.imageId", imageId)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.eq("data.imageId", imageId)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.eq("data.imageId", imageId)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;

	}

	public StickerShareData getImageIDCountViewed(String imageId, Date startDate, String platform,
			StickerShareData ssd) {
		long Count = 0;
		String dataString = null;
		String catagoryString = null;
		String subCatagoryString = null;
		String languageString = "";
		
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "6")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.eq("data.imageId", imageId)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "6")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.eq("data.imageId", imageId)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "6")),
							Aggregates.match(Filters.eq("data.imageId", imageId)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				if (dataString == null) {
					dataString = d.get("data").toString();
					//logger.info(dataString);
					String[] contentArray = dataString.split("\\{\\{")[1].split("\\}\\}");
					String[] keyValpair = contentArray[0].split(",");
					
					for(String s: keyValpair) {
						if (s.split("=") != null && s.split("=").length > 1) {							
							if(s.split("=")[0].trim().equalsIgnoreCase("category")) {
								catagoryString = s.split("=")[1];
							}
							
							if(s.split("=")[0].trim().equalsIgnoreCase("subcategory")) {
								subCatagoryString = s.split("=")[1];
							}
							
							if(s.split("=")[0].trim().equalsIgnoreCase("languagepreference")) {
								languageString = s.split("=")[1];
								//logger.info("Update Language 1>> "+languageString);
							}					
						}						
					}
					
					
					if(catagoryString==null) {
						catagoryString = "NA";
					}
					
					if(subCatagoryString==null) {
						subCatagoryString = "NA";
					}
					
					if(languageString==null || languageString.equals("")) {
						languageString = "NA";
					}
					
					/*if (keyValpair[0].split("=") != null && keyValpair[0].split("=").length > 1) {
						catagoryString = keyValpair[0].split("=")[0].trim().equalsIgnoreCase("category")
								? keyValpair[0].split("=")[1]
								: "N/A";						
					} else {
						catagoryString = "N/A";						
					}
					
					if (keyValpair[1].split("=") != null && keyValpair[1].split("=").length > 1) {
						subCatagoryString = keyValpair[1].split("=")[0].trim().equalsIgnoreCase("subcategory")
								? keyValpair[1].split("=")[1]
								: "N/A";
					} else {						
						subCatagoryString = "N/A";
					}*/
				}
				Count++;
			}
		}
		if (catagoryString != null && (!catagoryString.equals(""))) {
			ssd.setCategory(catagoryString);
			ssd.setSubCategory(subCatagoryString);
			ssd.setLanguage(languageString);
		}
		ssd.setViewCount(Count);
		ssd.setType("StandAlone");
		return ssd;
	}

	public StickerShareData getImageIDCountShared(String imageId, Date startDate, String platform,
			StickerShareData ssd) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		String dataString = null;
		String eventCode = null;
		String catagoryString = null;
		String subCatagoryString = null;
		String languageString = "";

		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("eventCode", "4"));
		filters.add(Filters.eq("eventCode", "5"));
		filters.add(Filters.eq("eventCode", "7"));
		filters.add(Filters.eq("eventCode", "38"));
		filters.add(Filters.eq("eventCode", "76"));
		filters.add(Filters.eq("eventCode", "102"));

		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "Android")), Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.eq("data.imageId", imageId)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "iOS")), Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.eq("data.imageId", imageId)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.eq("data.imageId", imageId)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				if (dataString == null) {
					eventCode = d.get("eventCode").toString();
					dataString = d.get("data").toString();
					//logger.info(dataString);
					String[] contentArray = dataString.split("\\{\\{")[1].split("\\}\\}");
					String[] keyValpair = contentArray[0].split(",");
					for(String s: keyValpair) {
						if (s.split("=") != null && s.split("=").length > 1) {							
							if(s.split("=")[0].trim().equalsIgnoreCase("category")) {
								catagoryString = s.split("=")[1];
							}
							
							if(s.split("=")[0].trim().equalsIgnoreCase("subcategory")) {
								subCatagoryString = s.split("=")[1];
							}
							
							if(s.split("=")[0].trim().equalsIgnoreCase("languagepreference")) {
								//logger.info("Update Language 2>> "+languageString);
								languageString = s.split("=")[1];
							}					
						}						
					}
					
					
					if(catagoryString==null) {
						catagoryString = "NA";
					}
					
					if(subCatagoryString==null) {
						subCatagoryString = "NA";
					}
					
					if((ssd.getLanguage() == null || ssd.getLanguage().equals(""))&& languageString==null) {
						languageString = "NA";
					}
					
					if (eventCode.equals("4") || eventCode.equals("5")) {
						ssd.setType("Keyboard");
					} else if (eventCode.equals("7")) {
						ssd.setType("StandAlone");
					} else if (eventCode.equals("38")) {
						ssd.setType("Recommendation");
					} else if (eventCode.equals("76")) {
						ssd.setType("Searched");
					} else if (eventCode.equals("102")) {
						ssd.setType("Promotion");
					}
				}
				Count++;
			}
		}
		if (catagoryString != null && (!catagoryString.equals(""))) {
			ssd.setCategory(catagoryString);
			ssd.setSubCategory(subCatagoryString);
			ssd.setLanguage(languageString);
		}
		ssd.setShareCount(Count);
		return ssd;
	}

	@Override
	public long getImageIDCountSharedByDate(String imageId, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("eventCode", "4"));
		filters.add(Filters.eq("eventCode", "5"));
		filters.add(Filters.eq("eventCode", "7"));
		filters.add(Filters.eq("eventCode", "38"));
		filters.add(Filters.eq("eventCode", "76"));
		filters.add(Filters.eq("eventCode", "102"));

		cursor = mongoTemplate.getCollection("messageBroker")
				.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
						Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
						Aggregates.match(Filters.or(filters)), Aggregates.match(Filters.eq("data.imageId", imageId)),
						Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
				.iterator();

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public List<String> getAllImageIDsBydate(Date startDate) {
		List<String> imageList = new ArrayList<String>();
		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("eventCode", "6"));
		filters.add(Filters.eq("eventCode", "4"));
		filters.add(Filters.eq("eventCode", "5"));
		filters.add(Filters.eq("eventCode", "7"));
		filters.add(Filters.eq("eventCode", "38"));
		filters.add(Filters.eq("eventCode", "76"));
		filters.add(Filters.eq("eventCode", "102"));
		AggregateIterable<Document> result = mongoTemplate.getCollection("messageBroker")
				.aggregate(Arrays.asList(Aggregates.match(Filters.or(filters)),
						Aggregates.match(Filters.gte("dateTime", startDate)),
						Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
						Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
						Aggregates.group("$data.imageId", Accumulators.sum("count", 1)),
						Aggregates.project(Projections.include("data.imageId"))));

		if (result == null) {
			return imageList;
		}
		for (Document d : result) {
			if (d == null || d.get("_id") == null || d.get("_id").toString().equals("")) {
				continue;
			}
			imageList.add(d.get("_id").toString());
		}
		return imageList;
	}

	@Override
	public long getUserWiseTargetAppShareCount(String AppId, String tagetApp, String imageId, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("eventCode", "4"));
		filters.add(Filters.eq("eventCode", "5"));
		filters.add(Filters.eq("eventCode", "7"));
		filters.add(Filters.eq("eventCode", "38"));
		filters.add(Filters.eq("eventCode", "76"));
		filters.add(Filters.eq("eventCode", "102"));
		cursor = mongoTemplate.getCollection("messageBroker")
				.aggregate(Arrays.asList(Aggregates.match(Filters.or(filters)),
						Aggregates.match(Filters.eq("applicationID", AppId)),
						Aggregates.match(Filters.eq("data.imageId", imageId)),
						Aggregates.match(Filters.eq("data.targetapplication", tagetApp)),
						Aggregates.match(Filters.gte("dateTime", startDate)),
						Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
				.iterator();
		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getExtendedTargetAppShareCount(String tagetApp, String imageId, Date startDate, String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("eventCode", "4"));
		filters.add(Filters.eq("eventCode", "5"));
		filters.add(Filters.eq("eventCode", "7"));
		filters.add(Filters.eq("eventCode", "38"));
		filters.add(Filters.eq("eventCode", "76"));
		filters.add(Filters.eq("eventCode", "102"));

		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.eq("data.imageId", imageId)),
							Aggregates.match(Filters.eq("data.targetapplication", tagetApp)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.eq("data.imageId", imageId)),
							Aggregates.match(Filters.eq("data.targetapplication", tagetApp)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.eq("data.targetapplication", tagetApp)),
							Aggregates.match(Filters.eq("data.imageId", imageId)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	public Map<String, Integer> getAllKeywordsByDate(Date dateStart, String platform) {
		Map<String, Integer> keywordMap = new HashMap<String, Integer>();
		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("eventCode", "1"));
		filters.add(Filters.eq("eventCode", "75"));
		AggregateIterable<Document> result = null;
		if (platform.equalsIgnoreCase("Android")) {
			result = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", dateStart)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(dateStart))),
							Aggregates.match(Filters.or(filters)), Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$data.keyword", Accumulators.sum("count", 1))));
		} else if (platform.equalsIgnoreCase("iOS")) {
			result = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", dateStart)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(dateStart))),
							Aggregates.match(Filters.or(filters)), Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$data.keyword", Accumulators.sum("count", 1))));

		} else if (platform.equalsIgnoreCase("All")) {
			result = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", dateStart)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(dateStart))),
							Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$data.keyword", Accumulators.sum("count", 1))));
		}
		if (result == null) {
			return null;
		}
		for (Document d : result) {
			if (d == null || d.get("_id") == null || d.get("_id").toString().equals("")) {
				continue;
			}
			keywordMap.put(d.get("_id").toString(), Integer.parseInt(d.get("count").toString()));
		}
		return keywordMap;
	}

	@Override
	public long getLanguageCount(String language, Date startDate, String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		List<Bson> langFilter = new ArrayList<>();
		if (language.equalsIgnoreCase("Hindi")) {
			langFilter.add(Filters.eq("data.languagepreference", "Hindi"));
			langFilter.add(Filters.eq("data.languagepreference", "हिंदी"));
		} else {
			langFilter.add(Filters.eq("data.languagepreference", language));
		}

		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "82")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.or(langFilter)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "82")),
							Aggregates.match(Filters.eq("platform", "iOS")), Aggregates.match(Filters.or(langFilter)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "82")),
							Aggregates.match(Filters.or(langFilter)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getTargetAppShareCount(String tagetApp, Date startDate, String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("eventCode", "4"));
		filters.add(Filters.eq("eventCode", "5"));
		filters.add(Filters.eq("eventCode", "7"));
		filters.add(Filters.eq("eventCode", "38"));
		filters.add(Filters.eq("eventCode", "76"));
		filters.add(Filters.eq("eventCode", "102"));

		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.eq("data.targetapplication", tagetApp)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.eq("data.targetapplication", tagetApp)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.eq("data.targetapplication", tagetApp)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getMobileUserInitReportByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.eq("eventCode", "22")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.eq("eventCode", "22")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("eventCode", "22")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getMobileUserResetOTPReportByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.eq("eventCode", "24")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.eq("eventCode", "24")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("eventCode", "24")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getFBUserInitReportByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.eq("eventCode", "20")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.eq("eventCode", "20")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("eventCode", "20")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getNotificationViewCountByDate(Date Date, String platform) {
		Query queryEnabled = new Query();
		if (platform.toUpperCase().equals("ANDROID")) {
			queryEnabled.addCriteria(Criteria.where("dateTime").gte(Date).lt(getNextDateObject(Date)));
			queryEnabled.addCriteria(Criteria.where("eventCode").is("85"));
			queryEnabled.addCriteria(Criteria.where("platform").is("Android"));
			queryEnabled.addCriteria(Criteria.where("applicationID").nin(Declaration.getExclusionList()));
		} else if (platform.toUpperCase().equals("IOS")) {
			queryEnabled.addCriteria(Criteria.where("dateTime").gte(Date).lt(getNextDateObject(Date)));
			queryEnabled.addCriteria(Criteria.where("eventCode").is("85"));
			queryEnabled.addCriteria(Criteria.where("platform").is("iOS"));
			queryEnabled.addCriteria(Criteria.where("applicationID").nin(Declaration.getExclusionList()));
		} else if (platform.toUpperCase().equals("ALL")) {
			queryEnabled.addCriteria(Criteria.where("dateTime").gte(Date).lt(getNextDateObject(Date)));
			queryEnabled.addCriteria(Criteria.where("eventCode").is("85"));
			queryEnabled.addCriteria(Criteria.where("applicationID").nin(Declaration.getExclusionList()));
		}
		long count = mongoTemplate.count(queryEnabled, UsageData.class);
		return count;
	}
	
	@Override
	public long getReadNotificationCountByNid(String n_id) {			
			Query query = new Query();
			query.addCriteria(Criteria.where("eventCode").is("85"));
			query.addCriteria(Criteria.where("data.notificationid").is(n_id));					
			return mongoTemplate.count(query, UsageData.class);
	}
	

	@Override
	public Map<String, Integer> getNotificationReadCountByNid(Date startDate, String platform) {
		Map<String, Integer> keywordMap = new HashMap<String, Integer>();
		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("eventCode", "85"));
		AggregateIterable<Document> result = null;
		if (platform.equalsIgnoreCase("Android")) {
			result = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "85")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$data.notificationid", Accumulators.sum("count", 1))));
		} else if (platform.equalsIgnoreCase("iOS")) {
			result = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "85")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$data.notificationid", Accumulators.sum("count", 1))));
		} else if (platform.equalsIgnoreCase("All")) {
			result = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "85")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$data.notificationid", Accumulators.sum("count", 1))));
		}
		if (result == null) {
			return null;
		}
		for (Document d : result) {
			if (d == null || d.get("_id") == null || d.get("_id").toString().equals("")) {
				continue;
			}
			keywordMap.put(d.get("_id").toString(), Integer.parseInt(d.get("count").toString()));
		}
		return keywordMap;
	}

	@Override
	public Map<String, Integer> getNotificationReadCountByNidNoExclusion(Date startDate, String platform) {
		Map<String, Integer> keywordMap = new HashMap<String, Integer>();
		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("eventCode", "85"));

		AggregateIterable<Document> result = null;
		if (platform.equalsIgnoreCase("Android")) {
			result = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "85")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$data.notificationid", Accumulators.sum("count", 1))));
		} else if (platform.equalsIgnoreCase("iOS")) {
			result = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "85")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$data.notificationid", Accumulators.sum("count", 1))));
		} else if (platform.equalsIgnoreCase("All")) {
			result = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "85")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$data.notificationid", Accumulators.sum("count", 1))));
		}
		if (result == null) {
			return null;
		}
		for (Document d : result) {
			if (d == null || d.get("_id") == null || d.get("_id").toString().equals("")) {
				continue;
			}
			keywordMap.put(d.get("_id").toString(), Integer.parseInt(d.get("count").toString()));
		}
		return keywordMap;
	}

	@Override
	public Map<String, Integer> getViewedCountByImageId(Date startDate, Date endDate) {
		Map<String, Integer> viewedMap = new LinkedHashMap<String, Integer>();
		MongoCursor<Document> cursor = null;

		cursor = mongoTemplate.getCollection("messageBroker")
				.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "6")),
						Aggregates.match(Filters.gte("dateTime", startDate)),
						Aggregates.match(Filters.lt("dateTime", getNextDateObject(endDate))),
						Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
						Aggregates.group("$data.imageId", Accumulators.sum("count", 1))))
				.iterator();
		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				if (d.getString("_id") == null || d.getString("_id").toString().equals("")) {
					continue;
				} else {
					viewedMap.put(d.getString("_id").toString(), d.getInteger("count"));
				}
			}
		}
		return viewedMap;
	}

	@Override
	public Map<String, Integer> getSharedCountByImageID(Date startDate, Date endDate) {
		Map<String, Integer> sharedMap = new LinkedHashMap<String, Integer>();
		MongoCursor<Document> cursor = null;
		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("eventCode", "4"));
		filters.add(Filters.eq("eventCode", "5"));
		filters.add(Filters.eq("eventCode", "7"));
		filters.add(Filters.eq("eventCode", "38"));
		filters.add(Filters.eq("eventCode", "76"));
		filters.add(Filters.eq("eventCode", "102"));

		cursor = mongoTemplate.getCollection("messageBroker")
				.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
						Aggregates.match(Filters.lt("dateTime", getNextDateObject(endDate))),
						Aggregates.match(Filters.or(filters)),
						Aggregates.group("$data.imageId", Accumulators.sum("count", 1)),
						Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
				.iterator();

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				if (d.getString("_id") == null || d.getString("_id").toString().equals("")) {
					continue;
				} else {
					sharedMap.put(d.getString("_id").toString(), d.getInteger("count"));
				}
			}
		}
		return sharedMap;
	}
	
	@Override
	public ArrayList<String> getTotalActiveAppIDByDate(String platform, Date startDate) {
		MongoCursor<Document> cursor = null;	
		ArrayList<String> appIdList = new ArrayList<String>();	
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.nin("eventCode", "126")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.nin("eventCode", "126")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("eventCode", "126")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				if (d.getString("_id") == null || d.getString("_id").toString().equals("")) {
					continue;
				} else {
					appIdList.add(d.getString("_id").trim().toString());					
				}				
			}
		}		
		return appIdList;
	}
	
	@Override
	public ArrayList<String> getTotalActiveAppIDBetweenDate(String platform, Date startDate,Date endDate) {
		MongoCursor<Document> cursor = null;	
		ArrayList<String> appIdList = new ArrayList<String>();	
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime",endDate )),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.nin("eventCode", "126")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", endDate)),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.nin("eventCode", "126")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", endDate)),
							Aggregates.match(Filters.nin("eventCode", "126")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				if (d.getString("_id") == null || d.getString("_id").toString().equals("")) {
					continue;
				} else {
					appIdList.add(d.getString("_id").trim().toString());					
				}				
			}
		}		
		return appIdList;
	}
	
	

	@Override
	public long getTotalActiveUsersByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;

		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.nin("eventCode", "126")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.nin("eventCode", "126")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("eventCode", "126")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}
		
		logger.info("TIME : "+startDate+"  to  "+getNextDateObject(startDate));
		
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				logger.info(d.toJson());
				Count++;
			}
		}
		return Count;
	}

	@Override
	public List<String> getAllActiveUsersListBetweenDate(String platform, Date startDate, Date endDate) {
		List<String> AllApplicationIDs = new ArrayList<String>();
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(endDate))),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.nin("eventCode", "126")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.nin("eventCode", "126")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.nin("eventCode", "126")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				if (d.getString("_id") == null || d.getString("_id").toString().equals("")) {
					continue;
				} else {
					// System.out.println(d.toJson() + " " + d.getString("_id"));
					AllApplicationIDs.add(d.getString("_id"));
				}
			}
		}
		return AllApplicationIDs;
	}

	@Override
	public long getTotalNewUsersByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.eq("eventCode", "9")),
							Aggregates.group("$deviceId", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.eq("eventCode", "9")),
							Aggregates.group("$deviceId", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("eventCode", "9")),
							Aggregates.group("$deviceId", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	// ################################### REPORT ENDS
	// ##############################################################

	/**
	 * Notification DAO START
	 */

	@Override
	public boolean isAppFirstOpen(String AppID, Date startDate) {
		Query query = new Query(Criteria.where("dateTime").gte(startDate).lt(getNextDateObject(startDate)));
		query.addCriteria(Criteria.where("eventCode").is("14"));
		query.addCriteria(Criteria.where("applicationID").is(AppID));
		long count = mongoTemplate.count(query, UsageData.class);
		return (count > 0 ? true : false);
	}

	@Override
	public boolean isGuestUser(String AppID) {
		Query query = new Query();
		Criteria criteria = new Criteria();
		query.addCriteria(Criteria.where("applicationID").is(AppID));
		query.addCriteria(criteria.orOperator(Criteria.where("userID").ne(""), Criteria.where("userID").ne("NA"),
				Criteria.where("userID").ne(null)));
		long count = mongoTemplate.count(query, UsageData.class);
		return (count > 0 ? false : true);
	}

	@Override
	public boolean isKeyboardConnected(String AppID) {
		Query query = new Query();
		query.addCriteria(Criteria.where("applicationID").is(AppID));
		query.addCriteria(Criteria.where("eventCode").is("17"));
		long enbaleCount = mongoTemplate.count(query, UsageData.class);
		Query query2 = new Query();
		query2.addCriteria(Criteria.where("applicationID").is(AppID));
		query2.addCriteria(Criteria.where("eventCode").is("88"));
		long disableCount = mongoTemplate.count(query2, UsageData.class);
		return (enbaleCount - disableCount > 0 ? true : false);
	}

	@Override
	public boolean isAvatarCreated(String AppID) {
		Query query = new Query();
		query.addCriteria(Criteria.where("applicationID").is(AppID));
		query.addCriteria(Criteria.where("eventCode").is("43"));
		long enbaleCount = mongoTemplate.count(query, UsageData.class);
		Query query2 = new Query();
		query2.addCriteria(Criteria.where("applicationID").is(AppID));
		query2.addCriteria(Criteria.where("eventCode").is("41"));
		long disableCount = mongoTemplate.count(query2, UsageData.class);
		return (enbaleCount - disableCount > 0 ? true : false);
	}

	/**
	 * Notification DAO END
	 */

	/**
	 * Analytics Report DAO START
	 */

	@Override
	public long findAvatarCreatedByDateUserLevel(String platform, Date Date) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "43")),
							Aggregates.group("$deviceId", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "43")),
							Aggregates.group("$deviceId", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "43")),
							Aggregates.group("$deviceId", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null || d.get("_id") == null || d.get("_id").equals("")) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long findAvatarResetByDateUserLevel(String platform, Date Date) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "41")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "41")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "41")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null || d.get("_id") == null || d.get("_id").equals("")) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getSignupCountByDate(Date Date, String platform, String signupType) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			if (signupType.toUpperCase().equals("FB")) {
				cursor = mongoTemplate.getCollection("messageBroker")
						.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
								Aggregates.match(Filters.gte("dateTime", Date)),
								Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
								Aggregates.match(Filters.eq("eventCode", "21")),
								Aggregates.group("$userID", Accumulators.sum("count", 1))))
						.iterator();
			} else if (signupType.toUpperCase().equals("MOBILE")) {
				cursor = mongoTemplate.getCollection("messageBroker")
						.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
								Aggregates.match(Filters.gte("dateTime", Date)),
								Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
								Aggregates.match(Filters.eq("eventCode", "23")),
								Aggregates.group("$userID", Accumulators.sum("count", 1))))
						.iterator();
			}
		} else if (platform.toUpperCase().equals("IOS")) {
			if (signupType.toUpperCase().equals("FB")) {
				cursor = mongoTemplate.getCollection("messageBroker")
						.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
								Aggregates.match(Filters.gte("dateTime", Date)),
								Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
								Aggregates.match(Filters.eq("eventCode", "21")),
								Aggregates.group("$userID", Accumulators.sum("count", 1))))
						.iterator();
			} else if (signupType.toUpperCase().equals("MOBILE")) {
				cursor = mongoTemplate.getCollection("messageBroker")
						.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
								Aggregates.match(Filters.gte("dateTime", Date)),
								Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
								Aggregates.match(Filters.eq("eventCode", "23")),
								Aggregates.group("$userID", Accumulators.sum("count", 1))))
						.iterator();
			}
		} else if (platform.toUpperCase().equals("ALL")) {
			if (signupType.toUpperCase().equals("FB")) {
				cursor = mongoTemplate.getCollection("messageBroker")
						.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
								Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
								Aggregates.match(Filters.eq("eventCode", "21")),
								Aggregates.group("$userID", Accumulators.sum("count", 1))))
						.iterator();
			} else if (signupType.toUpperCase().equals("MOBILE")) {
				cursor = mongoTemplate.getCollection("messageBroker")
						.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
								Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
								Aggregates.match(Filters.eq("eventCode", "23")),
								Aggregates.group("$userID", Accumulators.sum("count", 1))))
						.iterator();
			}
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null || d.get("_id") == null || d.get("_id").equals("")) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getKeyboardEnabledCountByDate(Date Date, String platform) {
		Query queryEnabled = new Query();
		if (platform.toUpperCase().equals("ANDROID")) {
			queryEnabled.addCriteria(Criteria.where("dateTime").gte(Date).lt(getNextDateObject(Date)));
			queryEnabled.addCriteria(Criteria.where("eventCode").is("17"));
			queryEnabled.addCriteria(Criteria.where("platform").is("Android"));
		} else if (platform.toUpperCase().equals("IOS")) {
			queryEnabled.addCriteria(Criteria.where("dateTime").gte(Date).lt(getNextDateObject(Date)));
			queryEnabled.addCriteria(Criteria.where("eventCode").is("17"));
			queryEnabled.addCriteria(Criteria.where("platform").is("iOS"));
		} else if (platform.toUpperCase().equals("ALL")) {
			queryEnabled.addCriteria(Criteria.where("dateTime").gte(Date).lt(getNextDateObject(Date)));
			queryEnabled.addCriteria(Criteria.where("eventCode").is("17"));
		}
		long count = mongoTemplate.count(queryEnabled, UsageData.class);
		return count;
	}

	@Override
	public long getKeyboardDisabledCountByDate(Date Date, String platform) {
		Query queryDisabled = new Query();
		if (platform.toUpperCase().equals("ANDROID")) {
			queryDisabled.addCriteria(Criteria.where("dateTime").gte(Date).lt(getNextDateObject(Date)));
			queryDisabled.addCriteria(Criteria.where("eventCode").is("88"));
			queryDisabled.addCriteria(Criteria.where("platform").is("Android"));
		} else if (platform.toUpperCase().equals("IOS")) {
			queryDisabled.addCriteria(Criteria.where("dateTime").gte(Date).lt(getNextDateObject(Date)));
			queryDisabled.addCriteria(Criteria.where("eventCode").is("88"));
			queryDisabled.addCriteria(Criteria.where("platform").is("iOS"));
		} else if (platform.toUpperCase().equals("ALL")) {
			queryDisabled.addCriteria(Criteria.where("dateTime").gte(Date).lt(getNextDateObject(Date)));
			queryDisabled.addCriteria(Criteria.where("eventCode").is("88"));
		}
		long count = mongoTemplate.count(queryDisabled, UsageData.class);
		return count;
	}

	@Override
	public long getTotalUniqueUsers(String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		switch (platform) {
		case "Android":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;
		case "iOS":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;

		case "All":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.group("$_id", Accumulators.sum("count", 1)))).iterator();
			break;
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getKeyboardEnabledCountTillDate(Date sDate, String platform) {
		Query query = new Query();
		if (platform.equals("Android")) {
			query.addCriteria(Criteria.where("eventCode").is("17"));
			query.addCriteria(Criteria.where("platform").is(platform));
			query.addCriteria(Criteria.where("dateTime").lte(sDate));
		} else if (platform.equals("iOS")) {
			query.addCriteria(Criteria.where("eventCode").is("17"));
			query.addCriteria(Criteria.where("platform").is(platform));
			query.addCriteria(Criteria.where("dateTime").lte(sDate));
		} else if (platform.equals("All")) {
			query.addCriteria(Criteria.where("eventCode").is("17"));
			query.addCriteria(Criteria.where("dateTime").lte(sDate));
		}
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getKeyboardDisabledCountTillDate(Date sDate, String platform) {
		Query query = new Query();
		if (platform.equals("Android")) {
			query.addCriteria(Criteria.where("eventCode").is("88"));
			query.addCriteria(Criteria.where("platform").is(platform));
			query.addCriteria(Criteria.where("dateTime").lte(sDate));
		} else if (platform.equals("iOS")) {
			query.addCriteria(Criteria.where("eventCode").is("88"));
			query.addCriteria(Criteria.where("platform").is(platform));
			query.addCriteria(Criteria.where("dateTime").lte(sDate));
		} else if (platform.equals("All")) {
			query.addCriteria(Criteria.where("eventCode").is("88"));
			query.addCriteria(Criteria.where("dateTime").lte(sDate));
		}
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public HashSet<String> getSubCategoryList(String platform, Date sDate) {
		HashSet<String> catList = new HashSet<String>();
		Query query = new Query();
		if (platform.equals("Android")) {
			query.addCriteria(Criteria.where("eventCode").is("7"));
			query.addCriteria(Criteria.where("platform").is("Android"));
			query.addCriteria(Criteria.where("dateTime").gte(sDate).lt(getNextDateObject(sDate)));
			query.addCriteria(Criteria.where("data.subcategory").exists(true));
		} else if (platform.equals("iOS")) {
			query.addCriteria(Criteria.where("eventCode").is("7"));
			query.addCriteria(Criteria.where("platform").is("iOS"));
			query.addCriteria(Criteria.where("dateTime").gte(sDate).lt(getNextDateObject(sDate)));
			query.addCriteria(Criteria.where("data.subcategory").exists(true));
		} else if (platform.equals("All")) {
			query.addCriteria(Criteria.where("eventCode").is("7"));
			query.addCriteria(Criteria.where("dateTime").gte(sDate).lt(getNextDateObject(sDate)));
			query.addCriteria(Criteria.where("data.subcategory").exists(true));
		}
		List<UsageData> tempList = mongoTemplate.find(query, UsageData.class);
		for (UsageData ud : tempList) {
			Map<String, String> mTemp = ud.getData();
			catList.add(mTemp.get("subcategory"));
		}
		return catList;
	}

	@Override
	public List<String> getSubCategoryListAndroid() {
		ArrayList<String> catList = new ArrayList<String>();
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("7"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		query.addCriteria(Criteria.where("data.subcategory").exists(true));
		List<UsageData> tempList = mongoTemplate.find(query, UsageData.class);
		for (UsageData ud : tempList) {
			Map<String, String> mTemp = ud.getData();
			catList.add(mTemp.get("subcategory"));
		}
		return catList;
	}

	@Override
	public List<String> getSubCategoryListiOS() {
		ArrayList<String> catList = new ArrayList<String>();
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("7"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		query.addCriteria(Criteria.where("data.subcategory").exists(true));
		List<UsageData> tempList = mongoTemplate.find(query, UsageData.class);
		for (UsageData ud : tempList) {
			Map<String, String> mTemp = ud.getData();
			catList.add(mTemp.get("subcategory"));
		}
		return catList;
	}

	@Override
	public HashSet<String> getSubCategoryList() {
		HashSet<String> catList = new HashSet<String>();
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("7"));
		query.addCriteria(Criteria.where("data.subcategory").exists(true));
		List<UsageData> tempList = mongoTemplate.find(query, UsageData.class);
		for (UsageData ud : tempList) {
			Map<String, String> mTemp = ud.getData();
			catList.add(mTemp.get("subcategory"));
		}
		return catList;
	}

	@Override
	public long getSubCategoryCountforAndroidByDate(Date sDate, String subCat) {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("7"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		query.addCriteria(Criteria.where("data.subcategory").is(subCat));
		query.addCriteria(Criteria.where("dateTime").is(sDate));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getSubCategoryCountforiOSByDate(Date sDate, String subCat) {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("7"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		query.addCriteria(Criteria.where("data.subcategory").is(subCat));
		query.addCriteria(Criteria.where("dateTime").is(sDate));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getSubCategoryCountforAllByDate(Date sDate, String subCat) {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("7"));
		query.addCriteria(Criteria.where("data.subcategory").is(subCat));
		query.addCriteria(Criteria.where("dateTime").is(sDate));
		long tempCount = mongoTemplate.count(query, UsageData.class);
		return tempCount;
	}

	@Override
	public LinkedHashMap<String, List<String>> getAllImageIDsBydateAndAppId(Date startDate, String ApplicationId) {
		LinkedHashMap<String, List<String>> imageMap = new LinkedHashMap<String, List<String>>();
		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("eventCode", "4"));
		filters.add(Filters.eq("eventCode", "5"));
		filters.add(Filters.eq("eventCode", "7"));
		filters.add(Filters.eq("eventCode", "38"));
		filters.add(Filters.eq("eventCode", "76"));
		filters.add(Filters.eq("eventCode", "102"));

		AggregateIterable<Document> result = null;
		result = mongoTemplate.getCollection("messageBroker")
				.aggregate(Arrays.asList(Aggregates.match(Filters.or(filters)),
						Aggregates.match(Filters.gte("dateTime", startDate)),
						Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
						Aggregates.match(Filters.eq("applicationID", ApplicationId))));
		if (result == null) {
			return imageMap;
		}
		int count = 0;

		for (Document d : result) {
			if (d == null || d.get("_id") == null || d.get("_id").toString().equals("")) {
				continue;
			}
			List<String> tempList = new ArrayList<String>();
			@SuppressWarnings("rawtypes")
			Map notificationMap = null;
			ObjectMapper mapper = new ObjectMapper();
			try {
				notificationMap = mapper.readValue(d.toJson(), Map.class);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			@SuppressWarnings("unchecked")
			Map<String, String> mapData = (Map<String, String>) notificationMap.get("data");

			if (mapData == null || mapData.get("imageId") == null) {
				continue;
			}
			// Add Data to List
			tempList.add(d.getDate("dateTime").toString());// 0
			tempList.add(d.getString("platform"));// 1
			if (d.getString("eventCode").equals("4") || d.getString("eventCode").equals("5")) {
				tempList.add("Keyboard");// 2
			} else if (d.getString("eventCode").equals("7")) {
				tempList.add("StandAlone"); // 2
			} else if (d.getString("eventCode").equals("38")) {
				tempList.add("Recommendation"); // 2
			} else if (d.getString("eventCode").equals("76")) {
				tempList.add("Searched"); // 2
			} else if (d.getString("eventCode").equals("102")) {
				tempList.add("Promotion"); // 2
			}
			tempList.add(mapData.get("imageId"));// 3
			tempList.add(mapData.get("category"));// 4
			tempList.add(mapData.get("subcategory")); // 5
			if (imageMap != null && imageMap.keySet().contains(tempList.get(3))) {
				count = Integer.parseInt(imageMap.get(tempList.get(3)).get(6));
				tempList.add((count + 1) + "");// 6
			} else {
				tempList.add("1");
			}
			imageMap.put(tempList.get(3), tempList);
		}
		return imageMap;
	}

	@Override
	public List<String> getTargetAppListByDate(Date startDate, String platform) {
		List<String> imageList = new ArrayList<String>();
		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("eventCode", "6"));
		filters.add(Filters.eq("eventCode", "4"));
		filters.add(Filters.eq("eventCode", "5"));
		filters.add(Filters.eq("eventCode", "7"));
		filters.add(Filters.eq("eventCode", "38"));
		filters.add(Filters.eq("eventCode", "76"));
		filters.add(Filters.eq("eventCode", "102"));
		AggregateIterable<Document> result = null;

		if (platform.equalsIgnoreCase("All")) {
			result = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$data.targetapplication", Accumulators.sum("count", 1)),
							Aggregates.project(Projections.include("data.targetapplication"))));

		} else {
			result = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.or(filters)),
							Aggregates.match(Filters.gte("platform", platform)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$data.targetapplication", Accumulators.sum("count", 1)),
							Aggregates.project(Projections.include("data.targetapplication"))));

		}
		if (result == null) {
			return imageList;
		}

		for (Document d : result) {
			if (d == null || d.get("_id") == null || d.get("_id").toString().equals("")) {
				continue;
			}
			imageList.add(d.get("_id").toString());
		}
		return imageList;
	}

	public long getTargetShareCountByDate(Date startDate, String platform, String targetApp) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "6")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.eq("data.targetapplication", targetApp)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "6")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.eq("data.targetapplication", targetApp)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "6")),
							Aggregates.match(Filters.eq("data.targetapplication", targetApp)),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	public long getKeywordSearchCountByDate(String keyword, String platform, Date startDate) {
		Query query = new Query();
		Criteria criteria = new Criteria();
		if (platform.equals("Android")) {
			query.addCriteria(Criteria.where("dateTime").gte(startDate).lt(getNextDateObject(startDate)));
			query.addCriteria(
					criteria.orOperator(Criteria.where("eventCode").is("1"), Criteria.where("eventCode").is("75")));
			query.addCriteria(Criteria.where("platform").is("Android"));
			query.addCriteria(Criteria.where("data.keyword").is(keyword));
		} else if (platform.equals("iOS")) {
			query.addCriteria(Criteria.where("dateTime").gte(startDate).lt(getNextDateObject(startDate)));
			query.addCriteria(
					criteria.orOperator(Criteria.where("eventCode").is("1"), Criteria.where("eventCode").is("75")));
			query.addCriteria(Criteria.where("platform").is("iOS"));
			query.addCriteria(Criteria.where("data.keyword").is(keyword));
		} else if (platform.equals("All")) {
			query.addCriteria(Criteria.where("dateTime").gte(startDate).lt(getNextDateObject(startDate)));
			query.addCriteria(
					criteria.orOperator(Criteria.where("eventCode").is("1"), Criteria.where("eventCode").is("75")));
			query.addCriteria(Criteria.where("data.keyword").is(keyword));
		}
		return mongoTemplate.count(query, UsageData.class);
	}

	// ############################################################################################################
	/* REPORTS START */

	@Override
	public long getTotalUniqueUsersGraphByDate(String platform, Date startDate) {
		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("eventCode", "21"));
		filters.add(Filters.eq("eventCode", "23"));
		filters.add(Filters.eq("eventCode", "11"));
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "Android")), Aggregates.match(Filters.or(filters)),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "iOS")), Aggregates.match(Filters.or(filters)),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {

			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.or(filters)),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null || d.get("_id") == null || d.get("_id").equals("")) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getTotalKeyboardEnableGraphByDate(String platform, Date startDate) {
		Query query = new Query();
		if (platform.equals("Android")) {
			query.addCriteria(Criteria.where("eventCode").is("17"));
			query.addCriteria(Criteria.where("platform").is("Android"));
			query.addCriteria(Criteria.where("dateTime").gte(startDate).lt(getNextDateObject(startDate)));
		} else if (platform.equals("iOS")) {
			query.addCriteria(Criteria.where("eventCode").is("17"));
			query.addCriteria(Criteria.where("platform").is("iOS"));
			query.addCriteria(Criteria.where("dateTime").gte(startDate).lt(getNextDateObject(startDate)));
		} else if (platform.equals("All")) {
			query.addCriteria(Criteria.where("eventCode").is("17"));
			query.addCriteria(Criteria.where("dateTime").gte(startDate).lt(getNextDateObject(startDate)));
		}
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotalKeyboardDisableGraphByDate(String platform, Date startDate) {
		Query query = new Query();
		if (platform.equals("Android")) {
			query.addCriteria(Criteria.where("eventCode").is("88"));
			query.addCriteria(Criteria.where("platform").is("Android"));
			query.addCriteria(Criteria.where("dateTime").gte(startDate).lt(getNextDateObject(startDate)));
		} else if (platform.equals("iOS")) {
			query.addCriteria(Criteria.where("eventCode").is("88"));
			query.addCriteria(Criteria.where("platform").is("iOS"));
			query.addCriteria(Criteria.where("dateTime").gte(startDate).lt(getNextDateObject(startDate)));
		} else if (platform.equals("All")) {
			query.addCriteria(Criteria.where("eventCode").is("88"));
			query.addCriteria(Criteria.where("dateTime").gte(startDate).lt(getNextDateObject(startDate)));
		}
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getOnboardingCountByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.eq("eventCode", "14")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.eq("eventCode", "14")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("eventCode", "14")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getSkippedCountByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;

		if (platform.equals("Android")) {

			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.eq("eventCode", "14")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.eq("eventCode", "14")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("eventCode", "14")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getTotalRegisteredUsersByDate(String platform, Date date) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("eventCode", "21"));
		filters.add(Filters.eq("eventCode", "23"));
		switch (platform) {
		case "Android":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(date))),
							Aggregates.match(Filters.eq("platform", "Android")), Aggregates.match(Filters.or(filters)),
							Aggregates.group("$userID", Accumulators.sum("count", 1))))
					.iterator();
			break;
		case "iOS":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(date))),
							Aggregates.match(Filters.eq("platform", "iOS")), Aggregates.match(Filters.or(filters)),
							Aggregates.group("$userID", Accumulators.sum("count", 1))))
					.iterator();
			break;

		case "All":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(date))),
							Aggregates.match(Filters.or(filters)),
							Aggregates.group("$userID", Accumulators.sum("count", 1))))
					.iterator();
			break;
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null || d.get("_id") == null || d.get("_id").equals("")) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getMobileUserCompleteReportByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.eq("eventCode", "23")),
							Aggregates.group("$userID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.eq("eventCode", "23")),
							Aggregates.group("$userID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("eventCode", "23")),
							Aggregates.group("$userID", Accumulators.sum("count", 1))))
					.iterator();
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getFBUserCompleteReportByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.eq("eventCode", "21")),
							Aggregates.group("$userID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.eq("eventCode", "21")),
							Aggregates.group("$userID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.match(Filters.eq("eventCode", "21")),
							Aggregates.group("$userID", Accumulators.sum("count", 1))))
					.iterator();
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {

				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getNewUserCountforAndroidByDate(Date sDate) {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("9"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		query.addCriteria(Criteria.where("dateTime").is(sDate));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getNewUserCountforiOSByDate(Date sDate) {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("9"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		query.addCriteria(Criteria.where("dateTime").is(sDate));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getNewUserCountforAllByDate(Date sDate) {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("9"));
		query.addCriteria(Criteria.where("dateTime").is(sDate));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getReturningUserCountforAndroidByDate(Date sDate) {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("11"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		query.addCriteria(Criteria.where("dateTime").is(sDate));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getReturningUserCountforiOSByDate(Date sDate) {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("11"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		query.addCriteria(Criteria.where("dateTime").is(sDate));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getReturningUserCountforAllByDate(Date sDate) {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("11"));
		query.addCriteria(Criteria.where("dateTime").is(sDate));
		return mongoTemplate.count(query, UsageData.class);
	}

	public static Date getNextDateObject(Date curDate) {
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(curDate);
		calendar.add(Calendar.DAY_OF_YEAR, 1);
		return calendar.getTime();
	}

	public static Date getlastWeekObject(Date curDate) {
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(curDate);
		calendar.roll(Calendar.DAY_OF_YEAR, -7);
		//LocalDate today = LocalDate.now();
		/*if (calendar.get(Calendar.DAY_OF_MONTH) < 8) {
			calendar.roll(Calendar.MONTH, -1);
		}*/
		return calendar.getTime();
	}

	public static Date getlastMonthObject(Date curDate) {
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(curDate);
		calendar.roll(Calendar.MONTH, -1);
		LocalDate localDate = curDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int month = localDate.getMonthValue();

		if (month == 1) {
			calendar.roll(Calendar.YEAR, -1);
		}
		return calendar.getTime();
	}

	@Override
	public List<UsageData> getBetweenDate1(Date startDate, Date endDate) {
		Query query = new Query(Criteria.where("dateTime").gte(startDate).lt(getNextDateObject(endDate)));
		return mongoTemplate.find(query, UsageData.class);
	}

	@Override
	public long getBetweenDate1Count(Date startDate, Date endDate) {
		Query query = new Query(Criteria.where("dateTime").gte(startDate).lt(getNextDateObject(endDate)));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public List<UsageData> getByDate2(Date startDate) {
		Query query = new Query(Criteria.where("dateTime").gte(startDate).lte(getNextDateObject(startDate)));
		return mongoTemplate.find(query, UsageData.class);
	}

	@Override
	public long getByDate2Count(Date startDate) {
		Query query = new Query(Criteria.where("dateTime").gte(startDate).lte(getNextDateObject(startDate)));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public List<UsageData> getByDateAndEvent1(Date startDate, Date endDate) {
		Query query = new Query(Criteria.where("dateTime").gte(startDate).lte(getNextDateObject(endDate)));
		query.addCriteria(Criteria.where("eventCode").is("43"));
		return mongoTemplate.find(query, UsageData.class);
	}

	@Override
	public long getByDateAndEvent1Count(Date startDate, Date endDate) {
		Query query = new Query(Criteria.where("dateTime").gte(startDate).lte(getNextDateObject(endDate)));
		query.addCriteria(Criteria.where("eventCode").is("43"));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public List<UsageData> getByDateAndEvent2(Date startDate) {
		Query query = new Query(Criteria.where("dateTime").gte(startDate).lte(getNextDateObject(startDate)));
		query.addCriteria(Criteria.where("eventCode").is("43"));
		return mongoTemplate.find(query, UsageData.class);
	}

	@Override
	public long getByDateAndEvent2Count(Date startDate) {
		Query query = new Query(Criteria.where("dateTime").gte(startDate).lte(getNextDateObject(startDate)));
		query.addCriteria(Criteria.where("eventCode").is("43"));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getAndroidSignupCountForFB(String sDate, String eDate) {
		Date startDate = dateFormatter(sDate);
		Date endDate = dateFormatter(eDate);
		if (startDate == null || endDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("21"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		query.addCriteria(
				Criteria.where("dateTime").gte(startDate).andOperator(Criteria.where("dateTime").lte(endDate)));
		List<UsageData> ud = mongoTemplate.find(query, UsageData.class);
		return ud.size();
	}

	@Override
	public long getiOSSignupCountForFB(String sDate, String eDate) {
		Date startDate = dateFormatter(sDate);
		Date endDate = dateFormatter(eDate);
		if (startDate == null || endDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("21"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		query.addCriteria(
				Criteria.where("dateTime").gte(startDate).andOperator(Criteria.where("dateTime").lte(endDate)));
		List<UsageData> ud = mongoTemplate.find(query, UsageData.class);
		return ud.size();
	}

	@Override
	public long getAndroidSignupCountForMobile(String sDate, String eDate) {// Parse Date
		Date startDate = dateFormatter(sDate);
		Date endDate = dateFormatter(eDate);

		if (startDate == null || endDate == null) {
			return 0;
		}

		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("23"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		query.addCriteria(
				Criteria.where("dateTime").gte(startDate).andOperator(Criteria.where("dateTime").lte(endDate)));
		List<UsageData> ud = mongoTemplate.find(query, UsageData.class);
		return ud.size();
	}

	@Override
	public long getiOSSignupCountForMobile(String sDate, String eDate) {
		Date startDate = dateFormatter(sDate);
		Date endDate = dateFormatter(eDate);
		if (startDate == null || endDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("23"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		query.addCriteria(
				Criteria.where("dateTime").gte(startDate).andOperator(Criteria.where("dateTime").lte(endDate)));
		List<UsageData> ud = mongoTemplate.find(query, UsageData.class);
		return ud.size();
	}

	private Date dateFormatter(String sDate) {
		DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date date = null;
		try {
			date = formatter.parse(sDate);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		return date;
	}

	@Override
	public long getTotalAndroidSignupCountForFB() {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("21"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		return mongoTemplate.count(query, "messageBroker");
	}

	@Override
	public long getTotaliOSSignupCountForFB() {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("21"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		return mongoTemplate.count(query, "messageBroker");
	}

	@Override
	public long getTotalAndroidSignupCountForMobile() {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("23"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		return mongoTemplate.count(query, "messageBroker");
	}

	@Override
	public long getTotaliOSSignupCountForMobile() {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("23"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		return mongoTemplate.count(query, "messageBroker");
	}

	@Override
	public String saveUsageData(UsageData usageData) {
		mongoTemplate.save(usageData);
		return "save success \n" + usageData.toString();
	}

	@Override
	public String getAllData(String sPlatform, Integer iEvent) {
		Gson gson = new Gson();
		Query query = new Query();
		query.addCriteria(Criteria.where("platform").is(sPlatform));
		query.addCriteria(Criteria.where("eventCode").is(iEvent.toString()));
		return gson.toJson(mongoTemplate.find(query, UsageData.class, "messageBroker"));
	}

	@Override
	public String getAllData(String platform) {
		Gson gson = new Gson();
		Query query = new Query();
		query.addCriteria(Criteria.where("platform").is(platform));
		return gson.toJson(mongoTemplate.find(query, UsageData.class, "messageBroker"));
	}

	@Override
	public String getAllData() {
		Gson gson = new Gson();
		return gson.toJson(mongoTemplate.findAll(UsageData.class, "messageBroker"));
	}

	@Override
	public long getAndroidSignupCountForFBAfterDate(String sDate) {
		Date startDate = dateFormatter(sDate);
		if (startDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("21"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		query.addCriteria(Criteria.where("dateTime").gte(startDate));
		List<UsageData> ud = mongoTemplate.find(query, UsageData.class);
		return ud.size();
	}

	@Override
	public long getiOSSignupCountForFBAfterDate(String sDate) {
		Date startDate = dateFormatter(sDate);
		if (startDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("dateTime").gte(startDate));
		query.addCriteria(Criteria.where("eventCode").is("21").andOperator(Criteria.where("platform").is("iOS")));
		List<UsageData> ud = mongoTemplate.find(query, UsageData.class);
		return ud.size();
	}

	@Override
	public long getAndroidSignupCountForMobileAfterDate(String sDate) {
		Date startDate = dateFormatter(sDate);
		if (startDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("23"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		query.addCriteria(Criteria.where("dateTime").gte(startDate));
		List<UsageData> ud = mongoTemplate.find(query, UsageData.class);
		return ud.size();
	}

	@Override
	public long getiOSSignupCountForMobileAfterDate(String sDate) {
		Date startDate = dateFormatter(sDate);
		if (startDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("23"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		query.addCriteria(Criteria.where("dateTime").gte(startDate));
		List<UsageData> ud = mongoTemplate.find(query, UsageData.class);
		return ud.size();
	}

	@Override
	public long getAndroidSignupCountForFBBeforeDate(String eDate) {
		Date endDate = dateFormatter(eDate);
		if (endDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("21"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		query.addCriteria(Criteria.where("dateTime").lte(new Date().after(endDate)));
		List<UsageData> ud = mongoTemplate.find(query, UsageData.class);
		return ud.size();
	}

	@Override
	public long getiOSSignupCountForFBBeforeDate(String eDate) {
		Date endDate = dateFormatter(eDate);
		if (endDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("21"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		query.addCriteria(Criteria.where("dateTime").lte(endDate));
		List<UsageData> ud = mongoTemplate.find(query, UsageData.class);
		return ud.size();
	}

	@Override
	public long getAndroidSignupCountForMobileBeforeDate(String eDate) {
		Date endDate = dateFormatter(eDate);

		if (endDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("23"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		query.addCriteria(Criteria.where("dateTime").lte(endDate));
		List<UsageData> ud = mongoTemplate.find(query, UsageData.class);
		return ud.size();
	}

	@Override
	public long getiOSSignupCountForMobileBeforeDate(String eDate) {
		Date endDate = dateFormatter(eDate);

		if (endDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("23"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		query.addCriteria(Criteria.where("dateTime").lte(endDate));
		List<UsageData> ud = mongoTemplate.find(query, UsageData.class);
		return ud.size();
	}

	@Override
	public long getiOSSignupCountForFBByDate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("21"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		query.addCriteria(Criteria.where("dateTime").is(locDate));

		List<UsageData> ud = mongoTemplate.find(query, UsageData.class);
		return ud.size();
	}

	@Override
	public long getAndroidSignupCountForMobileByDate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("23"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		query.addCriteria(Criteria.where("dateTime").is(locDate));

		List<UsageData> ud = mongoTemplate.find(query, UsageData.class);
		return ud.size();
	}

	@Override
	public long getiOSSignupCountForMobileByDate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("23"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		query.addCriteria(Criteria.where("dateTime").is(locDate));

		List<UsageData> ud = mongoTemplate.find(query, UsageData.class);
		return ud.size();
	}

	@Override
	public long getiOSAndAndroidCountForFBByDate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("21"));
		query.addCriteria(Criteria.where("dateTime").is(locDate));
		List<UsageData> ud = mongoTemplate.find(query, UsageData.class);
		return ud.size();
	}

	@Override
	public long getiOSAndAndroidCountForMobByDate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("23"));
		query.addCriteria(Criteria.where("dateTime").is(locDate));
		List<UsageData> ud = mongoTemplate.find(query, UsageData.class);
		return ud.size();
	}

	@Override
	public long getAndroidKeyboardEnabledCountByDate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("17"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		query.addCriteria(Criteria.where("dateTime").is(locDate));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getiOSKeyboardEnabledCountByDate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("17"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		query.addCriteria(Criteria.where("dateTime").is(locDate));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getiOSAndAndroidKeyboardEnabledCountByDate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("17"));
		query.addCriteria(Criteria.where("dateTime").is(locDate));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getiOSAndAndroidKeyboardDisabledCountByDate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("88"));
		query.addCriteria(Criteria.where("dateTime").is(locDate));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotaliOSAndAndroidKeyboardEnabledCount() {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("17"));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotaliOSAndAndroidKeyboardDisabledCount() {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("88"));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getAllUniqueUsers() {
		Query query = new Query();
		query.addCriteria(Criteria.where("applicationID").exists(true));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getAndroidKeyboardDisabledCountByDate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("88"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		query.addCriteria(Criteria.where("dateTime").is(locDate));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getiOSKeyboardDisabledCountByDate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("88"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		query.addCriteria(Criteria.where("dateTime").is(locDate));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getAllUniqueUsersAndroid() {
		Query query = new Query();
		query.addCriteria(Criteria.where("platform").is("Android"));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getAllUniqueUsersiOS() {
		Query query = new Query();
		query.addCriteria(Criteria.where("platform").is("iOS"));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotaliOSKeyboardDisabledCount() {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("88"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotalAndroidKeyboardDisabledCount() {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("88"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		return mongoTemplate.count(query, UsageData.class);
	}

	/* CATEGORY */

	@Override
	public long getSubCategoryCountforAndroid(String subCat) {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("7"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		query.addCriteria(Criteria.where("data.subcategory").is(subCat));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getSubCategoryCountforiOS(String subCat) {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("7"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		query.addCriteria(Criteria.where("data.subcategory").is(subCat));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getSubCategoryCountforAll(String subCat) {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("7"));
		query.addCriteria(Criteria.where("data.subcategory").is(subCat));
		long tempCount = mongoTemplate.count(query, UsageData.class);
		return tempCount;
	}

	@Override
	public long getTotaliOSKeyboardEnabledCount() {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("17"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotalAndroidKeyboardEnabledCount() {
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("17"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotaliOSKeyboardDisabledCountTillDate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("88"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		query.addCriteria(Criteria.where("dateTime").lte(locDate));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotalAndroidKeyboardDisabledCountTillDate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("88"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		query.addCriteria(Criteria.where("dateTime").lte(locDate));
		long cnt = mongoTemplate.count(query, UsageData.class);
		return cnt;
	}

	@Override
	public long getTotaliOSKeyboardEnabledCountTillDate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("17"));
		query.addCriteria(Criteria.where("platform").is("iOS"));
		query.addCriteria(Criteria.where("dateTime").lte(locDate));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotalAndroidKeyboardEnabledCountTillDate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("17"));
		query.addCriteria(Criteria.where("platform").is("Android"));
		query.addCriteria(Criteria.where("dateTime").lte(locDate));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotaliOSAndAndroidKeyboardEnabledCountTillDate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("17"));
		query.addCriteria(Criteria.where("dateTime").lte(locDate));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotaliOSAndAndroidKeyboardDisabledCountTillDate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("eventCode").is("88"));
		query.addCriteria(Criteria.where("dateTime").lte(locDate));
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getAllUniqueUsersTilldate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("dateTime").lte(locDate));
		List<UsageData> udList = mongoTemplate.find(query, UsageData.class);
		HashSet<String> uniqueApp = new HashSet<String>();
		for (UsageData ud : udList) {
			uniqueApp.add(ud.getApplicationID());
		}
		return uniqueApp.size();
	}

	@Override
	public long getAllUniqueUsersAndroidTillDate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("platform").is("Android"));
		query.addCriteria(Criteria.where("dateTime").lte(locDate));
		List<UsageData> udList = mongoTemplate.find(query, UsageData.class);
		HashSet<String> uniqueApp = new HashSet<String>();
		for (UsageData ud : udList) {
			uniqueApp.add(ud.getApplicationID());
		}
		return uniqueApp.size();
	}

	@Override
	public long getAllUniqueUsersiOSTillDate(String sDate) {
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("platform").is("iOS"));
		query.addCriteria(Criteria.where("dateTime").lte(locDate));
		List<UsageData> udList = mongoTemplate.find(query, UsageData.class);
		HashSet<String> uniqueApp = new HashSet<String>();
		for (UsageData ud : udList) {
			uniqueApp.add(ud.getApplicationID());
		}
		return uniqueApp.size();
	}

	// ###############################################################
	// ###################### FRESH ################################
	// ###############################################################

	@Override
	public long getTotalUniqueUsersBeforeDate(String platform, String Date) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		Date locDate = dateFormatter(Date);
		if (locDate == null) {
			return 0;
		}

		switch (platform) {
		case "Android":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.lte("dateTime", locDate)),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;
		case "iOS":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.lte("dateTime", locDate)),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;

		case "All":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.lte("dateTime", locDate)),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getTotalUniqueUsersBetweenDates(String platform, String sDate, String eDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		Date locsDate = dateFormatter(sDate);
		Date loceDate = dateFormatter(eDate);
		if (locsDate == null || loceDate == null) {
			return 0;
		}

		switch (platform) {
		case "Android":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", locsDate)),
							Aggregates.match(Filters.lte("dateTime", loceDate)),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;
		case "iOS":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", locsDate)),
							Aggregates.match(Filters.lte("dateTime", loceDate)),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;

		case "All":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", locsDate)),
							Aggregates.match(Filters.lte("dateTime", loceDate)),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getTotalKeyboardEnable(String platform) {
		long count = 0;
		Query query = new Query();
		Query query2 = new Query();
		if (platform.toUpperCase().equals("ANDROID")) {
			query.addCriteria(Criteria.where("eventCode").is("17"));
			query.addCriteria(Criteria.where("platform").is("Android"));
			long countEn = mongoTemplate.count(query, UsageData.class);
			query2.addCriteria(Criteria.where("eventCode").is("88"));
			query2.addCriteria(Criteria.where("platform").is("Android"));
			long countDis = mongoTemplate.count(query2, UsageData.class);
			count = countEn - countDis;
		} else if (platform.toUpperCase().equals("IOS")) {
			query.addCriteria(Criteria.where("eventCode").is("17"));
			query.addCriteria(Criteria.where("platform").is("iOS"));
			long countEn = mongoTemplate.count(query, UsageData.class);
			query2.addCriteria(Criteria.where("eventCode").is("88"));
			query2.addCriteria(Criteria.where("platform").is("iOS"));
			long countDis = mongoTemplate.count(query2, UsageData.class);
			count = countEn - countDis;
		} else if (platform.toUpperCase().equals("ALL")) {
			query.addCriteria(Criteria.where("eventCode").is("17"));
			long countEn = mongoTemplate.count(query, UsageData.class);
			query2.addCriteria(Criteria.where("eventCode").is("88"));
			long countDis = mongoTemplate.count(query2, UsageData.class);
			count = countEn - countDis;
		}
		return count;
	}

	@Override
	public long getTotalKeyboardEnableByDate(String platform, String Date) {
		long count = 0;
		Query query = new Query();
		Date locDate = dateFormatter(Date);
		if (locDate == null) {
			return 0;
		}
		if (platform.toUpperCase().equals("ANDROID")) {
			query.addCriteria(Criteria.where("eventCode").is("17"));
			query.addCriteria(Criteria.where("platform").is("Android"));
			query.addCriteria(Criteria.where("dateTime").is(locDate));
			long enableCnt = mongoTemplate.count(query, UsageData.class);

			query.addCriteria(Criteria.where("eventCode").is("88"));
			query.addCriteria(Criteria.where("platform").is("Android"));
			query.addCriteria(Criteria.where("dateTime").is(locDate));
			long disableCnt = mongoTemplate.count(query, UsageData.class);
			count = enableCnt - disableCnt;

		} else if (platform.toUpperCase().equals("IOS")) {
			query.addCriteria(Criteria.where("eventCode").is("17"));
			query.addCriteria(Criteria.where("platform").is("iOS"));
			query.addCriteria(Criteria.where("dateTime").is(locDate));
			long enableCnt = mongoTemplate.count(query, UsageData.class);

			query.addCriteria(Criteria.where("eventCode").is("88"));
			query.addCriteria(Criteria.where("platform").is("iOS"));
			query.addCriteria(Criteria.where("dateTime").is(locDate));
			long disableCnt = mongoTemplate.count(query, UsageData.class);
			count = enableCnt - disableCnt;

		} else if (platform.toUpperCase().equals("ALL")) {
			query.addCriteria(Criteria.where("eventCode").is("17"));
			query.addCriteria(Criteria.where("dateTime").is(locDate));
			long enableCnt = mongoTemplate.count(query, UsageData.class);

			query.addCriteria(Criteria.where("eventCode").is("88"));
			query.addCriteria(Criteria.where("dateTime").is(locDate));
			long disableCnt = mongoTemplate.count(query, UsageData.class);
			count = enableCnt - disableCnt;
		}
		return count;
	}

	@Override
	public long getTotalKeyboardEnableBeforeDate(String platform, String Date) {
		long count = 0;
		Query query = new Query();
		Date locDate = dateFormatter(Date);
		if (locDate == null) {
			return 0;
		}
		if (platform.toUpperCase().equals("ANDROID")) {
			query.addCriteria(Criteria.where("eventCode").is("17"));
			query.addCriteria(Criteria.where("platform").is("Android"));
			query.addCriteria(Criteria.where("dateTime").lte(locDate));
			long enableCnt = mongoTemplate.count(query, UsageData.class);

			query.addCriteria(Criteria.where("eventCode").is("88"));
			query.addCriteria(Criteria.where("platform").is("Android"));
			query.addCriteria(Criteria.where("dateTime").lte(locDate));
			long disableCnt = mongoTemplate.count(query, UsageData.class);
			count = enableCnt - disableCnt;

		} else if (platform.toUpperCase().equals("IOS")) {
			query.addCriteria(Criteria.where("eventCode").is("17"));
			query.addCriteria(Criteria.where("platform").is("iOS"));
			query.addCriteria(Criteria.where("dateTime").lte(locDate));
			long enableCnt = mongoTemplate.count(query, UsageData.class);

			query.addCriteria(Criteria.where("eventCode").is("88"));
			query.addCriteria(Criteria.where("platform").is("iOS"));
			query.addCriteria(Criteria.where("dateTime").lte(locDate));
			long disableCnt = mongoTemplate.count(query, UsageData.class);
			count = enableCnt - disableCnt;

		} else if (platform.toUpperCase().equals("ALL")) {
			query.addCriteria(Criteria.where("eventCode").is("17"));
			query.addCriteria(Criteria.where("dateTime").lte(locDate));
			long enableCnt = mongoTemplate.count(query, UsageData.class);

			query.addCriteria(Criteria.where("eventCode").is("88"));
			query.addCriteria(Criteria.where("dateTime").lte(locDate));
			long disableCnt = mongoTemplate.count(query, UsageData.class);
			count = enableCnt - disableCnt;
		}
		return count;
	}

	@Override
	public long getTotalKeyboardEnableBetweenDates(String platform, String sDate, String eDate) {
		long count = 0;
		Query query = new Query();
		Date locsDate = dateFormatter(sDate);
		Date loceDate = dateFormatter(eDate);
		if (locsDate == null || loceDate == null) {
			return 0;
		}

		if (platform.toUpperCase().equals("ANDROID")) {
			query.addCriteria(Criteria.where("eventCode").is("17"));
			query.addCriteria(Criteria.where("platform").is("Android"));
			query.addCriteria(
					Criteria.where("dateTime").gte(locsDate).andOperator(Criteria.where("dateTime").lte(loceDate)));
			long enableCnt = mongoTemplate.count(query, UsageData.class);

			query.addCriteria(Criteria.where("eventCode").is("88"));
			query.addCriteria(Criteria.where("platform").is("Android"));
			query.addCriteria(
					Criteria.where("dateTime").gte(locsDate).andOperator(Criteria.where("dateTime").lte(loceDate)));
			long disableCnt = mongoTemplate.count(query, UsageData.class);
			count = enableCnt - disableCnt;

		} else if (platform.toUpperCase().equals("IOS")) {
			query.addCriteria(Criteria.where("eventCode").is("17"));
			query.addCriteria(Criteria.where("platform").is("iOS"));
			query.addCriteria(
					Criteria.where("dateTime").gte(locsDate).andOperator(Criteria.where("dateTime").lte(loceDate)));
			long enableCnt = mongoTemplate.count(query, UsageData.class);

			query.addCriteria(Criteria.where("eventCode").is("88"));
			query.addCriteria(Criteria.where("platform").is("iOS"));
			query.addCriteria(
					Criteria.where("dateTime").gte(locsDate).andOperator(Criteria.where("dateTime").lte(loceDate)));
			long disableCnt = mongoTemplate.count(query, UsageData.class);
			count = enableCnt - disableCnt;

		} else if (platform.toUpperCase().equals("ALL")) {
			query.addCriteria(Criteria.where("eventCode").is("17"));
			query.addCriteria(
					Criteria.where("dateTime").gte(locsDate).andOperator(Criteria.where("dateTime").lte(loceDate)));
			long enableCnt = mongoTemplate.count(query, UsageData.class);

			query.addCriteria(Criteria.where("eventCode").is("88"));
			query.addCriteria(
					Criteria.where("dateTime").gte(locsDate).andOperator(Criteria.where("dateTime").lte(loceDate)));
			long disableCnt = mongoTemplate.count(query, UsageData.class);
			count = enableCnt - disableCnt;
		}
		return count;
	}

	@Override
	public long getTotalAvatarCreated(String platform) {
		Query query = new Query();
		if (platform.toUpperCase().equals("ANDROID")) {
			query.addCriteria(Criteria.where("eventCode").is("43"));
			query.addCriteria(Criteria.where("platform").is("Android"));
		} else if (platform.toUpperCase().equals("IOS")) {
			query.addCriteria(Criteria.where("eventCode").is("43"));
			query.addCriteria(Criteria.where("platform").is("iOS"));
		} else if (platform.toUpperCase().equals("ALL")) {
			query.addCriteria(Criteria.where("eventCode").is("43"));
		} else {
			return 0;
		}
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotalAvatarCreatedBeforeDate(String platform, String Date) {
		Query query = new Query();
		Date locDate = dateFormatter(Date);
		if (locDate == null) {
			return 0;
		}
		if (platform.toUpperCase().equals("ANDROID")) {
			query.addCriteria(Criteria.where("eventCode").is("43"));
			query.addCriteria(Criteria.where("platform").is("Android"));
			query.addCriteria(Criteria.where("dateTime").lte(locDate));
		} else if (platform.toUpperCase().equals("IOS")) {
			query.addCriteria(Criteria.where("eventCode").is("43"));
			query.addCriteria(Criteria.where("platform").is("iOS"));
			query.addCriteria(Criteria.where("dateTime").lte(locDate));
		} else if (platform.toUpperCase().equals("ALL")) {
			query.addCriteria(Criteria.where("eventCode").is("43"));
			query.addCriteria(Criteria.where("dateTime").lte(locDate));
		} else {
			return 0;
		}
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotalAvatarCreatedBetweenDates(String platform, String sDate, String eDate) {
		Query query = new Query();
		Date locsDate = dateFormatter(sDate);
		Date loceDate = dateFormatter(eDate);
		if (locsDate == null || loceDate == null) {
			return 0;
		}
		if (platform.toUpperCase().equals("ANDROID")) {
			query.addCriteria(Criteria.where("eventCode").is("43"));
			query.addCriteria(Criteria.where("platform").is("Android"));
			query.addCriteria(
					Criteria.where("dateTime").gte(locsDate).andOperator(Criteria.where("dateTime").lte(loceDate)));
		} else if (platform.toUpperCase().equals("IOS")) {
			query.addCriteria(Criteria.where("eventCode").is("43"));
			query.addCriteria(Criteria.where("platform").is("iOS"));
			query.addCriteria(
					Criteria.where("dateTime").gte(locsDate).andOperator(Criteria.where("dateTime").lte(loceDate)));
		} else if (platform.toUpperCase().equals("ALL")) {
			query.addCriteria(Criteria.where("eventCode").is("43"));
			query.addCriteria(
					Criteria.where("dateTime").gte(locsDate).andOperator(Criteria.where("dateTime").lte(loceDate)));
		} else {
			return 0;
		}
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotalShares(String platform) {
		Query query = new Query();
		Criteria criteria = new Criteria();
		if (platform.toUpperCase().equals("ANDROID")) {
			query.addCriteria(criteria.orOperator(Criteria.where("eventCode").is("4"),
					Criteria.where("eventCode").is("5"), Criteria.where("eventCode").is("7")));
			query.addCriteria(Criteria.where("platform").is("Android"));
		} else if (platform.toUpperCase().equals("IOS")) {
			query.addCriteria(criteria.orOperator(Criteria.where("eventCode").is("4"),
					Criteria.where("eventCode").is("5"), Criteria.where("eventCode").is("7")));
			query.addCriteria(Criteria.where("platform").is("iOS"));
		} else if (platform.toUpperCase().equals("ALL")) {
			query.addCriteria(criteria.orOperator(Criteria.where("eventCode").is("4"),
					Criteria.where("eventCode").is("5"), Criteria.where("eventCode").is("7")));
		} else {
			return 0;
		}
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotalSharesByDate(String platform, Date startDate) {
		Query query = new Query();
		Criteria criteria = new Criteria();
		if (platform.toUpperCase().equals("ANDROID")) {
			query.addCriteria(
					criteria.orOperator(Criteria.where("eventCode").is("4"), Criteria.where("eventCode").is("5"),
							Criteria.where("eventCode").is("7"), Criteria.where("eventCode").is("89")));
			query.addCriteria(Criteria.where("dateTime").gte(startDate).lt(getNextDateObject(startDate)));
			query.addCriteria(Criteria.where("platform").is("Android"));
		} else if (platform.toUpperCase().equals("IOS")) {
			query.addCriteria(
					criteria.orOperator(Criteria.where("eventCode").is("4"), Criteria.where("eventCode").is("5"),
							Criteria.where("eventCode").is("7"), Criteria.where("eventCode").is("89")));
			query.addCriteria(Criteria.where("dateTime").gte(startDate).lt(getNextDateObject(startDate)));
			query.addCriteria(Criteria.where("platform").is("iOS"));
		} else if (platform.toUpperCase().equals("ALL")) {
			query.addCriteria(
					criteria.orOperator(Criteria.where("eventCode").is("4"), Criteria.where("eventCode").is("5"),
							Criteria.where("eventCode").is("7"), Criteria.where("eventCode").is("89")));
			query.addCriteria(Criteria.where("dateTime").gte(startDate).lt(getNextDateObject(startDate)));
		} else {
			return 0;
		}
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotalSharesBeforeDate(String platform, String Date) {
		Query query = new Query();
		Date locDate = dateFormatter(Date);
		if (locDate == null) {
			return 0;
		}
		Criteria criteria = new Criteria();
		if (platform.toUpperCase().equals("ANDROID")) {
			query.addCriteria(criteria.orOperator(Criteria.where("eventCode").is("4"),
					Criteria.where("eventCode").is("5"), Criteria.where("eventCode").is("7")));
			query.addCriteria(Criteria.where("dateTime").lte(locDate));
			query.addCriteria(Criteria.where("platform").is("Android"));
		} else if (platform.toUpperCase().equals("IOS")) {
			query.addCriteria(criteria.orOperator(Criteria.where("eventCode").is("4"),
					Criteria.where("eventCode").is("5"), Criteria.where("eventCode").is("7")));
			query.addCriteria(Criteria.where("dateTime").lte(locDate));
			query.addCriteria(Criteria.where("platform").is("iOS"));
		} else if (platform.toUpperCase().equals("ALL")) {
			query.addCriteria(criteria.orOperator(Criteria.where("eventCode").is("4"),
					Criteria.where("eventCode").is("5"), Criteria.where("eventCode").is("7")));
			query.addCriteria(Criteria.where("dateTime").lte(locDate));
		} else {
			return 0;
		}
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotalSharesBetweenDates(String platform, String sDate, String eDate) {
		Query query = new Query();
		Date locsDate = dateFormatter(sDate);
		Date loceDate = dateFormatter(eDate);
		if (locsDate == null || loceDate == null) {
			return 0;
		}
		Criteria criteria = new Criteria();
		if (platform.toUpperCase().equals("ANDROID")) {
			query.addCriteria(criteria.orOperator(Criteria.where("eventCode").is("4"),
					Criteria.where("eventCode").is("5"), Criteria.where("eventCode").is("7")));
			query.addCriteria(
					Criteria.where("dateTime").gte(locsDate).andOperator(Criteria.where("dateTime").lte(loceDate)));
			query.addCriteria(Criteria.where("platform").is("Android"));
		} else if (platform.toUpperCase().equals("IOS")) {
			query.addCriteria(criteria.orOperator(Criteria.where("eventCode").is("4"),
					Criteria.where("eventCode").is("5"), Criteria.where("eventCode").is("7")));
			query.addCriteria(
					Criteria.where("dateTime").gte(locsDate).andOperator(Criteria.where("dateTime").lte(loceDate)));
			query.addCriteria(Criteria.where("platform").is("iOS"));
		} else if (platform.toUpperCase().equals("ALL")) {
			query.addCriteria(criteria.orOperator(Criteria.where("eventCode").is("4"),
					Criteria.where("eventCode").is("5"), Criteria.where("eventCode").is("7")));
			query.addCriteria(
					Criteria.where("dateTime").gte(locsDate).andOperator(Criteria.where("dateTime").lte(loceDate)));
		} else {
			return 0;
		}
		return mongoTemplate.count(query, UsageData.class);
	}

	
	@Override
	public long getTotalRegisteredUsers(String platform, String medium) {
		return 0;
	}

	@Override
	public long getTotalRegisteredUsersBeforeDate(String platform, String medium, String Date) {

		return 0;
	}

	@Override
	public long getTotalRegisteredUsersBetweenDates(String platform, String medium, String sDate, String eDate) {
		return 0;
	}

	@Override
	public long getTotalSignupCount(String platform, String medium) {
		Query query = new Query();
		switch (platform) {
		case "Android":
			if (medium.toUpperCase().equals("FB")) {
				query.addCriteria(Criteria.where("eventCode").is("21"));
				query.addCriteria(Criteria.where("platform").is("Android"));
			} else if (medium.toUpperCase().equals("MOBILE")) {
				query.addCriteria(Criteria.where("eventCode").is("23"));
				query.addCriteria(Criteria.where("platform").is("Android"));
			}
			break;
		case "iOS":
			if (medium.toUpperCase().equals("FB")) {
				query.addCriteria(Criteria.where("eventCode").is("21"));
				query.addCriteria(Criteria.where("platform").is("iOS"));
			} else if (medium.toUpperCase().equals("MOBILE")) {
				query.addCriteria(Criteria.where("eventCode").is("23"));
				query.addCriteria(Criteria.where("platform").is("iOS"));
			}
			break;
		}
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotalSignupCountByDate(String platform, String medium, String Date) {
		Query query = new Query();
		Date locDate = dateFormatter(Date);
		if (locDate == null) {
			return 0;
		}
		switch (platform) {
		case "Android":
			if (medium.toUpperCase().equals("FB")) {
				query.addCriteria(Criteria.where("eventCode").is("21"));
				query.addCriteria(Criteria.where("dateTime").is(locDate));
				query.addCriteria(Criteria.where("platform").is("Android"));
			} else if (medium.toUpperCase().equals("MOBILE")) {
				query.addCriteria(Criteria.where("eventCode").is("23"));
				query.addCriteria(Criteria.where("dateTime").is(locDate));
				query.addCriteria(Criteria.where("platform").is("Android"));
			}
			break;
		case "iOS":
			if (medium.toUpperCase().equals("FB")) {
				query.addCriteria(Criteria.where("eventCode").is("21"));
				query.addCriteria(Criteria.where("dateTime").is(locDate));
				query.addCriteria(Criteria.where("platform").is("iOS"));
			} else if (medium.toUpperCase().equals("MOBILE")) {
				query.addCriteria(Criteria.where("eventCode").is("23"));
				query.addCriteria(Criteria.where("dateTime").is(locDate));
				query.addCriteria(Criteria.where("platform").is("iOS"));
			}
			break;
		}
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotalSignupCountBeforeDate(String platform, String medium, String Date) {
		Query query = new Query();
		Date locDate = dateFormatter(Date);
		if (locDate == null) {
			return 0;
		}
		switch (platform) {
		case "Android":
			if (medium.toUpperCase().equals("FB")) {
				query.addCriteria(Criteria.where("eventCode").is("21"));
				query.addCriteria(Criteria.where("dateTime").lte(locDate));
				query.addCriteria(Criteria.where("platform").is("Android"));
			} else if (medium.toUpperCase().equals("MOBILE")) {
				query.addCriteria(Criteria.where("eventCode").is("23"));
				query.addCriteria(Criteria.where("dateTime").lte(locDate));
				query.addCriteria(Criteria.where("platform").is("Android"));
			}
			break;
		case "iOS":
			if (medium.toUpperCase().equals("FB")) {
				query.addCriteria(Criteria.where("eventCode").is("21"));
				query.addCriteria(Criteria.where("dateTime").lte(locDate));
				query.addCriteria(Criteria.where("platform").is("iOS"));
			} else if (medium.toUpperCase().equals("MOBILE")) {
				query.addCriteria(Criteria.where("eventCode").is("23"));
				query.addCriteria(Criteria.where("dateTime").lte(locDate));
				query.addCriteria(Criteria.where("platform").is("iOS"));
			}
			break;
		}
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotalSignupCountBetweenDates(String platform, String medium, String sDate, String eDate) {
		Query query = new Query();
		Date locsDate = dateFormatter(sDate);
		Date loceDate = dateFormatter(eDate);
		if (locsDate == null || loceDate == null) {
			return 0;
		}
		switch (platform) {
		case "Android":
			if (medium.toUpperCase().equals("FB")) {
				query.addCriteria(Criteria.where("eventCode").is("21"));
				query.addCriteria(
						Criteria.where("dateTime").gte(locsDate).andOperator(Criteria.where("dateTime").lte(loceDate)));
				query.addCriteria(Criteria.where("platform").is("Android"));
			} else if (medium.toUpperCase().equals("MOBILE")) {
				query.addCriteria(Criteria.where("eventCode").is("23"));
				query.addCriteria(
						Criteria.where("dateTime").gte(locsDate).andOperator(Criteria.where("dateTime").lte(loceDate)));
				query.addCriteria(Criteria.where("platform").is("Android"));
			}
			break;
		case "iOS":
			if (medium.toUpperCase().equals("FB")) {
				query.addCriteria(Criteria.where("eventCode").is("21"));
				query.addCriteria(
						Criteria.where("dateTime").gte(locsDate).andOperator(Criteria.where("dateTime").lte(loceDate)));
				query.addCriteria(Criteria.where("platform").is("iOS"));
			} else if (medium.toUpperCase().equals("MOBILE")) {
				query.addCriteria(Criteria.where("eventCode").is("23"));
				query.addCriteria(
						Criteria.where("dateTime").gte(locsDate).andOperator(Criteria.where("dateTime").lte(loceDate)));
				query.addCriteria(Criteria.where("platform").is("iOS"));
			}
			break;
		}
		return mongoTemplate.count(query, UsageData.class);
	}

	@Override
	public long getTotalUnregisteredUsers(String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		switch (platform) {
		case "Android":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.eq("userID", "")),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;
		case "iOS":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.eq("userID", "")),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;

		case "All":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("userID", "")),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getTotalUnregisteredUsersByDate(String platform, String sDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		Date locDate = dateFormatter(sDate);
		if (locDate == null) {
			return 0;
		}
		switch (platform) {
		case "Android":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("dateTime", locDate)),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.eq("userID", "")),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;
		case "iOS":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("dateTime", locDate)),
							Aggregates.match(Filters.eq("platform", "iOS")), Aggregates.match(Filters.eq("userID", "")),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;

		case "All":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("dateTime", locDate)),
							Aggregates.match(Filters.eq("userID", "")),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getTotalUnregisteredBeforeDate(String platform, String Date) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		Date locDate = dateFormatter(Date);
		if (locDate == null) {
			return 0;
		}
		switch (platform) {
		case "Android":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.lte("dateTime", locDate)),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.eq("userID", "")),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;
		case "iOS":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.lte("dateTime", locDate)),
							Aggregates.match(Filters.eq("platform", "iOS")), Aggregates.match(Filters.eq("userID", "")),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;

		case "All":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.lte("dateTime", locDate)),
							Aggregates.match(Filters.eq("userID", "")),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getTotalUnregisteredBetweenDates(String platform, String sDate, String eDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		Date locsDate = dateFormatter(sDate);
		Date loceDate = dateFormatter(sDate);
		if (locsDate == null || loceDate == null) {
			return 0;
		}
		switch (platform) {
		case "Android":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", locsDate)),
							Aggregates.match(Filters.lte("dateTime", loceDate)),
							Aggregates.match(Filters.eq("userID", "")),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;
		case "iOS":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", locsDate)),
							Aggregates.match(Filters.lte("dateTime", loceDate)),
							Aggregates.match(Filters.eq("userID", "")),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;

		case "All":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("userID", "")),
							Aggregates.match(Filters.gte("dateTime", locsDate)),
							Aggregates.match(Filters.lte("dateTime", loceDate)),
							Aggregates.group("$_id", Accumulators.sum("count", 1))))
					.iterator();
			break;
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getTotalRegisteredUsers(String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		switch (platform) {
		case "Android":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.group("$userID", Accumulators.sum("count", 1))))
					.iterator();
			break;
		case "iOS":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.group("$userID", Accumulators.sum("count", 1))))
					.iterator();
			break;

		case "All":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.group("$userID", Accumulators.sum("count", 1)))).iterator();
			break;
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getTotalRegisteredUsersBeforeDate(String platform, String Date) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		Date locDate = dateFormatter(Date);
		if (locDate == null) {
			return 0;
		}
		switch (platform) {
		case "Android":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.lte("dateTime", locDate)),
							Aggregates.group("$userID", Accumulators.sum("count", 1))))
					.iterator();
			break;
		case "iOS":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.lte("dateTime", locDate)),
							Aggregates.group("$userID", Accumulators.sum("count", 1))))
					.iterator();
			break;

		case "All":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.lte("dateTime", locDate)),
							Aggregates.group("$userID", Accumulators.sum("count", 1))))
					.iterator();
			break;
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getTotalRegisteredUsersBetweenDates(String platform, String sDate, String eDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		Date locsDate = dateFormatter(sDate);
		Date loceDate = dateFormatter(eDate);
		if (locsDate == null || loceDate == null) {
			return 0;
		}
		switch (platform) {
		case "Android":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", locsDate)),
							Aggregates.match(Filters.lte("dateTime", loceDate)),
							Aggregates.group("$userID", Accumulators.sum("count", 1))))
					.iterator();
			break;
		case "iOS":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", locsDate)),
							Aggregates.match(Filters.lte("dateTime", loceDate)),
							Aggregates.group("$userID", Accumulators.sum("count", 1))))
					.iterator();
			break;

		case "All":
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", locsDate)),
							Aggregates.match(Filters.lte("dateTime", loceDate)),
							Aggregates.group("$userID", Accumulators.sum("count", 1))))
					.iterator();
			break;
		}
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	public String getNextDate(String curDate) {
		String nextDate = "";
		try {
			Calendar today = Calendar.getInstance();
			DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			Date date = format.parse(curDate);
			today.setTime(date);
			today.add(Calendar.DAY_OF_YEAR, 1);
			nextDate = format.format(today.getTime());
		} catch (Exception e) {
			return nextDate;
		}
		return nextDate;
	}

	@Override
	public long getUniqueSignupCountByDate(Date Date, String platform, String signupType) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		@SuppressWarnings("unused")
		String type = "";
		if (platform.toUpperCase().equals("ANDROID")) {
			if (signupType.toUpperCase().equals("FB")) {
				cursor = mongoTemplate.getCollection("messageBroker")
						.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
								Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
								Aggregates.match(Filters.eq("platform", "Android")),
								Aggregates.match(Filters.eq("eventCode", "21")),
								Aggregates.group("$userID", Accumulators.sum("count", 1))))
						.iterator();
			} else if (signupType.toUpperCase().equals("MOBILE")) {
				cursor = mongoTemplate.getCollection("messageBroker")
						.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
								Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
								Aggregates.match(Filters.eq("platform", "Android")),
								Aggregates.match(Filters.eq("eventCode", "23")),
								Aggregates.group("$userID", Accumulators.sum("count", 1))))
						.iterator();
			}
		} else if (platform.toUpperCase().equals("IOS")) {
			if (signupType.toUpperCase().equals("FB")) {
				cursor = mongoTemplate.getCollection("messageBroker")
						.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
								Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
								Aggregates.match(Filters.eq("platform", "iOS")),
								Aggregates.match(Filters.eq("eventCode", "21")),
								Aggregates.group("$userID", Accumulators.sum("count", 1))))
						.iterator();
			} else if (signupType.toUpperCase().equals("MOBILE")) {
				cursor = mongoTemplate.getCollection("messageBroker")
						.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
								Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
								Aggregates.match(Filters.eq("platform", "iOS")),
								Aggregates.match(Filters.eq("eventCode", "23")),
								Aggregates.group("$userID", Accumulators.sum("count", 1))))
						.iterator();
			}
		} else if (platform.toUpperCase().equals("ALL")) {
			if (signupType.toUpperCase().equals("FB")) {
				type = "FB";
				cursor = mongoTemplate.getCollection("messageBroker")
						.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
								Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
								Aggregates.match(Filters.eq("eventCode", "21")),
								Aggregates.group("$userID", Accumulators.sum("count", 1))))
						.iterator();

			} else if (signupType.toUpperCase().equals("MOBILE")) {
				type = "MOBILE";
				cursor = mongoTemplate.getCollection("messageBroker")
						.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
								Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
								Aggregates.match(Filters.eq("eventCode", "23")),
								Aggregates.group("$userID", Accumulators.sum("count", 1))))
						.iterator();
			}
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null || d.get("_id") == null || d.get("_id").equals("")) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getKeyboardInitCountByDate(Date Date, String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "15"))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "15"))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "15"))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getKeyboardCompleteCountByDate(Date Date, String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		// Query queryEnabled = new Query();
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "17"))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "17"))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "17"))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getKeyboardEnableCountByDate(Date Date, String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "16"))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "16"))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "16"))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getKeyboardSearchedCountByDate(Date Date, String platform) {
		Query queryEnabled = new Query();
		if (platform.toUpperCase().equals("ANDROID")) {
			queryEnabled.addCriteria(Criteria.where("dateTime").gte(Date).lt(getNextDateObject(Date)));
			queryEnabled.addCriteria(Criteria.where("eventCode").is("1"));
			queryEnabled.addCriteria(Criteria.where("platform").is("Android"));
		} else if (platform.toUpperCase().equals("IOS")) {
			queryEnabled.addCriteria(Criteria.where("dateTime").gte(Date).lt(getNextDateObject(Date)));
			queryEnabled.addCriteria(Criteria.where("eventCode").is("1"));
			queryEnabled.addCriteria(Criteria.where("platform").is("iOS"));
		} else if (platform.toUpperCase().equals("ALL")) {
			queryEnabled.addCriteria(Criteria.where("dateTime").gte(Date).lt(getNextDateObject(Date)));
			queryEnabled.addCriteria(Criteria.where("eventCode").is("1"));
		}
		long count = mongoTemplate.count(queryEnabled, UsageData.class);
		return count;
	}

	@Override
	public long getUniqueSearchViewCountByDate(Date Date, String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "77")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "77")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "77")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getUniqueSearchSharedCountByDate(Date Date, String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "76")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "76")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "76")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public long getUniqueSearchCountByDate(Date Date, String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "75")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "75")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "75")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	@Override
	public List<String> getAllTargetAppBydate(Date startDate, Date endDate) {
		List<String> imageList = new ArrayList<String>();
		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("eventCode", "6"));
		filters.add(Filters.eq("eventCode", "4"));
		filters.add(Filters.eq("eventCode", "5"));
		filters.add(Filters.eq("eventCode", "7"));
		filters.add(Filters.eq("eventCode", "38"));
		filters.add(Filters.eq("eventCode", "76"));
		filters.add(Filters.eq("eventCode", "102"));

		AggregateIterable<Document> result = mongoTemplate.getCollection("messageBroker")
				.aggregate(Arrays.asList(Aggregates.match(Filters.or(filters)),
						Aggregates.match(Filters.gte("dateTime", startDate)),
						Aggregates.match(Filters.lt("dateTime", getNextDateObject(endDate))),
						Aggregates.group("$data.targetapplication", Accumulators.sum("count", 1)),
						Aggregates.project(Projections.include("data.targetapplication"))));
		if (result == null) {
			return imageList;
		}

		for (Document d : result) {
			if (d == null || d.get("_id") == null || d.get("_id").toString().equals("")) {
				continue;
			}
			imageList.add(d.get("_id").toString());
		}
		return imageList;
	}

	@Override
	public List<String> findUniqueAppIdFirstOpenByDate(String platform, Date Date) {
		MongoCursor<Document> cursor = null;
		List<String> listLocal = new ArrayList<String>();
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null || d.get("_id") == null || d.get("_id").equals("")) {
				break;
			} else {
				listLocal.add(d.get("_id").toString());
			}
		}
		return listLocal;
	}

	@Override
	public List<String> findUniqueAppIdEachLaunchByDate(Date Date) {
		MongoCursor<Document> cursor = null;
		List<String> listLocal = new ArrayList<String>();
		cursor = mongoTemplate.getCollection("messageBroker")
				.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
						Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
						Aggregates.match(Filters.eq("eventCode", "11")),
						Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
				.iterator();
		while (true) {
			Document d = cursor.tryNext();
			if (d == null || d.get("_id") == null || d.get("_id").equals("")) {
				break;
			} else {
				listLocal.add(d.get("_id").toString());
			}
		}
		return listLocal;
	}

	@Override
	public List<String> findAllEventsForAppIdByDate(String appId, Date Date) {
		List<String> listLocal = new ArrayList<String>();
		Query queryEnabled = new Query();
		queryEnabled.addCriteria(Criteria.where("dateTime").gte(Date).lt(getNextDateObject(Date)));
		queryEnabled.addCriteria(Criteria.where("applicationID").is(appId));
		List<UsageData> udList = mongoTemplate.find(queryEnabled, UsageData.class);

		for (UsageData locUd : udList) {
			if (locUd == null || locUd.getEventName() == null || locUd.getDateTime().toString() == null) {
				continue;
			}
			listLocal.add(locUd.getEventName() + "," + locUd.getDateTime().toString());
		}
		return listLocal;
	}

	@Override
	public Map<String, String> findAllEventDataForAppIdByDate(String appId, Date Date) {

		Map<String, String> locMap = new LinkedHashMap<String, String>();
		Query queryAll = new Query();
		queryAll.addCriteria(Criteria.where("dateTime").gte(Date).lt(getNextDateObject(Date)));
		queryAll.addCriteria(Criteria.where("applicationID").is(appId));

		List<UsageData> udList = mongoTemplate.find(queryAll, UsageData.class);

		for (UsageData locUd : udList) {
			if (locUd == null || locUd.getEventName() == null || locUd.getDateTime().toString() == null) {
				continue;
			}
			locMap.put(locUd.getEventName(), locUd.getDateTime().toString());
		}
		return locMap;
	}

	public String getUserActivityByEventAndDate(String AppID, String eventCode, Date date) {
		Query queryEnabled = new Query();
		queryEnabled.addCriteria(Criteria.where("applicationID").is(AppID));
		queryEnabled.addCriteria(Criteria.where("dateTime").gte(date).lt(getNextDateObject(date)));
		queryEnabled.addCriteria(Criteria.where("eventCode").is(eventCode));
		List<UsageData> tempList = mongoTemplate.find(queryEnabled, UsageData.class);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < tempList.size(); i++) {
			sb.append(tempList.get(i).getDateTime());
			if (i < tempList.size() - 1) {
				sb.append(",");
			}
		}

		return sb.toString();
	}

	@Override
	public String findAllAppIDWithMinCount() {
		MongoCursor<Document> cursor = null;
		ArrayList<String> output = new ArrayList<String>();

		cursor = mongoTemplate.getCollection("messageBroker")
				.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
						Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
				.iterator();
		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				if (d.get("_id") != null && Integer.parseInt(d.get("count").toString()) == 1) {
					// System.out.print("AppID : " + d.get("_id") + " Count : " +
					// d.get("count").toString() + " ");
					String s = "AppID : " + d.get("_id").toString() + "  Count : " + d.get("count").toString() + "   ";
					output.add(s + " " + getEventCode(d.get("_id").toString()));
				}
			}
		}
		return new Gson().toJson(output);
	}

	public String getEventCode(String AppID) {
		Query queryAll = new Query();
		queryAll.addCriteria(Criteria.where("applicationID").is(AppID));
		UsageData locUd = mongoTemplate.findOne(queryAll, UsageData.class);
		if (locUd == null || locUd.getEventName() == null || locUd.getDateTime().toString() == null) {
			return "";
		}
		return "Date : " + locUd.getDateTime() + " EventCode : " + locUd.getEventCode();
	}

	// ###################### SCREEN VIEW ###############################

	@Override
	public long getHomeScreenViewedCountByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "119")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "119")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "119")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getAvatarScreenViewedCountByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "120")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "120")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "120")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getStickerScreenViewedCountByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "121")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "121")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "121")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getSettingsScreenViewedCountByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "122")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "122")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "122")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getSearchScreenViewedCountByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "123")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "123")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "123")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getOnboardingScreenViewedCountByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "124")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "124")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "124")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getNotificationSettingScreenViewedCountByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "128")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "128")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "128")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getLanguagePreferenceScreenViewedCountByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "131")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "131")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "131")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getSignUpScreenViewedCountByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "132")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "132")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "132")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getMobileSignUpScreenViewedCountByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "133")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "133")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "133")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getLoginScreenViewedCountByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "134")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "134")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "134")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getLoginMobileScreenViewedCountByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "135")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "135")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "135")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getHelpCentreScreenViewedCountByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "136")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "136")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "136")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getReportAProblemScreenViewedCountByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "137")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "137")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "137")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getContactUsScreenViewedCountByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "139")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "139")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "139")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getLegalInfoScreenViewedCountByDate(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "141")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "141")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "141")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate)))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getHomeScreenViewedCountByDateUserLevel(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "119")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "119")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "119")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getAvatarScreenViewedCountByDateUserLevel(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "120")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "120")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "120")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getStickerScreenViewedCountByDateUserLevel(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "121")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "121")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "121")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getSettingsScreenViewedCountByDateUserLevel(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "122")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "122")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "122")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getSearchScreenViewedCountByDateUserLevel(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "123")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "123")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "123")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getOnboardingScreenViewedCountByDateUserLevel(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "124")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "124")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "124")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getOnboardingGenderViewedCountByDateUserLevel(String platform, Date startDate) {
		long count = 0;
		List<String> appIdList = new ArrayList<String>();
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "160")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "160")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "160")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				count++;
			}
		}

		return count;
	}

	@Override
	public long getOnboardingGenderCompletedCountByDateUserLevel(String platform, Date startDate) {
		List<String> appIdList = new ArrayList<String>();
		MongoCursor<Document> cursor = null;
		long count = 0;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "150")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "150")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "150")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				count++;
			}
		}

		return count;
	}

	@Override
	public long getOnboardingLanguageViewedCountByDateUserLevel(String platform, Date startDate) {
		List<String> appIdList = new ArrayList<String>();
		long count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "159")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "159")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "159")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				count++;
			}
		}

		return count;
	}
	
	@Override
	public long getOnboardingCompletedCountByDateUserLevel(String platform, Date startDate) {
		List<String> appIdList = new ArrayList<String>();
		long count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "125")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "125")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "125")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				count++;
			}
		}

		return count;
	}
	
	@Override
	public long getOnboardingViewedCountByDateUserLevel(String platform, Date startDate) {
		List<String> appIdList = new ArrayList<String>();
		long count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "124")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "124")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "124")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				count++;
			}
		}

		return count;
	}

	@Override
	public long getOnboardingLanguageCompletedCountByDateUserLevel(String platform, Date startDate) {
		List<String> appIdList = new ArrayList<String>();
		MongoCursor<Document> cursor = null;
		long count = 0;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "149")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "149")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "149")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				count++;
			}
		}

		return count;
	}

	@Override
	public long getNotificationSettingScreenViewedCountByDateUserLevel(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "128")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "128")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "128")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getLanguagePreferenceScreenViewedCountByDateUserLevel(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "131")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "131")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "131")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getSignUpScreenViewedCountByDateUserLevel(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "132")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "132")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "132")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getMobileSignUpScreenViewedCountByDateUserLevel(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "133")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "133")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "133")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getLoginScreenViewedCountByDateUserLevel(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "134")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "134")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "134")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getLoginMobileScreenViewedCountByDateUserLevel(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "135")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "135")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "135")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getHelpCentreScreenViewedCountByDateUserLevel(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "136")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "136")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "136")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getReportAProblemScreenViewedCountByDateUserLevel(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "137")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "137")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "137")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getContactUsScreenViewedCountByDateUserLevel(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "139")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "139")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "139")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long getLegalInfoScreenViewedCountByDateUserLevel(String platform, Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.equals("Android")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "141")),
							Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("iOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "141")),
							Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.equals("All")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", "141")),
							Aggregates.match(Filters.gte("dateTime", startDate)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}

		return Count;
	}

	@Override
	public long findAvatarCreatedCountByDate(Date Date, String platform) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "43")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "43")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "43")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null || d.get("_id") == null || d.get("_id").equals("")) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}
	
	

	@Override
	public long getWeeklyActiveUserCountByDate(Date startDate) {
		long Count = 0;
		logger.info("WEEKLY StartDate "+getlastWeekObject(startDate)+"   ENd Date "+startDate);
		
		MongoCursor<Document> cursor = null;
		cursor = mongoTemplate.getCollection("messageBroker")
				.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", getlastWeekObject(startDate))),
						Aggregates.match(Filters.lt("dateTime", getNextDateObject(startDate))),
						Aggregates.match(Filters.nin("eventCode", "126")),
						Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
						Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
				.iterator();

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {
				break;
			} else {
				Count++;
			}
		}
		return Count;
	}

	
	
	@Override
	public long getMonthlyActiveUserCountByDate(Date startDate) {
		long Count = 0;
		MongoCursor<Document> cursor = null;
		
		Date stdate = getlastMonthObject(startDate);
		
		
		logger.info("MONTHLY : "+stdate+" --  "+startDate);
		
		cursor = mongoTemplate.getCollection("messageBroker")
				.aggregate(Arrays.asList(
						Aggregates.match(Filters.gte("dateTime", stdate)),						
						Aggregates.match(Filters.lt("dateTime", startDate)),
						Aggregates.match(Filters.nin("eventCode", "126")),
						Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
						Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
				.iterator();

		while (true) {
			Document d = cursor.tryNext();

			if (d == null) {				
				break;
			} else {
				logger.info(d.toJson());
				Count++;
			}
		}
		return Count;
	}

	@Override
	public Map<String, Long> getAvatarFeatureReportByDate(String platform, Date Date, String eventCode, String fetureId) {
		long count = 0;
		Map<String, Long> retMap = new LinkedHashMap<String, Long>();
		// Set<String> idList = new HashSet<String>();
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", eventCode))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", eventCode))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", eventCode))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			}			
			
			List<String> list = new ArrayList(((Document) d.get("data")).values());
			
			List<String> listKey = new ArrayList(((Document) d.get("data")).keySet());
			
			if(listKey!=null && listKey.contains(fetureId)) {
				
				if (retMap.keySet().contains(list.get(listKey.indexOf(fetureId)))) {
					++count;
					// logger.info("Updating : "+list.get(2).trim()+ " "+eventCode );
					retMap.put(list.get(listKey.indexOf(fetureId)), count);
				} else {
					// logger.info("Adding : "+list.get(2)+" "+eventCode);
					count = 1;
					retMap.put(list.get(listKey.indexOf(fetureId)), count);
				}				
			}
		}
		return retMap;
	}

	@Override
	public List<String> getUserTypeKeyboardCompleteCountByDateUserLevel(Date Date, String platform) {
		List<String> appIDList = new ArrayList<String>();
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "17")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "17")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "17")),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1)),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList()))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null) {
				break;
			} else {
				if (d == null || d.get("_id") == null || d.get("_id").toString().equals("")) {
					continue;
				}
				appIDList.add(d.get("_id").toString());
			}
		}
		return appIDList;
	}

	@Override
	public List<String> findUserTypeAvatarCreatedByDate(String platform, Date Date) {
		List<String> appIDList = new ArrayList<String>();
		MongoCursor<Document> cursor = null;
		if (platform.toUpperCase().equals("ANDROID")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "Android")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "43")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("IOS")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("platform", "iOS")),
							Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "43")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		} else if (platform.toUpperCase().equals("ALL")) {
			cursor = mongoTemplate.getCollection("messageBroker")
					.aggregate(Arrays.asList(Aggregates.match(Filters.gte("dateTime", Date)),
							Aggregates.match(Filters.lt("dateTime", getNextDateObject(Date))),
							Aggregates.match(Filters.eq("eventCode", "43")),
							Aggregates.match(Filters.nin("applicationID", Declaration.getExclusionList())),
							Aggregates.group("$applicationID", Accumulators.sum("count", 1))))
					.iterator();
		}

		while (true) {
			Document d = cursor.tryNext();
			if (d == null || d.get("_id") == null || d.get("_id").equals("")) {
				break;
			} else {
				if (d == null || d.get("_id") == null || d.get("_id").toString().equals("")) {
					continue;
				}
				appIDList.add(d.get("_id").toString());
			}
		}
		return appIDList;
	}

	@Override
	public long getTotalActiveUsers(String platform) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long getTotalActiveUsersBeforeDate(String platform, String Date) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long getTotalActiveUsersBetweenDates(String platform, String sDate, String eDate) {
		// TODO Auto-generated method stub
		return 0;
	}
}
