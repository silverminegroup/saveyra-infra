package com.saveyra.analytics.searchHelper;

public class SearchData {
	private String Date;
	private String Platform;
	private long Searched;
	private long Viewed;
	private long Shared;
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public long getTotalSearched() {
		return Searched;
	}
	public void setTotalSearched(long totalSearched) {
		Searched = totalSearched;
	}
	public long getViewed() {
		return Viewed;
	}
	public void setViewed(long viewed) {
		Viewed = viewed;
	}
	public long getShared() {
		return Shared;
	}
	public void setShared(long shared) {
		Shared = shared;
	}
	public SearchData(String date, String platform, long totalSearched, long viewed, long shared) {
		super();
		Date = date;
		Platform = platform;
		Searched = totalSearched;
		Viewed = viewed;
		Shared = shared;
	}	
}
