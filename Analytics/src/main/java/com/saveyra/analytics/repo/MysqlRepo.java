package com.saveyra.analytics.repo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.sql.DataSource;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.saveyra.analytics.avatarHelper.DeviceToken;
import com.saveyra.analytics.data.DeviceFirstOpenCountData;
import com.saveyra.analytics.data.DeviceFirstOpenResultData;
import com.saveyra.analytics.model.AuditTrail;
import com.saveyra.analytics.model.NotificationCenter;
import com.saveyra.analytics.utility.ConvertTimezoneNameToOffset;
import com.saveyra.analytics.utility.Declaration;

@Repository
public class MysqlRepo {

	private static final Logger logger = LoggerFactory.getLogger(MysqlRepo.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	DataSource dataSource;

	// ############################ REPORTS ################################
	
	@Transactional(readOnly = true)
	public ArrayList<String> findGenderLanguageByAppId(String appId) {
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		ArrayList<String> appIdList = new ArrayList<String>();
		//int count = 0;
		try {
			conn = dataSource.getConnection();
			query = "SELECT gender,language FROM smg_saveyra_api_user_settings where applicationId=? order by lastModified desc limit 1";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, appId);			
			rs = pstmt.executeQuery();
			if(rs.next()) {			
				appIdList.add((rs.getString(1) == null||rs.getString(1).equals(""))?"Male":rs.getString(1));
				appIdList.add((rs.getString(2) == null||rs.getString(2).equals(""))?"English":rs.getString(2));
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		return appIdList;
	}
		

	@Transactional(readOnly = true)
	public ArrayList<String> findUniqueLanguages() {
		logger.info("[ENTER] findUniqueLanguages");
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		ArrayList<String> uqList = new ArrayList<String>();

		try {
			conn = dataSource.getConnection();
			query = "SELECT distinct(language) FROM smg_saveyra_api_user_settings";
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				uqList.add(rs.getString(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] findUniqueLanguages");
		return uqList;
	}
	
	
	//SELECT ip FROM smg_saveyra_api_user_ip where applicationId='65202598-237f-454d-bb94-7404115acf9c';
	@Transactional(readOnly = true)
	public String findIP(String AppId) {
		logger.info("[ENTER] findIP");
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		String uqList = "";

		try {
			conn = dataSource.getConnection();
			query = "SELECT ip FROM smg_saveyra_api_user_ip where applicationId=?";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, AppId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				uqList= rs.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] findIP");
		return uqList;
	}
	

	@Transactional(readOnly = true)
	public List<NotificationCenter> getNotificationCenter(String language, String timezone) {
		logger.info("[ENTER] getNotificationCenter");
		List<NotificationCenter> notList = new ArrayList<NotificationCenter>();
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		try {
			conn = dataSource.getConnection();
			// Data does insert with schedule time in Asia/Kolkata timezone and database timezone is in UTC. When we do compare schedule time(Asia/Kolkata) with 
			// mysql native method current_timestamp(), we get unexpected results. 
			//To fix unexpected results, we need to convert inserted schedule date and time, expiry date from Asia/Kolkata  to client timezone and then convert it 
			//to database UTC timezone
		
			//get offset by timezone name
			String defaultOffset = ConvertTimezoneNameToOffset.getOffsetByTimeZoneName("Asia/Kolkata");
			String utcOffset = "+00:00";
			String clientOffset = ConvertTimezoneNameToOffset.getOffsetByTimeZoneName(timezone);
			
			//Schedule date and time
			String converInsertTimezoneToClientTimezoneForScheduleDateAndTime = "convert_tz(STR_TO_DATE(concat(scheduledate,' ',scheduletime),'%Y-%m-%d %H:%i:%s'),'"+defaultOffset+"', '"+clientOffset+"')";
			String convertClientTimezoneToUTCForScheduleDateAndTime = "convert_tz("+converInsertTimezoneToClientTimezoneForScheduleDateAndTime+", '"+clientOffset+"','"+ utcOffset+"')";
		
			//expire date
			String converInsertTimezoneToClientTimezoneForExpiryDate = "convert_tz(date(expirydate), '"+defaultOffset+"','"+clientOffset+"')";
			String convertClientTimezoneToUTCForExpiryDate = "convert_tz("+converInsertTimezoneToClientTimezoneForExpiryDate+", '"+clientOffset+"','"+ utcOffset+"')";
			
			query = "SELECT id,notificationtitle,message,navigation_to,thumbnail,image_id,"+converInsertTimezoneToClientTimezoneForScheduleDateAndTime+" as createdDate, sticker_category FROM smg_saveyra_notification where "+convertClientTimezoneToUTCForExpiryDate+" >=  CURDATE() and "+convertClientTimezoneToUTCForScheduleDateAndTime+" <= current_timestamp() and notificationdelivery like '%NC%' and language=? order by createdDate desc";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, language);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String thumbnail = rs.getString(5) == null ? "" : rs.getString(5);
				String imageId = rs.getString(6) == null ? "" : rs.getString(6);
				String stringDate = rs.getString(7).split("\\.")[0];
				logger.info("formatDate {}",stringDate);
				notList.add(new NotificationCenter(rs.getInt(1), rs.getString(2).trim(), rs.getString(3).trim(),
						rs.getString(4).trim(), thumbnail, imageId, stringDate, false, rs.getString(8)));
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				logger.error("[ERROR] in finally block while closing connection {}",e);
			}
		}
		logger.info("[LEAVE] getNotificationCenter");
		return notList;
	}

	// Get Language / Gender Report
	@Transactional(readOnly = true)
	public Integer findSettingsGenderCount(String dateStart, String columnCheck) {
		logger.info("[ENTER] findSettingsGenderCount");
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		Integer returnVal = 0;
		try {
			conn = dataSource.getConnection();
			if (columnCheck.equalsIgnoreCase("Male")) {
				query = "SELECT Count(id) FROM smg_saveyra_api_user_settings where (date(createdOn) = ? or date(lastModified) = ?) and (gender is null or gender in('','Male')) and applicationId !=''";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, dateStart);
				pstmt.setString(2, dateStart);
			} else {
				query = "SELECT Count(id) FROM smg_saveyra_api_user_settings where (date(createdOn) = ? or date(lastModified) = ?) and gender = ? and applicationId !=''";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, dateStart);
				pstmt.setString(2, dateStart);
				pstmt.setString(3, columnCheck);
			}

			rs = pstmt.executeQuery();
			if (rs.next()) {
				returnVal = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] findSettingsGenderCount");
		return returnVal;
	}

	// Get Language / Gender Report
	@Transactional(readOnly = true)
	public Integer findSettingsLanguageCount(String dateStart, String columnCheck) {
		logger.info("[ENTER] findSettingsLanguageCount");
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		Integer returnVal = 0;
		try {
			conn = dataSource.getConnection();

			if (columnCheck.equalsIgnoreCase("English")) {
				query = "SELECT Count(id) FROM smg_saveyra_api_user_settings where (date(createdOn) = ? or date(lastModified) = ?) and (language is null or language in('','English')) and applicationId !=''";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, dateStart);
				pstmt.setString(2, dateStart);
			} else {
				query = "SELECT Count(id) FROM smg_saveyra_api_user_settings where (date(createdOn) = ? or date(lastModified) = ?) and language = ? and applicationId !=''";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, dateStart);
				pstmt.setString(2, dateStart);
				pstmt.setString(3, columnCheck);

			}
			rs = pstmt.executeQuery();
			if (rs.next()) {
				returnVal = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] findSettingsLanguageCount");
		return returnVal;
	}

	// Get Notification Object by Date
	@Transactional(readOnly = true)
	public HashMap<Integer, List<String>> findNotificationDetailsBydate(String dateStart) {
		logger.info("[ENTER] findNotificationDetailsBydate");
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		HashMap<Integer, List<String>> notMap = new HashMap<Integer, List<String>>();
		try {
			conn = dataSource.getConnection();
			query = "SELECT id,notificationcategory,message,language FROM smg_saveyra_notification where scheduledate = ?";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, dateStart);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				List<String> ls = new ArrayList<String>();
				ls.add(rs.getString(2));
				ls.add(rs.getString(3));
				ls.add(rs.getString(4));
				notMap.put(rs.getInt(1), ls);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] findNotificationDetailsBydate");
		return notMap;
	}

	// Get Notification Sent NId by Date

	// Get Notification Sent Count by n_id
	@Transactional(readOnly = true)
	public Integer findNotificationSentDetailsByNid(Integer nId) {
		logger.info("[ENTER] findNotificationSentDetailsByNid "+nId);
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		Integer count = 0;
		try {
			conn = dataSource.getConnection();
			query = "SELECT count(id) FROM smg_saveyra_notification_sent where n_id =? and status = 'Success'";
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, nId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] findNotificationSentDetailsByNid "+count);
		return count;
	}

	@Transactional(readOnly = true)
	public List<String> findNewUsersListByDate(String dateStart, String platform) {
		logger.info("[ENTER] findNewUsersCountByDate");
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		List<String> newUserList = new ArrayList<String>();
		try {
			conn = dataSource.getConnection();
			if (platform.equalsIgnoreCase("All")) {
				query = "SELECT applicationId FROM smg_saveyra_api_user_device_premises where date(createdOn) = date(?) and applicationId not in (?);";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, dateStart);
				pstmt.setString(2, Declaration.getExclusion_list());
			} else {
				query = "SELECT applicationId FROM smg_saveyra_api_user_device_premises where date(createdOn) = date(?) and platform = ? and applicationID not in (?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, dateStart);
				pstmt.setString(2, platform);
				pstmt.setString(3, Declaration.getExclusion_list());
			}
			rs = pstmt.executeQuery();
			while (rs.next()) {
				if (rs.getString("applicationId") != null && !(rs.getString("applicationId").equals(""))) {
					newUserList.add(rs.getString("applicationId"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] findNewUsersCountByDate");
		return newUserList;
	}
	
	@Transactional(readOnly = true)
	public ArrayList<String> findNewUsersAppIDBetweenDate(String startDate,String endDate, String platform) {
		logger.info("[ENTER] findNewUsersAppIDByDate");
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		ArrayList<String> appIdList = new ArrayList<String>();
		int count = 0;
		try {
			conn = dataSource.getConnection();
			if (platform.equalsIgnoreCase("All")) {
				query = "SELECT applicationId FROM smg_saveyra_api_user_device_premises where timestamp(createdOn)>= timestamp(?) and timestamp(createdOn)<= timestamp(?)and applicationID not in (?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, startDate);
				pstmt.setString(2, endDate);
				pstmt.setString(3, Declaration.getExclusion_list());
			} else {
				query = "SELECT applicationId FROM smg_saveyra_api_user_device_premises where timestamp(createdOn)>= timestamp(?) and timestamp(createdOn)<= timestamp(?) and platform = ? and applicationID not in (?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, startDate);
				pstmt.setString(2, endDate);
				pstmt.setString(3, platform);
				pstmt.setString(4, Declaration.getExclusion_list());
			}
			rs = pstmt.executeQuery();
			while (rs.next()) {
				appIdList.add(rs.getString(1).trim());
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] findNewUsersAppIDByDate");
		return appIdList;
	}
	
	@Transactional(readOnly = true)
	public ArrayList<String> findNewUsersAppIDByDate(String date, String platform) {
		logger.info("[ENTER] findNewUsersAppIDByDate");
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		ArrayList<String> appIdList = new ArrayList<String>();
		int count = 0;
		try {
			conn = dataSource.getConnection();
			if (platform.equalsIgnoreCase("All")) {
				query = "SELECT applicationId FROM smg_saveyra_api_user_device_premises where date(createdOn)= date(?) and applicationID not in (?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, date);
				pstmt.setString(2, Declaration.getExclusion_list());
			} else {
				query = "SELECT applicationId FROM smg_saveyra_api_user_device_premises where date(createdOn)= date(?) and platform = ? and applicationID not in (?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, date);
				pstmt.setString(2, platform);
				pstmt.setString(3, Declaration.getExclusion_list());
			}
			rs = pstmt.executeQuery();
			while (rs.next()) {
				appIdList.add(rs.getString(1).trim());
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] findNewUsersAppIDByDate");
		return appIdList;
	}
	

	@Transactional(readOnly = true)
	public Integer findNewUsersCountByDate(String date, String platform) {
		logger.info("[ENTER] findNewUsersCountByDate");
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		int count = 0;
		try {
			conn = dataSource.getConnection();
			if (platform.equalsIgnoreCase("All")) {
				query = "SELECT count(id) FROM smg_saveyra_api_user_device_premises where date(createdOn)= date(?) and applicationID not in (?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, date);
				pstmt.setString(2, Declaration.getExclusion_list());
			} else {
				query = "SELECT count(id) FROM smg_saveyra_api_user_device_premises where date(createdOn)= date(?) and platform = ? and applicationID not in (?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, date);
				pstmt.setString(2, platform);
				pstmt.setString(3, Declaration.getExclusion_list());
			}
			rs = pstmt.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] findNewUsersCountByDate");
		return count;
	}

	@Transactional(readOnly = true)
	public Integer findMobileUsersByDate(String platform, String Date) {
		logger.info("[ENTER] findFBUsersByDate");
		if (Date == null) {
			logger.error("[ERROR] Error in Request Date");
			return 0;
		}
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		int numberOfRows = 0;
		try {
			conn = dataSource.getConnection();
			if (platform.equalsIgnoreCase("All")) {
				query = "SELECT count(*) FROM smg_saveyra_api_mobile_user_profile MU inner join smg_saveyra_api_user_device_premises DP on MU.applicationId=DP.applicationId where MU.createdOn between ? and ? and MU.applicationID not in (?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, Date);
				pstmt.setString(2, getNextDate(Date));
				pstmt.setString(3, Declaration.getExclusion_list());
			} else {
				query = "SELECT count(*) FROM smg_saveyra_api_mobile_user_profile MU inner join smg_saveyra_api_user_device_premises DP on MU.applicationId=DP.applicationId where DP.platform = ? and MU.createdOn between ? and ? and MU.applicationID not in (?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, platform);
				pstmt.setString(2, Date);
				pstmt.setString(3, getNextDate(Date));
				pstmt.setString(4, Declaration.getExclusion_list());
			}
			rs = pstmt.executeQuery();
			if (rs.next()) {
				numberOfRows = rs.getInt(1);
				// System.out.println("numberOfRows= " + numberOfRows);
			} else {
				logger.error("[ERROR] SQL Select Fail");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] findFBUsersByDate");
		return numberOfRows;
	}

	@Transactional(readOnly = true)
	public Integer findFBUsersByDate(String platform, String Date) {
		logger.info("[ENTER] findFBUsersByDate");
		if (Date == null) {
			logger.error("[ERROR] Error in Request Date");
			return 0;
		}
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		int numberOfRows = 0;
		try {
			conn = dataSource.getConnection();
			if (platform.equalsIgnoreCase("All")) {
				query = "SELECT count(*) FROM smg_saveyra_api_facebook_user_profile MU inner join smg_saveyra_api_user_device_premises DP on MU.applicationId=DP.applicationId where MU.createdOn between ? and ? and MU.applicationID not in (?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, Date);
				pstmt.setString(2, getNextDate(Date));
				pstmt.setString(3, Declaration.getExclusion_list());
			} else {
				query = "SELECT count(*) FROM smg_saveyra_api_facebook_user_profile MU inner join smg_saveyra_api_user_device_premises DP on MU.applicationId=DP.applicationId where DP.platform = ? and MU.createdOn between ? and ? and MU.applicationID not in (?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, platform);
				pstmt.setString(2, Date);
				pstmt.setString(3, getNextDate(Date));
				pstmt.setString(4, Declaration.getExclusion_list());
			}
			rs = pstmt.executeQuery();
			if (rs.next()) {
				numberOfRows = rs.getInt(1);
				// System.out.println("numberOfRows= " + numberOfRows);
			} else {
				logger.error("[ERROR] SQL Select Fail");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] findFBUsersByDate");
		return numberOfRows;
	}

	// ############################ REPORTS END ################################

	@Transactional(readOnly = true)
	public List<DeviceToken> findNewUsersByDate(String date) {
		logger.info("[ENTER] findFBUsersByDate");
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		List<DeviceToken> deviceTokenList = new ArrayList<DeviceToken>();
		try {
			conn = dataSource.getConnection();
			query = "SELECT deviceToken FROM smg_saveyra_api_user_device_premises where date(createdOn)= ? and deviceToken != '' and applicationID not in (?)";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, date);
			pstmt.setString(2, Declaration.getExclusion_list());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				deviceTokenList.add(new DeviceToken(rs.getString(1)));
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] findFBUsersByDate");
		return deviceTokenList;
	}

	public String getNextDate(String curDate) {
		String nextDate = "";
		try {
			Calendar today = Calendar.getInstance();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date date = format.parse(curDate);
			today.setTime(date);
			today.add(Calendar.DAY_OF_YEAR, 1);
			nextDate = format.format(today.getTime());
		} catch (Exception e) {
			return nextDate;
		}
		return nextDate;
	}

	public void insertRecord(String imageId, String tokens, String imagePath, String imageSizeFormat,
			String imageExtension, Date createdon, String status, Date expiry, String imageType, Integer paidStickers,
			Date lastModified, String caption, Date occassionActiveStartDate, Date occassionDate, String price,
			String category, String keywords, String subcategory, String productId) {
		logger.info("[ENTER] insertRecord");
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dataSource.getConnection();
			String query = "INSERT INTO smg_saveyra_stickers_metadata(imageId,tokens,imagePath,imageSizeFormat,imageExtension,createdon,status,productId,expiry,imageType,paidStickers,lastModified,caption,occasionActiveStartDate,occasionDate,price,category,keywords,subcategory) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, imageId);
			pstmt.setString(2, tokens);
			pstmt.setString(3, imagePath);
			pstmt.setString(4, imageSizeFormat);
			pstmt.setString(5, imageExtension);
			if (createdon == null) {
				pstmt.setDate(6, null);
			} else {
				pstmt.setDate(6, new java.sql.Date(createdon.getTime()));
			}
			pstmt.setString(7, status);
			pstmt.setString(8, productId);
			if (expiry == null) {
				pstmt.setDate(9, null);
			} else {
				pstmt.setDate(9, new java.sql.Date(expiry.getTime()));
			}
			pstmt.setString(10, imageType);
			pstmt.setInt(11, paidStickers);
			if (lastModified == null) {
				pstmt.setDate(12, null);
			} else {
				pstmt.setDate(12, new java.sql.Date(lastModified.getTime()));
			}
			pstmt.setString(13, caption);
			if (occassionActiveStartDate == null) {
				pstmt.setDate(14, null);
			} else {
				pstmt.setDate(14, new java.sql.Date(occassionActiveStartDate.getTime()));
			}

			if (occassionActiveStartDate == null) {
				pstmt.setDate(15, null);
			} else {
				pstmt.setDate(15, new java.sql.Date(occassionDate.getTime()));
			}
			pstmt.setString(16, price);
			pstmt.setString(17, category);
			pstmt.setString(18, keywords);
			pstmt.setString(19, subcategory);
			// execute insert SQL stetement
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Insert Fail");
		} finally {
			try {
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] insertRecord");
	}

	public void updateRecord(String imageId, String tokens, String imagePath, String imageSizeFormat,
			String imageExtension, Date createdon, String status, Date expiry, String imageType, Integer paidStickers,
			Date lastModified, String caption, Date occassionActiveStartDate, Date occassionDate, String price,
			String category, String keywords, String subcategory, String productId) {
		logger.info("[ENTER] updateRecord");
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dataSource.getConnection();
			String query = "UPDATE smg_saveyra_stickers_metadata SET tokens = ?,imagePath = ?,imageSizeFormat = ?,imageExtension = ?,createdon = ?,status = ?, productId=? ,expiry = ?,imageType = ?,paidStickers = ?,lastModified = ?,caption = ?,occasionActiveStartDate = ?,occasionDate = ?,price = ?,category = ?,keywords = ?,subcategory = ? WHERE imageId = ?";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, tokens);
			pstmt.setString(2, imagePath);
			pstmt.setString(3, imageSizeFormat);
			pstmt.setString(4, imageExtension);
			if (createdon == null) {
				pstmt.setDate(5, null);
			} else {
				pstmt.setDate(5, new java.sql.Date(createdon.getTime()));
			}
			pstmt.setString(6, status);

			pstmt.setString(7, productId);
			if (expiry == null) {
				pstmt.setDate(8, null);
			} else {
				pstmt.setDate(8, new java.sql.Date(expiry.getTime()));
			}
			pstmt.setString(9, imageType);
			pstmt.setInt(10, paidStickers);
			if (lastModified == null) {
				pstmt.setDate(11, null);
			} else {
				pstmt.setDate(11, new java.sql.Date(lastModified.getTime()));
			}
			pstmt.setString(12, caption);
			if (occassionActiveStartDate == null) {
				pstmt.setDate(13, null);
			} else {
				pstmt.setDate(13, new java.sql.Date(occassionActiveStartDate.getTime()));
			}

			if (occassionDate == null) {
				pstmt.setDate(14, null);
			} else {
				pstmt.setDate(14, new java.sql.Date(occassionDate.getTime()));
			}
			pstmt.setString(15, price);
			pstmt.setString(16, category);
			pstmt.setString(17, keywords);
			pstmt.setString(18, subcategory);
			pstmt.setString(19, imageId);
			// execute insert SQL stetement
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] updateRecord");
	}

	public void deleteRecord(String imageId) {
		logger.info("[ENTER] updateRecord");
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dataSource.getConnection();
			String query = "DELETE FROM smg_saveyra_stickers_metadata WHERE imageId = ?";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, imageId);
			// execute insert SQL statement
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] updateRecord");
	}

	@Transactional(readOnly = true)
	public List<String> findAllUsers(String Date) {
		logger.info("[ENTER] findAllUsers");

		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		List<String> appIdList = new ArrayList<String>();

		try {
			conn = dataSource.getConnection();
			query = "SELECT applicationId FROM smg_saveyra_api_user_device_premises where createdOn < ? and applicationID not in (?)";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, Date);
			pstmt.setString(2, Declaration.getExclusion_list());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				appIdList.add(rs.getString("applicationId"));

			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] findAllUsers");
		return appIdList;
	}

	@Transactional(readOnly = true)
	public long findAllUsersCountByDate(String Date, String platform) {
		logger.info("[ENTER] findAllUsersCountByDate");

		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		// List<String> appIdList = new ArrayList<String>();
		long numberOfRows = 0;
		try {
			conn = dataSource.getConnection();
			if (platform.equalsIgnoreCase("All")) {
				query = "SELECT COUNT(applicationId) FROM smg_saveyra_api_user_device_premises where date(createdOn) = ? and applicationID not in (?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, Date);
				pstmt.setString(2, Declaration.getExclusion_list());
			} else {
				query = "SELECT COUNT(applicationId) FROM smg_saveyra_api_user_device_premises where date(createdOn) = ? and platform = ? and applicationID not in (?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, Date);
				pstmt.setString(2, platform);
				pstmt.setString(3, Declaration.getExclusion_list());
			}
			rs = pstmt.executeQuery();
			while (rs.next()) {
				numberOfRows = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] findAllUsersCountByDate");
		return numberOfRows;
	}

	@Transactional(readOnly = true)
	public List<String> findAvatarCreatedAppIDListByDate(String Date, String gender) {
		logger.info("[ENTER] findAvatarCreatedAppIDListByDate");

		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		List<String> appIdList = new ArrayList<String>();
		// long numberOfRows = 0;
		try {
			conn = dataSource.getConnection();
			if (gender.equalsIgnoreCase("male")) {
				query = "SELECT applicationId FROM smg_saveyra_api_user_avatar where LOWER(imageSet) LIKE '%\"male\"%' and date(createdOn) = ? and applicationID not in (?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, Date);
				pstmt.setString(2, Declaration.getExclusion_list());
			} else if (gender.equalsIgnoreCase("female")) {
				query = "SELECT applicationId FROM smg_saveyra_api_user_avatar where LOWER(imageSet) LIKE '%\"female\"%' and date(createdOn) = ? and applicationID not in (?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, Date);
				pstmt.setString(2, Declaration.getExclusion_list());
			}
			if (pstmt != null) {
				rs = pstmt.executeQuery();
				while (rs.next()) {
					appIdList.add(rs.getString("applicationId"));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] findAvatarCreatedCountByDate");
		return appIdList;
	}

	@Transactional(readOnly = true)
	public boolean isAppIDAndroid(String AppID) {
		logger.info("[ENTER] findAvatarCreatedCountByDate");
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		long numberOfRows = 0;
		try {
			conn = dataSource.getConnection();
			query = "SELECT COUNT(*) FROM smg_saveyra_api_user_device_premises where applicationId = '?' and platform = 'Android' and applicationID not in (?)";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, AppID);
			pstmt.setString(2, Declaration.getExclusion_list());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				numberOfRows = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] findAvatarCreatedCountByDate");
		if (numberOfRows > 0) {
			return true;
		}
		return false;
	}

	@Transactional(readOnly = true)
	public long findAvatarCreatedCountByDate(String Date, String platform) {
		logger.info("[ENTER] findAvatarCreatedAppIDListByDate");

		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		// List<String> appIdList = new ArrayList<String>();
		long numberOfRows = 0;
		try {
			conn = dataSource.getConnection();
			if (platform.equalsIgnoreCase("Android")) {
				query = "SELECT COUNT(*) FROM smg_saveyra_api_user_avatar INNER JOIN smg_saveyra_api_user_device_premises ON smg_saveyra_api_user_avatar.applicationId = smg_saveyra_api_user_device_premises.applicationId where smg_saveyra_api_user_device_premises.platform = ? and date(smg_saveyra_api_user_avatar.createdOn) = ? and smg_saveyra_api_user_avatar.applicationID not in (?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, "Android");
				pstmt.setString(2, Date);
				pstmt.setString(3, Declaration.getExclusion_list());
			} else if (platform.equalsIgnoreCase("iOS")) {
				query = "SELECT COUNT(*) FROM smg_saveyra_api_user_avatar INNER JOIN smg_saveyra_api_user_device_premises ON smg_saveyra_api_user_avatar.applicationId = smg_saveyra_api_user_device_premises.applicationId where smg_saveyra_api_user_device_premises.platform = ? and date(smg_saveyra_api_user_avatar.createdOn) = ? and smg_saveyra_api_user_avatar.applicationID not in (?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, "iOS");
				pstmt.setString(2, Date);
				pstmt.setString(3, Declaration.getExclusion_list());
			} else {
				query = "SELECT COUNT(*) FROM smg_saveyra_api_user_avatar INNER JOIN smg_saveyra_api_user_device_premises ON smg_saveyra_api_user_avatar.applicationId = smg_saveyra_api_user_device_premises.applicationId where date(smg_saveyra_api_user_avatar.createdOn) = ? and smg_saveyra_api_user_avatar.applicationID not in (?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, Date);
				pstmt.setString(2, Declaration.getExclusion_list());
			}
			rs = pstmt.executeQuery();

			while (rs.next()) {
				numberOfRows = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] findAvatarCreatedCountByDate");
		return numberOfRows;
	}

	/**
	 * This method gives total count of both fingerPrint and nonFingerPrint,
	 * fingerPrint and nonFingerPrint. This is counted by using manufacturing
	 * details and find out fingerPrint field in manufacturing details. if device
	 * name is found in fingerPrint field, get device name (3) (for ex:- /1/2/3:1)
	 * else considered it as non finger print.
	 * 
	 * @param startDate
	 * @param endDate
	 * @return DeviceFirstOpenResultData
	 */
	@Transactional(readOnly = true)
	public DeviceFirstOpenResultData getFirstOpenCountByDateRange(String startDate, String endDate) {
		logger.info("[ENTER] getFirstOpenCountByDateRange");
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		Map<String, Map<String, Integer>> createdDateAndCount = new HashMap<String, Map<String, Integer>>();
		Map<String, Integer> createdDateAndmissingCount = new HashMap<String, Integer>();
		Map<String, Integer> deviceFoundAndCount = null;
		DeviceFirstOpenResultData firstOpenCount = new DeviceFirstOpenResultData();
		try {
			conn = dataSource.getConnection();
			query = "SELECT manufacturingDetails, date(createdOn) as createdDate FROM smg_saveyra_api_user_device_premises where date(createdOn)>=? and date(createdOn)<=?";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, startDate);
			pstmt.setString(2, endDate);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				int missingCount = 0;
				String manufacturingDetatailsString = rs.getString("manufacturingDetails");
				String createdDate = rs.getString("createdDate");
				if (createdDate != null) {
					if (createdDateAndCount.get(createdDate) == null) {
						deviceFoundAndCount = new HashMap<String, Integer>();
						createdDateAndCount.put(createdDate, deviceFoundAndCount);
					} else {
						createdDateAndCount.put(createdDate, deviceFoundAndCount);
					}

					if (createdDateAndmissingCount.get(createdDate) == null) {
						createdDateAndmissingCount.put(createdDate, missingCount);
					} else {
						missingCount = createdDateAndmissingCount.get(createdDate);
					}
				}

				if (manufacturingDetatailsString != null && manufacturingDetatailsString.length() > 0) {
					if (manufacturingDetatailsString.contains("{") && !manufacturingDetatailsString.contains("[")) {
						JSONParser parser = new JSONParser();
						JSONObject jsonObject = (JSONObject) parser.parse(manufacturingDetatailsString);
						String _fingerPrint = "";
						if (jsonObject.get("_fingerPrint") != null) {
							_fingerPrint = jsonObject.get("_fingerPrint").toString();
						} else if (jsonObject.get("fingerPrint") != null) {
							_fingerPrint = jsonObject.get("fingerPrint").toString();
						}
						if (_fingerPrint.contains("/")) {
							String colonSplits[] = _fingerPrint.split(":");
							_fingerPrint = colonSplits[0];
							String slashSplits[] = _fingerPrint.split("/");
							_fingerPrint = slashSplits[2];
							deviceFoundAndCount = createdDateAndCount.get(createdDate);
							if (deviceFoundAndCount.get(_fingerPrint) == null) {
								deviceFoundAndCount.put(_fingerPrint, 1);
							} else {
								deviceFoundAndCount.put(_fingerPrint, deviceFoundAndCount.get(_fingerPrint) + 1);
							}
						} else {
							createdDateAndmissingCount.put(createdDate, missingCount + 1);
						}

					} else {
						createdDateAndmissingCount.put(createdDate, missingCount + 1);
					}
				} else {
					createdDateAndmissingCount.put(createdDate, missingCount + 1);
				}
			}

			long fingerPrintCount = 0l;
			long nonFingerPrintCount = 0l;
			long totalCount = 0l;
			for (String date : createdDateAndCount.keySet()) {
				if (createdDateAndCount.get(date) != null) {
					for (String deviceName : createdDateAndCount.get(date).keySet()) {
						fingerPrintCount = fingerPrintCount + createdDateAndCount.get(date).get(deviceName);
					}
				}
			}
			for (String date : createdDateAndmissingCount.keySet()) {
				nonFingerPrintCount = nonFingerPrintCount + createdDateAndmissingCount.get(date);
			}
			totalCount = fingerPrintCount + nonFingerPrintCount;
			// if manufacturingDetails is in json then considered as fingerPrint and it is
			// count
			firstOpenCount.setFingerPrintCount(fingerPrintCount);
			// if manufacturingDetails is not in json then considered as non fingerPrint and
			// it is count
			firstOpenCount.setNonFingerPrintCount(nonFingerPrintCount);
			// Total count
			firstOpenCount.setTotalCount(totalCount);
			firstOpenCount.setFingerPrint(createdDateAndCount);
			firstOpenCount.setNonFingerPrint(createdDateAndmissingCount);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] getFirstOpenCountByDateRange");
		return firstOpenCount;
	}

	@Transactional(readOnly = true)
	public DeviceFirstOpenCountData getOpenCountWithoutDeviceNameByDateRange(String startDate, String endDate) {
		logger.info("[ENTER] getOpenCountWithoutDeviceNameByDateRange");
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		Map<String, Map<String, Integer>> createdDateAndCount = new HashMap<String, Map<String, Integer>>();
		TreeMap<String, Integer> nonFingerPrintCount = new TreeMap<String, Integer>();
		Map<String, Integer> deviceFoundAndCount = null;
		DeviceFirstOpenCountData firstOpenCount = new DeviceFirstOpenCountData();
		try {
			conn = dataSource.getConnection();
			query = "SELECT manufacturingDetails, date(createdOn) as createdDate FROM smg_saveyra_api_user_device_premises where date(createdOn)>=? and date(createdOn)<=?";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, startDate);
			pstmt.setString(2, endDate);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				int missingCount = 0;
				String manufacturingDetatailsString = rs.getString("manufacturingDetails");
				String createdDate = rs.getString("createdDate");
				if (createdDate != null) {
					if (createdDateAndCount.get(createdDate) == null) {
						deviceFoundAndCount = new HashMap<String, Integer>();
						createdDateAndCount.put(createdDate, deviceFoundAndCount);
					} else {
						createdDateAndCount.put(createdDate, deviceFoundAndCount);
					}

					if (nonFingerPrintCount.get(createdDate) == null) {
						nonFingerPrintCount.put(createdDate, missingCount);
					} else {
						missingCount = nonFingerPrintCount.get(createdDate);
					}
				}

				if (manufacturingDetatailsString != null && manufacturingDetatailsString.length() > 0) {
					if (manufacturingDetatailsString.contains("{") && !manufacturingDetatailsString.contains("[")) {
						JSONParser parser = new JSONParser();
						JSONObject jsonObject = (JSONObject) parser.parse(manufacturingDetatailsString);
						String _fingerPrint = "";
						if (jsonObject.get("_fingerPrint") != null) {
							_fingerPrint = jsonObject.get("_fingerPrint").toString();
						} else if (jsonObject.get("fingerPrint") != null) {
							_fingerPrint = jsonObject.get("fingerPrint").toString();
						}
						if (_fingerPrint.contains("/")) {
							String colonSplits[] = _fingerPrint.split(":");
							_fingerPrint = colonSplits[0];
							String slashSplits[] = _fingerPrint.split("/");
							_fingerPrint = slashSplits[2];
							deviceFoundAndCount = createdDateAndCount.get(createdDate);
							if (deviceFoundAndCount.get(_fingerPrint) == null) {
								deviceFoundAndCount.put(_fingerPrint, 1);
							} else {
								deviceFoundAndCount.put(_fingerPrint, deviceFoundAndCount.get(_fingerPrint) + 1);
							}
						} else {
							nonFingerPrintCount.put(createdDate, missingCount + 1);
						}

					} else {
						nonFingerPrintCount.put(createdDate, missingCount + 1);
					}
				} else {
					nonFingerPrintCount.put(createdDate, missingCount + 1);
				}
			}

			long fingerPrintCount = 0l;
			TreeMap<String, Integer> fingerPrint = new TreeMap<String, Integer>();
			for (String date : createdDateAndCount.keySet()) {
				if (createdDateAndCount.get(date) != null) {
					for (String deviceName : createdDateAndCount.get(date).keySet()) {
						fingerPrintCount = fingerPrintCount + createdDateAndCount.get(date).get(deviceName);
						int count = (createdDateAndCount.get(date).get(deviceName) != null)
								? createdDateAndCount.get(date).get(deviceName)
								: 0;
						if (fingerPrint.get(date) == null) {
							fingerPrint.put(date, count);
						} else {
							fingerPrint.put(date, fingerPrint.get(date) + count);
						}
					}
				}
			}
			firstOpenCount.setFingerPrint(fillMissingDaysByDateRange(startDate, endDate, fingerPrint));
			firstOpenCount.setNonFingerPrint(fillMissingDaysByDateRange(startDate, endDate, nonFingerPrintCount));

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Update Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] getOpenCountWithoutDeviceNameByDateRange");
		return firstOpenCount;
	}

	private TreeMap<String, Integer> fillMissingDaysByDateRange(String startDate, String endDate,
			TreeMap<String, Integer> firstOpenCount) {
		logger.info("[Entered] fillMissingDaysByDateRange");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = new GregorianCalendar();
		try {
			Date start = sdf.parse(startDate);
			Date end = sdf.parse(endDate);
			calendar.setTime(start);
			while (calendar.getTime().getTime() <= end.getTime()) {
				String result = sdf.format(calendar.getTime());
				if (firstOpenCount.get(result) == null) {
					firstOpenCount.put(result, 0);
				}
				calendar.add(Calendar.DATE, 1);
			}
		} catch (ParseException e) {
			e.printStackTrace();
			logger.error("[Error] fillMissingDaysByDateRange=> {}", e.getMessage(), e);
		}
		logger.info("[Exit] fillMissingDaysByDateRange");
		return firstOpenCount;
	}

	@Transactional(readOnly = true)
	public String getTypeForNid(String nid) {
		logger.info("[ENTER] getTypeForNid " + nid);
		if (nid == null) {
			logger.error("[ERROR] Type Not Found");
			return null;
		}
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String type = null;
		try {
			conn = dataSource.getConnection();
			String query = null;
			query = "SELECT notificationtype FROM smg_saveyra_notification  where id = ? Limit 1";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, nid);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				type = rs.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Select Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] getTypeForNid");
		return type;
	}

	@Transactional(readOnly = true)
	public List<String> getNotificationCreatedCount(String date) {
		logger.info("[ENTER] getNotificationCreatedCount ");
		if (date == null) {
			logger.error("[ERROR] Type Not Found");
			return null;
		}
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		Integer notificationSentCount = 0;
		String displayString = "";
		List<String> returnString = new ArrayList<String>();
		try {
			int count = 0;
			String type = null;
			conn = dataSource.getConnection();
			String query = null;
			query = "SELECT count(*)as count,notificationtype FROM smg_saveyra_notification where date(created_at) = date(?) group by notificationtype";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, date);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				count = rs.getInt("count");
				type = rs.getString("notificationtype");
				if (type.equalsIgnoreCase("News")) {
					notificationSentCount += count;
					displayString += "N(" + count + ")";
				}
				if (type.equalsIgnoreCase("Promotions")) {
					notificationSentCount += count;
					displayString += "P(" + count + ")";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Select Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		try {
			conn = dataSource.getConnection();
			String query = null;
			int count = 0;
			String type = null;
			query = "SELECT count(*)as count,notificationcategory FROM smg_saveyra_notification_sent where date(created_at) = date(?) group by notificationcategory";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, date);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				count = rs.getInt("count");
				type = rs.getString("notificationcategory");
				if (type.equalsIgnoreCase("Avatar Completion Reminder")) {
					if (count > 0) {
						notificationSentCount += 1;
						displayString += "A(" + 1 + ")";
					}
				}
				if (type.equalsIgnoreCase("Avatar Completion Reminder")) {
					if (count > 0) {
						notificationSentCount += 1;
						displayString += "W(" + 1 + ")";
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Select Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		logger.info("[LEAVE] getNotificationCreatedCount");
		returnString.add(notificationSentCount + "");
		returnString.add(displayString);
		return returnString;
	}

	/*
	 * @Transactional(readOnly = true) public Integer
	 * getNotificationSentCountByType(String date , String type,String platform) {
	 * logger.info("[ENTER] getNotificationSentCountByType "+type); if (type ==
	 * null) { logger.error("[ERROR] Type Not Found"); return null; } ResultSet rs =
	 * null; Connection conn = null; PreparedStatement pstmt = null; int count = 0;
	 * try { conn = dataSource.getConnection(); String query = null;
	 * if(platform.equalsIgnoreCase("All")) { query =
	 * "SELECT count(id) FROM smg_saveyra_notification_sent where date(created_at) = ? and status ='success' and notificationtype = ?"
	 * ; pstmt = conn.prepareStatement(query); pstmt.setString(1, date);
	 * pstmt.setString(2, type); }else { query =
	 * "SELECT count(id) FROM smg_saveyra_notification_sent where date(created_at) = ? and status ='success' and notificationtype = ? and platform = ?"
	 * ; pstmt = conn.prepareStatement(query); pstmt.setString(1, date);
	 * pstmt.setString(2, type); pstmt.setString(3, platform); } rs =
	 * pstmt.executeQuery(); if (rs.next()) { count = rs.getInt(1); } } catch
	 * (Exception e) { e.printStackTrace(); logger.error("[ERROR] SQL Select Fail");
	 * } finally { try { rs.close(); pstmt.close(); conn.close(); } catch
	 * (SQLException e) { e.printStackTrace(); } }
	 * logger.info("[LEAVE] getNotificationSentCountByType"); return count; }
	 */

	@Transactional(readOnly = true)
	public Integer getNotificationSentCount(String date, String category, String platform) {
		logger.info("[ENTER] getNotificationSentCount " + category);
		if (category == null) {
			logger.error("[ERROR] Category Not Found");
			return null;
		}
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		int count = 0;
		try {
			conn = dataSource.getConnection();
			String query = null;
			if (platform.equalsIgnoreCase("All")) {
				query = "SELECT count(id) FROM smg_saveyra_notification_sent where date(created_at) = ? and status ='success' and notificationcategory = ?";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, date);
				pstmt.setString(2, category);
			} else {
				query = "SELECT count(id) FROM smg_saveyra_notification_sent where date(created_at) = ? and status ='success' and notificationcategory = ? and platform = ?";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, date);
				pstmt.setString(2, category);
				pstmt.setString(3, platform);
			}
			rs = pstmt.executeQuery();
			if (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Select Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] getNotificationSentCount");
		return count;
	}

	@Transactional(readOnly = true)
	public Integer getAllNotificationSentCount(String date, String platform) {
		logger.info("[ENTER] getallNotificationSentCount ");
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		int count = 0;
		try {
			conn = dataSource.getConnection();
			String query = null;
			if (platform.equalsIgnoreCase("All")) {
				query = "SELECT count(id) FROM smg_saveyra_notification_sent where date(created_at) = ? and status ='success'";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, date);
			} else {
				query = "SELECT count(id) FROM smg_saveyra_notification_sent where date(created_at) = ? and status ='success' and platform = ?";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, date);
				pstmt.setString(2, platform);
			}

			rs = pstmt.executeQuery();
			if (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Select Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] getallNotificationSentCount");
		return count;
	}

	@Transactional(readOnly = true)
	public Integer getAllNotificationCreatedCount(String date) {
		logger.info("[ENTER] getAllNotificationCreatedCount ");
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		int count = 0;
		try {
			conn = dataSource.getConnection();
			String query = "SELECT count(id) FROM smg_saveyra_notification where date(created_at)=?";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, date);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Select Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] getAllNotificationCreatedCount");
		return count;
	}

	public List<AuditTrail> getAllAuditTrailsByDateRangeForPlatform(String startDate, String endDate, String platform) {
		List<AuditTrail> auditTrails = new ArrayList<AuditTrail>();

		List<Map<String, Object>> rows = jdbcTemplate.queryForList(
				"SELECT * FROM saveyra_notification_sent where date(created_at)>=? and date(created_at)<=? and platform=?",
				startDate, endDate, platform);

		for (Map<String, Object> row : rows) {
			AuditTrail auditTrail = new AuditTrail();
			auditTrail.setId((int) row.get("id"));
			auditTrail.setApplicationId((String) row.get("application_id"));
			auditTrail.setDeviceToken((String) row.get("notification_id"));
			auditTrail.setPlatform((String) row.get("platform"));
			auditTrail.setNotificationType((String) row.get("notificationtype"));
			auditTrail.setStatus((String) row.get("status"));
			auditTrail.setMessageId((String) row.get("message_id"));
			auditTrail.setCreatedOn((Date) row.get("created_at"));
			auditTrails.add(auditTrail);
		}
		return auditTrails;

	}

	@Transactional(readOnly = true)
	public Map<String, List<List<String>>> getAvatarStickerDataByDate(String Date) {
		logger.info("[ENTER] getAvatarStickerDataByDate ");
		if (Date == null) {
			logger.error("[ERROR] Date Not Found");
			return null;
		}
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		Map<String, List<List<String>>> avatarDataMap = new HashMap<String, List<List<String>>>();
		List<List<String>> rows = new ArrayList<List<String>>();
		String currentAppId = "";
		try {
			conn = dataSource.getConnection();
			String query = null;
			query = "SELECT SAS.applicationId,imageId,imagePath,expression FROM smg_saveyra_api_avatar_stickers SAS inner join smg_saveyra_api_avatar_expression_category SAC on SAS.categoryId=SAC.categoryId where date(SAS.createdon) = date(?) order by SAS.applicationId";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, Date);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				// Create Container
				List<String> rowItems = new ArrayList<String>();
				// Add Current Values
				rowItems.add(rs.getString("applicationId"));
				rowItems.add(rs.getString("imageId"));
				rowItems.add(rs.getString("imagePath"));
				rowItems.add(rs.getString("expression"));
				// Check if
				if (currentAppId.equalsIgnoreCase(rs.getString("applicationId"))) {
					// Application ID matches
					rows.add(rowItems);
				} else {
					// New Application ID
					avatarDataMap.put(rs.getString("applicationId"), rows);
					rows.clear();
					rows.add(rowItems);
				}
				currentAppId = rs.getString("applicationId");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Select Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info("[LEAVE] getTypeForNid");
		return avatarDataMap;
	}
	
	/***
	 * This method takes applicationId as input and get latest time-zone of the applicationId.
	 * if database does not have time zone for the applicationId, returns default Asia/Kolkata time-zone
	 * 
	 * @param <appId> ex: 89cb99a0-84e0-4834-a791-2f91dceeb18f
	 * @return <time-zone> ex: Asia/Kolkata
	 */
	@Transactional(readOnly = true)
	public String findUserTimezoneByAppId(String appId) {
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		//default time-zone
		String timezone = "Asia/Kolkata";
		try {
			conn = dataSource.getConnection();
			query = "SELECT timezone FROM smg_saveyra_api_user_settings where applicationId=? order by lastModified desc limit 1";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, appId);			
			rs = pstmt.executeQuery();
			if(rs.next()) {
				if ((rs.getString(1) == null || rs.getString(1).equals(""))) {
					logger.info("Set default timezone {} as timezone is not there in DB", timezone);
					return timezone;
				} else {
					String dbTimezone = rs.getString(1).trim();
					String [] timezoneIDs = TimeZone.getAvailableIDs();
					List<String> timezoneNames = Arrays.asList(timezoneIDs);
					if (timezoneNames.contains(dbTimezone) == true) {
						logger.info("Timezone from DB {}", dbTimezone);
						return dbTimezone;
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (pstmt != null && !pstmt.isClosed()) {
					pstmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		logger.info("Set default timezone {} as timezone is invalid in DB", timezone);
		return timezone;
	}
}
