package com.saveyra.analytics.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class DataSourceConfig {
	
	@Autowired 
	private Environment env;
     
  /*  @Bean
    public DataSource getDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("com.mysql.jdbc.Driver");
        dataSourceBuilder.url("jdbc:mysql://"+env.getProperty("SAV_RDS_HOST")+":"+env.getProperty("SAV_RDS_PORT")+"/"+env.getProperty("SAV_RDS_DB")+"?useSSL=false&useUnicode=yes&characterEncoding=UTF-8");
        dataSourceBuilder.username(env.getProperty("SAV_RDS_USER"));
        dataSourceBuilder.password(env.getProperty("SAV_RDS_PASSWORD"));
        return dataSourceBuilder.build();
    }*/
}
