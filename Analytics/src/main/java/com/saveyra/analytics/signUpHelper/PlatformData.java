package com.saveyra.analytics.signUpHelper;

public class PlatformData {
	private String sPlatform;
	private long lFBCount;
	private long lMobCount;
	private long lTotalCount;

	public PlatformData(String name, long lFBCount, long lMobCount) {
		this.lFBCount = lFBCount;
		this.lMobCount = lMobCount;
		this.setsPlatform(name);
		this.setlTotalCount(this.lFBCount + this.lMobCount);
	}

	public long getlFBCount() {
		return lFBCount;
	}

	public void setlFBCount(long lFBCount) {
		this.lFBCount = lFBCount;
	}

	public long getlMobCount() {
		return lMobCount;
	}

	public void setlMobCount(long lMobCount) {
		this.lMobCount = lMobCount;
	}

	public long getlTotalCount() {
		return lTotalCount;
	}

	public void setlTotalCount(long lTotalCount) {
		this.lTotalCount = lTotalCount;
	}

	public String getsPlatform() {
		return sPlatform;
	}

	public void setsPlatform(String sPlatform) {
		this.sPlatform = sPlatform;
	}
}
