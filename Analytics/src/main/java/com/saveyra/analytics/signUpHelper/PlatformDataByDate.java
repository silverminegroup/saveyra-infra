package com.saveyra.analytics.signUpHelper;

public class PlatformDataByDate {
	private String Date;
	private String Platform;
	private long FBCount;
	private long MobCount;
	private long TotalCount;
	

	public PlatformDataByDate(String sDate,String name, long lFBCount, long lMobCount) {
		this.setDate(sDate);
		this.FBCount = lFBCount;
		this.MobCount = lMobCount;
		this.setsPlatform(name);
		this.setlTotalCount(this.FBCount + this.MobCount);
		
	}

	public long getlFBCount() {
		return FBCount;
	}

	public void setlFBCount(long lFBCount) {
		this.FBCount = lFBCount;
	}

	public long getlMobCount() {
		return MobCount;
	}

	public void setlMobCount(long lMobCount) {
		this.MobCount = lMobCount;
	}

	public long getlTotalCount() {
		return TotalCount;
	}

	public void setlTotalCount(long lTotalCount) {
		this.TotalCount = lTotalCount;
	}

	public String getsPlatform() {
		return Platform;
	}

	public void setsPlatform(String sPlatform) {
		this.Platform = sPlatform;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}
}
