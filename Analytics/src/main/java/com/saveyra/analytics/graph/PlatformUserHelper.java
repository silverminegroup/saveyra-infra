package com.saveyra.analytics.graph;

public class PlatformUserHelper {
	private String Date;
	private long Android_Count;
	private long iOS_Count;

	public PlatformUserHelper(String date, long android_Count, long iOS_Count) {
		super();
		this.setDate(date);
		setAndroid_Count(android_Count);
		this.setiOS_Count(iOS_Count);
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		this.Date = date;
	}

	public long getAndroid_Count() {
		return Android_Count;
	}

	public void setAndroid_Count(long android_Count) {
		Android_Count = android_Count;
	}

	public long getiOS_Count() {
		return iOS_Count;
	}

	public void setiOS_Count(long iOS_Count) {
		this.iOS_Count = iOS_Count;
	}
}
