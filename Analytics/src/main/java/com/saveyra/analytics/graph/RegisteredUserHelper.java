package com.saveyra.analytics.graph;

public class RegisteredUserHelper {
	private String date;
	private String platform;
	private long registered_Count;
	private long guest_count;
	
	public RegisteredUserHelper(String date,String platform, long registered_Count, long guest_count) {
		super();
		this.setDate(date);
		this.platform=platform;
		this.setRegistered_Count(registered_Count);
		this.setGuest_count(guest_count);
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public long getRegistered_Count() {
		return registered_Count;
	}

	public void setRegistered_Count(long registered_Count) {
		this.registered_Count = registered_Count;
	}

	public long getGuest_count() {
		return guest_count;
	}

	public void setGuest_count(long guest_count) {
		this.guest_count = guest_count;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}
}
