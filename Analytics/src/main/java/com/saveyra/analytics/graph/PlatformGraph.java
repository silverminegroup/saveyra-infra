package com.saveyra.analytics.graph;

public class PlatformGraph {
	private long Android_Count;
	private long iOS_Count;
	private long Total_Count;
	
	public long getiOS_Count() {
		return iOS_Count;
	}
	public void setiOS_Count(long iOS_Count) {
		this.iOS_Count = iOS_Count;
	}
	public long getAndroid_Count() {
		return Android_Count;
	}
	public void setAndroid_Count(long android_Count) {
		Android_Count = android_Count;
	}
	public PlatformGraph(long android_Count, long iOS_Count, long total_Count) {
		super();
		this.Android_Count = android_Count;
		this.iOS_Count = iOS_Count;		
		this.Total_Count = total_Count;
	}
	public long getTotal_Count() {
		return Total_Count;
	}
	public void setTotal_Count(long total_Count) {
		Total_Count = total_Count;
	}
	
	
}
