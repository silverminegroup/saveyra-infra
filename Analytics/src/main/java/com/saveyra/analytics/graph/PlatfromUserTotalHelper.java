package com.saveyra.analytics.graph;

public class PlatfromUserTotalHelper {
	private String platform;
	private long count;

	public PlatfromUserTotalHelper(String platform, long count) {
		super();
		this.platform = platform;
		this.count = count;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

}
