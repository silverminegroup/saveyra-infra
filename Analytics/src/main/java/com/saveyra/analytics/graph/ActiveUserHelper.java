package com.saveyra.analytics.graph;

public class ActiveUserHelper {
	private String Date;
	private String Platform;
	private long Count;
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public long getCount() {
		return Count;
	}
	public void setCount(long count) {
		Count = count;
	}
	public ActiveUserHelper(String date, String platform, long count) {
		super();
		Date = date;
		Platform = platform;
		Count = count;
	}
	
	
}
