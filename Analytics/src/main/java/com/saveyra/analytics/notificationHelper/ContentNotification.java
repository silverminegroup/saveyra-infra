package com.saveyra.analytics.notificationHelper;

public class ContentNotification {
    private String Date;
	private String Id;
    private String Category;
    private String Message;
    private String Language;
    private String Sent;
    private String Read;
    
	public ContentNotification(String date, String id, String category, String message, String language, String sent,
			String read) {
		super();
		Date = date;
		Id = id;
		Category = category;
		Message = message;
		Language = language;
		Sent = sent;
		Read = read;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getCategory() {
		return Category;
	}

	public void setCategory(String category) {
		Category = category;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public String getLanguage() {
		return Language;
	}

	public void setLanguage(String language) {
		Language = language;
	}

	public String getSent() {
		return Sent;
	}

	public void setSent(String sent) {
		Sent = sent;
	}

	public String getRead() {
		return Read;
	}

	public void setRead(String read) {
		Read = read;
	}	
}
