package com.saveyra.analytics.notificationHelper;

public class NotificationData {
	private String Date;
	private String Platform;
	private long Created;
	private long Read;
	private long Published;
	private int App_first_open;
	private int New_Feature;
	private int New_Avatar_Accessories;
	private int AD_Update;
	private int Create_Avatar;
	private int Avatar_Completion_Reminder;
	private int App_Rating_Review;
	private int Sign_Up;
	private int Connect_Keyboard;
	private int Daily_Weekly_Trending_Stickers;
	private int Unexplored_Sticker_Category;
	private int Sticker_pack;
	private int Sticker_of_the_day;
	private int New_Sticker_based_on_region;
	private int New_App_language;
	private int New_KB_App_language;
	private int Region_based_notification;
	private int Others;
	private int WelcomeNotification;
	private int News;
	
	public int getWelcomeNotification() {
		return WelcomeNotification;
	}
	public void setWelcomeNotification(int welcomeNotification) {
		WelcomeNotification = welcomeNotification;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public long getRead() {
		return Read;
	}
	public void setRead(long read) {
		Read = read;
	}
	public long getPublished() {
		return Published;
	}
	public void setPublished(long published) {
		Published = published;
	}
	public long getApp_first_open() {
		return App_first_open;
	}
	public int getNew_Avatar_Accessories() {
		return New_Avatar_Accessories;
	}
	public void setNew_Avatar_Accessories(int new_Avatar_Accessories) {
		New_Avatar_Accessories = new_Avatar_Accessories;
	}
	public int getNew_Feature() {
		return New_Feature;
	}
	public void setNew_Feature(int new_Feature) {
		New_Feature = new_Feature;
	}
	public int getAD_Update() {
		return AD_Update;
	}
	public void setAD_Update(int aD_Update) {
		AD_Update = aD_Update;
	}
	public int getCreate_Avatar() {
		return Create_Avatar;
	}
	public void setCreate_Avatar(int create_Avatar) {
		Create_Avatar = create_Avatar;
	}
	public int getAvatar_Completion_Reminder() {
		return Avatar_Completion_Reminder;
	}
	public void setAvatar_Completion_Reminder(int avatar_Completion_Reminder) {
		Avatar_Completion_Reminder = avatar_Completion_Reminder;
	}
	public int getApp_Rating_Review() {
		return App_Rating_Review;
	}
	public void setApp_Rating_Review(int app_Rating_Review) {
		App_Rating_Review = app_Rating_Review;
	}
	public int getSign_Up() {
		return Sign_Up;
	}
	public void setSign_Up(int sign_Up) {
		Sign_Up = sign_Up;
	}
	public int getConnect_Keyboard() {
		return Connect_Keyboard;
	}
	public void setConnect_Keyboard(int connect_Keyboard) {
		Connect_Keyboard = connect_Keyboard;
	}
	public int getDaily_Weekly_Trending_Stickers() {
		return Daily_Weekly_Trending_Stickers;
	}
	public void setDaily_Weekly_Trending_Stickers(int daily_Weekly_Trending_Stickers) {
		Daily_Weekly_Trending_Stickers = daily_Weekly_Trending_Stickers;
	}
	public int getUnexplored_Sticker_Category() {
		return Unexplored_Sticker_Category;
	}
	public void setUnexplored_Sticker_Category(int unexplored_Sticker_Category) {
		Unexplored_Sticker_Category = unexplored_Sticker_Category;
	}
	public int getSticker_pack() {
		return Sticker_pack;
	}
	public void setSticker_pack(int sticker_pack) {
		Sticker_pack = sticker_pack;
	}
	public int getSticker_of_the_day() {
		return Sticker_of_the_day;
	}
	public void setSticker_of_the_day(int sticker_of_the_day) {
		Sticker_of_the_day = sticker_of_the_day;
	}
	public int getNew_Sticker_based_on_region() {
		return New_Sticker_based_on_region;
	}
	public void setNew_Sticker_based_on_region(int new_Sticker_based_on_region) {
		New_Sticker_based_on_region = new_Sticker_based_on_region;
	}
	public int getNew_App_language() {
		return New_App_language;
	}
	public void setNew_App_language(int new_App_language) {
		New_App_language = new_App_language;
	}
	public int getNew_KB_App_language() {
		return New_KB_App_language;
	}
	public void setNew_KB_App_language(int new_KB_App_language) {
		New_KB_App_language = new_KB_App_language;
	}
	public int getRegion_based_notification() {
		return Region_based_notification;
	}
	public void setRegion_based_notification(int region_based_notification) {
		Region_based_notification = region_based_notification;
	}
	public int getOthers() {
		return Others;
	}
	public void setOthers(int others) {
		Others = others;
	}
	public void setApp_first_open(int app_first_open) {
		App_first_open = app_first_open;
	}
	public NotificationData(String date, String platform,long created, long read, long published, int app_first_open,
			int new_Feature, int new_Avatar_Accessories, int aD_Update, int create_Avatar,
			int avatar_Completion_Reminder, int app_Rating_Review, int sign_Up, int connect_Keyboard,
			int daily_Weekly_Trending_Stickers, int unexplored_Sticker_Category, int sticker_pack,
			int sticker_of_the_day, int new_Sticker_based_on_region, int new_App_language, int new_KB_App_language,
			int region_based_notification, int others,int welcomeNotification,int news) {
		super();
		Date = date;
		Platform = platform;
		Created = created;
		Read = read;
		Published = published;
		App_first_open = app_first_open;
		New_Feature = new_Feature;
		New_Avatar_Accessories = new_Avatar_Accessories;
		AD_Update = aD_Update;
		Create_Avatar = create_Avatar;
		Avatar_Completion_Reminder = avatar_Completion_Reminder;
		App_Rating_Review = app_Rating_Review;
		Sign_Up = sign_Up;
		Connect_Keyboard = connect_Keyboard;
		Daily_Weekly_Trending_Stickers = daily_Weekly_Trending_Stickers;
		Unexplored_Sticker_Category = unexplored_Sticker_Category;
		Sticker_pack = sticker_pack;
		Sticker_of_the_day = sticker_of_the_day;
		New_Sticker_based_on_region = new_Sticker_based_on_region;
		New_App_language = new_App_language;
		New_KB_App_language = new_KB_App_language;
		Region_based_notification = region_based_notification;
		Others = others;
		WelcomeNotification = welcomeNotification;
		News = news;
	}
	public long getCreated() {
		return Created;
	}
	public void setCreated(long created) {
		Created = created;
	}
	public int getNews() {
		return News;
	}
	public void setNews(int news) {
		News = news;
	}
	
	
}
