package com.saveyra.analytics.notificationHelper;

public class ReducedNotificationData {
	
		private String Date;
		private String Platform;
		private long Published;
		private long Read;
		private int News;
		private int Promotions;
		private int WelcomeNotification;
		private int Avatar_Completed;
		public String getDate() {
			return Date;
		}
		public void setDate(String date) {
			Date = date;
		}
		public String getPlatform() {
			return Platform;
		}
		public void setPlatform(String platform) {
			Platform = platform;
		}
		public long getPublished() {
			return Published;
		}
		public void setPublished(long published) {
			Published = published;
		}
		public long getRead() {
			return Read;
		}
		public void setRead(long read) {
			Read = read;
		}
		public int getNews() {
			return News;
		}
		public void setNews(int news) {
			News = news;
		}
		public int getPromotions() {
			return Promotions;
		}
		public void setPromotions(int promotions) {
			Promotions = promotions;
		}
		public int getWelcomeNotification() {
			return WelcomeNotification;
		}
		public void setWelcomeNotification(int welcomeNotification) {
			WelcomeNotification = welcomeNotification;
		}
		public int getAvatar_Completion_Reminder() {
			return Avatar_Completed;
		}
		public void setAvatar_Completion_Reminder(int avatar_Completion_Reminder) {
			Avatar_Completed = avatar_Completion_Reminder;
		}
		public ReducedNotificationData(String date, String platform, long published, long read, int news,
				int promotions, int welcomeNotification, int avatar_Completion_Reminder) {
			super();
			Date = date;
			Platform = platform;
			Published = published;
			Read = read;
			News = news;
			Promotions = promotions;
			WelcomeNotification = welcomeNotification;
			Avatar_Completed = avatar_Completion_Reminder;
		}		
}
