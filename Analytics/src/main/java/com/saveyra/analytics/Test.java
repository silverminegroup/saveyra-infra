package com.saveyra.analytics;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

public class Test {
	
	public static void main(String[] args) {
		Date startDate =null;
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		Date input = null;
		try {
			input = formatter.parse("2019-03-31");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		System.out.println(" >> "+formatter.format(getlastMonthObject(input)));
		
		
		
		/*try {
			startDate = formatter.parse("2019-05-09");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ArrayList<Date> dateArray = new ArrayList<Date>();
		
		for(int i=0;i<25;i++) {			
			Calendar calendar=Calendar.getInstance();
			calendar.setTime(startDate);
			calendar.set(Calendar.HOUR_OF_DAY, i);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			Date date=calendar.getTime();			
			dateArray.add(date);			
		}
		
		

		
		DateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		
		for(Date d : dateArray) {
			System.out.println(formatter2.format(d));
		}*/
		
		
		
		
		
		
		
		
		/*
		
		
		String s = "Identification and Treatment \r\n" + 
				" <LB>  Reason behind PPH (atonic, traumatic, other)\r\n" + 
				" <LB> Basis of suspicion? (excessive bleeding / vitals)\r\n" + 
				" <LB> Is IV fluid given , if yes when IV fluids started?\r\n" + 
				" <LB> What IV fluids given? (RL or NS should be given)\r\n" + 
				" <LB> How fast were the IV fluids administered?\r\n" + 
				" <LB> If BT given, how many units? (at least two)\r\n" + 
				" <LB> Uterotonics given? (Oxytocin upto 60 IU - 10 IU per bottle)\r\n" + 
				" <LB> Uterine massage done? (may help)\r\n" + 
				" <GB>\r\n" + 
				" Preventive aspect\r\n" + 
				" <LB> Oxytocin given within a minute of delivery of baby?\r\n" + 
				" <LB> Immediate BF given? \r\n" + 
				"  <GB>\r\n" + 
				" Augmentation(Mal practice) \r\n" + 
				" <LB> Fundal pressure applied during labor? (possible cause)\r\n" + 
				" <LB> Did labor last a very short period? (Misuse of Oxytocin) \r\n" + 
				"  <GB>\r\n" + 
				" Managed and discharged\r\n" + 
				" <LB> What other definitive management? (surgery,other procedure)\r\n" + 
				" <LB> Discharge note given? Did it mentiondiagnosis, status of vital signs at time of discharge, treatment given? \r\n" + 
				" <LB> Status of baby?";
		
		Map<String, String[]> labelMap = new LinkedHashMap<String, String[]>();
		
		String[] groupsArray = s.split("<GB>");
		
		for(int i=0;i<groupsArray.length;i++) {
			String[] innerList = groupsArray[i].trim().split("<LB>");			
			labelMap.put(groupsArray[i].trim().split("<LB>")[0], innerList);		
		}
		
		
		for(String s2: labelMap.keySet()) {
			System.out.println("Group : "+s2);
			for(int i=1;i<labelMap.get(s2).length;i++) {
				System.out.println("      Inner : "+labelMap.get(s2)[i]);
			}			
		}*/		
		
	}
	
	public static Date getlastMonthObject(Date curDate) {
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(curDate);
		calendar.roll(Calendar.MONTH, -1);
		LocalDate localDate = curDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int month = localDate.getMonthValue();

		if (month == 1) {
			calendar.roll(Calendar.YEAR, -1);
		}
		return calendar.getTime();
	}

}
