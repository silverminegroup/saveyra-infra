package com.saveyra.analytics.consolidatedReport;

public class ConsolidatedReport {
	long DailyActiveUser;
	long WeeklyReport;
	long MonthlyReport;	
	long FirstTimeUser;
	long ReturningUser;
	long FirstOpenUserViewed;
	long UsersInititatedKeyboardConnection;
	long UsersEnabledKeyboard;
	long UsersInitiatedFacebookSignUp;
	long UsersCompletedFacebookSignUp;
	long UsersInitiatedSignUpUsingMobileNo;
	long UsersCompletedSignUpUsingMobileNo;
	long ResetOTP;
	long UsersLandedOnCreateAvatarPage;
	long SelectedGenderFemale;
	long SelectedGenderMale;
	long UsersCompletedSavedAvatar;
	long UsersResetTheirAvatar;
	String NotificationCreated;
	long NoOfUsersNotificationsDelivered;
	long NoOfPeopleOpenedNotification;
	String NotificationString;
	long WelcomeNotificationSent;
	long WelcomeNotificationRead;
	long HomeScreenViewed;
	long StickerScreenViewed;
	long AvatarScreenViewed;
	long SettingsScreenViewed;
	long SearchScreenViewed;
	
	
	
	public ConsolidatedReport(long dailyActiveUser, long weeklyReport, long monthlyReport, long firstTimeUser,
			long returningUser, long firstOpenUserViewed, long usersInititatedKeyboardConnection,
			long usersEnabledKeyboard, long usersInitiatedFacebookSignUp, long usersCompletedFacebookSignUp,
			long usersInitiatedSignUpUsingMobileNo, long usersCompletedSignUpUsingMobileNo, long resetOTP,
			long usersLandedOnCreateAvatarPage, long selectedGenderFemale, long selectedGenderMale,
			long usersCompletedSavedAvatar, long usersResetTheirAvatar, String notificationCreated,
			long noOfUsersNotificationsDelivered, long noOfPeopleOpenedNotification, String notificationString,
			long welcomeNotificationSent, long welcomeNotificationRead, long homeScreenViewed, long stickerScreenViewed,
			long avatarScreenViewed, long settingsScreenViewed, long searchScreenViewed) {
		super();
		DailyActiveUser = dailyActiveUser;
		WeeklyReport = weeklyReport;
		MonthlyReport = monthlyReport;
		FirstTimeUser = firstTimeUser;
		ReturningUser = returningUser;
		FirstOpenUserViewed = firstOpenUserViewed;
		UsersInititatedKeyboardConnection = usersInititatedKeyboardConnection;
		UsersEnabledKeyboard = usersEnabledKeyboard;
		UsersInitiatedFacebookSignUp = usersInitiatedFacebookSignUp;
		UsersCompletedFacebookSignUp = usersCompletedFacebookSignUp;
		UsersInitiatedSignUpUsingMobileNo = usersInitiatedSignUpUsingMobileNo;
		UsersCompletedSignUpUsingMobileNo = usersCompletedSignUpUsingMobileNo;
		ResetOTP = resetOTP;
		UsersLandedOnCreateAvatarPage = usersLandedOnCreateAvatarPage;
		SelectedGenderFemale = selectedGenderFemale;
		SelectedGenderMale = selectedGenderMale;
		UsersCompletedSavedAvatar = usersCompletedSavedAvatar;
		UsersResetTheirAvatar = usersResetTheirAvatar;
		NotificationCreated = notificationCreated;
		NoOfUsersNotificationsDelivered = noOfUsersNotificationsDelivered;
		NoOfPeopleOpenedNotification = noOfPeopleOpenedNotification;
		NotificationString = notificationString;
		WelcomeNotificationSent = welcomeNotificationSent;
		WelcomeNotificationRead = welcomeNotificationRead;
		HomeScreenViewed = homeScreenViewed;
		StickerScreenViewed = stickerScreenViewed;
		AvatarScreenViewed = avatarScreenViewed;
		SettingsScreenViewed = settingsScreenViewed;
		SearchScreenViewed = searchScreenViewed;
	}
	public long getDailyActiveUser() {
		return DailyActiveUser;
	}
	public void setDailyActiveUser(long dailyActiveUser) {
		DailyActiveUser = dailyActiveUser;
	}
	public long getWeeklyReport() {
		return WeeklyReport;
	}
	public void setWeeklyReport(long weeklyReport) {
		WeeklyReport = weeklyReport;
	}
	public long getMonthlyReport() {
		return MonthlyReport;
	}
	public void setMonthlyReport(long monthlyReport) {
		MonthlyReport = monthlyReport;
	}
	public long getFirstTimeUser() {
		return FirstTimeUser;
	}
	public void setFirstTimeUser(long firstTimeUser) {
		FirstTimeUser = firstTimeUser;
	}
	public long getReturningUser() {
		return ReturningUser;
	}
	public void setReturningUser(long returningUser) {
		ReturningUser = returningUser;
	}
	public long getFirstOpenUserViewed() {
		return FirstOpenUserViewed;
	}
	public void setFirstOpenUserViewed(long firstOpenUserViewed) {
		FirstOpenUserViewed = firstOpenUserViewed;
	}
	public long getUsersInititatedKeyboardConnection() {
		return UsersInititatedKeyboardConnection;
	}
	public void setUsersInititatedKeyboardConnection(long usersInititatedKeyboardConnection) {
		UsersInititatedKeyboardConnection = usersInititatedKeyboardConnection;
	}
	public long getUsersEnabledKeyboard() {
		return UsersEnabledKeyboard;
	}
	public void setUsersEnabledKeyboard(long usersEnabledKeyboard) {
		UsersEnabledKeyboard = usersEnabledKeyboard;
	}
	public long getUsersInitiatedFacebookSignUp() {
		return UsersInitiatedFacebookSignUp;
	}
	public void setUsersInitiatedFacebookSignUp(long usersInitiatedFacebookSignUp) {
		UsersInitiatedFacebookSignUp = usersInitiatedFacebookSignUp;
	}
	public long getUsersCompletedFacebookSignUp() {
		return UsersCompletedFacebookSignUp;
	}
	public void setUsersCompletedFacebookSignUp(long usersCompletedFacebookSignUp) {
		UsersCompletedFacebookSignUp = usersCompletedFacebookSignUp;
	}
	public long getUsersInitiatedSignUpUsingMobileNo() {
		return UsersInitiatedSignUpUsingMobileNo;
	}
	public void setUsersInitiatedSignUpUsingMobileNo(long usersInitiatedSignUpUsingMobileNo) {
		UsersInitiatedSignUpUsingMobileNo = usersInitiatedSignUpUsingMobileNo;
	}
	public long getUsersCompletedSignUpUsingMobileNo() {
		return UsersCompletedSignUpUsingMobileNo;
	}
	public void setUsersCompletedSignUpUsingMobileNo(long usersCompletedSignUpUsingMobileNo) {
		UsersCompletedSignUpUsingMobileNo = usersCompletedSignUpUsingMobileNo;
	}
	public long getResetOTP() {
		return ResetOTP;
	}
	public void setResetOTP(long resetOTP) {
		ResetOTP = resetOTP;
	}
	public long getUsersLandedOnCreateAvatarPage() {
		return UsersLandedOnCreateAvatarPage;
	}
	public void setUsersLandedOnCreateAvatarPage(long usersLandedOnCreateAvatarPage) {
		UsersLandedOnCreateAvatarPage = usersLandedOnCreateAvatarPage;
	}
	public long getSelectedGenderFemale() {
		return SelectedGenderFemale;
	}
	public void setSelectedGenderFemale(long selectedGenderFemale) {
		SelectedGenderFemale = selectedGenderFemale;
	}
	public long getSelectedGenderMale() {
		return SelectedGenderMale;
	}
	public void setSelectedGenderMale(long selectedGenderMale) {
		SelectedGenderMale = selectedGenderMale;
	}
	public long getUsersCompletedSavedAvatar() {
		return UsersCompletedSavedAvatar;
	}
	public void setUsersCompletedSavedAvatar(long usersCompletedSavedAvatar) {
		UsersCompletedSavedAvatar = usersCompletedSavedAvatar;
	}
	public long getUsersResetTheirAvatar() {
		return UsersResetTheirAvatar;
	}
	public void setUsersResetTheirAvatar(long usersResetTheirAvatar) {
		UsersResetTheirAvatar = usersResetTheirAvatar;
	}
	public String getNotificationCreated() {
		return NotificationCreated;
	}
	public void setNotificationCreated(String notificationCreated) {
		NotificationCreated = notificationCreated;
	}
	public long getNoOfUsersNotificationsDelivered() {
		return NoOfUsersNotificationsDelivered;
	}
	public void setNoOfUsersNotificationsDelivered(long noOfUsersNotificationsDelivered) {
		NoOfUsersNotificationsDelivered = noOfUsersNotificationsDelivered;
	}
	public long getNoOfPeopleOpenedNotification() {
		return NoOfPeopleOpenedNotification;
	}
	public void setNoOfPeopleOpenedNotification(long noOfPeopleOpenedNotification) {
		NoOfPeopleOpenedNotification = noOfPeopleOpenedNotification;
	}
	public String getNotificationString() {
		return NotificationString;
	}
	public void setNotificationString(String notificationString) {
		NotificationString = notificationString;
	}
	public long getWelcomeNotificationSent() {
		return WelcomeNotificationSent;
	}
	public void setWelcomeNotificationSent(long welcomeNotificationSent) {
		WelcomeNotificationSent = welcomeNotificationSent;
	}
	public long getWelcomeNotificationRead() {
		return WelcomeNotificationRead;
	}
	public void setWelcomeNotificationRead(long welcomeNotificationRead) {
		WelcomeNotificationRead = welcomeNotificationRead;
	}
	public long getHomeScreenViewed() {
		return HomeScreenViewed;
	}
	public void setHomeScreenViewed(long homeScreenViewed) {
		HomeScreenViewed = homeScreenViewed;
	}
	public long getStickerScreenViewed() {
		return StickerScreenViewed;
	}
	public void setStickerScreenViewed(long stickerScreenViewed) {
		StickerScreenViewed = stickerScreenViewed;
	}
	public long getAvatarScreenViewed() {
		return AvatarScreenViewed;
	}
	public void setAvatarScreenViewed(long avatarScreenViewed) {
		AvatarScreenViewed = avatarScreenViewed;
	}
	public long getSettingsScreenViewed() {
		return SettingsScreenViewed;
	}
	public void setSettingsScreenViewed(long settingsScreenViewed) {
		SettingsScreenViewed = settingsScreenViewed;
	}
	public long getSearchScreenViewed() {
		return SearchScreenViewed;
	}
	public void setSearchScreenViewed(long searchScreenViewed) {
		SearchScreenViewed = searchScreenViewed;
	}
}
