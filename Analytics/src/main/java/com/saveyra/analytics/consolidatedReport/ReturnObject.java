package com.saveyra.analytics.consolidatedReport;

public class ReturnObject {
	String Date;
	String Key;
	String Value;
	
	
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getKey() {
		return Key;
	}
	public void setKey(String key) {
		Key = key;
	}
	public String getValue() {
		return Value;
	}
	public void setValue(String value) {
		Value = value;
	}
	public ReturnObject(String date, String key, String value) {
		super();
		Date = date;
		Key = key;
		Value = value;
	}	
}
