package com.saveyra.analytics.consolidatedReport;

public class ConsolidatedObject {
	private String Date;
	private String Daily_active_user;
	private String Daily_active_user_Android;
	private String Daily_active_user_iOS;
	private String Weekly_Active_users;
	private String Monthly_Active_users;
	private String First_time_user;
	private String Returning_user;
	private String First_open_User_viewed;
	private String Users_inititated_keyboard_connection;
	private String Users_enabled_keyboard;
	private String Users_initiated_Facebook_sign_up;
	private String Users_completed_Facebook_Sign_up;
	private String Users_initiated_sign_up_using_mobile_no;
	private String Users_completed_sign_up_using_mobile_no;
	private String Reset_OTP;
	private String Users_landed_on_create_avatar_page;
	private String Selected_gender_female;
	private String Selected_gender_male;
	private String Users_completed_saved_avatar;
	private String Users_reset_their_avatar;
	private String No_of_notification_created;
	private String No_of_users_notifications_was_delivered;
	private String No_of_people_opened_the_notification;
	private String Notification_types;
	private String Welcome_Notification_Sent;
	private String Welcome_Notification_Read;
	private String Home_screen_viewed;
	private String Sticker_screen_viewed;
	private String Avatar_screen_viewed;
	private String Settings_screen_viewed;
	private String Search_screen_viewed;
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getDaily_active_user() {
		return Daily_active_user;
	}
	public void setDaily_active_user(String daily_active_user) {
		Daily_active_user = daily_active_user;
	}
	public String getDaily_active_user_Android() {
		return Daily_active_user_Android;
	}
	public void setDaily_active_user_Android(String daily_active_user_Android) {
		Daily_active_user_Android = daily_active_user_Android;
	}
	public String getDaily_active_user_iOS() {
		return Daily_active_user_iOS;
	}
	public void setDaily_active_user_iOS(String daily_active_user_iOS) {
		Daily_active_user_iOS = daily_active_user_iOS;
	}
	public String getWeekly_Active_users() {
		return Weekly_Active_users;
	}
	public void setWeekly_Active_users(String weekly_Active_users) {
		Weekly_Active_users = weekly_Active_users;
	}
	public String getMonthly_Active_users() {
		return Monthly_Active_users;
	}
	public void setMonthly_Active_users(String monthly_Active_users) {
		Monthly_Active_users = monthly_Active_users;
	}
	public String getFirst_time_user() {
		return First_time_user;
	}
	public void setFirst_time_user(String first_time_user) {
		First_time_user = first_time_user;
	}
	public String getReturning_user() {
		return Returning_user;
	}
	public void setReturning_user(String returning_user) {
		Returning_user = returning_user;
	}
	public String getFirst_open_User_viewed() {
		return First_open_User_viewed;
	}
	public void setFirst_open_User_viewed(String first_open_User_viewed) {
		First_open_User_viewed = first_open_User_viewed;
	}
	public String getUsers_inititated_keyboard_connection() {
		return Users_inititated_keyboard_connection;
	}
	public void setUsers_inititated_keyboard_connection(String users_inititated_keyboard_connection) {
		Users_inititated_keyboard_connection = users_inititated_keyboard_connection;
	}
	public String getUsers_enabled_keyboard() {
		return Users_enabled_keyboard;
	}
	public void setUsers_enabled_keyboard(String users_enabled_keyboard) {
		Users_enabled_keyboard = users_enabled_keyboard;
	}
	public String getUsers_initiated_Facebook_sign_up() {
		return Users_initiated_Facebook_sign_up;
	}
	public void setUsers_initiated_Facebook_sign_up(String users_initiated_Facebook_sign_up) {
		Users_initiated_Facebook_sign_up = users_initiated_Facebook_sign_up;
	}
	public String getUsers_completed_Facebook_Sign_up() {
		return Users_completed_Facebook_Sign_up;
	}
	public void setUsers_completed_Facebook_Sign_up(String users_completed_Facebook_Sign_up) {
		Users_completed_Facebook_Sign_up = users_completed_Facebook_Sign_up;
	}
	public String getUsers_initiated_sign_up_using_mobile_no() {
		return Users_initiated_sign_up_using_mobile_no;
	}
	public void setUsers_initiated_sign_up_using_mobile_no(String users_initiated_sign_up_using_mobile_no) {
		Users_initiated_sign_up_using_mobile_no = users_initiated_sign_up_using_mobile_no;
	}
	public String getUsers_completed_sign_up_using_mobile_no() {
		return Users_completed_sign_up_using_mobile_no;
	}
	public void setUsers_completed_sign_up_using_mobile_no(String users_completed_sign_up_using_mobile_no) {
		Users_completed_sign_up_using_mobile_no = users_completed_sign_up_using_mobile_no;
	}
	public String getReset_OTP() {
		return Reset_OTP;
	}
	public void setReset_OTP(String reset_OTP) {
		Reset_OTP = reset_OTP;
	}
	public String getUsers_landed_on_create_avatar_page() {
		return Users_landed_on_create_avatar_page;
	}
	public void setUsers_landed_on_create_avatar_page(String users_landed_on_create_avatar_page) {
		Users_landed_on_create_avatar_page = users_landed_on_create_avatar_page;
	}
	public String getSelected_gender_female() {
		return Selected_gender_female;
	}
	public void setSelected_gender_female(String selected_gender_female) {
		Selected_gender_female = selected_gender_female;
	}
	public String getSelected_gender_male() {
		return Selected_gender_male;
	}
	public void setSelected_gender_male(String selected_gender_male) {
		Selected_gender_male = selected_gender_male;
	}
	public String getUsers_completed_saved_avatar() {
		return Users_completed_saved_avatar;
	}
	public void setUsers_completed_saved_avatar(String users_completed_saved_avatar) {
		Users_completed_saved_avatar = users_completed_saved_avatar;
	}
	public String getUsers_reset_their_avatar() {
		return Users_reset_their_avatar;
	}
	public void setUsers_reset_their_avatar(String users_reset_their_avatar) {
		Users_reset_their_avatar = users_reset_their_avatar;
	}
	public String getNo_of_notification_created() {
		return No_of_notification_created;
	}
	public void setNo_of_notification_created(String no_of_notification_created) {
		No_of_notification_created = no_of_notification_created;
	}
	public String getNo_of_users_notifications_was_delivered() {
		return No_of_users_notifications_was_delivered;
	}
	public void setNo_of_users_notifications_was_delivered(String no_of_users_notifications_was_delivered) {
		No_of_users_notifications_was_delivered = no_of_users_notifications_was_delivered;
	}
	public String getNo_of_people_opened_the_notification() {
		return No_of_people_opened_the_notification;
	}
	public void setNo_of_people_opened_the_notification(String no_of_people_opened_the_notification) {
		No_of_people_opened_the_notification = no_of_people_opened_the_notification;
	}
	public String getNotification_types() {
		return Notification_types;
	}
	public void setNotification_types(String notification_types) {
		Notification_types = notification_types;
	}
	public String getWelcome_Notification_Sent() {
		return Welcome_Notification_Sent;
	}
	public void setWelcome_Notification_Sent(String welcome_Notification_Sent) {
		Welcome_Notification_Sent = welcome_Notification_Sent;
	}
	public String getWelcome_Notification_Read() {
		return Welcome_Notification_Read;
	}
	public void setWelcome_Notification_Read(String welcome_Notification_Read) {
		Welcome_Notification_Read = welcome_Notification_Read;
	}
	public String getHome_screen_viewed() {
		return Home_screen_viewed;
	}
	public void setHome_screen_viewed(String home_screen_viewed) {
		Home_screen_viewed = home_screen_viewed;
	}
	public String getSticker_screen_viewed() {
		return Sticker_screen_viewed;
	}
	public void setSticker_screen_viewed(String sticker_screen_viewed) {
		Sticker_screen_viewed = sticker_screen_viewed;
	}
	public String getAvatar_screen_viewed() {
		return Avatar_screen_viewed;
	}
	public void setAvatar_screen_viewed(String avatar_screen_viewed) {
		Avatar_screen_viewed = avatar_screen_viewed;
	}
	public String getSettings_screen_viewed() {
		return Settings_screen_viewed;
	}
	public void setSettings_screen_viewed(String settings_screen_viewed) {
		Settings_screen_viewed = settings_screen_viewed;
	}
	public String getSearch_screen_viewed() {
		return Search_screen_viewed;
	}
	public void setSearch_screen_viewed(String search_screen_viewed) {
		Search_screen_viewed = search_screen_viewed;
	}
	public ConsolidatedObject(String date, String daily_active_user, String daily_active_user_Android,
			String daily_active_user_iOS, String weekly_Active_users, String monthly_Active_users,
			String first_time_user, String returning_user, String first_open_User_viewed,
			String users_inititated_keyboard_connection, String users_enabled_keyboard,
			String users_initiated_Facebook_sign_up, String users_completed_Facebook_Sign_up,
			String users_initiated_sign_up_using_mobile_no, String users_completed_sign_up_using_mobile_no,
			String reset_OTP, String users_landed_on_create_avatar_page, String selected_gender_female,
			String selected_gender_male, String users_completed_saved_avatar, String users_reset_their_avatar,
			String no_of_notification_created, String no_of_users_notifications_was_delivered,
			String no_of_people_opened_the_notification, String notification_types, String welcome_Notification_Sent,
			String welcome_Notification_Read, String home_screen_viewed, String sticker_screen_viewed,
			String avatar_screen_viewed, String settings_screen_viewed, String search_screen_viewed) {
		super();
		Date = date;
		Daily_active_user = daily_active_user;
		Daily_active_user_Android = daily_active_user_Android;
		Daily_active_user_iOS = daily_active_user_iOS;
		Weekly_Active_users = weekly_Active_users;
		Monthly_Active_users = monthly_Active_users;
		First_time_user = first_time_user;
		Returning_user = returning_user;
		First_open_User_viewed = first_open_User_viewed;
		Users_inititated_keyboard_connection = users_inititated_keyboard_connection;
		Users_enabled_keyboard = users_enabled_keyboard;
		Users_initiated_Facebook_sign_up = users_initiated_Facebook_sign_up;
		Users_completed_Facebook_Sign_up = users_completed_Facebook_Sign_up;
		Users_initiated_sign_up_using_mobile_no = users_initiated_sign_up_using_mobile_no;
		Users_completed_sign_up_using_mobile_no = users_completed_sign_up_using_mobile_no;
		Reset_OTP = reset_OTP;
		Users_landed_on_create_avatar_page = users_landed_on_create_avatar_page;
		Selected_gender_female = selected_gender_female;
		Selected_gender_male = selected_gender_male;
		Users_completed_saved_avatar = users_completed_saved_avatar;
		Users_reset_their_avatar = users_reset_their_avatar;
		No_of_notification_created = no_of_notification_created;
		No_of_users_notifications_was_delivered = no_of_users_notifications_was_delivered;
		No_of_people_opened_the_notification = no_of_people_opened_the_notification;
		Notification_types = notification_types;
		Welcome_Notification_Sent = welcome_Notification_Sent;
		Welcome_Notification_Read = welcome_Notification_Read;
		Home_screen_viewed = home_screen_viewed;
		Sticker_screen_viewed = sticker_screen_viewed;
		Avatar_screen_viewed = avatar_screen_viewed;
		Settings_screen_viewed = settings_screen_viewed;
		Search_screen_viewed = search_screen_viewed;
	}
	
	
}
