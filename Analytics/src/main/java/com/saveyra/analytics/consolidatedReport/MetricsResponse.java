package com.saveyra.analytics.consolidatedReport;

public class MetricsResponse {

	String Date;
    String Published;
    String Delivered;
    String Failed;
    String Platform;
    
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPublished() {
		return Published;
	}
	public void setPublished(String published) {
		Published = published;
	}
	public String getDelivered() {
		return Delivered;
	}
	public void setDelivered(String delivered) {
		Delivered = delivered;
	}
	public String getFailed() {
		return Failed;
	}
	public void setFailed(String failed) {
		Failed = failed;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public MetricsResponse(String date, String published, String delivered, String failed, String platform) {
		super();
		Date = date;
		Published = published;
		Delivered = delivered;
		Failed = failed;
		Platform = platform;
	}
}
