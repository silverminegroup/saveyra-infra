package com.saveyra.analytics.screenviewHelper;

public class ScreenViewData {
	private String Date;
	private String Platform;
	private String HomeScreen;
	private String AvatarScreen;        
	private String StickerScreen;        
	private String SettingsScreen;        
	private String SearchScreen;     
	private String OnboardingScreen;     
	private String NotificationSettingScreen;        
	private String LanguagePreferenceScreen;          
	private String SignUpScreen;            
	private String MobileSignUpScreen;            
	private String LoginScreen;            
	private String LoginMobileScreen;            
	private String HelpCentreScreen;            
	private String ReportAProblemScreen;            
	private String ContactUsScreen;  
	private String LegalInfoScreen;
	
	public ScreenViewData(String date, String platform, String homeScreen, String avatarScreen, String stickerScreen,
			String settingsScreen, String searchScreen, String onboardingScreen, String notificationSettingScreen,
			String languagePreferenceScreen, String signUpScreen, String mobileSignUpScreen, String loginScreen,
			String loginMobileScreen, String helpCentreScreen, String reportAProblemScreen, String contactUsScreen,
			String legalInfoScreen) {
		super();
		Date = date;
		Platform = platform;
		HomeScreen = homeScreen;
		AvatarScreen = avatarScreen;
		StickerScreen = stickerScreen;
		SettingsScreen = settingsScreen;
		SearchScreen = searchScreen;
		OnboardingScreen = onboardingScreen;
		NotificationSettingScreen = notificationSettingScreen;
		LanguagePreferenceScreen = languagePreferenceScreen;
		SignUpScreen = signUpScreen;
		MobileSignUpScreen = mobileSignUpScreen;
		LoginScreen = loginScreen;
		LoginMobileScreen = loginMobileScreen;
		HelpCentreScreen = helpCentreScreen;
		ReportAProblemScreen = reportAProblemScreen;
		ContactUsScreen = contactUsScreen;
		LegalInfoScreen = legalInfoScreen;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public String getHomeScreen() {
		return HomeScreen;
	}
	public void setHomeScreen(String homeScreen) {
		HomeScreen = homeScreen;
	}
	public String getAvatarScreen() {
		return AvatarScreen;
	}
	public void setAvatarScreen(String avatarScreen) {
		AvatarScreen = avatarScreen;
	}
	public String getStickerScreen() {
		return StickerScreen;
	}
	public void setStickerScreen(String stickerScreen) {
		StickerScreen = stickerScreen;
	}
	public String getSettingsScreen() {
		return SettingsScreen;
	}
	public void setSettingsScreen(String settingsScreen) {
		SettingsScreen = settingsScreen;
	}
	public String getSearchScreen() {
		return SearchScreen;
	}
	public void setSearchScreen(String searchScreen) {
		SearchScreen = searchScreen;
	}
	public String getOnboardingScreen() {
		return OnboardingScreen;
	}
	public void setOnboardingScreen(String onboardingScreen) {
		OnboardingScreen = onboardingScreen;
	}
	public String getNotificationSettingScreen() {
		return NotificationSettingScreen;
	}
	public void setNotificationSettingScreen(String notificationSettingScreen) {
		NotificationSettingScreen = notificationSettingScreen;
	}
	public String getLanguagePreferenceScreen() {
		return LanguagePreferenceScreen;
	}
	public void setLanguagePreferenceScreen(String languagePreferenceScreen) {
		LanguagePreferenceScreen = languagePreferenceScreen;
	}
	public String getSignUpScreen() {
		return SignUpScreen;
	}
	public void setSignUpScreen(String signUpScreen) {
		SignUpScreen = signUpScreen;
	}
	public String getMobileSignUpScreen() {
		return MobileSignUpScreen;
	}
	public void setMobileSignUpScreen(String mobileSignUpScreen) {
		MobileSignUpScreen = mobileSignUpScreen;
	}
	public String getLoginScreen() {
		return LoginScreen;
	}
	public void setLoginScreen(String loginScreen) {
		LoginScreen = loginScreen;
	}
	public String getLoginMobileScreen() {
		return LoginMobileScreen;
	}
	public void setLoginMobileScreen(String loginMobileScreen) {
		LoginMobileScreen = loginMobileScreen;
	}
	public String getHelpCentreScreen() {
		return HelpCentreScreen;
	}
	public void setHelpCentreScreen(String helpCentreScreen) {
		HelpCentreScreen = helpCentreScreen;
	}
	public String getReportAProblemScreen() {
		return ReportAProblemScreen;
	}
	public void setReportAProblemScreen(String reportAProblemScreen) {
		ReportAProblemScreen = reportAProblemScreen;
	}
	public String getContactUsScreen() {
		return ContactUsScreen;
	}
	public void setContactUsScreen(String contactUsScreen) {
		ContactUsScreen = contactUsScreen;
	}
	public String getLegalInfoScreen() {
		return LegalInfoScreen;
	}
	public void setLegalInfoScreen(String legalInfoScreen) {
		LegalInfoScreen = legalInfoScreen;
	}

	
	
}
