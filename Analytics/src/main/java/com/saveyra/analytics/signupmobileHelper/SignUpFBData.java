package com.saveyra.analytics.signupmobileHelper;

public class SignUpFBData {
	private String Date;
	private String Platform;
	private long Initialized;
	private long Completed;
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public long getInitialized() {
		return Initialized;
	}
	public void setInitialized(long initialized) {
		Initialized = initialized;
	}
	public long getCompleted() {
		return Completed;
	}
	public void setCompleted(long completed) {
		Completed = completed;
	}
	public SignUpFBData(String date, String platform, long initialized, long completed) {
		super();
		Date = date;
		Platform = platform;
		Initialized = initialized;
		Completed = completed;
	}	
}
