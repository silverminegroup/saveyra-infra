package com.saveyra.analytics.stickerShareHelper;

import java.util.Map;

public class StickerShareUserData {
	private String Date;
	private String TimeStamp;
	private String ApplicationId;
	private String Usage;
	private String Platform;
	private String UserType;
	private String AppType;
	private String StickerID;
	private String StickerURL;
	private String Category;
	private String SubCategory;
	private String ShareCount;	
	private Map<String,String> targetMap;
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getTimeStamp() {
		return TimeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		TimeStamp = timeStamp;
	}
	public String getApplicationId() {
		return ApplicationId;
	}
	public void setApplicationId(String applicationId) {
		ApplicationId = applicationId;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public String getUserType() {
		return UserType;
	}
	public void setUserType(String userType) {
		UserType = userType;
	}
	public String getAppType() {
		return AppType;
	}
	public void setAppType(String appType) {
		AppType = appType;
	}
	public String getStickerID() {
		return StickerID;
	}
	public void setStickerID(String stickerID) {
		StickerID = stickerID;
	}
	public String getStickerURL() {
		return StickerURL;
	}
	public void setStickerURL(String stickerURL) {
		StickerURL = stickerURL;
	}
	public String getCategory() {
		return Category;
	}
	public void setCategory(String category) {
		Category = category;
	}
	public String getSubCategory() {
		return SubCategory;
	}
	public void setSubCategory(String subCategory) {
		SubCategory = subCategory;
	}
	public String getShareCount() {
		return ShareCount;
	}
	public void setShareCount(String shareCount) {
		ShareCount = shareCount;
	}
	public Map<String, String> getTargetMap() {
		return targetMap;
	}
	public void setTargetMap(Map<String, String> targetMap) {
		this.targetMap = targetMap;
	}
	public String getUsage() {
		return Usage;
	}
	public void setUsage(String usage) {
		Usage = usage;
	}
	public StickerShareUserData(String date, String timeStamp, String applicationId, String usage, String platform,
			String userType, String appType, String stickerID, String stickerURL, String category, String subCategory,
			String shareCount, Map<String, String> targetMap) {
		super();
		Date = date;
		TimeStamp = timeStamp;
		ApplicationId = applicationId;
		Usage = usage;
		Platform = platform;
		UserType = userType;
		AppType = appType;
		StickerID = stickerID;
		StickerURL = stickerURL;
		Category = category;
		SubCategory = subCategory;
		ShareCount = shareCount;
		this.targetMap = targetMap;
	}	
}
