package com.saveyra.analytics.stickerShareHelper;

import java.util.Map;

public class StickerShareData {
	private String Date;
	private String Platform;
	private String StickerID;
	private String Category;
	private String SubCategory;
	private String Language;
	private long Downloaded;
	private long Liked;
	private long UnLiked;
	private long ShareCount;
	private long ViewCount;
	private String Type;
	/*private Map<String,String> targetMap;*/
	
	
	
	public String getDate() {
		return Date;
	}
	public StickerShareData(String date, String platform, String stickerID, String category, String subCategory,
			String language, long downloaded, long liked, long unLiked, long shareCount, long viewCount, String type) {
		super();
		Date = date;
		Platform = platform;
		StickerID = stickerID;
		Category = category;
		SubCategory = subCategory;
		Language = language;
		Downloaded = downloaded;
		Liked = liked;
		UnLiked = unLiked;
		ShareCount = shareCount;
		ViewCount = viewCount;
		Type = type;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public String getCategory() {
		return Category;
	}
	public void setCategory(String category) {
		Category = category;
	}
	public String getSubCategory() {
		return SubCategory;
	}
	public void setSubCategory(String subCategory) {
		SubCategory = subCategory;
	}
	public long getShareCount() {
		return ShareCount;
	}
	public void setShareCount(long shareCount) {
		ShareCount = shareCount;
	}
	public long getViewCount() {
		return ViewCount;
	}
	public void setViewCount(long viewCount) {
		ViewCount = viewCount;
	}
	public StickerShareData() {
		super();
	}
	public String getStickerID() {
		return StickerID;
	}
	public void setStickerID(String stickerID) {
		StickerID = stickerID;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	/*public Map<String,String> getTargetMap() {
		return targetMap;
	}
	public void setTargetMap(Map<String,String> targetMap) {
		this.targetMap = targetMap;
	}*/

	public long getDownloaded() {
		return Downloaded;
	}
	public void setDownloaded(long downloaded) {
		Downloaded = downloaded;
	}
	public long getLiked() {
		return Liked;
	}
	public void setLiked(long liked) {
		Liked = liked;
	}
	public long getUnLiked() {
		return UnLiked;
	}
	public void setUnLiked(long unLiked) {
		UnLiked = unLiked;
	}
	
	public String getLanguage() {
		return Language;
	}
	public void setLanguage(String language) {
		Language = language;
	}
	/*public StickerShareData(String date, String platform, String stickerID, String category, String subCategory,
			String language, long downloaded, long liked, long unLiked, long shareCount, long viewCount, String type,
			Map<String, String> targetMap) {
		super();
		Date = date;
		Platform = platform;
		StickerID = stickerID;
		Category = category;
		SubCategory = subCategory;
		Language = language;
		Downloaded = downloaded;
		Liked = liked;
		UnLiked = unLiked;
		ShareCount = shareCount;
		ViewCount = viewCount;
		Type = type;
		this.targetMap = targetMap;
	}	*/
}
