package com.saveyra.analytics.userTypeHelper;

public class UserTypeExtended {
	private String Date;
	private String Platform;
	private long NewUser_Event;
	private long NewUser_User;
	private long ReturningUser;

	
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public long getNewUser_Event() {
		return NewUser_Event;
	}
	public void setNewUser_Event(long newUser_Event) {
		NewUser_Event = newUser_Event;
	}
	public long getNewUser_User() {
		return NewUser_User;
	}
	public void setNewUser_User(long newUser_User) {
		NewUser_User = newUser_User;
	}		
	public long getReturningUser() {
		return ReturningUser;
	}
	public void setReturningUser(long returningUser) {
		ReturningUser = returningUser;
	}
	public UserTypeExtended(String date, String platform, long newUser_Event, long newUser_User, long returningUser) {
		super();
		Date = date;
		Platform = platform;
		NewUser_Event = newUser_Event;
		NewUser_User = newUser_User;
		ReturningUser = returningUser;
	}	
}
