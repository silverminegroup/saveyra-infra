package com.saveyra.analytics.userTypeHelper;

public class UserTypeData {
	
	private String Date;
	private String Platform;
	private long NewUser;
	private long ReturningUser;
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public long getNewUser() {
		return NewUser;
	}
	public void setNewUser(long newUser) {
		NewUser = newUser;
	}
	public long getReturningUser() {
		return ReturningUser;
	}
	public void setReturningUser(long returningUser) {
		ReturningUser = returningUser;
	}
	public UserTypeData(String date, String platform, long newUser, long returningUser) {
		super();
		Date = date;
		Platform = platform;
		NewUser = newUser;
		ReturningUser = returningUser;
	}

}
