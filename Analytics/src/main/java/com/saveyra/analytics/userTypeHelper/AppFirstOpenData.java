package com.saveyra.analytics.userTypeHelper;

public class AppFirstOpenData {
	private String Date;
	private String Platform;
	private long EventLevel;
	private long UserLevel;
	
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public long getEventLevel() {
		return EventLevel;
	}
	public void setEventLevel(long eventLevel) {
		EventLevel = eventLevel;
	}
	public long getUserLevel() {
		return UserLevel;
	}
	public void setUserLevel(long userLevel) {
		UserLevel = userLevel;
	}
	public AppFirstOpenData(String date, String platform, long eventLevel, long userLevel) {
		super();
		Date = date;
		Platform = platform;
		EventLevel = eventLevel;
		UserLevel = userLevel;
	}
}
