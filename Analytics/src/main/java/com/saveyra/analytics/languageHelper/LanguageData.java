package com.saveyra.analytics.languageHelper;

import java.util.HashMap;
import java.util.Map;

public class LanguageData {
	private String Date;
	private String Platform;
	private HashMap<String,Integer> targetMap;
	private long Male;
	private long Female;
	
	public LanguageData(String date, String platform, HashMap<String, Integer> lang, long male, long female) {
		super();
		Date = date;
		Platform = platform;
		this.targetMap = lang;
		Male = male;
		Female = female;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public String getPlatform() {
		return Platform;
	}

	public void setPlatform(String platform) {
		Platform = platform;
	}

	public Map<String, Integer> getLang() {
		return targetMap;
	}

	public void setLang(HashMap<String, Integer> lang) {
		this.targetMap = lang;
	}

	public long getMale() {
		return Male;
	}

	public void setMale(long male) {
		Male = male;
	}

	public long getFemale() {
		return Female;
	}

	public void setFemale(long female) {
		Female = female;
	}	
}
