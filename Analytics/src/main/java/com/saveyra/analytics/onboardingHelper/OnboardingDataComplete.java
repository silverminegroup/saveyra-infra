package com.saveyra.analytics.onboardingHelper;

public class OnboardingDataComplete {
	
	private String Date;
	private String Platform;
	private long LanguageScreenViewed;
	private long LanguageScreenCompeted;
	private long GenderScreenViewed;
	private long GenderScreenCompleted;
	private long OnboardingViewed;
	private long OnboardingCompleted;
	
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public long getLanguageScreenViewed() {
		return LanguageScreenViewed;
	}
	public void setLanguageScreenViewed(long languageScreenViewed) {
		LanguageScreenViewed = languageScreenViewed;
	}
	public long getLanguageScreenCompeted() {
		return LanguageScreenCompeted;
	}
	public void setLanguageScreenCompeted(long languageScreenCompeted) {
		LanguageScreenCompeted = languageScreenCompeted;
	}
	public long getGenderScreenViewed() {
		return GenderScreenViewed;
	}
	public void setGenderScreenViewed(long genderScreenViewed) {
		GenderScreenViewed = genderScreenViewed;
	}
	public long getGenderScreenCompleted() {
		return GenderScreenCompleted;
	}
	public void setGenderScreenCompleted(long genderScreenCompleted) {
		GenderScreenCompleted = genderScreenCompleted;
	}
	public long getOnboardingViewed() {
		return OnboardingViewed;
	}
	public void setOnboardingViewed(long onboardingViewed) {
		OnboardingViewed = onboardingViewed;
	}
	public long getOnboardingCompleted() {
		return OnboardingCompleted;
	}
	public void setOnboardingCompleted(long onboardingCompleted) {
		OnboardingCompleted = onboardingCompleted;
	}
	public OnboardingDataComplete(String date, String platform, long languageScreenViewed, long languageScreenCompeted,
			long genderScreenViewed, long genderScreenCompleted, long onboardingViewed, long onboardingCompleted) {
		super();
		Date = date;
		Platform = platform;
		LanguageScreenViewed = languageScreenViewed;
		LanguageScreenCompeted = languageScreenCompeted;
		GenderScreenViewed = genderScreenViewed;
		GenderScreenCompleted = genderScreenCompleted;
		OnboardingViewed = onboardingViewed;
		OnboardingCompleted = onboardingCompleted;
	}	
}
