package com.saveyra.analytics.utility;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

public class Test {
	
	public static void main(String args[]) {
		
		
		Map<String,ArrayList<String>> testMap = new LinkedHashMap<String,ArrayList<String>>();
		
		for(int i=0;i<5;i++) {
			List<String> myList = new ArrayList<String>();
			for(int j=0;j<5;j++) {
				myList.add("Randomm Data "+Math.random());
			}
			
			testMap.put("APPID"+i, (ArrayList<String>) myList);
		}
		
		System.out.println(new Gson().toJson(testMap));
	}

}
