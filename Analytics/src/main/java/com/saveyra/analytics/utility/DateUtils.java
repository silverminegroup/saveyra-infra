package com.saveyra.analytics.utility;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
	public static final String DB_FORMAT_DATETIME = "dd-MM-yyyy hh:mm:ss";        

    private DateUtils() {
        // not publicly instantiable
    }       

    public static Date getDate(String dateStr, String format) {
        final DateFormat formatter = new SimpleDateFormat(format);
        try {
            return formatter.parse(dateStr);
        } catch (ParseException e) {                
            return null;
        }
    }
}
