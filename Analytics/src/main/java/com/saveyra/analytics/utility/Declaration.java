package com.saveyra.analytics.utility;

import java.util.ArrayList;
import java.util.List;

public class Declaration {
	private static final int MAX_EVENT_CODE = 123;
	private static String exclusion_list = "";
	
	private static List<String> exclusionList = new ArrayList<String>();

	public static int getMaxEventCode() {
		return MAX_EVENT_CODE;
	}

	public static List<String> getExclusionList() {
		return exclusionList;
	}

	public static void setExclusionList(List<String> exclusionList) {
		Declaration.exclusionList = exclusionList;
	}

	public static String getExclusion_list() {
		return exclusion_list;
	}

	public static void setExclusion_list(String exclusion_list) {
		Declaration.exclusion_list = exclusion_list;
	}
}
