package com.saveyra.analytics.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import com.amazonaws.services.cloudwatch.model.Datapoint;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsRequest;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsResult;
import com.amazonaws.services.cloudwatch.model.StandardUnit;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class NotificationDataMetrics {
	final static long offsetInMilliseconds = 1000 * 60 * 60 * 24;
	final static Integer period = 60 * 60;
	final static BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAIIGSDYMQGEZV47ZA", "BR96gGn+HOVieXRydNwTxur7HsMSImaDvrMiK2cy");
	final static AmazonCloudWatch cw = AmazonCloudWatchClientBuilder.standard()
			.withCredentials(new AWSStaticCredentialsProvider(awsCreds)).withRegion("us-east-1").build();
	final static String namespace = "AWS/SNS";
	final static String deliveredMetricName = "NumberOfNotificationsDelivered";
	final static String failedMetricName = "NumberOfNotificationsFailed";
	final static String publishedMetricName = "NumberOfMessagesPublished";
	final static String[] statistics = {"Sum"};
	final static ObjectMapper mapper = new ObjectMapper();
	public static void main(String args[]) throws JsonProcessingException {
		
		List<Dimension> applications = new ArrayList<>();
		Dimension ios = new Dimension();
		ios.setName("Application");
		ios.setValue("Emoji_IOS");
		applications.add(ios);
		Dimension android = new Dimension();
		android.setName("Application");
		android.setValue("Saveyra_Android");
		applications.add(android);
		Map<String, Map<String, Map<Date, Double>>> byApplicationStatistics = new HashMap<>();
		//"byApplication":{"Emoji_IOS":{"published": 2.0, "delivered":2.0, "failed": 2.0}, "Saveyra_Android":{"published": 2.0, "delivered":2.0, "failed": 2.0}}"
		
		System.out.println("By Application: "+mapper.writeValueAsString(getStatisticsByApplication(applications, byApplicationStatistics)));
		
		List<Dimension> platforms = new ArrayList<>();
		Dimension iosPm = new Dimension();
		iosPm.setName("Platform");
		iosPm.setValue("APNS_SANDBOX");
		platforms.add(iosPm);
		Dimension androidPm = new Dimension();
		androidPm.setName("Platform");
		androidPm.setValue("GCM");
		platforms.add(androidPm);
		Map<String, Map<String, Map<Date, Double>>> byPlatformStatistics = new HashMap<>();
		//"byPlatform":{"GCM":{"published": 2.0, "delivered":2.0, "failed": 2.0}, "APNS_SANDBOX":{"published": 2.0, "delivered":2.0, "failed": 2.0}}"
		System.out.println("By Platform: "+mapper.writeValueAsString(getStatisticsByPlatform(platforms, byPlatformStatistics)));
	}
	
	private static Map<String, Map<String, Map<Date, Double>>> getStatisticsByApplication(List<Dimension> applications, Map<String, Map<String, Map<Date, Double>>> byApplicationStatistics) {
		applications.forEach(byApplication->{
			Map<String, Map<Date, Double>> byApplicationMetricStatistics = new HashMap<>();
			Map<Date, Double> publishedStatistics = getStatisticsData(namespace, publishedMetricName, statistics, byApplication);
			byApplicationMetricStatistics.put("published", publishedStatistics);
			Map<Date, Double> deliveredStatistics = getStatisticsData(namespace, deliveredMetricName, statistics, byApplication);
			byApplicationMetricStatistics.put("delivered", deliveredStatistics);
			Map<Date, Double> failedStatistics = getStatisticsData(namespace, failedMetricName, statistics, byApplication);
			byApplicationMetricStatistics.put("failed", failedStatistics);
			byApplicationStatistics.put(byApplication.getValue(), byApplicationMetricStatistics);
		});
		return byApplicationStatistics;
	}
	
	private static Map<String, Map<String, Map<Date, Double>>> getStatisticsByPlatform(List<Dimension> platforms, Map<String, Map<String, Map<Date, Double>>> byPlatformStatistics) {
		platforms.forEach(byPlatform->{
			Map<String, Map<Date, Double>> byPlatformMetricStatistics = new HashMap<>();
			Map<Date, Double> publishedStatistics = getStatisticsData(namespace, publishedMetricName, statistics, byPlatform);
			byPlatformMetricStatistics.put("published", publishedStatistics);
			Map<Date, Double> deliveredStatistics = getStatisticsData(namespace, deliveredMetricName, statistics, byPlatform);
			byPlatformMetricStatistics.put("delivered", deliveredStatistics);
			Map<Date, Double> failedStatistics = getStatisticsData(namespace, failedMetricName, statistics, byPlatform);
			byPlatformMetricStatistics.put("failed", failedStatistics);
			byPlatformStatistics.put(byPlatform.getValue(), byPlatformMetricStatistics);
		});
		return byPlatformStatistics;
	}
	
	private static Map<Date, Double> getStatisticsData(String namespace, String metricName, String[] statistics,
			Dimension filters) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		Calendar c = Calendar.getInstance();
	    c.setTime(new Date(new Date().getTime() - offsetInMilliseconds));
	    c.add(Calendar.DAY_OF_MONTH, -7);

	    c.set(Calendar.HOUR_OF_DAY, 0);
	    c.set(Calendar.MINUTE, 0);
	    c.set(Calendar.SECOND, 0);
	    Date start = c.getTime();
	    
	    System.out.println("startTime: "+start);
	    
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(new Date());
	    cal.set(Calendar.HOUR_OF_DAY, 0);
	    cal.set(Calendar.MINUTE, 0);
	    cal.set(Calendar.SECOND, 0);
	    Date end = cal.getTime();
	    
	    System.out.println("endTime: "+end);
	    
		GetMetricStatisticsRequest request;
		try {
			request = new GetMetricStatisticsRequest()
					.withStartTime(sdf.parse(sdf.format(start))).withNamespace(namespace)
					.withPeriod(period).withMetricName(metricName).withStatistics(statistics).withUnit(StandardUnit.Count)
					.withDimensions(filters).withEndTime(sdf.parse(sdf.format(end)));
			GetMetricStatisticsResult response = cw.getMetricStatistics(request);

			// To read the Data
			Map<Date, Double> sumStatisticsData = new HashMap<>();
			for (Datapoint dp : response.getDatapoints()) {
				sumStatisticsData.put(dp.getTimestamp(), dp.getSum());
			}
			return sumStatisticsData;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
}
