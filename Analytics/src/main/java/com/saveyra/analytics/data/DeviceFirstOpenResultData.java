package com.saveyra.analytics.data;

import java.util.HashMap;
import java.util.Map;

public class DeviceFirstOpenResultData {
	private Map<String, Map<String, Integer>> fingerPrint = new HashMap<String, Map<String, Integer>>();
	private Map<String, Integer> nonFingerPrint = new HashMap<String, Integer>();
	private long totalCount;
	private long fingerPrintCount;
	private long nonFingerPrintCount;

	/**
	 * @return the fingerPrint
	 */
	public Map<String, Map<String, Integer>> getFingerPrint() {
		return fingerPrint;
	}

	/**
	 * @param fingerPrint the fingerPrint to set
	 */
	public void setFingerPrint(Map<String, Map<String, Integer>> fingerPrint) {
		this.fingerPrint = fingerPrint;
	}

	/**
	 * @return the nonFingerPrint
	 */
	public Map<String, Integer> getNonFingerPrint() {
		return nonFingerPrint;
	}

	/**
	 * @param nonFingerPrint the nonFingerPrint to set
	 */
	public void setNonFingerPrint(Map<String, Integer> nonFingerPrint) {
		this.nonFingerPrint = nonFingerPrint;
	}

	/**
	 * @return the totalCount
	 */
	public long getTotalCount() {
		return totalCount;
	}

	/**
	 * @param totalCount the totalCount to set
	 */
	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	/**
	 * @return the fingerPrintCount
	 */
	public long getFingerPrintCount() {
		return fingerPrintCount;
	}

	/**
	 * @param fingerPrintCount the fingerPrintCount to set
	 */
	public void setFingerPrintCount(long fingerPrintCount) {
		this.fingerPrintCount = fingerPrintCount;
	}

	/**
	 * @return the nonFingerPrintCount
	 */
	public long getNonFingerPrintCount() {
		return nonFingerPrintCount;
	}

	/**
	 * @param nonFingerPrintCount the nonFingerPrintCount to set
	 */
	public void setNonFingerPrintCount(long nonFingerPrintCount) {
		this.nonFingerPrintCount = nonFingerPrintCount;
	}
}
