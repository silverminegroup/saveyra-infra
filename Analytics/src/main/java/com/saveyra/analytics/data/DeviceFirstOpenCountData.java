package com.saveyra.analytics.data;

import java.util.HashMap;
import java.util.Map;

public class DeviceFirstOpenCountData {
	private Map<String, Integer> fingerPrint = new HashMap<String, Integer>();
	private Map<String, Integer> nonFingerPrint = new HashMap<String, Integer>();

	/**
	 * @return the fingerPrint
	 */
	public Map<String, Integer> getFingerPrint() {
		return fingerPrint;
	}

	/**
	 * @param fingerPrint the fingerPrint to set
	 */
	public void setFingerPrint(Map<String, Integer> fingerPrint) {
		this.fingerPrint = fingerPrint;
	}

	/**
	 * @return the nonFingerPrint
	 */
	public Map<String, Integer> getNonFingerPrint() {
		return nonFingerPrint;
	}

	/**
	 * @param nonFingerPrint the nonFingerPrint to set
	 */
	public void setNonFingerPrint(Map<String, Integer> nonFingerPrint) {
		this.nonFingerPrint = nonFingerPrint;
	}
}
