package com.saveyra.analytics.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StatisticsData {
	@JsonProperty("Date")
	private String date;
	@JsonProperty("Published")
	private Integer published;
	@JsonProperty("Delivered")
	private Integer delivered;
	@JsonProperty("Failed")
	private Integer failed;
	@JsonProperty("Platform")
	private String platform;


	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the published
	 */
	public Integer getPublished() {
		return published;
	}

	/**
	 * @param published the published to set
	 */
	public void setPublished(Integer published) {
		this.published = published;
	}

	/**
	 * @return the delivered
	 */
	public Integer getDelivered() {
		return delivered;
	}

	/**
	 * @param delivered the delivered to set
	 */
	public void setDelivered(Integer delivered) {
		this.delivered = delivered;
	}

	/**
	 * @return the failed
	 */
	public Integer getFailed() {
		return failed;
	}

	/**
	 * @param failed the failed to set
	 */
	public void setFailed(Integer failed) {
		this.failed = failed;
	}

	/**
	 * @return the platform
	 */
	public String getPlatform() {
		return platform;
	}

	/**
	 * @param platform the platform to set
	 */
	public void setPlatform(String platform) {
		this.platform = platform;
	}
}
