package com.saveyra.analytics.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.saveyra.analytics.DAO.UsageDataDAO;
import com.saveyra.analytics.model.NotificationCenter;
import com.saveyra.analytics.repo.MysqlRepo;

@RestController
@RequestMapping("/")
@EnableAutoConfiguration
public class APIController {


	@Autowired
	UsageDataDAO usageDataDAO;

	@Autowired
	MysqlRepo repo;
	
	private static final Logger logger = LoggerFactory.getLogger(APIController.class);
	
	@CrossOrigin
	@PostMapping("/getUserBasedNotification")
	public ResponseEntity<String> getUserBasedNotification(@Valid @RequestBody String json, @RequestHeader(name="TIMEZONE", required=false) String timezone) throws IOException {
		logger.info("\n[Enter] getUserBasedNotification "+ json);
		HttpHeaders respHeader = new HttpHeaders();
		respHeader.set("Content-Type", "application/json");
		List<NotificationCenter> notList = new ArrayList<NotificationCenter>();;	
		try {
			final ObjectNode node = new ObjectMapper().readValue(json, ObjectNode.class);
			if (node.has("applicationId") == false) {
				ResponseEntity<String> resp = new ResponseEntity<String>("{\"status\":\"Error\",\"message\":\"applicationId key not found\"}", respHeader, HttpStatus.BAD_REQUEST);
				return resp;
			}
			if (node.has("language") == false) {			
				ResponseEntity<String> resp = new ResponseEntity<String>("{\"status\":\"Error\",\"message\":\"Language key not found\"}", respHeader, HttpStatus.BAD_REQUEST);
				return resp;
			}
			
			// If no time zone from the client, get it from database.
			if (timezone == null) {
				timezone = repo.findUserTimezoneByAppId(node.get("applicationId").toString().split("\"")[1]);
			}	

			//Validate time zone. If timezone is not found in the default timezone list, using default timezone 'Asia/Kolkata'
			String [] timezoneIDs = TimeZone.getAvailableIDs();
			List<String> timezoneNames = Arrays.asList(timezoneIDs);
			if (timezoneNames.contains(timezone) == false) {
				String defaultTimezone = "Asia/Kolkata";
				logger.info("Set default timezone "+defaultTimezone+" as client sent invalid timezone {}", timezone);
				timezone = defaultTimezone;
			}
			
			if (node.has("language")) {			
				notList = repo.getNotificationCenter(node.get("language").toString().split("\"")[1], timezone);	
			}	
			
			if (node.has("applicationId")) {
				//System.out.println("APP ID "+node.get("applicationId").toString());
				for(NotificationCenter nc : notList) {
					boolean x = usageDataDAO.isNotificationViewed(node.get("applicationId").toString().split("\"")[1],nc.getId()+"");
					nc.setRead(x);
				}
			}
		} catch(Exception e) {
			logger.error("Error in getting notifications for notifcation center");
		}
		ResponseEntity<String> resp = new ResponseEntity<String>(new Gson().toJson(notList), respHeader, HttpStatus.OK);
		logger.info("\n[Leave] getUserBasedNotification "+resp);
		return resp;
		
	}
}
