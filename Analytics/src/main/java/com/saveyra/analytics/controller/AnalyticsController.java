package com.saveyra.analytics.controller;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.saveyra.analytics.DAO.UsageDataDAO;
import com.saveyra.analytics.UserActivityHelper.ActiveUserData;
import com.saveyra.analytics.UserActivityHelper.OnboardingData;
import com.saveyra.analytics.UserActivityHelper.UserActivityHelper;
import com.saveyra.analytics.avatarHelper.AvatarReport;
import com.saveyra.analytics.avatarHelper.GenderReport;
import com.saveyra.analytics.categoryHelper.CategoryData;
import com.saveyra.analytics.categoryHelper.CategoryDataTotal;
import com.saveyra.analytics.data.DeviceFirstOpenCountData;
import com.saveyra.analytics.data.DeviceFirstOpenResultData;
import com.saveyra.analytics.data.RequestData;
import com.saveyra.analytics.data.RestResponse;
import com.saveyra.analytics.graph.PlatfromUserTotalHelper;
import com.saveyra.analytics.graph.RegisteredUserHelper;
import com.saveyra.analytics.imageHelper.ImageData;
import com.saveyra.analytics.keyboardHelper.KeyboardDetail;
import com.saveyra.analytics.keyboardHelper.PlatformKeyboard;
import com.saveyra.analytics.keywordHelper.KeywordSearch;
import com.saveyra.analytics.languageHelper.LanguageData;
import com.saveyra.analytics.model.MysqlFilterResponse;
import com.saveyra.analytics.notificationHelper.NotificationData;
import com.saveyra.analytics.repo.MysqlRepo;
import com.saveyra.analytics.repository.PersonRepository;
import com.saveyra.analytics.screenviewHelper.ScreenViewData;
import com.saveyra.analytics.searchHelper.SearchData;
import com.saveyra.analytics.signUpHelper.PlatformData;
import com.saveyra.analytics.signUpHelper.PlatformDataByDate;
import com.saveyra.analytics.signupmobileHelper.SignUpFBData;
import com.saveyra.analytics.signupmobileHelper.SignUpMobileData;
import com.saveyra.analytics.stickerShareHelper.StickerShareData;
import com.saveyra.analytics.targetApp.ShareTargetData;
import com.saveyra.analytics.userTypeHelper.AppFirstOpenData;
import com.saveyra.analytics.userTypeHelper.UserTypeData;

@RestController
@RequestMapping("/")
@EnableAutoConfiguration
public class AnalyticsController {

	@Autowired
	UsageDataDAO usageDataDAO;

	@Autowired
	PersonRepository perRep;

	@Autowired
	MysqlRepo repo;

	// Member Declarations
	private static final Logger logger = LoggerFactory.getLogger(AnalyticsController.class);

	// ################################## NOTIFICATION API START
	// ##########################
	
	@CrossOrigin
	@RequestMapping(value = "/getNewUserReport", method = RequestMethod.POST)
	public String getNewUserReport(@RequestBody String request) {
		logger.info("[INFO] Get getNewUserReport report");		
		Date date = new Date();		
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");		
		String startDate = formatter.format(date);
		//System.out.println("\n\n DATE "+startDate);
		Gson gson = new Gson();
		return gson.toJson(repo.findNewUsersByDate(startDate));
	}
	
	
	/*@CrossOrigin
	@RequestMapping(value = "/getAppFirstOpenReport", method = RequestMethod.POST)
	public String getAppFirstOpenReport(@RequestBody String request) {
		logger.info("[INFO] Get App Open report for " + request);
		String gender = null;
		String platform = null;
		String frequency = null;
		String age_grp = null;
		Gson gson = new Gson();
		try {
			final ObjectNode node = new ObjectMapper().readValue(request, ObjectNode.class);
			if (node.has("gender")) {
				gender = node.get("gender").toString();
			}
			if (node.has("platform")) {
				platform = node.get("platform").toString();
			}
			if (node.has("frequency")) {
				frequency = node.get("frequency").toString();
			}
			if (node.has("age_grp")) {
				age_grp = node.get("age_grp").toString();
			}
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		// String urlString =
		// "https://saveyra.com/saveyra/v1/users/notification?access_token=e2cf3ff0-8661-11e8-adc0-fa7ae01bbebc";
		RestTemplate restTemplate = new RestTemplate();
		// create request body
		JsonObject requestSend = new JsonObject();
		requestSend.addProperty("gender", gender);
		requestSend.addProperty("platform", platform);
		requestSend.addProperty("frequency", frequency);
		requestSend.addProperty("age_grp", age_grp);

		// set headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		// send request and parse result
		ResponseEntity<String> loginResponse = restTemplate.exchange(pythonURI, HttpMethod.POST, entity, String.class);
		String userJson = "";
		if (loginResponse.getStatusCode() == HttpStatus.OK) {
			userJson = gson.toJson(loginResponse.getBody());
		} else if (loginResponse.getStatusCode() == HttpStatus.UNAUTHORIZED) {
			userJson = gson.toJson("{\"message\",\"Error\"}");
			return userJson;
		} else  {
			logger.error("[ERROR] Failed to get response");
			userJson = gson.toJson("{\"message\",\"Error\"}");
			return userJson;
		}
		
		if(userJson!=null && userJson.contains("\\\"")) {
			userJson = userJson.replace("\\\"", "\"");
		}
		if(userJson!=null && userJson.contains("\"[")) {
			userJson = userJson.replace("\"[", "[");
		}
		if(userJson!=null && userJson.contains("]\"")) {
			userJson = userJson.replace("]\"", "]");
		}	

		Type listType = new TypeToken<List<MysqlFilterResponse>>() {
		}.getType();
		List<MysqlFilterResponse> mResp = new Gson().fromJson(userJson, listType);

		List<MysqlFilterResponse> sendList = new ArrayList<MysqlFilterResponse>();

		for (MysqlFilterResponse mfr : mResp) {
			if (usageDataDAO.isAppFirstOpen(mfr.getApplicationId(), new Date())) {
				sendList.add(mfr);
			}
		}
		return userJson;
	}

	@CrossOrigin
	@RequestMapping(value = "/getGuestUserList", method = RequestMethod.POST)
	public String getGuestUserList(@RequestBody String request) {
		logger.info("[INFO] Get guest user report for " + request);
		String gender = null;
		String platform = null;
		String frequency = null;
		String age_grp = null;
		Gson gson = new Gson();
		try {
			final ObjectNode node = new ObjectMapper().readValue(request, ObjectNode.class);
			if (node.has("gender")) {
				gender = node.get("gender").toString();
			}
			if (node.has("platform")) {
				platform = node.get("platform").toString();
			}
			if (node.has("frequency")) {
				frequency = node.get("frequency").toString();
			}
			if (node.has("age_grp")) {
				age_grp = node.get("age_grp").toString();
			}
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		// String urlString =
		// "https://saveyra.com/saveyra/v1/users/notification?access_token=e2cf3ff0-8661-11e8-adc0-fa7ae01bbebc";
		RestTemplate restTemplate = new RestTemplate();
		// create request body
		JsonObject requestSend = new JsonObject();
		requestSend.addProperty("gender", gender);
		requestSend.addProperty("platform", platform);
		requestSend.addProperty("frequency", frequency);
		requestSend.addProperty("age_grp", age_grp);

		// set headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		// send request and parse result
		ResponseEntity<String> loginResponse = restTemplate.exchange(pythonURI, HttpMethod.POST, entity, String.class);
		String userJson = null;
		if (loginResponse.getStatusCode() == HttpStatus.OK) {
			userJson = gson.toJson(loginResponse.getBody());
		} else if (loginResponse.getStatusCode() == HttpStatus.UNAUTHORIZED) {
			userJson = gson.toJson("{\"message\",\"Error\"}");
			return userJson;
		} else {
			logger.error("[ERROR] Failed to Get Response");
			userJson = gson.toJson("{\"message\",\"Error\"}");
			return userJson;
		}
		
		if(userJson.contains("\\\"")) {
			userJson = userJson.replace("\\\"", "\"");
		}
		if(userJson.contains("\"[")) {
			userJson = userJson.replace("\"[", "[");
		}
		if(userJson.contains("]\"")) {
			userJson = userJson.replace("]\"", "]");
		}	

		Type listType = new TypeToken<List<MysqlFilterResponse>>() {
		}.getType();
		List<MysqlFilterResponse> mResp = new Gson().fromJson(userJson, listType);
		Set<MysqlFilterResponse> sendList = new HashSet<MysqlFilterResponse>();

		for (MysqlFilterResponse mfr : mResp) {
			if (usageDataDAO.isGuestUser(mfr.getApplicationId())) {
				sendList.add(mfr);
			}
		}
		return gson.toJson(sendList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getKeyboardNotConnectedList", method = RequestMethod.POST)
	public String getKeyboardNotConnectedList(@RequestBody String request) {
		logger.info("[INFO] Get App Open report for " + request);
		String gender = null;
		String platform = null;
		String frequency = null;
		String age_grp = null;
		Gson gson = new Gson();
		try {
			final ObjectNode node = new ObjectMapper().readValue(request, ObjectNode.class);
			if (node.has("gender")) {
				gender = node.get("gender").toString();
			}
			if (node.has("platform")) {
				platform = node.get("platform").toString();
			}
			if (node.has("frequency")) {
				frequency = node.get("frequency").toString();
			}
			if (node.has("age_grp")) {
				age_grp = node.get("age_grp").toString();
			}
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		// String urlString =
		// "https://saveyra.com/saveyra/v1/users/notification?access_token=e2cf3ff0-8661-11e8-adc0-fa7ae01bbebc";

		RestTemplate restTemplate = new RestTemplate();
		// create request body
		JsonObject requestSend = new JsonObject();
		requestSend.addProperty("gender", gender);
		requestSend.addProperty("platform", platform);
		requestSend.addProperty("frequency", frequency);
		requestSend.addProperty("age_grp", age_grp);

		// set headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		// send request and parse result
		ResponseEntity<String> loginResponse = restTemplate.exchange(pythonURI, HttpMethod.POST, entity, String.class);
		String userJson = null;
		if (loginResponse.getStatusCode() == HttpStatus.OK) {
			userJson = gson.toJson(loginResponse.getBody());
		} else if (loginResponse.getStatusCode() == HttpStatus.UNAUTHORIZED) {
			userJson = gson.toJson("{\"message\",\"Error\"}");
			return userJson;
		} else {
			logger.error("[ERROR] Failed to Get Response");
			userJson = gson.toJson("{\"message\",\"Error\"}");
			return userJson;
		}
		
		if(userJson.contains("\\\"")) {
			userJson = userJson.replace("\\\"", "\"");
		}
		if(userJson.contains("\"[")) {
			userJson = userJson.replace("\"[", "[");
		}
		if(userJson.contains("]\"")) {
			userJson = userJson.replace("]\"", "]");
		}	



		Type listType = new TypeToken<List<MysqlFilterResponse>>() {
		}.getType();
		List<MysqlFilterResponse> mResp = new Gson().fromJson(userJson, listType);

		Set<MysqlFilterResponse> sendList = new HashSet<MysqlFilterResponse>();

		for (MysqlFilterResponse mfr : mResp) {
			if (!usageDataDAO.isKeyboardConnected(mfr.getApplicationId())) {
				sendList.add(mfr);
			}
		}
		return gson.toJson(sendList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getAvatarNotCreatedList", method = RequestMethod.POST)
	public String getAvatarNotCreatedList(@RequestBody String request) {
		logger.info("[INFO] Get App Open report for " + request);
		String gender = null;
		String platform = null;
		String frequency = null;
		String age_grp = null;
		Gson gson = new Gson();
		try {
			final ObjectNode node = new ObjectMapper().readValue(request, ObjectNode.class);
			if (node.has("gender")) {
				gender = node.get("gender").toString();
			}
			if (node.has("platform")) {
				platform = node.get("platform").toString();
			}
			if (node.has("frequency")) {
				frequency = node.get("frequency").toString();
			}
			if (node.has("age_grp")) {
				age_grp = node.get("age_grp").toString();
			}
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		// String urlString =
		// "https://saveyra.com/saveyra/v1/users/notification?access_token=e2cf3ff0-8661-11e8-adc0-fa7ae01bbebc";
		RestTemplate restTemplate = new RestTemplate();
		// create request body
		JsonObject requestSend = new JsonObject();
		requestSend.addProperty("gender", gender);
		requestSend.addProperty("platform", platform);
		requestSend.addProperty("frequency", frequency);
		requestSend.addProperty("age_grp", age_grp);

		// set headers
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		// send request and parse result
		ResponseEntity<String> loginResponse = restTemplate.exchange(pythonURI, HttpMethod.POST, entity, String.class);
		String userJson = null;
		if (loginResponse.getStatusCode() == HttpStatus.OK) {
			userJson = gson.toJson(loginResponse.getBody());
		} else if (loginResponse.getStatusCode() == HttpStatus.UNAUTHORIZED) {
			userJson = gson.toJson("{\"message\",\"Error\"}");
			return userJson;
		} else {
			logger.error("[ERROR] Failed to get response");
			userJson = gson.toJson("{\"message\",\"Error\"}");
			return userJson;
		}
		if(userJson.contains("\\\"")) {
			userJson = userJson.replace("\\\"", "\"");
		}
		if(userJson.contains("\"[")) {
			userJson = userJson.replace("\"[", "[");
		}
		if(userJson.contains("]\"")) {
			userJson = userJson.replace("]\"", "]");
		}	

		
		Type listType = new TypeToken<List<MysqlFilterResponse>>() {
		}.getType();
		List<MysqlFilterResponse> mResp = new Gson().fromJson(userJson, listType);
		Set<MysqlFilterResponse> sendList = new HashSet<MysqlFilterResponse>();
		for (MysqlFilterResponse mfr : mResp) {
			if (!usageDataDAO.isAvatarCreated(mfr.getApplicationId())) {
				sendList.add(mfr);
			}
		}
		return gson.toJson(sendList);
	}*/

	@CrossOrigin
	@RequestMapping(value = "/getGuestUserListTest-{userID}", method = RequestMethod.POST)
	public String getGuestUserListTest(@PathVariable String userID) {
		logger.info("[INFO] Get Guest User Open report for " + userID);
		// //System.out.println(userID);

		if (usageDataDAO.isGuestUser(userID)) {
			return "isGuest";
		} else {
			return "notGuest";
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/getKeyboardNotConnectedListTest-{userID}", method = RequestMethod.POST)
	public String getKeyboardNotConnectedListTest(@PathVariable String userID) {
		logger.info("[INFO] Get Guest User Open report for " + userID);
		// //System.out.println(userID);

		if (usageDataDAO.isKeyboardConnected(userID)) {
			return "isConnected";
		} else {
			return "notConnected";
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/getAvatarNotCreatedListTest-{userID}", method = RequestMethod.POST)
	public String getAvatarNotCreatedListTest(@PathVariable String userID) {
		logger.info("[INFO] Get Guest User Open report for " + userID);
		// //System.out.println(userID);

		if (usageDataDAO.isAvatarCreated(userID)) {
			return "Created";
		} else {
			return "notCreated";
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/getAppFirstOpenListTest-{AppID}", method = RequestMethod.POST)
	public String getAppFirstOpenListTest(@PathVariable String AppID) {
		logger.info("[INFO] Get App First Open report for " + AppID);
		// //System.out.println(AppID);

		if (usageDataDAO.isAppFirstOpen(AppID, new Date())) {
			return "Created";
		} else {
			return "notCreated";
		}
	}

	// ################################## NOTIFICATION API END
	// ##########################

	// ################################### ANALYTICS API START
	// ##########################

	@CrossOrigin
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String test() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<String> list = null;

		usageDataDAO.findAllAppIDWithMinCount();

		/*
		 * list = repo.findAllUsers(sDate); int count = 0; for(String s:list) { count++;
		 * ////System.out.println("App ID : "+s); }
		 * 
		 * ////System.out.println("Total Count : "+count);
		 */

		/*
		 * try { logger.info("\n\n Android");
		 * usageDataDAO.getAllImageIDsBydateAndPlatform(sdf.parse("2018-10-18"),
		 * "Android"); logger.info("\n\n IOS");
		 * usageDataDAO.getAllImageIDsBydateAndPlatform(sdf.parse("2018-10-18"), "iOS");
		 * logger.info("\n\n All");
		 * usageDataDAO.getAllImageIDsBydateAndPlatform(sdf.parse("2018-10-18"), "All");
		 * } catch (ParseException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */

		/*
		 * try { //list = usageDataDAO.getAllKeywordsByDate(sdf.parse("2018-10-05"),
		 * sdf.parse("2018-10-10")); } catch (ParseException e) { // TODO Auto-generated
		 * catch block e.printStackTrace(); }
		 */

		/*
		 * for (String s : list) { ////System.out.println("KEYWORD : >> " + s); try {
		 * //////System.out.println("\t\tCOUNT >>"+usageDataDAO.
		 * getKeywordSearchCountByDate( s,"All",sdf.parse("2018-10-05"),
		 * sdf.parse("2018-10-10"))); } catch (ParseException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); } //
		 * ////System.out.println("\t\tCOUNT >> //
		 * "+usageDataDAO.getKeywordSearchCount(s,"Android")); //
		 * ////System.out.println("\t\tCOUNT >> //
		 * "+usageDataDAO.getKeywordSearchCount(s,"iOS"));
		 * 
		 * }
		 */
		/*
		 * for(String s:list) { try { ////System.out.println("IMAGE ID : "+s);
		 * ////System.out.println("\tViewd : "+usageDataDAO.getImageIDCountViewed(s,
		 * sdf.parse("2018-09-30")));
		 * ////System.out.println("\tShared : "+usageDataDAO.getImageIDCountShared(s,
		 * sdf.parse("2018-09-30")));
		 * 
		 * ////System.out.println("\tViewd : "+usageDataDAO.getImageIDCountViewed(s,
		 * sdf.parse("2018-10-01")));
		 * ////System.out.println("\tShared : "+usageDataDAO.getImageIDCountShared(s,
		 * sdf.parse("2018-10-01")));
		 * 
		 * ////System.out.println("\tViewd : "+usageDataDAO.getImageIDCountViewed(s,
		 * sdf.parse("2018-10-02")));
		 * ////System.out.println("\tShared : "+usageDataDAO.getImageIDCountShared(s,
		 * sdf.parse("2018-10-02")));
		 * 
		 * ////System.out.println("\tViewd : "+usageDataDAO.getImageIDCountViewed(s,
		 * sdf.parse("2018-10-03")));
		 * ////System.out.println("\tShared : "+usageDataDAO.getImageIDCountShared(s,
		 * sdf.parse("2018-10-03")));
		 * 
		 * } catch (ParseException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } }
		 */

		/*
		 * 
		 * try {
		 * 
		 * 
		 * /*
		 * 
		 * ////System.out.println("[] usageDataDAO.getSignupCountByDate  " +
		 * usageDataDAO.getSignupCountByDate(sdf.parse("2018-10-03"), "Android", "FB"));
		 * ////System.out.println("[] usageDataDAO.getSignupCountByDate  " +
		 * usageDataDAO.getSignupCountByDate(sdf.parse("2018-10-03"), "Android",
		 * "MOBILE")); ////System.out.println("[] usageDataDAO.getSignupCountByDate  " +
		 * usageDataDAO.getSignupCountByDate(sdf.parse("2018-10-03"), "iOS", "FB"));
		 * ////System.out.println("[] usageDataDAO.getSignupCountByDate  " +
		 * usageDataDAO.getSignupCountByDate(sdf.parse("2018-10-03"), "iOS", "MOBILE"));
		 * ////System.out.println("[] usageDataDAO.getSignupCountByDate  " +
		 * usageDataDAO.getSignupCountByDate(sdf.parse("2018-10-03"), "All", "FB"));
		 * ////System.out.println("[] usageDataDAO.getSignupCountByDate  " +
		 * usageDataDAO.getSignupCountByDate(sdf.parse("2018-10-03"), "All", "MOBILE"));
		 * 
		 * } catch (ParseException e1) { // TODO Auto-generated catch block
		 * e1.printStackTrace(); }
		 */

		/*
		 * // perRep.findAvtarCreatedReportBydateTime(new Date()); List<UsageData>
		 * lUdata = new ArrayList<UsageData>(); List<UsageData> lUdata2 = new
		 * ArrayList<UsageData>(); List<UsageData> lUdata3 = new ArrayList<UsageData>();
		 * List<UsageData> lUdata4 = new ArrayList<UsageData>();
		 * 
		 * 
		 * 
		 * try {
		 * 
		 * ////System.out.println("[0] >> " +
		 * usageDataDAO.isAppFirstOpen("65c2d92b-0a54-4239-aadb-1bd866fc2911",
		 * sdf.parse("2018-10-02")));
		 * 
		 * ////System.out.println("[0.1] >> " +
		 * usageDataDAO.isAppFirstOpen("65c2d92b-0a54-4239-aadb-1bd866fc2911",
		 * sdf.parse("2018-10-03")));
		 * 
		 * lUdata = usageDataDAO.getBetweenDate1(sdf.parse("2018-10-01"),
		 * sdf.parse("2018-10-03")); ////System.out.println("[1] : getBetweenDate1 >> " +
		 * lUdata.size()); ////System.out.println("[2] : getBetweenDate1Count >> " +
		 * usageDataDAO.getBetweenDate1Count(sdf.parse("2018-10-01"),
		 * sdf.parse("2018-10-03"))); lUdata2 =
		 * usageDataDAO.getByDate2(sdf.parse("2018-10-03"));
		 * ////System.out.println("[3] : getByDate2 >> " + lUdata2.size());
		 * ////System.out.println("[4] : getByDate2Count >> " +
		 * usageDataDAO.getByDate2Count(sdf.parse("2018-10-03")));
		 * 
		 * // --
		 * 
		 * lUdata3 = usageDataDAO.getByDateAndEvent1(sdf.parse("2018-10-01"),
		 * sdf.parse("2018-10-03")); ////System.out.println("[5] : getByDateAndEvent1 >> "
		 * + lUdata3.size()); ////System.out.println("[6] : getByDateAndEvent1Count >> " +
		 * usageDataDAO.getByDateAndEvent1Count(sdf.parse("2018-10-01"),
		 * sdf.parse("2018-10-03")));
		 * 
		 * lUdata4 = usageDataDAO.getByDateAndEvent2(sdf.parse("2018-10-03"));
		 * ////System.out.println("[7] : getByDateAndEvent2 >> " + lUdata4.size());
		 * ////System.out.println("[8] : getByDateAndEvent2Count >> " +
		 * usageDataDAO.getByDateAndEvent2Count(sdf.parse("2018-10-03")));
		 * 
		 * } catch (ParseException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */
		return null;

	}

	/*@CrossOrigin
	@RequestMapping(value = "/getActiveUsersReportByDate", method = RequestMethod.POST)
	public String getActiveUsersReport(@RequestBody String request) {
		logger.info("[INFO] Get Active User report by date");
		logger.info("[INFO] Get User Type [New /Existing] report");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		ArrayList<ActiveUserData> dataList = new ArrayList<ActiveUserData>();
		ActiveUserData pdAndroid = null;
		ActiveUserData pdiOS = null;
		ActiveUserData pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				logger.info("[INFO] Fetching for Date : " + startDate.toString());
				Long lActiveUser = new Long(usageDataDAO.getTotalActiveUsersByDate("Android", startDate));
				Long lNewUser = new Long(usageDataDAO.getTotalNewUsersByDate("Android", startDate));
				Long returningUser = lActiveUser - lNewUser;
				pdAndroid = new ActiveUserData(sDate, "Android", lActiveUser, lNewUser, returningUser);
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Long lActiveUser = new Long(usageDataDAO.getTotalActiveUsersByDate("iOS", startDate));
				Long lNewUser = new Long(usageDataDAO.getTotalNewUsersByDate("iOS", startDate));
				Long returningUser = lActiveUser - lNewUser;
				pdiOS = new ActiveUserData(sDate, "Android", lActiveUser, lNewUser, returningUser);
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				logger.info("[INFO] Fetching for Date : " + startDate.toString());
				Long lObj = new Long(usageDataDAO.getTotalActiveUsersByDate("All", startDate));

				Long lActiveUser = new Long(usageDataDAO.getTotalActiveUsersByDate("All", startDate));
				Long lNewUser = new Long(usageDataDAO.getTotalNewUsersByDate("All", startDate));
				Long returningUser = lActiveUser - lNewUser;
				pdDev = new ActiveUserData(sDate, "All", lActiveUser, lNewUser, returningUser);
				dataList.add(pdDev);

				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}*/

	/*
	 * @CrossOrigin
	 * 
	 * @RequestMapping(value = "/getActiveUsersReportByDate", method =
	 * RequestMethod.POST) public String getActiveUsersReportByDate(@RequestBody
	 * String request) { logger.info("[INFO] Get Active User report by date");
	 * logger.info("[INFO] Get User Type [New /Existing] report"); String sDate =
	 * null; String eDate = null; String sPlatform = null; Gson gson = new Gson();
	 * try { sPlatform = getsPlatform(request).split("\"")[1]; eDate =
	 * getsEndDate(request).split("\"")[1]; sDate =
	 * getsStartDate(request).split("\"")[1]; } catch (IOException e2) {
	 * 
	 * e2.printStackTrace(); logger.error("[ERROR] Bad Request"); return
	 * gson.toJson("Bad Request"); } DateFormat formatter = new
	 * SimpleDateFormat("yyyy-MM-dd"); Date startDate; try { startDate =
	 * formatter.parse(sDate); } catch (ParseException e1) {
	 * logger.error("[ERROR] DATE not parsable"); e1.printStackTrace(); return
	 * gson.toJson("Invalid Date"); } Date endDate; try { endDate =
	 * formatter.parse(eDate); } catch (ParseException e1) { e1.printStackTrace();
	 * logger.error("[ERROR] DATE not parsable"); return
	 * gson.toJson("Invalid Date"); }
	 * 
	 * ArrayList<ActiveUserData> dataList = new ArrayList<ActiveUserData>();
	 * 
	 * ActiveUserData pdAndroid = null; ActiveUserData pdiOS = null; ActiveUserData
	 * pdDev = null;
	 * 
	 * if (sPlatform.equals("Android")) { while (true) { //ArrayList<String>
	 * localAr2 = new ArrayList<String>(); if (startDate.compareTo(endDate) > 0) {
	 * break; } //logger.info("[INFO] Fetching for Date : " + startDate.toString());
	 * pdAndroid = new
	 * ActiveUserData(sDate,"Android",usageDataDAO.getTotalActiveUsersByDate(
	 * "Android", startDate)+""); dataList.add(pdAndroid); sDate =
	 * getNextDate(sDate); startDate = getNextDateObject(startDate); } } else if
	 * (sPlatform.equals("iOS")) { while (true) { if (startDate.compareTo(endDate) >
	 * 0) { break; } pdiOS = new
	 * ActiveUserData(sDate,"Android",usageDataDAO.getTotalActiveUsersByDate(
	 * "Android", startDate)+""); dataList.add(pdiOS); sDate = getNextDate(sDate);
	 * startDate = getNextDateObject(startDate); } } else if
	 * (sPlatform.toUpperCase().equals("ALL")) { while (true) { if
	 * (startDate.compareTo(endDate) > 0) { break; } pdDev = new
	 * ActiveUserData(sDate,"Android",usageDataDAO.getTotalActiveUsersByDate(
	 * "Android", startDate)+""); dataList.add(pdDev); sDate = getNextDate(sDate);
	 * startDate = getNextDateObject(startDate); } } else {
	 * logger.error("[ERROR] Invalid Platform"); return
	 * gson.toJson("{\"message\":\"Invalid Platform\"}"); }
	 * logger.info("[INFO] DATA :" + gson.toJson(dataList)); return
	 * gson.toJson(dataList); }
	 */

	@CrossOrigin
	@RequestMapping(value = "/getOnboardingReportByDate", method = RequestMethod.POST)
	public String getOnboardingReportByDate(@RequestBody String request) {
		logger.info("[INFO] Get Onboarding report by date");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		ArrayList<OnboardingData> dataList = new ArrayList<OnboardingData>();

		OnboardingData pdAndroid = null;
		OnboardingData pdiOS = null;
		OnboardingData pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				// ArrayList<String> localAr2 = new ArrayList<String>();
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				// logger.info("[INFO] Fetching for Date : " + startDate.toString());
				pdAndroid = new OnboardingData(sDate, "Android",
						usageDataDAO.getOnboardingCountByDate("Android", startDate) + "",
						usageDataDAO.getSkippedCountByDate("Android", startDate) + "");
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new OnboardingData(sDate, "iOS", usageDataDAO.getOnboardingCountByDate("iOS", startDate) + "",
						usageDataDAO.getSkippedCountByDate("iOS", startDate) + "");
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new OnboardingData(sDate, "All", usageDataDAO.getOnboardingCountByDate("All", startDate) + "",
						usageDataDAO.getSkippedCountByDate("All", startDate) + "");
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getAvtarCreatedReportByDate", method = RequestMethod.POST)
	public String getFilteredAvtarDataByDate(@RequestBody String request) {
		logger.info("[INFO] Get avatar report by date ");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}
		if (sDate == null || eDate == null) {
			logger.error("[ERROR] GET Headers missing");
			return gson.toJson("{\"message\":\"Date-String-Start and Date-String-End Required in GET Header\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<AvatarReport> dataList = new ArrayList<AvatarReport>();
		AvatarReport pdAndroid = null;
		AvatarReport pdiOS = null;
		AvatarReport pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}

				long created = usageDataDAO.findAvatarCreatedByDate("Android", startDate);
				long reset = usageDataDAO.findAvatarResetByDate("Android", startDate);
				long init = usageDataDAO.findAvatarInitByDate("Android", startDate);

				pdAndroid = new AvatarReport(sDate, "Android", init, created, reset);

				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				long created = usageDataDAO.findAvatarCreatedByDate("iOS", startDate);
				long reset = usageDataDAO.findAvatarResetByDate("iOS", startDate);
				long init = usageDataDAO.findAvatarInitByDate("iOS", startDate);

				pdiOS = new AvatarReport(sDate, "iOS", init, created, reset);

				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				long created = usageDataDAO.findAvatarCreatedByDate("All", startDate);
				long reset = usageDataDAO.findAvatarResetByDate("All", startDate);
				long init = usageDataDAO.findAvatarInitByDate("All", startDate);

				pdDev = new AvatarReport(sDate, "All", init, created, reset);

				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getAvtarCreatedReportByDateUserLevel", method = RequestMethod.POST)
	public String getFilteredAvtarDataByDateUserLevel(@RequestBody String request) {
		logger.info("[INFO] Get avatar report by date ");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}
		if (sDate == null || eDate == null) {
			logger.error("[ERROR] GET Headers missing");
			return gson.toJson("{\"message\":\"Date-String-Start and Date-String-End Required in GET Header\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<AvatarReport> dataList = new ArrayList<AvatarReport>();
		AvatarReport pdAndroid = null;
		AvatarReport pdiOS = null;
		AvatarReport pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}

				// long created = usageDataDAO.findAvatarCreatedByDateUserLevel("Android",
				// startDate);
				long created = repo.findAvatarCreatedCountByDate(sDate, "Android");
				long reset = usageDataDAO.findAvatarResetByDateUserLevel("Android", startDate);
				long init = usageDataDAO.findAvatarInitByDateUserLevel("Android", startDate);

				pdAndroid = new AvatarReport(sDate, "Android", init, created, reset);

				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				// long created = usageDataDAO.findAvatarCreatedByDateUserLevel("iOS",
				// startDate);
				long created = repo.findAvatarCreatedCountByDate(sDate, "iOS");
				long reset = usageDataDAO.findAvatarResetByDateUserLevel("iOS", startDate);
				long init = usageDataDAO.findAvatarInitByDateUserLevel("iOS", startDate);

				pdiOS = new AvatarReport(sDate, "iOS", init, created, reset);

				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				// long created = usageDataDAO.findAvatarCreatedByDateUserLevel("All",
				// startDate);
				long created = repo.findAvatarCreatedCountByDate(sDate, "All");
				long reset = usageDataDAO.findAvatarResetByDateUserLevel("All", startDate);
				long init = usageDataDAO.findAvatarInitByDateUserLevel("All", startDate);

				pdDev = new AvatarReport(sDate, "All", init, created, reset);

				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}

	@Deprecated
	@CrossOrigin
	@RequestMapping(value = "/getSignupReportByDate", method = RequestMethod.POST)
	public String getFilteredUsageDataByDate(@RequestBody String request) {
		logger.info("[INFO] Get signup report By date");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<PlatformDataByDate> dataList = new ArrayList<PlatformDataByDate>();
		PlatformDataByDate pdAndroid = null;
		PlatformDataByDate pdiOS = null;
		PlatformDataByDate pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new PlatformDataByDate(sDate, sPlatform,
						usageDataDAO.getSignupCountByDate(startDate, "Android", "FB"),
						usageDataDAO.getSignupCountByDate(startDate, "Android", "MOBILE"));
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new PlatformDataByDate(sDate, sPlatform,
						usageDataDAO.getSignupCountByDate(startDate, "iOS", "FB"),
						usageDataDAO.getSignupCountByDate(startDate, "iOS", "MOBILE"));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new PlatformDataByDate(sDate, "All", usageDataDAO.getSignupCountByDate(startDate, "All", "FB"),
						usageDataDAO.getSignupCountByDate(startDate, "All", "MOBILE"));
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getKeyboardReportByDate", method = RequestMethod.POST)
	public String getFilteredKeyboardDataByDate(@RequestBody String request) {
		logger.info("[INFO] Get signup report for " + request);
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<PlatformKeyboard> dataList = new ArrayList<PlatformKeyboard>();
		PlatformKeyboard pdAndroid = null;
		PlatformKeyboard pdiOS = null;
		PlatformKeyboard pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new PlatformKeyboard(sDate, sPlatform,
						usageDataDAO.getKeyboardEnabledCountByDate(startDate, "Android")
								- usageDataDAO.getKeyboardDisabledCountByDate(startDate, "Android"));
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new PlatformKeyboard(sDate, sPlatform,
						usageDataDAO.getKeyboardEnabledCountByDate(startDate, "iOS")
								- usageDataDAO.getKeyboardDisabledCountByDate(startDate, "iOS"));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new PlatformKeyboard(sDate, sPlatform,
						usageDataDAO.getKeyboardEnabledCountByDate(startDate, "All")
								- usageDataDAO.getKeyboardDisabledCountByDate(startDate, "All"));
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}

	/*@CrossOrigin
	@RequestMapping(value = "/getCategoryReportByDate", method = RequestMethod.POST)
	public String getCategoryDataByDate(@RequestBody String request) {
		logger.info("[INFO] Get category report for " + request);

		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		ArrayList<CategoryData> dataList = new ArrayList<CategoryData>();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		CategoryData pdAndroid = null;
		CategoryData pdiOS = null;
		CategoryData pdDev = null;

		HashSet<String> catList = (HashSet<String>) usageDataDAO.getSubCategoryListBetweenDates("Android", startDate,
				endDate);

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				// Get All Categories for Android
				// HashSet<String> catList =(HashSet<String>)
				// usageDataDAO.getSubCategoryList("Android",startDate);

				// ////System.out.println("\nCAT COUNT : " + catList.size());
				HashMap<String, String> hMap = new HashMap<String, String>();
				for (String cat : catList) {
					long count = usageDataDAO.getSubCategoryCountByDate(startDate, cat, "Android");
					hMap.put(cat, count + "");
				}
				pdAndroid = new CategoryData(sDate, sPlatform, hMap);
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				// Get All Categories for iOS
				// HashSet<String> catList =(HashSet<String>)
				// usageDataDAO.getSubCategoryList("iOS",startDate);
				HashMap<String, String> hMap = new HashMap<String, String>();
				for (String cat : catList) {
					long count = usageDataDAO.getSubCategoryCountByDate(startDate, cat, "iOS");
					hMap.put(cat, +count + "");
				}

				pdiOS = new CategoryData(sDate, sPlatform, hMap);
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				// Get All Categories
				// HashSet<String> catList =(HashSet<String>)
				// usageDataDAO.getSubCategoryList("All",startDate);
				HashMap<String, String> hMap = new HashMap<String, String>();
				for (String cat : catList) {
					long count = usageDataDAO.getSubCategoryCountByDate(startDate, cat, "All");
					hMap.put(cat, count + "");
				}
				pdDev = new CategoryData(sDate, sPlatform, hMap);
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("Invalid Platform");
		}

		// Create SendFormat

		List<Map<String, String>> categoryList = new ArrayList<Map<String, String>>();

		for (CategoryData catData : dataList) {
			Map<String, String> catparser = new LinkedHashMap<String, String>();
			// CatagoryParser catparser = new CatagoryParser();
			catparser.put("Date", catData.getDate());
			catparser.put("Platform", catData.getPlatform());
			for (String key : catData.getCategoryMap().keySet()) {
				catparser.put(key, catData.getCategoryMap().get(key));
			}
			categoryList.add(catparser);
		}

		logger.info("[INFO] DATA :" + gson.toJson(categoryList));
		return gson.toJson(categoryList);
	}*/

	/*@CrossOrigin
	@RequestMapping(value = "/getUserTypeReportByDate", method = RequestMethod.POST)
	public String getFilteredUserTypeDataByDate(@RequestBody String request) {
		logger.info("[INFO] Get User Type [New /Existing] report");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		ArrayList<UserTypeData> dataList = new ArrayList<UserTypeData>();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		UserTypeData pdAndroid = null;
		UserTypeData pdiOS = null;
		UserTypeData pdDev = null;
		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new UserTypeData(sDate, sPlatform, usageDataDAO.getNewUserCountByDate(startDate, "Android"),
						usageDataDAO.getReturningUserCountByDate(startDate, "Android"));
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new UserTypeData(sDate, sPlatform, usageDataDAO.getNewUserCountByDate(startDate, "iOS"),
						usageDataDAO.getReturningUserCountByDate(startDate, "iOS"));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new UserTypeData(sDate, sPlatform, usageDataDAO.getNewUserCountByDate(startDate, "All"),
						usageDataDAO.getTotalUserCountByDate(startDate, "All"));
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}

		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}*/

/*	@CrossOrigin
	@RequestMapping(value = "/getAppFirstOpenReportByDate", method = RequestMethod.POST)
	public String getAppFirstOpenReportByDate(@RequestBody String request) {
		logger.info("[INFO] Get App First Open report");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		ArrayList<AppFirstOpenData> dataList = new ArrayList<AppFirstOpenData>();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		AppFirstOpenData pdAndroid = null;
		AppFirstOpenData pdiOS = null;
		AppFirstOpenData pdDev = null;
		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new AppFirstOpenData(sDate, sPlatform,
						usageDataDAO.getNewUserCountByDate(startDate, "Android"),
						repo.findAllUsersCountByDate(sDate, "Android"));
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new AppFirstOpenData(sDate, sPlatform, usageDataDAO.getNewUserCountByDate(startDate, "iOS"),
						repo.findAllUsersCountByDate(sDate, "iOS"));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new AppFirstOpenData(sDate, "All", usageDataDAO.getNewUserCountByDate(startDate, "All"),
						repo.findAllUsersCountByDate(sDate, "All"));
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}

		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}*/

	/*@CrossOrigin
	@RequestMapping(value = "/getAvatarGenderReportByDate", method = RequestMethod.POST)
	public String getAvatarGenderBasedReportByDate(@RequestBody String request) {
		logger.info("[INFO] Get App First Open report");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		ArrayList<GenderReport> dataList = new ArrayList<GenderReport>();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		GenderReport pdAndroid = null;
		GenderReport pdiOS = null;
		GenderReport pdDev = null;
		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}

				ArrayList<String> AppIdListMale = (ArrayList<String>) repo.findAvatarCreatedAppIDListByDate(sDate,
						"male");
				ArrayList<String> AppIdListFeMale = (ArrayList<String>) repo.findAvatarCreatedAppIDListByDate(sDate,
						"female");
				int maleCountAndroid = 0;
				int femaleCount = 0;
				for (String s : AppIdListMale) {
					if (repo.isAppIDAndroid(s)) {
						maleCountAndroid++;
					}
				}
				for (String s2 : AppIdListFeMale) {
					if (repo.isAppIDAndroid(s2)) {
						femaleCount++;
					}
				}

				pdAndroid = new GenderReport(sDate, "Android", maleCountAndroid, femaleCount);
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				ArrayList<String> AppIdListMale = (ArrayList<String>) repo.findAvatarCreatedAppIDListByDate(sDate,
						"male");
				ArrayList<String> AppIdListFeMale = (ArrayList<String>) repo.findAvatarCreatedAppIDListByDate(sDate,
						"female");
				int maleCountAndroid = 0;
				int femaleCount = 0;
				for (String s : AppIdListMale) {
					if (!repo.isAppIDAndroid(s)) {
						maleCountAndroid++;
					}
				}
				for (String s2 : AppIdListFeMale) {
					if (!repo.isAppIDAndroid(s2)) {
						femaleCount++;
					}
				}

				pdiOS = new GenderReport(sDate, "iOS", maleCountAndroid, femaleCount);
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				ArrayList<String> AppIdListMale = (ArrayList<String>) repo.findAvatarCreatedAppIDListByDate(sDate,
						"male");
				ArrayList<String> AppIdListFeMale = (ArrayList<String>) repo.findAvatarCreatedAppIDListByDate(sDate,
						"female");
				int maleCountAndroid = 0;
				int femaleCount = 0;
				for (String s : AppIdListMale) {
					// if(!repo.isAppIDAndroid(s)) {
					maleCountAndroid++;
					// }
				}
				for (String s2 : AppIdListFeMale) {
					// if(!repo.isAppIDAndroid(s2)) {
					femaleCount++;
					// }
				}

				pdDev = new GenderReport(sDate, "All", maleCountAndroid, femaleCount);
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}

		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}*/

	/*@CrossOrigin
	@RequestMapping(value = "/getStickerShareReportByImageID", method = RequestMethod.POST)
	public String getStickerShareViewedReportByDate(@RequestBody String request) {
		logger.info("[INFO] Get App First Open report");
		String sDate = null;
		String eDate = null;
		Gson gson = new Gson();
		try {
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		ArrayList<ImageData> dataList = new ArrayList<ImageData>();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		Date endDate;

		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}
		Map<String, Integer> viewedMap = usageDataDAO.getViewedCountByImageId(startDate, endDate);
		Map<String, Integer> sharedMap = usageDataDAO.getSharedCountByImageID(startDate, endDate);

		Set<String> imageIDSet = new HashSet<String>();
		imageIDSet.addAll(sharedMap.keySet());
		imageIDSet.addAll(viewedMap.keySet());

		for (String imageId : imageIDSet) {
			Integer shareCount = sharedMap.get(imageId);
			Integer viewCount = viewedMap.get(imageId);
			dataList.add(
					new ImageData(imageId, shareCount == null ? 0 : shareCount, viewCount == null ? 0 : viewCount));
		}

		// //System.out.println(new Gson().toJson(dataList));
		return new Gson().toJson(dataList);
	}*/

/*	@CrossOrigin
	@RequestMapping(value = "/getExtendedStickerWiseShareReportByDate", method = RequestMethod.POST)
	public String getStickerWiseShareReportByDate(@RequestBody String request) {
		logger.info("[INFO] Get App First Open report");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		ArrayList<StickerShareData> dataList = new ArrayList<StickerShareData>();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		Date endDate;

		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		ArrayList<String> targetAppList = (ArrayList<String>) usageDataDAO.getAllTargetAppBydate(startDate, endDate);

		StickerShareData pdAndroid = null;
		StickerShareData pdiOS = null;
		StickerShareData pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}

				List<String> list = usageDataDAO.getAllImageIDsBydate(startDate);

				if (list == null) {
					logger.error("[ERROR] No Content Found");
					continue;
				}
				for (String stickerId : list) {
					logger.error("[INFO] Looping for " + stickerId);
					Map<String, String> targetCountMap = new LinkedHashMap<String, String>();

					pdAndroid = new StickerShareData();
					pdAndroid.setDate(sDate);
					pdAndroid.setStickerID(stickerId);
					pdAndroid.setPlatform("Android");
					pdAndroid = usageDataDAO.getImageIDCountViewed(stickerId, startDate, "Android", pdAndroid);
					pdAndroid = usageDataDAO.getImageIDCountShared(stickerId, startDate, "Android", pdAndroid);

					for (String s : targetAppList) {
						long count = usageDataDAO.getExtendedTargetAppShareCount(s, stickerId, startDate, "Android");
						targetCountMap.put(s, count + "");
					}

					pdAndroid.setTargetMap(targetCountMap);

					if (pdAndroid.getShareCount() + pdAndroid.getViewCount() != 0) {
						dataList.add(pdAndroid);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				List<String> list = usageDataDAO.getAllImageIDsBydate(startDate);

				if (list == null) {
					logger.error("[ERROR] No Content Found");
					continue;
				}

				for (String stickerId : list) {
					logger.error("[INFO] Looping for " + stickerId);
					Map<String, String> targetCountMap = new LinkedHashMap<String, String>();

					pdiOS = new StickerShareData();
					pdiOS.setDate(sDate);
					pdiOS.setStickerID(stickerId);
					pdiOS.setPlatform("iOS");
					pdiOS = usageDataDAO.getImageIDCountViewed(stickerId, startDate, "iOS", pdiOS);
					pdiOS = usageDataDAO.getImageIDCountShared(stickerId, startDate, "iOS", pdiOS);

					for (String s : targetAppList) {
						long count = usageDataDAO.getExtendedTargetAppShareCount(s, stickerId, startDate, "iOS");
						targetCountMap.put(s, count + "");
					}

					pdiOS.setTargetMap(targetCountMap);

					if (pdiOS.getShareCount() + pdiOS.getViewCount() != 0) {
						dataList.add(pdiOS);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				List<String> list = usageDataDAO.getAllImageIDsBydate(startDate);

				if (list == null) {
					logger.error("[ERROR] No Content Found");
					continue;
				}

				for (String stickerId : list) {
					logger.error("[INFO] Looping for " + stickerId);
					Map<String, String> targetCountMap = new LinkedHashMap<String, String>();

					pdDev = new StickerShareData();
					pdDev.setDate(sDate);
					pdDev.setStickerID(stickerId);
					pdDev.setPlatform("All");
					pdDev = usageDataDAO.getImageIDCountViewed(stickerId, startDate, "All", pdDev);
					pdDev = usageDataDAO.getImageIDCountShared(stickerId, startDate, "All", pdDev);

					for (String s : targetAppList) {
						long count = usageDataDAO.getExtendedTargetAppShareCount(s, stickerId, startDate, "All");
						targetCountMap.put(s, count + "");
					}

					pdDev.setTargetMap(targetCountMap);

					if (pdDev.getShareCount() + pdDev.getViewCount() != 0) {
						dataList.add(pdDev);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}*/

	@CrossOrigin
	@RequestMapping(value = "/getStickerWiseShareReportByDate", method = RequestMethod.POST)
	public String getStickerWiseShareReportByDateReduced(@RequestBody String request) {
		logger.info("[INFO] Get App First Open report");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		ArrayList<StickerShareData> dataList = new ArrayList<StickerShareData>();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		Date endDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		// ArrayList<String> targetAppList = (ArrayList<String>)
		// usageDataDAO.getAllTargetAppBydate(startDate, endDate);

		StickerShareData pdAndroid = null;
		StickerShareData pdiOS = null;
		StickerShareData pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}

				List<String> list = usageDataDAO.getAllImageIDsBydate(startDate);

				if (list == null) {
					logger.error("[ERROR] No Content Found");
					continue;
				}

				for (String stickerId : list) {
					logger.error("[INFO] Looping for " + stickerId);
					Map<String, String> targetCountMap = new LinkedHashMap<String, String>();

					pdAndroid = new StickerShareData();
					pdAndroid.setDate(sDate);
					pdAndroid.setStickerID(stickerId);
					pdAndroid.setPlatform("Android");
					pdAndroid = usageDataDAO.getImageIDCountViewed(stickerId, startDate, "Android", pdAndroid);
					pdAndroid = usageDataDAO.getImageIDCountShared(stickerId, startDate, "Android", pdAndroid);

					/*
					 * for (String s : targetAppList) { long count =
					 * usageDataDAO.getTargetAppShareCount(s, stickerId, startDate, "Android");
					 * targetCountMap.put(s, count + ""); }
					 */

					// pdAndroid.setTargetMap(targetCountMap);

					if (pdAndroid.getShareCount() + pdAndroid.getViewCount() != 0) {
						dataList.add(pdAndroid);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				List<String> list = usageDataDAO.getAllImageIDsBydate(startDate);

				if (list == null) {
					logger.error("[ERROR] No Content Found");
					continue;
				}

				for (String stickerId : list) {
					logger.error("[INFO] Looping for " + stickerId);
					Map<String, String> targetCountMap = new LinkedHashMap<String, String>();

					pdiOS = new StickerShareData();
					pdiOS.setDate(sDate);
					pdiOS.setStickerID(stickerId);
					pdiOS.setPlatform("iOS");
					pdiOS = usageDataDAO.getImageIDCountViewed(stickerId, startDate, "iOS", pdiOS);
					pdiOS = usageDataDAO.getImageIDCountShared(stickerId, startDate, "iOS", pdiOS);

					/*
					 * for (String s : targetAppList) { long count =
					 * usageDataDAO.getTargetAppShareCount(s, stickerId, startDate, "iOS");
					 * targetCountMap.put(s, count + ""); }
					 */

					// pdiOS.setTargetMap(targetCountMap);

					if (pdiOS.getShareCount() + pdiOS.getViewCount() != 0) {
						dataList.add(pdiOS);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				List<String> list = usageDataDAO.getAllImageIDsBydate(startDate);

				if (list == null) {
					logger.error("[ERROR] No Content Found");
					continue;
				}

				for (String stickerId : list) {
					logger.error("[INFO] Looping for " + stickerId);
					Map<String, String> targetCountMap = new LinkedHashMap<String, String>();

					pdDev = new StickerShareData();
					pdDev.setDate(sDate);
					pdDev.setStickerID(stickerId);
					pdDev.setPlatform("All");
					pdDev = usageDataDAO.getImageIDCountViewed(stickerId, startDate, "All", pdDev);
					pdDev = usageDataDAO.getImageIDCountShared(stickerId, startDate, "All", pdDev);

					/*
					 * for (String s : targetAppList) { long count =
					 * usageDataDAO.getTargetAppShareCount(s, stickerId, startDate, "All");
					 * targetCountMap.put(s, count + ""); }
					 */

					// pdDev.setTargetMap(targetCountMap);

					if (pdDev.getShareCount() + pdDev.getViewCount() != 0) {
						dataList.add(pdDev);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}

	/*
	 * @CrossOrigin
	 * 
	 * @RequestMapping(value = "/getkeywordSearchReport", method =
	 * RequestMethod.POST) public String getkeywordSearchReport(@RequestBody String
	 * request) { logger.info("[INFO] Get keyword Search report");
	 * ArrayList<KeywordSearch> dataList = new ArrayList<KeywordSearch>(); long
	 * Count = 0; Gson gson = new Gson(); String sPlatform; String sDate = null;
	 * String eDate = null; try { sPlatform = getsPlatform(request).split("\"")[1];
	 * eDate = getsEndDate(request).split("\"")[1]; sDate =
	 * getsStartDate(request).split("\"")[1]; } catch (IOException e2) {
	 * 
	 * e2.printStackTrace(); logger.error("[ERROR] Bad Request"); return
	 * gson.toJson("Bad Request"); }
	 * 
	 * DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); Date startDate;
	 * Date endDate;
	 * 
	 * KeywordSearch pdAndroid = null; KeywordSearch pdiOS = null; KeywordSearch
	 * pdDev = null; List<String> keywordsList = usageDataDAO.getAllKeywords();
	 * 
	 * if (sPlatform.equals("Android")) { for (String key : keywordsList) { Count =
	 * usageDataDAO.getKeywordSearchCount(key, "Android"); pdAndroid = new
	 * KeywordSearch(key, Count); if(Count > 0) { dataList.add(pdAndroid); } } }
	 * else if (sPlatform.equals("iOS")) { for (String key : keywordsList) { Count =
	 * usageDataDAO.getKeywordSearchCount(key, "iOS"); pdiOS = new
	 * KeywordSearch(key,Count); if(Count > 0) { dataList.add(pdiOS); } } } else if
	 * (sPlatform.toUpperCase().equals("ALL")) { for (String key : keywordsList) {
	 * Count = usageDataDAO.getKeywordSearchCount(key, "All"); pdDev = new
	 * KeywordSearch(key, Count); if(Count > 0) { dataList.add(pdDev); } } } else {
	 * logger.error("[ERROR] Invalid Platform"); return
	 * gson.toJson("{\"message\":\"Invalid Platform\"}"); }
	 * 
	 * logger.info("[INFO] DATA :" + gson.toJson(dataList)); return
	 * gson.toJson(dataList); }
	 */

	/*@CrossOrigin
	@RequestMapping(value = "/getkeywordSearchReportByDate", method = RequestMethod.POST)
	public String getkeywordSearchReport(@RequestBody String request) {
		logger.info("[INFO] Get keyword Search report");
		ArrayList<KeywordSearch> dataList = new ArrayList<KeywordSearch>();
		// long Count = 0;
		Gson gson = new Gson();
		String sPlatform;
		String sDate = null;
		String eDate = null;

		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String backupStartDate = sDate;
		String backupEndDate = eDate;
		Date startDate;
		Date endDate;

		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		
		 * while (true) { }
		 * 
		 * List<String> keywordsList = usageDataDAO.getAllKeywordsByDate(startDate,
		 * endDate);
		 * 
		 * for (String keyword : keywordsList) {
		 

		// ////System.out.println("SID : " + stickerId);
		KeywordSearch pdAndroid = null;
		KeywordSearch pdiOS = null;
		KeywordSearch pdDev = null;
		sDate = backupStartDate;
		eDate = backupEndDate;
		try {
			startDate = formatter.parse(backupStartDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		try {
			endDate = formatter.parse(backupEndDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}
		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Map<String, Integer> keywordsList = usageDataDAO.getAllKeywordsByDate(startDate, "Android");
				for (String key : keywordsList.keySet()) {
					pdAndroid = new KeywordSearch(sDate, key, keywordsList.get(key));
					if (pdAndroid.getCount() > 0) {
						dataList.add(pdAndroid);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			
			 * while (true) { if (startDate.compareTo(endDate) > 0) { break; } List<String>
			 * keywordsList = usageDataDAO.getAllKeywordsByDate(startDate, startDate); if
			 * (keywordsList == null) { continue; } Date tempdate = startDate; for (String
			 * keyword : keywordsList) { if (keyword == null || keyword.equals("")) {
			 * continue; } if (startDate.compareTo(endDate) > 0) { startDate = tempdate;
			 * break; } pdAndroid = new KeywordSearch(sDate, keyword,
			 * usageDataDAO.getKeywordSearchCountByDate(keyword, "iOS", startDate)); if
			 * (pdAndroid.getCount() > 0) { dataList.add(pdAndroid); } sDate =
			 * getNextDate(sDate); startDate = getNextDateObject(startDate); } startDate =
			 * getNextDateObject(startDate); }
			 
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Map<String, Integer> keywordsList = usageDataDAO.getAllKeywordsByDate(startDate, "iOS");
				for (String key : keywordsList.keySet()) {
					pdiOS = new KeywordSearch(sDate, key, keywordsList.get(key));
					if (pdiOS.getCount() > 0) {
						dataList.add(pdiOS);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			
			 * while (true) {
			 * 
			 * if (startDate.compareTo(endDate) > 0) { break; } List<String> keywordsList =
			 * usageDataDAO.getAllKeywordsByDate(startDate, startDate); if (keywordsList ==
			 * null) { continue; } Date tempdate = startDate; for (String keyword :
			 * keywordsList) { if (keyword == null || keyword.equals("")) { continue; } if
			 * (startDate.compareTo(endDate) > 0) { startDate = tempdate; break; } pdAndroid
			 * = new KeywordSearch(sDate, keyword,
			 * usageDataDAO.getKeywordSearchCountByDate(keyword, "All", startDate)); if
			 * (pdAndroid.getCount() > 0) { dataList.add(pdAndroid); } sDate =
			 * getNextDate(sDate); startDate = getNextDateObject(startDate); } startDate =
			 * getNextDateObject(startDate); }
			 
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Map<String, Integer> keywordsList = usageDataDAO.getAllKeywordsByDate(startDate, "All");
				for (String key : keywordsList.keySet()) {
					pdDev = new KeywordSearch(sDate, key, keywordsList.get(key));
					if (pdDev.getCount() > 0) {
						dataList.add(pdDev);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		// }

		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}*/

	/*@CrossOrigin
	@RequestMapping(value = "/getSignupMobileReportByDate", method = RequestMethod.POST)
	public String getSignupMobileDataByDate(@RequestBody String request) {
		logger.info("[INFO] Get signup report for " + request);
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<SignUpMobileData> dataList = new ArrayList<SignUpMobileData>();
		SignUpMobileData pdAndroid = null;
		SignUpMobileData pdiOS = null;
		SignUpMobileData pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new SignUpMobileData(sDate, sPlatform,
						usageDataDAO.getMobileUserInitReportByDate("Android", startDate),
						repo.findMobileUsersByDate("Android", sDate),
						usageDataDAO.getMobileUserResetOTPReportByDate("Android", startDate));
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new SignUpMobileData(sDate, sPlatform,
						usageDataDAO.getMobileUserInitReportByDate("iOS", startDate),
						repo.findMobileUsersByDate("iOS", sDate),
						usageDataDAO.getMobileUserResetOTPReportByDate("iOS", startDate));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new SignUpMobileData(sDate, sPlatform,
						usageDataDAO.getMobileUserInitReportByDate("All", startDate),
						repo.findMobileUsersByDate("All", sDate),
						usageDataDAO.getMobileUserResetOTPReportByDate("All", startDate));
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getSignupFBReportByDate", method = RequestMethod.POST)
	public String getSignupFBDataByDate(@RequestBody String request) {
		logger.info("[INFO] Get signup report for " + request);
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<SignUpFBData> dataList = new ArrayList<SignUpFBData>();
		SignUpFBData pdAndroid = null;
		SignUpFBData pdiOS = null;
		SignUpFBData pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new SignUpFBData(sDate, sPlatform,
						usageDataDAO.getFBUserInitReportByDate("Android", startDate),
						repo.findFBUsersByDate("Android", sDate));
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new SignUpFBData(sDate, sPlatform, usageDataDAO.getFBUserInitReportByDate("iOS", startDate),
						repo.findFBUsersByDate("iOS", sDate));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new SignUpFBData(sDate, sPlatform, usageDataDAO.getFBUserInitReportByDate("All", startDate),
						repo.findFBUsersByDate("All", sDate));
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}*/

	// ################################### ANALYTICS ##############################

	// ############################ GRAPH REPORTS #################################

	@CrossOrigin
	@RequestMapping(value = "/getTotalUniqueUsersGraphByDate", method = RequestMethod.POST)
	public String getTotalUsers(@RequestBody String request) {
		logger.info("[INFO] Get keyword Search report");
		long Count = 0;
		Gson gson = new Gson();
		String sPlatform;
		String sDate = null;
		String eDate = null;

		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		Date endDate;

		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Count += usageDataDAO.getNewUserCountByDate(startDate, "Android")
						+ usageDataDAO.getTotalUserCountByDate(startDate, "Android");
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}

				Count += usageDataDAO.getNewUserCountByDate(startDate, "iOS")
						+ usageDataDAO.getTotalUserCountByDate(startDate, "iOS");
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Count += usageDataDAO.getNewUserCountByDate(startDate, "All")
						+ usageDataDAO.getTotalUserCountByDate(startDate, "All");
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}

		return gson.toJson(Count);
	}

	@CrossOrigin
	@RequestMapping(value = "/getTotalKeyboardEnableGraphByDate", method = RequestMethod.POST)
	public String getTotalKeyboardEnable(@RequestBody String request) {
		logger.info("[INFO] Get keyword Search report");
		long Count = 0;
		Gson gson = new Gson();
		String sPlatform;
		String sDate = null;
		String eDate = null;

		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		Date endDate;

		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Count += (usageDataDAO.getTotalKeyboardEnableGraphByDate("Android", startDate));
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Count += (usageDataDAO.getTotalKeyboardEnableGraphByDate("iOS", startDate));
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Count += (usageDataDAO.getTotalKeyboardEnableGraphByDate("All", startDate));
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}

		return gson.toJson(Count);
	}

	@CrossOrigin
	@RequestMapping(value = "/getTotalAvatarCreatedGraphByDate", method = RequestMethod.POST)
	public String getTotalAvatarCreated(@RequestBody String request) {
		logger.info("[INFO] Get keyword Search report");
		long Count = 0;
		Gson gson = new Gson();
		String sPlatform;
		String sDate = null;
		String eDate = null;

		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		Date endDate;

		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Count += (usageDataDAO.findAvatarCreatedByDate("Android", startDate));
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Count += (usageDataDAO.findAvatarCreatedByDate("iOS", startDate));
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Count += (usageDataDAO.findAvatarCreatedByDate("All", startDate));
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}

		return gson.toJson(Count);
	}

	@CrossOrigin
	@RequestMapping(value = "/getTotalSharesGraphByDate", method = RequestMethod.POST)
	public String getTotalSharesData(@RequestBody String request) {
		logger.info("[INFO] Get keyword Search report");
		long Count = 0;
		Gson gson = new Gson();
		String sPlatform;
		String sDate = null;
		String eDate = null;

		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		Date endDate;

		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Count += (usageDataDAO.getTotalSharesByDate("Android", startDate));
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Count += (usageDataDAO.getTotalSharesByDate("iOS", startDate));
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Count += (usageDataDAO.getTotalSharesByDate("All", startDate));
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}

		return gson.toJson(Count);
	}

	@CrossOrigin
	@RequestMapping(value = "/getActiveUsersGraphByDate", method = RequestMethod.POST)
	public String getActiveUsers(@RequestBody String request) {
		logger.info("[INFO] Get Active User report by date");
		logger.info("[INFO] Get User Type [New /Existing] report");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		ArrayList<ArrayList<Object>> dataList = new ArrayList<ArrayList<Object>>();
		ArrayList<Object> pdAndroid = null;
		ArrayList<Object> pdiOS = null;
		ArrayList<Object> pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				// ArrayList<String> localAr2 = new ArrayList<String>();
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				logger.info("[INFO] Fetching for Date : " + startDate.toString());

				Long lObj = new Long(usageDataDAO.getTotalActiveUsersByDate("Android", startDate));

				pdAndroid = new ArrayList<Object>();
				pdAndroid.add(sDate);
				pdAndroid.add(lObj);
				dataList.add(pdAndroid);

				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				logger.info("[INFO] Fetching for Date : " + startDate.toString());

				/*
				 * pdiOS = new ActiveUserHelper(sDate,"iOS",
				 * usageDataDAO.getTotalActiveUsersByDate("iOS", startDate));
				 * dataList.add(pdiOS);
				 */

				Long lObj = new Long(usageDataDAO.getTotalActiveUsersByDate("iOS", startDate));

				pdiOS = new ArrayList<Object>();
				pdiOS.add(sDate);
				pdiOS.add(lObj);
				dataList.add(pdiOS);

				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				logger.info("[INFO] Fetching for Date : " + startDate.toString());

				/*
				 * pdDev = new ActiveUserHelper(sDate,"All",
				 * usageDataDAO.getTotalActiveUsersByDate("All", startDate));
				 * dataList.add(pdDev);
				 */

				Long lObj = new Long(usageDataDAO.getTotalActiveUsersByDate("All", startDate));

				pdDev = new ArrayList<Object>();
				pdDev.add(sDate);
				pdDev.add(lObj);
				dataList.add(pdDev);

				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getPlatformUserGraphByDate", method = RequestMethod.POST)
	public String getPlatformUsers(@RequestBody String request) {
		logger.info("[INFO] Get Active User report by date");
		logger.info("[INFO] Get User Type [New /Existing] report");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		List<PlatfromUserTotalHelper> platList = new ArrayList<PlatfromUserTotalHelper>();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}
		long andCount = 0;
		long iosCount = 0;
		// long totalCount = 0;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				// logger.info("[INFO] Fetching for Date : " + startDate.toString());
				// andCount+=usageDataDAO.getTotalActiveUsersByDate("Android", startDate);
				andCount += usageDataDAO.getNewUserCountByDate(startDate, "Android")
						+ usageDataDAO.getTotalUserCountByDate(startDate, "Android");

				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
			platList.add(new PlatfromUserTotalHelper("Android", andCount));
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				// logger.info("[INFO] Fetching for Date : " + startDate.toString());
				// iosCount+=usageDataDAO.getTotalActiveUsersByDate("iOS", startDate);
				iosCount += usageDataDAO.getNewUserCountByDate(startDate, "iOS")
						+ usageDataDAO.getTotalUserCountByDate(startDate, "iOS");
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				andCount += usageDataDAO.getNewUserCountByDate(startDate, "Android")
						+ usageDataDAO.getTotalUserCountByDate(startDate, "Android");
				iosCount += usageDataDAO.getNewUserCountByDate(startDate, "iOS")
						+ usageDataDAO.getTotalUserCountByDate(startDate, "iOS");
				// totalCount+=usageDataDAO.getTotalActiveUsersByDate("All", startDate);

				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
			platList.add(new PlatfromUserTotalHelper("Android", andCount));
			platList.add(new PlatfromUserTotalHelper("iOS", iosCount));
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}

		logger.info("[INFO] DATA :" + gson.toJson(platList));
		return gson.toJson(platList);

	}

	@CrossOrigin
	@RequestMapping(value = "/getUserTypeGraphByDate", method = RequestMethod.POST)
	public String getUserType(@RequestBody String request) {
		logger.info("[INFO] Get User Type [New /Existing] report");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		List<UserTypeData> userDataList = new ArrayList<UserTypeData>();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}
		UserTypeData totalCount;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				long newUserCount = repo.findNewUsersCountByDate(sDate, sPlatform);
				long returningUserCount = usageDataDAO.getTotalUserCountByDate(startDate, sPlatform) - newUserCount;
				totalCount = new UserTypeData(sDate, sPlatform, newUserCount, returningUserCount);
				userDataList.add(totalCount);	
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				long newUserCount = repo.findNewUsersCountByDate(sDate, sPlatform);
				long returningUserCount = usageDataDAO.getTotalUserCountByDate(startDate, sPlatform) - newUserCount;
				totalCount = new UserTypeData(sDate, sPlatform, newUserCount, returningUserCount);
				userDataList.add(totalCount);	
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				long newUserCount = repo.findNewUsersCountByDate(sDate, sPlatform);
				long returningUserCount = usageDataDAO.getTotalUserCountByDate(startDate, sPlatform) - newUserCount;
				totalCount = new UserTypeData(sDate, sPlatform, newUserCount, returningUserCount);
				userDataList.add(totalCount);				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(userDataList));
		return gson.toJson(userDataList);

	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/getTargetGraphByDate", method = RequestMethod.POST)
	public String getTargetApplikcationDataByDate(@RequestBody String request) {
		logger.info("[Enter] getTargetGraphByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		
		HashMap<String,Integer> targetMap = new HashMap<String,Integer>();		
				
		//ArrayList<ShareTargetData> dataList = new ArrayList<ShareTargetData>();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		Date endDate;
		List<String> list = null;
		try {
			list = usageDataDAO.getAllTargetAppBydate(formatter.parse(sDate), formatter.parse(eDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		String backupStartDate = sDate;
		String backupEndDate = eDate;

		if (list == null) {
			logger.error("[ERROR] No Content Found");
			return null;
		}

		for (String targetApp : list) {
			if (targetApp == null || targetApp.equals("")) {
				continue;
			}
			sDate = backupStartDate;
			eDate = backupEndDate;
			try {
				startDate = formatter.parse(backupStartDate);
			} catch (ParseException e1) {
				logger.error("[ERROR] DATE not parsable");
				e1.printStackTrace();
				return gson.toJson("Invalid Date");
			}
			try {
				endDate = formatter.parse(backupEndDate);
			} catch (ParseException e1) {
				e1.printStackTrace();
				logger.error("[ERROR] DATE not parsable");
				return gson.toJson("Invalid Date");
			}
			if (sPlatform.equals("Android")) {
				while (true) {
					if (startDate.compareTo(endDate) > 0) {
						break;
					}
					long Count = usageDataDAO.getTargetAppShareCount(targetApp, startDate, "Android");					
					if(targetMap.containsKey(targetApp)) {
						int newCount = (int) (targetMap.get(targetApp) + Count);
						targetMap.put(targetApp, newCount);						
					}else {
						targetMap.put(targetApp, (int) (long)Count);
					}	
					sDate = getNextDate(sDate);
					startDate = getNextDateObject(startDate);
				}
			} else if (sPlatform.equals("iOS")) {
				while (true) {
					if (startDate.compareTo(endDate) > 0) {
						break;
					}
					long Count = usageDataDAO.getTargetAppShareCount(targetApp, startDate, "Android");					
					if(targetMap.containsKey(targetApp)) {
						int newCount = (int) (targetMap.get(targetApp) + Count);
						targetMap.put(targetApp, newCount);						
					}else {
						targetMap.put(targetApp, (int) (long)Count);
					}	
					sDate = getNextDate(sDate);
					startDate = getNextDateObject(startDate);
					sDate = getNextDate(sDate);
					startDate = getNextDateObject(startDate);
				}
			} else if (sPlatform.toUpperCase().equals("ALL")) {
				while (true) {
					if (startDate.compareTo(endDate) > 0) {
						break;
					}
					long Count = usageDataDAO.getTargetAppShareCount(targetApp, startDate, "Android");	
					
					if(targetMap.containsKey(targetApp)) {
						int newCount = (int) (targetMap.get(targetApp) + Count);
						targetMap.put(targetApp, newCount);						
					}else {
						targetMap.put(targetApp, (int) (long)Count);
					}			
					
					sDate = getNextDate(sDate);
					startDate = getNextDateObject(startDate);
					sDate = getNextDate(sDate);
					startDate = getNextDateObject(startDate);
				}
			} else {
				logger.error("[ERROR] Invalid Platform");
				return gson.toJson("{\"message\":\"Invalid Platform\"}");
			}
		}
		logger.info("[Leave] getTargetGraphByDate");
		return gson.toJson(targetMap);
	}

	@CrossOrigin
	@RequestMapping(value = "/getUserRegistrationGraphByDate", method = RequestMethod.POST)
	public String getUserRegistrationDataByDate(@RequestBody String request) {
		logger.info("[INFO] Get User Registration report");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		List<RegisteredUserHelper> reguserList = new ArrayList<RegisteredUserHelper>();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}
		RegisteredUserHelper totalCount;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				long totalUsers = repo.findAllUsersCountByDate(sDate, sPlatform);
				long signedUpCount = repo.findFBUsersByDate(sPlatform, sDate) + repo.findMobileUsersByDate(sPlatform, sDate);
				totalCount = new RegisteredUserHelper(sDate, sPlatform, signedUpCount, (totalUsers - signedUpCount));
				reguserList.add(totalCount);				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				long totalUsers = repo.findAllUsersCountByDate(sDate, sPlatform);
				long signedUpCount = repo.findFBUsersByDate(sPlatform, sDate) + repo.findMobileUsersByDate(sPlatform, sDate);
				totalCount = new RegisteredUserHelper(sDate, sPlatform, signedUpCount, (totalUsers - signedUpCount));
				reguserList.add(totalCount);				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				long totalUsers = repo.findAllUsersCountByDate(sDate, sPlatform);
				long signedUpCount = repo.findFBUsersByDate(sPlatform, sDate) + repo.findMobileUsersByDate(sPlatform, sDate);
				totalCount = new RegisteredUserHelper(sDate, sPlatform, signedUpCount, (totalUsers - signedUpCount));
				reguserList.add(totalCount);				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(reguserList));
		return gson.toJson(reguserList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getCategoryGraphByDate", method = RequestMethod.POST)
	public String getCategoryGraphByDate(@RequestBody String request) {
		logger.info("[INFO] Get category graph for " + request);

		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		ArrayList<CategoryData> dataList = new ArrayList<CategoryData>();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		CategoryData pdAndroid = null;
		CategoryData pdiOS = null;
		CategoryData pdDev = null;

		HashSet<String> catList = (HashSet<String>) usageDataDAO.getSubCategoryListBetweenDates("Android", startDate,
				endDate);

		if (sPlatform.equals("Android")) {
			// Get All Categories for Android

			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				// ////System.out.println("\nCAT COUNT : " + catList.size());
				HashMap<String, String> hMap = new HashMap<String, String>();
				for (String cat : catList) {
					long count = usageDataDAO.getSubCategoryCountByDate(startDate, cat, "Android");
					if (hMap.containsKey(cat)) {
						hMap.put(cat, (count + Integer.parseInt(hMap.get(cat))) + "");
					} else {
						hMap.put(cat, count + "");
					}
				}
				pdAndroid = new CategoryData(sDate, sPlatform, hMap);
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			// Get All Categories for iOS
			// HashSet<String> catList =(HashSet<String>)
			// usageDataDAO.getSubCategoryList("iOS",startDate);
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				HashMap<String, String> hMap = new HashMap<String, String>();
				for (String cat : catList) {
					long count = usageDataDAO.getSubCategoryCountByDate(startDate, cat, "iOS");
					if (hMap.containsKey(cat)) {
						hMap.put(cat, (count + Integer.parseInt(hMap.get(cat))) + "");
					} else {
						hMap.put(cat, +count + "");
					}
				}

				pdiOS = new CategoryData(sDate, sPlatform, hMap);
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			// Get All Categories
			// HashSet<String> catList =(HashSet<String>)
			// usageDataDAO.getSubCategoryList("All",startDate);
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				HashMap<String, String> hMap = new HashMap<String, String>();
				for (String cat : catList) {
					long count = usageDataDAO.getSubCategoryCountByDate(startDate, cat, "All");
					if (hMap.containsKey(cat)) {
						hMap.put(cat, (count + Integer.parseInt(hMap.get(cat))) + "");
					} else {
						hMap.put(cat, +count + "");
					}
				}
				pdDev = new CategoryData(sDate, sPlatform, hMap);
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("Invalid Platform");
		}

		// Create SendFormat

		Map<String, Integer> categoryList = new HashMap<String, Integer>();

		for (CategoryData catData : dataList) {
			Map<String, String> catparser = new LinkedHashMap<String, String>();
			// CatagoryParser catparser = new CatagoryParser();
			// catparser.put("Date", catData.getDate());
			// catparser.put("Platform", catData.getPlatform());
			for (String key : catData.getCategoryMap().keySet()) {
				// ////System.out.println("Key "+ key+" VAL: "+catData.getCategoryMap().get(key));
				// catparser.put(key, catData.getCategoryMap().get(key));

				if (categoryList.containsKey(key)) {
					categoryList.put(key,
							(categoryList.get(key)) + Integer.parseInt(catData.getCategoryMap().get(key)));
				} else {
					categoryList.put(key, Integer.parseInt(catData.getCategoryMap().get(key)));
				}
			}
			// categoryList.add(catparser);
		}
		logger.info("[INFO] DATA :" + gson.toJson(categoryList));
		return gson.toJson(categoryList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getTotalAvtarCreatedGraphByDate", method = RequestMethod.GET)
	public String getTotalAvtarData() {
		Gson gson = new Gson();
		return gson.toJson(usageDataDAO.getTotalAvatarCreated("All"));
	}

	@CrossOrigin
	@RequestMapping(value = "/getTotalKeyboardEnabledGraphByDate", method = RequestMethod.GET)
	public String getTotalKeyboardData() {
		Gson gson = new Gson();
		return gson.toJson(usageDataDAO.getTotalKeyboardEnable("All"));
	}

	@CrossOrigin
	@RequestMapping(value = "/getActiveUsersBeforeDate-{platform}", method = RequestMethod.GET)
	public String getAllUsers1(@PathVariable("platform") String platform) {
		Gson gson = new Gson();
		return gson.toJson(usageDataDAO.getTotalUniqueUsers(platform));
	}

	// ############################ ACTIVE USER REPORTS END
	// #################################

	// ############################ PLATFORM USER REPORTS START
	// #################################

	@CrossOrigin
	@RequestMapping(value = "/getPlatformUserReport-{platform}", method = RequestMethod.GET)
	public String getTotalPlatformUsers(@PathVariable("platform") String platform) {
		logger.info("[INFO] Get platform User report");
		Gson gson = new Gson();
		PlatfromUserTotalHelper pdAndroid = null;
		PlatfromUserTotalHelper pdiOS = null;
		PlatfromUserTotalHelper pdDev = null;
		ArrayList<PlatfromUserTotalHelper> dataList = new ArrayList<PlatfromUserTotalHelper>();

		if (platform.equals("Android")) {
			pdAndroid = new PlatfromUserTotalHelper("Android", usageDataDAO.getTotalUniqueUsers("Android"));
			dataList.add(pdAndroid);
		} else if (platform.equals("iOS")) {
			pdiOS = new PlatfromUserTotalHelper("iOS", usageDataDAO.getTotalUniqueUsers("iOS"));
			dataList.add(pdiOS);
		} else if (platform.toUpperCase().equals("ALL")) {
			pdDev = new PlatfromUserTotalHelper("Android", usageDataDAO.getTotalUniqueUsers("Android"));
			dataList.add(pdDev);
			pdiOS = new PlatfromUserTotalHelper("iOS", usageDataDAO.getTotalUniqueUsers("iOS"));
			dataList.add(pdiOS);
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}

	/*
	 * 
	 * @CrossOrigin
	 * 
	 * @RequestMapping(value = "/getPlatformUserReportByDate-{platform}", method =
	 * RequestMethod.GET) public String getPlatformUsers(HttpServletRequest
	 * request, @PathVariable("platform") String platform) {
	 * logger.info("[INFO] Get platform User report by date"); String sDate =
	 * request.getHeader("Date-String-Start"); String eDate =
	 * request.getHeader("Date-String-End"); Gson gson = new Gson(); if (sDate ==
	 * null || eDate == null) { logger.error("[ERROR] GET Headers missing"); return
	 * gson.
	 * toJson("{\"message\":\"Date-String-Start and Date-String-End Required in GET Header\"}"
	 * ); } Date startDate = getFormattedDate(sDate); Date endDate =
	 * getFormattedDate(eDate); if (startDate == null || endDate == null) {
	 * logger.error("[ERROR] Date Not Parsable"); return
	 * gson.toJson("{\"message\":\"Date Not Parsable, Use dd-MM-yyyy format\"}"); }
	 * ArrayList<PlatformUserHelper> dataList = new ArrayList<PlatformUserHelper>();
	 * PlatformUserHelper pdAndroid = null; PlatformUserHelper pdiOS = null;
	 * PlatformUserHelper pdDev = null;
	 * 
	 * if (platform.equals("Android")) { while (true) { if
	 * (startDate.compareTo(endDate) > 0) { break; } pdAndroid = new
	 * PlatformUserHelper(sDate, usageDataDAO.getTotalUniqueUsersByDate("Android",
	 * sDate), usageDataDAO.getTotalUniqueUsersByDate("iOS", sDate));
	 * dataList.add(pdAndroid);
	 * 
	 * sDate = getNextDate(sDate); startDate = getFormattedDate(sDate); } } else if
	 * (platform.equals("iOS")) { while (true) { if (startDate.compareTo(endDate) >
	 * 0) { break; } pdiOS = new PlatformUserHelper(sDate,
	 * usageDataDAO.getTotalUniqueUsersByDate("Android", sDate),
	 * usageDataDAO.getTotalUniqueUsersByDate("iOS", sDate)); dataList.add(pdiOS);
	 * sDate = getNextDate(sDate); startDate = getFormattedDate(sDate); } } else if
	 * (platform.toUpperCase().equals("ALL")) { while (true) { if
	 * (startDate.compareTo(endDate) > 0) { break; } pdDev = new
	 * PlatformUserHelper(sDate, usageDataDAO.getTotalUniqueUsersByDate("Android",
	 * sDate), usageDataDAO.getTotalUniqueUsersByDate("iOS", sDate));
	 * dataList.add(pdDev); sDate = getNextDate(sDate); startDate =
	 * getFormattedDate(sDate); } } else { logger.error("[ERROR] Invalid Platform");
	 * return gson.toJson("{\"message\":\"Invalid Platform\"}"); }
	 * 
	 * logger.info("[INFO] DATA :" + gson.toJson(dataList)); return
	 * gson.toJson(dataList); }
	 * 
	 * // ############################ PLATFORM USER REPORTS END //
	 * #################################
	 * 
	 * // ############################ USER REGISTRATION REPORTS START //
	 * #################################
	 * 
	 * @CrossOrigin
	 * 
	 * @RequestMapping(value = "/getUserRegistrationReportByDate-{platform}", method
	 * = RequestMethod.GET) public String
	 * getUserRegistrationDataByDate(HttpServletRequest
	 * request, @PathVariable("platform") String platform) {
	 * logger.info("[INFO] Get User Registration report"); String sDate =
	 * request.getHeader("Date-String-Start"); String eDate =
	 * request.getHeader("Date-String-End"); Gson gson = new Gson(); if (sDate ==
	 * null || eDate == null) { logger.error("[ERROR] GET Headers missing"); return
	 * gson.
	 * toJson("{\"message\":\"Date-String-Start and Date-String-End Required in GET Header\"}"
	 * ); } Date startDate = getFormattedDate(sDate); Date endDate =
	 * getFormattedDate(eDate); if (startDate == null || endDate == null) {
	 * logger.error("[ERROR] Date Not Parsable"); return
	 * gson.toJson("{\"message\":\"Date Not Parsable, Use dd-MM-yyyy format\"}"); }
	 * ArrayList<RegisteredUserHelper> dataList = new
	 * ArrayList<RegisteredUserHelper>(); RegisteredUserHelper pdAndroid = null;
	 * RegisteredUserHelper pdiOS = null; RegisteredUserHelper pdDev = null;
	 * 
	 * if (platform.equals("Android")) { while (true) { if
	 * (startDate.compareTo(endDate) > 0) { break; } pdAndroid = new
	 * RegisteredUserHelper(sDate, "Android",
	 * usageDataDAO.getTotalRegisteredUsersByDate(platform, sDate),
	 * usageDataDAO.getTotalUnregisteredUsersByDate(platform, sDate));
	 * dataList.add(pdAndroid); sDate = getNextDate(sDate); startDate =
	 * getFormattedDate(sDate); } } else if (platform.equals("iOS")) { while (true)
	 * { if (startDate.compareTo(endDate) > 0) { break; } pdiOS = new
	 * RegisteredUserHelper(sDate, "iOS",
	 * usageDataDAO.getTotalRegisteredUsersByDate(platform, sDate),
	 * usageDataDAO.getTotalUnregisteredUsersByDate(platform, sDate));
	 * dataList.add(pdiOS); sDate = getNextDate(sDate); startDate =
	 * getFormattedDate(sDate); } } else if (platform.toUpperCase().equals("ALL")) {
	 * while (true) { if (startDate.compareTo(endDate) > 0) { break; } pdDev = new
	 * RegisteredUserHelper(sDate, "All",
	 * usageDataDAO.getTotalRegisteredUsersByDate("All", sDate),
	 * usageDataDAO.getTotalUnregisteredUsersByDate("All", sDate));
	 * dataList.add(pdDev); sDate = getNextDate(sDate); startDate =
	 * getFormattedDate(sDate); } } else { logger.error("[ERROR] Invalid Platform");
	 * return gson.toJson("{\"message\":\"Invalid Platform\"}"); }
	 * logger.info("[INFO] DATA :" + gson.toJson(dataList)); return
	 * gson.toJson(dataList); }
	 * 
	 * // ############################ USER REGISTRATION REPORTS END //
	 * #################################
	 * 
	 * @CrossOrigin
	 * 
	 * @RequestMapping(value = "/getAllUsersByDate-{platform}", method =
	 * RequestMethod.GET) public String getAllUsersByDate(HttpServletRequest
	 * request, @PathVariable("platform") String platform) { String date =
	 * request.getHeader("Date-String"); ////System.out.println(date); Gson gson = new
	 * Gson(); return gson.toJson(usageDataDAO.getTotalUniqueUsersByDate(platform,
	 * date)); }
	 * 
	 * @CrossOrigin
	 * 
	 * @RequestMapping(value = "/getAllUsersBeforeDate-{platform}", method =
	 * RequestMethod.GET) public String getAllUsersBeforeDate(HttpServletRequest
	 * request, @PathVariable("platform") String platform) { String date =
	 * request.getHeader("Date-String"); Gson gson = new Gson(); return
	 * gson.toJson(usageDataDAO.getTotalUniqueUsersBeforeDate(platform, date)); }
	 * 
	 * @CrossOrigin
	 * 
	 * @RequestMapping(value = "/getAllUsersBetweenDate-{platform}", method =
	 * RequestMethod.GET) public String getAllUsersBetweenDates(HttpServletRequest
	 * request, @PathVariable("platform") String platform) { String sDate =
	 * request.getHeader("Date-String-Start"); String eDate =
	 * request.getHeader("Date-String-End"); Gson gson = new Gson(); return
	 * gson.toJson(usageDataDAO.getTotalUniqueUsersBetweenDates(platform, sDate,
	 * eDate)); }
	 * 
	 * @CrossOrigin
	 * 
	 * @RequestMapping(value = "/getAllUnregUsers-{platform}", method =
	 * RequestMethod.GET) public String getUnregUsers(@PathVariable("platform")
	 * String platform) { Gson gson = new Gson(); return
	 * gson.toJson(usageDataDAO.getTotalUnregisteredUsers(platform)); }
	 * 
	 * @CrossOrigin
	 * 
	 * @RequestMapping(value = "/getAllRegisteredUsers-{platform}", method =
	 * RequestMethod.GET) public String getAllUsers(@PathVariable("platform") String
	 * platform) { Gson gson = new Gson(); return
	 * gson.toJson(usageDataDAO.getTotalRegisteredUsers(platform)); }
	 * 
	 * // ######################### ACTIVE USERS REPORT END //
	 * ##################################
	 * 
	 * @CrossOrigin
	 * 
	 * @RequestMapping(value = "/getActiveUsersReportByDate1-{platform}", method =
	 * RequestMethod.GET) public String getAvatarCreatedByDate1(HttpServletRequest
	 * request, @PathVariable("platform") String platform) { String date =
	 * request.getHeader("Date-String"); ////System.out.println(date); Gson gson = new
	 * Gson(); return gson.toJson(usageDataDAO.getTotalUniqueUsersByDate(platform,
	 * date)); }
	 * 
	 * @CrossOrigin
	 * 
	 * @RequestMapping(value = "/getActiveUsersReportBeforeDate-{platform}", method
	 * = RequestMethod.GET) public String
	 * getAvatarCreatedBeforeDate(HttpServletRequest
	 * request, @PathVariable("platform") String platform) { String date =
	 * request.getHeader("Date-String"); Gson gson = new Gson(); return
	 * gson.toJson(usageDataDAO.getTotalUniqueUsersBeforeDate(platform, date)); }
	 * 
	 * @CrossOrigin
	 * 
	 * @RequestMapping(value = "/getActiveUsersReportBetweenDate-{platform}", method
	 * = RequestMethod.GET) public String
	 * getAvatarCreatedBetweenDates(HttpServletRequest
	 * request, @PathVariable("platform") String platform) { String sDate =
	 * request.getHeader("Date-String-Start"); String eDate =
	 * request.getHeader("Date-String-End"); Gson gson = new Gson(); return
	 * gson.toJson(usageDataDAO.getTotalUniqueUsersBetweenDates(platform, sDate,
	 * eDate)); }
	 * 
	 * @CrossOrigin
	 * 
	 * @RequestMapping(value = "/createTenUsers", method = RequestMethod.GET) public
	 * String saveUsageData() { logger.info("Save a user.");
	 * 
	 * ArrayList<HashMap<String, String>> aMap = new ArrayList<HashMap<String,
	 * String>>(); HashMap<String, String> dataMap = new HashMap<String, String>();
	 * 
	 * dataMap.put("category", "Greetings"); dataMap.put("subcategory",
	 * "Daily Talk Phrases"); dataMap.put("imageId", "VALUE 3"); aMap.add(dataMap);
	 * 
	 * HashMap<String, String> dataMap2 = new HashMap<String, String>();
	 * dataMap2.put("subcategory", "Happy"); dataMap2.put("imageId", "VALUE 3");
	 * aMap.add(dataMap2);
	 * 
	 * HashMap<String, String> dataMap3 = new HashMap<String, String>();
	 * dataMap3.put("subcategory", "Sad"); dataMap3.put("imageId", "VALUE 3");
	 * aMap.add(dataMap3);
	 * 
	 * HashMap<String, String> dataMap4 = new HashMap<String, String>();
	 * dataMap4.put("subcategory", "Crazy"); dataMap4.put("imageId", "VALUE 3");
	 * aMap.add(dataMap4);
	 * 
	 * HashMap<String, String> dataMap5 = new HashMap<String, String>();
	 * dataMap5.put("subcategory", "Crazy"); dataMap5.put("imageId", "VALUE 3");
	 * aMap.add(dataMap5);
	 * 
	 * HashMap<String, String> dataMap6 = new HashMap<String, String>();
	 * dataMap5.put("subcategory", "Crazy"); dataMap5.put("imageId", "VALUE 3");
	 * aMap.add(dataMap6);
	 * 
	 * HashMap<String, String> dataMap7 = new HashMap<String, String>();
	 * dataMap5.put("subcategory", "Crazy"); dataMap5.put("imageId", "VALUE 3");
	 * aMap.add(dataMap7);
	 * 
	 * HashMap<String, String> dataMap8 = new HashMap<String, String>();
	 * dataMap5.put("subcategory", "Crazy"); dataMap5.put("imageId", "VALUE 3");
	 * aMap.add(dataMap8);
	 * 
	 * HashMap<String, String> dataMap9 = new HashMap<String, String>();
	 * dataMap5.put("subcategory", "Crazy"); dataMap5.put("imageId", "VALUE 3");
	 * aMap.add(dataMap9);
	 * 
	 * HashMap<String, String> dataMap10 = new HashMap<String, String>();
	 * dataMap5.put("subcategory", "Crazy"); dataMap5.put("imageId", "VALUE 3");
	 * aMap.add(dataMap10); ArrayList<Date> aDate = new ArrayList<Date>();
	 * 
	 * aDate.add(getFormattedDate("17-08-2018 00:00:00"));
	 * aDate.add(getFormattedDate("18-08-2018 00:00:00"));
	 * aDate.add(getFormattedDate("19-08-2018 00:00:00"));
	 * aDate.add(getFormattedDate("20-08-2018 00:00:00"));
	 * aDate.add(getFormattedDate("21-08-2018 00:00:00"));
	 * aDate.add(getFormattedDate("22-08-2018 00:00:00"));
	 * aDate.add(getFormattedDate("23-08-2018 00:00:00"));
	 * aDate.add(getFormattedDate("24-08-2018 00:00:00"));
	 * aDate.add(getFormattedDate("25-08-2018 00:00:00"));
	 * 
	 * for (int i = 0; i < 9; i++) { UsageData ud = new UsageData();
	 * ud.setApplicationID("ABCDEF-" + Math.random() * 49 + "ABCDEF" + Math.random()
	 * * 49 + "AA" + i); ud.setDateTime(aDate.get(i)); ud.setData(aMap.get(i));
	 * ud.setEventCode("11"); ud.setPlatform("Android"); ud.setUserID("USER" +
	 * Math.random() * 39);
	 * 
	 * usageDataDAO.saveUsageData(ud);
	 * 
	 * if (i > 5) { continue; }
	 * 
	 * UsageData ud2 = new UsageData(); ud2.setApplicationID("ZXCVBN-" +
	 * Math.random() * 49 + "ZXCVBN" + Math.random() * 49 + "AA" + i);
	 * ud2.setDateTime(aDate.get(i)); ud2.setData(aMap.get(i));
	 * ud2.setEventCode("11"); ud2.setPlatform("iOS"); ud2.setUserID("USERIOS" +
	 * Math.random() * 39);
	 * 
	 * usageDataDAO.saveUsageData(ud2); /* UsageData ud3 = new UsageData();
	 * ud3.setApplicationID("ZXCVBN-" + Math.random() * 49 + "ZXCVBN" +
	 * Math.random() * 49 + "AA" + i); ud3.setDateTime(aDate.get(i));
	 * ud3.setData(dataMap); ud3.setEventCode("11"); ud3.setPlatform("iOS");
	 * ud3.setUserID("USERIOS" + Math.random() * 39);
	 * 
	 * usageDataDAO.saveUsageData(ud3);
	 * 
	 * UsageData ud4 = new UsageData(); ud4.setApplicationID("ABCDEF-" +
	 * Math.random() * 49 + "ABCDEF" + Math.random() * 49 + "AA" + i);
	 * ud4.setDateTime(aDate.get(i)); ud4.setData(dataMap); ud4.setEventCode("11");
	 * ud4.setPlatform("Android"); ud4.setUserID("USER" + Math.random() * 39);
	 * 
	 * usageDataDAO.saveUsageData(ud4);
	 */
	/*
	 * } return "success"; }
	 */

	/*
	 * ANALYTICS START
	 */

	@CrossOrigin
	@RequestMapping(value = "/getAllData", method = RequestMethod.GET)
	public String getAllData() {
		logger.info("[INFO]Get all data.");
		String data = usageDataDAO.getAllData();
		logger.info("[INFO] DATA : " + data);
		return data;
	}

	@CrossOrigin
	@RequestMapping(value = "/getAllDataByPlatform-{sPlatform}", method = RequestMethod.GET)
	public String getAllData(@PathVariable("sPlatform") String sPlatform) {
		logger.info("[INFO]Get platform data for." + sPlatform);
		String data = usageDataDAO.getAllData(sPlatform);
		logger.info("[INFO] DATA : " + data);
		return data;
	}

	@CrossOrigin
	@RequestMapping(value = "/getAllDataByPlatformAndEvent-{sPlatform}-{iEvent}", method = RequestMethod.GET)
	public String getAllData(@PathVariable("sPlatform") String sPlatform, @PathVariable("iEvent") Integer iEvent) {
		logger.info("[INFO]Get platform data for." + sPlatform);
		String data = usageDataDAO.getAllData(sPlatform, iEvent);
		logger.info("[INFO] DATA : " + data);
		return data;
	}

	@CrossOrigin
	@RequestMapping(value = "/getSignupReport", method = RequestMethod.GET)
	public String getUsageData() {
		logger.info("[INFO]Get signup report.");
		Gson gson = new Gson();
		ArrayList<PlatformData> dataList = new ArrayList<PlatformData>();

		PlatformData pdAndroid = new PlatformData("Android", usageDataDAO.getTotalAndroidSignupCountForFB(),
				usageDataDAO.getTotalAndroidSignupCountForMobile());
		PlatformData piOS = new PlatformData("iOS", usageDataDAO.getTotaliOSSignupCountForFB(),
				usageDataDAO.getTotaliOSSignupCountForMobile());
		dataList.add(pdAndroid);
		dataList.add(piOS);
		logger.info("[INFO] DATA : " + gson.toJson(dataList));
		return gson.toJson(dataList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getSignupReportBefore_{sPlatform}_{eDate}", method = RequestMethod.GET)
	public String getFilteredUsageDataBefore(@PathVariable("sPlatform") String sPlatform,
			@PathVariable("eDate") String eDate) {
		logger.info("[INFO] Get signup report for " + sPlatform + " Before " + eDate);
		Gson gson = new Gson();
		ArrayList<PlatformData> dataList = new ArrayList<PlatformData>();

		if (sPlatform.equals("Android")) {
			PlatformData pdAndroid = new PlatformData("Android",
					usageDataDAO.getAndroidSignupCountForFBBeforeDate(eDate),
					usageDataDAO.getAndroidSignupCountForMobileBeforeDate(eDate));
			dataList.add(pdAndroid);
			logger.info("[INFO] DATA :" + gson.toJson(dataList));
			return gson.toJson(dataList);
		} else if (sPlatform.equals("iOS")) {
			PlatformData piOS = new PlatformData("iOS", usageDataDAO.getiOSSignupCountForFBBeforeDate(eDate),
					usageDataDAO.getiOSSignupCountForMobileBeforeDate(eDate));
			dataList.add(piOS);
			logger.info("[INFO] DATA :" + gson.toJson(dataList));
			return gson.toJson(dataList);
		} else if (sPlatform.toUpperCase().equals("BOTH")) {
			PlatformData piOS = new PlatformData("iOS", usageDataDAO.getiOSSignupCountForFBBeforeDate(eDate),
					usageDataDAO.getiOSSignupCountForMobileBeforeDate(eDate));
			PlatformData pdAndroid = new PlatformData("Android",
					usageDataDAO.getAndroidSignupCountForFBBeforeDate(eDate),
					usageDataDAO.getAndroidSignupCountForMobileBeforeDate(eDate));
			dataList.add(pdAndroid);
			dataList.add(piOS);
			logger.info("[INFO] DATA :" + gson.toJson(dataList));
			return gson.toJson(dataList);
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("Invalid Platform");
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/getSignupReportAfter_{sPlatform}_{sDate}", method = RequestMethod.GET)
	public String getFilteredUsageData(@PathVariable("sPlatform") String sPlatform,
			@PathVariable("sDate") String sDate) {
		logger.info("[INFO] Get signup report for " + sPlatform + " After " + sDate);
		Gson gson = new Gson();
		ArrayList<PlatformData> dataList = new ArrayList<PlatformData>();

		if (sPlatform.equals("Android")) {
			PlatformData pdAndroid = new PlatformData("Android",
					usageDataDAO.getAndroidSignupCountForFBAfterDate(sDate),
					usageDataDAO.getAndroidSignupCountForMobileAfterDate(sDate));
			dataList.add(pdAndroid);
			logger.info("[INFO] DATA :" + gson.toJson(dataList));
			return gson.toJson(dataList);
		} else if (sPlatform.equals("iOS")) {
			PlatformData piOS = new PlatformData("iOS", usageDataDAO.getiOSSignupCountForFBAfterDate(sDate),
					usageDataDAO.getiOSSignupCountForMobileAfterDate(sDate));
			dataList.add(piOS);
			logger.info("[INFO] DATA :" + gson.toJson(dataList));
			return gson.toJson(dataList);
		} else if (sPlatform.toUpperCase().equals("BOTH")) {
			PlatformData piOS = new PlatformData("iOS", usageDataDAO.getiOSSignupCountForFBAfterDate(sDate),
					usageDataDAO.getiOSSignupCountForMobileAfterDate(sDate));
			PlatformData pdAndroid = new PlatformData("Android",
					usageDataDAO.getAndroidSignupCountForFBAfterDate(sDate),
					usageDataDAO.getAndroidSignupCountForMobileAfterDate(sDate));
			dataList.add(pdAndroid);
			dataList.add(piOS);
			logger.info("[INFO] DATA :" + gson.toJson(dataList));
			return gson.toJson(dataList);
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("Invalid Platform");
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/getSignupReportBetween_{sPlatform}_{sDate}_{eDate}", method = RequestMethod.GET)
	public String getFilteredUsageData(@PathVariable("sPlatform") String sPlatform, @PathVariable("sDate") String sDate,
			@PathVariable("eDate") String eDate) {
		logger.info("[INFO] Get signup report for " + sPlatform + " and dates " + sDate + " to " + eDate);
		Gson gson = new Gson();
		ArrayList<PlatformData> dataList = new ArrayList<PlatformData>();

		if (sPlatform.equals("Android")) {
			PlatformData pdAndroid = new PlatformData("Android", usageDataDAO.getAndroidSignupCountForFB(sDate, eDate),
					usageDataDAO.getAndroidSignupCountForMobile(sDate, eDate));
			dataList.add(pdAndroid);
			logger.info("[INFO] DATA :" + gson.toJson(dataList));
			return gson.toJson(dataList);
		} else if (sPlatform.equals("iOS")) {
			PlatformData piOS = new PlatformData("iOS", usageDataDAO.getiOSSignupCountForFB(sDate, eDate),
					usageDataDAO.getiOSSignupCountForMobile(sDate, eDate));
			dataList.add(piOS);
			logger.info("[INFO] DATA :" + gson.toJson(dataList));
			return gson.toJson(dataList);
		} else if (sPlatform.toUpperCase().equals("BOTH")) {
			PlatformData piOS = new PlatformData("iOS", usageDataDAO.getiOSSignupCountForFB(sDate, eDate),
					usageDataDAO.getiOSSignupCountForMobile(sDate, eDate));
			PlatformData pdAndroid = new PlatformData("Android", usageDataDAO.getAndroidSignupCountForFB(sDate, eDate),
					usageDataDAO.getAndroidSignupCountForMobile(sDate, eDate));
			dataList.add(pdAndroid);
			dataList.add(piOS);
			logger.info("[INFO] DATA :" + gson.toJson(dataList));
			return gson.toJson(dataList);
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("Invalid Platform");
		}
	}

	/*
	 * 
	 * @CrossOrigin
	 * 
	 * @RequestMapping(value = "/getSignupReportByDate_{sPlatform}_{sDate}_{eDate}",
	 * method = RequestMethod.GET) public String
	 * getFilteredUsageDataByDate(@PathVariable("sPlatform") String
	 * sPlatform, @PathVariable("sDate") String sDate,
	 * 
	 * @PathVariable("eDate") String eDate) {
	 * logger.info("[INFO] Get signup report for " + sPlatform + " and dates " +
	 * sDate + " to " + eDate); Gson gson = new Gson();
	 * ArrayList<PlatformDataByDate> dataList = new ArrayList<PlatformDataByDate>();
	 * DateFormat formatter = new
	 * SimpleDateFormat("dd-MM-yyyy hh:mm:ss",Locale.ENGLISH); Date startDate; try {
	 * startDate = formatter.parse(sDate); } catch (ParseException e1) {
	 * logger.error("[ERROR] DATE not parsable"); return
	 * gson.toJson("Invalid Platform"); } Date endDate; try { endDate =
	 * formatter.parse(eDate); } catch (ParseException e1) {
	 * logger.error("[ERROR] DATE not parsable"); return
	 * gson.toJson("Invalid Platform"); } PlatformDataByDate pdAndroid = null;
	 * PlatformDataByDate pdiOS =null; PlatformDataByDate pdDev = null;
	 * 
	 * 
	 * if(sPlatform.equals("Android")) { while(true) {
	 * if(startDate.compareTo(endDate)>=0) { break; }
	 * 
	 * pdAndroid = new
	 * PlatformDataByDate(sPlatform,usageDataDAO.getAndroidSignupCountForFBByDate(
	 * sDate),usageDataDAO.getAndroidSignupCountForMobileByDate(sDate),sDate);
	 * dataList.add(pdAndroid);
	 * 
	 * sDate = getNextDate(sDate); try { startDate = formatter.parse(sDate); } catch
	 * (ParseException e) { logger.error("[ERROR] DATE not parsable"); return
	 * gson.toJson("Invalid Platform"); } } }else if(sPlatform.equals("iOS")) {
	 * while(true) { if(startDate.compareTo(endDate)>=0) { break; }
	 * 
	 * pdiOS = new
	 * PlatformDataByDate(sPlatform,usageDataDAO.getiOSSignupCountForFBByDate(sDate)
	 * ,usageDataDAO.getiOSSignupCountForMobileByDate(sDate),sDate);
	 * dataList.add(pdiOS);
	 * 
	 * sDate = getNextDate(sDate);
	 * 
	 * try { startDate = formatter.parse(sDate); } catch (ParseException e) {
	 * logger.error("[ERROR] DATE not parsable"); return
	 * gson.toJson("Invalid Platform"); } } }else
	 * if(sPlatform.toUpperCase().equals("BOTH")) { while(true) {
	 * if(startDate.compareTo(endDate)>=0) { break; }
	 * 
	 * pdDev = new
	 * PlatformDataByDate(sPlatform,usageDataDAO.getiOSAndAndroidCountForFBByDate(
	 * sDate),usageDataDAO.getiOSAndAndroidCountForMobByDate(sDate),sDate);
	 * 
	 * dataList.add(pdDev);
	 * 
	 * 
	 * sDate = getNextDate(sDate); try { startDate = formatter.parse(sDate); } catch
	 * (ParseException e) { logger.error("[ERROR] DATE not parsable"); return
	 * gson.toJson("Invalid Platform"); } } }else {
	 * logger.error("[ERROR] Invalid Platform"); return
	 * gson.toJson("Invalid Platform"); }
	 * 
	 * logger.info("[INFO] DATA :" + gson.toJson(dataList)); return
	 * gson.toJson(dataList); }
	 */

	private String getsStartDate(@Valid String message) throws JsonParseException, JsonMappingException, IOException {

		final ObjectNode node = new ObjectMapper().readValue(message, ObjectNode.class);
		if (node.has("startDate")) {
			return node.get("startDate").toString();
		}
		return null;
	}

	private String getsEndDate(@Valid String message) throws JsonParseException, JsonMappingException, IOException {
		final ObjectNode node = new ObjectMapper().readValue(message, ObjectNode.class);
		if (node.has("endDate")) {
			// ////System.out.println("platform: " + node.get("platform"));
			return node.get("endDate").toString();
		}
		return null;
	}

	private String getsPlatform(@Valid String message) throws JsonParseException, JsonMappingException, IOException {
		final ObjectNode node = new ObjectMapper().readValue(message, ObjectNode.class);
		if (node.has("platform")) {
			// ////System.out.println("platform: " + node.get("platform"));
			return node.get("platform").toString();
		}
		return null;
	}

	public String getNextDate(String curDate) {
		String nextDate = "";
		try {
			Calendar today = Calendar.getInstance();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date date = format.parse(curDate);
			today.setTime(date);
			today.add(Calendar.DAY_OF_YEAR, 1);
			nextDate = format.format(today.getTime());
		} catch (Exception e) {
			return nextDate;
		}
		return nextDate;
	}

	/* Category */

	@CrossOrigin
	@RequestMapping(value = "/getCategoryReport-{platform}", method = RequestMethod.GET)
	public String getCategoryData(@PathVariable("platform") String platform) {
		logger.info("[INFO] Get category report");
		String sPlatform = platform;
		Gson gson = new Gson();

		// ////System.out.println("\n Platform :"+sPlatform+" Start DATe :"+sDate+" End
		// Date
		// :"+eDate);

		ArrayList<CategoryDataTotal> dataList = new ArrayList<CategoryDataTotal>();

		CategoryDataTotal pdAndroid = null;
		CategoryDataTotal pdiOS = null;
		CategoryDataTotal pdDev = null;

		if (sPlatform.equals("Android")) {

			// Get All Categories for Android
			ArrayList<String> catList = (ArrayList<String>) usageDataDAO.getSubCategoryListAndroid();

			// ////System.out.println("\nCAT COUNT : "+catList.size());
			HashMap<String, String> hMap = new HashMap<String, String>();
			for (String cat : catList) {
				long count = usageDataDAO.getSubCategoryCountforAndroid(cat);
				hMap.put(cat, count + "");
			}

			pdAndroid = new CategoryDataTotal(sPlatform, hMap);
			dataList.add(pdAndroid);

		} else if (sPlatform.equals("iOS")) {

			// Get All Categories for iOS
			ArrayList<String> catList = (ArrayList<String>) usageDataDAO.getSubCategoryListiOS();
			HashMap<String, String> hMap = new HashMap<String, String>();
			for (String cat : catList) {
				long count = usageDataDAO.getSubCategoryCountforiOS(cat);
				hMap.put(cat, +count + "");
			}

			pdiOS = new CategoryDataTotal(sPlatform, hMap);
			dataList.add(pdiOS);

		} else if (sPlatform.toUpperCase().equals("ALL")) {

			// Get All Categories for iOS
			HashSet<String> catList = (HashSet<String>) usageDataDAO.getSubCategoryList();
			HashMap<String, String> hMap = new HashMap<String, String>();
			for (String cat : catList) {
				long count = usageDataDAO.getSubCategoryCountforAndroid(cat);
				hMap.put(cat, count + "");
			}
			pdDev = new CategoryDataTotal("Android", hMap);
			dataList.add(pdDev);

			HashMap<String, String> hMap2 = new HashMap<String, String>();
			for (String cat : catList) {
				long count = usageDataDAO.getSubCategoryCountforiOS(cat);
				hMap2.put(cat, count + "");
			}
			pdiOS = new CategoryDataTotal("iOS", hMap2);
			dataList.add(pdiOS);

		} else {
			// logger.error("[ERROR] Invalid Platform");
			return gson.toJson("Invalid Platform");
		}

		// logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}

	// ############################# FRESH ANALYTICS
	// #######################################

	public Date getFormattedDate(String date) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault());
		// formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		Date retDate = null;
		try {
			retDate = formatter.parse(date + " 00:00:00");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return retDate;
	}

	public static Date getNextDateObject(Date curDate) {
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(curDate);
		calendar.add(Calendar.DAY_OF_YEAR, 1);
		return calendar.getTime();
	}

	// ############################## NEW ##################################

	/*@CrossOrigin
	@RequestMapping(value = "/getScreenViewReportByDate", method = RequestMethod.POST)
	public String getScreenViewDataByDate(@RequestBody String request) {
		logger.info("[INFO] Get getScreenViewDataByDate report for " + request);
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<ScreenViewData> dataList = new ArrayList<ScreenViewData>();
		ScreenViewData pdAndroid = null;
		ScreenViewData pdiOS = null;
		ScreenViewData pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new ScreenViewData(sDate, sPlatform,
						usageDataDAO.getHomeScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getAvatarScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getStickerScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getSettingsScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getSearchScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getOnboardingScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getNotificationSettingScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getLanguagePreferenceScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getSignUpScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getMobileSignUpScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getLoginScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getLoginMobileScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getHelpCentreScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getReportAProblemScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getContactUsScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getLegalInfoScreenViewedCountByDate(sPlatform, startDate));
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new ScreenViewData(sDate, sPlatform,
						usageDataDAO.getHomeScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getAvatarScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getStickerScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getSettingsScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getSearchScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getOnboardingScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getNotificationSettingScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getLanguagePreferenceScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getSignUpScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getMobileSignUpScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getLoginScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getLoginMobileScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getHelpCentreScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getReportAProblemScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getContactUsScreenViewedCountByDate(sPlatform, startDate),
						usageDataDAO.getLegalInfoScreenViewedCountByDate(sPlatform, startDate));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new ScreenViewData(sDate, sPlatform,
						usageDataDAO.getHomeScreenViewedCountByDate("All", startDate),
						usageDataDAO.getAvatarScreenViewedCountByDate("All", startDate),
						usageDataDAO.getStickerScreenViewedCountByDate("All", startDate),
						usageDataDAO.getSettingsScreenViewedCountByDate("All", startDate),
						usageDataDAO.getSearchScreenViewedCountByDate("All", startDate),
						usageDataDAO.getOnboardingScreenViewedCountByDate("All", startDate),
						usageDataDAO.getNotificationSettingScreenViewedCountByDate("All", startDate),
						usageDataDAO.getLanguagePreferenceScreenViewedCountByDate("All", startDate),
						usageDataDAO.getSignUpScreenViewedCountByDate("All", startDate),
						usageDataDAO.getMobileSignUpScreenViewedCountByDate("All", startDate),
						usageDataDAO.getLoginScreenViewedCountByDate("All", startDate),
						usageDataDAO.getLoginMobileScreenViewedCountByDate("All", startDate),
						usageDataDAO.getHelpCentreScreenViewedCountByDate("All", startDate),
						usageDataDAO.getReportAProblemScreenViewedCountByDate("All", startDate),
						usageDataDAO.getContactUsScreenViewedCountByDate("All", startDate),
						usageDataDAO.getLegalInfoScreenViewedCountByDate("All", startDate));
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}*/
	
/*	@CrossOrigin
	@RequestMapping(value = "/getScreenViewReportByDateUserLevel", method = RequestMethod.POST)
	public String getScreenViewDataByDateUserLevel(@RequestBody String request) {
		logger.info("[INFO] Get getScreenViewDataByDate report for " + request);
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<ScreenViewData> dataList = new ArrayList<ScreenViewData>();
		ScreenViewData pdAndroid = null;
		ScreenViewData pdiOS = null;
		ScreenViewData pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new ScreenViewData(sDate, sPlatform,
usageDataDAO.getHomeScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getHomeScreenViewedCountByDateUserLevel(sPlatform, startDate),
usageDataDAO.getAvatarScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getAvatarScreenViewedCountByDateUserLevel(sPlatform, startDate),
usageDataDAO.getStickerScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getStickerScreenViewedCountByDateUserLevel(sPlatform, startDate),
usageDataDAO.getSettingsScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getSettingsScreenViewedCountByDateUserLevel(sPlatform, startDate),
usageDataDAO.getSearchScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getSearchScreenViewedCountByDateUserLevel(sPlatform, startDate),
usageDataDAO.getOnboardingScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getOnboardingScreenViewedCountByDateUserLevel(sPlatform, startDate),
usageDataDAO.getNotificationSettingScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getNotificationSettingScreenViewedCountByDateUserLevel(sPlatform, startDate),
usageDataDAO.getLanguagePreferenceScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getLanguagePreferenceScreenViewedCountByDateUserLevel(sPlatform, startDate),
usageDataDAO.getSignUpScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getSignUpScreenViewedCountByDateUserLevel(sPlatform, startDate),
usageDataDAO.getMobileSignUpScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getMobileSignUpScreenViewedCountByDateUserLevel(sPlatform, startDate),
usageDataDAO.getLoginScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getLoginScreenViewedCountByDateUserLevel(sPlatform, startDate),
usageDataDAO.getLoginMobileScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getLoginMobileScreenViewedCountByDateUserLevel(sPlatform, startDate),
usageDataDAO.getHelpCentreScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getHelpCentreScreenViewedCountByDateUserLevel(sPlatform, startDate),
usageDataDAO.getReportAProblemScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getReportAProblemScreenViewedCountByDateUserLevel(sPlatform, startDate),
usageDataDAO.getContactUsScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getContactUsScreenViewedCountByDateUserLevel(sPlatform, startDate),
usageDataDAO.getLegalInfoScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getLegalInfoScreenViewedCountByDateUserLevel(sPlatform, startDate));
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new ScreenViewData(sDate, sPlatform,
						usageDataDAO.getHomeScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getHomeScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getAvatarScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getAvatarScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getStickerScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getStickerScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getSettingsScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getSettingsScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getSearchScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getSearchScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getOnboardingScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getNotificationSettingScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getNotificationSettingScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getLanguagePreferenceScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getLanguagePreferenceScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getSignUpScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getSignUpScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getMobileSignUpScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getMobileSignUpScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getLoginScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getLoginScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getLoginMobileScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getLoginMobileScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getHelpCentreScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getHelpCentreScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getReportAProblemScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getReportAProblemScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getContactUsScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getContactUsScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getLegalInfoScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getLegalInfoScreenViewedCountByDateUserLevel(sPlatform, startDate));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new ScreenViewData(sDate, sPlatform,
						usageDataDAO.getHomeScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getHomeScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getAvatarScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getAvatarScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getStickerScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getStickerScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getSettingsScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getSettingsScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getSearchScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getSearchScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getOnboardingScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getNotificationSettingScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getNotificationSettingScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getLanguagePreferenceScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getLanguagePreferenceScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getSignUpScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getSignUpScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getMobileSignUpScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getMobileSignUpScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getLoginScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getLoginScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getLoginMobileScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getLoginMobileScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getHelpCentreScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getHelpCentreScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getReportAProblemScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getReportAProblemScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getContactUsScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getContactUsScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getLegalInfoScreenViewedCountByDate(sPlatform, startDate)+"-"+usageDataDAO.getLegalInfoScreenViewedCountByDateUserLevel(sPlatform, startDate));
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}*/

/*	@CrossOrigin
	@RequestMapping(value = "/getUniqueSignupReportByDate", method = RequestMethod.POST)
	public String getUniqueFilteredUsageDataByDate(@RequestBody String request) {
		logger.info("[INFO] Get signup report By date");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<PlatformDataByDate> dataList = new ArrayList<PlatformDataByDate>();
		PlatformDataByDate pdAndroid = null;
		PlatformDataByDate pdiOS = null;
		PlatformDataByDate pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new PlatformDataByDate(sDate, sPlatform, repo.findFBUsersByDate("Android", sDate),
						repo.findMobileUsersByDate("Android", sDate)
				// usageDataDAO.getUniqueSignupCountByDate(startDate, "Android", "FB"),
				// usageDataDAO.getUniqueSignupCountByDate(startDate, "Android", "MOBILE")
				);
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new PlatformDataByDate(sDate, sPlatform,
						usageDataDAO.getUniqueSignupCountByDate(startDate, "iOS", "FB"),
						usageDataDAO.getUniqueSignupCountByDate(startDate, "iOS", "MOBILE"));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new PlatformDataByDate(sDate, "All",
						// usageDataDAO.getUniqueSignupCountByDate(startDate, "All", "FB"),
						// usageDataDAO.getUniqueSignupCountByDate(startDate, "All", "MOBILE")
						repo.findFBUsersByDate("All", sDate), repo.findMobileUsersByDate("All", sDate));
				dataList.add(pdDev);

				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}*/

	@CrossOrigin
	@RequestMapping(value = "/getExtendedKeyboardReportByDate", method = RequestMethod.POST)
	public String getExtendedKeyboardDataByDate(@RequestBody String request) {
		logger.info("[INFO] Get signup report for " + request);
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<KeyboardDetail> dataList = new ArrayList<KeyboardDetail>();
		KeyboardDetail pdAndroid = null;
		KeyboardDetail pdiOS = null;
		KeyboardDetail pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new KeyboardDetail(sDate, sPlatform,
						usageDataDAO.getKeyboardInitCountByDate(startDate, "Android"),
						usageDataDAO.getKeyboardEnableCountByDate(startDate, "Android"),
						usageDataDAO.getKeyboardCompleteCountByDate(startDate, "Android"),
						usageDataDAO.getKeyboardSearchedCountByDate(startDate, "Android"));
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new KeyboardDetail(sDate, sPlatform, usageDataDAO.getKeyboardInitCountByDate(startDate, "iOS"),
						usageDataDAO.getKeyboardEnableCountByDate(startDate, "iOS"),
						usageDataDAO.getKeyboardCompleteCountByDate(startDate, "iOS"),
						usageDataDAO.getKeyboardSearchedCountByDate(startDate, "iOS"));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new KeyboardDetail(sDate, sPlatform, usageDataDAO.getKeyboardInitCountByDate(startDate, "All"),
						usageDataDAO.getKeyboardEnableCountByDate(startDate, "All"),
						usageDataDAO.getKeyboardCompleteCountByDate(startDate, "All"),
						usageDataDAO.getKeyboardSearchedCountByDate(startDate, "All"));
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}

	/*@CrossOrigin
	@RequestMapping(value = "/getExtendedKeyboardReportByDateUserLevel", method = RequestMethod.POST)
	public String getExtendedKeyboardDataByDateUserLevel(@RequestBody String request) {
		logger.info("[INFO] Get signup report for " + request);
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<KeyboardDetail> dataList = new ArrayList<KeyboardDetail>();
		KeyboardDetail pdAndroid = null;
		KeyboardDetail pdiOS = null;
		KeyboardDetail pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new KeyboardDetail(sDate, sPlatform,
						usageDataDAO.getKeyboardInitCountByDateUserLevel(startDate, "Android"),
						usageDataDAO.getKeyboardEnableCountByDateUserLevel(startDate, "Android"),
						usageDataDAO.getKeyboardCompleteCountByDateUserLevel(startDate, "Android"),
						usageDataDAO.getKeyboardSearchedCountByDateUserLevel(startDate, "Android"));
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new KeyboardDetail(sDate, sPlatform, usageDataDAO.getKeyboardInitCountByDate(startDate, "iOS"),
						usageDataDAO.getKeyboardEnableCountByDate(startDate, "iOS"),
						usageDataDAO.getKeyboardCompleteCountByDate(startDate, "iOS"),
						usageDataDAO.getKeyboardSearchedCountByDate(startDate, "iOS"));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new KeyboardDetail(sDate, sPlatform, usageDataDAO.getKeyboardInitCountByDate(startDate, "All"),
						usageDataDAO.getKeyboardEnableCountByDate(startDate, "All"),
						usageDataDAO.getKeyboardCompleteCountByDate(startDate, "All"),
						usageDataDAO.getKeyboardSearchedCountByDate(startDate, "All"));
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}*/

	/*@CrossOrigin
	@RequestMapping(value = "/getNotificationViewReportByDate", method = RequestMethod.POST)
	public String getNotificationViewDataByDate(@RequestBody String request) {
		logger.info("[INFO] Get notification view report for " + request);
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<NotificationData> dataList = new ArrayList<NotificationData>();
		NotificationData pdAndroid = null;
		NotificationData pdiOS = null;
		NotificationData pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new NotificationData(sDate, sPlatform,
						usageDataDAO.getNotificationViewCountByDate(startDate, "Android"),
						repo.getAllNotificationSentCount(sDate, "Android"),
						repo.getNotificationSentCount(sDate, "App first open", "Android"),
						repo.getNotificationSentCount(sDate, "New Feature", "Android"),
						repo.getNotificationSentCount(sDate, "New Avatar Accessories", "Android"),
						repo.getNotificationSentCount(sDate, "AD-Update", "Android"),
						repo.getNotificationSentCount(sDate, "Create Avatar", "Android"),
						repo.getNotificationSentCount(sDate, "Avatar Completion Reminder", "Android"),
						repo.getNotificationSentCount(sDate, "App Rating/Review", "Android"),
						repo.getNotificationSentCount(sDate, "Sign Up", "Android"),
						repo.getNotificationSentCount(sDate, "Connect Keyboard", "Android"),
						repo.getNotificationSentCount(sDate, "Daily/Weekly Trending Stickers", "Android"),
						repo.getNotificationSentCount(sDate, "Unexplored Sticker Category", "Android"),
						repo.getNotificationSentCount(sDate, "Sticker pack", "Android"),
						repo.getNotificationSentCount(sDate, "Sticker of the day", "Android"),
						repo.getNotificationSentCount(sDate, "New Sticker based on region", "Android"),
						repo.getNotificationSentCount(sDate, "New App language", "Android"),
						repo.getNotificationSentCount(sDate, "New KB App language", "Android"),
						repo.getNotificationSentCount(sDate, "Region based notification", "Android"),
						repo.getNotificationSentCount(sDate, "Others", "Android"),
						repo.getNotificationSentCount(sDate, "Welcome Notification", "Android")
						);
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new NotificationData(sDate, sPlatform,
						usageDataDAO.getNotificationViewCountByDate(startDate, "iOS"),
						repo.getAllNotificationSentCount(sDate, "iOS"),
						repo.getNotificationSentCount(sDate, "App first open", "iOS"),
						repo.getNotificationSentCount(sDate, "New Feature", "iOS"),
						repo.getNotificationSentCount(sDate, "New Avatar Accessories", "iOS"),
						repo.getNotificationSentCount(sDate, "AD-Update", "iOS"),
						repo.getNotificationSentCount(sDate, "Create Avatar", "iOS"),
						repo.getNotificationSentCount(sDate, "Avatar Completion Reminder", "iOS"),
						repo.getNotificationSentCount(sDate, "App Rating/Review", "iOS"),
						repo.getNotificationSentCount(sDate, "Sign Up", "iOS"),
						repo.getNotificationSentCount(sDate, "Connect Keyboard", "iOS"),
						repo.getNotificationSentCount(sDate, "Daily/Weekly Trending Stickers", "iOS"),
						repo.getNotificationSentCount(sDate, "Unexplored Sticker Category", "iOS"),
						repo.getNotificationSentCount(sDate, "Sticker pack", "iOS"),
						repo.getNotificationSentCount(sDate, "Sticker of the day", "iOS"),
						repo.getNotificationSentCount(sDate, "New Sticker based on region", "iOS"),
						repo.getNotificationSentCount(sDate, "New App language", "iOS"),
						repo.getNotificationSentCount(sDate, "New KB App language", "iOS"),
						repo.getNotificationSentCount(sDate, "Region based notification", "iOS"),
						repo.getNotificationSentCount(sDate, "Others", "iOS"),
						repo.getNotificationSentCount(sDate, "Welcome Notification", "iOS"));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new NotificationData(sDate, sPlatform,
						usageDataDAO.getNotificationViewCountByDate(startDate, "All"),
						repo.getAllNotificationSentCount(sDate, "All"),
						repo.getNotificationSentCount(sDate, "App first open", "All"),
						repo.getNotificationSentCount(sDate, "New Feature", "All"),
						repo.getNotificationSentCount(sDate, "New Avatar Accessories", "All"),
						repo.getNotificationSentCount(sDate, "AD-Update", "All"),
						repo.getNotificationSentCount(sDate, "Create Avatar", "All"),
						repo.getNotificationSentCount(sDate, "Avatar Completion Reminder", "All"),
						repo.getNotificationSentCount(sDate, "App Rating/Review", "All"),
						repo.getNotificationSentCount(sDate, "Sign Up", "All"),
						repo.getNotificationSentCount(sDate, "Connect Keyboard", "All"),
						repo.getNotificationSentCount(sDate, "Daily/Weekly Trending Stickers", "All"),
						repo.getNotificationSentCount(sDate, "Unexplored Sticker Category", "All"),
						repo.getNotificationSentCount(sDate, "Sticker pack", "All"),
						repo.getNotificationSentCount(sDate, "Sticker of the day", "All"),
						repo.getNotificationSentCount(sDate, "New Sticker based on region", "All"),
						repo.getNotificationSentCount(sDate, "New App language", "All"),
						repo.getNotificationSentCount(sDate, "New KB App language", "All"),
						repo.getNotificationSentCount(sDate, "Region based notification", "All"),
						repo.getNotificationSentCount(sDate, "Others", "All"),
						repo.getNotificationSentCount(sDate, "Welcome Notification", "All"));
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}*/

	@CrossOrigin
	@RequestMapping(value = "/getsearchResultsReportByDate", method = RequestMethod.POST)
	public String getsearchResultDataByDate(@RequestBody String request) {
		logger.info("[INFO] Get notification view report for " + request);
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<SearchData> dataList = new ArrayList<SearchData>();
		SearchData pdAndroid = null;
		SearchData pdiOS = null;
		SearchData pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new SearchData(sDate, sPlatform,
						usageDataDAO.getUniqueSearchCountByDate(startDate, "Android"),
						usageDataDAO.getUniqueSearchViewCountByDate(startDate, "Android"),
						usageDataDAO.getUniqueSearchSharedCountByDate(startDate, "Android"));
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new SearchData(sDate, sPlatform, usageDataDAO.getUniqueSearchCountByDate(startDate, "iOS"),
						usageDataDAO.getUniqueSearchViewCountByDate(startDate, "iOS"),
						usageDataDAO.getUniqueSearchSharedCountByDate(startDate, "iOS"));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new SearchData(sDate, sPlatform, usageDataDAO.getUniqueSearchCountByDate(startDate, "All"),
						usageDataDAO.getUniqueSearchViewCountByDate(startDate, "All"),
						usageDataDAO.getUniqueSearchSharedCountByDate(startDate, "All"));
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}

	/*@CrossOrigin
	@RequestMapping(value = "/getShareTargetReportByDate", method = RequestMethod.POST)
	public String getTargetApplikcationDataByDate(@RequestBody String request) {
		logger.info("[INFO] Get share target report");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		ArrayList<ShareTargetData> dataList = new ArrayList<ShareTargetData>();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		Date endDate;
		List<String> list = null;
		try {
			list = usageDataDAO.getAllTargetAppBydate(formatter.parse(sDate), formatter.parse(eDate));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String backupStartDate = sDate;
		String backupEndDate = eDate;

		if (list == null) {
			logger.error("[ERROR] No Content Found");
			return null;
		}

		for (String targetApp : list) {
			if (targetApp == null || targetApp.equals("")) {
				continue;
			}

			// ////System.out.println("SID : " + stickerId);
			ShareTargetData pdAndroid = null;
			ShareTargetData pdiOS = null;
			ShareTargetData pdDev = null;
			sDate = backupStartDate;
			eDate = backupEndDate;
			try {
				startDate = formatter.parse(backupStartDate);
			} catch (ParseException e1) {
				logger.error("[ERROR] DATE not parsable");
				e1.printStackTrace();
				return gson.toJson("Invalid Date");
			}
			try {
				endDate = formatter.parse(backupEndDate);
			} catch (ParseException e1) {
				e1.printStackTrace();
				logger.error("[ERROR] DATE not parsable");
				return gson.toJson("Invalid Date");
			}
			if (sPlatform.equals("Android")) {
				while (true) {
					if (startDate.compareTo(endDate) > 0) {
						break;
					}

					long Count = usageDataDAO.getTargetAppShareCount(targetApp, startDate, "Android");
					pdAndroid = new ShareTargetData(sDate, "Android", targetApp, Count);

					if (Count != 0) {
						dataList.add(pdAndroid);
					}
					sDate = getNextDate(sDate);
					startDate = getNextDateObject(startDate);
				}
			} else if (sPlatform.equals("iOS")) {
				while (true) {
					if (startDate.compareTo(endDate) > 0) {
						break;
					}
					long Count = usageDataDAO.getTargetAppShareCount(targetApp, startDate, "iOS");
					pdiOS = new ShareTargetData(sDate, "iOS", targetApp, Count);

					if (Count != 0) {
						dataList.add(pdiOS);
					}
					sDate = getNextDate(sDate);
					startDate = getNextDateObject(startDate);
				}
			} else if (sPlatform.toUpperCase().equals("ALL")) {
				while (true) {
					if (startDate.compareTo(endDate) > 0) {
						break;
					}
					long Count = usageDataDAO.getTargetAppShareCount(targetApp, startDate, "All");
					pdDev = new ShareTargetData(sDate, "All", targetApp, Count);

					if (Count != 0) {
						dataList.add(pdDev);
					}
					sDate = getNextDate(sDate);
					startDate = getNextDateObject(startDate);
				}
			} else {
				logger.error("[ERROR] Invalid Platform");
				return gson.toJson("{\"message\":\"Invalid Platform\"}");
			}
		}

		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}*/

/*	@CrossOrigin
	@RequestMapping(value = "/getLanguageReportByDate", method = RequestMethod.POST)
	public String getLanguageDataByDate(@RequestBody String request) {
		logger.info("[INFO] Get language report for " + request);
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<LanguageData> dataList = new ArrayList<LanguageData>();
		LanguageData pdAndroid = null;
		LanguageData pdiOS = null;
		LanguageData pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new LanguageData(sDate, sPlatform,
						usageDataDAO.getLanguageCount("Hindi", startDate, "Android"),
						usageDataDAO.getLanguageCount("English", startDate, "Android"));
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new LanguageData(sDate, sPlatform, usageDataDAO.getLanguageCount("Hindi", startDate, "iOS"),
						usageDataDAO.getLanguageCount("English", startDate, "iOS"));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new LanguageData(sDate, sPlatform, usageDataDAO.getLanguageCount("Hindi", startDate, "All"),
						usageDataDAO.getLanguageCount("English", startDate, "All"));
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}*/

	@CrossOrigin
	@RequestMapping(value = "/getAppIdWiseTotalReportByDate", method = RequestMethod.POST)
	public String getAppIdWiseReportByDate(@RequestBody String request) {
		logger.info("[INFO] Get AppID report for " + request);
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		List<String> dataList = new ArrayList<String>();

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				List<String> appIdList = usageDataDAO.findUniqueAppIdFirstOpenByDate("Android", startDate);
				if (appIdList == null || appIdList.size() == 0) {
					sDate = getNextDate(sDate);
					startDate = getNextDateObject(startDate);
					continue;
				}

				for (String appId : appIdList) {
					StringBuilder sb = new StringBuilder();
					sb.append(appId);
					sb.append("|");

					List<String> locList = usageDataDAO.findAllEventsForAppIdByDate(appId, startDate);
					for (int i = 0; i < locList.size(); i++) {
						sb.append(locList.get(i));
						if (i < locList.size() - 1) {
							sb.append(",");
						}
					}
					sb.append("\r\n");
					dataList.add(sb.toString());
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				List<String> appIdList = usageDataDAO.findUniqueAppIdFirstOpenByDate("iOS", startDate);
				if (appIdList == null || appIdList.size() == 0) {
					sDate = getNextDate(sDate);
					startDate = getNextDateObject(startDate);
					continue;
				}
				for (String appId : appIdList) {
					StringBuilder sb = new StringBuilder();
					sb.append(appId);
					sb.append("|");

					List<String> locList = usageDataDAO.findAllEventsForAppIdByDate(appId, startDate);
					for (int i = 0; i < locList.size(); i++) {
						sb.append(locList.get(i));
						if (i < locList.size() - 1) {
							sb.append(",");
						}
					}
					sb.append("\r\n");
					dataList.add(sb.toString());
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {

					break;
				}
				List<String> appIdList = usageDataDAO.findUniqueAppIdFirstOpenByDate("All", startDate);
				if (appIdList == null || appIdList.size() == 0) {
					sDate = getNextDate(sDate);
					startDate = getNextDateObject(startDate);
					continue;
				}
				// //System.out.println("DATE "+sDate);
				for (String appId : appIdList) {
					StringBuilder sb = new StringBuilder();
					sb.append(appId);
					sb.append("|");

					List<String> locList = usageDataDAO.findAllEventsForAppIdByDate(appId, startDate);
					for (int i = 0; i < locList.size(); i++) {
						sb.append(locList.get(i));
						if (i < locList.size() - 1) {
							sb.append(",");
						}
					}
					sb.append("\r\n");
					dataList.add(sb.toString());
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[INFO] DATA :" + gson.toJson(dataList));
		return gson.toJson(dataList);
	}

	// ############################# DUMP REQUEST
	// ####################################
	@CrossOrigin
	@RequestMapping(value = "/getUserActivityReportByDate", method = RequestMethod.POST)
	public String getUserActivityReportByDate(@RequestBody String request) {
		logger.info("[INFO] Get avatar report by date ");

		String sDate = null;
		Gson gson = new Gson();
		try {
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate = null;
		try {
			startDate = sdf.parse(sDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (startDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		// GET ALL APP IDs
		List<String> list = null;
		list = usageDataDAO.findUniqueAppIdEachLaunchByDate(startDate);// repo.findAllUsers(sDate);

		List<UserActivityHelper> uaList = new ArrayList<UserActivityHelper>();
		int Count = list.size();
		for (String s : list) {
			logger.error("[INFO] Looping for AppID : " + s + "  of " + Count);
			// Loop for all events
			ArrayList<String> activityList = new ArrayList<String>();
			// for(int i=0;i<Declaration.getMaxEventCode();i++) {
			Map<String, String> locActivityMap = new LinkedHashMap<String, String>();
			// activityList.add(usageDataDAO.getUserActivityByEventAndDate(s,(i+1)+"",startDate));
			locActivityMap = usageDataDAO.findAllEventDataForAppIdByDate(s, startDate);

			if (locActivityMap == null) {
				continue;
			}
			//// //System.out.println("DATA >>>>>>
			//// "+((locActivityMap.get("keyboardSearched_1")!=null)?locActivityMap.get("keyboardSearched_1"):""));
			activityList.add(
					locActivityMap.get("keyboardSearched_1") != null ? locActivityMap.get("keyboardSearched_1") : "");
			activityList.add(
					locActivityMap.get("keyboardSessionStart_2") != null ? locActivityMap.get("keyboardSessionStart_2")
							: "");
			activityList.add(
					locActivityMap.get("keyboardTextpad_3") != null ? locActivityMap.get("keyboardTextpad_3") : "");
			activityList.add(locActivityMap.get("keyboardSearchresultsStickerselected_4") != null
					? ("keyboardSearchresultsStickerselected_4")
					: "");
			activityList.add(locActivityMap.get("keyboardDefaultStickerselected_5") != null
					? locActivityMap.get("keyboardDefaultStickerselected_5")
					: "");
			activityList.add(locActivityMap.get("standaloneStickerStickerviewed_6") != null
					? locActivityMap.get("standaloneStickerStickerviewed_6")
					: "");
			activityList.add(locActivityMap.get("standaloneStickerStickershared_7") != null
					? locActivityMap.get("standaloneStickerStickershared_7")
					: "");
			activityList.add(locActivityMap.get("createUser_8") != null ? locActivityMap.get("createUser_8") : "");
			activityList.add(locActivityMap.get("firstOpen_9") != null ? locActivityMap.get("firstOpen_9") : "");
			activityList.add(locActivityMap.get("appUpdate_10") != null ? locActivityMap.get("appUpdate_10") : "");
			activityList
					.add(locActivityMap.get("appEachLaunch_11") != null ? locActivityMap.get("appEachLaunch_11") : "");
			activityList.add(
					locActivityMap.get("appSessionStart_12") != null ? locActivityMap.get("appSessionStart_12") : "");
			activityList
					.add(locActivityMap.get("appSessionEnd_13") != null ? locActivityMap.get("appSessionEnd_13") : "");
			activityList.add(
					locActivityMap.get("onboardingCompleted_14") != null ? locActivityMap.get("onboardingCompleted_14")
							: "");
			activityList.add(locActivityMap.get("connectKeyboardInitiated_15") != null
					? locActivityMap.get("connectKeyboardInitiated_15")
					: "");
			activityList.add(
					locActivityMap.get("enabledKeyboard_16") != null ? locActivityMap.get("enabledKeyboard_16") : "");
			activityList.add(locActivityMap.get("connectKeyboardCompleted_17") != null
					? locActivityMap.get("connectKeyboardCompleted_17")
					: "");
			activityList.add(locActivityMap.get("keyboardTakeMeThereClicked_18") != null
					? locActivityMap.get("keyboardTakeMeThereClicked_18")
					: "");
			activityList
					.add(locActivityMap.get("signupClicked_19") != null ? locActivityMap.get("signupClicked_19") : "");
			activityList.add(locActivityMap.get("signupFacebookInitiated_20") != null
					? locActivityMap.get("signupFacebookInitiated_20")
					: "");
			activityList.add(locActivityMap.get("signupFacebookCompleted_21") != null
					? locActivityMap.get("signupFacebookCompleted_21")
					: "");
			activityList.add(locActivityMap.get("signupMobileInitiated_22") != null
					? locActivityMap.get("signupMobileInitiated_22")
					: "");
			activityList.add(locActivityMap.get("signupMobileCompleted_23") != null
					? locActivityMap.get("signupMobileCompleted_23")
					: "");
			activityList.add(locActivityMap.get("signupMobileResetOtp_24") != null
					? locActivityMap.get("signupMobileResetOtp_24")
					: "");
			activityList.add(locActivityMap.get("signupMobileExceededOtpAttempt_25") != null
					? locActivityMap.get("signupMobileExceededOtpAttempt_25")
					: "");
			activityList
					.add(locActivityMap.get("signupProfile_26") != null ? locActivityMap.get("signupProfile_26") : "");
			activityList
					.add(locActivityMap.get("loginClicked_27") != null ? locActivityMap.get("loginClicked_27") : "");
			activityList.add(locActivityMap.get("loginFacebookInitiated_28") != null
					? locActivityMap.get("loginFacebookInitiated_28")
					: "");
			activityList.add(locActivityMap.get("loginFacebookCompleted_29") != null
					? locActivityMap.get("loginFacebookCompleted_29")
					: "");
			activityList.add(locActivityMap.get("loginMobileInitiated_30") != null
					? locActivityMap.get("loginMobileInitiated_30")
					: "");
			activityList.add(locActivityMap.get("loginMobileCompleted_31") != null
					? locActivityMap.get("loginMobileCompleted_31")
					: "");
			activityList.add(
					locActivityMap.get("loginMobileResetOtp_32") != null ? locActivityMap.get("loginMobileResetOtp_32")
							: "");
			activityList.add(locActivityMap.get("loginMobileExceededOtpAttempt_33") != null
					? locActivityMap.get("loginMobileExceededOtpAttempt_33")
					: "");
			activityList
					.add(locActivityMap.get("legalClicked_34") != null ? locActivityMap.get("legalClicked_34") : "");
			activityList.add(locActivityMap.get("newsClicked_35") != null ? locActivityMap.get("newsClicked_35") : "");
			activityList
					.add(locActivityMap.get("promotionsClicked_36") != null ? locActivityMap.get("promotionsClicked_36")
							: "");
			activityList.add(locActivityMap.get("recommendedStickersViewed_37") != null
					? locActivityMap.get("recommendedStickersViewed_37")
					: "");
			activityList.add(locActivityMap.get("recommendedStickersShared_38") != null
					? locActivityMap.get("recommendedStickersShared_38")
					: "");
			activityList.add(
					locActivityMap.get("reminderClicked_39") != null ? locActivityMap.get("reminderClicked_39") : "");
			activityList.add(
					locActivityMap.get("announcementClicked_40") != null ? locActivityMap.get("announcementClicked_40")
							: "");
			activityList.add(locActivityMap.get("avatarReset_41") != null ? locActivityMap.get("avatarReset_41") : "");
			activityList.add(locActivityMap.get("createAvatarInitiated_42") != null
					? locActivityMap.get("createAvatarInitiated_42")
					: "");
			activityList.add(locActivityMap.get("createAvatarCompleted_43") != null
					? locActivityMap.get("createAvatarCompleted_43")
					: "");
			activityList.add(locActivityMap.get("avatarGenderSelected_44") != null
					? locActivityMap.get("avatarGenderSelected_44")
					: "");
			activityList.add(locActivityMap.get("avatarFaceShapeSelected_45") != null
					? locActivityMap.get("avatarFaceShapeSelected_45")
					: "");
			activityList.add(locActivityMap.get("avatarFaceShapeViewed_46") != null
					? locActivityMap.get("avatarFaceShapeViewed_46")
					: "");
			activityList.add(locActivityMap.get("avatarSkintoneSelected_47") != null
					? locActivityMap.get("avatarSkintoneSelected_47")
					: "");
			activityList.add(locActivityMap.get("avatarSkintoneViewed_48") != null
					? locActivityMap.get("avatarSkintoneViewed_48")
					: "");
			activityList.add(locActivityMap.get("avatarHairstyleSelected_49") != null
					? locActivityMap.get("avatarHairstyleSelected_49")
					: "");
			activityList.add(locActivityMap.get("avatarHairstyleViewed_50") != null
					? locActivityMap.get("avatarHairstyleViewed_50")
					: "");
			activityList.add(locActivityMap.get("avatarHaircolourSelected_51") != null
					? locActivityMap.get("avatarHaircolourSelected_51")
					: "");
			activityList.add(locActivityMap.get("avatarHaircolourViewed_52") != null
					? locActivityMap.get("avatarHaircolourViewed_52")
					: "");
			activityList.add(locActivityMap.get("avatarFacialHairSelected_53") != null
					? locActivityMap.get("avatarFacialHairSelected_53")
					: "");
			activityList.add(locActivityMap.get("avatarFacialHairViewed_54") != null
					? locActivityMap.get("avatarFacialHairViewed_54")
					: "");
			activityList.add(locActivityMap.get("avatarFacialHaircolourSelected_55") != null
					? locActivityMap.get("avatarFacialHaircolourSelected_55")
					: "");
			activityList.add(locActivityMap.get("avatarFacialHaircolourViewed_56") != null
					? locActivityMap.get("avatarFacialHaircolourViewed_56")
					: "");
			activityList.add(locActivityMap.get("avatarEyeShapeSelected_57") != null
					? locActivityMap.get("avatarEyeShapeSelected_57")
					: "");
			activityList.add(locActivityMap.get("avatarEyeShapeViewed_58") != null
					? locActivityMap.get("avatarEyeShapeViewed_58")
					: "");
			activityList.add(locActivityMap.get("avatarEyeColourSelected_59") != null
					? locActivityMap.get("avatarEyeColourSelected_59")
					: "");
			activityList.add(locActivityMap.get("avatarEyeColourViewed_60") != null
					? locActivityMap.get("avatarEyeColourViewed_60")
					: "");
			activityList.add(locActivityMap.get("avatarEyebrowSelected_61") != null
					? locActivityMap.get("avatarEyebrowSelected_61")
					: "");
			activityList.add(
					locActivityMap.get("avatarEyebrowViewed_62") != null ? locActivityMap.get("avatarEyebrowViewed_62")
							: "");
			activityList.add(
					locActivityMap.get("avatarNoseSelected_63") != null ? locActivityMap.get("avatarNoseSelected_63")
							: "");
			activityList.add(
					locActivityMap.get("avatarNoseViewed_64") != null ? locActivityMap.get("avatarNoseViewed_64") : "");
			activityList.add(
					locActivityMap.get("avatarMouthSelected_65") != null ? locActivityMap.get("avatarMouthSelected_65")
							: "");
			activityList
					.add(locActivityMap.get("avatarMouthViewed_66") != null ? locActivityMap.get("avatarMouthViewed_66")
							: "");
			activityList.add(locActivityMap.get("avatarLipstickSelected_67") != null
					? locActivityMap.get("avatarLipstickSelected_67")
					: "");
			activityList.add(locActivityMap.get("avatarLipstickViewed_68") != null
					? locActivityMap.get("avatarLipstickViewed_68")
					: "");
			activityList.add(locActivityMap.get("avatarBodytypeSelected_69") != null
					? locActivityMap.get("avatarBodytypeSelected_69")
					: "");
			activityList.add(locActivityMap.get("avatarBodytypeViewed_70") != null
					? locActivityMap.get("avatarBodytypeViewed_70")
					: "");
			activityList.add(locActivityMap.get("avatarOutfitSelected_71") != null
					? locActivityMap.get("avatarOutfitSelected_71")
					: "");
			activityList.add(
					locActivityMap.get("avatarOutfitViewed_72") != null ? locActivityMap.get("avatarOutfitViewed_72")
							: "");
			activityList.add(locActivityMap.get("searchSuggestedKeywordSelected_73") != null
					? locActivityMap.get("searchSuggestedKeywordSelected_73")
					: "");
			activityList.add(locActivityMap.get("searchTopCategoriesSelected_74") != null
					? locActivityMap.get("searchTopCategoriesSelected_74")
					: "");
			activityList.add(
					locActivityMap.get("standaloneSearched_75") != null ? locActivityMap.get("standaloneSearched_75")
							: "");
			activityList.add(locActivityMap.get("standaloneSearchresultsStickerShared_76") != null
					? locActivityMap.get("standaloneSearchresultsStickerShared_76")
					: "");
			activityList.add(locActivityMap.get("standaloneSearchresultsStickerViewed_77") != null
					? locActivityMap.get("standaloneSearchresultsStickerViewed_77")
					: "");
			activityList.add(locActivityMap.get("settingLocationAccess_78") != null
					? locActivityMap.get("settingLocationAccess_78")
					: "");
			activityList.add(
					locActivityMap.get("settingMediaAccess_79") != null ? locActivityMap.get("settingMediaAccess_79")
							: "");
			activityList.add(locActivityMap.get("settingsNotifications_80") != null
					? locActivityMap.get("settingsNotifications_80")
					: "");
			activityList.add(locActivityMap.get("settingsNotificationFrequency_81") != null
					? locActivityMap.get("settingsNotificationFrequency_81")
					: "");
			activityList.add(locActivityMap.get("settingsLanguagePreferenceSelected_82") != null
					? locActivityMap.get("settingsLanguagePreferenceSelected_82")
					: "");
			activityList.add(locActivityMap.get("settingsInviteFriends_83") != null
					? locActivityMap.get("settingsInviteFriends_83")
					: "");
			activityList.add(locActivityMap.get("settingsRateInPlaystoreInitiated_84") != null
					? locActivityMap.get("settingsRateInPlaystoreInitiated_84")
					: "");
			activityList.add(locActivityMap.get("pushnotificationOpened_85") != null
					? locActivityMap.get("pushnotificationOpened_85")
					: "");
			activityList.add(
					locActivityMap.get("keyboardSessionEnd_86") != null ? locActivityMap.get("keyboardSessionEnd_86")
							: "");
			activityList.add(
					locActivityMap.get("settingsLogout_87") != null ? locActivityMap.get("settingsLogout_87") : "");
			activityList.add(
					locActivityMap.get("keyboardDisable_88") != null ? locActivityMap.get("keyboardDisable_88") : "");
			activityList.add(locActivityMap.get("keyboardStickerShared_89") != null
					? locActivityMap.get("keyboardStickerShared_89")
					: "");
			activityList.add(
					locActivityMap.get("avatarEarViewed_90") != null ? locActivityMap.get("avatarEarViewed_90") : "");
			activityList
					.add(locActivityMap.get("avatarEarSelected_91") != null ? locActivityMap.get("avatarEarSelected_91")
							: "");
			activityList.add(locActivityMap.get("standaloneStickerLiked_92") != null
					? locActivityMap.get("standaloneStickerLiked_92")
					: "");
			activityList.add(locActivityMap.get("standaloneStickerUnliked_93") != null
					? locActivityMap.get("standaloneStickerUnliked_93")
					: "");
			activityList.add(locActivityMap.get("standaloneStickerDownloaded_94") != null
					? locActivityMap.get("standaloneStickerDownloaded_94")
					: "");
			activityList.add(locActivityMap.get("recommendedStickerDownloaded_95") != null
					? locActivityMap.get("recommendedStickerDownloaded_95")
					: "");
			activityList.add(locActivityMap.get("standaloneSearchedresultsStickerDownloaded_96") != null
					? locActivityMap.get("standaloneSearchedresultsStickerDownloaded_96")
					: "");
			activityList.add(locActivityMap.get("standaloneOnboardingScreen1Viewed_97") != null
					? locActivityMap.get("standaloneOnboardingScreen1Viewed_97")
					: "");
			activityList.add(locActivityMap.get("standaloneOnboardingScreen2Viewed_98") != null
					? locActivityMap.get("standaloneOnboardingScreen2Viewed_98")
					: "");
			activityList.add(locActivityMap.get("standaloneOnboardingScreen3Viewed_99") != null
					? locActivityMap.get("standaloneOnboardingScreen3Viewed_99")
					: "");
			activityList.add(locActivityMap.get("standaloneOnboardingScreen4Viewed_100") != null
					? locActivityMap.get("standaloneOnboardingScreen4Viewed_100")
					: "");
			activityList.add(locActivityMap.get("promotionsStickerViewed_101") != null
					? locActivityMap.get("promotionsStickerViewed_101")
					: "");
			activityList.add(locActivityMap.get("promotionsStickerShared_102") != null
					? locActivityMap.get("promotionsStickerShared_102")
					: "");
			activityList.add(locActivityMap.get("standaloneAcceptPermissionInitiated_103") != null
					? locActivityMap.get("standaloneAcceptPermissionInitiated_103")
					: "");
			activityList.add(locActivityMap.get("promotionsStickerDownloaded_104") != null
					? locActivityMap.get("promotionsStickerDownloaded_104")
					: "");
			activityList.add(locActivityMap.get("promotionsStickerLiked_105") != null
					? locActivityMap.get("promotionsStickerLiked_105")
					: "");
			activityList.add(locActivityMap.get("promotionsStickerShared_106") != null
					? locActivityMap.get("promotionsStickerShared_106")
					: "");
			activityList.add(locActivityMap.get("notificationStickerShared_107") != null
					? locActivityMap.get("notificationStickerShared_107")
					: "");
			activityList.add(locActivityMap.get("notificationStickerViewed_108") != null
					? locActivityMap.get("notificationStickerViewed_108")
					: "");
			activityList.add(locActivityMap.get("notificationStickerDownloaded_109") != null
					? locActivityMap.get("notificationStickerDownloaded_109")
					: "");
			activityList.add(locActivityMap.get("notificationStickerLiked_110") != null
					? locActivityMap.get("notificationStickerLiked_110")
					: "");
			activityList.add(locActivityMap.get("recommendationStickerLiked_111") != null
					? locActivityMap.get("recommendationStickerLiked_111")
					: "");
			activityList.add(locActivityMap.get("recommendationStickerUnliked_112") != null
					? locActivityMap.get("recommendationStickerUnliked_112")
					: "");
			activityList.add(locActivityMap.get("promotionStickerUnLiked_113") != null
					? locActivityMap.get("promotionStickerUnLiked_113")
					: "");
			activityList.add(locActivityMap.get("notificationStickerUnLiked_114") != null
					? locActivityMap.get("notificationStickerUnLiked_114")
					: "");
			activityList.add(locActivityMap.get("recommendationStickerSelected_115") != null
					? locActivityMap.get("recommendationStickerSelected_115")
					: "");
			activityList.add(locActivityMap.get("standaloneSearchedresultsStickerLiked_116") != null
					? locActivityMap.get("standaloneSearchedresultsStickerLiked_116")
					: "");
			activityList.add(locActivityMap.get("standaloneSearchedresultsStickerUnLiked_117") != null
					? locActivityMap.get("standaloneSearchedresultsStickerUnLiked_117")
					: "");
			activityList.add(locActivityMap.get("homescreenNewFeatureItemClicked_118") != null
					? locActivityMap.get("homescreenNewFeatureItemClicked_118")
					: "");
			activityList
					.add(locActivityMap.get("homeScreenViewed_119") != null ? locActivityMap.get("homeScreenViewed_119")
							: "");
			activityList.add(
					locActivityMap.get("avatarScreenViewed_120") != null ? locActivityMap.get("avatarScreenViewed_120")
							: "");
			activityList.add(locActivityMap.get("stickerScreenViewed_121") != null
					? locActivityMap.get("stickerScreenViewed_121")
					: "");
			activityList.add(locActivityMap.get("settingScreenViewed_122") != null
					? locActivityMap.get("settingScreenViewed_122")
					: "");
			activityList.add(
					locActivityMap.get("searchScreenViewed_123") != null ? locActivityMap.get("searchScreenViewed_123")
							: "");
			// }
			uaList.add(new UserActivityHelper(s, activityList));
		}

		logger.info("[INFO] DATA :" + gson.toJson(uaList));
		return gson.toJson(uaList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getFirstOpenCountReport", method = RequestMethod.POST)
	public RestResponse getFirstOpenCountReport(@RequestBody RequestData requestData) {
		RestResponse response = new RestResponse();
		if (requestData == null || requestData.getStartDate() == null || requestData.getEndDate() == null) {
			return getFailureResponse(response, "Invalid request.");
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			sdf.parse(sdf.format(requestData.getStartDate()));
		} catch (ParseException e) {
			return getFailureResponse(response, "Invalid date format for startDate: " + requestData.getStartDate()
					+ ". Date format should be 'yyyy-MM-dd'");
		}

		try {
			sdf.parse(sdf.format(requestData.getEndDate()));
		} catch (ParseException e) {
			return getFailureResponse(response, "Invalid date format for endDate: " + requestData.getStartDate()
					+ ". Date format should be 'yyyy-MM-dd'");
		}
		DeviceFirstOpenResultData firstOpenCount = repo.getFirstOpenCountByDateRange(
				sdf.format(requestData.getStartDate()), sdf.format(requestData.getEndDate()));
		response.setData(firstOpenCount);
		return getSuccessResponse(response, "Device first open count retrieved successfully.");
	}

	@CrossOrigin
	@RequestMapping(value = "/getOpenCountWithoutDeviceNameReport", method = RequestMethod.POST)
	public RestResponse getOpenCountWithoutDeviceNameByDateRange(@RequestBody RequestData requestData) {
		RestResponse response = new RestResponse();
		if (requestData == null || requestData.getStartDate() == null || requestData.getEndDate() == null) {
			return getFailureResponse(response, "Invalid request.");
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			sdf.parse(sdf.format(requestData.getStartDate()));
		} catch (ParseException e) {
			return getFailureResponse(response, "Invalid date format for startDate: " + requestData.getStartDate()
					+ ". Date format should be 'yyyy-MM-dd'");
		}

		try {
			sdf.parse(sdf.format(requestData.getEndDate()));
		} catch (ParseException e) {
			return getFailureResponse(response, "Invalid date format for endDate: " + requestData.getStartDate()
					+ ". Date format should be 'yyyy-MM-dd'");
		}
		DeviceFirstOpenCountData firstOpenCount = repo.getOpenCountWithoutDeviceNameByDateRange(
				sdf.format(requestData.getStartDate()), sdf.format(requestData.getEndDate()));
		response.setData(firstOpenCount);
		return getSuccessResponse(response, "Device first open count retrieved successfully.");
	}

	private RestResponse getFailureResponse(RestResponse response, String statusMessage) {
		logger.info("getFailureResponse()==>Entered");
		response.setStatus("FAILURE");
		response.setStatusMessage(statusMessage);
		logger.info("getFailureResponse()==>Exit");
		return response;
	}

	private RestResponse getSuccessResponse(RestResponse response, String statusMessage) {
		logger.info("getSuccessResponse()==>Entered");
		response.setStatus("SUCCESS");
		response.setStatusMessage(statusMessage);
		logger.info("getSuccessResponse()==>Exit");
		return response;
	}

}
