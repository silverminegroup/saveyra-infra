package com.saveyra.analytics.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.saveyra.analytics.data.RequestData;
import com.saveyra.analytics.data.RestResponse;
import com.saveyra.analytics.model.AuditTrail;
import com.saveyra.analytics.repo.MysqlRepo;

@RestController
@RequestMapping("/")
public class AuditTrailController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AuditTrailController.class);
	
	@Autowired
	private MysqlRepo auditTrailRepository;

	public enum Platform {
		ANDROID("Android"), IOS("iOS"), ALL("All");
		private String platform;

		Platform(String platform) {
			this.platform = platform;
		}

		/**
		 * @return the platform
		 */
		public String getPlatform() {
			return platform;
		}
	}

	@CrossOrigin
	@PostMapping("/notification/audittrail")
	public RestResponse getAllAuditTrail(@RequestBody RequestData requestData) throws ParseException {
		LOGGER.info("getAllAuditTrail()==>GET /notification/audittrail call is initiated.");
		RestResponse response = new RestResponse();
		if (requestData == null) {
			return getFailureResponse(response, "Invalid request.");
		}
		LOGGER.info("getAllMetrics()==>startDate: {}, endDate: {}, platform: {}", requestData.getStartDate(),
				requestData.getEndDate(), requestData.getPlatform());

		if (requestData.getStartDate() == null) {
			return getFailureResponse(response, "Invalid startDate.");
		}

		if (requestData.getEndDate() == null) {
			return getFailureResponse(response, "Invalid endDate.");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			sdf.parse(sdf.format(requestData.getStartDate()));
		} catch (ParseException e) {
			return getFailureResponse(response, "Invalid date format for startDate: "+requestData.getStartDate()+". Date format should be 'yyyy-MM-dd'");
		}
		
		try {
			sdf.parse(sdf.format(requestData.getEndDate()));
		} catch (ParseException e) {
			return getFailureResponse(response, "Invalid date format for endDate: "+requestData.getStartDate()+". Date format should be 'yyyy-MM-dd'");
		}
		
		if (requestData.getPlatform() == null || !Arrays.asList(Platform.values())
				.contains(Platform.valueOf(requestData.getPlatform().toUpperCase()))) {
			return getFailureResponse(response, "Invalid platform.");
		}

		List<AuditTrail> auditTrails = auditTrailRepository.getAllAuditTrailsByDateRangeForPlatform(sdf.format(requestData.getStartDate()), sdf.format(requestData.getEndDate()), requestData.getPlatform());
		response.setData(auditTrails);
		LOGGER.info("getAllAuditTrail()==>GET /notification/audittrail  call is end.");
		return getSuccessResponse(response, "Audit Trail retrieved successfully.");
	}

	private RestResponse getFailureResponse(RestResponse response, String statusMessage) {
		LOGGER.info("getFailureResponse()==>Entered");
		response.setStatus("FAILURE");
		response.setStatusMessage(statusMessage);
		LOGGER.info("getFailureResponse()==>Exit");
		return response;
	}

	private RestResponse getSuccessResponse(RestResponse response, String statusMessage) {
		LOGGER.info("getSuccessResponse()==>Entered");
		response.setStatus("SUCCESS");
		response.setStatusMessage(statusMessage);
		LOGGER.info("getSuccessResponse()==>Exit");
		return response;
	}
}
