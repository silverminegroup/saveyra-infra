package com.saveyra.analytics.controller;

import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.saveyra.analytics.DAO.UsageDataDAO;
import com.saveyra.analytics.UserActivityHelper.ActiveUserData;
import com.saveyra.analytics.UserActivityHelper.ActiveUserDataDetail;
import com.saveyra.analytics.UserActivityHelper.UserActivityHourBasis;
import com.saveyra.analytics.avatarHelper.AvatarCreateUserType;
import com.saveyra.analytics.avatarHelper.AvatarDetailUserType;
import com.saveyra.analytics.avatarHelper.AvatarDetails;
import com.saveyra.analytics.avatarHelper.AvatarFeatureHelper;
import com.saveyra.analytics.avatarHelper.ExtendedAvatarHelper;
import com.saveyra.analytics.avatarHelper.GenderReport;
import com.saveyra.analytics.categoryHelper.CategoryData;
import com.saveyra.analytics.consolidatedReport.ReturnObject;
import com.saveyra.analytics.data.RestResponse;
import com.saveyra.analytics.imageHelper.ImageData;
import com.saveyra.analytics.keyboardHelper.KeyboardDetail;
import com.saveyra.analytics.keyboardHelper.UserTypeKeyBoard;
import com.saveyra.analytics.keywordHelper.KeywordSearch;
import com.saveyra.analytics.languageHelper.LanguageData;
import com.saveyra.analytics.model.AppIdIPModel;
import com.saveyra.analytics.model.HomeScreenPermissionReport;
import com.saveyra.analytics.model.HomeScreenReminderReport;
import com.saveyra.analytics.model.HomeScreenViewReport;
import com.saveyra.analytics.notificationHelper.ContentNotification;
import com.saveyra.analytics.notificationHelper.NotificationData;
import com.saveyra.analytics.notificationHelper.ReducedNotificationData;
import com.saveyra.analytics.onboardingHelper.OnboardingDataComplete;
import com.saveyra.analytics.repo.MysqlRepo;
import com.saveyra.analytics.screenviewHelper.ScreenViewData;
import com.saveyra.analytics.signUpHelper.PlatformDataByDate;
import com.saveyra.analytics.signupmobileHelper.SignUpFBData;
import com.saveyra.analytics.signupmobileHelper.SignUpMobileData;
import com.saveyra.analytics.stickerShareHelper.StickerShareData;
import com.saveyra.analytics.stickerShareHelper.StickerShareUserData;
import com.saveyra.analytics.targetApp.ShareTargetData;
import com.saveyra.analytics.userTypeHelper.AppFirstOpenData;
import com.saveyra.analytics.userTypeHelper.UserTypeExtended;

@RestController
@RequestMapping("/")
@EnableAutoConfiguration
public class ReportController {

	@Autowired
	UsageDataDAO usageDataDAO;

	@Autowired
	MysqlRepo repo;

	private static final Logger logger = LoggerFactory.getLogger(ReportController.class);
		
	//>>>>>>>>>>>>>>
	
	@CrossOrigin
	@RequestMapping(value = "/getAppIdIPReport", method = RequestMethod.POST)
	public String getActiveUsersAppIDReport(@RequestBody String request) {
		logger.info("[Enter] getAppIdIPReport");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		ArrayList<AppIdIPModel> dataList = new ArrayList<AppIdIPModel>();

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}	
				
				ArrayList<String> totalAppId = usageDataDAO.getTotalActiveAppIDByDate(sPlatform, startDate);
				
				for(String s:totalAppId) {
					String sIP = repo.findIP(s);
					
					dataList.add(new AppIdIPModel(sDate,sPlatform,s,sIP==null?"":sIP));							
				}				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}	
				
				ArrayList<String> totalAppId = usageDataDAO.getTotalActiveAppIDByDate(sPlatform, startDate);
				
				for(String s:totalAppId) {
					String sIP = repo.findIP(s);
					
					dataList.add(new AppIdIPModel(sDate,sPlatform,s,sIP==null?"":sIP));							
				}				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}	
				
				ArrayList<String> totalAppId = usageDataDAO.getTotalActiveAppIDByDate(sPlatform, startDate);
				
				for(String s:totalAppId) {
					String sIP = repo.findIP(s);
					
					dataList.add(new AppIdIPModel(sDate,sPlatform,s,sIP==null?"":sIP));							
				}				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getAppIdIPReport");
		return gson.toJson(dataList);
	}	
	
	@CrossOrigin
	@RequestMapping(value = "/getActiveUsersDetailedReportByHour", method = RequestMethod.POST)
	public String getActiveUsersDetailedReportByHour(@RequestBody String request) {
		logger.info("[Enter] getActiveUsersDetailedReportByHour");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		ArrayList<UserActivityHourBasis> dataList = new ArrayList<UserActivityHourBasis>();		
		ArrayList<String> languageList = repo.findUniqueLanguages();	

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}				
				
				Calendar calendar=Calendar.getInstance();
				calendar.setTime(startDate);
				
				ArrayList<Date> startDateArray = new ArrayList<Date>();
				ArrayList<Date> endDateArray = new ArrayList<Date>();
				for(int i=0;i<3;i++) {	
					switch(i) {
					case 0:
						calendar.set(Calendar.HOUR_OF_DAY, 4);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						startDateArray.add(calendar.getTime());
						calendar.set(Calendar.HOUR_OF_DAY, 12);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						endDateArray.add(calendar.getTime());
						break;
					case 1:
						calendar.set(Calendar.HOUR_OF_DAY, 12);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						startDateArray.add(calendar.getTime());
						calendar.set(Calendar.HOUR_OF_DAY, 18);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						endDateArray.add(calendar.getTime());
						break;
					case 2:
						calendar.set(Calendar.HOUR_OF_DAY, 18);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						startDateArray.add(calendar.getTime());
						calendar.set(Calendar.HOUR_OF_DAY, 24);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						endDateArray.add(calendar.getTime());
						break;
					}					
					
					ArrayList<String> totalAppId = usageDataDAO.getTotalActiveAppIDBetweenDate(sPlatform, startDateArray.get(i),endDateArray.get(i));
					DateFormat formatterDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					
					ArrayList<String> newAppId = repo.findNewUsersAppIDBetweenDate(formatterDt.format(startDateArray.get(i)),formatterDt.format(endDateArray.get(i)), sPlatform);
					
					for(String s:newAppId) {
						if(totalAppId.contains(s)) {
							totalAppId.remove(s);
						}
					}
					
					long newUserCount = newAppId.size();
					long returningUserCount = totalAppId.size();
					
					HashMap<String, Integer> targetMap = new HashMap<String, Integer>();
					
					for(String s:languageList) {			
						if(s==null||s.equalsIgnoreCase("")) {
							continue;
						}					
						targetMap.put(s.toLowerCase(), 0);
					}
					
					//New User
					long countMale = 0;
					long countFemale = 0;			
					
					for(String s:newAppId) {
						
						if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
							newUserCount--;
							continue;
						}
						
						String currentGender = repo.findGenderLanguageByAppId(s).get(0);
						String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
						
						
						//System.out.println("Current "+currentGender+"   "+currentLanguage);
							
						//Update Gender Count
						if(currentGender.trim().equalsIgnoreCase("Female")) {
							countFemale++;
						}else {
							countMale++;
						}
						
						//Update language count
						Integer languageCount = targetMap.get(currentLanguage.toLowerCase());
						targetMap.put(currentLanguage.toLowerCase(),(languageCount+1));					
					}
					
					switch(i) {
					case 0:
						dataList.add(new UserActivityHourBasis(sDate,"04 to 12",sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
						break;
					case 1:
						dataList.add(new UserActivityHourBasis(sDate,"12 to 18",sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
						break;
					case 2:
						dataList.add(new UserActivityHourBasis(sDate,"18 to 24",sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
						break;
					}
					
					
										
					HashMap<String, Integer> targetMapReturning = new HashMap<String, Integer>();
					
					for(String s:languageList) {			
						if(s==null||s.equalsIgnoreCase("")) {
							continue;
						}
						targetMapReturning.put(s.toLowerCase(), 0);
					}
									
					//Returning User
					countMale = 0;
					countFemale = 0;
					
					for(String s:totalAppId) {
						
						if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
							returningUserCount--;
							continue;
						}
						
						String currentGender = repo.findGenderLanguageByAppId(s).get(0);
						String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
							
						//Update Gender Count
						if(currentGender.trim().equalsIgnoreCase("Female")) {
							countFemale++;
						}else {
							countMale++;
						}
						
						//Update language count
						Integer languageCount = targetMapReturning.get(currentLanguage.toLowerCase());					
						targetMapReturning.put(currentLanguage.toLowerCase(),(languageCount+1));		
					}	
					
					switch(i) {
					case 0:
						dataList.add(new UserActivityHourBasis(sDate,"04 to 12",sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
						break;
					case 1:
						dataList.add(new UserActivityHourBasis(sDate,"12 to 18",sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
						break;
					case 2:
						dataList.add(new UserActivityHourBasis(sDate,"18 to 24",sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
						break;
					}									
				}				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}				
				
				Calendar calendar=Calendar.getInstance();
				calendar.setTime(startDate);
				
				ArrayList<Date> startDateArray = new ArrayList<Date>();
				ArrayList<Date> endDateArray = new ArrayList<Date>();
				for(int i=0;i<3;i++) {	
					switch(i) {
					case 0:
						calendar.set(Calendar.HOUR_OF_DAY, 4);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						startDateArray.add(calendar.getTime());
						calendar.set(Calendar.HOUR_OF_DAY, 12);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						endDateArray.add(calendar.getTime());
						break;
					case 1:
						calendar.set(Calendar.HOUR_OF_DAY, 12);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						startDateArray.add(calendar.getTime());
						calendar.set(Calendar.HOUR_OF_DAY, 18);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						endDateArray.add(calendar.getTime());
						break;
					case 2:
						calendar.set(Calendar.HOUR_OF_DAY, 18);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						startDateArray.add(calendar.getTime());
						calendar.set(Calendar.HOUR_OF_DAY, 24);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						endDateArray.add(calendar.getTime());
						break;
					}					
					
					ArrayList<String> totalAppId = usageDataDAO.getTotalActiveAppIDBetweenDate(sPlatform, startDateArray.get(i),endDateArray.get(i));
					DateFormat formatterDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					
					ArrayList<String> newAppId = repo.findNewUsersAppIDBetweenDate(formatterDt.format(startDateArray.get(i)),formatterDt.format(endDateArray.get(i)), sPlatform);
					
					for(String s:newAppId) {
						if(totalAppId.contains(s)) {
							totalAppId.remove(s);
						}
					}
					
					long newUserCount = newAppId.size();
					long returningUserCount = totalAppId.size();
					
					HashMap<String, Integer> targetMap = new HashMap<String, Integer>();
					
					for(String s:languageList) {			
						if(s==null||s.equalsIgnoreCase("")) {
							continue;
						}					
						targetMap.put(s.toLowerCase(), 0);
					}
					
					//New User
					long countMale = 0;
					long countFemale = 0;			
					
					for(String s:newAppId) {
						
						if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
							newUserCount--;
							continue;
						}
						
						String currentGender = repo.findGenderLanguageByAppId(s).get(0);
						String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
						
						
						//System.out.println("Current "+currentGender+"   "+currentLanguage);
							
						//Update Gender Count
						if(currentGender.trim().equalsIgnoreCase("Female")) {
							countFemale++;
						}else {
							countMale++;
						}
						
						//Update language count
						Integer languageCount = targetMap.get(currentLanguage.toLowerCase());
						targetMap.put(currentLanguage.toLowerCase(),(languageCount+1));					
					}
					
					switch(i) {
					case 0:
						dataList.add(new UserActivityHourBasis(sDate,"04 to 12",sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
						break;
					case 1:
						dataList.add(new UserActivityHourBasis(sDate,"12 to 18",sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
						break;
					case 2:
						dataList.add(new UserActivityHourBasis(sDate,"18 to 24",sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
						break;
					}
					
					
										
					HashMap<String, Integer> targetMapReturning = new HashMap<String, Integer>();
					
					for(String s:languageList) {			
						if(s==null||s.equalsIgnoreCase("")) {
							continue;
						}
						targetMapReturning.put(s.toLowerCase(), 0);
					}
									
					//Returning User
					countMale = 0;
					countFemale = 0;
					
					for(String s:totalAppId) {
						
						if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
							returningUserCount--;
							continue;
						}
						
						String currentGender = repo.findGenderLanguageByAppId(s).get(0);
						String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
							
						//Update Gender Count
						if(currentGender.trim().equalsIgnoreCase("Female")) {
							countFemale++;
						}else {
							countMale++;
						}
						
						//Update language count
						Integer languageCount = targetMapReturning.get(currentLanguage.toLowerCase());					
						targetMapReturning.put(currentLanguage.toLowerCase(),(languageCount+1));		
					}	
					
					switch(i) {
					case 0:
						dataList.add(new UserActivityHourBasis(sDate,"04 to 12",sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
						break;
					case 1:
						dataList.add(new UserActivityHourBasis(sDate,"12 to 18",sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
						break;
					case 2:
						dataList.add(new UserActivityHourBasis(sDate,"18 to 24",sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
						break;
					}									
				}				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}				
				
				Calendar calendar=Calendar.getInstance();
				calendar.setTime(startDate);
				
				ArrayList<Date> startDateArray = new ArrayList<Date>();
				ArrayList<Date> endDateArray = new ArrayList<Date>();
				for(int i=0;i<3;i++) {	
					switch(i) {
					case 0:
						calendar.set(Calendar.HOUR_OF_DAY, 4);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						startDateArray.add(calendar.getTime());
						calendar.set(Calendar.HOUR_OF_DAY, 12);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						endDateArray.add(calendar.getTime());
						break;
					case 1:
						calendar.set(Calendar.HOUR_OF_DAY, 12);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						startDateArray.add(calendar.getTime());
						calendar.set(Calendar.HOUR_OF_DAY, 18);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						endDateArray.add(calendar.getTime());
						break;
					case 2:
						calendar.set(Calendar.HOUR_OF_DAY, 18);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						startDateArray.add(calendar.getTime());
						calendar.set(Calendar.HOUR_OF_DAY, 24);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						endDateArray.add(calendar.getTime());
						break;
					}					
					
					ArrayList<String> totalAppId = usageDataDAO.getTotalActiveAppIDBetweenDate(sPlatform, startDateArray.get(i),endDateArray.get(i));
					DateFormat formatterDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					
					ArrayList<String> newAppId = repo.findNewUsersAppIDBetweenDate(formatterDt.format(startDateArray.get(i)),formatterDt.format(endDateArray.get(i)), sPlatform);
					
					for(String s:newAppId) {
						if(totalAppId.contains(s)) {
							totalAppId.remove(s);
						}
					}
					
					long newUserCount = newAppId.size();
					long returningUserCount = totalAppId.size();
					
					HashMap<String, Integer> targetMap = new HashMap<String, Integer>();
					
					for(String s:languageList) {			
						if(s==null||s.equalsIgnoreCase("")) {
							continue;
						}					
						targetMap.put(s.toLowerCase(), 0);
					}
					
					//New User
					long countMale = 0;
					long countFemale = 0;			
					
					for(String s:newAppId) {
						
						if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
							newUserCount--;
							continue;
						}
						
						String currentGender = repo.findGenderLanguageByAppId(s).get(0);
						String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
						
						
						//System.out.println("Current "+currentGender+"   "+currentLanguage);
							
						//Update Gender Count
						if(currentGender.trim().equalsIgnoreCase("Female")) {
							countFemale++;
						}else {
							countMale++;
						}
						
						//Update language count
						Integer languageCount = targetMap.get(currentLanguage.toLowerCase());
						targetMap.put(currentLanguage.toLowerCase(),(languageCount+1));					
					}
					
					switch(i) {
					case 0:
						dataList.add(new UserActivityHourBasis(sDate,"04 to 12",sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
						break;
					case 1:
						dataList.add(new UserActivityHourBasis(sDate,"12 to 18",sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
						break;
					case 2:
						dataList.add(new UserActivityHourBasis(sDate,"18 to 24",sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
						break;
					}
					
					
										
					HashMap<String, Integer> targetMapReturning = new HashMap<String, Integer>();
					
					for(String s:languageList) {			
						if(s==null||s.equalsIgnoreCase("")) {
							continue;
						}
						targetMapReturning.put(s.toLowerCase(), 0);
					}
									
					//Returning User
					countMale = 0;
					countFemale = 0;
					
					for(String s:totalAppId) {
						
						if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
							returningUserCount--;
							continue;
						}
						
						String currentGender = repo.findGenderLanguageByAppId(s).get(0);
						String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
							
						//Update Gender Count
						if(currentGender.trim().equalsIgnoreCase("Female")) {
							countFemale++;
						}else {
							countMale++;
						}
						
						//Update language count
						Integer languageCount = targetMapReturning.get(currentLanguage.toLowerCase());					
						targetMapReturning.put(currentLanguage.toLowerCase(),(languageCount+1));		
					}	
					
					switch(i) {
					case 0:
						dataList.add(new UserActivityHourBasis(sDate,"04 to 12",sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
						break;
					case 1:
						dataList.add(new UserActivityHourBasis(sDate,"12 to 18",sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
						break;
					case 2:
						dataList.add(new UserActivityHourBasis(sDate,"18 to 24",sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
						break;
					}									
				}				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getActiveUsersDetailedReportByHour");
		return gson.toJson(dataList);
	}	
	
	
	
	
	@CrossOrigin
	@RequestMapping(value = "/getActiveUsersDetailedReportByEveryHour", method = RequestMethod.POST)
	public String getActiveUsersDetailedReportByEveryHour(@RequestBody String request) {
		logger.info("[Enter] getActiveUsersDetailedReportByEveryHour");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		ArrayList<UserActivityHourBasis> dataList = new ArrayList<UserActivityHourBasis>();		
		ArrayList<String> languageList = repo.findUniqueLanguages();	

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}				
				
				Calendar calendar=Calendar.getInstance();
				calendar.setTime(startDate);
				
				ArrayList<Date> startDateArray = new ArrayList<Date>();
				ArrayList<Date> endDateArray = new ArrayList<Date>();
				for(int i=0;i<3;i++) {	
					switch(i) {
					case 0:
						calendar.set(Calendar.HOUR_OF_DAY, 4);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						startDateArray.add(calendar.getTime());
						calendar.set(Calendar.HOUR_OF_DAY, 12);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						endDateArray.add(calendar.getTime());
						break;
					case 1:
						calendar.set(Calendar.HOUR_OF_DAY, 12);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						startDateArray.add(calendar.getTime());
						calendar.set(Calendar.HOUR_OF_DAY, 18);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						endDateArray.add(calendar.getTime());
						break;
					case 2:
						calendar.set(Calendar.HOUR_OF_DAY, 18);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						startDateArray.add(calendar.getTime());
						calendar.set(Calendar.HOUR_OF_DAY, 24);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						endDateArray.add(calendar.getTime());
						break;
					}					
					
					ArrayList<String> totalAppId = usageDataDAO.getTotalActiveAppIDBetweenDate(sPlatform, startDateArray.get(i),endDateArray.get(i));
					DateFormat formatterDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					
					ArrayList<String> newAppId = repo.findNewUsersAppIDBetweenDate(formatterDt.format(startDateArray.get(i)),formatterDt.format(endDateArray.get(i)), sPlatform);
					
					for(String s:newAppId) {
						if(totalAppId.contains(s)) {
							totalAppId.remove(s);
						}
					}
					
					long newUserCount = newAppId.size();
					long returningUserCount = totalAppId.size();
					
					HashMap<String, Integer> targetMap = new HashMap<String, Integer>();
					
					for(String s:languageList) {			
						if(s==null||s.equalsIgnoreCase("")) {
							continue;
						}					
						targetMap.put(s.toLowerCase(), 0);
					}
					
					//New User
					long countMale = 0;
					long countFemale = 0;			
					
					for(String s:newAppId) {
						
						if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
							newUserCount--;
							continue;
						}
						
						String currentGender = repo.findGenderLanguageByAppId(s).get(0);
						String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
						
						
						//System.out.println("Current "+currentGender+"   "+currentLanguage);
							
						//Update Gender Count
						if(currentGender.trim().equalsIgnoreCase("Female")) {
							countFemale++;
						}else {
							countMale++;
						}
						
						//Update language count
						Integer languageCount = targetMap.get(currentLanguage.toLowerCase());
						targetMap.put(currentLanguage.toLowerCase(),(languageCount+1));					
					}
					
					switch(i) {
					case 0:
						dataList.add(new UserActivityHourBasis(sDate,"04 to 12",sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
						break;
					case 1:
						dataList.add(new UserActivityHourBasis(sDate,"12 to 18",sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
						break;
					case 2:
						dataList.add(new UserActivityHourBasis(sDate,"18 to 24",sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
						break;
					}
					
					
										
					HashMap<String, Integer> targetMapReturning = new HashMap<String, Integer>();
					
					for(String s:languageList) {			
						if(s==null||s.equalsIgnoreCase("")) {
							continue;
						}
						targetMapReturning.put(s.toLowerCase(), 0);
					}
									
					//Returning User
					countMale = 0;
					countFemale = 0;
					
					for(String s:totalAppId) {
						
						if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
							returningUserCount--;
							continue;
						}
						
						String currentGender = repo.findGenderLanguageByAppId(s).get(0);
						String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
							
						//Update Gender Count
						if(currentGender.trim().equalsIgnoreCase("Female")) {
							countFemale++;
						}else {
							countMale++;
						}
						
						//Update language count
						Integer languageCount = targetMapReturning.get(currentLanguage.toLowerCase());					
						targetMapReturning.put(currentLanguage.toLowerCase(),(languageCount+1));		
					}	
					
					switch(i) {
					case 0:
						dataList.add(new UserActivityHourBasis(sDate,"04 to 12",sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
						break;
					case 1:
						dataList.add(new UserActivityHourBasis(sDate,"12 to 18",sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
						break;
					case 2:
						dataList.add(new UserActivityHourBasis(sDate,"18 to 24",sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
						break;
					}									
				}				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}				
				
				Calendar calendar=Calendar.getInstance();
				calendar.setTime(startDate);
				
				ArrayList<Date> startDateArray = new ArrayList<Date>();
				ArrayList<Date> endDateArray = new ArrayList<Date>();
				for(int i=0;i<3;i++) {	
					switch(i) {
					case 0:
						calendar.set(Calendar.HOUR_OF_DAY, 4);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						startDateArray.add(calendar.getTime());
						calendar.set(Calendar.HOUR_OF_DAY, 12);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						endDateArray.add(calendar.getTime());
						break;
					case 1:
						calendar.set(Calendar.HOUR_OF_DAY, 12);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						startDateArray.add(calendar.getTime());
						calendar.set(Calendar.HOUR_OF_DAY, 18);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						endDateArray.add(calendar.getTime());
						break;
					case 2:
						calendar.set(Calendar.HOUR_OF_DAY, 18);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						startDateArray.add(calendar.getTime());
						calendar.set(Calendar.HOUR_OF_DAY, 24);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						endDateArray.add(calendar.getTime());
						break;
					}					
					
					ArrayList<String> totalAppId = usageDataDAO.getTotalActiveAppIDBetweenDate(sPlatform, startDateArray.get(i),endDateArray.get(i));
					DateFormat formatterDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					
					ArrayList<String> newAppId = repo.findNewUsersAppIDBetweenDate(formatterDt.format(startDateArray.get(i)),formatterDt.format(endDateArray.get(i)), sPlatform);
					
					for(String s:newAppId) {
						if(totalAppId.contains(s)) {
							totalAppId.remove(s);
						}
					}
					
					long newUserCount = newAppId.size();
					long returningUserCount = totalAppId.size();
					
					HashMap<String, Integer> targetMap = new HashMap<String, Integer>();
					
					for(String s:languageList) {			
						if(s==null||s.equalsIgnoreCase("")) {
							continue;
						}					
						targetMap.put(s.toLowerCase(), 0);
					}
					
					//New User
					long countMale = 0;
					long countFemale = 0;			
					
					for(String s:newAppId) {
						
						if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
							newUserCount--;
							continue;
						}
						
						String currentGender = repo.findGenderLanguageByAppId(s).get(0);
						String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
						
						
						//System.out.println("Current "+currentGender+"   "+currentLanguage);
							
						//Update Gender Count
						if(currentGender.trim().equalsIgnoreCase("Female")) {
							countFemale++;
						}else {
							countMale++;
						}
						
						//Update language count
						Integer languageCount = targetMap.get(currentLanguage.toLowerCase());
						targetMap.put(currentLanguage.toLowerCase(),(languageCount+1));					
					}
					
					switch(i) {
					case 0:
						dataList.add(new UserActivityHourBasis(sDate,"04 to 12",sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
						break;
					case 1:
						dataList.add(new UserActivityHourBasis(sDate,"12 to 18",sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
						break;
					case 2:
						dataList.add(new UserActivityHourBasis(sDate,"18 to 24",sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
						break;
					}
					
					
										
					HashMap<String, Integer> targetMapReturning = new HashMap<String, Integer>();
					
					for(String s:languageList) {			
						if(s==null||s.equalsIgnoreCase("")) {
							continue;
						}
						targetMapReturning.put(s.toLowerCase(), 0);
					}
									
					//Returning User
					countMale = 0;
					countFemale = 0;
					
					for(String s:totalAppId) {
						
						if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
							returningUserCount--;
							continue;
						}
						
						String currentGender = repo.findGenderLanguageByAppId(s).get(0);
						String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
							
						//Update Gender Count
						if(currentGender.trim().equalsIgnoreCase("Female")) {
							countFemale++;
						}else {
							countMale++;
						}
						
						//Update language count
						Integer languageCount = targetMapReturning.get(currentLanguage.toLowerCase());					
						targetMapReturning.put(currentLanguage.toLowerCase(),(languageCount+1));		
					}	
					
					switch(i) {
					case 0:
						dataList.add(new UserActivityHourBasis(sDate,"04 to 12",sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
						break;
					case 1:
						dataList.add(new UserActivityHourBasis(sDate,"12 to 18",sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
						break;
					case 2:
						dataList.add(new UserActivityHourBasis(sDate,"18 to 24",sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
						break;
					}									
				}				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}				
				
				Calendar calendar=Calendar.getInstance();
				calendar.setTime(startDate);
				
				ArrayList<Date> startDateArray = new ArrayList<Date>();
				ArrayList<Date> endDateArray = new ArrayList<Date>();
				for(int i=0;i<3;i++) {	
					switch(i) {
					case 0:
						calendar.set(Calendar.HOUR_OF_DAY, 22);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						startDateArray.add(calendar.getTime());
						calendar.set(Calendar.HOUR_OF_DAY, 22);
						calendar.set(Calendar.MINUTE, 59);
						calendar.set(Calendar.SECOND, 59);
						calendar.set(Calendar.MILLISECOND, 0);
						endDateArray.add(calendar.getTime());
						break;
					case 1:
						calendar.set(Calendar.HOUR_OF_DAY, 23);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						startDateArray.add(calendar.getTime());
						calendar.set(Calendar.HOUR_OF_DAY, 23);
						calendar.set(Calendar.MINUTE, 59);
						calendar.set(Calendar.SECOND, 59);
						calendar.set(Calendar.MILLISECOND, 0);
						endDateArray.add(calendar.getTime());
						break;
					case 2:
						calendar.set(Calendar.HOUR_OF_DAY, 24);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						startDateArray.add(calendar.getTime());
						calendar.set(Calendar.HOUR_OF_DAY, 24);
						calendar.set(Calendar.MINUTE, 59);
						calendar.set(Calendar.SECOND, 59);
						calendar.set(Calendar.MILLISECOND, 0);
						endDateArray.add(calendar.getTime());
						break;
					}					
					
					ArrayList<String> totalAppId = usageDataDAO.getTotalActiveAppIDBetweenDate(sPlatform, startDateArray.get(i),endDateArray.get(i));
					DateFormat formatterDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					
					ArrayList<String> newAppId = repo.findNewUsersAppIDBetweenDate(formatterDt.format(startDateArray.get(i)),formatterDt.format(endDateArray.get(i)), sPlatform);
					
					for(String s:newAppId) {
						if(totalAppId.contains(s)) {
							totalAppId.remove(s);
						}
					}
					
					long newUserCount = newAppId.size();
					long returningUserCount = totalAppId.size();
					
					HashMap<String, Integer> targetMap = new HashMap<String, Integer>();
					
					for(String s:languageList) {			
						if(s==null||s.equalsIgnoreCase("")) {
							continue;
						}					
						targetMap.put(s.toLowerCase(), 0);
					}
					
					//New User
					long countMale = 0;
					long countFemale = 0;			
					
					for(String s:newAppId) {
						
						if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
							newUserCount--;
							continue;
						}
						
						String currentGender = repo.findGenderLanguageByAppId(s).get(0);
						String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
						
						
						//System.out.println("Current "+currentGender+"   "+currentLanguage);
							
						//Update Gender Count
						if(currentGender.trim().equalsIgnoreCase("Female")) {
							countFemale++;
						}else {
							countMale++;
						}
						
						//Update language count
						Integer languageCount = targetMap.get(currentLanguage.toLowerCase());
						targetMap.put(currentLanguage.toLowerCase(),(languageCount+1));					
					}
					
					switch(i) {
					case 0:
						dataList.add(new UserActivityHourBasis(sDate,"04 to 12",sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
						break;
					case 1:
						dataList.add(new UserActivityHourBasis(sDate,"12 to 18",sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
						break;
					case 2:
						dataList.add(new UserActivityHourBasis(sDate,"18 to 24",sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
						break;
					}
					
					
										
					HashMap<String, Integer> targetMapReturning = new HashMap<String, Integer>();
					
					for(String s:languageList) {			
						if(s==null||s.equalsIgnoreCase("")) {
							continue;
						}
						targetMapReturning.put(s.toLowerCase(), 0);
					}
									
					//Returning User
					countMale = 0;
					countFemale = 0;
					
					for(String s:totalAppId) {
						
						if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
							returningUserCount--;
							continue;
						}
						
						String currentGender = repo.findGenderLanguageByAppId(s).get(0);
						String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
							
						//Update Gender Count
						if(currentGender.trim().equalsIgnoreCase("Female")) {
							countFemale++;
						}else {
							countMale++;
						}
						
						//Update language count
						Integer languageCount = targetMapReturning.get(currentLanguage.toLowerCase());					
						targetMapReturning.put(currentLanguage.toLowerCase(),(languageCount+1));		
					}	
					
					switch(i) {
					case 0:
						dataList.add(new UserActivityHourBasis(sDate,"04 to 12",sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
						break;
					case 1:
						dataList.add(new UserActivityHourBasis(sDate,"12 to 18",sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
						break;
					case 2:
						dataList.add(new UserActivityHourBasis(sDate,"18 to 24",sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
						break;
					}									
				}				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getActiveUsersDetailedReportByEveryHour");
		return gson.toJson(dataList);
	}	
	
	
	
	@CrossOrigin
	@RequestMapping(value = "/getActiveUsersDetailedReport", method = RequestMethod.POST)
	public String getActiveUsersDetailedReport(@RequestBody String request) {
		logger.info("[Enter] getActiveUsersDetailedReport");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		ArrayList<ActiveUserDataDetail> dataList = new ArrayList<ActiveUserDataDetail>();		
		ArrayList<String> languageList = repo.findUniqueLanguages();	

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}				
				ArrayList<String> totalAppId = usageDataDAO.getTotalActiveAppIDByDate(sPlatform, startDate);
				//System.out.println("Toatl "+totalAppId.size());
				ArrayList<String> newAppId = repo.findNewUsersAppIDByDate(sDate, sPlatform);
				//System.out.println("New "+newAppId.size());
				//Returning APP IDs
				//totalAppId.removeAll(newAppId);	
				
				for(String s:newAppId) {
					if(totalAppId.contains(s)) {
						totalAppId.remove(s);
					}else {
						//System.out.println("Not Found");
					}
				}
								
				//System.out.println("Returning "+totalAppId.size());
				
				long newUserCount = newAppId.size();
				long returningUserCount = totalAppId.size();
				
				HashMap<String, Integer> targetMap = new HashMap<String, Integer>();
				
				for(String s:languageList) {			
					if(s==null||s.equalsIgnoreCase("")) {
						continue;
					}					
					targetMap.put(s.toLowerCase(), 0);
				}
				
				
				//New User
				long countMale = 0;
				long countFemale = 0;			
				
				for(String s:newAppId) {
					
					if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
						newUserCount--;
						continue;
					}
					
					String currentGender = repo.findGenderLanguageByAppId(s).get(0);
					String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
					
					
					//System.out.println("Current "+currentGender+"   "+currentLanguage);
						
					//Update Gender Count
					if(currentGender.trim().equalsIgnoreCase("Female")) {
						countFemale++;
					}else {
						countMale++;
					}
					
					//Update language count
					Integer languageCount = targetMap.get(currentLanguage.toLowerCase());
					targetMap.put(currentLanguage.toLowerCase(),(languageCount+1));					
				}
				
				dataList.add(new ActiveUserDataDetail(sDate,sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
				
				
				
				HashMap<String, Integer> targetMapReturning = new HashMap<String, Integer>();
				
				for(String s:languageList) {			
					if(s==null||s.equalsIgnoreCase("")) {
						continue;
					}
					//System.out.println("Adding Language >"+s+"<<<");
					targetMapReturning.put(s.toLowerCase(), 0);
				}
				

				
				//Returning User
				countMale = 0;
				countFemale = 0;
				
				for(String s:totalAppId) {
					
					if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
						returningUserCount--;
						continue;
					}
					
					String currentGender = repo.findGenderLanguageByAppId(s).get(0);
					String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
						
					//Update Gender Count
					if(currentGender.trim().equalsIgnoreCase("Female")) {
						countFemale++;
					}else {
						countMale++;
					}
					
					//Update language count
					Integer languageCount = targetMapReturning.get(currentLanguage.toLowerCase());					
					targetMapReturning.put(currentLanguage.toLowerCase(),(languageCount+1));		
				}	
				
				dataList.add(new ActiveUserDataDetail(sDate,sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				ArrayList<String> totalAppId = usageDataDAO.getTotalActiveAppIDByDate(sPlatform, startDate);
				//System.out.println("Toatl "+totalAppId.size());
				ArrayList<String> newAppId = repo.findNewUsersAppIDByDate(sDate, sPlatform);
				//System.out.println("New "+newAppId.size());
				//Returning APP IDs
				//totalAppId.removeAll(newAppId);	
				
				for(String s:newAppId) {
					if(totalAppId.contains(s)) {
						totalAppId.remove(s);
					}else {
						//System.out.println("Not Found");
					}
				}
								
				//System.out.println("Returning "+totalAppId.size());
				
				long newUserCount = newAppId.size();
				long returningUserCount = totalAppId.size();
				
				HashMap<String, Integer> targetMap = new HashMap<String, Integer>();
				
				for(String s:languageList) {			
					if(s==null||s.equalsIgnoreCase("")) {
						continue;
					}					
					targetMap.put(s.toLowerCase(), 0);
				}
				
				
				//New User
				long countMale = 0;
				long countFemale = 0;			
				
				for(String s:newAppId) {
					
					if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
						newUserCount--;
						continue;
					}
					
					String currentGender = repo.findGenderLanguageByAppId(s).get(0);
					String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
					
					
					//System.out.println("Current "+currentGender+"   "+currentLanguage);
						
					//Update Gender Count
					if(currentGender.trim().equalsIgnoreCase("Female")) {
						countFemale++;
					}else {
						countMale++;
					}
					
					//Update language count
					Integer languageCount = targetMap.get(currentLanguage.toLowerCase());
					targetMap.put(currentLanguage.toLowerCase(),(languageCount+1));					
				}
				
				dataList.add(new ActiveUserDataDetail(sDate,sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
				
				
				
				HashMap<String, Integer> targetMapReturning = new HashMap<String, Integer>();
				
				for(String s:languageList) {			
					if(s==null||s.equalsIgnoreCase("")) {
						continue;
					}
					//System.out.println("Adding Language >"+s+"<<<");
					targetMapReturning.put(s.toLowerCase(), 0);
				}
				

				
				//Returning User
				countMale = 0;
				countFemale = 0;
				
				for(String s:totalAppId) {
					
					if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
						returningUserCount--;
						continue;
					}
					
					String currentGender = repo.findGenderLanguageByAppId(s).get(0);
					String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
						
					//Update Gender Count
					if(currentGender.trim().equalsIgnoreCase("Female")) {
						countFemale++;
					}else {
						countMale++;
					}
					
					//Update language count
					Integer languageCount = targetMapReturning.get(currentLanguage.toLowerCase());					
					targetMapReturning.put(currentLanguage.toLowerCase(),(languageCount+1));		
				}	
				
				dataList.add(new ActiveUserDataDetail(sDate,sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}				
				
				ArrayList<String> totalAppId = usageDataDAO.getTotalActiveAppIDByDate(sPlatform, startDate);
				//System.out.println("Toatl "+totalAppId.size());
				ArrayList<String> newAppId = repo.findNewUsersAppIDByDate(sDate, sPlatform);
				//System.out.println("New "+newAppId.size());
				//Returning APP IDs
				//totalAppId.removeAll(newAppId);	
				
				for(String s:newAppId) {
					if(totalAppId.contains(s)) {
						totalAppId.remove(s);
					}else {
						//System.out.println("Not Found");
					}
				}
								
				//System.out.println("Returning "+totalAppId.size());
				
				long newUserCount = newAppId.size();
				long returningUserCount = totalAppId.size();
				
				HashMap<String, Integer> targetMap = new HashMap<String, Integer>();
				
				for(String s:languageList) {			
					if(s==null||s.equalsIgnoreCase("")) {
						continue;
					}					
					targetMap.put(s.toLowerCase(), 0);
				}
				
				
				//New User
				long countMale = 0;
				long countFemale = 0;			
				
				for(String s:newAppId) {
					
					if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
						newUserCount--;
						continue;
					}
					
					String currentGender = repo.findGenderLanguageByAppId(s).get(0);
					String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
					
					
					//System.out.println("Current "+currentGender+"   "+currentLanguage);
						
					//Update Gender Count
					if(currentGender.trim().equalsIgnoreCase("Female")) {
						countFemale++;
					}else {
						countMale++;
					}
					
					//Update language count
					Integer languageCount = targetMap.get(currentLanguage.toLowerCase());
					targetMap.put(currentLanguage.toLowerCase(),(languageCount+1));					
				}
				
				dataList.add(new ActiveUserDataDetail(sDate,sPlatform,"New User",newUserCount,countMale,countFemale,targetMap));
				
				
				
				HashMap<String, Integer> targetMapReturning = new HashMap<String, Integer>();
				
				for(String s:languageList) {			
					if(s==null||s.equalsIgnoreCase("")) {
						continue;
					}
					//System.out.println("Adding Language >"+s+"<<<");
					targetMapReturning.put(s.toLowerCase(), 0);
				}
				

				
				//Returning User
				countMale = 0;
				countFemale = 0;
				
				for(String s:totalAppId) {
					
					if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
						returningUserCount--;
						continue;
					}
					
					String currentGender = repo.findGenderLanguageByAppId(s).get(0);
					String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
						
					//Update Gender Count
					if(currentGender.trim().equalsIgnoreCase("Female")) {
						countFemale++;
					}else {
						countMale++;
					}
					
					//Update language count
					Integer languageCount = targetMapReturning.get(currentLanguage.toLowerCase());					
					targetMapReturning.put(currentLanguage.toLowerCase(),(languageCount+1));		
				}	
				
				dataList.add(new ActiveUserDataDetail(sDate,sPlatform,"Returning User",returningUserCount,countMale,countFemale,targetMapReturning));
				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getActiveUsersDetailedReport");
		return gson.toJson(dataList);
	}	
	

	@CrossOrigin
	@RequestMapping(value = "/getRemindersReport", method = RequestMethod.POST)
	public String getRemindersReport(@RequestBody String request) {
		logger.info("[Enter] getRemindersReport");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}

		ArrayList<HomeScreenReminderReport> reportObj = new ArrayList<HomeScreenReminderReport>();

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				//Home Count
				long countLoginClickedHome = usageDataDAO.getReminderByDate(startDate, sPlatform, "home", "27");
				long countSignupClickedHome = usageDataDAO.getReminderByDate(startDate, sPlatform, "home", "19");
				long countKeyboardInitHome = usageDataDAO.getReminderByDate(startDate, sPlatform, "home", "15");
				long countCreateAvatarHome = usageDataDAO.getReminderByDate(startDate, sPlatform, "home", "42");
				//Settings Count
				long countLoginClickedSettings = usageDataDAO.getReminderByDate(startDate, sPlatform, "settings", "27");
				long countSignupClickedSettings = usageDataDAO.getReminderByDate(startDate, sPlatform, "settings",
						"19");
				long countKeyboardInitSettings = usageDataDAO.getReminderByDate(startDate, sPlatform, "settings", "15");
				long countCreateAvatarSettings = usageDataDAO.getReminderByDate(startDate, sPlatform, "settings", "42");
				
				//Update Count
				if(countLoginClickedHome+countLoginClickedSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"Login",countLoginClickedHome,"Home"));
				}				
				if(countSignupClickedHome+countSignupClickedSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"SignUp",countSignupClickedHome,"Home"));
				}				
				if(countKeyboardInitHome+countKeyboardInitSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"Keyboard",countKeyboardInitHome,"Home"));
				}				
				if(countCreateAvatarHome+countCreateAvatarSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"Avatar",countCreateAvatarHome,"Home"));
				}				
				if(countLoginClickedHome+countLoginClickedSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"Login",countLoginClickedSettings,"Settings"));
				}				
				if(countSignupClickedHome+countSignupClickedSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"SignUp",countSignupClickedSettings,"Settings"));
				}				
				if(countKeyboardInitHome+countKeyboardInitSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"Keyboard",countKeyboardInitSettings,"Settings"));
				}				
				if(countCreateAvatarHome+countCreateAvatarSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"Avatar",countCreateAvatarSettings,"Settings"));
				}			 

				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}

				//Home Count
				long countLoginClickedHome = usageDataDAO.getReminderByDate(startDate, sPlatform, "home", "27");
				long countSignupClickedHome = usageDataDAO.getReminderByDate(startDate, sPlatform, "home", "19");
				long countKeyboardInitHome = usageDataDAO.getReminderByDate(startDate, sPlatform, "home", "15");
				long countCreateAvatarHome = usageDataDAO.getReminderByDate(startDate, sPlatform, "home", "42");
				//Settings Count
				long countLoginClickedSettings = usageDataDAO.getReminderByDate(startDate, sPlatform, "settings", "27");
				long countSignupClickedSettings = usageDataDAO.getReminderByDate(startDate, sPlatform, "settings",
						"19");
				long countKeyboardInitSettings = usageDataDAO.getReminderByDate(startDate, sPlatform, "settings", "15");
				long countCreateAvatarSettings = usageDataDAO.getReminderByDate(startDate, sPlatform, "settings", "42");
				
				//Update Count
				if(countLoginClickedHome+countLoginClickedSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"Login",countLoginClickedHome,"Home"));
				}				
				if(countSignupClickedHome+countSignupClickedSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"SignUp",countSignupClickedHome,"Home"));
				}				
				if(countKeyboardInitHome+countKeyboardInitSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"Keyboard",countKeyboardInitHome,"Home"));
				}				
				if(countCreateAvatarHome+countCreateAvatarSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"Avatar",countCreateAvatarHome,"Home"));
				}				
				if(countLoginClickedHome+countLoginClickedSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"Login",countLoginClickedSettings,"Settings"));
				}				
				if(countSignupClickedHome+countSignupClickedSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"SignUp",countSignupClickedSettings,"Settings"));
				}				
				if(countKeyboardInitHome+countKeyboardInitSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"Keyboard",countKeyboardInitSettings,"Settings"));
				}				
				if(countCreateAvatarHome+countCreateAvatarSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"Avatar",countCreateAvatarSettings,"Settings"));
				}	
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}

				//Home Count
				long countLoginClickedHome = usageDataDAO.getReminderByDate(startDate, sPlatform, "home", "27");
				long countSignupClickedHome = usageDataDAO.getReminderByDate(startDate, sPlatform, "home", "19");
				long countKeyboardInitHome = usageDataDAO.getReminderByDate(startDate, sPlatform, "home", "15");
				long countCreateAvatarHome = usageDataDAO.getReminderByDate(startDate, sPlatform, "home", "42");
				//Settings Count
				long countLoginClickedSettings = usageDataDAO.getReminderByDate(startDate, sPlatform, "settings", "27");
				long countSignupClickedSettings = usageDataDAO.getReminderByDate(startDate, sPlatform, "settings",
						"19");
				long countKeyboardInitSettings = usageDataDAO.getReminderByDate(startDate, sPlatform, "settings", "15");
				long countCreateAvatarSettings = usageDataDAO.getReminderByDate(startDate, sPlatform, "settings", "42");
				
				//Update Count
				if(countLoginClickedHome+countLoginClickedSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"Login",countLoginClickedHome,"Home"));
				}				
				if(countSignupClickedHome+countSignupClickedSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"SignUp",countSignupClickedHome,"Home"));
				}				
				if(countKeyboardInitHome+countKeyboardInitSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"Keyboard",countKeyboardInitHome,"Home"));
				}				
				if(countCreateAvatarHome+countCreateAvatarSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"Avatar",countCreateAvatarHome,"Home"));
				}				
				if(countLoginClickedHome+countLoginClickedSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"Login",countLoginClickedSettings,"Settings"));
				}				
				if(countSignupClickedHome+countSignupClickedSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"SignUp",countSignupClickedSettings,"Settings"));
				}				
				if(countKeyboardInitHome+countKeyboardInitSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"Keyboard",countKeyboardInitSettings,"Settings"));
				}				
				if(countCreateAvatarHome+countCreateAvatarSettings>0) {
					reportObj.add(new HomeScreenReminderReport(sDate,sPlatform,"Avatar",countCreateAvatarSettings,"Settings"));
				}	

				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}

		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getRemindersReport");
		return new Gson().toJson(reportObj);
	}

	@CrossOrigin
	@RequestMapping(value = "/getPermissionsReport", method = RequestMethod.POST)
	public String getPermissionsReport(@RequestBody String request) {
		logger.info("[Enter] getPermissionsReport");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}

		ArrayList<HomeScreenPermissionReport> reportObj = new ArrayList<HomeScreenPermissionReport>();

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}

				long countSuccessNotHome = usageDataDAO.getPermissionByDate(startDate, sPlatform, "home", "true", "80");
				long countfailureNotHome = usageDataDAO.getPermissionByDate(startDate, sPlatform, "home", "false",
						"80");
				long countSuccessNotSettings = usageDataDAO.getPermissionByDate(startDate, sPlatform, "settings",
						"true", "80");
				long countfailureNotSettings = usageDataDAO.getPermissionByDate(startDate, sPlatform, "settings",
						"false", "80");

				if (countSuccessNotHome + countfailureNotHome > 0) {
					reportObj.add(new HomeScreenPermissionReport(sDate, sPlatform, "Notification",
							countSuccessNotHome + "", countfailureNotHome + "", "homeScreen"));
				}
				if (countSuccessNotSettings + countfailureNotSettings > 0) {
					reportObj.add(new HomeScreenPermissionReport(sDate, sPlatform, "Notification",
							countSuccessNotSettings + "", countfailureNotSettings + "", "settings"));
				}

				long countSuccessMedHome = usageDataDAO.getPermissionByDate(startDate, sPlatform, "home", "true", "79");
				long countfailureMedHome = usageDataDAO.getPermissionByDate(startDate, sPlatform, "home", "false",
						"79");
				long countSuccessMedSettings = usageDataDAO.getPermissionByDate(startDate, sPlatform, "settings",
						"true", "79");
				long countfailureMedSettings = usageDataDAO.getPermissionByDate(startDate, sPlatform, "settings",
						"false", "79");

				if (countSuccessMedHome + countfailureMedHome > 0) {
					reportObj.add(new HomeScreenPermissionReport(sDate, sPlatform, "Notification",
							countSuccessMedHome + "", countfailureMedHome + "", "homeScreen"));
				}
				if (countSuccessMedSettings + countfailureMedSettings > 0) {
					reportObj.add(new HomeScreenPermissionReport(sDate, sPlatform, "Notification",
							countSuccessMedSettings + "", countfailureMedSettings + "", "settings"));
				}

				long countSuccessLocHome = usageDataDAO.getPermissionByDate(startDate, sPlatform, "home", "true", "78");
				long countfailureLocHome = usageDataDAO.getPermissionByDate(startDate, sPlatform, "home", "false",
						"79");
				long countSuccessLocSettings = usageDataDAO.getPermissionByDate(startDate, sPlatform, "settings",
						"true", "79");
				long countfailureLocSettings = usageDataDAO.getPermissionByDate(startDate, sPlatform, "settings",
						"false", "79");

				if (countSuccessLocHome + countfailureLocHome > 0) {
					reportObj.add(new HomeScreenPermissionReport(sDate, sPlatform, "Notification",
							countSuccessLocHome + "", countfailureLocHome + "", "homeScreen"));
				}
				if (countSuccessLocSettings + countfailureLocSettings > 0) {
					reportObj.add(new HomeScreenPermissionReport(sDate, sPlatform, "Notification",
							countSuccessLocSettings + "", countfailureLocSettings + "", "settings"));
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}

		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}

				long countSuccessNotHome = usageDataDAO.getPermissionByDate(startDate, sPlatform, "home", "true", "80");
				long countfailureNotHome = usageDataDAO.getPermissionByDate(startDate, sPlatform, "home", "false",
						"80");
				long countSuccessNotSettings = usageDataDAO.getPermissionByDate(startDate, sPlatform, "settings",
						"true", "80");
				long countfailureNotSettings = usageDataDAO.getPermissionByDate(startDate, sPlatform, "settings",
						"false", "80");

				if (countSuccessNotHome + countfailureNotHome > 0) {
					reportObj.add(new HomeScreenPermissionReport(sDate, sPlatform, "Notification",
							countSuccessNotHome + "", countfailureNotHome + "", "homeScreen"));
				}
				if (countSuccessNotSettings + countfailureNotSettings > 0) {
					reportObj.add(new HomeScreenPermissionReport(sDate, sPlatform, "Notification",
							countSuccessNotSettings + "", countfailureNotSettings + "", "settings"));
				}

				long countSuccessMedHome = usageDataDAO.getPermissionByDate(startDate, sPlatform, "home", "true", "79");
				long countfailureMedHome = usageDataDAO.getPermissionByDate(startDate, sPlatform, "home", "false",
						"79");
				long countSuccessMedSettings = usageDataDAO.getPermissionByDate(startDate, sPlatform, "settings",
						"true", "79");
				long countfailureMedSettings = usageDataDAO.getPermissionByDate(startDate, sPlatform, "settings",
						"false", "79");

				if (countSuccessMedHome + countfailureMedHome > 0) {
					reportObj.add(new HomeScreenPermissionReport(sDate, sPlatform, "Notification",
							countSuccessMedHome + "", countfailureMedHome + "", "homeScreen"));
				}
				if (countSuccessMedSettings + countfailureMedSettings > 0) {
					reportObj.add(new HomeScreenPermissionReport(sDate, sPlatform, "Notification",
							countSuccessMedSettings + "", countfailureMedSettings + "", "settings"));
				}

				long countSuccessLocHome = usageDataDAO.getPermissionByDate(startDate, sPlatform, "home", "true", "78");
				long countfailureLocHome = usageDataDAO.getPermissionByDate(startDate, sPlatform, "home", "false",
						"79");
				long countSuccessLocSettings = usageDataDAO.getPermissionByDate(startDate, sPlatform, "settings",
						"true", "79");
				long countfailureLocSettings = usageDataDAO.getPermissionByDate(startDate, sPlatform, "settings",
						"false", "79");

				if (countSuccessLocHome + countfailureLocHome > 0) {
					reportObj.add(new HomeScreenPermissionReport(sDate, sPlatform, "Notification",
							countSuccessLocHome + "", countfailureLocHome + "", "homeScreen"));
				}
				if (countSuccessLocSettings + countfailureLocSettings > 0) {
					reportObj.add(new HomeScreenPermissionReport(sDate, sPlatform, "Notification",
							countSuccessLocSettings + "", countfailureLocSettings + "", "settings"));
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}

				long countSuccessNotHome = usageDataDAO.getPermissionByDate(startDate, sPlatform, "home", "true", "80");
				long countfailureNotHome = usageDataDAO.getPermissionByDate(startDate, sPlatform, "home", "false",
						"80");
				long countSuccessNotSettings = usageDataDAO.getPermissionByDate(startDate, sPlatform, "settings",
						"true", "80");
				long countfailureNotSettings = usageDataDAO.getPermissionByDate(startDate, sPlatform, "settings",
						"false", "80");

				if (countSuccessNotHome + countfailureNotHome > 0) {
					reportObj.add(new HomeScreenPermissionReport(sDate, sPlatform, "Notification",
							countSuccessNotHome + "", countfailureNotHome + "", "homeScreen"));
				}
				if (countSuccessNotSettings + countfailureNotSettings > 0) {
					reportObj.add(new HomeScreenPermissionReport(sDate, sPlatform, "Notification",
							countSuccessNotSettings + "", countfailureNotSettings + "", "settings"));
				}

				long countSuccessMedHome = usageDataDAO.getPermissionByDate(startDate, sPlatform, "home", "true", "79");
				long countfailureMedHome = usageDataDAO.getPermissionByDate(startDate, sPlatform, "home", "false",
						"79");
				long countSuccessMedSettings = usageDataDAO.getPermissionByDate(startDate, sPlatform, "settings",
						"true", "79");
				long countfailureMedSettings = usageDataDAO.getPermissionByDate(startDate, sPlatform, "settings",
						"false", "79");

				if (countSuccessMedHome + countfailureMedHome > 0) {
					reportObj.add(new HomeScreenPermissionReport(sDate, sPlatform, "Media",
							countSuccessMedHome + "", countfailureMedHome + "", "homeScreen"));
				}
				if (countSuccessMedSettings + countfailureMedSettings > 0) {
					reportObj.add(new HomeScreenPermissionReport(sDate, sPlatform, "Media",
							countSuccessMedSettings + "", countfailureMedSettings + "", "settings"));
				}

				long countSuccessLocHome = usageDataDAO.getPermissionByDate(startDate, sPlatform, "home", "true", "78");
				long countfailureLocHome = usageDataDAO.getPermissionByDate(startDate, sPlatform, "home", "false",
						"78");
				long countSuccessLocSettings = usageDataDAO.getPermissionByDate(startDate, sPlatform, "location",
						"true", "78");
				long countfailureLocSettings = usageDataDAO.getPermissionByDate(startDate, sPlatform, "location",
						"false", "78");

				if (countSuccessLocHome + countfailureLocHome > 0) {
					reportObj.add(new HomeScreenPermissionReport(sDate, sPlatform, "location",
							countSuccessLocHome + "", countfailureLocHome + "", "homeScreen"));
				}
				if (countSuccessLocSettings + countfailureLocSettings > 0) {
					reportObj.add(new HomeScreenPermissionReport(sDate, sPlatform, "location",
							countSuccessLocSettings + "", countfailureLocSettings + "", "settings"));
				}

				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}

		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getPermissionsReport");
		return new Gson().toJson(reportObj);
	}

	@CrossOrigin
	@RequestMapping(value = "/getHomeCardViewShareReport", method = RequestMethod.POST)
	public String getHomeCardViewShareReport(@RequestBody String request) {
		logger.info("[Enter] getHomeCardViewShareReport");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<HomeScreenViewReport> dataList = new ArrayList<HomeScreenViewReport>();

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				
				HashMap<String, Integer> mtemp = (HashMap<String, Integer>) usageDataDAO
						.getHomeCardTypeViewCountUnique(startDate, sPlatform);
				
				
				for(String key:mtemp.keySet()) {
					
					dataList.add(new HomeScreenViewReport(sDate, sPlatform, key, mtemp.get(key)+"",usageDataDAO.getHomeCardTypeShareCountUnique(startDate, sPlatform, key)+""));			
						
				}
				
				
				/*HashMap<String, Integer> mtemp = (HashMap<String, Integer>) usageDataDAO
						.getHomeCardViewCountUnique(startDate, sPlatform);
				HashMap<String, Integer> targetMap = new HashMap<String, Integer>();

				int count = 0;
				for (String s : mtemp.keySet()) {
					// System.out.println("CARD ID "+s);
					List<HomeScreenCard> retList = (List<HomeScreenCard>) usageDataDAO.getHomeCardShareDetails(s,
							sPlatform);
					Integer PollOption1Count = 0;
					Integer PollOption2Count = 0;
					Integer MCQOption1Count = 0;
					Integer MCQOption2Count = 0;
					Integer MCQOption3Count = 0;
					Integer MCQOption4Count = 0;
					Integer MCQCorrectCount = 0;

					for (HomeScreenCard hsc : retList) {
						// System.out.println(">>>> "+hsc.getCard());
						if (hsc.getCard().equalsIgnoreCase("poll")) {
							if (hsc.getOptionIndex().equals("1")) {
								// System.out.println(">>>> option 1");
								++PollOption1Count;
							} else {
								// System.out.println(">>>> option2");
								++PollOption2Count;
							}
						} else if (hsc.getCard().equalsIgnoreCase("mcq")) {
							// System.out.println(">>>> "+hsc.getCard() + " >> "+hsc.getOptionIndex());
							if (Integer.parseInt(hsc.getOptionIndex().trim()) == 1) {
								// System.out.println(">>>> MCQ1");
								++MCQOption1Count;
							} else if (Integer.parseInt(hsc.getOptionIndex().trim()) == 2) {
								// System.out.println(">>>> MCQ2");
								++MCQOption2Count;
							} else if (Integer.parseInt(hsc.getOptionIndex().trim()) == 3) {
								// System.out.println(">>>> MCQ3");
								++MCQOption3Count;
							} else if (Integer.parseInt(hsc.getOptionIndex().trim()) == 4) {
								// System.out.println(">>>> MCQ4");
								++MCQOption4Count;
							}
							if (hsc.getMCQIsCorrect().equals("correct")) {
								// System.out.println(">>>> MCQ - CORRECT");
								++MCQCorrectCount;
							}

						} else {
							targetMap.put(hsc.getTargetMap(), targetMap.get(hsc.getTargetMap()) == null ? 1
									: (targetMap.get(hsc.getTargetMap()) + 1));
						}
					}
					count++;
					if (mtemp.keySet().size() == (count)) {
						dataList.add(new HomeScreenReport(sDate, sPlatform, retList.get(0).getCard(), mtemp.get(s) + "",
								count + "", retList.get(0).getHomeScreenPosition(), targetMap, PollOption1Count + "",
								PollOption2Count + "", (PollOption1Count + PollOption2Count) + "", MCQOption1Count + "",
								MCQOption2Count + "", MCQOption3Count + "", MCQOption4Count + "",
								MCQCorrectCount + ""));
					}
				}*/
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				
				/*HashMap<String, Integer> mtemp = (HashMap<String, Integer>) usageDataDAO
						.getHomeCardTypeViewCountUnique(startDate, sPlatform);
				
				
				for(String key:mtemp.keySet()) {
					
					dataList.add(new HomeScreenViewReport(sDate, sPlatform, key, mtemp.get(key)+""));					
					
				}*/
				/*HashMap<String, Integer> mtemp = (HashMap<String, Integer>) usageDataDAO
						.getHomeCardViewCountUnique(startDate, sPlatform);
				HashMap<String, Integer> targetMap = new HashMap<String, Integer>();

				int count = 0;
				for (String s : mtemp.keySet()) {
					// System.out.println("CARD ID "+s);
					List<HomeScreenCard> retList = (List<HomeScreenCard>) usageDataDAO.getHomeCardShareDetails(s,
							sPlatform);
					Integer PollOption1Count = 0;
					Integer PollOption2Count = 0;
					Integer MCQOption1Count = 0;
					Integer MCQOption2Count = 0;
					Integer MCQOption3Count = 0;
					Integer MCQOption4Count = 0;
					Integer MCQCorrectCount = 0;

					for (HomeScreenCard hsc : retList) {
						// System.out.println(">>>> "+hsc.getCard());
						if (hsc.getCard().equalsIgnoreCase("poll")) {
							if (hsc.getOptionIndex().equals("1")) {
								// System.out.println(">>>> option 1");
								++PollOption1Count;
							} else {
								// System.out.println(">>>> option2");
								++PollOption2Count;
							}
						} else if (hsc.getCard().equalsIgnoreCase("mcq")) {
							// System.out.println(">>>> "+hsc.getCard() + " >> "+hsc.getOptionIndex());
							if (Integer.parseInt(hsc.getOptionIndex().trim()) == 1) {
								// System.out.println(">>>> MCQ1");
								++MCQOption1Count;
							} else if (Integer.parseInt(hsc.getOptionIndex().trim()) == 2) {
								// System.out.println(">>>> MCQ2");
								++MCQOption2Count;
							} else if (Integer.parseInt(hsc.getOptionIndex().trim()) == 3) {
								// System.out.println(">>>> MCQ3");
								++MCQOption3Count;
							} else if (Integer.parseInt(hsc.getOptionIndex().trim()) == 4) {
								// System.out.println(">>>> MCQ4");
								++MCQOption4Count;
							}
							if (hsc.getMCQIsCorrect().equals("correct")) {
								// System.out.println(">>>> MCQ - CORRECT");
								++MCQCorrectCount;
							}

						} else {
							targetMap.put(hsc.getTargetMap(), targetMap.get(hsc.getTargetMap()) == null ? 1
									: (targetMap.get(hsc.getTargetMap()) + 1));
						}
					}
					count++;
					if (mtemp.keySet().size() == (count)) {
						dataList.add(new HomeScreenReport(sDate, sPlatform, retList.get(0).getCard(), mtemp.get(s) + "",
								count + "", retList.get(0).getHomeScreenPosition(), targetMap, PollOption1Count + "",
								PollOption2Count + "", (PollOption1Count + PollOption2Count) + "", MCQOption1Count + "",
								MCQOption2Count + "", MCQOption3Count + "", MCQOption4Count + "",
								MCQCorrectCount + ""));
					}
				}*/
				
				
				HashMap<String, Integer> mtemp = (HashMap<String, Integer>) usageDataDAO
						.getHomeCardTypeViewCountUnique(startDate, sPlatform);
				
				
				for(String key:mtemp.keySet()) {
					
					dataList.add(new HomeScreenViewReport(sDate, sPlatform, key, mtemp.get(key)+"",usageDataDAO.getHomeCardTypeShareCountUnique(startDate, sPlatform, key)+""));			
						
				}
				
				
				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				
				
				HashMap<String, Integer> mtemp = (HashMap<String, Integer>) usageDataDAO
						.getHomeCardTypeViewCountUnique(startDate, sPlatform);
				
				
				for(String key:mtemp.keySet()) {
					
					dataList.add(new HomeScreenViewReport(sDate, sPlatform, key, mtemp.get(key)+"",usageDataDAO.getHomeCardTypeShareCountUnique(startDate, sPlatform, key)+""));			
						
				}
				

				/*HashMap<String, Integer> mtemp = (HashMap<String, Integer>) usageDataDAO
						.getHomeCardViewCountUnique(startDate, sPlatform);
				
				
				for(String key:mtemp.keySet()) {
					
					dataList.add(new HomeScreenViewReport(sDate, sPlatform, key, mtemp.get(key)+""));					
					
				}
				
				HashMap<String, Integer> targetMap = new HashMap<String, Integer>();

				int count = 0;
				
				if(mtemp==null || mtemp.keySet().size()==0) {
					dataList.add(new HomeScreenReport(sDate, sPlatform, "","0","0","0",null,"0","0","0","0","0","0","0","0"));
					continue;
				}
				
				for (String s : mtemp.keySet()) {
					// System.out.println("CARD ID "+s);
					List<HomeScreenCard> retList = (List<HomeScreenCard>) usageDataDAO.getHomeCardShareDetails(s,
							sPlatform);
					Integer PollOption1Count = 0;
					Integer PollOption2Count = 0;
					Integer MCQOption1Count = 0;
					Integer MCQOption2Count = 0;
					Integer MCQOption3Count = 0;
					Integer MCQOption4Count = 0;
					Integer MCQCorrectCount = 0;

					for (HomeScreenCard hsc : retList) {
						// System.out.println(">>>> "+hsc.getCard());
						if (hsc.getCard().equalsIgnoreCase("poll")) {
							if (hsc.getOptionIndex().equals("1")) {
								// System.out.println(">>>> option 1");
								++PollOption1Count;
							} else {
								// System.out.println(">>>> option2");
								++PollOption2Count;
							}
						} else if (hsc.getCard().equalsIgnoreCase("mcq")) {
							// System.out.println(">>>> "+hsc.getCard() + " >> "+hsc.getOptionIndex());
							try {
								if (Integer.parseInt(hsc.getOptionIndex().trim()) == 1) {
									// System.out.println(">>>> MCQ1");
									++MCQOption1Count;
								} else if (Integer.parseInt(hsc.getOptionIndex().trim()) == 2) {
									// System.out.println(">>>> MCQ2");
									++MCQOption2Count;
								} else if (Integer.parseInt(hsc.getOptionIndex().trim()) == 3) {
									// System.out.println(">>>> MCQ3");
									++MCQOption3Count;
								} else if (Integer.parseInt(hsc.getOptionIndex().trim()) == 4) {
									// System.out.println(">>>> MCQ4");
									++MCQOption4Count;
								}
								if (hsc.getMCQIsCorrect().equals("correct")) {
									// System.out.println(">>>> MCQ - CORRECT");
									++MCQCorrectCount;
								}
							} catch (Exception e) {

							}

						} else {
							try {
								targetMap.put(hsc.getTargetMap(), targetMap.get(hsc.getTargetMap()) == null ? 1
										: (targetMap.get(hsc.getTargetMap()) + 1));

							} catch (Exception E) {

							}
						}
					}
					count++;
					if (mtemp.keySet().size() == (count)) {
						try {
							dataList.add(new HomeScreenReport(sDate, sPlatform, retList.get(0).getCard(),
									mtemp.get(s) + "", count + "", retList.get(0).getHomeScreenPosition(), targetMap,
									PollOption1Count + "", PollOption2Count + "",
									(PollOption1Count + PollOption2Count) + "", MCQOption1Count + "",
									MCQOption2Count + "", MCQOption3Count + "", MCQOption4Count + "",
									MCQCorrectCount + ""));
						} catch (Exception e) {

						}
					}
				}*/
				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getHomeCardViewShareReport");
		return gson.toJson(dataList);
	}

	/**
	 * getUniqueSignupReportByDate : Get unique sign-up report By date
	 * 
	 * @param request {"platform":"All/iOS/Android","startDate":"yyyy-mm-dd","endDate":"yyyy-mm-dd"}
	 * @return {Date , Platform , FB Count(SQL), Mobile Count (SQL), Total Count}
	 */
	@CrossOrigin
	@RequestMapping(value = "/getUniqueSignupReportByDate", method = RequestMethod.POST)
	public String getUniqueFilteredUsageDataByDate(@RequestBody String request) {
		logger.info("[Enter] getUniqueSignupReportByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<PlatformDataByDate> dataList = new ArrayList<PlatformDataByDate>();
		PlatformDataByDate pdAndroid = null;
		PlatformDataByDate pdiOS = null;
		PlatformDataByDate pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new PlatformDataByDate(sDate, sPlatform, repo.findFBUsersByDate(sPlatform, sDate),
						repo.findMobileUsersByDate(sPlatform, sDate));
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new PlatformDataByDate(sDate, sPlatform, repo.findFBUsersByDate(sPlatform, sDate),
						repo.findMobileUsersByDate(sPlatform, sDate));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new PlatformDataByDate(sDate, sPlatform, repo.findFBUsersByDate(sPlatform, sDate),
						repo.findMobileUsersByDate(sPlatform, sDate));
				dataList.add(pdDev);

				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getUniqueSignupReportByDate");
		return gson.toJson(dataList);
	}

	/**
	 * getUserTypeReportByDate : Get User Type [New / Existing] report
	 * 
	 * @param request {"platform":"All/iOS/Android","startDate":"yyyy-mm-dd","endDate":"yyyy-mm-dd"}
	 * @return {Date , Platform , NewUser_Event, NewUser_User, ReturningUser}
	 */
	@CrossOrigin
	@RequestMapping(value = "/getUserTypeReportByDate", method = RequestMethod.POST)
	public String getFilteredUserTypeExtendedByDate(@RequestBody String request) {
		logger.info("[Enter] getUserTypeReportByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		ArrayList<UserTypeExtended> dataList = new ArrayList<UserTypeExtended>();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		UserTypeExtended pdAndroid = null;
		UserTypeExtended pdiOS = null;
		UserTypeExtended pdDev = null;
		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				long newUserCount = repo.findNewUsersCountByDate(sDate, sPlatform);
				long returningUserCount = usageDataDAO.getTotalUserCountByDate(startDate, sPlatform) - newUserCount;

				pdAndroid = new UserTypeExtended(sDate, sPlatform,
						usageDataDAO.getNewUserCountByDate(startDate, sPlatform), newUserCount, returningUserCount);
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				long newUserCount = repo.findNewUsersCountByDate(sDate, sPlatform);
				long returningUserCount = usageDataDAO.getTotalUserCountByDate(startDate, sPlatform)
						- repo.findNewUsersCountByDate(sDate, sPlatform);

				pdiOS = new UserTypeExtended(sDate, sPlatform, usageDataDAO.getNewUserCountByDate(startDate, sPlatform),
						newUserCount, returningUserCount);
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				long newUserCount = repo.findNewUsersCountByDate(sDate, sPlatform);
				long returningUserCount = usageDataDAO.getTotalUserCountByDate(startDate, sPlatform)
						- repo.findNewUsersCountByDate(sDate, sPlatform);

				pdDev = new UserTypeExtended(sDate, sPlatform, usageDataDAO.getNewUserCountByDate(startDate, sPlatform),
						newUserCount, returningUserCount);
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}

		logger.info("[Leave] getUserTypeReportByDate");
		return gson.toJson(dataList);
	}

	/**
	 * getCategoryReportByDate :
	 * 
	 * @param request {"platform":"All/iOS/Android","startDate":"yyyy-mm-dd","endDate":"yyyy-mm-dd"}
	 * @return {Date , Platform , CategoryMap}
	 */
	@CrossOrigin
	@RequestMapping(value = "/getCategoryReportByDate", method = RequestMethod.POST)
	public String getCategoryDataByDate(@RequestBody String request) {
		logger.info("[Enter] getCategoryReportByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		ArrayList<CategoryData> dataList = new ArrayList<CategoryData>();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		CategoryData pdAndroid = null;
		CategoryData pdiOS = null;
		CategoryData pdDev = null;

		HashSet<String> catList = (HashSet<String>) usageDataDAO.getSubCategoryListBetweenDates("Android", startDate,
				endDate);

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				HashMap<String, String> hMap = new HashMap<String, String>();
				for (String cat : catList) {
					long count = usageDataDAO.getSubCategoryCountByDate(startDate, cat, "Android");
					hMap.put(cat, count + "");
				}
				pdAndroid = new CategoryData(sDate, sPlatform, hMap);
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				HashMap<String, String> hMap = new HashMap<String, String>();
				for (String cat : catList) {
					long count = usageDataDAO.getSubCategoryCountByDate(startDate, cat, "iOS");
					hMap.put(cat, +count + "");
				}

				pdiOS = new CategoryData(sDate, sPlatform, hMap);
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				HashMap<String, String> hMap = new HashMap<String, String>();
				for (String cat : catList) {
					long count = usageDataDAO.getSubCategoryCountByDate(startDate, cat, "All");
					hMap.put(cat, count + "");
				}
				pdDev = new CategoryData(sDate, sPlatform, hMap);
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("Invalid Platform");
		}
		List<Map<String, String>> categoryList = new ArrayList<Map<String, String>>();
		for (CategoryData catData : dataList) {
			Map<String, String> catparser = new LinkedHashMap<String, String>();
			catparser.put("Date", catData.getDate());
			catparser.put("Platform", catData.getPlatform());
			for (String key : catData.getCategoryMap().keySet()) {
				catparser.put(key, catData.getCategoryMap().get(key));
			}
			categoryList.add(catparser);
		}
		logger.info("[Enter] getCategoryReportByDate");
		return gson.toJson(categoryList);
	}

	/**
	 * getExtendedKeyboardReportByDateUserLevel
	 * 
	 * @param request {"platform":"All/iOS/Android","startDate":"yyyy-mm-dd","endDate":"yyyy-mm-dd"}
	 * @return {Date , Platform , Init Count, Enable Count, Complete Count, Search
	 *         Count}
	 */
	@CrossOrigin
	@RequestMapping(value = "/getExtendedKeyboardReportByDateUserLevel", method = RequestMethod.POST)
	public String getExtendedKeyboardDataByDateUserLevel(@RequestBody String request) {
		logger.info("[Enter] getExtendedKeyboardReportByDateUserLevel");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<KeyboardDetail> dataList = new ArrayList<KeyboardDetail>();
		KeyboardDetail pdAndroid = null;
		KeyboardDetail pdiOS = null;
		KeyboardDetail pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new KeyboardDetail(sDate, sPlatform,
						usageDataDAO.getKeyboardInitCountByDateUserLevel(startDate, "Android"),
						usageDataDAO.getKeyboardEnableCountByDateUserLevel(startDate, "Android"),
						usageDataDAO.getKeyboardCompleteCountByDateUserLevel(startDate, "Android"),
						usageDataDAO.getKeyboardSearchedCountByDateUserLevel(startDate, "Android"));
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new KeyboardDetail(sDate, sPlatform, usageDataDAO.getKeyboardInitCountByDate(startDate, "iOS"),
						usageDataDAO.getKeyboardEnableCountByDate(startDate, "iOS"),
						usageDataDAO.getKeyboardCompleteCountByDate(startDate, "iOS"),
						usageDataDAO.getKeyboardSearchedCountByDate(startDate, "iOS"));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new KeyboardDetail(sDate, sPlatform, usageDataDAO.getKeyboardInitCountByDate(startDate, "All"),
						usageDataDAO.getKeyboardEnableCountByDate(startDate, "All"),
						usageDataDAO.getKeyboardCompleteCountByDate(startDate, "All"),
						usageDataDAO.getKeyboardSearchedCountByDate(startDate, "All"));
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getExtendedKeyboardReportByDateUserLevel");
		return gson.toJson(dataList);
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	@CrossOrigin
	@RequestMapping(value = "/getExtendedAvtarReportByDate", method = RequestMethod.POST)
	public String getFilteredAvtarDataByDateUserLevel(@RequestBody String request) {
		logger.info("[Enter] getExtendedAvtarReportByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}
		if (sDate == null || eDate == null) {
			logger.error("[ERROR] GET Headers missing");
			return gson.toJson("{\"message\":\"Date-String-Start and Date-String-End Required in GET Header\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<ExtendedAvatarHelper> dataList = new ArrayList<ExtendedAvatarHelper>();
		ExtendedAvatarHelper pdAndroid = null;
		ExtendedAvatarHelper pdiOS = null;
		ExtendedAvatarHelper pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				long created = repo.findAvatarCreatedCountByDate(sDate, sPlatform);
				long created_event = usageDataDAO.findAvatarCreatedByDate(sPlatform, startDate);
				long reset_event = usageDataDAO.findAvatarResetByDate(sPlatform, startDate);
				long reset_user = usageDataDAO.findAvatarResetByDateUnique(sPlatform, startDate);
				long init_event = usageDataDAO.findAvatarInitByDate(sPlatform, startDate);
				long init_user = usageDataDAO.findAvatarInitByDateUserLevel(sPlatform, startDate);

				pdAndroid = new ExtendedAvatarHelper(sDate, sPlatform, init_event, init_user, created_event, created,
						reset_event, reset_user);
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				long created = repo.findAvatarCreatedCountByDate(sDate, sPlatform);
				long created_event = usageDataDAO.findAvatarCreatedByDate(sPlatform, startDate);
				long reset_event = usageDataDAO.findAvatarResetByDate(sPlatform, startDate);
				long reset_user = usageDataDAO.findAvatarResetByDateUnique(sPlatform, startDate);
				long init_event = usageDataDAO.findAvatarInitByDate(sPlatform, startDate);
				long init_user = usageDataDAO.findAvatarInitByDateUserLevel(sPlatform, startDate);

				pdiOS = new ExtendedAvatarHelper(sDate, sPlatform, init_event, init_user, created_event, created,
						reset_event, reset_user);
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				long created = repo.findAvatarCreatedCountByDate(sDate, sPlatform);
				long created_event = usageDataDAO.findAvatarCreatedByDate(sPlatform, startDate);
				long reset_event = usageDataDAO.findAvatarResetByDate(sPlatform, startDate);
				long reset_user = usageDataDAO.findAvatarResetByDateUnique(sPlatform, startDate);
				long init_event = usageDataDAO.findAvatarInitByDate(sPlatform, startDate);
				long init_user = usageDataDAO.findAvatarInitByDateUserLevel(sPlatform, startDate);

				pdDev = new ExtendedAvatarHelper(sDate, sPlatform, init_event, init_user, created_event, created,
						reset_event, reset_user);
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getExtendedAvtarReportByDate");
		return gson.toJson(dataList);
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	@CrossOrigin
	@RequestMapping(value = "/getAppFirstOpenReportByDate", method = RequestMethod.POST)
	public String getAppFirstOpenReportByDate(@RequestBody String request) {
		logger.info("[Enter] getAppFirstOpenReportByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		ArrayList<AppFirstOpenData> dataList = new ArrayList<AppFirstOpenData>();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		AppFirstOpenData pdAndroid = null;
		AppFirstOpenData pdiOS = null;
		AppFirstOpenData pdDev = null;
		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new AppFirstOpenData(sDate, sPlatform,
						usageDataDAO.getNewUserCountByDate(startDate, "Android"),
						repo.findAllUsersCountByDate(sDate, "Android"));
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new AppFirstOpenData(sDate, sPlatform, usageDataDAO.getNewUserCountByDate(startDate, "iOS"),
						repo.findAllUsersCountByDate(sDate, "iOS"));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new AppFirstOpenData(sDate, "All", usageDataDAO.getNewUserCountByDate(startDate, "All"),
						repo.findAllUsersCountByDate(sDate, "All"));
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getAppFirstOpenReportByDate");
		return gson.toJson(dataList);
	}

	/**
	 * 
	 * @param request
	 * @return
	 */

	// 12 download(94,95) + like(92)

	/*@CrossOrigin
	@RequestMapping(value = "/getExtendedStickerWiseShareReportByDate", method = RequestMethod.POST)
	public String getStickerWiseShareReportByDate(@RequestBody String request) {
		logger.info("[Enter] getExtendedStickerWiseShareReportByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		ArrayList<StickerShareData> dataList = new ArrayList<StickerShareData>();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		Date endDate;

		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		ArrayList<String> targetAppList = (ArrayList<String>) usageDataDAO.getAllTargetAppBydate(startDate, endDate);
		StickerShareData pdAndroid = null;
		StickerShareData pdiOS = null;
		StickerShareData pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				List<String> list = usageDataDAO.getAllImageIDsBydate(startDate);
				if (list == null) {
					logger.error("[ERROR] No Content Found");
					continue;
				}
				for (String stickerId : list) {
					Map<String, String> targetCountMap = new LinkedHashMap<String, String>();

					pdAndroid = new StickerShareData();
					pdAndroid.setDate(sDate);
					pdAndroid.setStickerID(stickerId);
					pdAndroid.setPlatform("Android");
					pdAndroid.setDownloaded(usageDataDAO.getImageIDCountDownloaded(stickerId, startDate, sPlatform));
					pdAndroid.setLiked(usageDataDAO.getImageIDCountLiked(stickerId, startDate, sPlatform));
					pdAndroid.setUnLiked(usageDataDAO.getImageIDCountUnLiked(stickerId, startDate, sPlatform));
					pdAndroid = usageDataDAO.getImageIDCountViewed(stickerId, startDate, "Android", pdAndroid);
					pdAndroid = usageDataDAO.getImageIDCountShared(stickerId, startDate, "Android", pdAndroid);
					for (String s : targetAppList) {
						long count = usageDataDAO.getExtendedTargetAppShareCount(s, stickerId, startDate, "Android");
						targetCountMap.put(s, count + "");
					}
					pdAndroid.setTargetMap(targetCountMap);
					if (pdAndroid.getShareCount() + pdAndroid.getViewCount() != 0) {
						dataList.add(pdAndroid);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				List<String> list = usageDataDAO.getAllImageIDsBydate(startDate);

				if (list == null) {
					logger.error("[ERROR] No Content Found");
					continue;
				}

				for (String stickerId : list) {
					Map<String, String> targetCountMap = new LinkedHashMap<String, String>();

					pdiOS = new StickerShareData();
					pdiOS.setDate(sDate);
					pdiOS.setStickerID(stickerId);
					pdiOS.setPlatform("iOS");
					pdiOS.setDownloaded(usageDataDAO.getImageIDCountDownloaded(stickerId, startDate, sPlatform));
					pdiOS.setLiked(usageDataDAO.getImageIDCountLiked(stickerId, startDate, sPlatform));
					pdiOS.setUnLiked(usageDataDAO.getImageIDCountUnLiked(stickerId, startDate, sPlatform));
					pdiOS = usageDataDAO.getImageIDCountViewed(stickerId, startDate, "iOS", pdiOS);
					pdiOS = usageDataDAO.getImageIDCountShared(stickerId, startDate, "iOS", pdiOS);

					for (String s : targetAppList) {
						long count = usageDataDAO.getExtendedTargetAppShareCount(s, stickerId, startDate, "iOS");
						targetCountMap.put(s, count + "");
					}

					pdiOS.setTargetMap(targetCountMap);

					if (pdiOS.getShareCount() + pdiOS.getViewCount() != 0) {
						dataList.add(pdiOS);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				List<String> list = usageDataDAO.getAllImageIDsBydate(startDate);

				if (list == null) {
					logger.error("[ERROR] No Content Found");
					continue;
				}

				for (String stickerId : list) {
					Map<String, String> targetCountMap = new LinkedHashMap<String, String>();
					pdDev = new StickerShareData();
					pdDev.setDate(sDate);
					pdDev.setStickerID(stickerId);
					pdDev.setPlatform("All");
					pdDev.setDownloaded(usageDataDAO.getImageIDCountDownloaded(stickerId, startDate, sPlatform));
					pdDev.setLiked(usageDataDAO.getImageIDCountLiked(stickerId, startDate, sPlatform));
					pdDev.setUnLiked(usageDataDAO.getImageIDCountUnLiked(stickerId, startDate, sPlatform));
					pdDev = usageDataDAO.getImageIDCountViewed(stickerId, startDate, "All", pdDev);
					pdDev = usageDataDAO.getImageIDCountShared(stickerId, startDate, "All", pdDev);
					for (String s : targetAppList) {
						long count = usageDataDAO.getExtendedTargetAppShareCount(s, stickerId, startDate, "All");
						targetCountMap.put(s, count + "");
					}
					pdDev.setTargetMap(targetCountMap);
					if (pdDev.getShareCount() + pdDev.getViewCount() != 0) {
						dataList.add(pdDev);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getExtendedStickerWiseShareReportByDate");
		return gson.toJson(dataList);
	}*/
	
	@CrossOrigin
	@RequestMapping(value = "/getExtendedStickerWiseShareReportByDate", method = RequestMethod.POST)
	public String getStickerWiseShareReportByDate(@RequestBody String request) {
		logger.info("[Enter] getExtendedStickerWiseShareReportByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		ArrayList<StickerShareData> dataList = new ArrayList<StickerShareData>();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		Date endDate;

		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		//ArrayList<String> targetAppList = (ArrayList<String>) usageDataDAO.getAllTargetAppBydate(startDate, endDate);
		StickerShareData pdAndroid = null;
		StickerShareData pdiOS = null;
		StickerShareData pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				List<String> list = usageDataDAO.getAllImageIDsBydate(startDate);
				if (list == null) {
					logger.error("[ERROR] No Content Found");
					continue;
				}
				for (String stickerId : list) {
					//Map<String, String> targetCountMap = new LinkedHashMap<String, String>();

					pdAndroid = new StickerShareData();
					pdAndroid.setDate(sDate);
					pdAndroid.setStickerID(stickerId);
					pdAndroid.setPlatform("Android");
					pdAndroid.setDownloaded(usageDataDAO.getImageIDCountDownloaded(stickerId, startDate, sPlatform));
					pdAndroid.setLiked(usageDataDAO.getImageIDCountLiked(stickerId, startDate, sPlatform));
					pdAndroid.setUnLiked(usageDataDAO.getImageIDCountUnLiked(stickerId, startDate, sPlatform));
					pdAndroid = usageDataDAO.getImageIDCountViewed(stickerId, startDate, "Android", pdAndroid);
					pdAndroid = usageDataDAO.getImageIDCountShared(stickerId, startDate, "Android", pdAndroid);
					/*for (String s : targetAppList) {
						long count = usageDataDAO.getExtendedTargetAppShareCount(s, stickerId, startDate, "Android");
						targetCountMap.put(s, count + "");
					}
					pdAndroid.setTargetMap(targetCountMap);*/
					if (pdAndroid.getShareCount() + pdAndroid.getViewCount() != 0) {
						dataList.add(pdAndroid);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				List<String> list = usageDataDAO.getAllImageIDsBydate(startDate);

				if (list == null) {
					logger.error("[ERROR] No Content Found");
					continue;
				}

				for (String stickerId : list) {
					//Map<String, String> targetCountMap = new LinkedHashMap<String, String>();

					pdiOS = new StickerShareData();
					pdiOS.setDate(sDate);
					pdiOS.setStickerID(stickerId);
					pdiOS.setPlatform("iOS");
					pdiOS.setDownloaded(usageDataDAO.getImageIDCountDownloaded(stickerId, startDate, sPlatform));
					pdiOS.setLiked(usageDataDAO.getImageIDCountLiked(stickerId, startDate, sPlatform));
					pdiOS.setUnLiked(usageDataDAO.getImageIDCountUnLiked(stickerId, startDate, sPlatform));
					pdiOS = usageDataDAO.getImageIDCountViewed(stickerId, startDate, "iOS", pdiOS);
					pdiOS = usageDataDAO.getImageIDCountShared(stickerId, startDate, "iOS", pdiOS);

					/*for (String s : targetAppList) {
						long count = usageDataDAO.getExtendedTargetAppShareCount(s, stickerId, startDate, "iOS");
						targetCountMap.put(s, count + "");
					}

					pdiOS.setTargetMap(targetCountMap);*/

					if (pdiOS.getShareCount() + pdiOS.getViewCount() != 0) {
						dataList.add(pdiOS);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				List<String> list = usageDataDAO.getAllImageIDsBydate(startDate);

				if (list == null) {
					logger.error("[ERROR] No Content Found");
					continue;
				}

				for (String stickerId : list) {
					//Map<String, String> targetCountMap = new LinkedHashMap<String, String>();
					pdDev = new StickerShareData();
					pdDev.setDate(sDate);
					pdDev.setStickerID(stickerId);
					pdDev.setPlatform("All");
					pdDev.setDownloaded(usageDataDAO.getImageIDCountDownloaded(stickerId, startDate, sPlatform));
					pdDev.setLiked(usageDataDAO.getImageIDCountLiked(stickerId, startDate, sPlatform));
					pdDev.setUnLiked(usageDataDAO.getImageIDCountUnLiked(stickerId, startDate, sPlatform));
					pdDev = usageDataDAO.getImageIDCountViewed(stickerId, startDate, "All", pdDev);
					pdDev = usageDataDAO.getImageIDCountShared(stickerId, startDate, "All", pdDev);
					/*for (String s : targetAppList) {
						long count = usageDataDAO.getExtendedTargetAppShareCount(s, stickerId, startDate, "All");
						targetCountMap.put(s, count + "");
					}
					pdDev.setTargetMap(targetCountMap);*/
					if (pdDev.getShareCount() + pdDev.getViewCount() != 0) {
						dataList.add(pdDev);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		
		try{    
	       FileWriter fw=new FileWriter("/bitnami/tomcat/logs/getExtendedStickerWiseShareReportByDate.json");    
           fw.write(gson.toJson(dataList));    
           fw.close();    
          }catch(Exception e){
        	  e.printStackTrace();
          }    

		
		logger.info("[Leave] getExtendedStickerWiseShareReportByDate");
		return gson.toJson(dataList);
	}

	// TEST
	@CrossOrigin
	@RequestMapping(value = "/getUserWiseStickerShareReportByDate", method = RequestMethod.POST)
	public String getStickerUserWiseReportByDate(@RequestBody String request) {
		logger.info("[Enter] getUserWiseStickerShareReportByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		Date endDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}
		ArrayList<StickerShareUserData> dataList = new ArrayList<StickerShareUserData>();
		ArrayList<String> targetAppList = (ArrayList<String>) usageDataDAO.getAllTargetAppBydate(startDate, endDate);

		StickerShareUserData pdAndroid = null;
		StickerShareUserData pdiOS = null;
		StickerShareUserData pdDev = null;

		if (sPlatform.equals("Android")) {
			int index = 0;
			while (true) {
				if (startDate.compareTo(getNextDateObject(endDate)) > 0) {
					break;
				}
				index++;

				String usageDate = "Day " + index;

				ArrayList<String> AllAppList = (ArrayList<String>) usageDataDAO
						.getAllActiveUsersListBetweenDate(sPlatform, startDate, startDate);
				ArrayList<String> newAppList = (ArrayList<String>) repo.findNewUsersListByDate(sDate, sPlatform);
				// Remove New Users
				AllAppList.removeAll(newAppList);
				// Process Returning Users
				for (String ApplicationID : AllAppList) {
					if (ApplicationID == null || ApplicationID.equals("")) {
						logger.error("[ERROR] No Content Found");
						continue;
					}
					LinkedHashMap<String, List<String>> imageMap = usageDataDAO.getAllImageIDsBydateAndAppId(startDate,
							ApplicationID);
					if (imageMap == null) {
						continue;
					}
					for (String stickerId : imageMap.keySet()) {
						Map<String, String> targetCountMap = new LinkedHashMap<String, String>();

						for (String target : targetAppList) {
							long count = usageDataDAO.getUserWiseTargetAppShareCount(ApplicationID, target, stickerId,
									startDate);
							targetCountMap.put(target, count + "");
						}
						pdAndroid = new StickerShareUserData(sDate, imageMap.get(stickerId).get(0), ApplicationID,
								usageDate, imageMap.get(stickerId).get(1), "Returning", imageMap.get(stickerId).get(2),
								imageMap.get(stickerId).get(3), "GET URL", imageMap.get(stickerId).get(4),
								imageMap.get(stickerId).get(5), imageMap.get(stickerId).get(6), targetCountMap);
						dataList.add(pdAndroid);
					}
				}

				// Process New Users
				for (String ApplicationID : newAppList) {
					if (ApplicationID == null || ApplicationID.equals("")) {
						logger.error("[ERROR] No Content Found");
						continue;
					}
					LinkedHashMap<String, List<String>> imageMap = usageDataDAO.getAllImageIDsBydateAndAppId(startDate,
							ApplicationID);
					if (imageMap == null) {
						continue;
					}
					for (String stickerId : imageMap.keySet()) {
						Map<String, String> targetCountMap = new LinkedHashMap<String, String>();

						for (String target : targetAppList) {
							long count = usageDataDAO.getUserWiseTargetAppShareCount(ApplicationID, target, stickerId,
									startDate);
							targetCountMap.put(target, count + "");
						}

						pdAndroid = new StickerShareUserData(sDate, imageMap.get(stickerId).get(0), ApplicationID,
								usageDate, imageMap.get(stickerId).get(1), "New", imageMap.get(stickerId).get(2),
								imageMap.get(stickerId).get(3), "GET URL", imageMap.get(stickerId).get(4),
								imageMap.get(stickerId).get(5), imageMap.get(stickerId).get(6), targetCountMap);
						dataList.add(pdAndroid);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			int index = 0;
			while (true) {
				if (startDate.compareTo(getNextDateObject(endDate)) > 0) {
					break;
				}
				index++;
				String usageDate = "Day " + index;
				ArrayList<String> AllAppList = (ArrayList<String>) usageDataDAO
						.getAllActiveUsersListBetweenDate(sPlatform, startDate, startDate);
				ArrayList<String> newAppList = (ArrayList<String>) repo.findNewUsersListByDate(sDate, sPlatform);
				// Remove New Users
				AllAppList.removeAll(newAppList);
				// Process Returning Users
				for (String ApplicationID : AllAppList) {
					if (ApplicationID == null || ApplicationID.equals("")) {
						logger.error("[ERROR] No Content Found");
						continue;
					}
					LinkedHashMap<String, List<String>> imageMap = usageDataDAO.getAllImageIDsBydateAndAppId(startDate,
							ApplicationID);
					if (imageMap == null) {
						continue;
					}
					for (String stickerId : imageMap.keySet()) {
						Map<String, String> targetCountMap = new LinkedHashMap<String, String>();
						for (String target : targetAppList) {
							long count = usageDataDAO.getUserWiseTargetAppShareCount(ApplicationID, target, stickerId,
									startDate);
							targetCountMap.put(target, count + "");
						}
						pdiOS = new StickerShareUserData(sDate, imageMap.get(stickerId).get(0), ApplicationID,
								usageDate, imageMap.get(stickerId).get(1), "Returning", imageMap.get(stickerId).get(2),
								imageMap.get(stickerId).get(3), "GET URL", imageMap.get(stickerId).get(4),
								imageMap.get(stickerId).get(5), imageMap.get(stickerId).get(6), targetCountMap);
						dataList.add(pdiOS);
					}
				}

				// Process New Users
				for (String ApplicationID : newAppList) {
					if (ApplicationID == null || ApplicationID.equals("")) {
						logger.error("[ERROR] No Content Found");
						continue;
					}
					LinkedHashMap<String, List<String>> imageMap = usageDataDAO.getAllImageIDsBydateAndAppId(startDate,
							ApplicationID);
					if (imageMap == null) {
						continue;
					}
					for (String stickerId : imageMap.keySet()) {
						Map<String, String> targetCountMap = new LinkedHashMap<String, String>();
						for (String target : targetAppList) {
							long count = usageDataDAO.getUserWiseTargetAppShareCount(ApplicationID, target, stickerId,
									startDate);
							targetCountMap.put(target, count + "");
						}
						pdiOS = new StickerShareUserData(sDate, imageMap.get(stickerId).get(0), ApplicationID,
								usageDate, imageMap.get(stickerId).get(1), "New", imageMap.get(stickerId).get(2),
								imageMap.get(stickerId).get(3), "GET URL", imageMap.get(stickerId).get(4),
								imageMap.get(stickerId).get(5), imageMap.get(stickerId).get(6), targetCountMap);
						dataList.add(pdiOS);
					}
				}

				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			int index = 0;
			while (true) {
				if (startDate.compareTo(getNextDateObject(endDate)) > 0) {
					break;
				}
				index++;
				String usageDate = "Day " + index;
				ArrayList<String> AllAppList = (ArrayList<String>) usageDataDAO
						.getAllActiveUsersListBetweenDate(sPlatform, startDate, startDate);
				ArrayList<String> newAppList = (ArrayList<String>) repo.findNewUsersListByDate(sDate, sPlatform);
				// Remove New Users
				AllAppList.removeAll(newAppList);

				// Process Returning Users
				for (String ApplicationID : AllAppList) {
					if (ApplicationID == null || ApplicationID.equals("")) {
						logger.error("[ERROR] No Content Found");
						continue;
					}
					LinkedHashMap<String, List<String>> imageMap = usageDataDAO.getAllImageIDsBydateAndAppId(startDate,
							ApplicationID);
					if (imageMap == null) {
						continue;
					}
					for (String stickerId : imageMap.keySet()) {
						Map<String, String> targetCountMap = new LinkedHashMap<String, String>();
						for (String target : targetAppList) {
							long count = usageDataDAO.getUserWiseTargetAppShareCount(ApplicationID, target, stickerId,
									startDate);
							targetCountMap.put(target, count + "");
						}

						pdDev = new StickerShareUserData(sDate, imageMap.get(stickerId).get(0), ApplicationID,
								usageDate, imageMap.get(stickerId).get(1), "Returning", imageMap.get(stickerId).get(2),
								imageMap.get(stickerId).get(3), "GET URL", imageMap.get(stickerId).get(4),
								imageMap.get(stickerId).get(5), imageMap.get(stickerId).get(6), targetCountMap);
						dataList.add(pdDev);
					}
				}

				// Process New Users
				for (String ApplicationID : newAppList) {
					if (ApplicationID == null || ApplicationID.equals("")) {
						logger.error("[ERROR] No Content Found");
						continue;
					}
					LinkedHashMap<String, List<String>> imageMap = usageDataDAO.getAllImageIDsBydateAndAppId(startDate,
							ApplicationID);
					if (imageMap == null) {
						continue;
					}
					for (String stickerId : imageMap.keySet()) {
						Map<String, String> targetCountMap = new LinkedHashMap<String, String>();
						for (String target : targetAppList) {
							long count = usageDataDAO.getUserWiseTargetAppShareCount(ApplicationID, target, stickerId,
									startDate);
							targetCountMap.put(target, count + "");
						}
						pdDev = new StickerShareUserData(sDate, imageMap.get(stickerId).get(0), ApplicationID,
								usageDate, imageMap.get(stickerId).get(1), "New", imageMap.get(stickerId).get(2),
								imageMap.get(stickerId).get(3), "GET URL", imageMap.get(stickerId).get(4),
								imageMap.get(stickerId).get(5), imageMap.get(stickerId).get(6), targetCountMap);
						dataList.add(pdDev);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		
		try{    
		       FileWriter fw=new FileWriter("/bitnami/tomcat/logs/getUserWiseStickerShareReportByDate.json");    
	           fw.write(gson.toJson(dataList));    
	           fw.close();    
	          }catch(Exception e){
	        	  e.printStackTrace();
	          }  
		
		logger.info("[Leave] getUserWiseStickerShareReportByDate");
		return gson.toJson(dataList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getkeywordSearchReportByDate", method = RequestMethod.POST)
	public String getkeywordSearchReport(@RequestBody String request) {
		logger.info("[Enter] getkeywordSearchReportByDate");
		ArrayList<KeywordSearch> dataList = new ArrayList<KeywordSearch>();
		Gson gson = new Gson();
		String sPlatform;
		String sDate = null;
		String eDate = null;

		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String backupStartDate = sDate;
		String backupEndDate = eDate;
		Date startDate;
		Date endDate;

		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}
		KeywordSearch pdAndroid = null;
		KeywordSearch pdiOS = null;
		KeywordSearch pdDev = null;
		sDate = backupStartDate;
		eDate = backupEndDate;
		try {
			startDate = formatter.parse(backupStartDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		try {
			endDate = formatter.parse(backupEndDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}
		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Map<String, Integer> keywordsList = usageDataDAO.getAllKeywordsByDate(startDate, "Android");
				for (String key : keywordsList.keySet()) {
					pdAndroid = new KeywordSearch(sDate, key, keywordsList.get(key));
					if (pdAndroid.getCount() > 0) {
						dataList.add(pdAndroid);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Map<String, Integer> keywordsList = usageDataDAO.getAllKeywordsByDate(startDate, "iOS");
				for (String key : keywordsList.keySet()) {
					pdiOS = new KeywordSearch(sDate, key, keywordsList.get(key));
					if (pdiOS.getCount() > 0) {
						dataList.add(pdiOS);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Map<String, Integer> keywordsList = usageDataDAO.getAllKeywordsByDate(startDate, "All");
				for (String key : keywordsList.keySet()) {
					pdDev = new KeywordSearch(sDate, key, keywordsList.get(key));
					if (pdDev.getCount() > 0) {
						dataList.add(pdDev);
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getkeywordSearchReportByDate");
		return gson.toJson(dataList);
	}

	
	@CrossOrigin
	@RequestMapping(value = "/getLanguageReportByDate", method = RequestMethod.POST)
	public String getLanguageDataByDate(@RequestBody String request) {
		logger.info("[Enter] getLanguageReportByDate");
		/*String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<LanguageData> dataList = new ArrayList<LanguageData>();
		LanguageData pdAndroid = null;
		LanguageData pdiOS = null;
		LanguageData pdDev = null;
		
		
		ArrayList<String> uqLang = repo.findUniqueLanguages();

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				
				Map<String,String> mLang = new HashMap<String,String>();
				
				for(String s:uqLang) {
					if(s == null || s.equalsIgnoreCase("")) {
						continue;
					}					
					if(s.equalsIgnoreCase("English")) {
						mLang.put(s,repo.findSettingsLanguageCount(sDate,"English")+"");
					}else {
						mLang.put(s,repo.findSettingsLanguageCount(sDate, s)+"");
					}					
				}		
				
				pdAndroid = new LanguageData(sDate, sPlatform,
						mLang,
						repo.findSettingsGenderCount(sDate,"Male"),
						repo.findSettingsGenderCount(sDate,"Female"));
				
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Map<String,String> mLang = new HashMap<String,String>();
				
				for(String s:uqLang) {
					if(s == null || s.equalsIgnoreCase("")) {
						continue;
					}					
					if(s.equalsIgnoreCase("English")) {
						mLang.put(s,repo.findSettingsLanguageCount(sDate,"English")+"");
					}else {
						mLang.put(s,repo.findSettingsLanguageCount(sDate, s)+"");
					}					
				}		
				
				pdiOS = new LanguageData(sDate, sPlatform,
						mLang,
						repo.findSettingsGenderCount(sDate,"Male"),
						repo.findSettingsGenderCount(sDate,"Female"));
				
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Map<String,String> mLang = new HashMap<String,String>();
				
				for(String s:uqLang) {
					if(s == null || s.equalsIgnoreCase("")) {
						continue;
					}					
					if(s.equalsIgnoreCase("English")) {
						mLang.put(s,repo.findSettingsLanguageCount(sDate,"English")+"");
					}else {
						mLang.put(s,repo.findSettingsLanguageCount(sDate, s)+"");
					}					
				}		
				
				pdDev = new LanguageData(sDate, sPlatform,
						mLang,
						repo.findSettingsGenderCount(sDate,"Male"),
						repo.findSettingsGenderCount(sDate,"Female"));
				
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}*/
		
		//logger.info("[Enter] getActiveUsersDetailedReport");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		ArrayList<LanguageData> dataList = new ArrayList<LanguageData>();		
		ArrayList<String> languageList = repo.findUniqueLanguages();	

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}				
				ArrayList<String> totalAppId = usageDataDAO.getTotalActiveAppIDByDate(sPlatform, startDate);
				//System.out.println("Toatl "+totalAppId.size());
				ArrayList<String> newAppId = repo.findNewUsersAppIDByDate(sDate, sPlatform);
				//System.out.println("New "+newAppId.size());
				//Returning APP IDs
				//totalAppId.removeAll(newAppId);	
				
				for(String s:newAppId) {
					if(totalAppId.contains(s)) {
						totalAppId.remove(s);
					}else {
						//System.out.println("Not Found");
					}
				}
								
				//System.out.println("Returning "+totalAppId.size());
				
				long newUserCount = newAppId.size();
				long returningUserCount = totalAppId.size();
				
				HashMap<String, Integer> targetMap = new HashMap<String, Integer>();
				
				for(String s:languageList) {			
					if(s==null||s.equalsIgnoreCase("")) {
						continue;
					}					
					targetMap.put(s.toLowerCase(), 0);
				}
				
				
				//New User
				long countMale = 0;
				long countFemale = 0;			
				
				for(String s:newAppId) {
					
					if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
						newUserCount--;
						continue;
					}
					
					String currentGender = repo.findGenderLanguageByAppId(s).get(0);
					String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
					
					
					//System.out.println("Current "+currentGender+"   "+currentLanguage);
						
					//Update Gender Count
					if(currentGender.trim().equalsIgnoreCase("Female")) {
						countFemale++;
					}else {
						countMale++;
					}
					
					//Update language count
					Integer languageCount = targetMap.get(currentLanguage.toLowerCase());
					targetMap.put(currentLanguage.toLowerCase(),(languageCount+1));					
				}
				
				//RETURNING
				
				//dataList.add(new LanguageData(sDate,sPlatform,targetMap,countMale,countFemale));				
				
				
				
				
				
				/*HashMap<String, Integer> targetMapReturning = new HashMap<String, Integer>();
				
				for(String s:languageList) {			
					if(s==null||s.equalsIgnoreCase("")) {
						continue;
					}
					//System.out.println("Adding Language >"+s+"<<<");
					targetMapReturning.put(s.toLowerCase(), 0);
				}*/
				
				//Returning User
				/*countMale = 0;
				countFemale = 0;*/
				
				for(String s:totalAppId) {
					
					if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
						returningUserCount--;
						continue;
					}
					
					String currentGender = repo.findGenderLanguageByAppId(s).get(0);
					String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
						
					//Update Gender Count
					if(currentGender.trim().equalsIgnoreCase("Female")) {
						countFemale++;
					}else {
						countMale++;
					}
					
					//Update language count
					Integer languageCount = targetMap.get(currentLanguage.toLowerCase());					
					targetMap.put(currentLanguage.toLowerCase(),(languageCount+1));		
				}	
				
				dataList.add(new LanguageData(sDate,sPlatform,targetMap,countMale,countFemale));
				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				ArrayList<String> totalAppId = usageDataDAO.getTotalActiveAppIDByDate(sPlatform, startDate);
				//System.out.println("Toatl "+totalAppId.size());
				ArrayList<String> newAppId = repo.findNewUsersAppIDByDate(sDate, sPlatform);
				//System.out.println("New "+newAppId.size());
				//Returning APP IDs
				//totalAppId.removeAll(newAppId);	
				
				for(String s:newAppId) {
					if(totalAppId.contains(s)) {
						totalAppId.remove(s);
					}else {
						//System.out.println("Not Found");
					}
				}
								
				//System.out.println("Returning "+totalAppId.size());
				
				long newUserCount = newAppId.size();
				long returningUserCount = totalAppId.size();
				
				HashMap<String, Integer> targetMap = new HashMap<String, Integer>();
				
				for(String s:languageList) {			
					if(s==null||s.equalsIgnoreCase("")) {
						continue;
					}					
					targetMap.put(s.toLowerCase(), 0);
				}
				
				
				//New User
				long countMale = 0;
				long countFemale = 0;			
				
				for(String s:newAppId) {
					
					if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
						newUserCount--;
						continue;
					}
					
					String currentGender = repo.findGenderLanguageByAppId(s).get(0);
					String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
					
					
					//System.out.println("Current "+currentGender+"   "+currentLanguage);
						
					//Update Gender Count
					if(currentGender.trim().equalsIgnoreCase("Female")) {
						countFemale++;
					}else {
						countMale++;
					}
					
					//Update language count
					Integer languageCount = targetMap.get(currentLanguage.toLowerCase());
					targetMap.put(currentLanguage.toLowerCase(),(languageCount+1));					
				}
				
				//RETURNING
				
				//dataList.add(new LanguageData(sDate,sPlatform,targetMap,countMale,countFemale));				
				
				
				
				
				
				/*HashMap<String, Integer> targetMapReturning = new HashMap<String, Integer>();
				
				for(String s:languageList) {			
					if(s==null||s.equalsIgnoreCase("")) {
						continue;
					}
					//System.out.println("Adding Language >"+s+"<<<");
					targetMapReturning.put(s.toLowerCase(), 0);
				}*/
				
				//Returning User
				/*countMale = 0;
				countFemale = 0;*/
				
				for(String s:totalAppId) {
					
					if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
						returningUserCount--;
						continue;
					}
					
					String currentGender = repo.findGenderLanguageByAppId(s).get(0);
					String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
						
					//Update Gender Count
					if(currentGender.trim().equalsIgnoreCase("Female")) {
						countFemale++;
					}else {
						countMale++;
					}
					
					//Update language count
					Integer languageCount = targetMap.get(currentLanguage.toLowerCase());					
					targetMap.put(currentLanguage.toLowerCase(),(languageCount+1));		
				}	
				
				dataList.add(new LanguageData(sDate,sPlatform,targetMap,countMale,countFemale));
				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}				
				ArrayList<String> totalAppId = usageDataDAO.getTotalActiveAppIDByDate(sPlatform, startDate);
				//System.out.println("Toatl "+totalAppId.size());
				ArrayList<String> newAppId = repo.findNewUsersAppIDByDate(sDate, sPlatform);
				//System.out.println("New "+newAppId.size());
				//Returning APP IDs
				//totalAppId.removeAll(newAppId);	
				
				for(String s:newAppId) {
					if(totalAppId.contains(s)) {
						totalAppId.remove(s);
					}else {
						//System.out.println("Not Found");
					}
				}
								
				//System.out.println("Returning "+totalAppId.size());
				
				long newUserCount = newAppId.size();
				long returningUserCount = totalAppId.size();
				
				HashMap<String, Integer> targetMap = new HashMap<String, Integer>();
				
				for(String s:languageList) {			
					if(s==null||s.equalsIgnoreCase("")) {
						continue;
					}					
					targetMap.put(s.toLowerCase(), 0);
				}
				
				
				//New User
				long countMale = 0;
				long countFemale = 0;			
				
				for(String s:newAppId) {
					
					if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
						newUserCount--;
						continue;
					}
					
					String currentGender = repo.findGenderLanguageByAppId(s).get(0);
					String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
					
					
					//System.out.println("Current "+currentGender+"   "+currentLanguage);
						
					//Update Gender Count
					if(currentGender.trim().equalsIgnoreCase("Female")) {
						countFemale++;
					}else {
						countMale++;
					}
					
					//Update language count
					Integer languageCount = targetMap.get(currentLanguage.toLowerCase());
					targetMap.put(currentLanguage.toLowerCase(),(languageCount+1));					
				}
				
				//RETURNING
				
				//dataList.add(new LanguageData(sDate,sPlatform,targetMap,countMale,countFemale));				
				
				
				
				
				
				/*HashMap<String, Integer> targetMapReturning = new HashMap<String, Integer>();
				
				for(String s:languageList) {			
					if(s==null||s.equalsIgnoreCase("")) {
						continue;
					}
					//System.out.println("Adding Language >"+s+"<<<");
					targetMapReturning.put(s.toLowerCase(), 0);
				}*/
				
				//Returning User
				/*countMale = 0;
				countFemale = 0;*/
				
				for(String s:totalAppId) {
					
					if(repo.findGenderLanguageByAppId(s)==null || repo.findGenderLanguageByAppId(s).size()<2) {
						returningUserCount--;
						continue;
					}
					
					String currentGender = repo.findGenderLanguageByAppId(s).get(0);
					String currentLanguage = repo.findGenderLanguageByAppId(s).get(1);
						
					//Update Gender Count
					if(currentGender.trim().equalsIgnoreCase("Female")) {
						countFemale++;
					}else {
						countMale++;
					}
					
					//Update language count
					Integer languageCount = targetMap.get(currentLanguage.toLowerCase());					
					targetMap.put(currentLanguage.toLowerCase(),(languageCount+1));		
				}	
				
				dataList.add(new LanguageData(sDate,sPlatform,targetMap,countMale,countFemale));
				
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		/*logger.info("[Leave] getActiveUsersDetailedReport");
		return gson.toJson(dataList);*/
		
		
		
		logger.info("[Leave] getLanguageReportByDate");
		return gson.toJson(dataList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getShareTargetReportByDate", method = RequestMethod.POST)
	public String getTargetApplikcationDataByDate(@RequestBody String request) {
		logger.info("[Enter] getShareTargetReportByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		ArrayList<ShareTargetData> dataList = new ArrayList<ShareTargetData>();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		Date endDate;
		List<String> list = null;
		try {
			list = usageDataDAO.getAllTargetAppBydate(formatter.parse(sDate), formatter.parse(eDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		String backupStartDate = sDate;
		String backupEndDate = eDate;

		if (list == null) {
			logger.error("[ERROR] No Content Found");
			return null;
		}

		for (String targetApp : list) {
			if (targetApp == null || targetApp.equals("")) {
				continue;
			}
			ShareTargetData pdAndroid = null;
			ShareTargetData pdiOS = null;
			ShareTargetData pdDev = null;
			sDate = backupStartDate;
			eDate = backupEndDate;
			try {
				startDate = formatter.parse(backupStartDate);
			} catch (ParseException e1) {
				logger.error("[ERROR] DATE not parsable");
				e1.printStackTrace();
				return gson.toJson("Invalid Date");
			}
			try {
				endDate = formatter.parse(backupEndDate);
			} catch (ParseException e1) {
				e1.printStackTrace();
				logger.error("[ERROR] DATE not parsable");
				return gson.toJson("Invalid Date");
			}
			if (sPlatform.equals("Android")) {
				while (true) {
					if (startDate.compareTo(endDate) > 0) {
						break;
					}

					long Count = usageDataDAO.getTargetAppShareCount(targetApp, startDate, "Android");
					pdAndroid = new ShareTargetData(sDate, "Android", targetApp, Count);

					if (Count != 0) {
						dataList.add(pdAndroid);
					}
					sDate = getNextDate(sDate);
					startDate = getNextDateObject(startDate);
				}
			} else if (sPlatform.equals("iOS")) {
				while (true) {
					if (startDate.compareTo(endDate) > 0) {
						break;
					}
					long Count = usageDataDAO.getTargetAppShareCount(targetApp, startDate, "iOS");
					pdiOS = new ShareTargetData(sDate, "iOS", targetApp, Count);

					if (Count != 0) {
						dataList.add(pdiOS);
					}
					sDate = getNextDate(sDate);
					startDate = getNextDateObject(startDate);
				}
			} else if (sPlatform.toUpperCase().equals("ALL")) {
				while (true) {
					if (startDate.compareTo(endDate) > 0) {
						break;
					}
					long Count = usageDataDAO.getTargetAppShareCount(targetApp, startDate, "All");
					pdDev = new ShareTargetData(sDate, "All", targetApp, Count);

					if (Count != 0) {
						dataList.add(pdDev);
					}
					sDate = getNextDate(sDate);
					startDate = getNextDateObject(startDate);
				}
			} else {
				logger.error("[ERROR] Invalid Platform");
				return gson.toJson("{\"message\":\"Invalid Platform\"}");
			}
		}
		logger.info("[Leave] getShareTargetReportByDate");
		return gson.toJson(dataList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getSignupMobileReportByDate", method = RequestMethod.POST)
	public String getSignupMobileDataByDate(@RequestBody String request) {
		logger.info("[Enter] getSignupMobileReportByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<SignUpMobileData> dataList = new ArrayList<SignUpMobileData>();
		SignUpMobileData pdAndroid = null;
		SignUpMobileData pdiOS = null;
		SignUpMobileData pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new SignUpMobileData(sDate, sPlatform,
						usageDataDAO.getMobileUserInitReportByDate("Android", startDate),
						repo.findMobileUsersByDate("Android", sDate),
						usageDataDAO.getMobileUserResetOTPReportByDate("Android", startDate));
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new SignUpMobileData(sDate, sPlatform,
						usageDataDAO.getMobileUserInitReportByDate("iOS", startDate),
						repo.findMobileUsersByDate("iOS", sDate),
						usageDataDAO.getMobileUserResetOTPReportByDate("iOS", startDate));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new SignUpMobileData(sDate, sPlatform,
						usageDataDAO.getMobileUserInitReportByDate("All", startDate),
						repo.findMobileUsersByDate("All", sDate),
						usageDataDAO.getMobileUserResetOTPReportByDate("All", startDate));
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getSignupMobileReportByDate");
		return gson.toJson(dataList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getSignupFBReportByDate", method = RequestMethod.POST)
	public String getSignupFBDataByDate(@RequestBody String request) {
		logger.info("[Enter] getSignupFBReportByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<SignUpFBData> dataList = new ArrayList<SignUpFBData>();
		SignUpFBData pdAndroid = null;
		SignUpFBData pdiOS = null;
		SignUpFBData pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new SignUpFBData(sDate, sPlatform,
						usageDataDAO.getFBUserInitReportByDate("Android", startDate),
						repo.findFBUsersByDate("Android", sDate));
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new SignUpFBData(sDate, sPlatform, usageDataDAO.getFBUserInitReportByDate("iOS", startDate),
						repo.findFBUsersByDate("iOS", sDate));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new SignUpFBData(sDate, sPlatform, usageDataDAO.getFBUserInitReportByDate("All", startDate),
						repo.findFBUsersByDate("All", sDate));
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getSignupFBReportByDate");
		return gson.toJson(dataList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getNotificationViewReportByDate", method = RequestMethod.POST)
	public String getNotificationViewDataByDate(@RequestBody String request) {
		logger.info("[Enter] getNotificationViewReportByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<NotificationData> dataList = new ArrayList<NotificationData>();
		NotificationData pdAndroid = null;
		NotificationData pdiOS = null;
		NotificationData pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new NotificationData(sDate, sPlatform, repo.getAllNotificationCreatedCount(sDate),
						usageDataDAO.getNotificationViewCountByDate(startDate, "Android"),
						repo.getAllNotificationSentCount(sDate, "Android"),
						repo.getNotificationSentCount(sDate, "App first open", "Android"),
						repo.getNotificationSentCount(sDate, "New Feature", "Android"),
						repo.getNotificationSentCount(sDate, "New Avatar Accessories", "Android"),
						repo.getNotificationSentCount(sDate, "AD-Update", "Android"),
						repo.getNotificationSentCount(sDate, "Create Avatar", "Android"),
						repo.getNotificationSentCount(sDate, "Avatar Completion Reminder", "Android"),
						repo.getNotificationSentCount(sDate, "App Rating/Review", "Android"),
						repo.getNotificationSentCount(sDate, "Sign Up", "Android"),
						repo.getNotificationSentCount(sDate, "Connect Keyboard", "Android"),
						repo.getNotificationSentCount(sDate, "Daily/Weekly Trending Stickers", "Android"),
						repo.getNotificationSentCount(sDate, "Unexplored Sticker Category", "Android"),
						repo.getNotificationSentCount(sDate, "Sticker pack", "Android"),
						repo.getNotificationSentCount(sDate, "Sticker of the day", "Android"),
						repo.getNotificationSentCount(sDate, "New Sticker based on region", "Android"),
						repo.getNotificationSentCount(sDate, "New App language", "Android"),
						repo.getNotificationSentCount(sDate, "New KB App language", "Android"),
						repo.getNotificationSentCount(sDate, "Region based notification", "Android"),
						repo.getNotificationSentCount(sDate, "Others", "Android"),
						repo.getNotificationSentCount(sDate, "WelcomeNotification", "Android"),
						repo.getNotificationSentCount(sDate, "News", "Android"));
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new NotificationData(sDate, sPlatform, repo.getAllNotificationCreatedCount(sDate),
						usageDataDAO.getNotificationViewCountByDate(startDate, "iOS"),
						repo.getAllNotificationSentCount(sDate, "iOS"),
						repo.getNotificationSentCount(sDate, "App first open", "iOS"),
						repo.getNotificationSentCount(sDate, "New Feature", "iOS"),
						repo.getNotificationSentCount(sDate, "New Avatar Accessories", "iOS"),
						repo.getNotificationSentCount(sDate, "AD-Update", "iOS"),
						repo.getNotificationSentCount(sDate, "Create Avatar", "iOS"),
						repo.getNotificationSentCount(sDate, "Avatar Completion Reminder", "iOS"),
						repo.getNotificationSentCount(sDate, "App Rating/Review", "iOS"),
						repo.getNotificationSentCount(sDate, "Sign Up", "iOS"),
						repo.getNotificationSentCount(sDate, "Connect Keyboard", "iOS"),
						repo.getNotificationSentCount(sDate, "Daily/Weekly Trending Stickers", "iOS"),
						repo.getNotificationSentCount(sDate, "Unexplored Sticker Category", "iOS"),
						repo.getNotificationSentCount(sDate, "Sticker pack", "iOS"),
						repo.getNotificationSentCount(sDate, "Sticker of the day", "iOS"),
						repo.getNotificationSentCount(sDate, "New Sticker based on region", "iOS"),
						repo.getNotificationSentCount(sDate, "New App language", "iOS"),
						repo.getNotificationSentCount(sDate, "New KB App language", "iOS"),
						repo.getNotificationSentCount(sDate, "Region based notification", "iOS"),
						repo.getNotificationSentCount(sDate, "Others", "iOS"),
						repo.getNotificationSentCount(sDate, "WelcomeNotification", "iOS"),
						repo.getNotificationSentCount(sDate, "News", "iOS"));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new NotificationData(sDate, sPlatform, repo.getAllNotificationCreatedCount(sDate),
						usageDataDAO.getNotificationViewCountByDate(startDate, "All"),
						repo.getAllNotificationSentCount(sDate, "All"),
						repo.getNotificationSentCount(sDate, "App first open", "All"),
						repo.getNotificationSentCount(sDate, "New Feature", "All"),
						repo.getNotificationSentCount(sDate, "New Avatar Accessories", "All"),
						repo.getNotificationSentCount(sDate, "AD-Update", "All"),
						repo.getNotificationSentCount(sDate, "Create Avatar", "All"),
						repo.getNotificationSentCount(sDate, "Avatar Completion Reminder", "All"),
						repo.getNotificationSentCount(sDate, "App Rating/Review", "All"),
						repo.getNotificationSentCount(sDate, "Sign Up", "All"),
						repo.getNotificationSentCount(sDate, "Connect Keyboard", "All"),
						repo.getNotificationSentCount(sDate, "Daily/Weekly Trending Stickers", "All"),
						repo.getNotificationSentCount(sDate, "Unexplored Sticker Category", "All"),
						repo.getNotificationSentCount(sDate, "Sticker pack", "All"),
						repo.getNotificationSentCount(sDate, "Sticker of the day", "All"),
						repo.getNotificationSentCount(sDate, "New Sticker based on region", "All"),
						repo.getNotificationSentCount(sDate, "New App language", "All"),
						repo.getNotificationSentCount(sDate, "New KB App language", "All"),
						repo.getNotificationSentCount(sDate, "Region based notification", "All"),
						repo.getNotificationSentCount(sDate, "Others", "All"),
						repo.getNotificationSentCount(sDate, "WelcomeNotification", "All"),
						repo.getNotificationSentCount(sDate, "News", "All"));
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getNotificationViewReportByDate");
		return gson.toJson(dataList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getReadNotificationReportByDateNoExcl", method = RequestMethod.POST)
	public String getReducedNotificationViewReportByDateNoExcl(@RequestBody String request) {
		logger.info("[Enter] getReadNotificationReportByDateNoExcl");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<ReducedNotificationData> dataList = new ArrayList<ReducedNotificationData>();
		ReducedNotificationData pdAndroid = null;
		ReducedNotificationData pdiOS = null;
		ReducedNotificationData pdDev = null;
		Map<String, Integer> resultMap = new HashMap<String, Integer>();

		// Counts
		int NewsCount = 0;
		int Promotions = 0;
		int WelcomeNotification = 0;
		int AvatarCompletionReminder = 0;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				NewsCount = 0;
				Promotions = 0;
				WelcomeNotification = 0;
				AvatarCompletionReminder = 0;
				resultMap = usageDataDAO.getNotificationReadCountByNidNoExclusion(startDate, "Android");

				for (String s : resultMap.keySet()) {
					if (s.equals("1")) {
						AvatarCompletionReminder += resultMap.get(s);
						continue;
					} else if (s.equals("2")) {
						WelcomeNotification += resultMap.get(s);
						continue;
					} else {
						String type = repo.getTypeForNid(s);

						if (type != null) {
							if (type.equalsIgnoreCase("News")) {
								NewsCount += resultMap.get(s);
								continue;
							} else if (type.equalsIgnoreCase("Promotions")) {
								Promotions += resultMap.get(s);
								continue;
							}
						}
					}
				}
				pdAndroid = new ReducedNotificationData(sDate, sPlatform,
						repo.getAllNotificationSentCount(sDate, "Android"),
						usageDataDAO.getNotificationViewCountByDate(startDate, "Android"), NewsCount, Promotions,
						WelcomeNotification, AvatarCompletionReminder);
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				NewsCount = 0;
				Promotions = 0;
				WelcomeNotification = 0;
				AvatarCompletionReminder = 0;

				resultMap = usageDataDAO.getNotificationReadCountByNidNoExclusion(startDate, "iOS");

				for (String s : resultMap.keySet()) {
					if (s.equals("1")) {
						AvatarCompletionReminder += resultMap.get(s);
						continue;
					} else if (s.equals("2")) {
						WelcomeNotification += resultMap.get(s);
						continue;
					} else {
						String type = repo.getTypeForNid(s);
						if (type != null) {
							if (type.equalsIgnoreCase("News")) {
								NewsCount += resultMap.get(s);
								continue;
							} else if (type.equalsIgnoreCase("Promotions")) {
								Promotions += resultMap.get(s);
								continue;
							}
						}
					}
				}
				pdiOS = new ReducedNotificationData(sDate, sPlatform, repo.getAllNotificationSentCount(sDate, "iOS"),
						usageDataDAO.getNotificationViewCountByDate(startDate, "iOS"), NewsCount, Promotions,
						WelcomeNotification, AvatarCompletionReminder);
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}

				NewsCount = 0;
				Promotions = 0;
				WelcomeNotification = 0;
				AvatarCompletionReminder = 0;
				
				//Get notifications for the Day by date
				HashMap<Integer, List<String>> notificationMap = (HashMap<Integer, List<String>>) repo
						.findNotificationDetailsBydate(sDate);			
				
				// loop for each notification ID
				for (Integer i : notificationMap.keySet()) {				
					if (i==1) {
						AvatarCompletionReminder += usageDataDAO.getReadNotificationCountByNid(i+"");
						continue;
					} else if (i==2) {
						AvatarCompletionReminder += usageDataDAO.getReadNotificationCountByNid(i+"");
						continue;
					} else {
						//String type = repo.getTypeForNid(s);
						String type = notificationMap.get(i).get(0);
						if (type != null) {
							if (type.equalsIgnoreCase("News")) {
								NewsCount += usageDataDAO.getReadNotificationCountByNid(i+"");
								continue;
							} else if (type.equalsIgnoreCase("Promotions")) {
								Promotions += usageDataDAO.getReadNotificationCountByNid(i+"");
								continue;
							} else {
								logger.error("[Debug] Null Type");
							}
						}
					}
				}
				

				resultMap = usageDataDAO.getNotificationReadCountByNidNoExclusion(startDate, "All");

				for (String s : resultMap.keySet()) {
					if (s.equals("1")) {
						AvatarCompletionReminder += resultMap.get(s);
						continue;
					} else if (s.equals("2")) {
						WelcomeNotification += resultMap.get(s);
						continue;
					} else {
						String type = repo.getTypeForNid(s);
						if (type != null) {
							if (type.equalsIgnoreCase("News")) {
								NewsCount += resultMap.get(s);
								continue;
							} else if (type.equalsIgnoreCase("Promotions")) {
								Promotions += resultMap.get(s);
								continue;
							} else {
								logger.error("[Debug] Null Type");
							}
						}
					}
				}

				pdDev = new ReducedNotificationData(sDate, sPlatform, repo.getAllNotificationSentCount(sDate, "All"),
						usageDataDAO.getNotificationViewCountByDate(startDate, "All"), NewsCount, Promotions,
						WelcomeNotification, AvatarCompletionReminder);
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getReadNotificationReportByDateNoExcl");
		return gson.toJson(dataList);
	}

	// >>>>>>>>>>>>>>>>>>>>

	@CrossOrigin
	@RequestMapping(value = "/getReadNotificationReportByDate", method = RequestMethod.POST)
	public String getReducedNotificationViewReportByDate(@RequestBody String request) {
		logger.info("[Enter] getReadNotificationReportByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<ReducedNotificationData> dataList = new ArrayList<ReducedNotificationData>();
		ReducedNotificationData pdAndroid = null;
		ReducedNotificationData pdiOS = null;
		ReducedNotificationData pdDev = null;
		Map<String, Integer> resultMap = new HashMap<String, Integer>();

		// Counts
		int NewsCount = 0;
		int Promotions = 0;
		int WelcomeNotification = 0;
		int AvatarCompletionReminder = 0;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				NewsCount = 0;
				Promotions = 0;
				WelcomeNotification = 0;
				AvatarCompletionReminder = 0;
				
				//Get All Notifications sent by date
				

				resultMap = usageDataDAO.getNotificationReadCountByNid(startDate, "Android");

				for (String s : resultMap.keySet()) {
					if (s.equals("1")) {
						AvatarCompletionReminder += resultMap.get(s);
						// continue;
					} else if (s.equals("2")) {
						WelcomeNotification += resultMap.get(s);
						// continue;
					} else {
						String type = repo.getTypeForNid(s);

						if (type != null) {
							if (type.equalsIgnoreCase("News")) {
								NewsCount += resultMap.get(s);
								// continue;
							} else if (type.equalsIgnoreCase("Promotions")) {
								Promotions += resultMap.get(s);
								// continue;
							}
						} else {
							logger.error("[Debug] Null Type");
						}
					}
				}
				pdAndroid = new ReducedNotificationData(sDate, sPlatform,
						repo.getAllNotificationSentCount(sDate, "Android"),
						usageDataDAO.getNotificationViewCountByDate(startDate, "Android"), NewsCount, Promotions,
						WelcomeNotification, AvatarCompletionReminder);
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				NewsCount = 0;
				Promotions = 0;
				WelcomeNotification = 0;
				AvatarCompletionReminder = 0;

				resultMap = usageDataDAO.getNotificationReadCountByNid(startDate, "iOS");

				for (String s : resultMap.keySet()) {
					if (s.equals("1")) {
						AvatarCompletionReminder += resultMap.get(s);
						// continue;
					} else if (s.equals("2")) {
						WelcomeNotification += resultMap.get(s);
						// continue;
					} else {
						String type = repo.getTypeForNid(s);
						if (type != null) {
							if (type.equalsIgnoreCase("News")) {
								NewsCount += resultMap.get(s);
								// continue;
							} else if (type.equalsIgnoreCase("Promotions")) {
								Promotions += resultMap.get(s);
								// continue;
							}
						} else {
							logger.error("[Debug] Null Type");
						}
					}
				}
				pdiOS = new ReducedNotificationData(sDate, sPlatform, repo.getAllNotificationSentCount(sDate, "iOS"),
						usageDataDAO.getNotificationViewCountByDate(startDate, "iOS"), NewsCount, Promotions,
						WelcomeNotification, AvatarCompletionReminder);
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				
				NewsCount = 0;
				Promotions = 0;
				WelcomeNotification = 0;
				AvatarCompletionReminder = 0;
				
				//Get notifications for the Day by date
				HashMap<Integer, List<String>> notificationMap = (HashMap<Integer, List<String>>) repo
						.findNotificationDetailsBydate(sDate);			
				
				// loop for each notification ID
				for (Integer i : notificationMap.keySet()) {				
					if (i==1) {
						AvatarCompletionReminder += usageDataDAO.getReadNotificationCountByNid(i+"");
						continue;
					} else if (i==2) {
						AvatarCompletionReminder += usageDataDAO.getReadNotificationCountByNid(i+"");
						continue;
					} else {
						String type = repo.getTypeForNid(i+"");
						//String type = notificationMap.get(i).get(0);
						if (type != null) {
							if (type.equalsIgnoreCase("News")) {
								NewsCount += usageDataDAO.getReadNotificationCountByNid(i+"");
								continue;
							} else if (type.equalsIgnoreCase("Promotions")) {
								Promotions += usageDataDAO.getReadNotificationCountByNid(i+"");
								continue;
							} else {
								logger.error("[Debug] Null Type");
							}
						}
					}
				}

				/*NewsCount = 0;
				Promotions = 0;
				WelcomeNotification = 0;
				AvatarCompletionReminder = 0;

				resultMap = usageDataDAO.getNotificationReadCountByNid(startDate, "All");

				for (String s : resultMap.keySet()) {

					if (s.equals("1")) {
						AvatarCompletionReminder += resultMap.get(s);
						// continue;
					} else if (s.equals("2")) {
						WelcomeNotification += resultMap.get(s);
						// continue;
					} else {
						String type = repo.getTypeForNid(s);
						if (type != null) {
							if (type.equalsIgnoreCase("News")) {
								NewsCount += resultMap.get(s);
								// continue;
							} else if (type.equalsIgnoreCase("Promotions")) {
								Promotions += resultMap.get(s);
								// continue;
							} else {
								logger.error("[Debug] Null Type");
							}
						}
					}
				}*/

				pdDev = new ReducedNotificationData(sDate, sPlatform, repo.getAllNotificationSentCount(sDate, "All"),
						usageDataDAO.getNotificationViewCountByDate(startDate, "All"), NewsCount, Promotions,
						WelcomeNotification, AvatarCompletionReminder);
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getReadNotificationReportByDate");
		return gson.toJson(dataList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getContentBasedReadNotificationReportByDate", method = RequestMethod.POST)
	public String getContentBasedReadNotificationReportByDate(@RequestBody String request) {
		logger.info("[Enter] getReadNotificationReportByDate");
		String sDate = null;
		String eDate = null;
		Gson gson = new Gson();
		try {
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}

		ArrayList<ContentNotification> dataList = new ArrayList<ContentNotification>();

		while (true) {
			if (startDate.compareTo(endDate) > 0) {
				break;
			}
			//Get notifications for the Day by date
			HashMap<Integer, List<String>> notificationMap = (HashMap<Integer, List<String>>) repo
					.findNotificationDetailsBydate(sDate);			
			
			// loop for each notification ID
			for (Integer i : notificationMap.keySet()) {				
				System.out.println("NID "+i);
				dataList.add(new ContentNotification(sDate, i.toString(), notificationMap.get(i).get(0),
						notificationMap.get(i).get(1),notificationMap.get(i).get(2), repo.findNotificationSentDetailsByNid(i) + "",
						usageDataDAO.getReadNotificationCountByNid(i+"")+""));
			}
			
			sDate = getNextDate(sDate);
			startDate = getNextDateObject(startDate);
		}
		return new Gson().toJson(dataList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getAvatarGenderReportByDate", method = RequestMethod.POST)
	public String getAvatarGenderBasedReportByDate(@RequestBody String request) {
		logger.info("[Enter] getAvatarGenderReportByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		ArrayList<GenderReport> dataList = new ArrayList<GenderReport>();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		GenderReport pdAndroid = null;
		GenderReport pdiOS = null;
		GenderReport pdDev = null;
		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}

				ArrayList<String> AppIdListMale = (ArrayList<String>) repo.findAvatarCreatedAppIDListByDate(sDate,
						"male");
				ArrayList<String> AppIdListFeMale = (ArrayList<String>) repo.findAvatarCreatedAppIDListByDate(sDate,
						"female");
				int maleCountAndroid = 0;
				int femaleCount = 0;
				for (String s : AppIdListMale) {
					if (repo.isAppIDAndroid(s)) {
						maleCountAndroid++;
					}
				}
				for (String s2 : AppIdListFeMale) {
					if (repo.isAppIDAndroid(s2)) {
						femaleCount++;
					}
				}

				pdAndroid = new GenderReport(sDate, "Android", maleCountAndroid, femaleCount);
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				ArrayList<String> AppIdListMale = (ArrayList<String>) repo.findAvatarCreatedAppIDListByDate(sDate,
						"male");
				ArrayList<String> AppIdListFeMale = (ArrayList<String>) repo.findAvatarCreatedAppIDListByDate(sDate,
						"female");
				int maleCountAndroid = 0;
				int femaleCount = 0;
				for (String s : AppIdListMale) {
					if (!repo.isAppIDAndroid(s)) {
						maleCountAndroid++;
					}
				}
				for (String s2 : AppIdListFeMale) {
					if (!repo.isAppIDAndroid(s2)) {
						femaleCount++;
					}
				}

				pdiOS = new GenderReport(sDate, "iOS", maleCountAndroid, femaleCount);
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				ArrayList<String> AppIdListMale = (ArrayList<String>) repo.findAvatarCreatedAppIDListByDate(sDate,
						"male");
				ArrayList<String> AppIdListFeMale = (ArrayList<String>) repo.findAvatarCreatedAppIDListByDate(sDate,
						"female");
				int maleCountAndroid = 0;
				int femaleCount = 0;
				for (String s : AppIdListMale) {
					// if(!repo.isAppIDAndroid(s)) {
					maleCountAndroid++;
					// }
				}
				for (String s2 : AppIdListFeMale) {
					// if(!repo.isAppIDAndroid(s2)) {
					femaleCount++;
					// }
				}

				pdDev = new GenderReport(sDate, "All", maleCountAndroid, femaleCount);
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}

		logger.info("[Leave] getAvatarGenderReportByDate");
		return gson.toJson(dataList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getStickerShareReportByImageID", method = RequestMethod.POST)
	public String getStickerShareViewedReportByDate(@RequestBody String request) {
		logger.info("[Enter] getStickerShareReportByImageID");
		String sDate = null;
		String eDate = null;
		Gson gson = new Gson();
		try {
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		ArrayList<ImageData> dataList = new ArrayList<ImageData>();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		Date endDate;

		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}
		Map<String, Integer> viewedMap = usageDataDAO.getViewedCountByImageId(startDate, endDate);
		Map<String, Integer> sharedMap = usageDataDAO.getSharedCountByImageID(startDate, endDate);

		Set<String> imageIDSet = new HashSet<String>();
		imageIDSet.addAll(sharedMap.keySet());
		imageIDSet.addAll(viewedMap.keySet());

		for (String imageId : imageIDSet) {
			Integer shareCount = sharedMap.get(imageId);
			Integer viewCount = viewedMap.get(imageId);
			dataList.add(
					new ImageData(imageId, shareCount == null ? 0 : shareCount, viewCount == null ? 0 : viewCount));
		}
		logger.info("[Leave] getStickerShareReportByImageID");
		return new Gson().toJson(dataList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getScreenViewReportByDateUserLevel", method = RequestMethod.POST)
	public String getScreenViewDataByDateUserLevel(@RequestBody String request) {
		logger.info("[Enter] getScreenViewReportByDateUserLevel");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<ScreenViewData> dataList = new ArrayList<ScreenViewData>();
		ScreenViewData pdAndroid = null;
		ScreenViewData pdiOS = null;
		ScreenViewData pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdAndroid = new ScreenViewData(sDate, sPlatform,
						usageDataDAO.getHomeScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getHomeScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getAvatarScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getAvatarScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getStickerScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getStickerScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getSettingsScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getSettingsScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getSearchScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getSearchScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getOnboardingScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getNotificationSettingScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getNotificationSettingScreenViewedCountByDateUserLevel(sPlatform,
										startDate),
						usageDataDAO.getLanguagePreferenceScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getLanguagePreferenceScreenViewedCountByDateUserLevel(sPlatform,
										startDate),
						usageDataDAO.getSignUpScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getSignUpScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getMobileSignUpScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getMobileSignUpScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getLoginScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getLoginScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getLoginMobileScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getLoginMobileScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getHelpCentreScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getHelpCentreScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getReportAProblemScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getReportAProblemScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getContactUsScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getContactUsScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getLegalInfoScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getLegalInfoScreenViewedCountByDateUserLevel(sPlatform, startDate));
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdiOS = new ScreenViewData(sDate, sPlatform,
						usageDataDAO.getHomeScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getHomeScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getAvatarScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getAvatarScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getStickerScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getStickerScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getSettingsScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getSettingsScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getSearchScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getSearchScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getOnboardingScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getNotificationSettingScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getNotificationSettingScreenViewedCountByDateUserLevel(sPlatform,
										startDate),
						usageDataDAO.getLanguagePreferenceScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getLanguagePreferenceScreenViewedCountByDateUserLevel(sPlatform,
										startDate),
						usageDataDAO.getSignUpScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getSignUpScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getMobileSignUpScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getMobileSignUpScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getLoginScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getLoginScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getLoginMobileScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getLoginMobileScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getHelpCentreScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getHelpCentreScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getReportAProblemScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getReportAProblemScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getContactUsScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getContactUsScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getLegalInfoScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getLegalInfoScreenViewedCountByDateUserLevel(sPlatform, startDate));
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				pdDev = new ScreenViewData(sDate, sPlatform,
						usageDataDAO.getHomeScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getHomeScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getAvatarScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getAvatarScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getStickerScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getStickerScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getSettingsScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getSettingsScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getSearchScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getSearchScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getOnboardingScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getNotificationSettingScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getNotificationSettingScreenViewedCountByDateUserLevel(sPlatform,
										startDate),
						usageDataDAO.getLanguagePreferenceScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getLanguagePreferenceScreenViewedCountByDateUserLevel(sPlatform,
										startDate),
						usageDataDAO.getSignUpScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getSignUpScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getMobileSignUpScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getMobileSignUpScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getLoginScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getLoginScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getLoginMobileScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getLoginMobileScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getHelpCentreScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getHelpCentreScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getReportAProblemScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getReportAProblemScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getContactUsScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getContactUsScreenViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getLegalInfoScreenViewedCountByDate(sPlatform, startDate) + "-*-"
								+ usageDataDAO.getLegalInfoScreenViewedCountByDateUserLevel(sPlatform, startDate));
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Enter] getScreenViewReportByDateUserLevel");
		return gson.toJson(dataList);
	}

	// ======= >>>>
	@CrossOrigin
	@RequestMapping(value = "/getAvatarActivityReportByDate", method = RequestMethod.POST)
	public String getAvatarActivityReportByDate(@RequestBody String request) {
		logger.info("[Enter] getAvatarActivityReportByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;

		// Map<String, Long> finalMap = new LinkedHashMap<String, Long>();
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		ArrayList<AvatarFeatureHelper> dataList = new ArrayList<AvatarFeatureHelper>();

		while (true) {
			if (startDate.compareTo(endDate) > 0) {
				break;
			}
			// for Face Shape 45 /46
			Set<String> faceSet = new HashSet<String>();
			Map<String, Long> faceMapView = usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "46","faceshapeid");
			Map<String, Long> faceMapSelect = usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "45","faceshapeid");
			faceSet.addAll(faceMapView.keySet());
			faceSet.addAll(faceMapSelect.keySet());
			for (String s : faceSet) {
				dataList.add(new AvatarFeatureHelper(sDate, sPlatform, "Face Shape", s,
						faceMapView.get(s) == null ? "0" : (faceMapView.get(s) + ""),
						faceMapSelect.get(s) == null ? "0" : (faceMapSelect.get(s) + "")));
			}

			// for Eye 57/58
			Set<String> eyeSet = new HashSet<String>();
			Map<String, Long> eyeMapView = usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "58","eyeshapeid");
			Map<String, Long> eyeMapSelect = usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "57","eyeshapeid");
			eyeSet.addAll(eyeMapView.keySet());
			eyeSet.addAll(eyeMapSelect.keySet());
			for (String s : eyeSet) {
				dataList.add(new AvatarFeatureHelper(sDate, sPlatform, "Eye", s,
						eyeMapView.get(s) == null ? "0" : (eyeMapView.get(s) + ""),
						eyeMapSelect.get(s) == null ? "0" : (eyeMapSelect.get(s) + "")));
			}

			// for Eyebrow 61/62
			Set<String> eyeBrowSet = new HashSet<String>();
			Map<String, Long> eyeBrowMapView = usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "62","eyebrowid");
			Map<String, Long> eyeBrowMapSelect = usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "61","eyebrowid");
			eyeBrowSet.addAll(eyeBrowMapView.keySet());
			eyeBrowSet.addAll(eyeBrowMapSelect.keySet());
			for (String s : eyeBrowSet) {
				dataList.add(new AvatarFeatureHelper(sDate, sPlatform, "Eyebrow", s,
						eyeBrowMapView.get(s) == null ? "0" : (eyeBrowMapView.get(s) + ""),
						eyeBrowMapSelect.get(s) == null ? "0" : (eyeBrowMapSelect.get(s) + "")));
			}

			// for Nose 63/64
			Set<String> noseSet = new HashSet<String>();
			Map<String, Long> noseMapView = usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "64","noseid");
			Map<String, Long> noseMapSelect = usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "63","noseid");
			noseSet.addAll(noseMapView.keySet());
			noseSet.addAll(noseMapSelect.keySet());
			for (String s : noseSet) {
				dataList.add(new AvatarFeatureHelper(sDate, sPlatform, "Nose", s,
						noseMapView.get(s) == null ? "0" : (noseMapView.get(s) + ""),
						noseMapSelect.get(s) == null ? "0" : (noseMapSelect.get(s) + "")));
			}

			// for Ear 91/90
			Set<String> earSet = new HashSet<String>();
			Map<String, Long> earMapView = usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "90","earid");
			Map<String, Long> earMapSelect = usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "91","earid");
			earSet.addAll(earMapView.keySet());
			earSet.addAll(earMapSelect.keySet());
			for (String s : earSet) {
				dataList.add(new AvatarFeatureHelper(sDate, sPlatform, "Ear", s,
						earMapView.get(s) == null ? "0" : (earMapView.get(s) + ""),
						earMapSelect.get(s) == null ? "0" : (earMapSelect.get(s) + "")));
			}

			// for Lips 67/68
			Set<String> lipsSet = new HashSet<String>();
			Map<String, Long> lipsMapView = usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "68","lipstickcolorid");
			Map<String, Long> lipsMapSelect = usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "67","lipstickcolorid");
			lipsSet.addAll(lipsMapView.keySet());
			lipsSet.addAll(lipsMapSelect.keySet());
			for (String s : lipsSet) {
				dataList.add(new AvatarFeatureHelper(sDate, sPlatform, "Lips", s,
						lipsMapView.get(s) == null ? "0" : (lipsMapView.get(s) + ""),
						lipsMapSelect.get(s) == null ? "0" : (lipsMapSelect.get(s) + "")));
			}

			// for Hair 49/50
			Set<String> hairSet = new HashSet<String>();
			Map<String, Long> hairMapView = usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "50","hairstyleid");
			Map<String, Long> hairMapSelect = usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "49","hairstyleid");
			hairSet.addAll(hairMapView.keySet());
			hairSet.addAll(hairMapSelect.keySet());
			for (String s : hairSet) {
				dataList.add(new AvatarFeatureHelper(sDate, sPlatform, "Hair", s,
						hairMapView.get(s) == null ? "0" : (hairMapView.get(s) + ""),
						hairMapSelect.get(s) == null ? "0" : (hairMapSelect.get(s) + "")));
			}

			// for Beard 53/54
			Set<String> beardSet = new HashSet<String>();
			Map<String, Long> beardMapView = usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "54","facialhairid");
			Map<String, Long> beardMapSelect = usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "53","facialhairid");
			beardSet.addAll(hairMapView.keySet());
			beardSet.addAll(hairMapSelect.keySet());
			for (String s : beardSet) {
				dataList.add(new AvatarFeatureHelper(sDate, sPlatform, "Beard", s,
						beardMapView.get(s) == null ? "0" : (beardMapView.get(s) + ""),
						beardMapSelect.get(s) == null ? "0" : (beardMapSelect.get(s) + "")));
			}
			sDate = getNextDate(sDate);
			startDate = getNextDateObject(startDate);
		}

		logger.info("[Leave] getAvatarActivityReportByDate");
		return gson.toJson(dataList);

	}

	/*
	 * @CrossOrigin
	 * 
	 * @RequestMapping(value = "/getAvatarActivityColourReportByDate", method =
	 * RequestMethod.POST) public String
	 * getAvatarActivityColourReportByDate(@RequestBody String request) {
	 * logger.info("[Enter] getAvatarActivityReportByDate"); String sDate = null;
	 * String eDate = null; String sPlatform = null;
	 * 
	 * //Map<String, Long> finalMap = new LinkedHashMap<String, Long>(); Gson gson =
	 * new Gson(); try { sPlatform = getsPlatform(request).split("\"")[1]; eDate =
	 * getsEndDate(request).split("\"")[1]; sDate =
	 * getsStartDate(request).split("\"")[1]; } catch (IOException e2) {
	 * 
	 * e2.printStackTrace(); logger.error("[ERROR] Bad Request"); return
	 * gson.toJson("Bad Request"); } DateFormat formatter = new
	 * SimpleDateFormat("yyyy-MM-dd"); Date startDate; try { startDate =
	 * formatter.parse(sDate); } catch (ParseException e1) {
	 * logger.error("[ERROR] DATE not parsable"); e1.printStackTrace(); return
	 * gson.toJson("Invalid Date"); } Date endDate; try { endDate =
	 * formatter.parse(eDate); } catch (ParseException e1) { e1.printStackTrace();
	 * logger.error("[ERROR] DATE not parsable"); return
	 * gson.toJson("Invalid Date"); }
	 * 
	 * ArrayList<AvatarFeatureHelper> dataList = new
	 * ArrayList<AvatarFeatureHelper>();
	 * 
	 * while (true) { if (startDate.compareTo(endDate) > 0) { break; } // for Face
	 * Shape 45 /46 Set<String> faceSet = new HashSet<String>(); Map<String, Long>
	 * faceMapView = usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate,
	 * "46"); Map<String, Long> faceMapSelect =
	 * usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "45");
	 * faceSet.addAll(faceMapView.keySet()); faceSet.addAll(faceMapSelect.keySet());
	 * for (String s : faceSet) { dataList.add( new AvatarFeatureHelper(sDate,
	 * sPlatform, "Face Shape", s, faceMapView.get(s) == null ? "0" :
	 * (faceMapView.get(s) + ""), faceMapSelect.get(s) == null ? "0" :
	 * (faceMapSelect.get(s) + ""))); }
	 * 
	 * // for Eye 57/58 Set<String> eyeSet = new HashSet<String>(); Map<String,
	 * Long> eyeMapView = usageDataDAO.getAvatarFeatureReportByDate(sPlatform,
	 * startDate, "58"); Map<String, Long> eyeMapSelect =
	 * usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "57");
	 * eyeSet.addAll(eyeMapView.keySet()); eyeSet.addAll(eyeMapSelect.keySet()); for
	 * (String s : eyeSet) { dataList.add( new AvatarFeatureHelper(sDate, sPlatform,
	 * "Eye", s, eyeMapView.get(s) == null ? "0" : (eyeMapView.get(s) + ""),
	 * eyeMapSelect.get(s) == null ? "0" : (eyeMapSelect.get(s) + ""))); }
	 * 
	 * // for Eyebrow 61/62 Set<String> eyeBrowSet = new HashSet<String>();
	 * Map<String, Long> eyeBrowMapView =
	 * usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "62");
	 * Map<String, Long> eyeBrowMapSelect =
	 * usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "61");
	 * eyeBrowSet.addAll(eyeBrowMapView.keySet());
	 * eyeBrowSet.addAll(eyeBrowMapSelect.keySet()); for (String s : eyeBrowSet) {
	 * dataList.add( new AvatarFeatureHelper(sDate, sPlatform, "Eyebrow", s,
	 * eyeBrowMapView.get(s) == null ? "0" : (eyeBrowMapView.get(s) + ""),
	 * eyeBrowMapSelect.get(s) == null ? "0" : (eyeBrowMapSelect.get(s) + ""))); }
	 * 
	 * // for Nose 63/64 Set<String> noseSet = new HashSet<String>(); Map<String,
	 * Long> noseMapView = usageDataDAO.getAvatarFeatureReportByDate(sPlatform,
	 * startDate, "64"); Map<String, Long> noseMapSelect =
	 * usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "63");
	 * noseSet.addAll(noseMapView.keySet()); noseSet.addAll(noseMapSelect.keySet());
	 * for (String s : noseSet) { dataList.add( new AvatarFeatureHelper(sDate,
	 * sPlatform, "Nose", s, noseMapView.get(s) == null ? "0" : (noseMapView.get(s)
	 * + ""), noseMapSelect.get(s) == null ? "0" : (noseMapSelect.get(s) + ""))); }
	 * 
	 * // for Ear 91/90 Set<String> earSet = new HashSet<String>(); Map<String,
	 * Long> earMapView = usageDataDAO.getAvatarFeatureReportByDate(sPlatform,
	 * startDate, "90"); Map<String, Long> earMapSelect =
	 * usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "91");
	 * earSet.addAll(earMapView.keySet()); earSet.addAll(earMapSelect.keySet()); for
	 * (String s : earSet) { dataList.add( new AvatarFeatureHelper(sDate, sPlatform,
	 * "Ear", s, earMapView.get(s) == null ? "0" : (earMapView.get(s) + ""),
	 * earMapSelect.get(s) == null ? "0" : (earMapSelect.get(s) + ""))); }
	 * 
	 * // for Lips 67/68 Set<String> lipsSet = new HashSet<String>(); Map<String,
	 * Long> lipsMapView = usageDataDAO.getAvatarFeatureReportByDate(sPlatform,
	 * startDate, "68"); Map<String, Long> lipsMapSelect =
	 * usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "67");
	 * lipsSet.addAll(lipsMapView.keySet()); lipsSet.addAll(lipsMapSelect.keySet());
	 * for (String s : lipsSet) { dataList.add( new AvatarFeatureHelper(sDate,
	 * sPlatform, "Lips", s, lipsMapView.get(s) == null ? "0" : (lipsMapView.get(s)
	 * + ""), lipsMapSelect.get(s) == null ? "0" : (lipsMapSelect.get(s) + ""))); }
	 * 
	 * // for Hair 49/50 Set<String> hairSet = new HashSet<String>(); Map<String,
	 * Long> hairMapView = usageDataDAO.getAvatarFeatureReportByDate(sPlatform,
	 * startDate, "50"); Map<String, Long> hairMapSelect =
	 * usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "49");
	 * hairSet.addAll(hairMapView.keySet()); hairSet.addAll(hairMapSelect.keySet());
	 * for (String s : hairSet) { dataList.add( new AvatarFeatureHelper(sDate,
	 * sPlatform, "Hair", s, hairMapView.get(s) == null ? "0" : (hairMapView.get(s)
	 * + ""), hairMapSelect.get(s) == null ? "0" : (hairMapSelect.get(s) + ""))); }
	 * 
	 * // for Beard 53/54 Set<String> beardSet = new HashSet<String>(); Map<String,
	 * Long> beardMapView = usageDataDAO.getAvatarFeatureReportByDate(sPlatform,
	 * startDate, "54"); Map<String, Long> beardMapSelect =
	 * usageDataDAO.getAvatarFeatureReportByDate(sPlatform, startDate, "53");
	 * beardSet.addAll(hairMapView.keySet());
	 * beardSet.addAll(hairMapSelect.keySet()); for (String s : beardSet) {
	 * dataList.add( new AvatarFeatureHelper(sDate, sPlatform, "Beard", s,
	 * beardMapView.get(s) == null ? "0" : (beardMapView.get(s) + ""),
	 * beardMapSelect.get(s) == null ? "0" : (beardMapSelect.get(s) + ""))); } sDate
	 * = getNextDate(sDate); startDate = getNextDateObject(startDate); }
	 * 
	 * logger.info("[Leave] getAvatarActivityReportByDate"); return
	 * gson.toJson(dataList);
	 * 
	 * }
	 */

	@CrossOrigin
	@RequestMapping(value = "/getActiveUsersReportByDate", method = RequestMethod.POST)
	public String getActiveUsersReport(@RequestBody String request) {
		logger.info("[Enter] getActiveUsersReportByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		ArrayList<ActiveUserData> dataList = new ArrayList<ActiveUserData>();
		ActiveUserData pdAndroid = null;
		ActiveUserData pdiOS = null;
		ActiveUserData pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				
				
				
				Long lActiveUser = new Long(usageDataDAO.getTotalActiveUsersByDate("Android", startDate));
				Integer lNewUser = repo.findNewUsersCountByDate(sDate, "Android");
				Long returningUser = lActiveUser - lNewUser;
				pdAndroid = new ActiveUserData(sDate, "Android", lActiveUser, lNewUser, returningUser);
				dataList.add(pdAndroid);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Long lActiveUser = new Long(usageDataDAO.getTotalActiveUsersByDate("iOS", startDate));
				Integer lNewUser = repo.findNewUsersCountByDate(sDate, "iOS");
				Long returningUser = lActiveUser - lNewUser;
				pdiOS = new ActiveUserData(sDate, "iOS", lActiveUser, lNewUser, returningUser);
				dataList.add(pdiOS);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				Long lActiveUser = new Long(usageDataDAO.getTotalActiveUsersByDate("All", startDate));
				Integer lNewUser = repo.findNewUsersCountByDate(sDate, "All");
				Long returningUser = lActiveUser - lNewUser;
				pdDev = new ActiveUserData(sDate, "All", lActiveUser, lNewUser, returningUser);
				dataList.add(pdDev);
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getActiveUsersReportByDate");
		return gson.toJson(dataList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getAvatarDetails", method = RequestMethod.POST)
	public String getAvatarDetailsByDate(@RequestBody String request) {
		logger.info("[Enter] getAvatarDetails");
		String sDate = null;
		String eDate = null;
		Gson gson = new Gson();
		try {
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		List<AvatarDetails> avatarDetailsList = new ArrayList<AvatarDetails>();

		while (true) {
			if (startDate.compareTo(endDate) > 0) {
				break;
			}

			Map<String, List<List<String>>> tempMap = repo.getAvatarStickerDataByDate(sDate);

			for (String appId : tempMap.keySet()) {
				// System.out.println("APP ID : " + appId);
				for (List<String> row : tempMap.get(appId)) {
					avatarDetailsList.add(new AvatarDetails(sDate, row.get(0), row.get(1), row.get(2), row.get(3),
							usageDataDAO.getImageIDCountSharedByDate(row.get(1), startDate)));
				}
			}
			sDate = getNextDate(sDate);
			startDate = getNextDateObject(startDate);
		}

		logger.info("[Leave] getAvatarDetails");
		return gson.toJson(avatarDetailsList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getConsolidatedReport", method = RequestMethod.POST)
	public String getConsolidatedReport(@RequestBody String request) {
		logger.info("[Enter] getConsolidatedReport");
		// Prepare Key List
		List<String> keyList = new ArrayList<String>();
		keyList.add("Daily active user (Not sessions)");
		keyList.add("Daily active user Android");
		keyList.add("Daily active user iOS");
		keyList.add("Weekly Active users");
		keyList.add("Monthly Active users");
		keyList.add("First time user");
		keyList.add("Returning user");
		keyList.add("First open - User viewed - Splash Screen (this event fire only once so this is users level data)");
		keyList.add("Users inititated keyboard connection");
		keyList.add("Users enabled keyboard");
		keyList.add("Users initiated Facebook sign up");
		keyList.add("Users completed Facebook Sign up");
		keyList.add("Users initiated sign up using mobile no.");
		keyList.add("Users completed sign up using mobile no.");
		keyList.add("Reset OTP");
		keyList.add("Users landed on \"create avatar\" page");
		keyList.add("Selected gender - female");
		keyList.add("Selected gender - male");
		keyList.add("Users completed/saved avatar");
		keyList.add("Users reset their avatar");
		keyList.add("No of notification created");
		keyList.add("No. of users notifications was delivered");
		keyList.add("No. of people opened the notification");
		keyList.add("Notification types (ex: Sticker Updates, promotions, news etc)");
		keyList.add("Welcome Notification Sent");
		keyList.add("Welcome Notification Read");
		keyList.add("Home screen viewed");
		keyList.add("Sticker screen viewed");
		keyList.add("Avatar screen viewed");
		keyList.add("Settings screen viewed");
		keyList.add("Search screen viewed");

		// Prepare Result Object
		// ArrayList<ConsolidatedObject> returnList = new
		// ArrayList<ConsolidatedObject>();
		List<ReturnObject> returnList = new ArrayList<ReturnObject>();

		String sDate = null;
		String eDate = null;
		Gson gson = new Gson();
		try {
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		while (true) {
			if (startDate.compareTo(endDate) > 0) {
				break;
			}

			// ArrayList<ReturnObject> returnObjectList = new ArrayList<ReturnObject>();

			List<String> notificationList = repo.getNotificationCreatedCount(sDate);
			Long lActiveUser = new Long(usageDataDAO.getTotalActiveUsersByDate("All", startDate));
			Long lActiveUserAndroid = new Long(usageDataDAO.getTotalActiveUsersByDate("Android", startDate));
			Long lActiveUseriOS = new Long(usageDataDAO.getTotalActiveUsersByDate("iOS", startDate));
			Long lNewUser = new Long(repo.findNewUsersCountByDate(sDate, "All"));

			Long returningUser = lActiveUser - lNewUser;

			ArrayList<String> AppIdListMale = (ArrayList<String>) repo.findAvatarCreatedAppIDListByDate(sDate, "male");
			ArrayList<String> AppIdListFeMale = (ArrayList<String>) repo.findAvatarCreatedAppIDListByDate(sDate,
					"female");
			long maleCount = AppIdListMale.size();
			long femaleCount = AppIdListFeMale.size();
			RestTemplate rt = new RestTemplate();

			Map<String, String> vars = new HashMap<String, String>();
			vars.put("platform", "All");
			vars.put("startDate", sDate);
			vars.put("endDate", sDate);

			ResponseEntity<RestResponse> response = null;
			RequestEntity<String> requestEntity;
			try {
				requestEntity = RequestEntity.post(new URL("https://saveyra.com/analytics/metrics").toURI())
				//requestEntity = RequestEntity.post(new URL("http://13.233.113.131/analytics/metrics").toURI())
						.contentType(MediaType.APPLICATION_JSON).body(gson.toJson(vars));
				response = rt.exchange(requestEntity, RestResponse.class);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			// Evaluate Delivered Count
			String[] splittedArray = response.getBody().getData().toString().split("Delivered=");
			long DeliveredCount = 0;
			for (int i = 1; i < splittedArray.length; i++) {
				DeliveredCount += Integer.parseInt(splittedArray[i].split(",")[0]);
			}
			returnList.add(new ReturnObject(sDate, keyList.get(0), lActiveUser + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(1), lActiveUserAndroid + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(2), lActiveUseriOS + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(3),
					usageDataDAO.getWeeklyActiveUserCountByDate(startDate) + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(4),
					usageDataDAO.getMonthlyActiveUserCountByDate(startDate) + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(5), lNewUser + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(6), returningUser + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(7), lNewUser + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(8),
					usageDataDAO.getKeyboardInitCountByDateUserLevel(startDate, "All") + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(9),
					usageDataDAO.getKeyboardEnableCountByDateUserLevel(startDate, "All") + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(10),
					usageDataDAO.getFBUserInitReportByDate("All", startDate) + ""));
			returnList
					.add(new ReturnObject(sDate, keyList.get(11), new Long(repo.findFBUsersByDate("All", sDate)) + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(12),
					usageDataDAO.getMobileUserInitReportByDate("All", startDate) + ""));
			returnList.add(
					new ReturnObject(sDate, keyList.get(13), new Long(repo.findMobileUsersByDate("All", sDate)) + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(14),
					usageDataDAO.getMobileUserResetOTPReportByDate("All", startDate) + ""));
			returnList.add(
					new ReturnObject(sDate, keyList.get(15), usageDataDAO.findAvatarInitByDate("All", startDate) + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(16), femaleCount + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(17), maleCount + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(18),
					usageDataDAO.findAvatarCreatedByDate("All", startDate) + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(19),
					usageDataDAO.findAvatarResetByDate("All", startDate) + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(20), notificationList.get(0)));
			returnList.add(new ReturnObject(sDate, keyList.get(21), DeliveredCount + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(22),
					usageDataDAO.getNotificationViewCountByDate(startDate, "All") + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(23), notificationList.get(1) + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(24),
					new Long(repo.getNotificationSentCount(sDate, "WelcomeNotification", "All")) + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(25),
					new Long(usageDataDAO.getNotificationReadCountByNid(startDate, "All").get("2") == null ? 0
							: usageDataDAO.getNotificationReadCountByNid(startDate, "All").get("2")) + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(26),
					usageDataDAO.getHomeScreenViewedCountByDateUserLevel("All", startDate) + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(27),
					usageDataDAO.getStickerScreenViewedCountByDateUserLevel("All", startDate) + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(28),
					usageDataDAO.getAvatarScreenViewedCountByDateUserLevel("All", startDate) + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(29),
					usageDataDAO.getSettingsScreenViewedCountByDateUserLevel("All", startDate) + ""));
			returnList.add(new ReturnObject(sDate, keyList.get(30),
					usageDataDAO.getSearchScreenViewedCountByDateUserLevel("All", startDate) + ""));

			// returnList.add(returnObjectList);
			sDate = getNextDate(sDate);
			startDate = getNextDateObject(startDate);
		}

		logger.info("[Leave] getConsolidatedReport");
		return gson.toJson(returnList);
	}

	// ############################# Support ################################

	/**
	 * getsStartDate : Extract StartDate String from request JSON
	 * 
	 * @param message
	 * @return String start date
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	private String getsStartDate(@Valid String message) throws JsonParseException, JsonMappingException, IOException {

		final ObjectNode node = new ObjectMapper().readValue(message, ObjectNode.class);
		if (node.has("startDate")) {
			return node.get("startDate").toString();
		}
		return null;
	}

	/**
	 * getsEndDate : Extract EndDate String from request JSON
	 * 
	 * @param message
	 * @return String end date
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	private String getsEndDate(@Valid String message) throws JsonParseException, JsonMappingException, IOException {
		final ObjectNode node = new ObjectMapper().readValue(message, ObjectNode.class);
		if (node.has("endDate")) {
			return node.get("endDate").toString();
		}
		return null;
	}

	/**
	 * getsPlatform : Extract Platform String from request JSON
	 * 
	 * @param message
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	private String getsPlatform(@Valid String message) throws JsonParseException, JsonMappingException, IOException {
		final ObjectNode node = new ObjectMapper().readValue(message, ObjectNode.class);
		if (node.has("platform")) {
			// //system.out.println("platform: " + node.get("platform"));
			return node.get("platform").toString();
		}
		return null;
	}

	/**
	 * getNextDate : get next date in string format
	 * 
	 * @param String Date
	 * @return String Next Date
	 */
	public String getNextDate(String curDate) {
		String nextDate = "";
		try {
			Calendar today = Calendar.getInstance();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date date = format.parse(curDate);
			today.setTime(date);
			today.add(Calendar.DAY_OF_YEAR, 1);
			nextDate = format.format(today.getTime());
		} catch (Exception e) {
			return nextDate;
		}
		return nextDate;
	}

	/**
	 * getNextDateObject : get next Date Object
	 * 
	 * @param Date Object
	 * @return Date Object
	 */
	public static Date getNextDateObject(Date curDate) {
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(curDate);
		calendar.add(Calendar.DAY_OF_YEAR, 1);
		return calendar.getTime();
	}

	// #################### ONBOARDING #################################

	@CrossOrigin
	@RequestMapping(value = "/getOnboardingReportByDateUserLevel", method = RequestMethod.POST)
	public String getOnboardingReportByDateUserLevel(@RequestBody String request) {
		logger.info("[Enter] getOnboardingReportByDateUserLevel");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<OnboardingDataComplete> dataList = new ArrayList<OnboardingDataComplete>();

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				dataList.add(new OnboardingDataComplete(sDate, sPlatform,
						usageDataDAO.getOnboardingLanguageViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingLanguageCompletedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingGenderViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingGenderCompletedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingCompletedCountByDateUserLevel(sPlatform, startDate)));
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				dataList.add(new OnboardingDataComplete(sDate, sPlatform,
						usageDataDAO.getOnboardingLanguageViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingLanguageCompletedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingGenderViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingGenderCompletedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingCompletedCountByDateUserLevel(sPlatform, startDate)));
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				dataList.add(new OnboardingDataComplete(sDate, sPlatform,
						usageDataDAO.getOnboardingLanguageViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingLanguageCompletedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingGenderViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingGenderCompletedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingViewedCountByDateUserLevel(sPlatform, startDate),
						usageDataDAO.getOnboardingCompletedCountByDateUserLevel(sPlatform, startDate)));
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Enter] getOnboardingReportByDateUserLevel");
		return gson.toJson(dataList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getUserTypeAvatarDetailsByDate", method = RequestMethod.POST)
	public String getUserTypeAvatarDetailsByDate(@RequestBody String request) {
		logger.info("[Enter] getUserTypeAvatarDetailsByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();

		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {

			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("Bad Request");
		}
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate;
		try {
			startDate = formatter.parse(sDate);
		} catch (ParseException e1) {
			logger.error("[ERROR] DATE not parsable");
			e1.printStackTrace();
			return gson.toJson("Invalid Date");
		}
		Date endDate;
		try {
			endDate = formatter.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
			logger.error("[ERROR] DATE not parsable");
			return gson.toJson("Invalid Date");
		}

		List<AvatarDetailUserType> avatarDetailsList = new ArrayList<AvatarDetailUserType>();

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				ArrayList<String> newAppList = (ArrayList<String>) repo.findNewUsersListByDate(sDate, sPlatform);
				Map<String, List<List<String>>> tempMap = repo.getAvatarStickerDataByDate(sDate);

				for (String appId : tempMap.keySet()) {
					// System.out.println("APP ID : " + appId);
					String userType = "";
					if (newAppList.contains(appId)) {
						userType = "New";
					} else {
						userType = "Returning";
					}

					for (List<String> row : tempMap.get(appId)) {
						avatarDetailsList
								.add(new AvatarDetailUserType(sDate, row.get(0), userType, row.get(1), row.get(2),
										row.get(3), usageDataDAO.getImageIDCountSharedByDate(row.get(1), startDate)));
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				ArrayList<String> newAppList = (ArrayList<String>) repo.findNewUsersListByDate(sDate, sPlatform);
				Map<String, List<List<String>>> tempMap = repo.getAvatarStickerDataByDate(sDate);

				for (String appId : tempMap.keySet()) {
					// System.out.println("APP ID : " + appId);
					String userType = "";
					if (newAppList.contains(appId)) {
						userType = "New";
					} else {
						userType = "Returning";
					}

					for (List<String> row : tempMap.get(appId)) {
						avatarDetailsList
								.add(new AvatarDetailUserType(sDate, row.get(0), userType, row.get(1), row.get(2),
										row.get(3), usageDataDAO.getImageIDCountSharedByDate(row.get(1), startDate)));
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("All")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}
				ArrayList<String> newAppList = (ArrayList<String>) repo.findNewUsersListByDate(sDate, sPlatform);
				Map<String, List<List<String>>> tempMap = repo.getAvatarStickerDataByDate(sDate);

				for (String appId : tempMap.keySet()) {
					// System.out.println("APP ID : " + appId);
					String userType = "";
					if (newAppList.contains(appId)) {
						userType = "New";
					} else {
						userType = "Returning";
					}

					for (List<String> row : tempMap.get(appId)) {
						avatarDetailsList
								.add(new AvatarDetailUserType(sDate, row.get(0), userType, row.get(1), row.get(2),
										row.get(3), usageDataDAO.getImageIDCountSharedByDate(row.get(1), startDate)));
					}
				}
				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		}
		logger.info("[Leave] getUserTypeAvatarDetailsByDate");
		return gson.toJson(avatarDetailsList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getUserTypeKeyboardDataByDate", method = RequestMethod.POST)
	public String getUserTypeKeyboardDataByDate(@RequestBody String request) {
		logger.info("[Enter] getUserTypeKeyboardDataByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<UserTypeKeyBoard> dataList = new ArrayList<UserTypeKeyBoard>();
		UserTypeKeyBoard pdAndroid = null;
		UserTypeKeyBoard pdiOS = null;
		UserTypeKeyBoard pdDev = null;

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}

				ArrayList<String> newAppList = (ArrayList<String>) repo.findNewUsersListByDate(sDate, sPlatform);
				ArrayList<String> appIdList = (ArrayList<String>) usageDataDAO
						.getUserTypeKeyboardCompleteCountByDateUserLevel(startDate, sPlatform);

				for (String appId : appIdList) {
					// System.out.println("APP ID : " + appId);
					String userType = "";
					if (newAppList.contains(appId)) {
						userType = "New";
					} else {
						userType = "Returning";
					}

					dataList.add(new UserTypeKeyBoard(sDate, sPlatform, appId, userType));
				}

				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}

				ArrayList<String> newAppList = (ArrayList<String>) repo.findNewUsersListByDate(sDate, sPlatform);
				ArrayList<String> appIdList = (ArrayList<String>) usageDataDAO
						.getUserTypeKeyboardCompleteCountByDateUserLevel(startDate, sPlatform);

				for (String appId : appIdList) {
					// System.out.println("APP ID : " + appId);
					String userType = "";
					if (newAppList.contains(appId)) {
						userType = "New";
					} else {
						userType = "Returning";
					}

					dataList.add(new UserTypeKeyBoard(sDate, sPlatform, appId, userType));
				}

				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}

				ArrayList<String> newAppList = (ArrayList<String>) repo.findNewUsersListByDate(sDate, sPlatform);
				ArrayList<String> appIdList = (ArrayList<String>) usageDataDAO
						.getUserTypeKeyboardCompleteCountByDateUserLevel(startDate, sPlatform);

				for (String appId : appIdList) {
					// System.out.println("APP ID : " + appId);
					String userType = "";
					if (newAppList.contains(appId)) {
						userType = "New";
					} else {
						userType = "Returning";
					}

					dataList.add(new UserTypeKeyBoard(sDate, sPlatform, appId, userType));
				}

				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getUserTypeKeyboardDataByDate");
		return gson.toJson(dataList);
	}

	@CrossOrigin
	@RequestMapping(value = "/getUserTypeAvatarCreatedByDate", method = RequestMethod.POST)
	public String getUserTypeAvatarCreatedByDate(@RequestBody String request) {
		logger.info("[Enter] getUserTypeAvatarCreatedByDate");
		String sDate = null;
		String eDate = null;
		String sPlatform = null;
		Gson gson = new Gson();
		try {
			sPlatform = getsPlatform(request).split("\"")[1];
			eDate = getsEndDate(request).split("\"")[1];
			sDate = getsStartDate(request).split("\"")[1];
		} catch (IOException e2) {
			e2.printStackTrace();
			logger.error("[ERROR] Bad Request");
			return gson.toJson("{\"message\":\"BAD REQUEST\"}");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(sDate);
			endDate = sdf.parse(eDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (startDate == null || endDate == null) {
			logger.error("[ERROR] Date Not Parsable");
			return gson.toJson("{\"message\":\"Date Not Parsable, Use yyyy-MM-dd format\"}");
		}
		ArrayList<AvatarCreateUserType> dataList = new ArrayList<AvatarCreateUserType>();

		if (sPlatform.equals("Android")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}

				ArrayList<String> newAppList = (ArrayList<String>) repo.findNewUsersListByDate(sDate, sPlatform);
				ArrayList<String> appIdList = (ArrayList<String>) usageDataDAO
						.findUserTypeAvatarCreatedByDate(sPlatform, startDate);

				for (String appId : appIdList) {
					// System.out.println("APP ID : " + appId);
					String userType = "";
					if (newAppList.contains(appId)) {
						userType = "New";
					} else {
						userType = "Returning";
					}

					dataList.add(new AvatarCreateUserType(sDate, sPlatform, appId, userType));
				}

				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.equals("iOS")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}

				ArrayList<String> newAppList = (ArrayList<String>) repo.findNewUsersListByDate(sDate, sPlatform);
				ArrayList<String> appIdList = (ArrayList<String>) usageDataDAO
						.findUserTypeAvatarCreatedByDate(sPlatform, startDate);

				for (String appId : appIdList) {
					// System.out.println("APP ID : " + appId);
					String userType = "";
					if (newAppList.contains(appId)) {
						userType = "New";
					} else {
						userType = "Returning";
					}

					dataList.add(new AvatarCreateUserType(sDate, sPlatform, appId, userType));
				}

				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else if (sPlatform.toUpperCase().equals("ALL")) {
			while (true) {
				if (startDate.compareTo(endDate) > 0) {
					break;
				}

				ArrayList<String> newAppList = (ArrayList<String>) repo.findNewUsersListByDate(sDate, sPlatform);
				ArrayList<String> appIdList = (ArrayList<String>) usageDataDAO
						.findUserTypeAvatarCreatedByDate(sPlatform, startDate);

				for (String appId : appIdList) {
					// System.out.println("APP ID : " + appId);
					String userType = "";
					if (newAppList.contains(appId)) {
						userType = "New";
					} else {
						userType = "Returning";
					}
					dataList.add(new AvatarCreateUserType(sDate, sPlatform, appId, userType));
				}

				sDate = getNextDate(sDate);
				startDate = getNextDateObject(startDate);
			}
		} else {
			logger.error("[ERROR] Invalid Platform");
			return gson.toJson("{\"message\":\"Invalid Platform\"}");
		}
		logger.info("[Leave] getUserTypeAvatarCreatedByDate");
		return gson.toJson(dataList);
	}
}
