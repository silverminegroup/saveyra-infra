package com.saveyra.analytics.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import com.amazonaws.services.cloudwatch.model.Datapoint;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsRequest;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsResult;
import com.amazonaws.services.cloudwatch.model.StandardUnit;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saveyra.analytics.data.RequestData;
import com.saveyra.analytics.data.RestResponse;
import com.saveyra.analytics.data.StatisticsData;

@RestController
@RequestMapping("/")
public class MetricsController {
	private static String accessKey = System.getenv("SAV_ACCESSKEY");
	
	//@Value("${secret.key}")
	private static String secretKey = System.getenv("SAV_SECRETKEY");
	
	private static final String PLATFORM = "Platform";
	private static final String GCM = "GCM";
	private static final String APNS = "APNS";
	private static final Logger LOGGER = LoggerFactory.getLogger(MetricsController.class);
	final static long offsetInMilliseconds = 1000 * 60 * 60 * 24;
	//final static BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAIIGSDYMQGEZV47ZA", "BR96gGn+HOVieXRydNwTxur7HsMSImaDvrMiK2cy");
	final static BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey,secretKey);
	final static AmazonCloudWatch cw = AmazonCloudWatchClientBuilder.standard()
			.withCredentials(new AWSStaticCredentialsProvider(awsCreds)).withRegion("ap-south-1").build();
	final static String namespace = "AWS/SNS";
	final static String deliveredMetricName = "NumberOfNotificationsDelivered";
	final static String failedMetricName = "NumberOfNotificationsFailed";
	final static String publishedMetricName = "NumberOfMessagesPublished";
	final static String[] statistics = {"Sum"};
	final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
	final static ObjectMapper mapper = new ObjectMapper();
	
	public enum Platform {
		ANDROID("Android"), IOS("iOS"), ALL("All") ;
		private String platform;
		Platform(String platform) {
			this.platform = platform;
		}
		/**
		 * @return the platform
		 */
		public String getPlatform() {
			return platform;
		}
	}

	@CrossOrigin
	@PostMapping("/metrics")
	public RestResponse getAllMetrics(@RequestBody RequestData requestData) throws ParseException {
		LOGGER.info("getAllMetrics()==>GET /metrics call is initiated.");
		RestResponse response = new RestResponse();
		if(requestData == null) {
			return getFailureResponse(response, "Invalid request.");
		}
		LOGGER.info("getAllMetrics()==>startDate: {}, endDate: {}, platform: {}", requestData.getStartDate(),
				requestData.getEndDate(), requestData.getPlatform());
		
		if(requestData.getStartDate() == null) {
			return getFailureResponse(response, "Invalid startDate.");
		}
		
		if(requestData.getEndDate() == null) {
			return getFailureResponse(response, "Invalid endDate.");
		}
		
		if(requestData.getPlatform() == null || !Arrays.asList(Platform.values()).contains(Platform.valueOf(requestData.getPlatform().toUpperCase()))) {
			return getFailureResponse(response, "Invalid platform.");
		}
		
		List<Dimension> platforms = new ArrayList<>();
		if(Platform.IOS.getPlatform().equalsIgnoreCase(requestData.getPlatform())) {
			Dimension iosPm = new Dimension();
			iosPm.setName(PLATFORM);
			iosPm.setValue(APNS);
			platforms.add(iosPm);
		} else if(Platform.ANDROID.getPlatform().equalsIgnoreCase(requestData.getPlatform())) {
			Dimension androidPm = new Dimension();
			androidPm.setName(PLATFORM);
			androidPm.setValue(GCM);
			platforms.add(androidPm);
		} else {
			Dimension iosPm = new Dimension();
			iosPm.setName(PLATFORM);
			iosPm.setValue(APNS);
			platforms.add(iosPm);
			
			Dimension androidPm = new Dimension();
			androidPm.setName(PLATFORM);
			androidPm.setValue(GCM);
			platforms.add(androidPm);
		}
		Map<String, Map<String, Map<String, Double>>> byPlatformStatistics = new HashMap<>();
		byPlatformStatistics = getStatisticsByPlatform(platforms, byPlatformStatistics, requestData.getStartDate(), requestData.getEndDate());
		
		List<StatisticsData> statistics = new ArrayList<>();
		
		if(byPlatformStatistics.get("iOS") != null) {
			if(byPlatformStatistics.get("iOS").get("published") != null) {
				for(String date: byPlatformStatistics.get("iOS").get("published").keySet()) {
					StatisticsData statistic = new StatisticsData();
					statistic.setDate(date);
					statistic.setPlatform(Platform.IOS.getPlatform());
					statistic.setPublished((byPlatformStatistics.get("iOS").get("published").get(date) != null)?byPlatformStatistics.get("iOS").get("published").get(date).intValue():0);
					statistic.setDelivered((byPlatformStatistics.get("iOS").get("delivered").get(date) != null)?byPlatformStatistics.get("iOS").get("delivered").get(date).intValue():0);
					statistic.setFailed((byPlatformStatistics.get("iOS").get("failed").get(date) != null)?byPlatformStatistics.get("iOS").get("failed").get(date).intValue():0);
					statistics.add(statistic);
				}
			}
		}
		if(byPlatformStatistics.get("Android") != null) {
			if(byPlatformStatistics.get("Android").get("published") != null) {
				for(String date: byPlatformStatistics.get("Android").get("published").keySet()) {
					StatisticsData statistic = new StatisticsData();
					statistic.setDate(date);
					statistic.setPlatform(Platform.ANDROID.getPlatform());
					statistic.setPublished((byPlatformStatistics.get("Android").get("published").get(date) != null)?byPlatformStatistics.get("Android").get("published").get(date).intValue():0);
					statistic.setDelivered((byPlatformStatistics.get("Android").get("delivered").get(date) != null)?byPlatformStatistics.get("Android").get("delivered").get(date).intValue():0);
					statistic.setFailed((byPlatformStatistics.get("Android").get("failed").get(date) != null)?byPlatformStatistics.get("Android").get("failed").get(date).intValue():0);
					statistics.add(statistic);
				}
			}
		}
		response.setData(statistics);
		LOGGER.info("getAllMetrics()==>GET /metrics call is end.");
		return getSuccessResponse(response, "Metrics statistics retrieved successfully.");
	}
	
	private RestResponse getFailureResponse(RestResponse response, String statusMessage) {
		LOGGER.info("getFailureResponse()==>Entered");
		response.setStatus("FAILURE");
		response.setStatusMessage(statusMessage);
		LOGGER.info("getFailureResponse()==>Exit");
		return response;
	}
	
	private RestResponse getSuccessResponse(RestResponse response, String statusMessage) {
		LOGGER.info("getSuccessResponse()==>Entered");
		response.setStatus("SUCCESS");
		response.setStatusMessage(statusMessage);
		LOGGER.info("getSuccessResponse()==>Exit");
		return response;
	}
	
	//TODO:: It will be used if we search by Application
	/*private static Map<String, Map<String, Map<Date, Double>>> getStatisticsByApplication(List<Dimension> applications, 
			Map<String, Map<String, Map<Date, Double>>> byApplicationStatistics, Date startDate, Date endDate) {
		applications.forEach(byApplication->{
			Map<String, Map<Date, Double>> byApplicationMetricStatistics = new HashMap<>();
			Map<Date, Double> publishedStatistics = getStatisticsData(namespace, publishedMetricName, statistics, byApplication, startDate, endDate);
			byApplicationMetricStatistics.put("published", publishedStatistics);
			Map<Date, Double> deliveredStatistics = getStatisticsData(namespace, deliveredMetricName, statistics, byApplication, startDate, endDate);
			byApplicationMetricStatistics.put("delivered", deliveredStatistics);
			Map<Date, Double> failedStatistics = getStatisticsData(namespace, failedMetricName, statistics, byApplication, startDate, endDate);
			byApplicationMetricStatistics.put("failed", failedStatistics);
			byApplicationStatistics.put(byApplication.getValue(), byApplicationMetricStatistics);
		});
		return byApplicationStatistics;
	}*/
	
	private Map<String, Map<String, Map<String, Double>>> getStatisticsByPlatform(List<Dimension> platforms, 
			Map<String, Map<String, Map<String, Double>>> byPlatformStatistics, Date startDate, Date endDate) {
		LOGGER.info("getStatisticsByPlatform()==>Entered.");
		platforms.forEach(byPlatform->{
			Map<String, Map<String, Double>> byPlatformMetricStatistics = new HashMap<>();
			Map<String, Double> publishedStatistics = getStatisticsData(namespace, publishedMetricName, statistics, byPlatform, startDate, endDate);
			byPlatformMetricStatistics.put("published", publishedStatistics);
			Map<String, Double> deliveredStatistics = getStatisticsData(namespace, deliveredMetricName, statistics, byPlatform, startDate, endDate);
			byPlatformMetricStatistics.put("delivered", deliveredStatistics);
			Map<String, Double> failedStatistics = getStatisticsData(namespace, failedMetricName, statistics, byPlatform, startDate, endDate);
			byPlatformMetricStatistics.put("failed", failedStatistics);
			if(GCM.equalsIgnoreCase(byPlatform.getValue())) {
				byPlatformStatistics.put(Platform.ANDROID.getPlatform(), byPlatformMetricStatistics);
			} else if(APNS.equalsIgnoreCase(byPlatform.getValue())) {
				byPlatformStatistics.put(Platform.IOS.getPlatform(), byPlatformMetricStatistics);
			}
		});
		LOGGER.info("getStatisticsByPlatform()==>Exit.");
		return byPlatformStatistics;
	}
	
	private  Map<String, Double> getStatisticsData(String namespace, String metricName, String[] statistics,
			Dimension filters, Date startDate, Date endDate) {
		LOGGER.info("getStatisticsData()==>Entered.");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		Calendar c = Calendar.getInstance();
	    c.setTime(startDate);
	    c.set(Calendar.HOUR_OF_DAY, 0);
	    c.set(Calendar.MINUTE, 0);
	    c.set(Calendar.SECOND, 0);
	    Date start = c.getTime();
	    
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(endDate);
	    cal.add(Calendar.DAY_OF_MONTH, 1);
	    cal.set(Calendar.HOUR_OF_DAY, 0);
	    cal.set(Calendar.MINUTE, 0);
	    cal.set(Calendar.SECOND, 0);
	    Date end = cal.getTime();
	    
	    LOGGER.info("getStatisticsData()==>startDate: {}, endDate: {}.", startDate, endDate);

	    long noOfDays = ChronoUnit.DAYS.between(startDate.toInstant(), endDate.toInstant());
	    int seconds = 60 * 60;
	    LOGGER.info("getStatisticsData()==>noOfDays: {}", noOfDays);
	    if(noOfDays > 14 && noOfDays<= 63) {
	    	seconds = 300 * 300;
	    } else if(noOfDays > 63) {
	    	seconds = 3600 * 3600;
	    }
	    //int seconds = (int)TimeUnit.DAYS.toSeconds(noOfDays);
	    LOGGER.info("getStatisticsData()==>seconds: {}", seconds);
		GetMetricStatisticsRequest request;
		Map<String, Double> sumStatisticsData = new HashMap<>();
		try {
			request = new GetMetricStatisticsRequest()
					.withStartTime(sdf.parse(sdf.format(start))).withNamespace(namespace)
					.withPeriod(seconds).withMetricName(metricName).withStatistics(statistics).withUnit(StandardUnit.Count)
					.withDimensions(filters).withEndTime(sdf.parse(sdf.format(end)));
			GetMetricStatisticsResult response = cw.getMetricStatistics(request);

			Calendar ca = Calendar.getInstance();
			// To read the Data
			for (Datapoint dp : response.getDatapoints()) {
				DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

				DateFormat istFormat = new SimpleDateFormat("yyyy-MM-dd");
				istFormat.setTimeZone(TimeZone.getTimeZone("IST"));

				ca.setTime(dp.getTimestamp());
				ca.set(Calendar.HOUR_OF_DAY, 0);
				ca.set(Calendar.MINUTE, 0);
				ca.set(Calendar.SECOND, 0);
				if(sumStatisticsData.get(istFormat.format(ca.getTime())) == null) {
					sumStatisticsData.put(istFormat.format(ca.getTime()), dp.getSum());
				} else {
					sumStatisticsData.put(istFormat.format(ca.getTime()), sumStatisticsData.get(istFormat.format(ca.getTime())) + dp.getSum());
				}
			}
		} catch (ParseException e) {
			LOGGER.error("getStatisticsByPlatform()==>error: {}.", e.getMessage(), e);
		}
		LOGGER.info("getStatisticsByPlatform()==>Exit.");
		return sumStatisticsData;
	}
}
