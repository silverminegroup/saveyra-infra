package com.saveyra.analytics.repository;

import java.util.Date;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import com.saveyra.analytics.model.UsageData;

@Repository
public interface PersonRepository extends MongoRepository<UsageData, String> {
	
			
	@Query("{ 'eventCode' : '43', 'platform' : 'Android', 'dateTime' : { '$gte' : { '$date' : 1538438400000 } }, '$and' : [{ 'dateTime' : { '$lte' : { '$date' : 1538438400000 } } }] }")
	public long findAvtarCreatedReportBydateTime(Date dateTime);

}
