package com.saveyra.analytics.targetApp;

public class ShareTargetData {	
	private String Date;
	private String Platform;
	private String TargetApplication;
	private long Count;
	
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public String getTargetApplication() {
		return TargetApplication;
	}
	public void setTargetApplication(String targetApplication) {
		TargetApplication = targetApplication;
	}
	public long getCount() {
		return Count;
	}
	public void setCount(long count) {
		Count = count;
	}
	public ShareTargetData(String date, String platform, String targetApplication, long count) {
		super();
		Date = date;
		Platform = platform;
		TargetApplication = targetApplication;
		Count = count;
	}
}
