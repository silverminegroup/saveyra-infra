package com.saveyra.analytics.avatarHelper;

public class AvatarDetailUserType {
	private String Date;	
	private String ApplicationID;
	private String UserType;
	private String ImageID;
	private String URL;
	private String Expression;
	private long ShareCount;
	
	public AvatarDetailUserType(String date, String applicationID, String userType, String imageID, String uRL,
			String expression, long shareCount) {
		super();
		Date = date;
		ApplicationID = applicationID;
		UserType = userType;
		ImageID = imageID;
		URL = uRL;
		Expression = expression;
		ShareCount = shareCount;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public String getApplicationID() {
		return ApplicationID;
	}

	public void setApplicationID(String applicationID) {
		ApplicationID = applicationID;
	}

	public String getUserType() {
		return UserType;
	}

	public void setUserType(String userType) {
		UserType = userType;
	}

	public String getImageID() {
		return ImageID;
	}

	public void setImageID(String imageID) {
		ImageID = imageID;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public String getExpression() {
		return Expression;
	}

	public void setExpression(String expression) {
		Expression = expression;
	}

	public long getShareCount() {
		return ShareCount;
	}

	public void setShareCount(long shareCount) {
		ShareCount = shareCount;
	}	
}
