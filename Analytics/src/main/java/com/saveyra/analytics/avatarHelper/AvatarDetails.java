package com.saveyra.analytics.avatarHelper;

public class AvatarDetails {
	String Date;	
	String ApplicationID;
	String ImageID;
	String URL;
	String Expression;
	long ShareCount;
	
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	
	public String getApplicationID() {
		return ApplicationID;
	}
	public void setApplicationID(String applicationID) {
		ApplicationID = applicationID;
	}
	public String getImageID() {
		return ImageID;
	}
	public void setImageID(String imageID) {
		ImageID = imageID;
	}
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	public String getExpression() {
		return Expression;
	}
	public void setExpression(String expression) {
		Expression = expression;
	}
	public long getShareCount() {
		return ShareCount;
	}
	public void setShareCount(long shareCount) {
		ShareCount = shareCount;
	}
	public AvatarDetails(String date, String applicationID, String imageID, String uRL, String expression, long shareCount) {
		super();
		Date = date;
		ApplicationID = applicationID;
		ImageID = imageID;
		URL = uRL;
		Expression = expression;
		ShareCount = shareCount;
	}	
}
