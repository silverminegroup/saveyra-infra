package com.saveyra.analytics.avatarHelper;

public class ExtendedAvatarHelper {	
	private String Date;
	private String Platform;
	private long Initialized_Event;
	private long Initialized_User;
	private long Created_Event;
	private long Created_User;
	private long Reset_Event;
	private long Reset_User;
	
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public long getInitialized_Event() {
		return Initialized_Event;
	}
	public void setInitialized_Event(long initialized_Event) {
		Initialized_Event = initialized_Event;
	}
	public long getInitialized_User() {
		return Initialized_User;
	}
	public void setInitialized_User(long initialized_User) {
		Initialized_User = initialized_User;
	}
	public long getCreated_Event() {
		return Created_Event;
	}
	public void setCreated_Event(long created_Event) {
		Created_Event = created_Event;
	}
	public long getCreated_User() {
		return Created_User;
	}
	public void setCreated_User(long created_User) {
		Created_User = created_User;
	}
	public long getReset_Event() {
		return Reset_Event;
	}
	public void setReset_Event(long reset_Event) {
		Reset_Event = reset_Event;
	}
	public long getReset_User() {
		return Reset_User;
	}
	public void setReset_User(long reset_User) {
		Reset_User = reset_User;
	}
	public ExtendedAvatarHelper(String date, String platform, long initialized_Event, long initialized_User,
			long created_Event, long created_User, long reset_Event, long reset_User) {
		super();
		Date = date;
		Platform = platform;
		Initialized_Event = initialized_Event;
		Initialized_User = initialized_User;
		Created_Event = created_Event;
		Created_User = created_User;
		Reset_Event = reset_Event;
		Reset_User = reset_User;
	}	
}