package com.saveyra.analytics.avatarHelper;

public class GenderReport {
	private String Date;
	private String Platform;
	private long Male;
	private long Female;
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public long getMale() {
		return Male;
	}
	public void setMale(long male) {
		Male = male;
	}
	public long getFemale() {
		return Female;
	}
	public void setFemale(long female) {
		Female = female;
	}
	public GenderReport(String date, String platform, long male, long female) {
		super();
		Date = date;
		Platform = platform;
		Male = male;
		Female = female;
	}
	
	
 }
