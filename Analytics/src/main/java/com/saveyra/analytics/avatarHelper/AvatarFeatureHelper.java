package com.saveyra.analytics.avatarHelper;

public class AvatarFeatureHelper {
	private String Date;
	private String Platform;
	private String AvatarFeature;
	private String AvatarFeatureID;
	private String ViewCount;
	private String SelectCount;
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getAvatarFeature() {
		return AvatarFeature;
	}
	public void setAvatarFeature(String avatarFeature) {
		AvatarFeature = avatarFeature;
	}
	public String getAvatarFeatureID() {
		return AvatarFeatureID;
	}
	public void setAvatarFeatureID(String avatarFeatureID) {
		AvatarFeatureID = avatarFeatureID;
	}
	public String getViewCount() {
		return ViewCount;
	}
	public void setViewCount(String viewCount) {
		ViewCount = viewCount;
	}
	public String getSelectCount() {
		return SelectCount;
	}
	public void setSelectCount(String selectCount) {
		SelectCount = selectCount;
	}
	
	public AvatarFeatureHelper(String date,String platform, String avatarFeature, String avatarFeatureID, String viewCount,
			String selectCount) {
		super();
		Date = date;
		Platform = platform;
		AvatarFeature = avatarFeature;
		AvatarFeatureID = avatarFeatureID;
		ViewCount = viewCount;
		SelectCount = selectCount;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}	
}
