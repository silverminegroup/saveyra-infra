package com.saveyra.analytics.avatarHelper;

public class AvatarReport {
	private String Date;
	private String Platform;
	private long Initialized;
	private long Created;
	private long Reset;
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public long getInitialized() {
		return Initialized;
	}
	public void setInitialized(long initialized) {
		Initialized = initialized;
	}
	public long getCreated() {
		return Created;
	}
	public void setCreated(long created) {
		Created = created;
	}
	public long getReset() {
		return Reset;
	}
	public void setReset(long reset) {
		Reset = reset;
	}
	public AvatarReport(String date, String platform, long initialized, long created, long reset) {
		super();
		Date = date;
		Platform = platform;
		Initialized = initialized;
		Created = created;
		Reset = reset;
	}
	
		
}
