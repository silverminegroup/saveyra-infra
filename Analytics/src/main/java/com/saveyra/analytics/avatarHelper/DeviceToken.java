package com.saveyra.analytics.avatarHelper;

public class DeviceToken {
	private String devicetoken;

	public String getDevicetoken() {
		return devicetoken;
	}

	public void setDevicetoken(String devicetoken) {
		this.devicetoken = devicetoken;
	}

	public DeviceToken(String devicetoken) {
		super();
		this.devicetoken = devicetoken;
	}
	
}
