package com.saveyra.analytics.model;

public class AppIdIPModel {
	private String Date;
	private String Platform;
	private String AppID;
	private String IP;
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public String getAppID() {
		return AppID;
	}
	public void setAppID(String appID) {
		AppID = appID;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public AppIdIPModel(String date, String platform, String appID, String iP) {
		super();
		Date = date;
		Platform = platform;
		AppID = appID;
		IP = iP;
	}
}
