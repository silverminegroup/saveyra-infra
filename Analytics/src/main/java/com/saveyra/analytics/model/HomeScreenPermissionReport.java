package com.saveyra.analytics.model;

public class HomeScreenPermissionReport {
	private String Date;
	private String Platform;
	private String Permission;
	private String Accepted;
	private String Denied;
	private String Screen;
	
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public String getPermission() {
		return Permission;
	}
	public void setPermission(String permission) {
		Permission = permission;
	}
	public String getAccepted() {
		return Accepted;
	}
	public void setAccepted(String accepted) {
		Accepted = accepted;
	}
	public String getDenied() {
		return Denied;
	}
	public void setDenied(String denied) {
		Denied = denied;
	}
	public String getScreen() {
		return Screen;
	}
	public void setScreen(String screen) {
		Screen = screen;
	}
	public HomeScreenPermissionReport(String date, String platform, String permission, String accepted, String denied,
			String screen) {
		super();
		Date = date;
		Platform = platform;
		Permission = permission;
		Accepted = accepted;
		Denied = denied;
		Screen = screen;
	}
	
	

}
