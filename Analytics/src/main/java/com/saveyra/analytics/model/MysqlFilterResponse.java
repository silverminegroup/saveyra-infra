package com.saveyra.analytics.model;

public class MysqlFilterResponse {
	private String devicetoken;
	private String userId;
	private String applicationId;	
	private String platform;
	
	
	public MysqlFilterResponse(String devicetoken,String userId, String applicationId,  String platform ) {
		super();
		this.devicetoken = devicetoken;
		this.userId = userId;
		this.setApplicationId(applicationId);		
		this.platform = platform;
		
	}



	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getDevicetoken() {
		return devicetoken;
	}

	public void setDevicetoken(String devicetoken) {
		this.devicetoken = devicetoken;
	}



	public String getApplicationId() {
		return applicationId;
	}



	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}	
}
