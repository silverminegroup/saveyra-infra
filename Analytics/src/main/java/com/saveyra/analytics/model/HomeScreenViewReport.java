package com.saveyra.analytics.model;

public class HomeScreenViewReport {
		
		private String Date;
		private String Platform;
		private String Card;
		private String ViewCount;
		private String ShareCount;
		
		public String getDate() {
			return Date;
		}
		public void setDate(String date) {
			Date = date;
		}
		public String getPlatform() {
			return Platform;
		}
		public void setPlatform(String platform) {
			Platform = platform;
		}
		public String getCard() {
			return Card;
		}
		public void setCard(String card) {
			Card = card;
		}
		public String getViewCount() {
			return ViewCount;
		}
		public void setViewCount(String viewCount) {
			ViewCount = viewCount;
		}
		public HomeScreenViewReport(String date, String platform, String card, String viewCount,String shareCount) {
			super();
			Date = date;
			Platform = platform;
			Card = card;
			ViewCount = viewCount;
			ShareCount = shareCount;
		}
		public String getShareCount() {
			return ShareCount;
		}
		public void setShareCount(String shareCount) {
			ShareCount = shareCount;
		}
}
