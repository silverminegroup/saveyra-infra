package com.saveyra.analytics.model;

import java.util.Map;

public class HomeScreenReport {
	
	private String Date;
	private String Platform;
	private String Card;
	private String ViewCount;
	private String ShareCount;
	private String HomeScreenPosition;
	private Map<String,Integer> targetMap;
	private String PollOption1Count;
	private String PollOption2Count;
	private String PollOptionTotal;
	private String MCQOption1Count;
	private String MCQOption2Count;
	private String MCQOption3Count;
	private String MCQOption4Count;
	private String MCQCorrectCount;
	
	public String getDate() {
		return Date;
	}
	
	public void setDate(String date) {
		Date = date;
	}
	
	public String getPlatform() {
		return Platform;
	}
	
	public void setPlatform(String platform) {
		Platform = platform;
	}
	
	public String getCard() {
		return Card;
	}
	
	public void setCard(String card) {
		Card = card;
	}
	
	public String getViewCount() {
		return ViewCount;
	}
	
	public void setViewCount(String viewCount) {
		ViewCount = viewCount;
	}
	
	public String getShareCount() {
		return ShareCount;
	}
	
	public void setShareCount(String shareCount) {
		ShareCount = shareCount;
	}
	
	public String getHomeScreenPosition() {
		return HomeScreenPosition;
	}
	
	public void setHomeScreenPosition(String homeScreenPosition) {
		HomeScreenPosition = homeScreenPosition;
	}
	
	public Map<String, Integer> getTargetMap() {
		return targetMap;
	}
	
	public void setTargetMap(Map<String, Integer> targetMap) {
		this.targetMap = targetMap;
	}
	
	public String getPollOption1Count() {
		return PollOption1Count;
	}
	
	public void setPollOption1Count(String pollOption1Count) {
		PollOption1Count = pollOption1Count;
	}
	
	public String getPollOption2Count() {
		return PollOption2Count;
	}
	
	public void setPollOption2Count(String pollOption2Count) {
		PollOption2Count = pollOption2Count;
	}
	
	public String getPollOptionTotal() {
		return PollOptionTotal;
	}
	
	public void setPollOptionTotal(String pollOptionTotal) {
		PollOptionTotal = pollOptionTotal;
	}
	
	public String getMCQOption1Count() {
		return MCQOption1Count;
	}
	
	public void setMCQOption1Count(String mCQOption1Count) {
		MCQOption1Count = mCQOption1Count;
	}
	
	public String getMCQOption2Count() {
		return MCQOption2Count;
	}
	
	public void setMCQOption2Count(String mCQOption2Count) {
		MCQOption2Count = mCQOption2Count;
	}
	
	public String getMCQOption3Count() {
		return MCQOption3Count;
	}
	
	public void setMCQOption3Count(String mCQOption3Count) {
		MCQOption3Count = mCQOption3Count;
	}
	
	public String getMCQOption4Count() {
		return MCQOption4Count;
	}
	
	public void setMCQOption4Count(String mCQOption4Count) {
		MCQOption4Count = mCQOption4Count;
	}
	
	public String getMCQCorrectCount() {
		return MCQCorrectCount;
	}
	
	public void setMCQCorrectCount(String mCQCorrectCount) {
		MCQCorrectCount = mCQCorrectCount;
	}
	
	public HomeScreenReport(String date, String platform, String card, String viewCount, String shareCount,
			String homeScreenPosition, Map<String, Integer> targetMap, String pollOption1Count, String pollOption2Count,
			String pollOptionTotal, String mCQOption1Count, String mCQOption2Count, String mCQOption3Count,
			String mCQOption4Count, String mCQCorrectCount) {
		super();
		Date = date;
		Platform = platform;
		Card = card;
		ViewCount = viewCount;
		ShareCount = shareCount;
		HomeScreenPosition = homeScreenPosition;
		this.targetMap = targetMap;
		PollOption1Count = pollOption1Count;
		PollOption2Count = pollOption2Count;
		PollOptionTotal = pollOptionTotal;
		MCQOption1Count = mCQOption1Count;
		MCQOption2Count = mCQOption2Count;
		MCQOption3Count = mCQOption3Count;
		MCQOption4Count = mCQOption4Count;
		MCQCorrectCount = mCQCorrectCount;
	}
}
