package com.saveyra.analytics.model;

public class NotificationCenter {
	private int id;
	private String notificationtitle;
	private String message;
	private String navigation_to;
	private String thumbnail;
	private String image_id;
	private String createdDate;	
	private String stickerCategory;
	private boolean read;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNotificationtitle() {
		return notificationtitle;
	}
	public void setNotificationtitle(String notificationtitle) {
		this.notificationtitle = notificationtitle;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getNavigation_to() {
		return navigation_to;
	}
	public void setNavigation_to(String navigation_to) {
		this.navigation_to = navigation_to;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getImage_id() {
		return image_id;
	}
	public void setImage_id(String image_id) {
		this.image_id = image_id;
	}
	public boolean isRead() {
		return read;
	}
	public void setRead(boolean read) {
		this.read = read;
	}
	public NotificationCenter(int id, String notificationtitle, String message, String navigation_to, String thumbnail,
			String image_id,String createdDate,  boolean read, String stickerCategory) {
		super();
		this.id = id;
		this.notificationtitle = notificationtitle;
		this.message = message;
		this.navigation_to = navigation_to;
		this.thumbnail = thumbnail;
		this.createdDate = createdDate;
		this.image_id = image_id;
		this.read = read;
		this.stickerCategory = stickerCategory;
	}
	public String getStickerCategory() {
		return stickerCategory;
	}
	public void setStickerCategory(String stickerCategory) {
		this.stickerCategory = stickerCategory;
	}
	
	
}
