package com.saveyra.analytics.model;

public class HomeScreenReminderReport {
	private String Date;
	private String Platform;
	private String Reminder;
	private long Count;
	private String Screen;
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public String getReminder() {
		return Reminder;
	}
	public void setReminder(String reminder) {
		Reminder = reminder;
	}
	public long getCount() {
		return Count;
	}
	public void setCount(long count) {
		Count = count;
	}
	public String getScreen() {
		return Screen;
	}
	public void setScreen(String screen) {
		Screen = screen;
	}
	public HomeScreenReminderReport(String date, String platform, String reminder, long count, String screen) {
		super();
		Date = date;
		Platform = platform;
		Reminder = reminder;
		Count = count;
		Screen = screen;
	}
	
	
}
