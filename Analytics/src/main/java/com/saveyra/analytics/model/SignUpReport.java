package com.saveyra.analytics.model;

import java.util.List;

import com.saveyra.analytics.signUpHelper.PlatformData;

public class SignUpReport {
	List<PlatformData> platformList;
	
	public SignUpReport(List<PlatformData> platformList) {
		this.platformList = platformList;
	}
}
