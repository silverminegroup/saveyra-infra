package com.saveyra.analytics.model;

public class HomeScreenCard {
	
	private String Date;
	private String Platform;
	private String Card;
	private String ViewCount;
	private String ShareCount;
	private String HomeScreenPosition;
	private String targetMap;
	private String OptionIndex;
	private String MCQIsCorrect;
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPlatform() {
		return Platform;
	}
	public void setPlatform(String platform) {
		Platform = platform;
	}
	public String getCard() {
		return Card;
	}
	public void setCard(String card) {
		Card = card;
	}
	public String getViewCount() {
		return ViewCount;
	}
	public void setViewCount(String viewCount) {
		ViewCount = viewCount;
	}
	public String getShareCount() {
		return ShareCount;
	}
	public void setShareCount(String shareCount) {
		ShareCount = shareCount;
	}
	public String getHomeScreenPosition() {
		return HomeScreenPosition;
	}
	public void setHomeScreenPosition(String homeScreenPosition) {
		HomeScreenPosition = homeScreenPosition;
	}
	public String getTargetMap() {
		return targetMap;
	}
	public void setTargetMap(String targetMap) {
		this.targetMap = targetMap;
	}
	public String getOptionIndex() {
		return OptionIndex;
	}
	public void setOptionIndex(String optionIndex) {
		OptionIndex = optionIndex;
	}
	public String getMCQIsCorrect() {
		return MCQIsCorrect;
	}
	public void setMCQIsCorrect(String mCQIsCorrect) {
		MCQIsCorrect = mCQIsCorrect;
	}
	public HomeScreenCard(String date, String platform, String card, String viewCount, String shareCount,
			String homeScreenPosition, String targetMap, String optionIndex, String mCQIsCorrect) {
		super();
		Date = date;
		Platform = platform;
		Card = card;
		ViewCount = viewCount;
		ShareCount = shareCount;
		HomeScreenPosition = homeScreenPosition;
		this.targetMap = targetMap;
		OptionIndex = optionIndex;
		MCQIsCorrect = mCQIsCorrect;
	}	
}
