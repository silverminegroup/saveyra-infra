package com.saveyra.analytics.model;

import java.util.List;

public class MysqlResponse {
	private List<MysqlFilterResponse> response;

	public List<MysqlFilterResponse> getResponse() {
		return response;
	}

	public void setResponse(List<MysqlFilterResponse> response) {
		this.response = response;
	}	
}
