package com.saveyra.analytics.categoryHelper;

import java.util.HashMap;

public class CategoryData {
	private String date;
	private String platform;
	private HashMap<String, String> categoryMap;

	public CategoryData(String sDate, String sPlatform, HashMap<String, String> mCategoryMap) {
		this.setDate(sDate);
		this.setPlatform(sPlatform);
		this.setCategoryMap(mCategoryMap);
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public HashMap<String, String> getCategoryMap() {
		return categoryMap;
	}

	public void setCategoryMap(HashMap<String, String> categoryMap) {
		this.categoryMap = categoryMap;
	}
}
