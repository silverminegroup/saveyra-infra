package com.saveyra.analytics.categoryHelper;

import java.util.LinkedHashMap;
import java.util.Map;

public class CatagoryParser {
	private Map<String,String> stickerShared = new LinkedHashMap<String,String>();

	public Map<String,String> getStickerShared() {
		return stickerShared;
	}

	public void setStickerShared(Map<String,String> stickerShared) {
		this.stickerShared = stickerShared;
	}
}
