package com.saveyra.analytics.categoryHelper;

import java.util.HashMap;

public class CategoryDataTotal {
	private String platform;
	private HashMap<String, String> categoryMap;

	public CategoryDataTotal(String platform, HashMap<String, String> categoryMap) {
		super();
		this.platform = platform;
		this.categoryMap = categoryMap;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public HashMap<String, String> getCategoryMap() {
		return categoryMap;
	}

	public void setCategoryMap(HashMap<String, String> categoryMap) {
		this.categoryMap = categoryMap;
	}

}
