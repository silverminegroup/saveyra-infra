package com.saveyra.scheduler.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.saveyra.scheduler.ExecutorServiceSingleton;
import com.saveyra.scheduler.SampleJob;
import com.saveyra.scheduler.data.RequestModelJson;
import com.saveyra.scheduler.data.RestResponse;
import com.saveyra.scheduler.model.FeederTasksEntity;
import com.saveyra.scheduler.service.FeederTasksService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ScheduleTasksController {
	private static final String CANCELLED = "CANCELLED";
	private static final String PENDING = "PENDING";
	private static final SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");  
	private static final Logger LOGGER = LoggerFactory.getLogger(ScheduleTasksController.class);
	@Autowired
	private FeederTasksService feederTasksService;
	@Value("${feeder.url}")
	private String feederUrl;

	@Value("${notification.url}")
	private String notificationUrl;
	
	@PostConstruct
	public void init(){
		//Schedule task
		ScheduledExecutorService schedule = ExecutorServiceSingleton.getInstance();
		List<FeederTasksEntity> tasks = feederTasksService.getByStatus(PENDING);
		tasks.forEach(task->{
			Date now = new Date();
			long total = 0l;
			if(now.before(task.getSchedTime())) {
				total = task.getSchedTime().getTime() - now.getTime();
				LOGGER.info("before: {}", total);
			} else {
				total =  task.getSchedTime().getTime() - task.getSchedTime().getTime();
				LOGGER.info("after: {}", total);
			}
			schedule.schedule(new SampleJob(feederTasksService, feederUrl, notificationUrl), total, TimeUnit.MILLISECONDS);
		});
	 }
	
	//Valid Categories
	public enum Categories {
		NEWS("News"), CARDS("Cards"), PROMOTIONS("Promotions"), STICKERS("Stickers"), IMAGES("Images"), FEATURES("Features"), NOTIFICATION("Notification");
		private String name;

		public String getName() {
			return this.name;
		}

		Categories(String name) {
			this.name = name;
		}
	}
	
	
	@CrossOrigin
	@PostMapping("/feedertasks/schedule")
	public RestResponse scheduleFeederTasks(@Valid @RequestBody RequestModelJson request, @RequestParam(name="schedTime") String schedTime) {
		LOGGER.info("scheduleFeederTasks()==>POST /feedertasks/schedule call is initiated.");
		RestResponse response = new RestResponse();
		Set<String> categories = new HashSet<String>();
		for(Categories ctgry: Categories.values()) {
			categories.add(ctgry.getName().toLowerCase());
		}
		try {
			if(request == null) {
				LOGGER.error("No data in the request");
				return getFailureResponse(response, "No data in the schedule task request");
			}
			//validate category
			String category = request.getCategoryType();
			if(!categories.contains(category)) {
				LOGGER.error("Invalid Category {}. It should be either 'News' or 'Cards' or 'Promotions' or 'Stickers' or 'Features' or 'Notification'.", category);
				if(category == null || category.isEmpty()) {
					return getFailureResponse(response, "Invalid Category empty(\\\"\\\"). It should be either 'News' or 'Cards' or 'Promotions' or 'Stickers' or 'Features' or 'Notification'");
				}
				return getFailureResponse(response, "Invalid Category '"+category+"'. It should be either 'News' or 'Cards' or 'Promotions'  or 'Stickers' sor 'Features' or 'Notification'");
			}
			//Parse json
			Map<String, Object> map = new HashMap<>();
			map.put("categoryType", request.getCategoryType());
			map.put("jsonData", request.getJsonData());
			String jsonString  = JSONObject.toJSONString(map);
			LOGGER.info("Category: {}, JSON data: {}, schedTime: {}", category, jsonString, schedTime);
			//Query pending task for an on scheduled time. If it exists update it else create new entry
			List<FeederTasksEntity> pendingTasks = feederTasksService.findByStatusSchedTimeAndCategory(PENDING, sdf.parse(schedTime), category);
			if(pendingTasks == null || pendingTasks.size() == 0) {
				FeederTasksEntity pendingfeederTask = new FeederTasksEntity();
				this.feederTasksService.persist(constructFeederTasksData(pendingfeederTask, jsonString, category, sdf.parse(schedTime)));
				//Schedule task
				ScheduledExecutorService schedule = ExecutorServiceSingleton.getInstance();
				Date now = new Date();
				long total = 0l;
				if(now.before(sdf.parse(schedTime))) {
					total = sdf.parse(schedTime).getTime() - now.getTime();
					LOGGER.info("before: {}", total);
				} else {
					total =  now.getTime() - sdf.parse(schedTime).getTime();
					LOGGER.info("after: {}", total);
				}
				schedule.schedule(new SampleJob(feederTasksService, feederUrl, notificationUrl), total, TimeUnit.MILLISECONDS);
			} else {
				for(FeederTasksEntity task :pendingTasks) {
					try {
						this.feederTasksService.update(constructFeederTasksData(task, jsonString, category, sdf.parse(schedTime)));
					} catch (ParseException e) {
						return getFailureResponse(response, "Invalid Date format for the schedule time. Date format should be 'yyyy-MM-dd HH:mm:ss.SSS'");
					}
				}
			}
			LOGGER.info("scheduleFeederTasks()==>POST /feedertasks/schedule call is end.");
		} catch (Exception e) {
			LOGGER.error("Exception: {}", e.getMessage(), e);
			return getFailureResponse(response, e.getMessage());
		}
		return getSuccessResponse(response, "Scheduled tasks successfully.");
	}

	@CrossOrigin
	@PostMapping("/feedertasks/schedule/cancel")
	public RestResponse cancelScheduledFeederTasks(@Valid @RequestBody List<String> categoriesRequest, @RequestParam(name="schedTime") String schedTime) {
		LOGGER.info("cancelScheduledFeederTasks()==>POST /feedertasks/schedule/cacel call is initiated.");
		RestResponse response = new RestResponse();
		Set<String> categories = new HashSet<>();
		for(Categories ctgry: Categories.values()) {
			categories.add(ctgry.getName().toLowerCase());
		}
		try {
			if(categoriesRequest == null || categoriesRequest.size() == 0) {
				LOGGER.error("No data in the request");
				return getFailureResponse(response, "No data in the schedule task request");
			}
			Set<String> categoriesFromClient = new HashSet<>();
			for(String ctgry: categoriesRequest) {
				categoriesFromClient.add(ctgry.toLowerCase());
			}
			//validate category
			if(!categories.containsAll(categoriesFromClient)) {
				LOGGER.error("Invalid Category {}. It should be either 'News' or 'Cards' or 'Promotions' or 'Features'.", categoriesFromClient.toString());
				return getFailureResponse(response, "Invalid Category '"+categoriesFromClient.toString()+"'. It should be either 'News' or 'Cards' or 'Promotions' or 'Features'");
			}
			LOGGER.info("Categories: {}, schedTime: {}", categoriesRequest, schedTime);
			for(String category: categoriesRequest) {
				//Query pending task for an on scheduled time. If it exists update it else create new entry
				List<FeederTasksEntity> pendingTasks = feederTasksService.findByStatusSchedTimeAndCategory(PENDING, sdf.parse(schedTime), category);
				if(pendingTasks != null) {
					LOGGER.info("CANCELLED TASKS for category {} and scheduled Time {}",category, schedTime);
					for(FeederTasksEntity feederTasksEntity :pendingTasks) {
						feederTasksEntity.setLastmodifiedOn(new Date());
						feederTasksEntity.setStatus(CANCELLED);
						this.feederTasksService.update(feederTasksEntity);
					}
				}
			}
			
			LOGGER.info("cancelScheduledFeederTasks()==>POST /feedertasks/schedule/cacel call is end.");
		} catch (Exception e) {
			LOGGER.error("Exception: {}", e.getMessage(), e);
			return getFailureResponse(response, e.getMessage());
		}
		return getSuccessResponse(response, "Scheduled tasks are cancelled successfully.");
	}
	
	private RestResponse getFailureResponse(RestResponse response, String statusMessage) {
		LOGGER.info("getFailureResponse()==>Entered");
		response.setStatus("FAILURE");
		response.setStatusMessage(statusMessage);
		LOGGER.info("getFailureResponse()==>Exit");
		return response;
	}
	
	private RestResponse getSuccessResponse(RestResponse response, String statusMessage) {
		LOGGER.info("getSuccessResponse()==>Entered");
		response.setStatus("SUCCESS");
		response.setStatusMessage(statusMessage);
		LOGGER.info("getSuccessResponse()==>Exit");
		return response;
	}
	
	private FeederTasksEntity constructFeederTasksData(FeederTasksEntity feederTasksEntity, String jsonString, String category, Date schedTime) {
		LOGGER.info("constructFeederTasksData()==>Entered");
		if(feederTasksEntity.getCreatedOn() == null)
			feederTasksEntity.setCreatedOn(new Date());
		feederTasksEntity.setLastmodifiedOn(new Date());
		feederTasksEntity.setStatus(PENDING);
		feederTasksEntity.setCategory(category);
		feederTasksEntity.setData(jsonString);
		feederTasksEntity.setSchedTime(schedTime);
		LOGGER.info("constructFeederTasksData()==>Exit");
		return feederTasksEntity;
	}
}
