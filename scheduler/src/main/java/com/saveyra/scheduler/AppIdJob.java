package com.saveyra.scheduler;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.saveyra.scheduler.service.SaveyraMsgBrokerService;

public class AppIdJob extends QuartzJobBean {
	private static Logger logger = LoggerFactory.getLogger(AppIdJob.class);

	private SaveyraMsgBrokerService saveyraMsgBrokerService;

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		logger.info("AppID update Job work started..");
		try {
			ApplicationContext applicationContext = (ApplicationContext)
			        context.getScheduler().getContext().get("applicationContext");
			saveyraMsgBrokerService = applicationContext.getBean(SaveyraMsgBrokerService.class);
			
			//Get appID from createUser_8 event and Update appId for firstOpen_9 event 
			saveyraMsgBrokerService.updateApplicationIDForFirstOpenEvent();
	
		} catch (SchedulerException e) {
			e.printStackTrace();
			logger.error("Error in loading bean from applicationcontext: {}",e.getMessage(), e);
		} 
		logger.info("AppID update Job work is done.");
	}

}
