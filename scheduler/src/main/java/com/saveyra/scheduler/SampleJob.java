/*
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.saveyra.scheduler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.saveyra.scheduler.model.FeederTasksEntity;
import com.saveyra.scheduler.service.FeederTasksService;

public class SampleJob implements Runnable {
	private static final String NOTIFICATION = "Notification";
	private static Logger logger = LoggerFactory.getLogger(SampleJob.class);
	private FeederTasksService feederTasksService;

	private String feederUrl;

	private String notificationUrl;
	
	public SampleJob(FeederTasksService feederTasksService, String feederUrl, String notificationUrl) {
		this.feederTasksService = feederTasksService;
		this.feederUrl = feederUrl;
		this.notificationUrl = notificationUrl;
	}
	
	@Override
	public void run() {
		logger.info("Task execution==>executeInternal()==>Entered");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date now = new Date();
		CloseableHttpClient httpClient = HttpClientBuilder.create().build(); // Use this instead
		try {
			now = format.parse(format.format(now));
			logger.info("Current time {}", now);
			List<FeederTasksEntity> pendingTasks = feederTasksService.findBySchedTime(now);
			if (pendingTasks != null && pendingTasks.size() > 0) {
				for(FeederTasksEntity feederTask: pendingTasks){
					HttpPost request = null;
					JSONParser parser = new JSONParser();
					JSONObject jsonObject = (JSONObject) parser.parse(feederTask.getData());
					List<JSONObject> list = new ArrayList<>();
					StringEntity params = null;
					if(NOTIFICATION.equalsIgnoreCase(feederTask.getCategory())) {
						request = new HttpPost(notificationUrl);
						Object parsedObj  = parser.parse(String.valueOf(jsonObject.get("jsonData")));
						JSONArray jsonArray = (JSONArray) parsedObj;
						if(jsonArray != null && jsonArray.size() > 0) {
							params = new StringEntity(jsonArray.get(0).toString(), "UTF-8");
						}
						logger.info("Request==> POST /saveyranotification-v1.1/bulkPushNotification {}", params);
					} else {
						request = new HttpPost(feederUrl);
						list.add(jsonObject);
						params = new StringEntity(list.toString(), "UTF-8");
						logger.info("Request==> POST /json-processor/createCategoryJsonData {}", params);
					}
					request.addHeader("content-type", "application/json");
					request.setEntity(params);
					HttpResponse response = httpClient.execute(request);
					logger.info("status code {}", response.getStatusLine().getStatusCode());
					BufferedReader br = new BufferedReader(
							new InputStreamReader((response.getEntity().getContent())));
					String output;
					while ((output = br.readLine()) != null) {
						JSONObject jsonResponse = (JSONObject) parser.parse(output);
						if(NOTIFICATION.equalsIgnoreCase(feederTask.getCategory())) {
							logger.info("Response==> POST /saveyranotification-v1.1/bulkPushNotification {}", output);
							if("Request Complete".equalsIgnoreCase(jsonResponse.get("message").toString())) {
								feederTasksService.updateStatus("COMPLETED", feederTask.getSchedTime());
							}
						} else {
							logger.info("Response==> POST /json-processor/createCategoryJsonData {}", output);
							if("SUCCESS".equalsIgnoreCase(jsonResponse.get("status").toString())) {
								feederTasksService.updateStatus("COMPLETED", feederTask.getSchedTime());
							}
						}
						
					}
				}
			} else {
				logger.info("No Scheduled tasks availble in System");
			}
		} catch (Exception ex) {
			logger.error("Error {}",ex.getMessage(), ex);
			ex.printStackTrace();
		} finally {
			try {
				httpClient.close();
			} catch (IOException e) {
				logger.error("IOException {}",e.getMessage(), e);
				e.printStackTrace();
			}
		}
		logger.info("Task execution==>executeInternal()==>exit");
	}

}
