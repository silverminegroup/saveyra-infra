package com.saveyra.scheduler.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.saveyra.scheduler.model.FeederTasksEntity;

@Repository
public interface FeederTasksRepository extends CrudRepository<FeederTasksEntity, Integer> {
	@Query(value="select * from feeder_tasks tasks where tasks.status='PENDING' and tasks.instance_id=:instanceId",nativeQuery = true)
	FeederTasksEntity findByInstanceId(String instanceId);
	@Modifying(clearAutomatically = true)
	@Query(value="UPDATE feeder_tasks tasks SET tasks.status =:status WHERE tasks.status = 'PENDING' and tasks.sched_time=:schedTime", nativeQuery = true)
	int updateStatus(@Param("status") String status, Date schedTime);
	List<FeederTasksEntity> findByStatus(String status);
	@Query(value="select * from feeder_tasks tasks where tasks.status='PENDING' and tasks.sched_time<=:schedTime",nativeQuery = true)
	List<FeederTasksEntity> findBySchedTime(Date schedTime);
	@Query(value="select * from feeder_tasks tasks where tasks.status=:status and tasks.sched_time=:schedTime and tasks.category=:category",nativeQuery = true)
	List<FeederTasksEntity> findByStatusSchedTimeAndCategory(String status, Date schedTime, String category);
}
