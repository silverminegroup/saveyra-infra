package com.saveyra.scheduler.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.saveyra.scheduler.model.MessageBrokerCollection;

@Repository
public interface MessageBrokerCollectionRepository extends MongoRepository<MessageBrokerCollection, String> {
	
	@Query(value = "{'data.deviceId' : ?0, 'eventCode' : ?1, 'eventName' : ?2}")
	Page<MessageBrokerCollection> findAll(String deviceId, String eventCode, String eventName, Pageable page);
	
	@Query(value = "{'eventCode' : ?0 , 'eventName' : ?1, 'dateTime': {'$gt': ?2 }, $or:[{'applicationID' : null}, {'applicationID': {'$exists': true, '$eq': \"\"}}] }")
	List<MessageBrokerCollection> findByDateTime(String eventCode, String eventName, Date dateTime);
	
	@Query(value = "{'eventCode' : ?0 , 'eventName' : ?1, $or:[{'applicationID' : null}, {'applicationID': {'$exists': true, '$eq': \"\"}}] }")
	List<MessageBrokerCollection> findByEventCode(String eventCode, String eventName);
	
	@Query(value = "{'eventCode' : ?0, 'eventName' : ?1, '_class' : {'$exists' : true, '$ne' : null}}")
	Page<MessageBrokerCollection> findByEventName(String eventCode, String eventName, Pageable pageable);
}
