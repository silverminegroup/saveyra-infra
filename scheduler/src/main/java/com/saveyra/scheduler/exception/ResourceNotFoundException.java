package com.saveyra.scheduler.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
//This will cause Spring boot to respond with the specified HTTP status code whenever this exception is thrown from your controller
public class ResourceNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**Custom Exception Class**/
	
	private String resourceName;
    private String fieldName;
   
    private Object fieldValue;
 
    public ResourceNotFoundException( String resourceName, String fieldName, Object fieldValue) {
    	
    	
        super(String.format("%s not found with %s : '%s'", resourceName, fieldName, fieldValue)); 
        System.out.println("system hit to the exception handler"+fieldName);
        this.resourceName = resourceName;
        this.fieldName = fieldName;
     
        this.fieldValue = fieldValue;
    }

    
    public String getResourceName() {
        return resourceName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Object getFieldValue() {
        return fieldValue;
    }
    
}
  