package com.saveyra.scheduler.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "feeder_tasks")
public class FeederTasksEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "data")
	private String data;
	@Column(name = "category")
	private String category;
	@Column(name = "status")
	private String status;
	@Column(name = "sched_time")
	private Date schedTime;
	@Column(name = "created_on")
	private Date createdOn;
	@Column(name = "lastmodified_on")
	private Date lastmodifiedOn;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the schedTime
	 */
	public Date getSchedTime() {
		return schedTime;
	}

	/**
	 * @param schedTime the schedTime to set
	 */
	public void setSchedTime(Date schedTime) {
		this.schedTime = schedTime;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the lastmodifiedOn
	 */
	public Date getLastmodifiedOn() {
		return lastmodifiedOn;
	}

	/**
	 * @param lastmodifiedOn the lastmodifiedOn to set
	 */
	public void setLastmodifiedOn(Date lastmodifiedOn) {
		this.lastmodifiedOn = lastmodifiedOn;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lastmodifiedOn == null) ? 0 : lastmodifiedOn.hashCode());
		result = prime * result + ((schedTime == null) ? 0 : schedTime.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FeederTasksEntity other = (FeederTasksEntity) obj;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastmodifiedOn == null) {
			if (other.lastmodifiedOn != null)
				return false;
		} else if (!lastmodifiedOn.equals(other.lastmodifiedOn))
			return false;
		if (schedTime == null) {
			if (other.schedTime != null)
				return false;
		} else if (!schedTime.equals(other.schedTime))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FeederTasksEntity [id=" + id + ", data=" + data + ", category=" + category + ", status=" + status
				+ ", schedTime=" + schedTime + ", createdOn=" + createdOn + ", lastmodifiedOn=" + lastmodifiedOn + "]";
	}
}
