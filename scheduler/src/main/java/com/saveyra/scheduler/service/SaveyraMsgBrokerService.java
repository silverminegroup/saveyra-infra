package com.saveyra.scheduler.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.saveyra.scheduler.model.MessageBrokerCollection;

public interface SaveyraMsgBrokerService {
	List<MessageBrokerCollection> findByEventCodeAndEventNameAndAppIDIsNull(String eventCode, String eventName);
	MessageBrokerCollection findUniqueAppIdDocByDeviceIdEventCodeAndName(String deviceId, String eventCode, String eventName);
	void update(MessageBrokerCollection collection);
	Map<String, Date> getAllDeviceIdsByEventCode(String eventCode, Date lastUpdatedDate);
	String getApplicationIDByDeviceIdAndEventCode(String deviceId, String eventCode);
	void updateApplicationIDByDeviceIdAndEventCode(String deviceId, String eventCode, String applicationID, Date insertedDateTime);
	void updateApplicationIDForFirstOpenEvent();
}

