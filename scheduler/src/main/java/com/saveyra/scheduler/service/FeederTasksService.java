package com.saveyra.scheduler.service;

import java.util.Date;
import java.util.List;

import com.saveyra.scheduler.model.FeederTasksEntity;

public interface FeederTasksService {
	FeederTasksEntity findByInstanceId(String instanceId);
	void persist(FeederTasksEntity smgSaveyraNewsEntity);
	void update(FeederTasksEntity smgSaveyraNewsEntity);
	List<FeederTasksEntity> findByAll();
	int updateStatus(String status, Date schedTime);
	List<FeederTasksEntity> getByStatus(String scheduled);
	List<FeederTasksEntity> findBySchedTime(Date schedTime);
	List<FeederTasksEntity> findByStatusSchedTimeAndCategory(String status, Date schedTime, String category);
}
