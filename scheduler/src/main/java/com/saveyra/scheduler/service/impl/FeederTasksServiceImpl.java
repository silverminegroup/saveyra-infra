package com.saveyra.scheduler.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saveyra.scheduler.model.FeederTasksEntity;
import com.saveyra.scheduler.repository.FeederTasksRepository;
import com.saveyra.scheduler.service.FeederTasksService;

@Service
public class FeederTasksServiceImpl implements FeederTasksService {
	@Autowired
	private FeederTasksRepository feederTasksRepository;

	@Transactional
	@Override
	public FeederTasksEntity findByInstanceId(String instanceId) {
		return feederTasksRepository.findByInstanceId(instanceId);
	}

	@Transactional(readOnly = false)
	@Override
	public void persist(FeederTasksEntity feederTasksEntity) {
		feederTasksRepository.save(feederTasksEntity);
	}

	@Transactional(readOnly = false)
	@Override
	public void update(FeederTasksEntity feederTasksEntity) {
		feederTasksRepository.save(feederTasksEntity);
	}

	@Override
	public List<FeederTasksEntity> findByAll() {
		List<FeederTasksEntity> list = new ArrayList<>();
		Iterable<FeederTasksEntity> records = feederTasksRepository.findAll();
		if(records != null )
			records.iterator().forEachRemaining(list::add);
		return list;
	}

	@Transactional(readOnly = false)
	@Override
	public int updateStatus(String status, Date schedTime) {
		return feederTasksRepository.updateStatus(status, schedTime);
		
	}

	@Override
	public List<FeederTasksEntity> getByStatus(String scheduled) {
		return feederTasksRepository.findByStatus(scheduled);
	}

	@Override
	public List<FeederTasksEntity> findBySchedTime(Date schedTime) {
		return feederTasksRepository.findBySchedTime(schedTime);
	}

	@Override
	public List<FeederTasksEntity> findByStatusSchedTimeAndCategory(String status, Date schedTime, String category) {
		return feederTasksRepository.findByStatusSchedTimeAndCategory(status, schedTime, category);
	}

}
