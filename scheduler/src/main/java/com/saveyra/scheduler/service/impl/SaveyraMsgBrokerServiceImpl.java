package com.saveyra.scheduler.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.UpdateResult;
import com.saveyra.scheduler.model.MessageBrokerCollection;
import com.saveyra.scheduler.repository.MessageBrokerCollectionRepository;
import com.saveyra.scheduler.service.SaveyraMsgBrokerService;

@Service
public class SaveyraMsgBrokerServiceImpl implements SaveyraMsgBrokerService {
	private static final String MSG_BROKER_COLLECTION = "messageBroker";
	private static Logger logger = LoggerFactory.getLogger(SaveyraMsgBrokerServiceImpl.class);
	private static Date lastAppIdUpdateMarker = null;
	@Autowired
	private MessageBrokerCollectionRepository repository;
	
	@Autowired
	private MongoTemplate mongoTemplate;

	enum EventCode {
		CREATE_USER("8"), FIRST_OPEN("9");
		
		String code;
		EventCode(String code) {
			this.code = code;
		}
		public String getCode() {
			return code;
		}
	}
	
	enum EventName {
		CREATE_USER("createUser_8"), FIRST_OPEN("firstOpen_9");
		
		String name;
		EventName(String name) {
			this.name = name;
		}
		public String getName() {
			return name;
		}
	}
	
	@Override
	public List<MessageBrokerCollection> findByEventCodeAndEventNameAndAppIDIsNull(String eventCode, String eventName) {
		MessageBrokerCollection doc = null;
		List<MessageBrokerCollection> docs =  repository.findByEventName(eventCode, eventName, PageRequest.of(0, 1, new Sort(Sort.Direction.DESC, "dateTime"))).getContent();
		if(docs != null && docs.size() > 0) {
			doc = docs.get(0);
		}
		if(doc != null) {
			SimpleDateFormat ISO8601DATEFORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			try {
				docs =  repository.findByDateTime(eventCode, eventName, ISO8601DATEFORMAT.parse(ISO8601DATEFORMAT.format(doc.getDateTime())));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else {
			try {
				docs =  repository.findByEventCode(eventCode, eventName);
			} catch (ConversionFailedException e) {
				e.printStackTrace();
			}
			
		}
		return docs;
	}

	@Override
	public MessageBrokerCollection findUniqueAppIdDocByDeviceIdEventCodeAndName(String deviceId, String eventCode,
			String eventName) {
		Pageable pageable = PageRequest.of(0, 1);
		MessageBrokerCollection doc = null;
		List<MessageBrokerCollection> docs = repository.findAll(deviceId, eventCode, eventName, pageable).getContent();
		if(docs != null && docs.size() > 0) {
			doc = docs.get(0);
		}
		return doc;
	}

	@Transactional(readOnly=false)
	@Override
	public void update(MessageBrokerCollection collection) {
		repository.save(collection);
	}

	
	@Override
	public Map<String, Date> getAllDeviceIdsByEventCode(String eventCode, Date date) {
		Map<String, Date> deviceIDandInsertedDate = new HashMap<>();
		List<Bson> filters = new ArrayList<>();
		filters.add(Filters.eq("applicationID", null));
		filters.add(Filters.eq("applicationID", ""));
		MongoCursor<Document> cursor = null;
		if(date == null) {
			cursor = mongoTemplate.getCollection(MSG_BROKER_COLLECTION)
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", eventCode)),
							Aggregates.match(Filters.or(filters)))).iterator();
		} else {
			cursor = mongoTemplate.getCollection(MSG_BROKER_COLLECTION)
					.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", eventCode)),
							Aggregates.match(Filters.gt("InsertedDateTime", date)),
							Aggregates.match(Filters.or(filters)))).iterator();
		}
		
		if(cursor != null) {
			JSONParser parser = new JSONParser();
			while (cursor.hasNext()) {
				Document d = cursor.tryNext();
				if (d == null) {
					continue;
				} else {
					logger.info("Document: {}", d.toJson());
					try {
						JSONObject jsonData = (JSONObject) parser.parse(d.toJson());
						if(jsonData.get("data") != null) {
							String dataString = jsonData.get("data").toString();
							JSONObject data = (JSONObject) parser.parse(dataString);
							if(data.get("deviceId") != null) {
								SimpleDateFormat ISO8601DATEFORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
								String jsonDateTime = null;
								if(jsonData.get("InsertedDateTime") != null) {
									jsonDateTime = jsonData.get("InsertedDateTime").toString();
									JSONObject jsonDate = (JSONObject) parser.parse(jsonDateTime);
									if(jsonDate.get("$date") != null) {
										Long timestamp = Long.parseLong(jsonDate.get("$date").toString());
										deviceIDandInsertedDate.put(data.get("deviceId").toString(), ISO8601DATEFORMAT.parse(ISO8601DATEFORMAT.format(new Date(timestamp))));
									} else {
										 continue;
									}
									
								}
							}
						}
					} catch (org.json.simple.parser.ParseException e) {
						e.printStackTrace();
						logger.error("Error in parsing json data using simple parser: {}", e.getMessage(), e);
					} catch (ParseException e) {
						e.printStackTrace();
						logger.error("Error in parsing date format: {}", e.getMessage(), e);
					}	
				}
			}
		}
		return deviceIDandInsertedDate;
	}
	
	
	@Override
	public String getApplicationIDByDeviceIdAndEventCode(String deviceId, String eventCode) {
		Document document = mongoTemplate.getCollection(MSG_BROKER_COLLECTION)
				.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", eventCode)),
						Aggregates.match(Filters.eq("data.deviceId", deviceId)),
						Aggregates.sort(Filters.eq("InsertedDateTime", -1)))).first();
		if (document != null) {
			JSONParser parser = new JSONParser();
			logger.info("Document: {}", document.toJson());
			try {
				JSONObject jsonData = (JSONObject) parser.parse(document.toJson());
				if (jsonData.get("applicationID") != null) {
					return jsonData.get("applicationID").toString();
				}
			} catch (org.json.simple.parser.ParseException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	@Transactional(readOnly=false)
	@Override
	public void updateApplicationIDByDeviceIdAndEventCode(String deviceId, String eventCode, String applicationID, Date insertedDateTime) {
		Query query = new Query();
		query.addCriteria(Criteria.where("data.deviceId").exists(true).andOperator(Criteria.where("data.deviceId").is(deviceId)))
		.addCriteria(Criteria.where("eventCode").is(eventCode))
		.addCriteria(Criteria.where("InsertedDateTime").is(insertedDateTime));
		
		Update update = new Update();
		update.set("applicationID", applicationID);
		UpdateResult result = mongoTemplate.updateMulti(query, update, MSG_BROKER_COLLECTION);
		if(result != null) {
			logger.info("modified count: {}", result.getModifiedCount());
		}
	}
	
	/***
	 * Get all deviceIds based on eventCode-9 and applicationID is null or empty if
	 * it is first time update else get based on eventCode-9 and applicationID is
	 * null or empty and lastAppIdUpdate doc InsertedDateTime (which is get it from
	 * in-memory -'lastAppIdUpdateMarker'. Iterate over on deviceId list, get an
	 * deviceId on each iteration and make use of it to get applicationID based on
	 * eventCode-8 and deviceId, which will have applicationId. Use applicationId to
	 * update for eventCode-9 based on deviceId, eventCode-9, and InsertedDateTime.
	 * 
	 * 
	 */
	@Override
	public void updateApplicationIDForFirstOpenEvent() {
		try {
			// Get all documents based on applicaitonID is null or empty for the eventCode -
			// 9. Use last updated document date if file contains last updated document
			// date, else use date is null otherwise.
			List<Bson> filters = new ArrayList<>();
			filters.add(Filters.eq("applicationID", null));
			filters.add(Filters.eq("applicationID", ""));
			MongoCursor<Document> cursor = null;
			logger.info("lastAppIdUpdateMarker: {}", lastAppIdUpdateMarker);
			if(lastAppIdUpdateMarker == null) {
				cursor = mongoTemplate.getCollection(MSG_BROKER_COLLECTION)
						.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", EventCode.FIRST_OPEN.getCode())),
								Aggregates.match(Filters.or(filters)))).iterator();
			} else {
				cursor = mongoTemplate.getCollection(MSG_BROKER_COLLECTION)
						.aggregate(Arrays.asList(Aggregates.match(Filters.eq("eventCode", EventCode.FIRST_OPEN.getCode())),
								Aggregates.match(Filters.gt("InsertedDateTime", lastAppIdUpdateMarker)),
								Aggregates.match(Filters.or(filters)))).iterator();
			}
			
			if(cursor != null && cursor.hasNext()) {
				JSONParser parser = new JSONParser();
				while (cursor.hasNext()) {
					Document d = cursor.tryNext();
					if (d == null) {
						continue;
					} else {
						try {
							//Parse JSON to get deviceId from the data map
							JSONObject jsonData = (JSONObject) parser.parse(d.toJson());
							if(jsonData.get("data") != null) {
								String dataString = jsonData.get("data").toString();
								JSONObject data = (JSONObject) parser.parse(dataString);
								if(data.get("deviceId") != null && data.get("deviceId").toString().length() > 0) {
									logger.info("deviceId: {}",data.get("deviceId").toString());
									//Get applicationId for the deviceId and eventCode - 8
									String applicationId = getApplicationIDByDeviceIdAndEventCode(data.get("deviceId").toString(), EventCode.CREATE_USER.getCode());
									if (applicationId != null && applicationId.length() > 0) {
										logger.info("applicationId: {}",applicationId);
										//Get InsertedDateTime of deviceId to track it as marker from where, next update applicationID
										SimpleDateFormat ISO8601DATEFORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
										String jsonDateTime = null;
										if(jsonData.get("InsertedDateTime") != null) {
											jsonDateTime = jsonData.get("InsertedDateTime").toString();
											JSONObject jsonDate = (JSONObject) parser.parse(jsonDateTime);
											if(jsonDate.get("$date") != null) {
												Long timestamp = Long.parseLong(jsonDate.get("$date").toString());
													
												// Update document with applicationId for the deviceId and eventCode
												updateApplicationIDByDeviceIdAndEventCode(
														data.get("deviceId").toString(),
														EventCode.FIRST_OPEN.getCode(), applicationId, ISO8601DATEFORMAT.parse(ISO8601DATEFORMAT.format(new Date(timestamp))));
												//Track last appID update insertedDateTime of document
												lastAppIdUpdateMarker = ISO8601DATEFORMAT.parse(ISO8601DATEFORMAT.format(new Date(timestamp)));
											} else {
												continue;
											}
										}
									} else {
										logger.warn("No Create User Event to get application ID and update it into First Open event.");
									}
								} else {
									logger.warn("No deviceId found in {} event", EventName.FIRST_OPEN.getName());
								}
							}
						} catch (org.json.simple.parser.ParseException e) {
							e.printStackTrace();
							logger.error("Error in parsing json data using simple parser: {}", e.getMessage(), e);
						} catch (ParseException e) {
							e.printStackTrace();
							logger.error("Error in parsing date format: {}", e.getMessage(), e);
						}	
					}
				}
			} else {
				logger.warn("No {} event documents found.", EventName.FIRST_OPEN.getName());
			}
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("Exception: {}",e.getMessage(), e);
		} 
	}
}
