package com.saveyra.scheduler;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class ExecutorServiceSingleton {
	private static final ScheduledExecutorService INSTANCE = Executors.newScheduledThreadPool(1);

	private ExecutorServiceSingleton() {

	}

	public static ScheduledExecutorService getInstance() {
		return ExecutorServiceSingleton.INSTANCE;
	}
}
