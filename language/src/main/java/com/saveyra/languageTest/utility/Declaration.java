package com.saveyra.languageTest.utility;

import java.util.HashMap;
import java.util.Map;

public class Declaration {
	private static String token ="1A23P45U88796R123V345A8178R1A229J0071816";	
	private static	Map <String,String>androidMap= new HashMap<String,String>();
			
	private static	Map <String,String>iOSMap= new HashMap<String,String>();

	public static String getToken() {
		return token;
	}

	public static void setToken(String token) {
		Declaration.token = token;
	}

	public static Map<String, String> getAndroidMap() {
		return androidMap;
	}

	public static void setAndroidMap(Map<String, String> androidMap) {
		Declaration.androidMap = androidMap;
	}

	public static Map<String, String> getiOSMap() {
		return iOSMap;
	}

	public static void setiOSMap(Map<String, String> iOSMap) {
		Declaration.iOSMap = iOSMap;
	}

}
