package com.saveyra.languageTest.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.saveyra.languageTest.Repo.MysqlRepo;
import com.saveyra.languageTest.model.LanguageModel;
import com.saveyra.languageTest.utility.Declaration;

@RestController
@RequestMapping("/")
public class LanguageApplicationController {

	@Autowired
	MysqlRepo repo;

	@CrossOrigin
	@PostMapping("/getLanguage")
	public ResponseEntity<String> getLanguage(@Valid @RequestBody String json) throws IOException {
		HttpHeaders respHeader = new HttpHeaders();
		respHeader.set("Content-Type", "application/json");

		final ObjectNode node = new ObjectMapper().readValue(json, ObjectNode.class);
		String lCode = "";
		String platform = "";
		String token = "";
		if (node.has("languageCode")) {
			lCode = node.get("languageCode").toString().split("\"")[1];
		} else {
			ResponseEntity<String> resp = new ResponseEntity<String>("{\"status\":\"Error\",\"message\":\"Language Code not found\"}", respHeader, HttpStatus.BAD_REQUEST);
			return resp;
		}

		if (node.has("platform")) {
			platform = node.get("platform").toString().split("\"")[1];
		} else {
			ResponseEntity<String> resp = new ResponseEntity<String>("{\"status\":\"Error\",\"message\":\"Platform not found\"}", respHeader, HttpStatus.BAD_REQUEST);
			return resp;
		}

		if (node.has("token")) {
			token = node.get("token").toString().split("\"")[1];
		} else {
			ResponseEntity<String> resp = new ResponseEntity<String>("{\"status\":\"Error\",\"message\":\"Access Token not found\"}", respHeader, HttpStatus.BAD_REQUEST);
			return resp;
		}

		if (!token.equalsIgnoreCase(Declaration.getToken())) {
			ResponseEntity<String> resp = new ResponseEntity<String>("{\"status\":\"Error\",\"message\":\"Access Token mismatch\"}", respHeader, HttpStatus.BAD_REQUEST);
			return resp;
		}

		String returnVal = "";

		if (platform.equalsIgnoreCase("Android")) {
			returnVal = Declaration.getAndroidMap().get(lCode);
		} else {
			returnVal = Declaration.getiOSMap().get(lCode);
		}

		if (returnVal == null || returnVal.equals("")) {
			//Repopulate Maps
			ArrayList<LanguageModel> languageArray = new ArrayList<LanguageModel>();
			languageArray.addAll(repo.getLanguage());

			for (LanguageModel lm : languageArray) {
				if (lm.getPlatform().equalsIgnoreCase("Android")) {
					Declaration.getAndroidMap().put(lm.getLanguageCode(), lm.getData());
				} else {
					Declaration.getiOSMap().put(lm.getLanguageCode(), lm.getData());

				}
			}
			
			if (platform.equalsIgnoreCase("Android")) {
				returnVal = Declaration.getAndroidMap().get(lCode);
			} else {
				returnVal = Declaration.getiOSMap().get(lCode);
			}
			
			if (returnVal == null || returnVal.equals("")) {
				ResponseEntity<String> resp = new ResponseEntity<String>("{\"status\":\"Error\",\"message\":\"Language Not Found\"}", respHeader, HttpStatus.BAD_REQUEST);
				return resp;
			}else {
				ResponseEntity<String> resp = new ResponseEntity<String>(returnVal, respHeader, HttpStatus.OK);
				return resp;
			}
		} else {
			ResponseEntity<String> resp = new ResponseEntity<String>(returnVal, respHeader, HttpStatus.OK);
			return resp;
		}

	}
	
	@CrossOrigin
	@GetMapping("/reload")
	public ResponseEntity<String> reload() throws IOException {
		HttpHeaders respHeader = new HttpHeaders();
		respHeader.set("Content-Type", "application/json");
		
		ArrayList<LanguageModel> languageArray = new ArrayList<LanguageModel>();
		languageArray.addAll(repo.getLanguage());

		for (LanguageModel lm : languageArray) {
			if (lm.getPlatform().equalsIgnoreCase("Android")) {
				Declaration.getAndroidMap().put(lm.getLanguageCode(), lm.getData());
			} else {
				Declaration.getiOSMap().put(lm.getLanguageCode(), lm.getData());

			}
		}
		String message = "{\"status\":\"Success\",\"message\":\"Map is reloaded successfully.\"}";
		ResponseEntity<String> resp = new ResponseEntity<String>(message, respHeader, HttpStatus.OK);
		return resp;
	}

}
