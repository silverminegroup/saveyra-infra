package com.saveyra.languageTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@ComponentScan("com.saveyra")
@EnableAutoConfiguration
@PropertySource(value = "classpath:application.properties")
public class LanguageTestApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(LanguageTestApplication.class, args);
	}
	
	@Override  
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(LanguageTestApplication.class);     
	} 

}
