package com.saveyra.languageTest.model;


public class LanguageModel {
	private String languageCode="";
	private String platform="";
	private String data="";
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatfom(String platfom) {
		this.platform = platfom;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public LanguageModel(String languageCode, String platfom, String data) {
		super();
		this.languageCode = languageCode;
		this.platform = platfom;
		this.data = data;
	}

}
