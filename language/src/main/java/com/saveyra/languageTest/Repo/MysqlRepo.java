package com.saveyra.languageTest.Repo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.saveyra.languageTest.model.LanguageModel;

@Repository
public class MysqlRepo {
	
	private static final Logger logger = LoggerFactory.getLogger(MysqlRepo.class);

	@SuppressWarnings("unused")
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	DataSource dataSource;
	
	@Transactional(readOnly = true)
	public List<LanguageModel> getLanguage() {
		//logger.info("[ENTER] getNotificationCenter");
		List<LanguageModel> langList = new ArrayList<LanguageModel>();
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = null;
		try {
			conn = dataSource.getConnection();
			query = "SELECT languageCode,platform,languageValue from smg_saveyra_languages";
			pstmt = conn.prepareStatement(query);
			rs =pstmt.executeQuery();
			while (rs.next()) {
				langList.add(new LanguageModel(rs.getString(1), rs.getString(2), rs.getString(3)));
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[ERROR] SQL Fetch Fail");
		} finally {
			try {
				if(pstmt!=null) {
					pstmt.close();
				}								
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if(rs!=null) {
					rs.close();
				}				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}				
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//logger.info("[LEAVE] getNotificationCenter");
		return langList;
	}

}
