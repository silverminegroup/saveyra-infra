package com.producer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Properties;
import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.event.EventListener;


/**
 * KafkaProducerApplication : Spring Boot Application Launcher 
 * @author Silvermine
 * 
 * Last Modified : Apurva : 10-08-2018 {Updated for dynamic token creation}
 * Last Modified : Apurva : 13-08-2018 {removed support for dynamic token}
 *
 */
@SpringBootApplication  
@EnableAutoConfiguration(exclude = {ErrorMvcAutoConfiguration.class}) 
@ComponentScan("com.producer.controller")
@PropertySource(value="classpath:application.properties")
public class KafkaProducerApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		//Generate Random Tokens
		//TokenHandler tokenHandler = new TokenHandler(); -- Removed support for dynamic token [Temporary]
		//Launch Application
		SpringApplication.run(KafkaProducerApplication.class, args);
	} 
	
	//Added - 10-05-2019 for Singleton 
	@EventListener(ApplicationReadyEvent.class)
	public void doSomethingAfterStartup() {
		Properties props = new Properties();
		
	}
	

	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		// TODO Auto-generated method stub
		return builder.sources(KafkaProducerApplication.class);
	}	
	
}
         