package com.producer.controller;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.producer.tokenhandler.CreateToken;
import com.producer.tokenhandler.TokenRefresh;
import com.producer.utility.Declaration;
import com.producer.utility.Errors;
import com.producer.utility.ProducerKafka;

/**
 * KafkaProducerController : Controller class for Kafka feed API. This class
 * provides rest end points to update platform action specification to database
 * for analytics.
 * 
 * @author Silvermine
 * 
 *         Last Modified 13-08-2018 {1. Added Support for token in producer end
 *         points, 2. Added HTTPS Security, 3 Removed Hard codings }
 *
 */

@RestController("/")
@EnableAutoConfiguration
@PropertySource(value = { "classpath:application.properties" })
public class KafkaProducerController {

	@Autowired
	private Environment env;
	//private Gson gson = new Gson();
	String returnString = "Success";

	private static final Logger logger = LoggerFactory.getLogger(KafkaProducerController.class);

	// Producer for DevServer
	@CrossOrigin
	@RequestMapping(value = "v1/producer", method = RequestMethod.POST, headers = "Content-type=application/json")
	public ResponseEntity<String> producerApi(@Valid @RequestHeader(value = "token") String token,
			@RequestBody String message, HttpServletRequest request) {
		logger.info("[INFO] Enter Kafka Producer : Message : " + message + "   IP : "+request.getRemoteAddr());
		/* Token Authentication */		
		
		if (token == null || token.equals("")) {
			logger.error("[Error] Null Token");
			HttpHeaders respHeader = new HttpHeaders();
			respHeader.set("Content-Type", "application/json");
			ResponseEntity<String> resp = new ResponseEntity<String>("{\"message\":\""+Errors.TOKEN_NULL+"\"}", respHeader,
					HttpStatus.BAD_REQUEST);
			return resp;
		}

		if (!token.equals(Declaration.sCommonToken)) {
			logger.error("[Error] Token Mismatch " + token);
			HttpHeaders respHeader = new HttpHeaders();
			respHeader.set("Content-Type", "application/json");
			ResponseEntity<String> resp = new ResponseEntity<String>("{\"message\":\""+Errors.TOKEN_MISMATCH+"\"}", respHeader,
					HttpStatus.BAD_REQUEST);			
			return resp;
		}

		// Support for platform specific token removed
		/*
		 * String sPlatformType = null; try { sPlatformType = getsPlatformType(message);
		 * } catch (IOException e1) { // TODO Auto-generated catch block
		 * e1.printStackTrace(); return Errors.PLATFORM_EXCEPTION; }
		 * 
		 * if (sPlatformType == null) { logger.error("[ERROR] Platform NULL"); return
		 * Errors.PLATFORM_NULL; }
		 * 
		 * // return if token mismatch switch (sPlatformType.toUpperCase()) { case
		 * "\"ANDROID\"": if (!token.equals(Declaration.sAndroidToken)) {
		 * logger.error("[ERROR] Token Mismatch for Android Device "); return
		 * Errors.TOKEN_MISMATCH; } break; case "\"IOS\"": if
		 * (!token.equals(Declaration.sIOSToken)) {
		 * logger.error("[ERROR] Token Mismatch for IOS Device "); return
		 * Errors.TOKEN_MISMATCH; } break; default:
		 * logger.error("[ERROR] Unknown Device Type "); return Errors.PLATFORM_INVALID;
		 * }
		 */

		/* Prepare Kafka Configuration */

		//Properties props = new Properties();
		/*
		 * props.put("bootstrap.servers", "54.152.201.198:9092");// ,54.83.154.209:9092
		 * -- values taken from properties file props.put("acks", "all");
		 * props.put("retries", 1); props.put("request.timeout.ms", 6000);
		 * props.put("batch.size", 16384); props.put("linger.ms", 0);
		 * props.put("buffer.memory", 33554432); props.put("key.serializer",
		 * "org.apache.kafka.common.serialization.StringSerializer");
		 * props.put("value.serializer",
		 * "org.apache.kafka.common.serialization.StringSerializer");
		 * props.put("zk.connect", "54.152.201.198:2181,54.83.154.209:2181");
		 */
		/*
		 * logger.debug("[DEBUG] PROPS : " +
		 * env.getRequiredProperty("BOOTSTRAP_SERVERS") + "\n" +
		 * Integer.parseInt(env.getRequiredProperty("REQUEST_TIMEOUT_MILLIS")) + "\n" +
		 * env.getRequiredProperty("KEY_SERIALIZER"));
		 */
		/*if (Declaration.QAEnv) {
			props.put("bootstrap.servers", env.getRequiredProperty("BOOTSTRAP_SERVERS_PRODUCTION"));
		} else {
			props.put("bootstrap.servers", env.getRequiredProperty("BOOTSTRAP_SERVERS"));// ,54.83.154.209:9092
		}
		props.put("acks", env.getRequiredProperty("ACKNOWLEDGEMENT"));
		props.put("retries", Integer.parseInt(env.getRequiredProperty("RETRIES")));
		props.put("request.timeout.ms", Integer.parseInt(env.getRequiredProperty("REQUEST_TIMEOUT_MILLIS")));
		props.put("batch.size", Integer.parseInt(env.getRequiredProperty("BATCH_SIZE")));
		props.put("linger.ms", Integer.parseInt(env.getRequiredProperty("LINGER_MILLIS")));
		props.put("buffer.memory", Integer.parseInt(env.getRequiredProperty("BUFFER_MEMORY")));
		props.put("key.serializer", env.getRequiredProperty("KEY_SERIALIZER"));
		props.put("value.serializer", env.getRequiredProperty("VALUE_SERIALIZER"));
*/
		Producer<String, String> kafkaProducer = null;
		try {
			kafkaProducer = ProducerKafka.getInstance();
			kafkaProducer.send(new ProducerRecord<String, String>("saveyra", message)); // topic name saveyra and msg
		} catch (Exception e) {
			//e.printStackTrace();
			returnString = "Failure";
			logger.error("[Error] Kafka Send Exception : " + e.getLocalizedMessage());
			HttpHeaders respHeader = new HttpHeaders();
			respHeader.set("Content-Type", "application/json");
			ResponseEntity<String> resp = new ResponseEntity<String>("{\"message\":\"Kafka Server Not Responding\"}", respHeader,
					HttpStatus.FAILED_DEPENDENCY);
			return resp;

		} /*finally {
			try {
			kafkaProducer.close();
			}catch(Exception e) {
				logger.error("[Error] Kafka Close Exception : " + e.getLocalizedMessage());
			}
		}*/
		logger.info("[INFO] Success");

		HttpHeaders respHeader = new HttpHeaders();
		respHeader.set("Content-Type", "application/json");
		ResponseEntity<String> resp = new ResponseEntity<String>("{\"message\":\""+returnString+"\"}", respHeader,
				HttpStatus.OK);
		return resp;
	}

	@CrossOrigin
	@RequestMapping(value = "v1/producer/getToken", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getToken() {
		String sDevicetoken = Declaration.sCommonToken;

		// Updated for common token implementation
		/*
		 * switch (sPlatformType) { case "Android": sDevicetoken =
		 * Declaration.sAndroidToken;
		 * logger.debug("[DEBUG] Returning token for Android " + sDevicetoken); break;
		 * case "iOS": sDevicetoken = Declaration.sIOSToken;
		 * logger.debug("[DEBUG] Returning token for IOS " + sDevicetoken); break;
		 * default: logger.debug("[ERROR] Unknown Device Type "); }
		 */

		return sDevicetoken;
	}

	@CrossOrigin
	@RequestMapping(value = "v1/producer/updateTokenByteSize", method = RequestMethod.POST, headers = "Content-type=application/json")
	public String updateTokenSize(@Valid @RequestBody String tokenByteSize) {
		// System.out.println("TokenSize" + tokenByteSize);
		Declaration.iTokenSize = Integer.parseInt(tokenByteSize);
		// Updated for common token implementation
		/*
		 * // Create Android Token CreateToken createTokenAndroid = new
		 * CreateToken(Declaration.iTokenSize); Declaration.sAndroidToken =
		 * createTokenAndroid.getsToken(); // Create IOS Token CreateToken
		 * createTokenIOS = new CreateToken(Declaration.iTokenSize);
		 * Declaration.sIOSToken = createTokenIOS.getsToken();
		 */
		CreateToken createTokenIOS = new CreateToken(Declaration.iTokenSize);
		Declaration.sCommonToken = createTokenIOS.getsToken();

		return "Token Size Updated SucessFully.";
	}

	@CrossOrigin
	@RequestMapping(value = "v1/producer/refreshToken", method = RequestMethod.POST, headers = "Content-type=application/json")
	public String updateToken(@Valid @RequestBody String updateData) {

		ObjectMapper mapper = new ObjectMapper();
		TokenRefresh tokenRefresh = null;
		try {
			tokenRefresh = mapper.readValue(updateData, TokenRefresh.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// System.out.println("New Token " + tokenRefresh.getNewToken() + " " +
		// tokenRefresh.getsPlatformType() + " "
		// + tokenRefresh.getOldToken());
		if (tokenRefresh.getNewToken() == null || tokenRefresh.getNewToken().equals("")) {
			return "Failed to update token. New Token is null or empty.";
		}
		/*
		 * switch (tokenRefresh.getsPlatformType()) { case "Android": if
		 * (tokenRefresh.getOldToken().equals(Declaration.sAndroidToken)) {
		 * Declaration.sAndroidToken = tokenRefresh.getNewToken(); } else { return
		 * "Failed to update token. Mismatch in android token."; } break; case "iOS": if
		 * (tokenRefresh.getOldToken().equals(Declaration.sIOSToken)) {
		 * Declaration.sAndroidToken = tokenRefresh.getNewToken(); } else { return
		 * "Failed to update token. Mismatch in IOS token."; } break; default: return
		 * "Failed to update token. Platform type not found."; }
		 */
		return "Token Updated Succesfully";
	}

	@Deprecated // depreciated after common token implementation
	private String getsPlatformType(@Valid String message)
			throws JsonParseException, JsonMappingException, IOException {
		// TODO Auto-generated method stub

		final ObjectNode node = new ObjectMapper().readValue(message, ObjectNode.class);
		if (node.has("platform")) {
			// System.out.println("platform: " + node.get("platform"));
			return node.get("platform").toString();
		}
		return null;
	}

	// Production Server
	/*
	 * @CrossOrigin
	 * 
	 * @RequestMapping(value = "v1/producer", method = RequestMethod.POST, headers =
	 * "Content-type=application/json") public String
	 * producerApiQA(@Valid @RequestHeader(value = "token") String
	 * token, @RequestBody String message) {
	 * logger.debug("[DEBUG] Enter Production Sever Producer " + token); String
	 * returnString = "Success";
	 */ /* Token Authentication */ /*
									 * if (token == null || token.equals("")) { return null; }
									 * 
									 * String sPlatformType = null; try { sPlatformType = getsPlatformType(message);
									 * } catch (IOException e1) { // TODO Auto-generated catch block
									 * e1.printStackTrace(); }
									 * 
									 * if(sPlatformType==null) { logger.error("[ERROR] Platform NULL"); return null;
									 * }
									 * 
									 * // return if token mismatch switch (sPlatformType.toUpperCase()) { case
									 * "\"ANDROID\"": if (!token.equals(Declaration.sAndroidToken)) {
									 * logger.error("[ERROR] Token Mismatch for Android Device "); return null; }
									 * break; case "\"IOS\"": if (!token.equals(Declaration.sIOSToken)) {
									 * logger.error("[ERROR] Token Mismatch for IOS Device "); return null; } break;
									 * default: logger.error("[ERROR] Unknown Device Type "); return null; }
									 * 
									 * Properties props = new Properties();
									 */
	/*
	 * props.put("bootstrap.servers", "kafka1:9091,kafka2:9092,kafka3:9093");//
	 * ,54.83.154.209:9092 props.put("acks", "all"); props.put("retries", 1);
	 * props.put("request.timeout.ms", 6000); props.put("batch.size", 16384);
	 * props.put("linger.ms", 0); props.put("buffer.memory", 33554432);
	 * props.put("key.serializer",
	 * "org.apache.kafka.common.serialization.StringSerializer");
	 * props.put("value.serializer",
	 * "org.apache.kafka.common.serialization.StringSerializer"); //
	 * props.put("zk.connect", "54.152.201.198:2181,54.83.154.209:2181");
	 * 
	 *//*
		 * props.put("bootstrap.servers",
		 * env.getRequiredProperty("BOOTSTRAP_SERVERS_PRODUCTION")); props.put("acks",
		 * env.getRequiredProperty("ACKNOWLEDGEMENT")); props.put("retries",
		 * Integer.parseInt(env.getRequiredProperty("RETRIES")));
		 * props.put("request.timeout.ms",
		 * Integer.parseInt(env.getRequiredProperty("REQUEST_TIMEOUT_MILLIS")));
		 * props.put("batch.size",
		 * Integer.parseInt(env.getRequiredProperty("BATCH_SIZE")));
		 * props.put("linger.ms",
		 * Integer.parseInt(env.getRequiredProperty("LINGER_MILLIS")));
		 * props.put("buffer.memory",
		 * Integer.parseInt(env.getRequiredProperty("BUFFER_MEMORY")));
		 * props.put("key.serializer", env.getRequiredProperty("KEY_SERIALIZER"));
		 * props.put("value.serializer", env.getRequiredProperty("VALUE_SERIALIZER"));
		 * 
		 * Producer<String, String> kafkaProducer = null; try { kafkaProducer = new
		 * KafkaProducer<>(props); String msg = message; kafkaProducer.send(new
		 * ProducerRecord<String, String>("saveyra", msg)); // topic name saveyra and
		 * msg is the // message content // message Check ?
		 * System.out.println("Sent message:" + msg);
		 * 
		 * } catch (Exception e) { e.printStackTrace(); returnString = "Failure";
		 * 
		 * } finally { kafkaProducer.close(); } return returnString; }
		 */
}
