package com.producer.controller;

import org.apache.catalina.servlet4preview.RequestDispatcher;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErrorHandler implements ErrorController {
	
	@Override
    public String getErrorPath() {
        return "/error";
    }
    
    @CrossOrigin
	@RequestMapping(value = "/error")
    public ResponseEntity<String> handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);  
        
        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());
         
            if(statusCode == HttpStatus.NOT_FOUND.value()) {
            	HttpHeaders respHeader = new HttpHeaders();
    			respHeader.set("Content-Type", "application/json");
    			ResponseEntity<String> resp = new ResponseEntity<String>("{\"message\": \"Page Not Found\"}", respHeader,
    					HttpStatus.NOT_FOUND);
    			return resp;
            }
            else if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
            	HttpHeaders respHeader = new HttpHeaders();
    			respHeader.set("Content-Type", "application/json");
    			ResponseEntity<String> resp = new ResponseEntity<String>("{\"message\": \"Internal Server Error\"}", respHeader,
    					HttpStatus.INTERNAL_SERVER_ERROR);
    			return resp;
            }
        }
        HttpHeaders respHeader = new HttpHeaders();
		respHeader.set("Content-Type", "application/json");
		ResponseEntity<String> resp = new ResponseEntity<String>("{\"message\": \"Bad Request\"}", respHeader,
				HttpStatus.BAD_REQUEST);
		return resp;
    }
}
