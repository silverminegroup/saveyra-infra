package com.producer.tokenhandler;

import java.security.SecureRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.producer.utility.Declaration;

/**
 * CreateToken : POJO to create tokens based on provided byte size
 * @author Apurva Raj
 *
 */
public class CreateToken {
	private static final Logger logger = LoggerFactory.getLogger(CreateToken.class);
	private String sToken = null;

	public CreateToken(Integer byteSize) {
		logger.debug("[DEBUG] Enter CreateToken");
		if (byteSize < 10) {
			byteSize = 10;
			logger.debug("[DEBUG] Token Size less than expected. Resetted to 10");
		}
		Declaration.iTokenSize = byteSize;
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[Declaration.iTokenSize];
		random.nextBytes(bytes);
		setsToken(bytes.toString());
		logger.debug("[DEBUG] Leave CreateToken, Token "+sToken);
	}

	public String getsToken() {
		logger.debug("[DEBUG] Enter getsToken");
		if (sToken == null && sToken.equals("")) {
			SecureRandom random = new SecureRandom();
			byte bytes[] = new byte[Declaration.iTokenSize];
			random.nextBytes(bytes);
			setsToken(bytes.toString());
		}
		logger.debug("[DEBUG] Leave getsToken, Token "+sToken);
		return sToken;
	}

	private void setsToken(String sToken) {
		this.sToken = sToken;
	}

}
