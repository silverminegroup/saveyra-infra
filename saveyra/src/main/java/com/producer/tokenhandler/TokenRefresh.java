package com.producer.tokenhandler;

/**
 * TokenRefresh : Model Class for Refresh Token 
 * @author Apurva Raj
 *
 */
@Deprecated // Depreciated after common token implementation
public class TokenRefresh {
	public String sPlatformType;
	public String oldToken;
	public String newToken;
	public String getsPlatformType() {
		return sPlatformType;
	}
	public void setsPlatformType(String sPlatformType) {
		this.sPlatformType = sPlatformType;
	}
	public String getOldToken() {
		return oldToken;
	}
	public void setOldToken(String oldToken) {
		this.oldToken = oldToken;
	}
	public String getNewToken() {
		return newToken;
	}
	public void setNewToken(String newToken) {
		this.newToken = newToken;
	}
	

}
