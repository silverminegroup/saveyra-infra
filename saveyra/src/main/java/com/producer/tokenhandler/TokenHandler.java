package com.producer.tokenhandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.producer.utility.Declaration;

/**
 * TokenHandler : Handler class for token services
 * @author Apurva Raj
 *
 */
public class TokenHandler {
	private static final Logger logger = LoggerFactory.getLogger(TokenHandler.class);
	public TokenHandler(){
		logger.debug("[DEBUG] Enter TokenHandler");
		/*updated for common token
		//Create Android Token
		CreateToken createTokenAndroid = new CreateToken(Declaration.iTokenSize);		
		Declaration.sAndroidToken = createTokenAndroid.getsToken();
		//Create IOS Token		
		CreateToken createTokenIOS = new CreateToken(Declaration.iTokenSize);
		Declaration.sIOSToken = createTokenIOS.getsToken();
		 */
		CreateToken createTokenIOS = new CreateToken(Declaration.iTokenSize);
		Declaration.sCommonToken = createTokenIOS.getsToken();
		logger.debug("[DEBUG] Leave TokenHandler");
	}
}

