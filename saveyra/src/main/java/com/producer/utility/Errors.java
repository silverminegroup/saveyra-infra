package com.producer.utility;

public class Errors {

	public static final String TOKEN_NULL = "Invalid Token";
	public static final String PLATFORM_EXCEPTION = "Platform Resolution Fail";
	public static final String PLATFORM_NULL ="Platform Null";
	public static final String PLATFORM_INVALID ="Platform Invalid";
	public static final String TOKEN_MISMATCH = "Invalid Token";
	public static final String TOKEN_INVALID = "Invalid Token";
	
			
}
