package com.producer.utility;

/**
 * Declaration : Global Declarations
 * @author Apurva Raj 
 * 
 * Last Modified : Apurva : 13-08-2018 {Fixed static token implementation}
 * 				   Apurva : 20-08-2018 {Common token for all platforms}
 * 
 */
public class Declaration {
	public static Integer iTokenSize = 40;
	//public static String sAndroidToken = null; -- create token support not available , Apurva 13-08-2018
	//public static String sIOSToken = null; -- create token support not available , Apurva 13-08-2018
	//public static String sAndroidToken = "1A23P45U88796R123V345A8178R1A229J0071816"; 
	//public static String sIOSToken = "90R73A131J34A536P0U132R82V3172A770"; -- commented, Common token implementation
	public static String sCommonToken = "1A23P45U88796R123V345A8178R1A229J0071816"; 
	public static final String SERVER_LOG_PATH = "${catalina.base}//logs//";
	public static final String SERVER_LOG_NAME = "KafkaProducerLog";	
	public static boolean QAEnv = false; 
}
