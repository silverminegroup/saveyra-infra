package com.producer.utility;

import java.util.Properties;

import javax.annotation.PreDestroy;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.producer.controller.KafkaProducerController;

/**
 * Singleton Class for org.apache.kafka.clients.producer object
 * 
 * @author Apurva Raj 10-05-2019
 *
 */

@Component
@PropertySource(value = { "classpath:application.properties" })
public class ProducerKafka {
	
	private static final Logger logger = LoggerFactory.getLogger(KafkaProducerController.class);
	
	@Autowired
	private static Environment env;
	
	private static Producer<String, String> kafkaProducer = null;

	private ProducerKafka() {};
	
	public static Producer<String, String> getInstance() {
		
	if(ProducerKafka.kafkaProducer==null) {
		Properties props = new Properties();		
		if (Declaration.QAEnv) {
			props.put("bootstrap.servers", env.getRequiredProperty("BOOTSTRAP_SERVERS_PRODUCTION"));
		} else {
			props.put("bootstrap.servers", env.getRequiredProperty("BOOTSTRAP_SERVERS"));// ,54.83.154.209:9092
		}
		props.put("acks", env.getRequiredProperty("ACKNOWLEDGEMENT"));
		props.put("retries", Integer.parseInt(env.getRequiredProperty("RETRIES")));
		props.put("request.timeout.ms", Integer.parseInt(env.getRequiredProperty("REQUEST_TIMEOUT_MILLIS")));
		props.put("batch.size", Integer.parseInt(env.getRequiredProperty("BATCH_SIZE")));
		props.put("linger.ms", Integer.parseInt(env.getRequiredProperty("LINGER_MILLIS")));
		props.put("buffer.memory", Integer.parseInt(env.getRequiredProperty("BUFFER_MEMORY")));
		props.put("key.serializer", env.getRequiredProperty("KEY_SERIALIZER"));
		props.put("value.serializer", env.getRequiredProperty("VALUE_SERIALIZER"));
		
	    ProducerKafka.kafkaProducer = new KafkaProducer<>(props);	    
	}

		return ProducerKafka.kafkaProducer;
	}	
	
	@PreDestroy
    public void destroy() {
		try {
			kafkaProducer.close();
			}catch(Exception e) {
				logger.error("[Error] Kafka Close Exception : " + e.getLocalizedMessage());
			}
    }
}
