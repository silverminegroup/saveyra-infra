package com.producer.utility;

import java.util.HashMap;

public class Data {
	HashMap<String, String> data;

	public HashMap<String, String> getData() {
		return data;
	}

	public void setData(HashMap<String, String> data) {
		this.data = data;
	}
	
}
